﻿#include "PhysXManager.h"
#include "PhysX/PxPhysicsAPI.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Graphics/Scenes/SceneManager.h"
//#include "ScriptManager.h"
#include "Render/RenderManager.h"
#include "Input/InputManager.h"
#include "XML/XML.h"
#include "Graphics/Scenes/SceneMesh.h"
#include "Graphics/Scenes/SceneMeshQuatRender.h"
#include "Graphics/Meshes/Mesh.h"
#include "Helper/ImguiHelper.h"

static physx::PxDefaultErrorCallback gDefaultErrorCallback;
static physx::PxDefaultAllocator gDefaultAllocatorCallback;

#if USE_PHYSX_DEBUG
#pragma comment(lib, "PhysX3Debug_x86.lib")
#pragma comment(lib, "PhysX3CommonDebug_x86.lib")
#pragma comment(lib, "PhysX3ExtensionsDebug.lib")
#pragma comment(lib, "PhysXProfileSDKDebug.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDKDebug.lib")
#pragma comment(lib, "PhysX3CharacterKinematicDebug_x86.lib")
#pragma comment(lib, "PhysX3CookingDebug_x86.lib")
#define					PVD_HOST			"127.0.0.1"
#else
#pragma comment(lib, "PhysX3_x86.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PhysXProfileSDK.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDK.lib")
#pragma comment(lib, "PhysX3CharacterKinematic_x86.lib")
#pragma comment(lib, "PhysX3Cooking_x86.lib")
#endif

#ifdef CHECKED_RELEASE
#undef CHECKED_RELEASE
#endif
#define CHECKED_RELEASE(x) if(x!=nullptr) {x->release(); x=nullptr;}

inline physx::PxVec3 CastVec(const Vect3f& v)
{
	return physx::PxVec3(v.x, v.y, v.z);
}
inline Vect3f CastVec(const physx::PxVec3& v)
{
	return Vect3f(v.x, v.y, v.z);
}
inline Vect3f CastVec(const physx::PxExtendedVec3& v)
{
	return Vect3f((float)v.x, (float)v.y, (float)v.z);
}

inline physx::PxQuat CastQuat(const Quatf& q)
{
	return physx::PxQuat(q.x, q.y, q.z, q.w);
}
inline Quatf CastQuat(const physx::PxQuat& q)
{
	return Quatf(q.x, q.y, q.z, q.w);
}

inline physx::PxMat44 CastTransform(const Mat44f& matrix)
{
	physx::PxMat44 physxMatrix;
	physxMatrix.column0 = physx::PxVec4(matrix.m00, matrix.m01, matrix.m02, matrix.m03);
	physxMatrix.column1 = physx::PxVec4(matrix.m10, matrix.m11, matrix.m12, matrix.m13);
	physxMatrix.column2 = physx::PxVec4(matrix.m20, matrix.m21, matrix.m22, matrix.m23);
	physxMatrix.column3 = physx::PxVec4(matrix.m30, matrix.m31, matrix.m32, matrix.m33);
	return physxMatrix;
}
/*NO BORRAR - DEPRONTO SI SIRVA PA ALGO*/
/*physx::PxFilterFlags FilterShader(
	physx::PxFilterObjectAttributes attributes0, physx::PxFilterData filterData0,
	physx::PxFilterObjectAttributes attributes1, physx::PxFilterData filterData1,
	physx::PxPairFlags& pairFlags, const void* constantBlock, physx::PxU32 constantBlockSize)
{
	// let triggers through, and do any other prefiltering you need. 
	/*if (physx::PxFilterObjectIsTrigger(attributes0) || physx::PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = physx::PxPairFlag::eTRIGGER_DEFAULT;
		return physx::PxFilterFlag::eDEFAULT;
	}
	// generate contacts for all that were not filtered above 
	pairFlags = physx::PxPairFlag::eCONTACT_DEFAULT | physx::PxPairFlag::eNOTIFY_TOUCH_FOUND;

	PxU32 ShapeGroup0 = filterData0.word0 & 31;
	PxU32 ShapeGroup1 = filterData1.word0 & 31;
	PxU32* groupCollisionFlags = (PxU32*)constantBlock;

	if ((groupCollisionFlags[ShapeGroup0] & (1 << ShapeGroup1)) == 0)
	return PxFilterFlag::eSUPPRESS;
	else
	return physx::PxFilterFlag::eDEFAULT;

	pairFlags = physx::PxPairFlag::eCONTACT_DEFAULT
		| physx::PxPairFlag::eTRIGGER_DEFAULT
		| physx::PxPairFlag::eNOTIFY_TOUCH_PERSISTS
		| physx::PxPairFlag::eNOTIFY_CONTACT_POINTS;
	return physx::PxFilterFlag::eDEFAULT;
}*/

/*class PlayerFilterCallback : physx::PxQueryFilterCallback
{
	physx::PxQueryHitType::Enum preFilter(
		const physx::PxFilterData& filterData, const physx::PxShape* shape, const physx::PxRigidActor* actor, physx::PxHitFlags& queryFlags)
	{
		physx::PxFilterData l_filterData2 = shape->getSimulationFilterData();
		if ((filterData.word0 & l_filterData2.word1) && (l_filterData2.word0 & filterData.word1))
		{
			return physx::PxQueryHitType::eTOUCH;
		}
		return physx::PxQueryHitType::eNONE;
	}

	physx::PxQueryHitType::Enum postFilter(const physx::PxFilterData& filterData, const physx::PxQueryHit& hit)
	{
		return physx::PxQueryHitType::eNONE;
	}
};*/


class CPhysXManagerImplementation :
	public CPhysXManager,
	public physx::PxSimulationEventCallback,
	public physx::PxUserControllerHitReport
{
public:
	CPhysXManagerImplementation();
	~CPhysXManagerImplementation();

	// CPhysXManager
	void AddCharacterController(const std::string& characterControllerName, float height, float radius, const Vect3f& position = Vect3f(0, 0, 0),
		const Quatf& orientation = Quatf(0, 0, 0, 1), const std::string& material = "default", float density = 15, int group = 1) override;

	// PxSimulationEventCallback
	void onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count)
	{
		std::string cas = m_ActorNames[0];
	}
	void onWake(physx::PxActor** actors, physx::PxU32 count)
	{
		std::string cas = m_ActorNames[0];
	}
	void onSleep(physx::PxActor** actors, physx::PxU32 count)
	{
		std::string cas = m_ActorNames[0];
	}
	void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 count) override;

	void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count) override;

	// PxUserControllerHitReport
	void onShapeHit(const physx::PxControllerShapeHit& hit)
	{
		std::string cas = m_ActorNames[0];
	};
	void onControllerHit(const physx::PxControllersHit& hit)
	{
		std::string cas = m_ActorNames[0];
	};
	void onObstacleHit(const physx::PxControllerObstacleHit& hit)
	{
		std::string cas = m_ActorNames[0];
	};
};

CPhysXManager::CPhysXManager(): m_Foundation(nullptr), m_PhysX(nullptr),  m_Dispatcher(nullptr), m_Scene(nullptr), m_Cooking(nullptr), m_ControllerManager(nullptr)
{
#	if USE_PHYSX_DEBUG
	m_DebugConnection = nullptr;
#endif
}

CPhysXManager::~CPhysXManager()
{
}

CPhysXManager* CPhysXManager::CreatePhysXManager()
{
	return new CPhysXManagerImplementation();
}

CPhysXManagerImplementation::~CPhysXManagerImplementation()
{
	CHECKED_RELEASE(m_ControllerManager);
	CHECKED_RELEASE(m_Scene);
	CHECKED_RELEASE(m_Dispatcher);
	physx::PxProfileZoneManager* profileZoneManager = m_PhysX->getProfileZoneManager();
#	if USE_PHYSX_DEBUG
	CHECKED_RELEASE(m_DebugConnection);
#	endif
	CHECKED_RELEASE(m_Cooking);
	CHECKED_RELEASE(m_PhysX);
	CHECKED_RELEASE(profileZoneManager);
	CHECKED_RELEASE(m_Foundation);
}

CPhysXManagerImplementation::CPhysXManagerImplementation()
{
	m_Foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
	assert(m_Foundation);
	physx::PxProfileZoneManager* profileZoneManager = nullptr;
#	if USE_PHYSX_DEBUG
	profileZoneManager = &physx::PxProfileZoneManager::createProfileZoneManager(m_Foundation);
#	endif
	m_PhysX = PxCreatePhysics(PX_PHYSICS_VERSION, *m_Foundation, physx::PxTolerancesScale(), true, profileZoneManager);
	assert(m_PhysX);
	m_Cooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_Foundation, physx::PxCookingParams(physx::PxTolerancesScale()));
	assert(m_Cooking);

#	if USE_PHYSX_DEBUG
	if (m_PhysX->getPvdConnectionManager())
	{
		m_PhysX->getVisualDebugger()->setVisualizeConstraints(true);
		m_PhysX->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
		m_PhysX->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_SCENEQUERIES, true);
		m_DebugConnection = physx::PxVisualDebuggerExt::createConnection(m_PhysX->getPvdConnectionManager(), PVD_HOST, 5425, 10);
	}
	else
	{
		m_DebugConnection = nullptr;
	}
#	endif

	m_Dispatcher = physx::PxDefaultCpuDispatcherCreate(2);

	physx::PxSceneDesc sceneDesc(m_PhysX->getTolerancesScale());
	sceneDesc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
	sceneDesc.cpuDispatcher = m_Dispatcher;
	sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
	sceneDesc.flags = physx::PxSceneFlag::eENABLE_ACTIVETRANSFORMS;
	//sceneDesc.flags = physx::PxSceneFlag::eENABLE_ACTIVETRANSFORMS | physx::PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS | physx::PxSceneFlag::eENABLE_KINEMATIC_PAIRS;
	m_Scene = m_PhysX->createScene(sceneDesc);
	assert(m_Scene);

	m_Scene->setSimulationEventCallback(this);

	m_ControllerManager = PxCreateControllerManager(*m_Scene, true);
	m_ControllerManager->setOverlapRecoveryModule(true);

#	if USE_PHYSX_DEBUG
	if (m_DebugConnection != nullptr)
	{
		m_Scene->setVisualizationParameter(physx::PxVisualizationParameter::eJOINT_LOCAL_FRAMES, 1.0f);
		m_Scene->setVisualizationParameter(physx::PxVisualizationParameter::eJOINT_LIMITS, 1.0f);
	}
#	endif

	// default material
	RegisterMaterial("default", 0.5f, 0.5f, 0.6f);
}

Vect3f CPhysXManager::MouseScreenToWorldPosition()
{
	Vect2f mousePosition = Vect2f(static_cast<float>(engine::CEngine::GetInstance().GetInputManager().GetMouseX()), static_cast<float>(engine::CEngine::GetInstance().GetInputManager().GetMouseY()));
	Vect3f mouseWorldPosition = v3fZERO;
	float height = static_cast<float>(engine::CEngine::GetInstance().GetRenderManager().GetHeight());
	float width = static_cast<float>(engine::CEngine::GetInstance().GetRenderManager().GetWidth());
	Mat44f viewProj = engine::CEngine::GetInstance().GetRenderManager().GetViewProjectionMatrix();

	mouseWorldPosition.x = (((2.0f * mousePosition.x) / width) - 1) / viewProj.m11;
	mouseWorldPosition.y = -(((2.0f * mousePosition.y) / height) - 1) / viewProj.m22;
	mouseWorldPosition.z = 1.0f;

	return mouseWorldPosition;
}

void CPhysXManagerImplementation::onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count)
{
	//Se procesa cuando un trigger hace contacto con un objeto
	for (physx::PxU32 i = 0; i < count; i++)
	{
		// ignore pairs when shapes have been deleted
		if ((pairs[i].flags & (physx::PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | physx::PxTriggerPairFlag::eREMOVED_SHAPE_OTHER)))
			continue;

		size_t indexTrigger = (size_t)pairs[i].triggerActor->userData;
		size_t indexActor = (size_t)pairs[i].otherActor->userData;

		std::string triggerName = m_ActorNames[indexTrigger];
		std::string actorName = m_ActorNames[indexActor];

		engine::scenes::CSceneNode* nodeTrigger = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(triggerName);
		engine::scenes::CSceneNode* nodeOther = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(actorName);

		if (nodeTrigger != nullptr && nodeOther != nullptr)
		{
			if (m_Triggers[triggerName])
			{
				nodeTrigger->OnTrigger(nodeOther);
				nodeOther->OnTrigger(nodeTrigger);
			}
			else if (!m_Triggers[triggerName] && nodeTrigger->WasTriggered(nodeOther) && nodeOther->WasTriggered(nodeTrigger))
			{
				nodeTrigger->OnTrigger(nodeOther);
				nodeOther->OnTrigger(nodeTrigger);
			}
		}
	}
}

void CPhysXManagerImplementation::onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 count)
{
	//Se procesa cuando un trigger hace contacto con un objeto
	for (physx::PxU32 i = 0; i < count; i++)
	{
		
		/*if ( pairs[i].flags & (physx::PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | physx::PxTriggerPairFlag::eREMOVED_SHAPE_OTHER) )
			continue;*/
		size_t indexTrigger = (size_t)pairHeader.actors[0]->userData;
		size_t indexActor = (size_t)pairHeader.actors[1]->userData;

		std::string triggerName = m_ActorNames[indexTrigger];
		std::string actorName = m_ActorNames[indexActor];

		triggerName = actorName;

		
	}
}

void CPhysXManager::RegisterMaterial(const std::string &name, float staticFriction, float dynamicFriction, float restitution)
{
	assert(m_Materials.find(name) == m_Materials.end());
	m_Materials[name] = m_PhysX->createMaterial(staticFriction, dynamicFriction, restitution);
}

void CPhysXManager::CreatePlane(std::string _name, const std::string _Material, Vect3f planeNormal, float planeDistance,
	Vect3f position, Quatf orientation)
{
	physx::PxMaterial* l_Material = m_Materials[_Material];
	physx::PxRigidStatic* groundPlane = physx::PxCreatePlane(*m_PhysX, physx::PxPlane(CastVec(planeNormal), planeDistance), *l_Material);
	size_t index = AddActorToObjSys(_name, position, orientation, groundPlane);
	groundPlane->userData = (void*)index;
	m_Scene->addActor(*groundPlane);
	physx::PxShape* shape;
	size_t numShapes = groundPlane->getShapes(&shape, 1);
	assert(numShapes == 1);
}

CPhysXManager::CharacterControllerData CPhysXManager::MoveCharacter(const std::string& name, const Vect3f& dir, float speed, float dt)
{
	return MoveCharacterController(name, dir * speed, dt);
}

engine::scenes::CSceneNode* CPhysXManager::GetPlayer(const std::string& name) const
{
	return engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("animated_models")->operator()(name);
}

void CPhysXManager::Update(float dt)
{
	m_LeftoverSeconds += dt;
	if (m_LeftoverSeconds >= PHYSX_UPDATE_STEP)
	{
		m_Scene->simulate(static_cast<physx::PxReal>(PHYSX_UPDATE_STEP));
		m_Scene->fetchResults(true);

		physx::PxU32 numActiveTransforms;
		const physx::PxActiveTransform* activeTransforms = m_Scene->getActiveTransforms(numActiveTransforms);

		for (physx::PxU32 i = 0; i < numActiveTransforms; ++i)
		{
			uintptr_t index = (uintptr_t)(activeTransforms[i].userData);
			m_ActorPositions[index] = CastVec(activeTransforms[i].actor2World.p);
			m_ActorOrientations[index] = CastQuat(activeTransforms[i].actor2World.q);
		}

		/*apply gravity to actors*/
		for (std::map<std::string, physx::PxController*>::iterator it = m_CharacterControllers.begin(); it != m_CharacterControllers.end(); ++it)
		{
			MoveCharacter(it->first, Vect3f(0.0f, 1.0f, 0.0f), -9.81f, dt);
		}

		m_LeftoverSeconds = static_cast<float>(fmod(m_LeftoverSeconds, PHYSX_UPDATE_STEP));
	}
}

size_t CPhysXManager::AddActorToObjSys(std::string actorName, Vect3f position, Quatf rotation, physx::PxActor* actor)
{
	size_t index = m_Actors.size();
	m_ActorIndexs[actorName] = index;
	m_ActorNames.push_back(actorName);
	m_ActorPositions.push_back(position);
	m_ActorOrientations.push_back(rotation);
	m_Actors.push_back(actor);
	return index;
}

Quatf CPhysXManager::GetActorOrientation(const std::string& actorName) const
{
	size_t index = m_ActorIndexs.find(actorName)->second;
	return m_ActorOrientations[index];
}

void CPhysXManager::GetActorTransform(const std::string& actorName, Vect3f& position, Quatf& orientation) const
{
	size_t index = m_ActorIndexs.find(actorName)->second;
	position = m_ActorPositions[index];
	orientation = m_ActorOrientations[index];
}

void CPhysXManager::SetActorPosition(const std::string& actorName, Vect3f& position)
{
	size_t index = m_ActorIndexs.find(actorName)->second;

	physx::PxTransform pxTransform = GetPxTransform(actorName);

	pxTransform.p.x = position.x;
	pxTransform.p.y = position.y;
	pxTransform.p.z = position.z;

	if (pxTransform.isValid())
	{
		auto flag = m_Actors[index]->getType();

		//add mpre types
		switch (flag)
		{
		case physx::PxActorType::eRIGID_DYNAMIC:
			m_Actors[index]->isRigidDynamic()->setGlobalPose(pxTransform);
			break;
		case physx::PxActorType::eRIGID_STATIC:
			m_Actors[index]->isRigidStatic()->setGlobalPose(pxTransform);
			break;
		}
		m_ActorPositions[index] = position;
	}

	engine::scenes::CSceneNode* anim = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("animated_models")->operator()(actorName);
	//engine::scenes::CSceneNode* anim = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(actorName);
	anim->SetPosition(position);
}


physx::PxTransform CPhysXManager::GetPxTransform(const std::string& actorName)
{
	size_t index = m_ActorIndexs.find(actorName)->second;
	Vect3f position = m_ActorPositions[index];
	Quatf rotation = m_ActorOrientations[index];

	physx::PxVec3 pxVec;
	pxVec.x = position.x;
	pxVec.y = position.y;
	pxVec.z = position.z;

	physx::PxQuat pxQuat;
	pxQuat.x = rotation.x;
	pxQuat.y = rotation.y;
	pxQuat.z = rotation.z;
	pxQuat.w = rotation.w;

	physx::PxTransform pxTransform(pxVec, pxQuat);

	return pxTransform;
}

void CPhysXManager::SetActorRotation(const std::string& actorName, float& yaw, float& pitch, float& roll)
{
	size_t index = m_ActorIndexs.find(actorName)->second;

	physx::PxTransform pxTransform = GetPxTransform(actorName);

	Quatf quaternion;
	quaternion.QuatFromYawPitchRoll(yaw, pitch, roll);
	pxTransform.q.x = quaternion.x;
	pxTransform.q.y = quaternion.y;
	pxTransform.q.z = quaternion.z;
	pxTransform.q.w = quaternion.w;

	if (pxTransform.isValid())
	{
		auto flag = m_Actors[index]->getType();
		//add more types
		switch (flag)
		{
		case physx::PxActorType::eRIGID_DYNAMIC:
			m_Actors[index]->isRigidDynamic()->setGlobalPose(pxTransform);
			break;
		case physx::PxActorType::eRIGID_STATIC:
			m_Actors[index]->isRigidStatic()->setGlobalPose(pxTransform);
			break;
		}
		m_ActorOrientations[index] = quaternion;
	}
}

void CPhysXManager::RemoveActor(const std::string _ActorName)
{
	auto it = m_ActorIndexs.find(_ActorName);
	if (it != m_ActorIndexs.end())
	{
		size_t index = m_ActorIndexs.find(_ActorName)->second;
		auto it_controller = m_CharacterControllers.find(_ActorName);
		if (it_controller != m_CharacterControllers.end())
		{
			CHECKED_RELEASE(it_controller->second);
			m_CharacterControllers.erase(it_controller);
		}
		else
		{
			m_Scene->removeActor(*m_Actors[index]);
			m_Actors[index]->release();
		}
		if (index == (m_Actors.size() - 1))
		{
			m_Actors.resize(m_Actors.size() - 1);
			m_ActorNames.resize(m_Actors.size());
			m_ActorPositions.resize(m_Actors.size());
			m_ActorOrientations.resize(m_Actors.size());
			m_ActorIndexs.erase(_ActorName);
			m_Triggers.erase(_ActorName);
		}
		else if (m_Actors.size() > 1)
		{
			m_Actors[m_Actors.size() - 1]->userData = reinterpret_cast<void *>(index);
			m_Actors[index] = m_Actors[m_Actors.size() - 1];
			m_Actors.resize(m_Actors.size() - 1);

			m_ActorNames[index] = m_ActorNames[m_Actors.size()];
			m_ActorNames.resize(m_Actors.size());

			m_ActorPositions[index] = m_ActorPositions[m_Actors.size()];
			m_ActorPositions.resize(m_Actors.size());

			m_ActorOrientations[index] = m_ActorOrientations[m_Actors.size()];
			m_ActorOrientations.resize(m_Actors.size());

			m_ActorIndexs[m_ActorNames[index]] = index;

			m_ActorIndexs.erase(_ActorName);
			m_Triggers.erase(_ActorName);
		}
		else
		{
			m_Actors.resize(0);
			m_ActorNames.resize(0);
			m_ActorPositions.resize(0);
			m_ActorOrientations.resize(0);
			m_ActorIndexs.clear();
			m_Triggers.clear();
		}
	}
}

void CPhysXManager::AddCharacterController(
	const std::string& characterControllerName, float height, float radius, const Vect3f& position,
	const Quatf& orientation, const std::string& material, float density, int group)
{

}

void CPhysXManagerImplementation::AddCharacterController(
	const std::string& characterControllerName, float height, float radius, const Vect3f& position,
	const Quatf& orientation, const std::string& material, float density, int group)
{
	/*assert(m_Materials.find(material) != m_Materials.end()); // missing material!
	assert(m_CharacterControllers.find(characterControllerName) == m_CharacterControllers.end()); // duplicated key!
	assert(m_ActorIndexs.find(characterControllerName) == m_ActorIndexs.end()); // duplicated key!

	assert(m_Actors.size() == m_ActorNames.size()); // AOS sync fail
	assert(m_Actors.size() == m_ActorPositions.size()); // AOS sync fail
	assert(m_Actors.size() == m_ActorOrientations.size()); // AOS sync fail
	assert(m_Actors.size() == m_ActorIndexs.size()); // AOS sync fail*/

	auto *l_Material = m_Materials[material];

	size_t index = m_Actors.size();

	physx::PxCapsuleControllerDesc desc;
	desc.height = height;
	desc.radius = radius;
	desc.climbingMode = physx::PxCapsuleClimbingMode::eEASY;
	desc.slopeLimit = cosf(3.1415f / 6); // 30
	desc.stepOffset = 0.5f;
	desc.density = density;
	desc.reportCallback = this;
	desc.position = physx::PxExtendedVec3(position.x, position.y + radius + height * 0.5f, position.z);
	desc.material = l_Material;
	desc.userData = reinterpret_cast<void*>(index);

	physx::PxController* cct = m_ControllerManager->createController(desc);

	//add default group to characters
	physx::PxShape* shape;
	cct->getActor()->getShapes(&shape, 1);
	physx::PxFilterData filterData;
	filterData.setToDefault();
	filterData.word0 = group;

	shape->setQueryFilterData(filterData);

	m_CharacterControllers[characterControllerName] = cct;

	m_ActorIndexs[characterControllerName] = index;
	m_ActorNames.push_back(characterControllerName);
	m_ActorPositions.push_back(position);
	m_ActorOrientations.push_back(orientation);
	m_Actors.push_back(cct->getActor());

	cct->getActor()->userData = reinterpret_cast<void*>(index);

	
}

CPhysXManager::CharacterControllerData CPhysXManager::MoveCharacterController(const std::string& characterControllerName, const Vect3f& movement, float elapsedTime)
{
	physx::PxController* cct = m_CharacterControllers[characterControllerName];
	//static PlayerFilterCallback s_PlayerFilterCallback;
	//const physx::PxControllerFilters filters(nullptr, (physx::PxQueryFilterCallback*)&s_PlayerFilterCallback, nullptr);
	const physx::PxControllerFilters filters(nullptr, nullptr, nullptr);
	CharacterControllerData result;
	physx::PxExtendedVec3 p;
	physx::PxVec3 v;
	if (cct != nullptr)
	{
		physx::PxRigidDynamic* actor = cct->getActor();
		size_t index = reinterpret_cast<size_t>(actor->userData);

		cct->move(CastVec(movement), movement.Length() * 0.01f, elapsedTime, filters);


		p = cct->getFootPosition();
		v = actor->getLinearVelocity();

		m_ActorPositions[index] = CastVec(p);

		engine::scenes::CSceneNode* anim = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("animated_models")->operator()(characterControllerName);
		if (anim != nullptr)
		{
			anim->SetPosition(m_ActorPositions[index]);
		}
#ifdef DEBUG
		else
		{
			engine::CEngine::GetInstance().GetImGUI().Log("[WARNING] Physx actor not found in scene: " + characterControllerName);
		}
#endif
	}
#ifdef DEBUG
	else
	{
		engine::CEngine::GetInstance().GetImGUI().Log("[WARNING] Physx actor not found: " + characterControllerName);
	}
#endif
	
	result.position = CastVec(p);
	result.linearVelocity = CastVec(v);
	return result;
}

void CPhysXManager::SetCharacterControllerPosition(const std::string& characterControllerName, const Vect3f& position)
{
	physx::PxController* cct = m_CharacterControllers[characterControllerName];
	physx::PxRigidDynamic* actor = cct->getActor();
	size_t index = (size_t)actor->userData;

	cct->setFootPosition(physx::PxExtendedVec3(position.x, position.y, position.z));
	m_ActorPositions[index] = CastVec(cct->getFootPosition());

	engine::scenes::CSceneNode* anim = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("animated_models")->operator()(characterControllerName);
	if (anim != nullptr)
	{
		anim->SetPosition(m_ActorPositions[index]);
	}
#ifdef DEBUG
	else
	{
		engine::CEngine::GetInstance().GetImGUI().Log("[WARNING] Physx actor not found: " + characterControllerName);
	}
#endif
}

bool CPhysXManager::Raycast(const Vect3f& origin, const Vect3f& end, RaycastData* result_)
{
	Vect3f dir = end - origin;
	float len = dir.Length();
	if (origin != Vect3f(0, 0, 0))
	{
		dir.Normalize(1);
	}
	else
	{
		dir = Vect3f(1, 0, 0);
	}

	physx::PxFilterData filterData;
	filterData.setToDefault();
	filterData.word0 = 1;

	physx::PxRaycastBuffer hit;
	bool status = m_Scene->raycast(CastVec(origin), CastVec(dir), len, hit);
	if (status && result_ != nullptr)
	{
		result_->position = Vect3f(hit.block.position.x, hit.block.position.y, hit.block.position.z);
		result_->normal = Vect3f(hit.block.normal.x, hit.block.normal.y, hit.block.normal.z);
		result_->distance = hit.block.distance;
		result_->actor = m_ActorNames[(size_t)hit.block.actor->userData];
	}
	return status;
}

physx::PxShape* CPhysXManager::CreateStaticShape(const std::string _name, const physx::PxGeometry &_geometry, const std::string _Material,
	Vect3f _position, Quatf _orientation, bool trigger, int _group)
{
	physx::PxMaterial* l_Material = m_Materials[_Material];

	physx::PxShape* shape = m_PhysX->createShape(_geometry, *l_Material);
	physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(_position), CastQuat(_orientation)));

	//add filter data
	physx::PxFilterData filterData;
	filterData.word0 = _group;
	shape->setQueryFilterData(filterData);
	
	if (trigger){
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		m_Triggers[_name] = true;
	}

	body->attachShape(*shape);
	size_t index = AddActorToObjSys(_name, _position, _orientation, body);
	body->userData = (void*)index;
	m_Scene->addActor(*body);

	return shape;
}

physx::PxShape* CPhysXManager::CreateDynamicShape(const std::string _name, const physx::PxGeometry &_geometry, const std::string _Material,
	Vect3f _position, Quatf _orientation, bool trigger, float _density, bool isKinematic, int _group)
{
	//WOKING CODE
	physx::PxMaterial* l_Material = m_Materials[_Material];
	physx::PxShape* shape = m_PhysX->createShape(_geometry, *l_Material);

	//add filter data
	physx::PxFilterData filterData;
	filterData.word0 = _group;
	shape->setQueryFilterData(filterData);

	if (trigger)
	{
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		m_Triggers[_name] = true;
	}

	physx::PxRigidDynamic* body = m_PhysX->createRigidDynamic(physx::PxTransform(CastVec(_position), CastQuat(_orientation)));
	body->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, isKinematic);
	body->attachShape(*shape);
	size_t index = AddActorToObjSys(_name, _position, _orientation, body);
	body->userData = (void*)index;	
	physx::PxRigidBodyExt::updateMassAndInertia(*body, _density);
	m_Scene->addActor(*body);

	return shape;
}

//void CPhysXManager::CreateConvexMesh(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation, std::string _group)
void CPhysXManager::CreateConvexMesh(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
	uint32 numVertexes, void* vertexData, uint32 numIndices, void* indexData, bool isStatic, bool trigger, bool isKinematic, float density,
	int _group)
{
	physx::PxConvexMeshDesc convexDesc;
	convexDesc.points.count = numVertexes;
	convexDesc.points.stride = sizeof(physx::PxVec3);
	convexDesc.points.data = vertexData;

	/*convexDesc.indices.count = numIndices;
	convexDesc.indices.stride = 3 * sizeof(physx::PxU16);
	convexDesc.indices.data = indexData;*/

	convexDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX;


	physx::PxDefaultMemoryOutputStream buf;
	physx::PxConvexMeshCookingResult::Enum result;
	m_Cooking->cookConvexMesh(convexDesc, buf, &result);

	physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	physx::PxConvexMesh* convexMesh = m_PhysX->createConvexMesh(input);

	CreateShape(
		_name,
		physx::PxConvexMeshGeometry(convexMesh),
		_Material,
		_position,
		_orientation,
		isStatic,
		trigger,
		isKinematic,
		density,
		_group);
}

void CPhysXManager::CreateTriangleMesh(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
	uint32 numVertexes, void* vertexData, uint32 numIndices, void* indexData, bool isStatic, bool trigger, bool isKinematic, float density,
	int _group)
{
	physx::PxTriangleMeshDesc meshDesc;
	meshDesc.points.count = numVertexes;
	meshDesc.points.stride = sizeof(physx::PxVec3);
	meshDesc.points.data = vertexData;

	meshDesc.triangles.count = numIndices / 3;
	meshDesc.triangles.stride = 3 * sizeof(physx::PxU16);
	meshDesc.triangles.data = indexData;
	meshDesc.flags = physx::PxMeshFlag::e16_BIT_INDICES;

	physx::PxDefaultMemoryOutputStream buf;
	m_Cooking->cookTriangleMesh(meshDesc, buf);

	physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	physx::PxTriangleMesh* triangleMesh = m_PhysX->createTriangleMesh(input);

	CreateShape(
		_name,
		physx::PxTriangleMeshGeometry(triangleMesh),
		_Material,
		_position,
		_orientation,
		isStatic,
		trigger,
		isKinematic,
		density,
		_group);
}

void CPhysXManager::CreateBox(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
	Vect3f size, bool isStatic, bool trigger, bool isKinematic, float density, int _group)
{
	CreateShape(
		_name,
		physx::PxBoxGeometry(size.x / 2, size.y / 2, size.z / 2),
		_Material,
		_position,
		_orientation,
		isStatic,
		trigger,
		isKinematic,
		density,
		_group);
}

void CPhysXManager::CreateSphere(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
	float _radius, bool isStatic, bool trigger, bool isKinematic, float density, int _group)
{
	CreateShape(
		_name,
		physx::PxSphereGeometry(_radius),
		_Material,
		_position,
		_orientation,
		isStatic,
		trigger,
		isKinematic,
		density,
		_group);
}

void CPhysXManager::CreateShape(const std::string _name, const physx::PxGeometry &_geometry, const std::string _Material, Vect3f _position,
	Quatf _orientation, bool isStatic, bool trigger, bool isKinematic, float density, int _group)
{
	if (isStatic){
		CreateStaticShape(
			_name,
			_geometry,
			_Material,
			_position,
			_orientation,
			trigger,
			_group)->release();
	}
	else{
		CreateDynamicShape(
			_name,
			_geometry,
			_Material,
			_position,
			_orientation,
			trigger,
			density,
			isKinematic,
			_group)->release();
	}
}

//groups are the tags that the raycast will select. If a tag group1 is in an actor the hit will be active
bool CPhysXManager::Raycast(const Vect3f _origin, const Vect3f _end, int _GROUPS, RaycastData* result_)
{
	//physx::PxFilterData l_filterData;
	physx::PxQueryFilterData filterData = physx::PxQueryFilterData();
	filterData.data.word0 = _GROUPS;
	//filterData.flags = physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
	//l_filterData.setToDefault();
	//l_filterData.word0 = _GROUPS;
	physx::PxRaycastBuffer l_hit;
	bool didHit = m_Scene->raycast(
		CastVec(_origin),
		CastVec((_end - _origin).GetNormalized()),
		_origin.Distance(_end),
		l_hit,
		physx::PxHitFlags(physx::PxHitFlag::eDEFAULT),
		filterData
		);
	if (didHit)
	{
		if (l_hit.hasBlock)
		{
			result_->position = CastVec(l_hit.block.position);
			result_->normal = CastVec(l_hit.block.normal);
			result_->distance = l_hit.block.distance;
			result_->actor = m_ActorNames[(size_t)l_hit.block.actor->userData];
		}
		else
			didHit = false;
	}
	return didHit;
}

void CPhysXManager::setKinematicTarget(std::string name, Mat44f transform)
{
	size_t index = m_ActorIndexs[name];
	physx::PxActor* actor = m_Actors[index];
	physx::PxRigidDynamic* body = actor->isRigidDynamic();

	physx::PxMat44& matrix = CastTransform(transform);
	physx::PxTransform ptransform(matrix);

	body->setKinematicTarget(ptransform);
}

void CPhysXManager::ActiveTrigger(std::string name)
{
	SetTriggerState(name, true);
}

void CPhysXManager::DisableTrigger(std::string name)
{
	SetTriggerState(name, false);
}

void CPhysXManager::SetTriggerState(std::string name, bool state)
{
	if (m_Triggers.find(name)!=m_Triggers.end()){
		m_Triggers[name] = state;
	}
}

void CPhysXManager::CreatePhysxObject(engine::scenes::CSceneNode* node, const CXMLElement* physxElement)
{
	std::string material = physxElement->GetAttribute<std::string>("material", "default");
	bool isStatic = physxElement->GetAttribute<bool>("is_static", true);
	bool isTrigger = physxElement->GetAttribute<bool>("is_trigger", false);
	bool isKinemmatic = physxElement->GetAttribute<bool>("is_kinematic", false);
	float density = physxElement->GetAttribute<float>("density", 1.0f);
	int group = physxElement->GetAttribute<int>("group", 1);
	PhysxGroups physxGroup;
	bool grouped = EnumString<PhysxGroups>::ToEnum(physxGroup, physxElement->GetAttribute<std::string>("group", "default"));
	if (grouped)
	{
		group = physxGroup;
	}

	float radius = physxElement->GetAttribute<float>("radius", 1.0f);
	Vect3f size = physxElement->GetAttribute<Vect3f>("size", Vect3f(0.0f, 0.0f, 0.0f));
	Vect3f physx_position = physxElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
	Quatf physx_rotation = physxElement->GetAttribute<Quatf>("rotation", Quatf(0.0f, 0.0f, 0.0f,1.0f));
	Vect3f final_position;
	Quatf final_rotation;
	if (physx_position != Vect3f(0.0f, 0.0f, 0.0f)){
		final_position = physx_position;
	}
	else
	{
		final_position = node->GetPosition();
	}
	if (!physx_rotation.Compare(Quatf(0.0f, 0.0f, 0.0f, 1.0f))){
		final_rotation = physx_rotation;
	}
	else
	{
		final_rotation = node->GetRotation();
	}

	node->m_hasPhysx = true;
	node->m_IsStatic = isStatic;
	node->m_IsKinematic = isKinemmatic;
	node->m_IsTrigger = isTrigger;

	std::string l_shape = physxElement->GetAttribute<std::string>("shape", "");
	assert(l_shape != "");
	EPhysxShape shape;
	EnumString<EPhysxShape>::ToEnum(shape, l_shape);

	engine::scenes::CSceneMesh* l_SM = nullptr;
	engine::scenes::CSceneMeshQuatRender* l_SMQuat = nullptr;
	engine::materials::CMesh* l_Mesh = nullptr;
	std::string l_physxMeshName;
	l_SMQuat = dynamic_cast<engine::scenes::CSceneMeshQuatRender*>(node);
	if (l_SMQuat)
	{
		l_Mesh = l_SMQuat->GetMesh();
		l_physxMeshName = l_SMQuat->m_PhysxFilename;
	}
	l_SM = dynamic_cast<engine::scenes::CSceneMesh*>(node);
	if (l_SM)
	{
		l_Mesh = l_SM->GetMesh();
		l_physxMeshName = l_SM->m_PhysxFilename;
	}

	switch (shape)
	{
	case EPhysxShape::eBox:
		if (size == Vect3f(0.0f, 0.0f, 0.0f)){
			if (l_Mesh)
			{
				size.x = mathUtils::Abs(l_Mesh->GetAABB().GetMax().x - l_Mesh->GetAABB().GetMin().x);
				size.y = mathUtils::Abs(l_Mesh->GetAABB().GetMax().y - l_Mesh->GetAABB().GetMin().y);
				size.z = mathUtils::Abs(l_Mesh->GetAABB().GetMax().z - l_Mesh->GetAABB().GetMin().z);
			}
		}
		node->m_Size = size;
		CreateBox(node->GetName(), material, final_position, final_rotation, size, isStatic, isTrigger, isKinemmatic, density,
			group);
		break;

	case EPhysxShape::eSphere:
		node->m_Radius = radius;
		CreateSphere(node->GetName(), material, final_position, final_rotation, radius, isStatic, isTrigger, isKinemmatic,
			density, group);
		break;

	case EPhysxShape::eConvexMesh:
		if (l_Mesh){
			engine::materials::CPhysxMesh * l_PhysxMesh = l_Mesh->GetPhysxMesh(l_physxMeshName);
			CreateConvexMesh(node->GetName(), material, node->GetPosition(), node->GetRotation(), l_PhysxMesh->numVertexes,
				l_PhysxMesh->m_Vertexes, l_PhysxMesh->numIndices, l_PhysxMesh->m_Indices, isStatic, isTrigger, isKinemmatic, density,
				group);
			delete l_PhysxMesh;
		}
		break;

	case EPhysxShape::eTriangleMesh:
		if (l_Mesh){
			engine::materials::CPhysxMesh * l_PhysxMesh = l_Mesh->GetPhysxMesh(l_physxMeshName);
			CreateTriangleMesh(node->GetName(), material, node->GetPosition(), node->GetRotation(), l_PhysxMesh->numVertexes,
				l_PhysxMesh->m_Vertexes, l_PhysxMesh->numIndices, l_PhysxMesh->m_Indices, isStatic, isTrigger, isKinemmatic, density,
				group);
			delete l_PhysxMesh;
		}
		break;

	case EPhysxShape::ePlane:
		Vect3f normal = physxElement->GetAttribute<Vect3f>("plane_normal", v3fFRONT);
		float Ypos = physxElement->GetAttribute<float>("plane_offset", 0.0f);
		CreatePlane(node->GetName(), material, normal, Ypos, node->GetPosition(), node->GetRotation());
		break;
	}
}
