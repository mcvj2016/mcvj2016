#ifndef _Physx_ENGINE_DPVD1_16022017194800_H
#define _Physx_ENGINE_DPVD1_16022017194800_H
#include "Math/Transform.h"
#include <map>
#include <vector>
#include "Math/Quaternion.h"
#include "Utils/EnumToString.h"

namespace physx
{
	class PxFoundation;
	class PxPhysics;
	class PxDefaultCpuDispatcher;
	class PxScene;
	class PxCooking;
	class PxMaterial;
	class PxActor;
	class PxRigidActor;
	class PxControllerManager;
	class PxController;
	class PxJoint;
	class PxTransform;
	class PxGeometry;
	class PxShape;

	namespace debugger
	{
		namespace comm
		{
			class PvdConnection;
		}
	}
	typedef debugger::comm::PvdConnection PxVisualDebuggerConnection;
}

typedef struct SRaycastData
{
	Vect3f position;
	Vect3f normal;
	float distance;
	std::string actor;
} RaycastData;

#ifdef _DEBUG
#define USE_PHYSX_DEBUG 1
#else
#define USE_PHYSX_DEBUG 0
#endif
#define PHYSX_UPDATE_STEP 0.05

namespace engine
{
	namespace scenes
	{
		class CSceneNode;
	}
}

class CPhysXManager
{
	struct CharacterControllerData
	{
		Vect3f position;
		Vect3f linearVelocity;
	};


protected:
	CPhysXManager();

	physx::PxFoundation *m_Foundation;
	physx::PxPhysics *m_PhysX;

#	if USE_PHYSX_DEBUG
	physx::PxVisualDebuggerConnection *m_DebugConnection;
#	endif

	physx::PxDefaultCpuDispatcher *m_Dispatcher;
	physx::PxScene *m_Scene;
	physx::PxCooking *m_Cooking;
	physx::PxControllerManager *m_ControllerManager;

	float m_LeftoverSeconds = 0.0f;

	std::map<std::string, physx::PxController*> m_CharacterControllers;

	std::map<std::string, bool> m_Triggers;
	std::map<std::string, size_t> m_ActorIndexs;
	std::vector<std::string> m_ActorNames;
	std::vector<Vect3f> m_ActorPositions;
	std::vector<Quatf> m_ActorOrientations;
	std::vector<physx::PxActor*> m_Actors;

public:
	enum EPhysxShape
	{
		eBox = 0,
		eSphere,
		eConvexMesh,
		eTriangleMesh,
		ePlane,
	};

	enum PhysxGroups
	{
		Default = (1 << 0),
		Ground = (1 << 1),
		Wall = (1 << 2),
		Event = (1 << 3),
		Enemy = (1 << 4)
	};

	static int GetPhysicalGroups()
	{
		return Default | Wall | Ground | Enemy;
	}

	static CPhysXManager* CreatePhysXManager();
	virtual ~CPhysXManager();
	std::map<std::string, physx::PxMaterial*> m_Materials;

	void Update(float dt);
	void RegisterMaterial(const std::string &name, float staticFriction, float dynamicFriction, float restitution);

	bool Raycast(const Vect3f& origin, const Vect3f& end, RaycastData* result_);
	bool Raycast(const Vect3f _origin, const Vect3f _end, int _GROUPS, RaycastData* result_);
	CharacterControllerData MoveCharacter(const std::string& name,const Vect3f& dir, float speed, float dt);
	engine::scenes::CSceneNode* GetPlayer(const std::string& name) const;

	size_t AddActorToObjSys(std::string actorName, Vect3f position, Quatf orientation, physx::PxActor* actor);
	Vect3f MouseScreenToWorldPosition();

	Quatf GetActorOrientation(const std::string& actorName) const;
	void GetActorTransform(const std::string& actorName, Vect3f& position, Quatf& orientation) const;

	CharacterControllerData MoveCharacterController(const std::string& characterControllerName, const Vect3f& movement, float elapsedTime);
	void CPhysXManager::SetCharacterControllerPosition(const std::string& characterControllerName, const Vect3f& position);

	void SetActorPosition(const std::string& actorName, Vect3f& position);
	physx::PxTransform GetPxTransform(const std::string& actorName);
	void SetActorRotation(const std::string& actorName, float& yaw, float& pitch, float& roll);
	void RemoveActor(const std::string _ActorName);
	void CPhysXManager::setKinematicTarget(std::string name, Mat44f transform);
	void CPhysXManager::ActiveTrigger(std::string name);
	void CPhysXManager::DisableTrigger(std::string name);

	virtual void AddCharacterController(const std::string& characterControllerName, float height, float radius, const Vect3f& position = Vect3f(0, 0, 0),
		const Quatf& orientation = Quatf(0, 0, 0, 1), const std::string& material = "default", float density = 15, int group = 1);

	void CreatePhysxObject(engine::scenes::CSceneNode* node, const CXMLElement* physxElement);

	void CreateConvexMesh(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
		uint32 numVertexes, void* vertexData, uint32 numIndices, void* indexData, bool isStatic = true, bool trigger = false,
		bool isKinematic = false, float density = 1.0f, int _group = 0);

	void CreateTriangleMesh(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
		uint32 numVertexes, void* vertexData, uint32 numIndices, void* indexData, bool isStatic = true, bool trigger = false,
		bool isKinematic = false, float density = 1.0f, int _group = 0);

	void CreateBox(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
		Vect3f size, bool isStatic = true, bool trigger = false, bool isKinematic = false, float density = 1.0f, int _group = 0);

	void CreateSphere(const std::string _name, const std::string _Material, Vect3f _position, Quatf _orientation,
		float _radius, bool isStatic = true, bool trigger = false, bool isKinematic = false, float density = 1.0f, int _group = 0);

	void CreatePlane(std::string _name, const std::string _Material, Vect3f planeNormal, float planeDistance,
		Vect3f position, Quatf orientation);

private:
	void CreateShape(const std::string _name, const physx::PxGeometry &_geometry, const std::string _Material, Vect3f _position,
		Quatf _orientation, bool isStatic, bool trigger, bool isKinematic, float density, int _group = 0);
	physx::PxShape* CreateStaticShape(const std::string _name, const physx::PxGeometry& _geometry, const std::string _Material,
		Vect3f _position, Quatf _orientation, bool trigger, int _group = 0);
	physx::PxShape* CreateDynamicShape(const std::string _name, const physx::PxGeometry& _geometry, const std::string _Material,
		Vect3f _position, Quatf _orientation, bool trigger, float _density, bool isKinematic, int _group = 0);

	void CPhysXManager::SetTriggerState(std::string name, bool state);
};

Begin_Enum_String(CPhysXManager::EPhysxShape)
{
	Enum_String_Id(CPhysXManager::EPhysxShape::eBox, "box");
	Enum_String_Id(CPhysXManager::EPhysxShape::eSphere, "sphere");
	Enum_String_Id(CPhysXManager::EPhysxShape::eConvexMesh, "convex");
	Enum_String_Id(CPhysXManager::EPhysxShape::eTriangleMesh, "triangle");
	Enum_String_Id(CPhysXManager::EPhysxShape::ePlane, "plane");
}
End_Enum_String;

Begin_Enum_String(CPhysXManager::PhysxGroups)
{
	Enum_String_Id(CPhysXManager::PhysxGroups::Default, "default");
	Enum_String_Id(CPhysXManager::PhysxGroups::Event, "event");
	Enum_String_Id(CPhysXManager::PhysxGroups::Ground, "ground");
	Enum_String_Id(CPhysXManager::PhysxGroups::Wall, "wall");
	Enum_String_Id(CPhysXManager::PhysxGroups::Enemy, "enemy");
}
End_Enum_String;


#endif