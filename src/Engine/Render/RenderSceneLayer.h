#ifndef _ENGINE_RENDER_CENDRENDERSCENELAYER_H
#define _ENGINE_RENDER_CENDRENDERSCENELAYER_H
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CRenderSceneLayer : public CRenderCmd
		{
		public:
			CRenderSceneLayer();
			virtual ~CRenderSceneLayer();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
			//void SetActive(bool active){ m_Active = active; }
		private:
			DISALLOW_COPY_AND_ASSIGN(CRenderSceneLayer);
			std::string m_LayerName;
		};
	}
}

#endif