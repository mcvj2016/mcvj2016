#include "UnSetRenderTarget.h"

engine::render::CUnSetRenderTarget::CUnSetRenderTarget()
{
}

engine::render::CUnSetRenderTarget::~CUnSetRenderTarget()
{
}

bool engine::render::CUnSetRenderTarget::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void engine::render::CUnSetRenderTarget::Execute(CRenderManager& lRM)
{
	lRM.UnsetRenderTargets();
}
