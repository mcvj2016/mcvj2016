#ifndef _ENGINE_RENDER_CSETRASTERIZEDSTATE_H
#define _ENGINE_RENDER_CSETRASTERIZERSTATE_H
#include "SetAlphaBlendState.h"

namespace engine
{
	namespace render
	{
		class CSetRasterizerState : public CRenderCmd
		{
		public:
			enum EFillMode
			{
				eSolid = 0,
				eWireFrame = 1
			};
		public:
			CSetRasterizerState();
			virtual ~CSetRasterizerState();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
			void ChangeState();
			virtual void DrawIMGUI();
		private:
			DISALLOW_COPY_AND_ASSIGN(CSetRasterizerState);
			ID3D11RasterizerState* m_RasterizerState;
			int m_CullMode;
			bool m_ClockWise;
			int m_FillMode;
			bool CreateRasterizerState();
		};
	}
}

#endif