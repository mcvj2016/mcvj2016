#ifndef _ENGINE_RENDERPIPELINEMANAGER_04052017012951_H
#define _ENGINE_RENDERPIPELINEMANAGER_04052017012951_H
#include "RenderPipeline.h"
#include "Utils/TemplatedMap.h"


namespace engine
{
	namespace render
	{
		class CRenderPipeline;

		class CRenderPipelineManager : public base::utils::CTemplatedMap<CRenderPipeline>
		{
		public:
			CRenderPipelineManager();
			virtual ~CRenderPipelineManager();
			bool HotLoad(const std::string& name);
			bool Load();
			void SetActivePipeline(const std::string& aName);
			void ChangeActivePipeline();
			CRenderPipeline& GetActivePipeline();
			bool m_ChangingPipeline;
		private:
			std::string m_Path;
			std::string m_Filename;
			std::string m_PipelinesPath;
			CRenderPipeline* m_ActivePipeline;
			std::string m_NextPipelineToLoad;
		};
	}
}


#endif	// _ENGINE_RENDERPIPELINEMANAGER_04052017012951_H