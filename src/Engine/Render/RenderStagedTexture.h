#ifndef _ENGINE_RENDER_CENDRENDERSTAGEDTEXTURE_H
#define _ENGINE_RENDER_CENDRENDERSTAGEDTEXTURE_H
#include "Math/Vector2.h"
#include "RenderManager.h"
#include <vector>
#include "DynamicTexture.h"
#include "RenderCmd.h"
#include "Utils/CheckedDelete.h"

namespace engine
{
	namespace render
	{
		class CRenderStagedTexture : public CRenderCmd
		{
		public:
			CRenderStagedTexture();
			virtual ~CRenderStagedTexture();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM) = 0;
			void CreateRenderTargetViewVector();
			void ActivateTextures();
		private:
			DISALLOW_COPY_AND_ASSIGN(CRenderStagedTexture);
		protected:
			class CStagedTexture
			{
			public:
				uint32 m_StageId;
				materials::CTexture * m_Texture;
				CStagedTexture(uint32 aStageId, materials::CTexture* aTexture);
				virtual ~CStagedTexture();
				void Activate();
			};
			class CDynamicTextureMaterial
			{
			public:
				CDynamicTexture *m_DynamicTexture;
				materials::CMaterial *m_Material;
				~CDynamicTextureMaterial()
				{
				};
				CDynamicTextureMaterial(CDynamicTexture *DynamicTexture, materials::CMaterial *Material)
				{
					m_DynamicTexture = DynamicTexture;
					m_Material = Material;
				}
				void Activate(int StageId);
			};
			std::vector<CStagedTexture*> m_StagedTextures;
			std::vector<CDynamicTextureMaterial*> m_DynamicTexturesMaterials;
			std::vector<ID3D11RenderTargetView *> m_RenderTargetViews;
			Vect2u m_ViewportPosition;
			Vect2u m_ViewportSize;
		};
	}
}

#endif