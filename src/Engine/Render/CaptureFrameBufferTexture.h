#ifndef CFB_TEXTURE_H
#define CFB_TEXTURE_H

#include "Graphics/Textures/Texture.h"

XML_FORWARD_DECLARATION

namespace engine
{
	namespace render
	{
		class CCaptureFrameBufferTexture : public materials::CTexture
		{
		protected:
			ID3D11Texture2D     *m_DataTexture;
			void Init();
			bool CreateSamplerState();
		public:
			CCaptureFrameBufferTexture();
			CCaptureFrameBufferTexture(const CXMLElement*TreeNode);
			~CCaptureFrameBufferTexture();
			bool Capture(unsigned int StageId);
		};
	}
}




#endif //TEXTURE_H