#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CApplyTechniquePool : public engine::render::CRenderCmd
		{
		public:
			CApplyTechniquePool();
			virtual ~CApplyTechniquePool();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(engine::render::CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CApplyTechniquePool);
			std::string m_PoolName;
		};
	}
}

