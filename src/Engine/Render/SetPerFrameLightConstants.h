#ifndef _ENGINE_RENDER_SETPERFRAMELIGHTCONSTANTS_H
#define _ENGINE_RENDER_SETPERFRAMELIGHTCONSTANTS_H
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CSetPerFrameLightConstants : public CRenderCmd
		{
		public:
			CSetPerFrameLightConstants();
			virtual ~CSetPerFrameLightConstants();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		};
	}
}

#endif