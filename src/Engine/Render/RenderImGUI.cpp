#include "RenderImGUI.h"
#include "XML/XML.h"
#include "Engine.h"
//#include "ScriptManager.h"
#include "Utils/Logger/Logger.h"
#include "Helper/ImguiHelper.h"


engine::render::CRenderImGUI::CRenderImGUI()
{
}

engine::render::CRenderImGUI::~CRenderImGUI()
{
}

bool engine::render::CRenderImGUI::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void engine::render::CRenderImGUI::Execute(CRenderManager& lRM)
{
	//try
	//{
#if defined(DEBUG) || defined(RELEASE)
	if (CEngine::GetInstance().m_ShowDebug)
	{
		CEngine::GetInstance().GetImGUI().DrawDebug();
	}
#endif
	//draw all components gui (if they have)
	//CEngine::GetInstance().GetSceneManager().GetCurrentScene()->UpdateNodesComponentsOnGUI();
	//CEngine::GetInstance().GetScriptComponentManager().GuiUpdate();
	CEngine::GetInstance().GetImGUI().ImGuiRender();

	//}
	//catch (luabind::error& lError)
	//{
	//	LOG_ERROR_APPLICATION("Error from luabind %s", lError.what());
	//}
}
