#include "CaptureFrameBuffer.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Textures/TextureManager.h"

engine::render::CCaptureFrameBuffer::CCaptureFrameBuffer()
	: m_CapturedFrameBufferTexture(nullptr)
{
}

engine::render::CCaptureFrameBuffer::~CCaptureFrameBuffer()
{
//	base::utils::CheckedDelete(m_CapturedFrameBufferTexture);
}

bool engine::render::CCaptureFrameBuffer::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	/*
	 * <capture_frame_buffer name="capture_frame_buffer"
texture_width_as_frame_buffer="true"/>

	 */

	m_CapturedFrameBufferTexture = new CCaptureFrameBufferTexture(aElement);
	m_StagedTextures.push_back(new CStagedTexture(0, m_CapturedFrameBufferTexture));
	assert(m_CapturedFrameBufferTexture);
	CEngine::GetInstance().GetTextureManager().AddTexture(m_CapturedFrameBufferTexture);
	
	return true;
}

void engine::render::CCaptureFrameBuffer::Execute(engine::render::CRenderManager& lRM)
{
	m_CapturedFrameBufferTexture->Capture(0);
}
