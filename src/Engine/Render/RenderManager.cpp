#include "RenderManager.h"
#include <d3dcompiler.h>
#include <cassert>

namespace engine
{
	namespace render
	{
		CRenderManager::CRenderManager()
		: 
		m_BackgroundColor(1.0f, 0, 1.0f, 0),
		m_ModelMatrix(m44fIDENTITY),
		m_ViewMatrix(m44fZERO),
		m_ProjectionMatrix(m44fZERO),
		m_ViewProjectionMatrix(m44fZERO),
		m_ModelViewProjectionMatrix(m44fZERO), 
		m_Device(nullptr),
		m_DeviceContext(nullptr),
		m_SwapChain(nullptr),
		m_ID3D11Debug(nullptr),
		m_RenderTargetView(nullptr),
		m_NumViews(0),
		m_CurrentDepthStencilView(nullptr),
		m_DepthStencil(nullptr),
		m_DepthStencilView(nullptr),
		m_DebugVertexBuffer(nullptr),
		m_DebugVertexBufferCurrentIndex(0),
		m_DebugVertexShader(nullptr),
		m_DebugVertexLayout(nullptr),
		m_DebugPixelShader(nullptr),
		m_AxisRenderableVertexs(nullptr),
		m_GridRenderableVertexs(nullptr),
		m_CubeRenderableVertexs(nullptr), 
		m_NumVerticesAxis(0), 
		m_NumVerticesGrid(0), 
		m_NumVerticesCube(0), 
		m_NumVerticesSphere(0),
		m_iWidth(800), 
		m_iHeight(600)
		{ }

		CRenderManager::~CRenderManager()
		{ }

		bool CRenderManager::InitDevice_SwapChain_DeviceContext(HWND &hWnd)
		{
			D3D_FEATURE_LEVEL featureLevels[] = {
				D3D_FEATURE_LEVEL_11_0,
				D3D_FEATURE_LEVEL_10_1,
				D3D_FEATURE_LEVEL_10_0
			};

			UINT numFeatureLevels = ARRAYSIZE(featureLevels);

			DXGI_SWAP_CHAIN_DESC sd;
			ZeroMemory(&sd, sizeof(sd));
			sd.BufferCount = 1;
			sd.BufferDesc.Width = this->m_iWidth;
			sd.BufferDesc.Height = this->m_iHeight;
			sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			sd.BufferDesc.RefreshRate.Numerator = 60;
			sd.BufferDesc.RefreshRate.Denominator = 1;
			sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			sd.OutputWindow = hWnd;
			sd.SampleDesc.Count = 1;
			sd.SampleDesc.Quality = 0;
			sd.Windowed = true;
			sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			unsigned int creationFlags = 0;
#ifdef _DEBUG
			creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
			ID3D11Device* l_Device;
			ID3D11DeviceContext* l_DeviceContext;
			IDXGISwapChain* l_SwapChain;
			if (FAILED(D3D11CreateDeviceAndSwapChain(
				nullptr,
				D3D_DRIVER_TYPE_HARDWARE,
				nullptr,
				0, //TODO al poner los flags no crea la hWnd en videogame??
				featureLevels,
				numFeatureLevels,
				D3D11_SDK_VERSION,
				&sd,
				&l_SwapChain,
				&l_Device,
				nullptr,
				&l_DeviceContext)))
			{
				return false;
			}

			m_Device.reset(l_Device);
			m_DeviceContext.reset(l_DeviceContext);
			m_SwapChain.reset(l_SwapChain);

			 
			HRESULT hr;
#if _DEBUG
			//ID3D11Debug *l_ID3D11Debug;
			hr = m_Device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&m_ID3D11Debug));
			//m_ID3D11Debug.reset(l_ID3D11Debug);
			if (FAILED(hr))
			{
				return hr != 0;
			}
#endif

			// treure el ALT+INTRO autom�tic
			IDXGIFactory* dxgiFactory;
			hr = m_SwapChain->GetParent(__uuidof(IDXGIFactory), (void **)&dxgiFactory);
			assert(hr == S_OK);

			hr = dxgiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);
			assert(hr == S_OK);

			dxgiFactory->Release();

			return S_OK;
		}

		bool CRenderManager::Get_RendertargetView()
		{
			ID3D11Texture2D *pBackBuffer;
			if (FAILED(m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
			{
				return false;
			}
			ID3D11RenderTargetView* l_RenderTargetView;
			HRESULT hr = m_Device->CreateRenderTargetView(pBackBuffer, nullptr, &l_RenderTargetView);
			m_RenderTargetView.reset(l_RenderTargetView);
			pBackBuffer->Release();
			if (FAILED(hr))
			{
				return false;
			}

			return true;
		}

		void CRenderManager::CreateBackBuffers(int width, int height, HWND& hWnd)
		{
			m_iHeight = height;
			m_iWidth = width;

			//activamos RenderTarget
			ID3D11RenderTargetView* l_RenderTargetView = m_RenderTargetView.get();

			//InitDevice_SwapChain_DeviceContext
			this->InitDevice_SwapChain_DeviceContext(hWnd);

			//Get_RendertargetView
			this->Get_RendertargetView();

			//textura de profundidad
			this->Create_DepthStencil();

			
			m_DeviceContext->OMSetRenderTargets(1, &l_RenderTargetView, m_DepthStencilView.get());
			//m_RenderTargetView.reset(l_RenderTargetView);

			//Set del viwport
			this->SetViewport();
		}

		void CRenderManager::Resize(int Width, int Height, HWND& hWnd)
		{
			if (this != nullptr && m_Device.get() != nullptr)
			{
				m_DeviceContext->OMSetRenderTargets(0, nullptr, nullptr);

				m_RenderTargetView.reset(nullptr);
				m_DepthStencil.reset(nullptr);
				m_DepthStencilView.reset(nullptr);

				m_SwapChain->ResizeBuffers(0, Width, Height, DXGI_FORMAT_UNKNOWN, 0);
				CreateBackBuffers(Width, Height, hWnd); // where we initialize m_RenderTargetView, m_DepthStencil, m_DepthStencilView
			}
		}

		void CRenderManager::UnsetRenderTargets()
		{
			ID3D11RenderTargetView * lTargetView = m_RenderTargetView.get();

			SetRenderTargets(1, nullptr, m_DepthStencilView.get());

			ResetViewport();
		}
		
		void CRenderManager::SetRenderTargets(int aNumViews, ID3D11RenderTargetView * const*aRenderTargetViews, ID3D11DepthStencilView *aDepthStencilViews)
		{
			m_NumViews = aNumViews;

			m_CurrentDepthStencilView = aDepthStencilViews != nullptr ? aDepthStencilViews : m_DepthStencilView.get();

			assert(m_NumViews <= (int)MAX_RENDER_TARGETS);
			if (aRenderTargetViews == nullptr)
			{
				ID3D11RenderTargetView * lTargetView = m_RenderTargetView.get();
				m_CurrentRenderTargetViews[0] = lTargetView;
			}
			else
			{
				for (int i = 0; i < MAX_RENDER_TARGETS; ++i)
				{
					if (i < m_NumViews){
						m_CurrentRenderTargetViews[i] = aRenderTargetViews[i];
					}
					else
					{
						m_CurrentRenderTargetViews[i] = NULL;
					}
				}
			}
			m_DeviceContext->OMSetRenderTargets(m_NumViews, &m_CurrentRenderTargetViews[0], m_CurrentDepthStencilView);
		}

		IDXGISwapChain* CRenderManager::GetSwapChain()
		{
			return m_SwapChain.get();
		}

		bool CRenderManager::Create_DepthStencil()
		{
			D3D11_TEXTURE2D_DESC descDepth;
			RtlSecureZeroMemory(&descDepth, sizeof(descDepth));
			descDepth.Width = this->m_iWidth;
			descDepth.Height = this->m_iHeight;
			descDepth.MipLevels = 1;
			descDepth.ArraySize = 1;
			descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			descDepth.SampleDesc.Count = 1;
			descDepth.SampleDesc.Quality = 0;
			descDepth.Usage = D3D11_USAGE_DEFAULT;
			descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			descDepth.CPUAccessFlags = 0;
			descDepth.MiscFlags = 0;

			ID3D11Texture2D* l_DepthStencil;
			HRESULT hr = m_Device->CreateTexture2D(&descDepth, nullptr, &l_DepthStencil);
			m_DepthStencil.reset(l_DepthStencil);
			if (FAILED(hr))
			{
				return false;
			}

			//crear vista asociada
			D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
			ZeroMemory(&descDSV, sizeof(descDSV));
			descDSV.Format = descDepth.Format;
			descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			descDSV.Texture2D.MipSlice = 0;
			ID3D11DepthStencilView* l_DepthStencilView;
			hr = m_Device->CreateDepthStencilView(l_DepthStencil, &descDSV, &l_DepthStencilView);
			m_DepthStencilView.reset(l_DepthStencilView);
			if (FAILED(hr))
			{
				return false;
			}

			return true;
		}

		void CRenderManager::SetViewport()
		{
			m_Viewport.Width = static_cast<float>(this->m_iWidth);
			m_Viewport.Height = static_cast<float>(this->m_iHeight);
			m_Viewport.MinDepth = 0.0f;
			m_Viewport.MaxDepth = 1.0f;
			m_Viewport.TopLeftX = 0;
			m_Viewport.TopLeftY = 0;
			m_DeviceContext->RSSetViewports(1, &m_Viewport);
		}

		void CRenderManager::SetViewport(const Vect2u & aPosition, const Vect2u & aSize)
		{
			D3D11_VIEWPORT l_Viewport;
			l_Viewport.Width = static_cast<float>(aSize.x);
			l_Viewport.Height = static_cast<float>(aSize.y);
			l_Viewport.MinDepth = 0.0f;
			l_Viewport.MaxDepth = 1.0f;
			l_Viewport.TopLeftX = static_cast<float>(aPosition.x);
			l_Viewport.TopLeftY = static_cast<float>(aPosition.y);
			m_DeviceContext->RSSetViewports(1, &l_Viewport);
		}
		
		void CRenderManager::ResetViewport()
		{
			m_DeviceContext->RSSetViewports(1, &m_Viewport);
		}

		bool CRenderManager::Init(HWND& hWnd, int Width, int Height)
		{
			m_Hwnd = hWnd;

			bool result = false;

			CreateBackBuffers(Width, Height, hWnd);

			return result;
		}

		void CRenderManager::Clear(bool renderTarget, bool depthStencil, const CColor& backgroundColor)
		{
			if (renderTarget)
			{
				for (int i = 0; i < m_NumViews; ++i)
				{
					GetDeviceContext()->ClearRenderTargetView(m_CurrentRenderTargetViews[i], &backgroundColor.x);
				}
			}
			if (depthStencil)
			{
				GetDeviceContext()->ClearDepthStencilView(m_CurrentDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
			}
		}

		void CRenderManager::Destroy()
		{
			if (m_ID3D11Debug){
				m_ID3D11Debug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
			}
		}

		void CRenderManager::BeginRender()
		{
			ResetViewport();
		}

		void CRenderManager::EndRender()
		{ 
#ifdef _DEBUG //VSYNC disabled in debug mode
			//m_SwapChain->Present(0, 0);
			m_SwapChain->Present(1, 0);
#else
			m_SwapChain->Present(1, 0);
#endif
		}

		bool CRenderManager::Create_DebugShader()
		{
			// C++ macros are nuts
#define STRINGIFY(X) #X
#define TOSTRING(X) STRINGIFY(X)

			//codigo del shader
			const char debugRenderEffectCode[] =
				"#line " TOSTRING(__LINE__) "\n"
				"struct VS_OUTPUT\n"
				"{\n"
				"	float4 Pos : SV_POSITION;\n"
				"	float4 Color : COLOR0;\n"
				"};\n"
				"\n"
				"VS_OUTPUT VS(float4 Pos : POSITION, float4 Color : COLOR)\n"
				"{\n"
				"	VS_OUTPUT l_Output = (VS_OUTPUT)0;\n"
				"	l_Output.Pos = Pos;\n"
				"	l_Output.Color = Color;\n"
				"	return l_Output;\n"
				"}\n"
				"\n"
				"float4 PS(VS_OUTPUT IN) : SV_Target\n"
				"{\n"
				"	//return float4(1,0,0,1);\n"
				"	//return m_BaseColor;\n"
				"	return IN.Color;\n"
				"}\n";

			ID3DBlob *vsBlob, *psBlob;
			ID3DBlob *errorBlob;

			//compilamos el vertex shader
			HRESULT hr = D3DCompile(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
				"VS", "vs_4_0", 0, 0, &vsBlob, &errorBlob);

			if (FAILED(hr))
			{
				if (errorBlob != nullptr)
					OutputDebugStringA(static_cast<char*>(errorBlob->GetBufferPointer()));
				if (errorBlob)
					errorBlob->Release();
				return false;
			}

			//compilaom el pixel shader
			hr = D3DCompile(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
				"PS", "ps_4_0", 0, 0, &psBlob, &errorBlob);

			if (FAILED(hr))
			{
				if (errorBlob != nullptr)
					OutputDebugStringA(static_cast<char*>(errorBlob->GetBufferPointer()));
				if (errorBlob)
					errorBlob->Release();
				return false;
			}

			//los convertimos en objetos directx
			ID3D11VertexShader* l_DebugVertexShader;
			hr = m_Device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, &l_DebugVertexShader);
			m_DebugVertexShader.reset(l_DebugVertexShader);
			if (FAILED(hr))
			{
				return false;
			}

			ID3D11PixelShader* l_DebugPixelShader;
			hr = m_Device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, &l_DebugPixelShader);
			m_DebugPixelShader.reset(l_DebugPixelShader);
			if (FAILED(hr))
			{
				return false;
			}

			//creamos un objeto layout para el VS antes creado
			D3D11_INPUT_ELEMENT_DESC layout[] = {
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }
			};
			ID3D11InputLayout* l_DebugVertexLayout;
			hr = m_Device->CreateInputLayout(layout, 2, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &l_DebugVertexLayout);
			m_DebugVertexLayout.reset(l_DebugVertexLayout);
			if (FAILED(hr))
			{
				return false;
			}

			D3D11_BUFFER_DESC l_BufferDescription;
			ZeroMemory(&l_BufferDescription, sizeof(l_BufferDescription));
			l_BufferDescription.Usage = D3D11_USAGE_DYNAMIC;
			l_BufferDescription.ByteWidth = sizeof(CDebugVertex)*DebugVertexBufferSize;
			l_BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			l_BufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			//D3D11_SUBRESOURCE_DATA InitData;
			//ZeroMemory(&InitData, sizeof(InitData));
			//InitData.pSysMem = Vtxs;
			ID3D11Buffer* l_DebugVertexBuffer;
			hr = m_Device->CreateBuffer(&l_BufferDescription, nullptr, &l_DebugVertexBuffer);
			m_DebugVertexBuffer.reset(l_DebugVertexBuffer);
			if (FAILED(hr))
			{
				return false;
			}

			return true;

		}

		//MemLeaks OK
		void CRenderManager::Draw_Triangle()
		{

			//datos para pintar el triangulo
			CDebugVertex resultBuffer[] = {
				{ Vect4f(+0.0f, +0.5f, 0.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) },
				{ Vect4f(+0.5f, -0.5f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },
				{ Vect4f(-0.5f, -0.5f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
			};

			//los pasamos a la gpu
			// set vertex data

			D3D11_MAPPED_SUBRESOURCE resource;
			HRESULT hr = m_DeviceContext->Map(m_DebugVertexBuffer.get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

			if (FAILED(hr))
			{
				return;
			} // TODO log

			assert(3 * sizeof(CDebugVertex) < DebugVertexBufferSize * sizeof(CDebugVertex));
			memcpy(resource.pData, resultBuffer, 3 * sizeof(CDebugVertex));

			m_DeviceContext->Unmap(m_DebugVertexBuffer.get(), 0);

			//preparamos estado necesario para pintar
			UINT stride = sizeof(CDebugVertex);
			UINT offset = 0;
			ID3D11Buffer* l_DebugVertexBuffer;
			m_DeviceContext->IASetVertexBuffers(0, 1, &l_DebugVertexBuffer, &stride, &offset);
			m_DebugVertexBuffer.reset(l_DebugVertexBuffer);
			m_DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			m_DeviceContext->IASetInputLayout(m_DebugVertexLayout.get());
			m_DeviceContext->VSSetShader(m_DebugVertexShader.get(), nullptr, 0);
			m_DeviceContext->PSSetShader(m_DebugPixelShader.get(), nullptr, 0);

			m_DeviceContext->Draw(3, 0);
		}

		ID3D11Device* CRenderManager::GetDevice()
		{
			return m_Device.get();
		}

		ID3D11DeviceContext* CRenderManager::GetDeviceContext()
		{
			return m_DeviceContext.get();
		}

		void CRenderManager::CreateDebugObjects()
		{
			//AXIS
			static CDebugVertex l_AxisVtxs[6] =
			{
				{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
				{ Vect4f(1.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 1.0f, 0.0f, 1.0f) },

				{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },
				{ Vect4f(0.0f, 1.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },

				{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) },
				{ Vect4f(0.0f, 0.0f, 1.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) }
			};

			m_AxisRenderableVertexs = l_AxisVtxs;
			m_NumVerticesAxis = 6;
			//CUBE
			static CDebugVertex l_CubeVtxs[] =
			{
				{ Vect4f(+1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(+1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(+1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(+1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(+1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, 1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, -1.0f, +1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(-1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(-1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },

				{ Vect4f(+1.0f, 1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
				{ Vect4f(+1.0f, -1.0f, -1.0f, 1.0f), CColor(0, 0, 0, 0) },
			};

			m_CubeRenderableVertexs = l_CubeVtxs;
			m_NumVerticesCube = 8 * 3;

			//GRID
			float l_Size = 10.0f;
			const int l_Grid = 10;
			static CDebugVertex l_GridVtxs[(l_Grid + 1) * 2 * 2]; //44 vertices...
			//LINEAS EN Y
			for (int b = 0; b <= l_Grid; ++b)
			{
				float y = b * 2.0f * l_Size / l_Grid - l_Size;
				l_GridVtxs[b * 2].Position = Vect4f(-l_Size, 0, y, 1);
				l_GridVtxs[b * 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
				l_GridVtxs[(b * 2) + 1].Position = Vect4f(l_Size, 0, y, 1);
				l_GridVtxs[(b * 2) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			}
			//LINEAS EN X
			for (int b = 0; b <= l_Grid; ++b)
			{
				float y = b * 2.0f * l_Size / l_Grid - l_Size;
				l_GridVtxs[(l_Grid + 1) * 2 + (b * 2)].Position = Vect4f(y, 0, -l_Size, 1); // DONE
				l_GridVtxs[(l_Grid + 1) * 2 + (b * 2)].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
				l_GridVtxs[(l_Grid + 1) * 2 + (b * 2) + 1].Position = Vect4f(y, 0, l_Size, 1); // DONE
				l_GridVtxs[(l_Grid + 1) * 2 + (b * 2) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			}

			m_GridRenderableVertexs = l_GridVtxs;
			m_NumVerticesGrid = (l_Grid + 1) * 2 * 2;

			//SPHERE
			//const int l_Aristas = 10;
			//static CDebugVertex l_SphereVtxs[4 * l_Aristas*l_Aristas];
			//for (int t = 0; t < l_Aristas; ++t)
			//{
			//	float l_RadiusRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) * (180.0f*((float)t)) / ((float)l_Aristas));
			//	for (int b = 0; b < l_Aristas; ++b)
			//	{
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);

			//		float l_RadiusNextRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas));

			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Position = Vect4f(l_RadiusNextRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas)), l_RadiusNextRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			//		l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			//	}
			//}

			//m_SphereRenderableVertexs = l_SphereVtxs;
			//m_NumVerticesSphere = 4 * l_Aristas*l_Aristas;

		}

		void CRenderManager::DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint)
		{

			CDebugVertex *resultBuffer = static_cast<CDebugVertex *>(alloca(numVertices * sizeof(CDebugVertex)));

			for (int i = 0; i < numVertices; ++i)
			{
				resultBuffer[i].Position = (modelVertices[i].Position * modelViewProj);

				resultBuffer[i].Color = modelVertices[i].Color * colorTint;
			}

			// set vertex data
			D3D11_MAPPED_SUBRESOURCE resource;
			HRESULT hr = m_DeviceContext->Map(m_DebugVertexBuffer.get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

			if (FAILED(hr))
				return; // TODO log

			assert(numVertices * sizeof(CDebugVertex) < DebugVertexBufferSize * sizeof(CDebugVertex));
			memcpy(resource.pData, resultBuffer, numVertices * sizeof(CDebugVertex));

			m_DeviceContext->Unmap(m_DebugVertexBuffer.get(), 0);


			UINT stride = sizeof(CDebugVertex);
			UINT offset = 0;
			ID3D11Buffer* l_DebugVertexBuffer = m_DebugVertexBuffer.get();
			m_DeviceContext->IASetVertexBuffers(0, 1, &l_DebugVertexBuffer, &stride, &offset);
			m_DebugVertexBuffer.reset(l_DebugVertexBuffer);
			m_DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
			m_DeviceContext->IASetInputLayout(m_DebugVertexLayout.get());
			m_DeviceContext->VSSetShader(m_DebugVertexShader.get(), nullptr, 0);
			m_DeviceContext->PSSetShader(m_DebugPixelShader.get(), nullptr, 0);

			m_DeviceContext->Draw(numVertices, 0);

		}

		const Mat44f CRenderManager::GetViewProjectionMatrix() const
		{
			return m_ViewProjectionMatrix;
		}

		void CRenderManager::DrawAxis(float SizeX, float SizeY, float SizeZ, const CColor &Color)
		{
			Mat44f scale;
			scale.SetIdentity();
			scale.SetScale(SizeX, SizeY, SizeZ);

			Mat44f viewProj = scale * m_ModelViewProjectionMatrix;

			DebugRender(viewProj, m_AxisRenderableVertexs, m_NumVerticesAxis, Color);
		}

		void CRenderManager::DrawCube(float SizeX, float SizeY, float SizeZ, const CColor& Color)
		{
			Mat44f scale;
			scale.SetIdentity();
			scale.SetScale(SizeX, SizeY, SizeZ);

			Mat44f viewProj = scale * m_ModelViewProjectionMatrix;

			DebugRender(viewProj, m_CubeRenderableVertexs, m_NumVerticesCube, Color);
		}

		void CRenderManager::DrawSphere(float Radius, Vect3f position, const CColor& Color)
		{

			//SPHERE
			const int l_Aristas = 10;
			static CDebugVertex l_SphereVtxs[4 * l_Aristas*l_Aristas];
			for (int t = 0; t < l_Aristas; ++t)
			{
				float l_RadiusRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) * (180.0f*static_cast<float>(t)) / static_cast<float>(l_Aristas));
				for (int b = 0; b < l_Aristas; ++b)
				{
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Position = Vect4f(
						l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b)) / static_cast<float>(l_Aristas))) + position.x,
						cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*(static_cast<float>(t)) / (static_cast<float>(l_Aristas)) + position.y,
						l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f*static_cast<float>(b) / (static_cast<float>(l_Aristas))) + position.z,
						1.0f
						)))));
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Position = Vect4f(
						l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b + 1)) / static_cast<float>(l_Aristas))) + position.x,
						cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*static_cast<float>(t)) / static_cast<float>(l_Aristas)) + position.y,
						l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b + 1)) / static_cast<float>(l_Aristas))) + position.z,
						1.0f
						);
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);

					float l_RadiusNextRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*static_cast<float>(t + 1)) / static_cast<float>(l_Aristas));

					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Position = Vect4f(
						l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b)) / static_cast<float>(l_Aristas))) + position.x,
						cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*static_cast<float>(t)) / static_cast<float>(l_Aristas)) + position.y,
						l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b)) / static_cast<float>(l_Aristas))) + position.z,
						1.0f
						);
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Position = Vect4f(
						l_RadiusNextRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b)) / static_cast<float>(l_Aristas))) + position.x,
						cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*static_cast<float>(t + 1)) / static_cast<float>(l_Aristas)) + position.y,
						l_RadiusNextRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *(static_cast<float>(360.0f * static_cast<float>(b)) / static_cast<float>(l_Aristas))) + position.z,
						1.0f
						);
					l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
				}
			}

			Mat44f scale;
			scale.SetIdentity();
			scale.SetScale(Radius, Radius, Radius);

			Mat44f viewProj = scale * m_ModelViewProjectionMatrix;

			DebugRender(viewProj, l_SphereVtxs, 4 * l_Aristas*l_Aristas, Color);
		}

		void CRenderManager::DrawGrid(float SizeX, float SizeY, float SizeZ, const CColor& Color)
		{
			Mat44f scale;
			scale.SetIdentity();
			scale.SetScale(SizeX, SizeY, SizeZ);

			Mat44f viewProj = scale * m_ModelViewProjectionMatrix;

			DebugRender(viewProj, m_GridRenderableVertexs, m_NumVerticesGrid, Color);
		}

		Vect2f CRenderManager::GetPositionInScreenCoordinates(const Vect3f &Position) const
		{
			Mat44f l_ViewProj = m_ViewMatrix*m_ProjectionMatrix;
			Vect4f l_Pos4f(Position.x, Position.y, Position.z, 1.0);
			float x = l_Pos4f.x*l_ViewProj.m00 + l_Pos4f.y*l_ViewProj.m10 + l_Pos4f.z*l_ViewProj.m20 + l_ViewProj.m30;
			float y = l_Pos4f.x*l_ViewProj.m01 + l_Pos4f.y*l_ViewProj.m11 + l_Pos4f.z*l_ViewProj.m21 + l_ViewProj.m31;
			float z = l_Pos4f.x*l_ViewProj.m02 + l_Pos4f.y*l_ViewProj.m12 + l_Pos4f.z*l_ViewProj.m22 + l_ViewProj.m32;
			float w = l_Pos4f.x*l_ViewProj.m03 + l_Pos4f.y*l_ViewProj.m13 + l_Pos4f.z*l_ViewProj.m23 + l_ViewProj.m33;
			if (w == 0){
				w = 1;
			}
			return Vect2f(x / w, y / w);
		}

	}
}
