#ifndef _ENGINE_RENDER_CRENDERCMD_H
#define _ENGINE_RENDER_CRENDERCMD_H

#include "Utils/Defines.h"
#include "Utils/Name.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine {namespace render {
	class CRenderManager;
}
}

namespace engine
{
	namespace render
	{
		class CRenderCmd : public CName
		{
		public:
			CRenderCmd();
			virtual ~CRenderCmd();
			virtual bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM) = 0;
			virtual void SetActive(bool Active);
			bool IsActive() const { return m_Active; };
		private:
			DISALLOW_COPY_AND_ASSIGN(CRenderCmd);
			
		protected:
			bool m_Active;
		};
	}
}
#endif
