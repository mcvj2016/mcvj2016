#include "EndRenderCmd.h"
#include "XML/XML.h"
#include "RenderManager.h"
#include "Engine.h"

engine::render::CEndRenderCmd::CEndRenderCmd()
{
}

engine::render::CEndRenderCmd::~CEndRenderCmd()
{
}

bool engine::render::CEndRenderCmd::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void engine::render::CEndRenderCmd::Execute(CRenderManager& lRM)
{
	lRM.EndRender();
}
