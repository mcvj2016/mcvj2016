#ifndef _ENGINE_RENDER_CSETDEPTHSTENCILSTATECMD_H
#define _ENGINE_RENDER_CSETDEPTHSTENCILSTATECMD_H
#include <d3d11.h>
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CSetDepthStencilStateCmd : public CRenderCmd
		{
		public:
			CSetDepthStencilStateCmd();
			virtual ~CSetDepthStencilStateCmd();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CSetDepthStencilStateCmd);
			ID3D11DepthStencilState *m_DepthStencilState;
			bool m_EnableZTest;
			bool m_WriteZBuffer;
			bool m_EnableStencil;
			int m_ComparisonFunc;
			bool CreateDepthStencilState();
		};
	}
}

#endif