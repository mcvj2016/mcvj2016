#include "RenderPipeline.h"
#include "Utils/Defines.h"
#include "XML/XML.h"
#include "Engine.h"
#include "SetPerFrameConstantsCmd.h"
#include "ApplyTechniquePool.h"
#include "RenderSceneLayer.h"
#include "RenderImGUI.h"
#include "SetRasterizerState.h"
#include "BeginRenderCmd.h"
#include "EndRenderCmd.h"
#include "ClearCmd.h"
#include "SetDepthStencilStateCmd.h"
#include "DrawQuad.h"
#include "CaptureFrameBuffer.h"
#include "SetRenderTarget.h"
#include "UnSetRenderTarget.h"
#include "SetUpMatricesCmd.h"
#include "SetPerFrameLightConstants.h"
#include "DeferredShading.h"
#include "ApplyFilter.h"
#include "GenerateShadowmap.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Lights/LightManager.h"
#include "CheckReloads.h"
#include "RenderGUI.h"
#include <functional>

#define RENDER_CMD_ENTRY(tag, command_class_name) { tag, [] { return new command_class_name(); } },
std::map<std::string, std::function<engine::render::CRenderCmd*(void)>> sComandsFactory =
{
	RENDER_CMD_ENTRY("begin_render", engine::render::CBeginRenderCmd)
	RENDER_CMD_ENTRY("end_render", engine::render::CEndRenderCmd)
	RENDER_CMD_ENTRY("set_per_frame_constants", engine::render::CSetPerFrameConstantsCmd)
	RENDER_CMD_ENTRY("set_resterized_state", engine::render::CSetRasterizerState)
	RENDER_CMD_ENTRY("apply_technique_pool", engine::render::CApplyTechniquePool)
	RENDER_CMD_ENTRY("render_layer", engine::render::CRenderSceneLayer)
	RENDER_CMD_ENTRY("render_imgui", engine::render::CRenderImGUI)
	RENDER_CMD_ENTRY("clear", engine::render::CClearCmd)
	RENDER_CMD_ENTRY("set_depth_stencil_state", engine::render::CSetDepthStencilStateCmd)
	RENDER_CMD_ENTRY("draw_quad", engine::render::CDrawQuad)
	RENDER_CMD_ENTRY("set_alpha_blend", engine::render::CSetAlphaBlendState)
	RENDER_CMD_ENTRY("capture_frame_buffer", engine::render::CCaptureFrameBuffer)
	RENDER_CMD_ENTRY("set_render_target", engine::render::CSetRenderTarget)
	RENDER_CMD_ENTRY("unset_render_target", engine::render::CUnSetRenderTarget)
	RENDER_CMD_ENTRY("set_matrices", engine::render::CSetUpMatricesCmd)
	RENDER_CMD_ENTRY("set_lights_constants", engine::render::CSetPerFrameLightConstants)
	RENDER_CMD_ENTRY("apply_filter", engine::render::CApplyFilter)
	RENDER_CMD_ENTRY("deferred_shading", engine::render::CDeferredShading)
	RENDER_CMD_ENTRY("generate_shadow_map", engine::render::CGenerateShadowMaps)
	RENDER_CMD_ENTRY("check_reloads", engine::render::CCheckReloads)
	RENDER_CMD_ENTRY("render_gui", engine::render::CRenderGUI)
};

void engine::render::CRenderPipeline::SwapRasterizedState()
{
	CSetRasterizerState* resterCmd = static_cast<CSetRasterizerState*> (this->operator()("set_resterizer_state_cmd"));
	resterCmd->ChangeState();
}

bool engine::render::CRenderPipeline::Load()
{
	CXMLDocument xmldoc;
	bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));

	assert(loaded);

	CXMLElement* pipeline = xmldoc.FirstChildElement("render_pipeline");
	if (pipeline != nullptr)
	{
		tinyxml2::XMLNode* node = pipeline->FirstChild();

		while (node != nullptr)
		{
			CXMLElement* el = node->ToElement();
			if (el != nullptr)
			{
				CRenderCmd* lCommand = sComandsFactory[el->Name()]();
				lCommand->Load(el);
				short int cmdCount = this->GetCount();
				std::string cmdName = el->GetAttribute<std::string>("name", "") + "_" + std::to_string((cmdCount + 1));
				//if it has no name attr, generate an unique name
				if (cmdName == "")
				{
					cmdName = std::string(el->Name()) + "_cmd_" + std::to_string((cmdCount + 1));
				}

				Add(cmdName, lCommand);
			}

			node = node->NextSibling();
		}
	}
	return true;
}


void engine::render::CRenderPipeline::Execute()
{
	for (size_t i = 0, lCount = this->GetCount(); i < lCount; ++i)
	{
		if (this->operator[](i)->IsActive())
		{
			this->operator[](i)->Execute(CEngine::GetInstance().GetRenderManager());
		}
	}
}

void engine::render::CRenderPipeline::SwapCmdActive(std::string aName)
{
	this->operator()(aName)->SetActive(!this->operator()(aName)->IsActive());
}

void engine::render::CRenderPipeline::Reload()
{
	CEngine::GetInstance().GetSceneManager().Reload();
	Destroy();
	Load();
	m_Reloading = false;
}

engine::render::CRenderPipeline::CRenderPipeline(std::string aFilename, bool aEnabled, std::string aName)
	: CName(aName), m_Reloading(false), m_Filename(aFilename), m_Enabled(aEnabled)
{
}

engine::render::CRenderPipeline::~CRenderPipeline()
{
}