#include "CheckReloads.h"
#include "Engine.h"
//#include "ScriptManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "RenderPipelineManager.h"

namespace engine
{
	namespace render
	{
		CCheckReloads::CCheckReloads()
		{
		}

		CCheckReloads::~CCheckReloads()
		{
		}

		bool CCheckReloads::Load(const tinyxml2::XMLElement* aElement)
		{
			return true;
		}

		void CCheckReloads::Execute(CRenderManager& lRM)
		{
			if (CEngine::GetInstance().GetSceneManager().m_ChangingScene)
			{
				CEngine::GetInstance().GetSceneManager().PerformChangeScene();
			}
			if (CEngine::GetInstance().GetRenderPipelineManager().m_ChangingPipeline)
			{
				CEngine::GetInstance().GetRenderPipelineManager().ChangeActivePipeline();
			}
			if (CEngine::GetInstance().GetSceneManager().m_ChangingScene)
			{
				CEngine::GetInstance().GetSceneManager().GetCurrentScene()->LoadNodesComponents();
				CEngine::GetInstance().GetSceneManager().m_ChangingScene = false;
			}
			if (CEngine::GetInstance().GetRenderPipelineManager().GetActivePipeline().m_Reloading)
			{
				CEngine::GetInstance().GetRenderPipelineManager().GetActivePipeline().Reload();
			}
			if (CEngine::GetInstance().GetSceneManager().GetCurrentScene()->m_Reloading)
			{
				CEngine::GetInstance().GetSceneManager().Reload();
			}
		}
	}
}
