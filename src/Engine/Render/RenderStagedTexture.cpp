#include "RenderStagedTexture.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Textures/TextureManager.h"
#include "RenderManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include <functional>

engine::render::CRenderStagedTexture::CRenderStagedTexture()
	: m_ViewportPosition((unsigned int)0, (unsigned int)0),
	m_ViewportSize((unsigned int)0, (unsigned int)0)
{
}

engine::render::CRenderStagedTexture::~CRenderStagedTexture()
{
	base::utils::CheckedDelete(m_StagedTextures);
	//base::utils::CheckedDelete(m_DynamicTexturesMaterials);

	m_DynamicTexturesMaterials.clear();
	//m_RenderTargetViews.clear();
}

bool engine::render::CRenderStagedTexture::Load(const CXMLElement* aElement)
{
	bool loaded = false;
	CRenderCmd::Load(aElement);
	const CXMLElement* textureElement = aElement->FirstChildElement("texture");
	materials::CTextureManager &l_TexManager = CEngine::GetInstance().GetTextureManager();
	//carga de texturas NO dinamicas
	while (textureElement != nullptr)
	{
		std::string textName = textureElement->GetAttribute<std::string>("name", "");
		int stageId = textureElement->GetAttribute<int>("stage", 0);
		materials::CTexture* tex = l_TexManager.GetTexture(textName);
		CStagedTexture* lstagedTexture = new CStagedTexture(stageId, tex);
		m_StagedTextures.push_back(lstagedTexture);

		textureElement = textureElement->NextSiblingElement("texture");

		loaded = true;
	}

	//carga de texturas SI dinamicas
	const CXMLElement* dynTextureElement = aElement->FirstChildElement("dynamic_texture");
	while (dynTextureElement != nullptr)
	{
		std::string textName = dynTextureElement->GetAttribute<std::string>("name", "");
		assert(textName != "");
		CDynamicTexture* dynTex = new CDynamicTexture(dynTextureElement);
		std::string materialName = dynTextureElement->GetAttribute<std::string>("material", "");
		CDynamicTextureMaterial* dynTexMat;
		if (materialName==""){
			dynTexMat = new CDynamicTextureMaterial(dynTex, nullptr);
		} else{
			materials::CMaterial *l_Material = CEngine::GetInstance().GetMaterialManager().operator()(materialName);
			dynTexMat = new CDynamicTextureMaterial(dynTex, l_Material);
		}
		m_DynamicTexturesMaterials.push_back(dynTexMat);

		dynTextureElement = dynTextureElement->NextSiblingElement("dynamic_texture");

		loaded = true;
	}

	//carga de datos de viewport
	m_ViewportPosition = aElement->GetAttribute<Vect2u>("viewport_position", Vect2u(0, 0));
	float height = CEngine::GetInstance().GetRenderManager().m_Viewport.Height;
	float width = CEngine::GetInstance().GetRenderManager().m_Viewport.Width;
	m_ViewportSize = aElement->GetAttribute<Vect2u>("viewport_size", Vect2u((unsigned int)width, (unsigned int)height));

	CreateRenderTargetViewVector();

	return loaded;
}

void engine::render::CRenderStagedTexture::CreateRenderTargetViewVector()
{
	for (size_t i = 0, lCount = m_DynamicTexturesMaterials.size(); i < lCount; ++i)
	{
		m_RenderTargetViews.push_back(m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetRenderTargetView());
	}
}

void engine::render::CRenderStagedTexture::ActivateTextures()
{
	for each (CStagedTexture* tex in m_StagedTextures)
	{
		if (tex!=nullptr)
			tex->Activate();
	}
}

engine::render::CRenderStagedTexture::CStagedTexture::CStagedTexture(uint32 aStageId, materials::CTexture* aTexture)
	: m_StageId(aStageId),
	m_Texture(aTexture)
{
}

engine::render::CRenderStagedTexture::CStagedTexture::~CStagedTexture()
{
}

void engine::render::CRenderStagedTexture::CStagedTexture::Activate()
{
	if (m_Texture!=nullptr)
		m_Texture->Bind(m_StageId, CEngine::GetInstance().GetRenderManager().GetDeviceContext());
}
