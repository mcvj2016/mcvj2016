#include "SetRasterizerState.h"
#include "Engine.h"
#include "XML/XML.h"
#include "RenderManager.h"

engine::render::CSetRasterizerState::CSetRasterizerState()
	: m_ClockWise(true),
m_CullMode(0),
m_FillMode(0)
{
}

engine::render::CSetRasterizerState::~CSetRasterizerState()
{
}

bool engine::render::CSetRasterizerState::Load(const tinyxml2::XMLElement* aElement)
{
	m_FillMode = aElement->GetAttribute<int>("fill_mode", 2);
	m_CullMode = aElement->GetAttribute<int>("cull_mode", 3);
	m_ClockWise = aElement->GetAttribute<bool>("clock_wise", true);
	return CreateRasterizerState();
}

void engine::render::CSetRasterizerState::Execute(CRenderManager& lRM)
{
	lRM.GetDeviceContext()->RSSetState(m_RasterizerState);
}

void engine::render::CSetRasterizerState::ChangeState()
{
	/*
	 * D3D11_FILL_WIREFRAME	= 2,
        D3D11_FILL_SOLID	= 3
	 */
	if (m_FillMode == 2)
	{
		m_FillMode = 3;
	}
	else
	{
		m_FillMode = 2;
	}
	CreateRasterizerState();
}

void engine::render::CSetRasterizerState::DrawIMGUI()
{
}

bool engine::render::CSetRasterizerState::CreateRasterizerState()
{
	D3D11_RASTERIZER_DESC lRasterDesc;
	ZeroMemory(&lRasterDesc, sizeof(D3D11_RASTERIZER_DESC));
	lRasterDesc.FillMode = (D3D11_FILL_MODE)m_FillMode;
	lRasterDesc.AntialiasedLineEnable = false;
	lRasterDesc.CullMode = (D3D11_CULL_MODE)m_CullMode;
	lRasterDesc.DepthBias = 0;
	lRasterDesc.DepthBiasClamp = 0.0f;
	lRasterDesc.DepthClipEnable = true;
	lRasterDesc.FrontCounterClockwise = m_ClockWise;
	lRasterDesc.MultisampleEnable = false;
	lRasterDesc.ScissorEnable = false;
	lRasterDesc.SlopeScaledDepthBias = 0.0f;
	// Create rasterizer state
	HRESULT lHR = CEngine::GetInstance().GetRenderManager().GetDevice()->CreateRasterizerState(&lRasterDesc, &m_RasterizerState);
	return SUCCEEDED(lHR);
}
