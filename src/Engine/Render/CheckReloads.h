#ifndef _ENGINE_CHECKRELOADS_08052017222716_H
#define _ENGINE_CHECKRELOADS_08052017222716_H
#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CCheckReloads;

		class CCheckReloads : public CRenderCmd
		{
		public:
			CCheckReloads();
			virtual ~CCheckReloads();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CCheckReloads);
		};
	}
}
#endif	// _ENGINE_CHECKRELOADS_08052017222716_H