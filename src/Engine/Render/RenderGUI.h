#ifndef _ENGINE_RENDER_CRENDERGUI_H
#define _ENGINE_RENDER_CRENDERGUI_H
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CRenderGUI : public CRenderCmd
		{
		public:
			CRenderGUI();
			virtual ~CRenderGUI();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
			//void SetActive(bool active){ m_Active = active; }
		private:
			DISALLOW_COPY_AND_ASSIGN(CRenderGUI);
		};
	}
}


#endif