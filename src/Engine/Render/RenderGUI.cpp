#include "RenderGUI.h"
#include "Engine.h"
#include "Graphics/GUI/GUIManager.h"

namespace engine
{
	namespace render
	{
		CRenderGUI::CRenderGUI()
		{
		}

		CRenderGUI::~CRenderGUI()
		{
		}

		bool CRenderGUI::Load(const tinyxml2::XMLElement* aElement)
		{
			CRenderCmd::Load(aElement);
			return true;
		}

		void CRenderGUI::Execute(CRenderManager& lRM)
		{
			CEngine::GetInstance().GetGUIManager().Render(lRM);
		}
	}
}
