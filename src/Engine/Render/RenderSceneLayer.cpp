#include "RenderSceneLayer.h"
#include "XML/XML.h"
#include "RenderManager.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"

engine::render::CRenderSceneLayer::CRenderSceneLayer()
	: m_LayerName("")
{
}

engine::render::CRenderSceneLayer::~CRenderSceneLayer()
{
}

bool engine::render::CRenderSceneLayer::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	this->m_LayerName = aElement->Attribute("layer");
	return true;
}

void engine::render::CRenderSceneLayer::Execute(CRenderManager& lRM)
{
	scenes::CSceneManager &l_SM = CEngine::GetInstance().GetSceneManager();
	if (l_SM.GetCurrentScene()->Exist(m_LayerName)){
		CEngine::GetInstance().GetSceneManager().Render(m_LayerName);
	}
}
