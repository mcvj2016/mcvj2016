#ifndef __CDRAWQUAD_HH__
#define __CDRAWQUAD_HH__

#include "RenderStagedTexture.h"
#include "Graphics/Scenes/Quad.h"
#include "Graphics/Lights/Light.h"

namespace engine
{
	namespace render
	{
		class CDrawQuad : public CRenderStagedTexture
		{
		public:
			CDrawQuad();
			virtual ~CDrawQuad();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& RM);
			materials::CMaterial *mMaterial;
		protected:
			DISALLOW_COPY_AND_ASSIGN(CDrawQuad);
			CQuad* mQuad;

			//Miembros utilizados para el LigthScattering
			int m_ParamPosition;
			bool m_UseLightScattering;
			std::string m_LightName;
			lights::CLight* m_ScatteringLight;
		};
	}
}
#endif
