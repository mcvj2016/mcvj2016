#ifndef _ENGINE_RENDER_SETUPMATRICES_H
#define _ENGINE_RENDER_SETUPMATRICES_H

#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CSetUpMatricesCmd : public CRenderCmd

		{
			public:
				CSetUpMatricesCmd();
				virtual ~CSetUpMatricesCmd();
				bool Load(const CXMLElement* aElement);
				virtual void Execute(CRenderManager& lRM);
			private:
				DISALLOW_COPY_AND_ASSIGN(CSetUpMatricesCmd);
		};
	}
}
#endif