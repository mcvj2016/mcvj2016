#include "ApplyFilter.h"
#include "Engine.h"
#include  "Graphics/Textures/TextureManager.h"
namespace engine
{
	namespace render
	{
		CApplyFilter::CApplyFilter(){
		}

		CApplyFilter::~CApplyFilter()
		{
			base::utils::CheckedDelete(m_DynamicTexturesMaterials);
		}
		bool CApplyFilter::Load(const CXMLElement* aElement)
		{
			if (CRenderStagedTexture::Load(aElement))
			{
				for (CDynamicTextureMaterial* material : m_DynamicTexturesMaterials)
				{
					CEngine::GetInstance().GetTextureManager().AddTexture(material->m_DynamicTexture);
				}
				mQuad = new CQuad();
				return true;
			}
			return false;
		}

		void CApplyFilter::Execute(engine::render::CRenderManager &lRM)
		{
			ID3D11DeviceContext* lContext = lRM.GetDeviceContext();
			for (size_t i = 0, lCount = m_DynamicTexturesMaterials.size(); i < lCount; ++i)
			{
				ID3D11DepthStencilView *l_DepthStencilView = m_DynamicTexturesMaterials.empty() ?
					nullptr : m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetDepthStencilView();
				lRM.SetRenderTargets(1, &m_RenderTargetViews[i], l_DepthStencilView);
			    lRM.SetViewport(Vect2u(0, 0),
					m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetSize());
				m_DynamicTexturesMaterials[i]->m_Material->Apply();
				ActivateTextures();
				if (i!=0)
				{
					m_DynamicTexturesMaterials[i - 1]->m_DynamicTexture->Bind(0, lContext);
				}
				mQuad->Render();
				//lRM.UnsetRenderTargets();
			}
		}
	}
}
