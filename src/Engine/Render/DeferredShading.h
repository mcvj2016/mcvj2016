#ifndef _ENGINE_SHADER_DEFERREDSHADING_H
#define _ENGINE_SHADER_DEFERREDSHADING_H

#include "Render/DrawQuad.h"


namespace engine
{
	namespace render
	{
		class CDeferredShading : public render::CDrawQuad

		{

		public:
			CDeferredShading();
			virtual ~CDeferredShading();
			bool Load(const CXMLElement* aElement);	
			virtual void Execute(render::CRenderManager &RM);

		private:
			DISALLOW_COPY_AND_ASSIGN(CDeferredShading);
			ID3D11BlendState *m_EnabledAlphaBlendState;

		};
	}
}
#endif
