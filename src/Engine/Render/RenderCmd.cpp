#include "RenderCmd.h"
#include "XML/XML.h"

engine::render::CRenderCmd::CRenderCmd()
	: m_Active(true)
{
}

engine::render::CRenderCmd::~CRenderCmd()
{
}

bool engine::render::CRenderCmd::Load(const tinyxml2::XMLElement* aElement)
{
	m_Name = aElement->GetAttribute<std::string>("name", "");
	m_Active = aElement->GetAttribute<bool>("enabled", true);
	return true;
}

void engine::render::CRenderCmd::SetActive(bool Active)
{
	m_Active = Active;
}
