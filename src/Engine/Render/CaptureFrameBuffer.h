#ifndef _ENGINE_RENDER_CCAPTUREFRAMEBUFFER_H
#define _ENGINE_RENDER_CCAPTUREFRAMEBUFFER_H
#include "Utils/Defines.h"
#include "RenderStagedTexture.h"
#include "CaptureFrameBufferTexture.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CCaptureFrameBuffer : public CRenderStagedTexture
		{
		public:
			CCaptureFrameBuffer();
			virtual ~CCaptureFrameBuffer();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CCaptureFrameBuffer);
			CCaptureFrameBufferTexture* m_CapturedFrameBufferTexture;
		};
	}
}

#endif