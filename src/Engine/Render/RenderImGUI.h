#ifndef _ENGINE_RENDER_CENDRENDERIMGUI_H
#define _ENGINE_RENDER_CENDRENDERIMGUI_H
#include "Utils/Defines.h"
#include "RenderCmd.h"
#include "Graphics/Scenes/SceneNode.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CRenderImGUI : public CRenderCmd
		{
		public:
			CRenderImGUI();
			virtual ~CRenderImGUI();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CRenderImGUI);
		};
	}
}


#endif