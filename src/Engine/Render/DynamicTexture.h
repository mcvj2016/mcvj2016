#ifndef DYNAMIC_TEXTURE_H
#define DYNAMIC_TEXTURE_H

#include "Utils/EnumToString.h"
#include "Utils/Defines.h"
#include "Graphics/Textures/Texture.h"

XML_FORWARD_DECLARATION

namespace engine
{
	namespace render
	{
		class CDynamicTexture : public engine::materials::CTexture
		{
		public:
			// https://msdn.microsoft.com/en-us/library/windows/desktop/bb173059(v=vs.85).aspx
			enum TFormatType
			{
				RGBA32_FLOAT = 2,
				RGBA8_UNORM = 28,
				R32_FLOAT = 41,
			};
		public:
			CDynamicTexture(const CXMLElement *TreeNode);
			CDynamicTexture(std::string aName, float width, float height, bool createDepthStencil, const std::string &format);
			virtual ~CDynamicTexture();
			void SetFormat(const std::string& Format);

			GET_SET_PTR(ID3D11RenderTargetView, RenderTargetView);
			GET_SET_PTR(ID3D11DepthStencilView, DepthStencilView);

		protected:
			ID3D11Texture2D         *m_RenderTargetTexture;
			ID3D11RenderTargetView  *m_RenderTargetView;
			ID3D11Texture2D         *m_DepthStencilBuffer;
			ID3D11DepthStencilView  *m_DepthStencilView;
			bool                    m_CreateDepthStencilBuffer;
			TFormatType             m_FormatType;

			void Init();
			virtual bool CreateSamplerState();
		};

		
		}
		}
		//---------------------------------------------------------------------------------------------------------
		Begin_Enum_String(engine::render::CDynamicTexture::TFormatType)
			{
				Enum_String_Id(engine::render::CDynamicTexture::R32_FLOAT, "R32_FLOAT");
				Enum_String_Id(engine::render::CDynamicTexture::RGBA32_FLOAT, "RGBA32_FLOAT");
				Enum_String_Id(engine::render::CDynamicTexture::RGBA8_UNORM, "RGBA8_UNORM");
			}
			End_Enum_String;

#endif //TEXTURE_H