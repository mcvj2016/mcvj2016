#include "ApplyTechniquePool.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Effects/TechniquePoolManager.h"

engine::render::CApplyTechniquePool::CApplyTechniquePool()
	: m_PoolName("")
{
}

engine::render::CApplyTechniquePool::~CApplyTechniquePool()
{
}

bool engine::render::CApplyTechniquePool::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	this->m_PoolName = aElement->GetAttribute<std::string>("pool_name","");
	assert(m_PoolName!="");
	return true;
}

void engine::render::CApplyTechniquePool::Execute(engine::render::CRenderManager& lRM)
{
	CEngine::GetInstance().GetTechniquePoolManager().Apply(m_PoolName);
}
