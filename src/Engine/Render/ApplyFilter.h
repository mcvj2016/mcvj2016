#ifndef _ENGINE_RENDER_CAPPLYFILTER_H
#define _ENGINE_RENDER_CAPPLYFILTER_H
#include "DrawQuad.h"

namespace engine
{
	namespace render
	{
		class CApplyFilter : public engine::render::CDrawQuad
		{
		public:
			CApplyFilter();
			virtual ~CApplyFilter();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(engine::render::CRenderManager &RM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CApplyFilter);
		};
	}
}


#endif
