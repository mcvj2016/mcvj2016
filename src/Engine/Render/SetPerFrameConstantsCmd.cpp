#include "SetPerFrameConstantsCmd.h"
#include "XML/XML.h"
#include "RenderManager.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Camera/CameraManager.h"

engine::render::CSetPerFrameConstantsCmd::CSetPerFrameConstantsCmd()
{
}

engine::render::CSetPerFrameConstantsCmd::~CSetPerFrameConstantsCmd()
{
}

bool engine::render::CSetPerFrameConstantsCmd::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void engine::render::CSetPerFrameConstantsCmd::Execute(CRenderManager& lRM)
{
	CCameraController &l_CameraController = *CEngine::GetInstance().GetCameraManager().GetMainCamera();
	buffers::CConstantBufferManager &l_CM = CEngine::GetInstance().GetConstantBufferManager();
	
	l_CM.mFrameDesc.m_View = lRM.m_ViewMatrix;
	l_CM.mFrameDesc.m_InverseView = lRM.m_ViewMatrix.GetInverted();
	l_CM.mFrameDesc.m_Projection = lRM.m_ProjectionMatrix;
	l_CM.mFrameDesc.m_InverseProjection = lRM.m_ProjectionMatrix.GetInverted();
	l_CM.mFrameDesc.m_ViewProjection = lRM.m_ViewProjectionMatrix;
	l_CM.mFrameDesc.m_CameraEye = l_CameraController.GetSceneCamera()->GetPosition();
	l_CM.mFrameDesc.m_CameraForward = l_CameraController.GetSceneCamera()->GetForward();
	l_CM.mFrameDesc.m_CameraRight = l_CameraController.GetSceneCamera()->GetRight();
	l_CM.mFrameDesc.m_CameraUp = l_CameraController.GetSceneCamera()->GetUp();

	//bind buffers, VS y PS
	ID3D11DeviceContext* lContext = lRM.GetDeviceContext();
	l_CM.BindVSBuffer(lContext, buffers::CConstantBufferManager::CB_Frame);
	l_CM.BindPSBuffer(lContext, buffers::CConstantBufferManager::CB_FramePS);

	//lights set up in setperframelightconstants

	//total time
	l_CM.mFrameDesc.m_totalTime = Vect4f(CEngine::GetInstance().m_totalTime,0,0,0);
}
