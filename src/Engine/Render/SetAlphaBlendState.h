#ifndef _ENGINE_RENDER_CSETALPHABLENDSTATE_H
#define _ENGINE_RENDER_CSETALPHABLENDSTATE_H
#include <d3d11.h>
#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;


		class CBlend : public CName
		{
		private:
			

			int CheckBlendType(const std::string _type);
			void Destroy();
		public:
			int m_Blend;
			CBlend(const CXMLElement* aElement, const std::string _name);
			virtual ~CBlend() { Destroy(); }
			int GetBlend(){ return m_Blend; }
		};

		class CBlendOp : public CName
		{
		private:
			

			int CheckBlendOpType(const std::string _type);
			void Destroy();
		public:
			int m_BlendOp;
			CBlendOp(const CXMLElement* aElement, const std::string _name);
			virtual ~CBlendOp() { Destroy(); }
			int GetBlendOp(){ return m_BlendOp; }
		};

		class CSetAlphaBlendState : public CRenderCmd
		{
		public:
			CSetAlphaBlendState();
			virtual ~CSetAlphaBlendState();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
			ID3D11BlendState *m_AlphaBlendState;
			CBlend m_SrcBlend;
			CBlend m_DestBlend;
			CBlend m_SrcAlphaBlend;
			CBlend m_DestAlphaBlend;
			CBlendOp m_OpBlend;
			CBlendOp m_OpAlphaBlend;
		private:
			DISALLOW_COPY_AND_ASSIGN(CSetAlphaBlendState);
			
			bool m_Enabled;
		};
	}
}


#endif