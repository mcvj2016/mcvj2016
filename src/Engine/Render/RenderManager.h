#ifndef _ENGINE_RENDERMANAGER_DPVD1_27102016203657_H
#define _ENGINE_RENDERMANAGER_DPVD1_27102016203657_H
#include <windows.h>
#include <d3d11.h>
#include "Math/Color.h"
#include "Math/Matrix44.h"
#include "Math/Vector4.h"
#include <memory>
#include <vector>

#define MAX_RENDER_TARGETS 6

template<typename T>
struct CReleaser
{
	void operator()(T* obj)
	{
		obj->Release();
	}
};

template<typename T>
using releaser_ptr = std::unique_ptr< T, CReleaser<T> >;

namespace engine
{
	namespace render
	{
		class CRenderManager
		{
		public:
			struct CDebugVertex
			{
				Vect4f Position;
				CColor Color;
			};

			static const int DebugVertexBufferSize = 0x10000;
			Vect4f m_BackgroundColor;
			D3D11_VIEWPORT m_Viewport;
			releaser_ptr<ID3D11DepthStencilView> m_DepthStencilView;
			HWND m_Hwnd;

			CRenderManager();
			~CRenderManager();
			bool Init(HWND& hWnd, int Width, int Height);
			void Clear(bool renderTarget, bool depthStencil, const CColor& backgroundColor);
			void Destroy();
			void BeginRender();
			void EndRender();
			void CreateDebugObjects();

			void SetModelMatrix(const Mat44f &Model) { m_ModelMatrix = Model; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
			void SetViewMatrix(const Mat44f &View) { m_ViewMatrix = View; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
			void SetProjectionMatrix(const Mat44f &Projection) { m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }

			void SetViewProjectionMatrix(const Mat44f &View, const Mat44f &Projection) { m_ViewMatrix = View; m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }

			void SetViewMatrix(const Vect3f& vPos, const Vect3f& vTarget, const Vect3f& vUp) { Mat44f View; View.SetIdentity(); View.SetFromLookAt(vPos, vTarget, vUp); SetViewMatrix(View); }
			void SetProjectionMatrix(float fovy, float aspect, float zn, float zf) { Mat44f Projection; Projection.SetIdentity(); Projection.SetFromPerspective(fovy, aspect, zn, zf); SetProjectionMatrix(Projection); }
			const Mat44f GetViewProjectionMatrix() const;
			void DrawAxis(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
			void DrawGrid(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
			void DrawCube(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
			void DrawSphere(float Radius, Vect3f position = (.0f, .0f, .0f), const CColor& Color = colWHITE);

			void DrawAxis(Vect3f Size, const CColor &Color = colWHITE) { DrawAxis(Size.x, Size.y, Size.z, Color); }
			void DrawAxis(float Size = 1.0f, const CColor &Color = colWHITE) { DrawAxis(Size, Size, Size, Color); }
			void DrawGrid(Vect3f Size, const CColor &Color = colWHITE) { DrawGrid(Size.x, Size.y, Size.z, Color); }
			void DrawGrid(float Size = 1.0f, const CColor &Color = colWHITE) { DrawGrid(Size, Size, Size, Color); }
			void DrawCube(Vect3f Size, const CColor &Color = colWHITE) { DrawCube(Size.x, Size.y, Size.z, Color); }
			void DrawCube(float Size = 1.0f, const CColor &Color = colWHITE) { DrawCube(Size, Size, Size, Color); }

			void Draw_Triangle();

			ID3D11Device* GetDevice();
			ID3D11DeviceContext* GetDeviceContext();

			void Resize(int Width, int Height, HWND& hWnd);
			void UnsetRenderTargets();
			void SetRenderTargets(int aNumViews, ID3D11RenderTargetView * const*aRenderTargetViews, ID3D11DepthStencilView* aDepthStencilViews);

			Mat44f m_ModelMatrix, m_ViewMatrix, m_ProjectionMatrix;
			Mat44f m_ViewProjectionMatrix, m_ModelViewProjectionMatrix;

			IDXGISwapChain* GetSwapChain();

			void SetViewport(const Vect2u& aPosition, const Vect2u& aSize);
			void ResetViewport();

			Vect2f GetPositionInScreenCoordinates(const Vect3f &Position) const;

			int GetWidth()
			{
				return m_iWidth;
			}
			int GetHeight()
			{
				return m_iHeight;
			}

		protected:
	
			releaser_ptr<ID3D11Device> m_Device;
			releaser_ptr<ID3D11DeviceContext> m_DeviceContext;
			releaser_ptr<IDXGISwapChain> m_SwapChain;
			ID3D11Debug* m_ID3D11Debug;
			releaser_ptr<ID3D11RenderTargetView> m_RenderTargetView;
			ID3D11RenderTargetView * m_CurrentRenderTargetViews[MAX_RENDER_TARGETS];
			int m_NumViews;
			ID3D11DepthStencilView* m_CurrentDepthStencilView;
			releaser_ptr<ID3D11Texture2D> m_DepthStencil;
			

			releaser_ptr<ID3D11Buffer> m_DebugVertexBuffer;
			int m_DebugVertexBufferCurrentIndex;
			releaser_ptr<ID3D11VertexShader> m_DebugVertexShader;
			releaser_ptr<ID3D11InputLayout> m_DebugVertexLayout;
			releaser_ptr<ID3D11PixelShader> m_DebugPixelShader;

			const CDebugVertex*						m_AxisRenderableVertexs;
			const CDebugVertex*						m_GridRenderableVertexs;
			const CDebugVertex*						m_CubeRenderableVertexs;
			//const CDebugVertex*						m_SphereRenderableVertexs;
			int m_NumVerticesAxis, m_NumVerticesGrid, m_NumVerticesCube, m_NumVerticesSphere;

			//Mat44f m_ModelMatrix, m_ViewMatrix, m_ProjectionMatrix;
			//Mat44f m_ViewProjectionMatrix, m_ModelViewProjectionMatrix;
		private:
			bool InitDevice_SwapChain_DeviceContext(HWND &hWnd);

			bool Get_RendertargetView();
			void CreateBackBuffers(int width, int height, HWND &hWnd);
			
			bool Create_DepthStencil();
			void SetViewport();
			
			bool Create_DebugShader();
			
			int m_iWidth;
			int m_iHeight;
			void DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint);
		};
	}
}
#endif