#ifndef _ENGINE_RENDER_CENDRENDERPIPELINE_H
#define _ENGINE_RENDER_CENDRENDERPIPELINE_H

#include "RenderCmd.h"
#include "Utils/TemplatedMapVector.h"

namespace engine
{
	namespace render
	{


		class CRenderPipeline : public base::utils::CTemplatedMapVector< CRenderCmd >, public CName
		{
		public:
			CRenderPipeline(std::string aFilename, bool aEnabled, std::string aName);
			virtual ~CRenderPipeline();
			void SwapRasterizedState();
			bool Load();
			void Execute();
			void SwapCmdActive(std::string aName);
			void Reload();
			GET_SET(bool, Enabled)
				bool m_Reloading;
		private:
			std::string m_Filename;
			bool m_Enabled;
			
		};
	}
}
#endif