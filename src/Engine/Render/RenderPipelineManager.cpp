#include "RenderPipelineManager.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"


namespace engine
{
	namespace render
	{
		CRenderPipelineManager::CRenderPipelineManager()
			: m_Path("data/common"),
			m_Filename(m_Path + "/render_pipelines.xml"),
			m_PipelinesPath(m_Path + "/render_pipelines/"),
			m_ActivePipeline(nullptr),
			m_ChangingPipeline(false),
			m_NextPipelineToLoad("")
		{
		}

		CRenderPipelineManager::~CRenderPipelineManager()
		{
		}

		bool CRenderPipelineManager::HotLoad(const std::string& name)
		{
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Shaders";
			}

			CXMLElement* pipelines = xmldoc.FirstChildElement("pipelines");

			assert(pipelines != nullptr);

			CXMLElement* pipeline = pipelines->FirstChildElement("pipeline");

			assert(pipeline != nullptr);

			while (pipeline != nullptr)
			{

				std::string pipFileName = pipeline->GetAttribute<std::string>("filename", "");
				assert(pipFileName != "");
				std::string pipName = pipeline->GetAttribute<std::string>("name", "");
				assert(pipName != "");

				CRenderPipeline* rpip = new CRenderPipeline(m_PipelinesPath + pipFileName, true, pipName);

				if (!Add(pipName, rpip))
				{
					CheckedDelete(rpip);
				}
				else
				{
					if (pipName == name)
					{
						rpip->Load();
						m_ActivePipeline = rpip;
					}
				}
				
				pipeline = pipeline->NextSiblingElement("pipeline");
			}
			return true;
		}

		bool CRenderPipelineManager::Load()
		{
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Shaders";
			}

			CXMLElement* pipelines = xmldoc.FirstChildElement("pipelines");

			assert(pipelines != nullptr);

			CXMLElement* pipeline = pipelines->FirstChildElement("pipeline");

			assert(pipeline != nullptr);

			while (pipeline != nullptr)
			{
				bool enabled = pipeline->GetAttribute<bool>("enabled", false);

				std::string pipFileName = pipeline->GetAttribute<std::string>("filename", "");
				assert(pipFileName != "");
				std::string pipName = pipeline->GetAttribute<std::string>("name", "");
				assert(pipName != "");
					
				
				CRenderPipeline* rpip = new CRenderPipeline(m_PipelinesPath + pipFileName, enabled, pipName);
				if (!Add(pipName, rpip))
				{
					CheckedDelete(rpip);
				}
				else
				{
					if (enabled)
					{
						rpip->Load();
						m_ActivePipeline = rpip;
					}
				}
				

				pipeline = pipeline->NextSiblingElement("pipeline");
			}
			return true;
		}

		void CRenderPipelineManager::SetActivePipeline(const std::string& aName)
		{
			m_ChangingPipeline = true;
			m_NextPipelineToLoad = aName;
		}

		void CRenderPipelineManager::ChangeActivePipeline()
		{
			m_ActivePipeline->Destroy();
			this->Destroy();
			HotLoad(m_NextPipelineToLoad);
			m_ChangingPipeline = false;
		}

		CRenderPipeline& CRenderPipelineManager::GetActivePipeline()
		{
			return *m_ActivePipeline;
		}
	}
}

