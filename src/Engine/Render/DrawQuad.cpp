#include "DrawQuad.h"
#include "Engine.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Materials/MaterialParameter.h"
#include "Graphics/Lights/LightManager.h"
#include "Camera/CameraManager.h"
#include "Camera/CameraController.h"
#include "Render/RenderManager.h"
#include "Graphics/Scenes/SceneLight.h"

namespace engine
{
	namespace render
	{
		CDrawQuad::CDrawQuad(): m_ScatteringLight(nullptr),m_ParamPosition(0),m_UseLightScattering(false), m_LightName("")
		{
			
		}

		CDrawQuad::~CDrawQuad()
		{
			base::utils::CheckedDelete(mQuad);
			//base::utils::CheckedDelete(m_ScatteringLight);
		}

		bool CDrawQuad::Load(const tinyxml2::XMLElement* aElement) 
		{
			CRenderStagedTexture::Load(aElement);
			//create material
			std::string matName = aElement->GetAttribute<std::string>("material", "");
			materials::CMaterialManager& matManager = CEngine::GetInstance().GetMaterialManager();
			mMaterial = matManager.operator()(matName);
			//create quad
			mQuad = new CQuad();

			m_UseLightScattering = aElement->GetAttribute<bool>("light_scattering", false);
			m_LightName = aElement->GetAttribute<std::string>("light_name", "");

			return true;
		}

		void CDrawQuad::Execute(CRenderManager& RM)
		{
			if (m_UseLightScattering)
			{
				std::vector<materials::CMaterialParameter*> l_Parameters = mMaterial->GetParameters();
				if (m_LightName != "" && m_ScatteringLight == nullptr)
				{
					m_ScatteringLight = CEngine::GetInstance().GetLightManager().GetLight(m_LightName);
					for (size_t i = 0; i<l_Parameters.size(); i++)
					{
						if (l_Parameters[i]->GetName() == "light_position")
						{
							m_ParamPosition = i;
							//memcpy(l_Parameters[i]->GetValueAddress(), &l_2DPos, sizeof(Vect2f));
						}
					}
				}
				if (m_ScatteringLight != nullptr)
				{
					engine::render::CRenderManager& l_RM = CEngine::GetInstance().GetRenderManager();
					scenes::CSceneLight* l_SceneLight = m_ScatteringLight->GetSceneLight();
					Vect2f l_2DPos = l_RM.GetPositionInScreenCoordinates(l_SceneLight->GetPosition()*100);
					memcpy(l_Parameters[m_ParamPosition]->GetAddr(), &l_2DPos, sizeof(Vect2f));
				}				
			}

			RM.SetViewport(m_ViewportPosition, m_ViewportSize);
			mMaterial->Apply();
			ActivateTextures();
			mQuad->Render();
			RM.ResetViewport();
		}
	}
}
