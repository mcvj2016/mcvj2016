#include "SetPerFrameLightConstants.h"
#include "Engine.h"
#include "Graphics/Lights/LightManager.h"

namespace engine
{
	namespace render
	{
		CSetPerFrameLightConstants::CSetPerFrameLightConstants()
		{
		}

		CSetPerFrameLightConstants::~CSetPerFrameLightConstants()
		{
		}

		void CSetPerFrameLightConstants::Execute(CRenderManager& lRM)
		{
			//lights
			lights::CLightManager &l_LM = CEngine::GetInstance().GetLightManager();
			l_LM.SetLightsConstants();
			l_LM.UpdateConstants();
		}

		bool CSetPerFrameLightConstants::Load(const tinyxml2::XMLElement* aElement)
		{
			CRenderCmd::Load(aElement);
			return true;
		}

	}
}
