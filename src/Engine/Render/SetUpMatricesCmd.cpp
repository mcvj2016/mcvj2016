#include "SetUpMatricesCmd.h"
#include "Camera/CameraController.h"
#include "Engine.h"
#include "RenderManager.h"
#include "Camera/CameraManager.h"

namespace engine
{
	namespace render
	{
		CSetUpMatricesCmd::CSetUpMatricesCmd()
		{
		}

		CSetUpMatricesCmd::~CSetUpMatricesCmd()
		{
		}

		bool CSetUpMatricesCmd::Load(const tinyxml2::XMLElement* aElement)
		{
			CRenderCmd::Load(aElement);
			return true;
		}

		void CSetUpMatricesCmd::Execute(CRenderManager& lRM)
		{
			CCameraController &l_CameraController = *CEngine::GetInstance().GetCameraManager().GetMainCamera();
			//l_CameraController.GetFOV()
			lRM.SetProjectionMatrix(
				l_CameraController.GetFOV(), 
				(float)lRM.m_Viewport.Width / (float)lRM.m_Viewport.Height, 
				l_CameraController.GetNear(), 
				l_CameraController.GetFar()
			);
			lRM.SetViewMatrix(l_CameraController.GetView());
		}
	}
}
