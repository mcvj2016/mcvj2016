#ifndef _ENGINE_RENDER_CSETPERFRAMECONSTANTSCMD_H
#define _ENGINE_RENDER_CSETPERFRAMECONSTANTSCMD_H
#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CSetPerFrameConstantsCmd : public CRenderCmd
		{
		public:
			CSetPerFrameConstantsCmd();
			virtual ~CSetPerFrameConstantsCmd();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CSetPerFrameConstantsCmd);
		};
	}
}

#endif