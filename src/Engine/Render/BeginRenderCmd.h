#ifndef _ENGINE_RENDER_CBEGINRENDERCMD_H
#define _ENGINE_RENDER_CBEGINRENDERCMD_H
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CBeginRenderCmd : public CRenderCmd
		{
		public:
			CBeginRenderCmd();
			virtual ~CBeginRenderCmd();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(engine::render::CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CBeginRenderCmd);
		};
	}
}

#endif