#ifndef _ENGINE_RENDER_CGENERATESHADOWMAPCMD_H
#define _ENGINE_RENDER_CGENERATESHADOWMAPCMD_H
#include "RenderCmd.h"

namespace engine
{
	namespace render
	{
		class CGenerateShadowMaps : public CRenderCmd
		{
			public:
				CGenerateShadowMaps();
				virtual ~CGenerateShadowMaps();
				bool Load(const CXMLElement* aElement);
				virtual void Execute(CRenderManager& lRM);
			private:
				DISALLOW_COPY_AND_ASSIGN(CGenerateShadowMaps);
		};
	}
}

#endif