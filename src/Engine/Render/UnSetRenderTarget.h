#ifndef _ENGINE_RENDER_CUNSETRENDERTARGET_H
#define _ENGINE_RENDER_CUNSETRENDERTARGET_H
#include "Utils/Defines.h"
#include "RenderStagedTexture.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CUnSetRenderTarget : public CRenderStagedTexture
		{
		public:
			CUnSetRenderTarget();
			virtual ~CUnSetRenderTarget();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CUnSetRenderTarget);
		};
	}
}

#endif