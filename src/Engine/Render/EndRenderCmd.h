#ifndef _ENGINE_RENDER_CENDRENDERCMD_H
#define _ENGINE_RENDER_CENDRENDERCMD_H
#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CEndRenderCmd : public CRenderCmd
		{
		public:
			CEndRenderCmd();
			virtual ~CEndRenderCmd();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CEndRenderCmd);
		};
	}
}

#endif