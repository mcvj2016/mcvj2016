#include "SetAlphaBlendState.h"
#include "Engine.h"
#include "RenderManager.h"
#include "XML/XML.h"
#include <cassert>

engine::render::CSetAlphaBlendState::CSetAlphaBlendState()
	: m_AlphaBlendState(nullptr),
	  m_SrcBlend(nullptr, "src"),
	  m_DestBlend(nullptr, "dest"),
	  m_SrcAlphaBlend(nullptr, "src_alpha"),
	  m_DestAlphaBlend(nullptr, "dest_alpha"),
	  m_OpBlend(nullptr, "operation"),
	  m_OpAlphaBlend(nullptr, "operation_alpha"),
	  m_Enabled(false)
{
}

engine::render::CSetAlphaBlendState::~CSetAlphaBlendState()
{
}

bool engine::render::CSetAlphaBlendState::Load(const CXMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	m_SrcBlend = CBlend(aElement, "src");
	m_DestBlend = CBlend(aElement, "dest");
	m_SrcAlphaBlend = CBlend(aElement, "src_alpha");
	m_DestAlphaBlend = CBlend(aElement, "dest_alpha");
	m_OpBlend = CBlendOp(aElement, "operation");
	m_OpAlphaBlend = CBlendOp(aElement, "operation_alpha");

	m_Enabled = aElement->GetAttribute<std::string>("state", "enabled") == std::string("enabled");

	D3D11_BLEND_DESC l_desc = {};
	l_desc.RenderTarget[0].BlendEnable = m_Enabled;
	l_desc.RenderTarget[0].SrcBlend = D3D11_BLEND(m_SrcBlend.GetBlend());
	l_desc.RenderTarget[0].DestBlend = D3D11_BLEND(m_DestBlend.GetBlend());
	l_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP(m_OpBlend.GetBlendOp());
	l_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND(m_SrcAlphaBlend.GetBlend());
	l_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND(m_DestAlphaBlend.GetBlend());
	l_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP(m_OpAlphaBlend.GetBlendOp());
	l_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	ID3D11Device* l_Device = CEngine::GetInstance().GetRenderManager().GetDevice();

	HRESULT l_HR = l_Device->CreateBlendState(&l_desc, &m_AlphaBlendState);
	assert(l_HR == S_OK);
	return true;
}

void engine::render::CSetAlphaBlendState::Execute(CRenderManager& lRM)
{
	if (m_Enabled)
	{
		lRM.GetDeviceContext()->OMSetBlendState(m_AlphaBlendState, nullptr, 0xffffffff);
	}
	else
	{
		lRM.GetDeviceContext()->OMSetBlendState(nullptr, nullptr, 0xffffffff);
	}
}

engine::render::CBlend::CBlend(const CXMLElement* aElement, const std::string _name) :
	CName(_name),
	m_Blend(D3D11_BLEND_ONE)
{
	if (aElement != nullptr)
	{
		const char* charName = _name.c_str();
		m_Blend = CheckBlendType(aElement->GetAttribute<std::string>(charName, ""));
	}
}

void engine::render::CBlend::Destroy()
{
	// CHECKED_RELEASE(m_Blend); CHECKED_DESTROY(m_Blend); OJOCUIDAO
}

int engine::render::CBlend::CheckBlendType(const std::string _type)
{
	if (_type == "zero") { return D3D11_BLEND_ZERO; }
	if (_type == "one") { return D3D11_BLEND_ONE; }
	if (_type == "src_color") { return D3D11_BLEND_SRC_COLOR; }
	if (_type == "inv_src_color") { return D3D11_BLEND_INV_SRC_COLOR; }
	if (_type == "src_alpha") { return D3D11_BLEND_SRC_ALPHA; }
	if (_type == "inv_src_alpha") { return D3D11_BLEND_INV_SRC_ALPHA; }
	if (_type == "dest_alpha") { return D3D11_BLEND_DEST_ALPHA; }
	if (_type == "inv_dest_alpha") { return D3D11_BLEND_INV_DEST_ALPHA; }
	if (_type == "dest_color") { return D3D11_BLEND_DEST_COLOR; }
	if (_type == "inv_dest_color") { return D3D11_BLEND_INV_DEST_COLOR; }
	if (_type == "src_alpha_sat") { return D3D11_BLEND_SRC_ALPHA_SAT; }
	if (_type == "blend_factor") { return D3D11_BLEND_BLEND_FACTOR; }
	if (_type == "inv_blend_factor") { return D3D11_BLEND_INV_BLEND_FACTOR; }
	if (_type == "src1_color") { return D3D11_BLEND_SRC1_COLOR; }
	if (_type == "inv_src1_color") { return D3D11_BLEND_INV_SRC1_COLOR; }
	if (_type == "src1_alpha") { return D3D11_BLEND_SRC1_ALPHA; }
	if (_type == "inv_src1_alpha") { return D3D11_BLEND_INV_SRC1_ALPHA; }
	//assert(!"This should not happen");
	return 0;
}


engine::render::CBlendOp::CBlendOp(const CXMLElement* aElement, const std::string _name) :
	CName(_name),
	m_BlendOp(D3D11_BLEND_OP_ADD)
{
	if (aElement != nullptr)
	{
		const char* charName = _name.c_str();
		m_BlendOp = CheckBlendOpType(aElement->GetAttribute<std::string>(charName, ""));
	}	
}

void engine::render::CBlendOp::Destroy()
{
	// CHECKED_RELEASE(m_BlendOp); CHECKED_DESTROY(m_BlendOp); OJOCUIDAO
}

int engine::render::CBlendOp::CheckBlendOpType(const std::string _type)
{
	if (_type == "add") { return D3D11_BLEND_OP_ADD; }
	if (_type == "substract") { return D3D11_BLEND_OP_SUBTRACT; }
	if (_type == "rev_substract") { return D3D11_BLEND_OP_REV_SUBTRACT; }
	if (_type == "min") { return D3D11_BLEND_OP_MIN; }
	if (_type == "max") { return D3D11_BLEND_OP_MAX; }
	//assert(!"This should not happen");
	return 0;
}
