#include "ClearCmd.h"
#include "XML/XML.h"
#include "RenderManager.h"

engine::render::CClearCmd::CClearCmd()
	: m_RenderTarget(true),
	m_DepthStencil(true),
	m_Color(CColor(.0,.0,.0,.0))
{
}

engine::render::CClearCmd::~CClearCmd()
{
}

bool engine::render::CClearCmd::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	this->m_Color = aElement->GetAttribute<CColor>("color", CColor(1.0f,0.0f,1.0f,0.0f));
	this->m_DepthStencil = aElement->GetAttribute<bool>("depth_stencil", true);
	this->m_RenderTarget = aElement->GetAttribute<bool>("render_target", true);
	return true;
}

void engine::render::CClearCmd::Execute(engine::render::CRenderManager& lRM)
{
	lRM.Clear(m_RenderTarget, m_DepthStencil, m_Color);
}
