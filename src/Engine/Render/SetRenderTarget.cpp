#include "SetRenderTarget.h"
#include "XML/XML.h"
#include "Utils/CheckedDelete.h"
#include "Engine.h"
#include "Graphics/Textures/TextureManager.h"
#include "RenderManager.h"

engine::render::CSetRenderTarget::CSetRenderTarget()
{
}

engine::render::CSetRenderTarget::~CSetRenderTarget()
{
	for (CDynamicTextureMaterial* dynamic_texture_material : m_DynamicTexturesMaterials)
	{
		base::utils::CheckedDelete(dynamic_texture_material);
	}
}

bool engine::render::CSetRenderTarget::Load(const tinyxml2::XMLElement* aElement)
{
	/*
	 *  <texture stage_id="0" name="capture_frame_buffer"/>
			<dynamic_texture name="StartZBlur" size="512 512" material="ZBlurStartMaterial"
			format="RGBA8_UNORM"/>
	 */

	bool loaded = CRenderCmd::Load(aElement);
	loaded = CRenderStagedTexture::Load(aElement);
	for (CDynamicTextureMaterial* material : m_DynamicTexturesMaterials)
	{
		CEngine::GetInstance().GetTextureManager().AddTexture(material->m_DynamicTexture);
	}
	return loaded;
}


void engine::render::CSetRenderTarget::Execute(engine::render::CRenderManager& lRM)
{
	ID3D11DepthStencilView *l_DepthStencilView = m_DynamicTexturesMaterials.empty() ?
	nullptr : m_DynamicTexturesMaterials[0]->m_DynamicTexture->GetDepthStencilView();
	lRM.SetRenderTargets((UINT)m_RenderTargetViews.size(), &m_RenderTargetViews[0], l_DepthStencilView);
}
