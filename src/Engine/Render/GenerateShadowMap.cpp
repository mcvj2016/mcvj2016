#include "GenerateShadowMap.h"
#include "XML/XML.h"
#include "RenderManager.h"
#include "Engine.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Scenes/Scene.h"

using namespace engine::render;
using namespace engine::lights;
using namespace engine::scenes;

CGenerateShadowMaps::CGenerateShadowMaps()
{
}

CGenerateShadowMaps::~CGenerateShadowMaps()
{
}

bool CGenerateShadowMaps::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void CGenerateShadowMaps::Execute(engine::render::CRenderManager& lRM)
{
	for (size_t i = 0; i < CEngine::GetInstance().GetLightManager().GetCount(); ++i)
	{
		CLight* light = CEngine::GetInstance().GetLightManager()[i];

		if (light->m_GenerateShadowMap){

			//set shadow map
			light->SetShadowMap(lRM);
			//clear zbuffer and depth stencil
			lRM.Clear(true, true, CColor(0.0f, 1.0f, 0.0f, 1.0f));

			//draw layers
			for (CLayer* layer : light->GetLayers()){
				layer->Render();
			}
			lRM.UnsetRenderTargets();
		}
	}
}
