#ifndef _ENGINE_RENDER_CSETRENDERTARGET_H
#define _ENGINE_RENDER_CSETRENDERTARGET_H
#include "Utils/Defines.h"
#include "RenderStagedTexture.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render
	{
		class CRenderManager;

		class CSetRenderTarget : public CRenderStagedTexture
		{
		public:
			CSetRenderTarget();
			virtual ~CSetRenderTarget();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CSetRenderTarget);
		};
	}
}


#endif