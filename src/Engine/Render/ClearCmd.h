#ifndef _ENGINE_RENDER_CLEARCMD_H
#define _ENGINE_RENDER_CLEARCMD_H

#include "Math/Color.h"
#include "Utils/Defines.h"
#include "RenderCmd.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine {namespace render {
	class CRenderManager;
}
}

namespace engine
{
	namespace render
	{
		class CClearCmd : public CRenderCmd
		{
		public:
			CClearCmd();
			virtual ~CClearCmd();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(engine::render::CRenderManager& lRM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CClearCmd);
			bool m_RenderTarget;
			bool m_DepthStencil;
			CColor m_Color;
		};
	}
}

#endif 