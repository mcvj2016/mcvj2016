#include "BeginRenderCmd.h"
#include "RenderManager.h"
#include "XML/XML.h"
#include "Engine.h"

engine::render::CBeginRenderCmd::CBeginRenderCmd()
{
}

engine::render::CBeginRenderCmd::~CBeginRenderCmd()
{
}

bool engine::render::CBeginRenderCmd::Load(const tinyxml2::XMLElement* aElement)
{
	CRenderCmd::Load(aElement);
	return true;
}

void engine::render::CBeginRenderCmd::Execute(engine::render::CRenderManager& lRM)
{
	lRM.BeginRender(); 
}
