#ifndef _ENGINE_INPUTMANAGER_DPVD1_27102016203657_H
#define _ENGINE_INPUTMANAGER_DPVD1_27102016203657_H
#include <Windows.h>
#include <XInput.h>

namespace InputDefinitions
{
	enum MouseButton {
		LEFT, MIDDLE, RIGHT
	};
	enum MouseAxis {
		MOUSE_X, MOUSE_Y, DX, DY, DZ
	};
	enum GamepadButton {
		A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	};
	enum GamepadAxis {
		LEFT_THUMB_X, LEFT_THUMB_Y, RIGHT_THUMB_X, RIGHT_THUMB_Y, LEFT_TRIGGER, RIGHT_TRIGGER
	};

	inline MouseButton GetMouseButtonFromString(const char* str, MouseButton defaultValue = (MouseButton)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "LEFT") == 0)
			return LEFT;
		else if (strcmp(str, "MIDDLE") == 0)
			return MIDDLE;
		else if (strcmp(str, "RIGHT") == 0)
			return RIGHT;
		else
			return (MouseButton)-1;
	}

	inline MouseAxis GetMouseAxisFromString(const char* str, MouseAxis defaultValue = (MouseAxis)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "MOUSE_X") == 0)
			return MOUSE_X;
		else if (strcmp(str, "MOUSE_Y") == 0)
			return MOUSE_Y;
		else if (strcmp(str, "DX") == 0)
			return DX;
		else if (strcmp(str, "DY") == 0)
			return DY;
		else if (strcmp(str, "DZ") == 0)
			return DZ;
		else
			return (MouseAxis)-1;
	}

	inline GamepadButton GetGamepadButtonFromString(const char* str, GamepadButton defaultValue = (GamepadButton)-1)
	{
		//A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
		if (str == nullptr)
			return defaultValue;
		if (strcmp(str, "B") == 0)
			return B;
		if (strcmp(str, "A") == 0)
			return A;
		if (strcmp(str, "X") == 0)
			return X;
		if (strcmp(str, "Y") == 0)
			return Y;
		if (strcmp(str, "START") == 0)
			return START;
		if (strcmp(str, "BACK") == 0)
			return BACK;
		if (strcmp(str, "LEFT_THUMB") == 0)
			return LEFT_THUMB;
		if (strcmp(str, "RIGHT_THUMB") == 0)
			return RIGHT_THUMB;
		if (strcmp(str, "LB") == 0)
			return LB;
		if (strcmp(str, "RB") == 0)
			return RB;
		if (strcmp(str, "DPAD_UP") == 0)
			return DPAD_UP;
		if (strcmp(str, "DPAD_LEFT") == 0)
			return DPAD_LEFT;
		if (strcmp(str, "DPAD_RIGHT") == 0)
			return DPAD_RIGHT;
		if (strcmp(str, "DPAD_DOWN") == 0)
			return DPAD_DOWN;
			
		return static_cast<GamepadButton>(-1);
	}

	inline GamepadAxis GetGamepadAxisFromString(const char* str, GamepadAxis defaultValue = (GamepadAxis)-1)
	{
		//LEFT_THUMB_X, LEFT_THUMB_Y, RIGHT_THUMB_X, RIGHT_THUMB_Y, LEFT_TRIGGER, RIGHT_TRIGGER
		if (str == nullptr)
			return defaultValue;
		if (strcmp(str, "LEFT_THUMB_Y") == 0)
			return LEFT_THUMB_Y;
		if (strcmp(str, "LEFT_THUMB_X") == 0)
			return LEFT_THUMB_X;
		if (strcmp(str, "RIGHT_THUMB_X") == 0)
			return RIGHT_THUMB_X;
		if (strcmp(str, "RIGHT_THUMB_Y") == 0)
			return RIGHT_THUMB_Y;
		if (strcmp(str, "LEFT_TRIGGER") == 0)
			return LEFT_TRIGGER;
		if (strcmp(str, "RIGHT_TRIGGER") == 0)
			return RIGHT_TRIGGER;

		return (GamepadAxis)-1;
	}
}

namespace engine
{
	namespace input
	{
		class CInputManager
		{
		public:
			CInputManager(HWND& hWnd);
			bool IsKeyPressed(unsigned char KeyCode) const;
			bool KeyBecomesPressed(unsigned char KeyCode) const;
			bool KeyBecomesReleased(unsigned char KeyCode) const;

			int GetMouseX() const;
			int GetMouseY() const;
			int GetMouseMovementX() const;
			int GetMouseMovementY() const;
			int GetMouseMovementZ() const;
			int GetMouseAxis(InputDefinitions::MouseAxis axis) const;
			bool IsMouseButtonPressed(InputDefinitions::MouseButton button) const;
			bool MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const;
			bool MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const;

			bool LeftMouseButtonPressed() const;
			bool MiddleMouseButtonPressed() const;
			bool RightMouseButtonPressed() const;

			bool LeftMouseButtonBecomesPressed() const;
			bool MiddleMouseButtonBecomesPressed() const;
			bool RightMouseButtonBecomesPressed() const;

			bool LeftMouseButtonBecomesReleased() const;
			bool MiddleMouseButtonBecomesReleased() const;
			bool RightMouseButtonBecomesReleased() const;

			//se llaman cuando recibimos mensajes de windows
			bool HandleKeyboard(const MSG& msg);
			bool HandleMouse(const MSG& mseg);
			void SetupHighprecisionMouse(HWND& hWnd);

			//usada para recopilar todos los mensajes de windows y consultar el gamepad
			void PreUpdate(bool isWindowActive);
			
			//se llama tras recopilar los mensajes de windows y recoger
			//el estado del gamepad
			void PostUpdate();
			float ProcessXInputStickValue(SHORT value, int deadZoneThreshold) const;
			float ProcessXInputTriggerValue(BYTE value, BYTE deadZoneThreshold) const;
			bool HasGamepad() const;
			bool IsGamepadButtonPressed(InputDefinitions::GamepadButton button) const;
			bool GamepadButtonBecomesPressed(InputDefinitions::GamepadButton button) const;
			bool GamepadButtonBecomesReleased(InputDefinitions::GamepadButton button) const;
			void SetupGamepad() const;
			float GetGamepadtAxis(InputDefinitions::GamepadAxis axis) const;

			wchar_t GetLastChar() const;
			void SetLastChar(wchar_t lastchar);
		private:
			//Struct con las teclas posibles y subgrupos de comunes
			struct KeyboardData
			{
				bool raw[256];
				bool escape, space;
				bool numpad[10];
				bool fkey[24];
				bool left, right, up, down;

				void SetKey(unsigned char key, bool state);
			} m_PreviousKeyboardState, m_KeyboardState;

			wchar_t m_LastChar;

			//MOUSE
			int m_MouseMovementX, m_MouseMovementY, m_MouseMovementZ, m_MouseX, m_MouseY;
			bool m_ButtonRight, m_PreviousButtonRight, m_ButtonLeft, m_PreviousButtonLeft, m_ButtonMiddle, m_PreviousButtonMiddle;
			bool m_HighPrecisionMouseRegistered;

			//GAMEPAD
			bool m_HasGamepad;
			//A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
			int m_GamepadA, m_PreviousGamepadA, m_GamepadB, m_PreviousGamepadB, m_GamepadX, m_PreviousGamepadX,
				m_GamepadY, m_PreviousGamepadY, m_GamepadLB, m_PreviousGamepadLB,
				m_GamepadStart, m_PreviousGamepadStart, m_GamepadRB, m_PreviousGamepadRB,
				m_GamepadBack, m_PreviousGamepadBack, m_GamepadLeftThumb, m_PreviousGamepadLeftThumb, 
				m_GamepadRightThumb, m_PreviousGamepadRightThumb, m_GamepadDPadUp, m_PreviousGamepadDPadUp,
				m_GamepadDPadLeft, m_PreviousGamepadDPadLeft, m_GamepadDPadRight, m_PreviousGamepadDPadRight,
				m_GamepadDPadDown, m_PreviousGamepadDPadDown;
			
			float m_GamepadStickLeftX, m_GamepadStickLeftY, m_GamepadStickRightX, m_GamepadStickRightY,
				m_GamepadLT, m_PreviousGamepadLT, m_GamepadRT, m_PreviousGamepadRT;
		};
	}
}
#endif