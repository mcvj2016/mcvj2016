#include "InputManager.h"
#include <cassert>

typedef DWORD WINAPI TInputGetState(DWORD dwUserIndex, XINPUT_STATE *pState);
typedef DWORD WINAPI TInputSetState(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration);
DWORD WINAPI InputGetStateStub(DWORD dwUserIndex, XINPUT_STATE *pState)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}
static TInputGetState* InputGetState = InputGetStateStub;

DWORD WINAPI InputSetStateStub(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}
typedef DWORD WINAPI TInputSetState(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration);
static TInputSetState* InputSetState = InputSetStateStub;

engine::input::CInputManager::CInputManager(HWND& hWnd) :
m_PreviousKeyboardState({}),
m_KeyboardState({}),
m_MouseMovementX(0),
m_MouseMovementY(0),
m_MouseMovementZ(0),
m_MouseX(0),
m_MouseY(0),
m_ButtonRight(false),
m_PreviousButtonRight(false),
m_ButtonLeft(false),
m_PreviousButtonLeft(false),
m_ButtonMiddle(false),
m_PreviousButtonMiddle(false),
m_HighPrecisionMouseRegistered(false),
m_GamepadA(0),
m_GamepadB(0),
m_GamepadX(0),
m_GamepadY(0),
m_GamepadStart(0),
m_GamepadLT(.0f),
m_GamepadLB(0),
m_GamepadRT(.0f),
m_GamepadRB(0),
m_PreviousGamepadA(0),
m_PreviousGamepadX(0),
m_PreviousGamepadY(0),
m_PreviousGamepadB(0),
m_PreviousGamepadStart(0),
m_PreviousGamepadLT(.0f),
m_PreviousGamepadLB(0),
m_PreviousGamepadRT(.0f),
m_PreviousGamepadRB(0),
m_HasGamepad(false),
m_GamepadStickLeftX(.0f),
m_GamepadStickLeftY(.0f),
m_GamepadStickRightX(.0f),
m_GamepadStickRightY(.0f),
m_GamepadBack(0),
m_GamepadLeftThumb(0),
m_GamepadRightThumb(0),
m_GamepadDPadUp(0),
m_GamepadDPadLeft(0),
m_GamepadDPadRight(0),
m_GamepadDPadDown(0),
m_LastChar(),
m_PreviousGamepadBack(0),
m_PreviousGamepadLeftThumb(0),
m_PreviousGamepadRightThumb(0),
m_PreviousGamepadDPadUp(0),
m_PreviousGamepadDPadLeft(0),
m_PreviousGamepadDPadRight(0),
m_PreviousGamepadDPadDown(0)
{
	SetupHighprecisionMouse(hWnd);
	SetupGamepad();
}

bool engine::input::CInputManager::IsKeyPressed(unsigned char KeyCode) const
{
	return m_KeyboardState.raw[KeyCode];
}

bool engine::input::CInputManager::KeyBecomesPressed(unsigned char KeyCode) const
{
	return m_KeyboardState.raw[KeyCode] && !m_PreviousKeyboardState.raw[KeyCode];
}

bool engine::input::CInputManager::KeyBecomesReleased(unsigned char KeyCode) const
{
	return !m_KeyboardState.raw[KeyCode] && m_PreviousKeyboardState.raw[KeyCode];
}

int engine::input::CInputManager::GetMouseX() const
{
	return m_MouseX;
}

int engine::input::CInputManager::GetMouseY() const
{
	return m_MouseY;
}

int engine::input::CInputManager::GetMouseMovementX() const
{
	return m_MouseMovementX;
}

int engine::input::CInputManager::GetMouseMovementY() const
{
	return m_MouseMovementY;
}

int engine::input::CInputManager::GetMouseMovementZ() const
{
	return m_MouseMovementZ;
}

wchar_t engine::input::CInputManager::GetLastChar() const
{
	return m_LastChar;
}

void engine::input::CInputManager::SetLastChar(wchar_t lastchar)
{
	m_LastChar = lastchar;
}

int engine::input::CInputManager::GetMouseAxis(InputDefinitions::MouseAxis axis) const
{
	switch (axis)
	{
	case InputDefinitions::MOUSE_X:
		return m_MouseX;
	case InputDefinitions::MOUSE_Y:
		return m_MouseY;
	case InputDefinitions::DX:
		return m_MouseMovementX;
	case InputDefinitions::DY:
		return m_MouseMovementY;
	case InputDefinitions::DZ:
		return m_MouseMovementZ;
	default:
		return 0;
	}
}

bool engine::input::CInputManager::HandleKeyboard(const MSG& msg)
{
	bool handled = false;

	unsigned char VKCode = static_cast<unsigned char>(msg.wParam);

	//unused vars for now
	//int repeat = msg.lParam & 0xffff;
	//bool AltKeyWasDown = ((msg.lParam & (1 << 29)) != 0);

	bool WasDown = ((msg.lParam & (1 << 30)) != 0);
	bool IsDown = ((msg.lParam & (1 << 31)) == 0);

	if (WasDown != IsDown)
	{
		m_KeyboardState.SetKey(VKCode, IsDown);

		handled = true;
	}
	return handled;
}

bool engine::input::CInputManager::HandleMouse(const MSG& msg)
{
	switch (msg.message)
	{
	case WM_LBUTTONDOWN:
		m_ButtonLeft = true;
		return true;
	case WM_MBUTTONDOWN:
		m_ButtonMiddle = true;
		return true;
	case WM_RBUTTONDOWN:
		m_ButtonRight = true;
		return true;
	case WM_LBUTTONUP:
		m_ButtonLeft = false;
		return true;
	case WM_MBUTTONUP:
		m_ButtonMiddle = false;
		return true;
	case WM_RBUTTONUP:
		m_ButtonRight = false;
		return true;
	case WM_MOUSEMOVE:
		m_MouseX = LOWORD(msg.lParam);
		m_MouseY = HIWORD(msg.lParam);
		return true;
	case WM_MOUSEWHEEL:
		//positivo hacia arriba negativo hacia abajo (+- 120)
		m_MouseMovementZ = GET_WHEEL_DELTA_WPARAM(msg.wParam);
	case WM_INPUT:
	{
		UINT dwSize = 60;
		static BYTE lpb[60];

		UINT read = GetRawInputData(reinterpret_cast<HRAWINPUT>(msg.lParam), RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = reinterpret_cast<RAWINPUT*>(lpb);

		if (raw->header.dwType == RIM_TYPEMOUSE && read <= dwSize)
		{
			m_MouseMovementX += raw->data.mouse.lLastX;
			m_MouseMovementY += raw->data.mouse.lLastY;

			return true;
		}
		return false;
	}
	default:
		return false;
	}
}

void engine::input::CInputManager::SetupHighprecisionMouse(HWND& hWnd)
{
	RAWINPUTDEVICE Rid[1];
	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x02;
	Rid[0].dwFlags = RIDEV_INPUTSINK;
	Rid[0].hwndTarget = hWnd;
	m_HighPrecisionMouseRegistered = (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) != 0);
}

void engine::input::CInputManager::PreUpdate(bool isWindowActive)
{
	//guardamos estado actual al anterior
	//keyboard
	m_PreviousKeyboardState = m_KeyboardState;
	//mouse
	m_PreviousButtonLeft = m_ButtonLeft;
	m_PreviousButtonMiddle = m_ButtonMiddle;
	m_PreviousButtonRight = m_ButtonRight;
	//gamepad
	m_PreviousGamepadA = m_GamepadA;
	m_PreviousGamepadB = m_GamepadB;
	m_PreviousGamepadX = m_GamepadX;
	m_PreviousGamepadY = m_GamepadY;
	m_PreviousGamepadLB = m_GamepadLB;
	m_PreviousGamepadLT = m_GamepadLT;
	m_PreviousGamepadRB = m_GamepadRB;
	m_PreviousGamepadRT = m_GamepadRT;
	m_PreviousGamepadStart = m_GamepadStart;
	m_PreviousGamepadBack = m_GamepadBack;
	m_PreviousGamepadLeftThumb = m_GamepadLeftThumb;
	m_PreviousGamepadRightThumb = m_GamepadRightThumb; 
	m_PreviousGamepadDPadUp = m_GamepadDPadUp;
	m_PreviousGamepadDPadLeft = m_GamepadDPadLeft; 
	m_PreviousGamepadDPadRight = m_GamepadDPadRight;
	m_PreviousGamepadDPadDown = m_GamepadDPadDown;

	//Seteamos ademas a zero los desplazamientos
	m_MouseMovementX = m_MouseMovementY = m_MouseMovementZ = 0;
	m_GamepadStickLeftX = m_GamepadStickLeftY = m_GamepadStickRightX = m_GamepadStickRightY = 0;

	//si se pierde el foco de la ventana limpiamos valores para al volver
	//no tener problemas
	if (!isWindowActive)
	{
		m_KeyboardState = {};
		m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
	}
}

void engine::input::CInputManager::PostUpdate()
{
	XINPUT_STATE l_ControllerState;
	if (InputGetState(0, &l_ControllerState) == ERROR_SUCCESS)
	{
		m_HasGamepad = true;
		//sticks (thumb)
		m_GamepadStickLeftY = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		m_GamepadStickLeftX = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		m_GamepadStickRightX = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		m_GamepadStickRightY = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

		//buttons
		m_GamepadA = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0;
		m_GamepadB = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B) != 0;
		m_GamepadY = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y) != 0;
		m_GamepadX = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X) != 0;
		//bumpers/shoulders
		m_GamepadLB = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) != 0;
		m_GamepadRB = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) != 0;
		//start/back
		m_GamepadStart = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START) != 0;
		m_GamepadBack = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) != 0;
		//pad
		m_GamepadDPadDown = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) != 0;
		m_GamepadDPadLeft = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) != 0;
		m_GamepadDPadRight = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) != 0;
		m_GamepadDPadUp = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) != 0;

		//triggers
		m_GamepadLT = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bLeftTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
		m_GamepadRT = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bRightTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
		/*if (l_ControllerState.Gamepad.bLeftTrigger && l_ControllerState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
		{
			m_GamepadLT = static_cast<int>(l_ControllerState.Gamepad.bLeftTrigger / 255.0f);
		}
		if (l_ControllerState.Gamepad.bRightTrigger && l_ControllerState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
		{
			m_GamepadRT = static_cast<int>(l_ControllerState.Gamepad.bRightTrigger / 255.0f);
		}*/
	}
	else
	{
		m_HasGamepad = false;
	}
}

float engine::input::CInputManager::GetGamepadtAxis(InputDefinitions::GamepadAxis axis) const
{
	switch (axis)
	{
	case InputDefinitions::LEFT_THUMB_Y:
	{
		return static_cast<float>(m_GamepadStickLeftY);
	}
	case InputDefinitions::LEFT_THUMB_X:
	{
		return static_cast<float>(m_GamepadStickLeftX);
	}
	case InputDefinitions::RIGHT_THUMB_Y:
	{
		return static_cast<float>(m_GamepadStickRightY);
	}
	case InputDefinitions::RIGHT_THUMB_X:
	{
		return static_cast<float>(m_GamepadStickRightX);
	}
	case InputDefinitions::LEFT_TRIGGER:
	{
		return static_cast<float>(m_GamepadLT);
	}
	case InputDefinitions::RIGHT_TRIGGER:
	{
		return static_cast<float>(m_GamepadRT);
	}
	case InputDefinitions::LEFT_THUMB:
	{
		return static_cast<float>(m_GamepadLeftThumb);
	}
	case InputDefinitions::RIGHT_THUMB:
	{
		return static_cast<float>(m_GamepadRightThumb);
	}
	default: return static_cast<InputDefinitions::GamepadAxis>(- 1); break;
	}
}

float engine::input::CInputManager::ProcessXInputStickValue(SHORT value, int deadZoneThreshold) const
{
	float result = .0f;

	if (value < -deadZoneThreshold)
	{
		result = static_cast<float>((value + deadZoneThreshold) / (32768.0f - deadZoneThreshold));
	}
	else if (value > deadZoneThreshold)
	{
		result = static_cast<float>((value - deadZoneThreshold) / (32768.0f - deadZoneThreshold));
	}

	return result;
}

float engine::input::CInputManager::ProcessXInputTriggerValue(BYTE value, BYTE deadZoneThreshold) const
{
	float result = .0f;

	if (value > deadZoneThreshold)
	{
		result = static_cast<float>((value - deadZoneThreshold) / (255.0f - deadZoneThreshold));
	}

	return result;
}

bool engine::input::CInputManager::HasGamepad() const
{
	return m_HasGamepad;
}

bool engine::input::CInputManager::IsGamepadButtonPressed(InputDefinitions::GamepadButton button) const
{
	//A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	switch (button)
	{
	case InputDefinitions::A:
		return m_GamepadA != 0;
	case InputDefinitions::B:
		return m_GamepadB != 0;
	case InputDefinitions::Y:
		return m_GamepadY != 0;
	case InputDefinitions::X:
		return m_GamepadX != 0;
	case InputDefinitions::START:
		return m_GamepadStart != 0;
	case InputDefinitions::BACK:
		return m_GamepadBack != 0;
	case InputDefinitions::LB:
		return m_GamepadLB != 0;
	case InputDefinitions::RB:
		return m_GamepadRB != 0;
	case InputDefinitions::LEFT_THUMB:
		return m_GamepadLeftThumb != 0;
	case InputDefinitions::RIGHT_THUMB:
		return m_GamepadRightThumb != 0;
	case InputDefinitions::DPAD_UP:
		return m_GamepadDPadUp != 0;
	case InputDefinitions::DPAD_LEFT:
		return m_GamepadDPadLeft != 0;
	case InputDefinitions::DPAD_RIGHT:
		return m_GamepadDPadRight != 0;
	case InputDefinitions::DPAD_DOWN:
		return m_GamepadDPadDown != 0;
	default:
		assert(false);
		return false;
	}
}

bool engine::input::CInputManager::GamepadButtonBecomesPressed(InputDefinitions::GamepadButton button) const
{
	//A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	switch (button)
	{
	case InputDefinitions::A:
		return m_GamepadA && !m_PreviousGamepadA;
	case InputDefinitions::B:
		return m_GamepadB && !m_PreviousGamepadB;
	case InputDefinitions::Y:
		return m_GamepadY && !m_PreviousGamepadY;
	case InputDefinitions::X:
		return m_GamepadX && !m_PreviousGamepadX;
	case InputDefinitions::START:
		return m_GamepadStart && !m_PreviousGamepadStart;
	case InputDefinitions::BACK:
		return m_GamepadBack && !m_PreviousGamepadBack;
	case InputDefinitions::LEFT_THUMB:
		return m_GamepadLeftThumb && !m_PreviousGamepadLeftThumb;
	case InputDefinitions::RIGHT_THUMB:
		return m_GamepadRightThumb && !m_PreviousGamepadRightThumb;
	case InputDefinitions::LB:
		return m_GamepadLB && !m_PreviousGamepadLB;
	case InputDefinitions::RB:
		return m_GamepadRB && !m_PreviousGamepadRB;
	case InputDefinitions::DPAD_UP:
		return m_GamepadDPadUp && !m_PreviousGamepadDPadUp;
	case InputDefinitions::DPAD_LEFT:
		return m_GamepadDPadLeft && !m_PreviousGamepadDPadLeft;
	case InputDefinitions::DPAD_RIGHT:
		return m_GamepadDPadRight && !m_PreviousGamepadDPadRight;
	case InputDefinitions::DPAD_DOWN:
		return m_GamepadDPadDown && !m_PreviousGamepadDPadDown;
	
	default:
		assert(false);
		return false;
	}
}

bool engine::input::CInputManager::GamepadButtonBecomesReleased(InputDefinitions::GamepadButton button) const
{
	//A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	switch (button)
	{
	case InputDefinitions::A:
		return !m_GamepadA && m_PreviousGamepadA;
	case InputDefinitions::B:
		return !m_GamepadB && m_PreviousGamepadB;
	case InputDefinitions::Y:
		return !m_GamepadY && m_PreviousGamepadY;
	case InputDefinitions::X:
		return !m_GamepadX && m_PreviousGamepadX;
	case InputDefinitions::START:
		return !m_GamepadStart && m_PreviousGamepadStart;
	case InputDefinitions::BACK:
		return !m_GamepadBack && m_PreviousGamepadBack;
	case InputDefinitions::LEFT_THUMB:
		return !m_GamepadLeftThumb && m_PreviousGamepadLeftThumb;
	case InputDefinitions::RIGHT_THUMB:
		return !m_GamepadRightThumb && m_PreviousGamepadRightThumb;
	case InputDefinitions::LB:
		return !m_GamepadLB && m_PreviousGamepadLB;
	case InputDefinitions::RB:
		return !m_GamepadRB && m_PreviousGamepadRB;
	case InputDefinitions::DPAD_UP:
		return !m_GamepadDPadUp && m_PreviousGamepadDPadUp;
	case InputDefinitions::DPAD_LEFT:
		return !m_GamepadDPadLeft && m_PreviousGamepadDPadLeft;
	case InputDefinitions::DPAD_RIGHT:
		return !m_GamepadDPadRight && m_PreviousGamepadDPadRight;
	case InputDefinitions::DPAD_DOWN:
		return !m_GamepadDPadDown && m_PreviousGamepadDPadDown;
	
	default:
		assert(false);
		return false;
	}
	
}

void engine::input::CInputManager::SetupGamepad() const
{
	HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
	if (!XInputLibrary)
	{
		XInputLibrary = LoadLibraryA("xinput9_1_0.dll");
	}

	if (!XInputLibrary)
	{
		XInputLibrary = LoadLibraryA("xinput1_3.dll");
	}

	if (XInputLibrary)
	{
		InputGetState = reinterpret_cast<TInputGetState *>(GetProcAddress(XInputLibrary, "XInputGetState"));
		if (!InputGetState)
		{
			InputGetState = InputGetStateStub;
		}

		InputSetState = reinterpret_cast<TInputSetState *>(GetProcAddress(XInputLibrary, "XInputSetState"));
		if (!InputSetState)
		{
			InputSetState = InputSetStateStub;
		}
	}
}

bool engine::input::CInputManager::IsMouseButtonPressed(InputDefinitions::MouseButton button) const
{
	switch (button)
	{
	case InputDefinitions::LEFT:
		return LeftMouseButtonPressed();
	case InputDefinitions::RIGHT:
		return RightMouseButtonPressed();
	case InputDefinitions::MIDDLE:
		return MiddleMouseButtonPressed();
	default:
		return 0;
	}
}

bool engine::input::CInputManager::MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const {
	switch (button)
	{
	case InputDefinitions::LEFT:
		return LeftMouseButtonBecomesPressed();
	case InputDefinitions::RIGHT:
		return RightMouseButtonBecomesPressed();
	case InputDefinitions::MIDDLE:
		return MiddleMouseButtonBecomesPressed();
	default:
		return 0;
	}
};

bool engine::input::CInputManager::MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const {
	switch (button)
	{
	case InputDefinitions::LEFT:
		return LeftMouseButtonBecomesReleased();
	case InputDefinitions::RIGHT:
		return RightMouseButtonBecomesReleased();
	case InputDefinitions::MIDDLE:
		return MiddleMouseButtonBecomesReleased();
	default:
		return 0;
	}
};

bool engine::input::CInputManager::LeftMouseButtonPressed() const { return m_ButtonLeft; }
bool engine::input::CInputManager::MiddleMouseButtonPressed() const { return m_ButtonMiddle; }
bool engine::input::CInputManager::RightMouseButtonPressed() const { return m_ButtonRight; }

bool engine::input::CInputManager::LeftMouseButtonBecomesPressed() const { return m_ButtonLeft && !m_PreviousButtonLeft; }
bool engine::input::CInputManager::MiddleMouseButtonBecomesPressed() const { return m_ButtonMiddle && !m_PreviousButtonMiddle; }
bool engine::input::CInputManager::RightMouseButtonBecomesPressed() const { return m_ButtonRight && !m_PreviousButtonRight; }

bool engine::input::CInputManager::LeftMouseButtonBecomesReleased() const { return !m_ButtonLeft && m_PreviousButtonLeft; }
bool engine::input::CInputManager::MiddleMouseButtonBecomesReleased() const { return !m_ButtonMiddle && m_PreviousButtonMiddle; }
bool engine::input::CInputManager::RightMouseButtonBecomesReleased() const { return !m_ButtonRight && m_PreviousButtonRight; }


void engine::input::CInputManager::KeyboardData::SetKey(unsigned char key, bool state)
{
	raw[key] = state;
	bool isSpecial = false;
	switch (key)
	{
	case VK_ESCAPE:
		escape = state;
		isSpecial = true;
		break;
	case VK_SPACE:
		space = state;
		isSpecial = true;
		//std::tr2::sys::space = state;
		break;
	case VK_LEFT:
		left = state;
		isSpecial = true;
		break;
	case VK_UP:
		up = state;
		break;
	case VK_RIGHT:
		isSpecial = true;
		right = state;
		break;
	case VK_DOWN:
		isSpecial = true;
		down = state;
		break;
	default: break;
	}

	if (!isSpecial)
	{
		if (key >= VK_NUMPAD0 || key <= VK_NUMPAD9)
		{
			numpad[key - VK_NUMPAD0] = state;
		}
		else if (key >= VK_F1 || key <= VK_F24)
		{
			fkey[key - VK_F1] = state;
		}
	}
}
