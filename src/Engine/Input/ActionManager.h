#ifndef _ENGINE_ACTIONMANAGER_DPVD1_27102016203657_H
#define _ENGINE_ACTIONMANAGER_DPVD1_27102016203657_H
#include <vector>
#include "Utils/TemplatedMap.h"
#include "InputManager.h"

namespace engine
{
	namespace input
	{
		struct ActionTrigger
		{
			enum ButtonActionType
			{
				IsPressed, IsReleased, BecomesPressed, BecomesReleased
			};
			enum TriggerType {
				KEYBOARD,
				MOUSE,
				MOUSE_BUTTON,
				GAMEPAD,
				GAMEPAD_BUTTON
			} type;
			
			union
			{
				struct
				{
					ButtonActionType actionType;
					unsigned char keyCode;
					float value;
					int arrow;
				} keyboard;
				struct MouseButton
				{
					ButtonActionType actionType;
					InputDefinitions::MouseButton button;
					float value;
				} mouseButton;
				struct
				{
					ButtonActionType actionType;
					InputDefinitions::GamepadButton button;
					float value;
				} gamepadButton;

				struct
				{
					float threshold;
					InputDefinitions::MouseAxis axis;
					bool geThreshold;
				} mouse;
				struct
				{
					float threshold;
					InputDefinitions::GamepadAxis axis;
					bool geThreshold;
				} gamepad;
			};

			static TriggerType GetTriggerTypeFromString(const char* str, TriggerType defaultValue = (TriggerType)-1);

			static ButtonActionType GetButtonActionTypeFromString(const char* str, ButtonActionType defaultValue = (ButtonActionType)-1);
		};

		struct InputAction
		{
			float value;
			bool active;

			std::vector<ActionTrigger> triggers;
		};

		class CActionManager : public base::utils::CTemplatedMap<InputAction>
		{
		public:
			CActionManager(const CInputManager& _InputManager);

			void Update();

			bool HasGamePad();

			void LoadXml();

			float GetGamepadAxisSensibility() const { return m_GamepadAxisSensibility; }
			float GetMouseAxisSensibility() const { return m_MouseAxisSensibility; }
			void SetMouseAxisSensibility(float value){ m_MouseAxisSensibility = value; }
			void SetGamepadAxisSensibility(float value){ m_GamepadAxisSensibility = value; }
			bool GetApplySensibilityOnLeftStick() const { return m_ApplySensibilityOnLeftStick; }
			bool GetApplySensibilityOnRightStick() const { return m_ApplySensibilityOnRightStick; }
			void SetApplySensibilityOnLeftStick(bool value){ m_ApplySensibilityOnLeftStick = value; }
			void SetApplySensibilityOnRightStick(bool value){ m_ApplySensibilityOnRightStick = value; }

			bool IsInputActionActive(const std::string& aName);

			float GetInputActionValue(const std::string& aName);

		private:
			const CInputManager& m_InputManager;	
			
			unsigned char GetKeyCode(const std::string& str);
			
			const char* m_XmlName;
			float m_MouseAxisSensibility, m_GamepadAxisSensibility;
			bool m_ApplySensibilityOnLeftStick, m_ApplySensibilityOnRightStick;
		};
	}
}
#endif