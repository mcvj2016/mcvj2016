#include "ActionManager.h"
#include <filesystem>
#include "XML/tinyxml2/tinyxml2.h"
#include "XML/XML.h"
#include <cassert>
#include "Utils/Defines.h"

engine::input::ActionTrigger::TriggerType engine::input::ActionTrigger::GetTriggerTypeFromString(const char* str, TriggerType defaultValue)
{
	if (str == nullptr)
		return defaultValue;
	if (strcmp(str, "KEYBOARD") == 0)
		return KEYBOARD;
	if (strcmp(str, "MOUSE") == 0)
		return MOUSE;
	if (strcmp(str, "MOUSE_BUTTON") == 0)
		return MOUSE_BUTTON;
	if (strcmp(str, "GAMEPAD") == 0)
		return GAMEPAD;
	if (strcmp(str, "GAMEPAD_BUTTON") == 0)
		return GAMEPAD_BUTTON;
			
	return static_cast<TriggerType>(-1);
}

engine::input::ActionTrigger::ButtonActionType engine::input::ActionTrigger::GetButtonActionTypeFromString(const char* str, ButtonActionType defaultValue)
{
	ButtonActionType result = defaultValue;

	if (strcmp(str, "IS_RELEASED") == 0){ result = IsReleased; }
	if (strcmp(str, "IS_PRESSED") == 0){ result = IsPressed; }
	if (strcmp(str, "BECOMES_PRESSED") == 0){ result = BecomesPressed; }
	if (strcmp(str, "BECOMES_RELEASED") == 0){ result = BecomesReleased; }

	return result;
}

engine::input::CActionManager::CActionManager(const CInputManager& _InputManager)
	: m_InputManager(_InputManager),
	  m_XmlName("data/common/actions.xml"), m_MouseAxisSensibility(0.5f), m_GamepadAxisSensibility(2.0f), m_ApplySensibilityOnLeftStick(false), m_ApplySensibilityOnRightStick(true)
{
}

void engine::input::CActionManager::Update()
{
	//reset de estado a inactivo de acciones actuales
	for (auto &actionIt : m_ResourcesMap)
	{
		InputAction *action = actionIt.second;
		action->active = false;
		action->value = 0;

		for (ActionTrigger& trigger : action->triggers)
		{
			switch (trigger.type)
			{
			case ActionTrigger::KEYBOARD:
				switch (trigger.keyboard.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (m_InputManager.KeyBecomesPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (m_InputManager.KeyBecomesReleased(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				default:
					assert(false);
					break;
				}
				break;
			case ActionTrigger::MOUSE:
			{
				float value = static_cast<float>(m_InputManager.GetMouseAxis(trigger.mouse.axis));
				bool active = trigger.mouse.geThreshold ? (fabs(value) >= trigger.mouse.threshold) : (fabs(value) <= trigger.mouse.threshold);
				if (fabs(value) >= fabs(action->value))
				{
					if (active || !action->active)
					{
						action->value = value * m_MouseAxisSensibility;
						action->active = active;
					}
				}
				break;
			}
			case ActionTrigger::GAMEPAD:
			{
				float value = static_cast<float>(m_InputManager.GetGamepadtAxis(trigger.gamepad.axis));
				bool active = trigger.gamepad.geThreshold ? (fabs(value) >= trigger.gamepad.threshold) : (fabs(value) <= trigger.gamepad.threshold);
				if (fabs(value) >= fabs(action->value))
				{
					if (active || !action->active)
					{
						if (m_ApplySensibilityOnLeftStick && 
							(trigger.gamepad.axis == InputDefinitions::LEFT_THUMB_X || trigger.gamepad.axis == InputDefinitions::LEFT_THUMB_Y))
						{
							action->value = value * m_GamepadAxisSensibility;
						}
						if (m_ApplySensibilityOnRightStick &&
							(trigger.gamepad.axis == InputDefinitions::RIGHT_THUMB_X || trigger.gamepad.axis == InputDefinitions::RIGHT_THUMB_Y))
						{
							action->value = value * m_GamepadAxisSensibility;
						}
						else
						{
							action->value = value;
						}
						
						action->active = active;
					}
				}
				break;
			}
			case ActionTrigger::MOUSE_BUTTON:
				switch (trigger.mouseButton.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsMouseButtonPressed(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsMouseButtonPressed(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (m_InputManager.MouseButtonBecomesPressed(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (m_InputManager.MouseButtonBecomesReleased(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				default:
					assert(false);
					break;
				}
				break;
			case ActionTrigger::GAMEPAD_BUTTON:
				switch (trigger.gamepadButton.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsGamepadButtonPressed(trigger.gamepadButton.button)){
						action->active = true;
						action->value = trigger.gamepadButton.value;
					}
					break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsGamepadButtonPressed(trigger.gamepadButton.button)){
						action->active = true;
						action->value = trigger.gamepadButton.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (m_InputManager.GamepadButtonBecomesPressed(trigger.gamepadButton.button))
					{
						action->active = true;
						action->value = trigger.gamepadButton.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (m_InputManager.GamepadButtonBecomesReleased(trigger.gamepadButton.button))
					{
						action->active = true;
						action->value = trigger.gamepadButton.value;
					}
					break;
				default: break;
				}
				break;
			default:
				assert(false);
				break;
			}
		}
	}
}

bool engine::input::CActionManager::HasGamePad()
{
	return m_InputManager.HasGamepad();
}

void engine::input::CActionManager::LoadXml()
{
	//doc object
	tinyxml2::XMLDocument xmldoc;

	//Load xml
	bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_XmlName));


	if (!loaded)
	{
		throw "Error Loading Materials XML";
	}

	const CXMLElement* actions = xmldoc.FirstChildElement("actions");
	//configure mouse/gamepad sensibilities
	const CXMLElement* sensibilities = xmldoc.FirstChildElement("sensibilities");
	if (sensibilities)
	{
		const CXMLElement* gamepad = sensibilities->FirstChildElement("gamepad_sensibility");
		assert(gamepad);
		this->m_GamepadAxisSensibility = gamepad->GetAttribute<float>("value", 2.0f);
		const CXMLElement* leftAxis = gamepad->FirstChildElement("left_stick");
		const CXMLElement* rightAxis = gamepad->FirstChildElement("right_stick");
		assert(leftAxis && rightAxis);
		this->m_ApplySensibilityOnLeftStick = leftAxis->GetAttribute<bool>("apply", false);
		this->m_ApplySensibilityOnRightStick = rightAxis->GetAttribute<bool>("apply", false);
		const CXMLElement* mouse = sensibilities->FirstChildElement("mouse_sensibility");
		assert(mouse);
		this->m_MouseAxisSensibility = mouse->GetAttribute<float>("value", 0.5f);
	}

	if (actions)
	{
		const CXMLElement* action = actions->FirstChildElement();
		InputAction actionContainer;

		while (action != nullptr)
		{
			//guardamos atributo nombre
			const std::string& actionName = action->GetAttribute<std::string>("name", "");
			assert(actionName != "");

			//recogemos elementos trigger
			const CXMLElement* trigger = action->FirstChildElement();
			while (trigger != nullptr)
			{
				ActionTrigger triggerContainer;

				//recogemos atributos
				const std::string& triggerType = trigger->GetAttribute<std::string>("type", "");
				const std::string& buttonType = trigger->GetAttribute<std::string>("action_type", "");
				float triggerValue = trigger->GetAttribute<float>("value", .0f);

				//tipo de accion
				ActionTrigger::TriggerType type = ActionTrigger::GetTriggerTypeFromString(triggerType.c_str());
				assert(type != -1);

				//componemos los triggers
				triggerContainer.type = type;

				switch (type)
				{
					case ActionTrigger::KEYBOARD:
					{
						ActionTrigger::ButtonActionType buttonActionType = ActionTrigger::GetButtonActionTypeFromString(buttonType.c_str());
						assert(buttonActionType != -1);
						//usamos funcion GetKeyCode para codificar correctamente el caracter del xml
						triggerContainer.keyboard.keyCode = GetKeyCode(trigger->GetAttribute<std::string>("key_code", ""));
						triggerContainer.keyboard.actionType = buttonActionType;
						triggerContainer.keyboard.value = triggerValue;

						break;

					}
					case ActionTrigger::MOUSE_BUTTON:
					{
						ActionTrigger::ButtonActionType buttonActionType = ActionTrigger::GetButtonActionTypeFromString(buttonType.c_str());
						assert(buttonActionType != -1);
						const std::string& buttonName = trigger->GetAttribute<std::string>("button", "");
						triggerContainer.mouseButton.button = InputDefinitions::GetMouseButtonFromString(buttonName.c_str());
						assert(triggerContainer.mouseButton.button != -1);
						triggerContainer.mouseButton.actionType = buttonActionType;
						if (triggerValue != 0)
						{
							triggerContainer.mouseButton.value = triggerValue;
						}
						break;
					}
					case ActionTrigger::GAMEPAD_BUTTON:
					{
						ActionTrigger::ButtonActionType buttonActionType = ActionTrigger::GetButtonActionTypeFromString(buttonType.c_str());
						assert(buttonActionType != -1);
						const std::string& buttonName = trigger->GetAttribute<std::string>("button", "");
						triggerContainer.gamepadButton.button = InputDefinitions::GetGamepadButtonFromString(buttonName.c_str());
						assert(triggerContainer.gamepadButton.button != -1);
						triggerContainer.gamepadButton.actionType = buttonActionType;
						if (triggerValue != 0)
						{
							triggerContainer.gamepadButton.value = triggerValue;
						}
						break;
					}
					case ActionTrigger::MOUSE:
					{
						const std::string& mouseAxis = trigger->GetAttribute<std::string>("axis", "");
						triggerContainer.mouse.axis = InputDefinitions::GetMouseAxisFromString(mouseAxis.c_str());
						assert(triggerContainer.mouse.axis != -1);
						triggerContainer.mouse.geThreshold = trigger->GetAttribute<bool>("geThreshold", false);
						if (triggerValue != 0)
						{
							triggerContainer.mouse.threshold = triggerValue;
						}
						break;
					}
					case ActionTrigger::GAMEPAD:
					{
						const std::string& gamepadAxis = trigger->GetAttribute<std::string>("axis", "");
						triggerContainer.gamepad.axis = InputDefinitions::GetGamepadAxisFromString(gamepadAxis.c_str());
						assert(triggerContainer.gamepad.axis != -1);
						triggerContainer.gamepad.geThreshold = trigger->GetAttribute<bool>("geThreshold", false);
						if (triggerValue != 0)
						{
							triggerContainer.gamepad.threshold = triggerValue;
						}
						break;
					}
				}

				//anyadimos el trigger al array de acciones
				actionContainer.triggers.push_back(triggerContainer);

				//siguiente trigger
				trigger = trigger->NextSiblingElement();
			}

			//anyadimos la accion al array de acciones
			CTemplatedMap<InputAction>::Update(actionName, new InputAction(actionContainer));

			actionContainer.triggers.clear();

			//siguiente elemento
			action = action->NextSiblingElement();
		}
	}
}

bool engine::input::CActionManager::IsInputActionActive(const std::string& aName)
{
	return this->operator()(aName)->active;
}

float engine::input::CActionManager::GetInputActionValue(const std::string& aName)
{
	return this->operator()(aName)->value;
}

unsigned char engine::input::CActionManager::GetKeyCode(const std::string& str)
{
	if (str == "&")
	{
		return VK_UP;
	}
	else if (str == "'")
	{
		return VK_RIGHT;
	}
	else if (str == "(")
	{
		return VK_DOWN;
	}
	else if (str == "%")
	{
		return VK_LEFT;
	}
	if (str.length() == 1 && ((str[0] >= 'A' && str[0] <= 'Z') || (str[0] >= '0' && str[0] <= '9')))
	{
		return str[0];
	}
	else if (str == "SPACE" || str == " ")
	{
		return VK_SPACE;
	}
	else if (str == "ESC")
	{
		return VK_ESCAPE;
	}
	else if (str == "F1")
	{
		return VK_F1;
	}
	else if (str == "SHIFT")
	{
		return VK_SHIFT;
	}
	else if (str == "CTRL")
	{
		return VK_CONTROL;
	}
	else
	{
		assert(false);
		return -1;
	}
}
