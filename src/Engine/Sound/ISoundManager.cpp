#include "ISoundManager.h"
#include "SoundManager.h"

namespace engine
{
	namespace sound
	{

		ISoundManager::~ISoundManager()
		{
		}
		ISoundManager *ISoundManager::InstantiateSoundManager()
		{
			return new CSoundManager();
		}
	}
}