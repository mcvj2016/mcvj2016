#include "SoundManager.h"
#include <cassert>
#include "Math/Vector3.h"
#include "Graphics/Scenes/SceneNode.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Camera/CameraManager.h"
#include "Utils/Logger/Logger.h"
#include "Helper/ImguiHelper.h"
#include "Utils/Coroutine.h"
#include <Wwise_IDs.h>

namespace AK
{
#ifdef WIN32
	void * AllocHook(size_t in_size)
	{
		return malloc(in_size);
	}
	void FreeHook(void * in_ptr)
	{
		free(in_ptr);
	}
	void * VirtualAllocHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dAllocationType,
		DWORD in_dwProtect
		)
	{
		return VirtualAlloc(in_pMemAddress, in_size, in_dAllocationType, in_dwProtect);
	}

	void VirtualFreeHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dwFreeType
		)
	{
		VirtualFree(in_pMemAddress, in_size, in_dwFreeType);
	}
#endif
}


namespace engine
{
	namespace sound
	{
		CSoundManager::CSoundManager(): 
			g_lowLevelIO(nullptr), 
			m_LastGameObjectID(0), 
			m_DefaultSpeakerId(0),
			m_Filename("sound.xml")
		{
			m_Path = "data/common/sound/";
		}

		CSoundManager::~CSoundManager()
		{
			CSoundManager::Terminate();
			m_FreeObjectsIDs.clear();
			m_NamedSpeakers.clear();
			m_GameObjectSpeakers.clear();
		}

		void CSoundManager::SetPath(const std::string &path)
		{
			m_Path = path;
		}

		bool CSoundManager::Init()
		{
		#if 1
			// Initialize audio engine
			// Memory.
			AkMemSettings memSettings;

			memSettings.uMaxNumPools = 20;

			// Streaming.
			AkStreamMgrSettings stmSettings;
			AK::StreamMgr::GetDefaultSettings(stmSettings);

			AkDeviceSettings deviceSettings;
			AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

			AkInitSettings l_InitSettings;
			AkPlatformInitSettings l_platInitSetings;
			AK::SoundEngine::GetDefaultInitSettings(l_InitSettings);
			AK::SoundEngine::GetDefaultPlatformInitSettings(l_platInitSetings);

			// Setting pool sizes for this game. Here, allow for user content; every game should determine its own optimal values.
			l_InitSettings.uDefaultPoolSize = 2 * 1024 * 1024;
			l_platInitSetings.uLEngineDefaultPoolSize = 4 * 1024 * 1024;

			AkMusicSettings musicInit;
			AK::MusicEngine::GetDefaultInitSettings(musicInit);

			AKRESULT eResult = AK::SOUNDENGINE_DLL::Init(&memSettings,
				&stmSettings,
				&deviceSettings,
				&l_InitSettings,
				&l_platInitSetings,
				&musicInit);

			if (eResult != AK_Success)
			{
				// Then, we will run the game without sound
				AK::SOUNDENGINE_DLL::Term();
				return false;
			}

			AkOSChar *akPath;
			CONVERT_CHAR_TO_OSCHAR(m_Path.c_str(), akPath);
			// load initialization and main soundbanks
			AK::SOUNDENGINE_DLL::SetBasePath(akPath);
			AK::StreamMgr::SetCurrentLanguage(L"English(US)");

			//load the default wwise sound config bank
			if(!LoadSoundBank(AK::BANKS::INIT))
			{
				LOG_ERROR_APPLICATION("Error al cargar Init.bnk", "");
				return false;
			}
			LOG_INFO_APPLICATION("Se ha cargado Init.bnk", "");
#else
			//
			// Create and initialize an instance of the default memory manager. Note
			// that you can override the default memory manager with your own. Refer
			// to the SDK documentation for more information.
			//

			AkMemSettings memSettings;
			memSettings.uMaxNumPools = 20;

			if (AK::MemoryMgr::Init(&memSettings) != AK_Success)
			{
				assert(!"Could not create the memory manager.");
				return false;
			}

			//
			// Create and initialize an instance of the default streaming manager. Note
			// that you can override the default streaming manager with your own. Refer
			// to the SDK documentation for more information.
			//

			AkStreamMgrSettings stmSettings;
			AK::StreamMgr::GetDefaultSettings(stmSettings);

			// Customize the Stream Manager settings here.

			if (!AK::StreamMgr::Create(stmSettings))
			{
				assert(!"Could not create the Streaming Manager");
				return false;
			}

			//
			// Create a streaming device with blocking low-level I/O handshaking.
			// Note that you can override the default low-level I/O module with your own. Refer
			// to the SDK documentation for more information.
			//
			AkDeviceSettings deviceSettings;
			AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

			// Customize the streaming device settings here.

			// CAkFilePackageLowLevelIOBlocking::Init() creates a streaming device
			// in the Stream Manager, and registers itself as the File Location Resolver.
			if (g_lowLevelIO->Init(deviceSettings) != AK_Success)
			{
				assert(!"Could not create the streaming device and Low-Level I/O system");
				return false;
			}

			//
			// Create the Sound Engine
			// Using default initialization parameters
			//

			AkInitSettings initSettings;
			AkPlatformInitSettings platformInitSettings;
			AK::SoundEngine::GetDefaultInitSettings(initSettings);
			AK::SoundEngine::GetDefaultPlatformInitSettings(platformInitSettings);

			if (AK::SoundEngine::Init(&initSettings, &platformInitSettings) != AK_Success)
			{
				assert(!"Could not initialize the Sound Engine.");
				return false;
			}

			//
			// Initialize the music engine
			// Using default initialization parameters
			//

			AkMusicSettings musicInit;
			AK::MusicEngine::GetDefaultInitSettings(musicInit);

			if (AK::MusicEngine::Init(&musicInit) != AK_Success)
			{
				assert(!"Could not initialize the Music Engine.");
				return false;
			}
#endif
			return true;
		}

		void CSoundManager::SetListenerPosition(CCameraController *camera)
		{
			AkListenerPosition l_ListenerPosition = {};
			AkVector AkPos;
			AkVector AkOrFront;

			CSceneNode* listener = camera->GetSceneCamera();
			if (listener != nullptr)
			{
				Vect3f lPos = camera->GetSceneCamera()->GetPosition();

				AkPos.X = lPos.x;
				AkPos.Y = lPos.y;
				AkPos.Z = lPos.z;

				Vect3f lOrientation = (camera->GetLookAt() - camera->GetSceneCamera()->GetPosition()).Normalize();
				lOrientation *= -1;

				AkOrFront.X = lOrientation.x;
				AkOrFront.Y = lOrientation.y;
				AkOrFront.Z = lOrientation.z;
				AkVector AkOrTop;
				AkOrTop.X = camera->GetSceneCamera()->GetUp().x;
				AkOrTop.Y = camera->GetSceneCamera()->GetUp().y;
				AkOrTop.Z = camera->GetSceneCamera()->GetUp().z;

				l_ListenerPosition.Set(AkPos, AkOrFront, AkOrTop);
			}
			else
			{
				AkPos.X = 0;
				AkPos.Y = 0;
				AkPos.Z = 0;
				AkOrFront.X = 1;
				AkOrFront.Y = 0;
				AkOrFront.Z = 0;
				AkVector AkOrTop;
				AkOrTop.X = 0;
				AkOrTop.Y = 1;
				AkOrTop.Z = 0;

				l_ListenerPosition.Set(AkPos, AkOrFront, AkOrTop);
			}
			
			AKRESULT r = AK::SoundEngine::SetListenerPosition(l_ListenerPosition);
			assert(r == AKRESULT::AK_Success);
		}

		void CSoundManager::UpdateSpeakersPositions()
		{
			for (auto it : m_GameObjectSpeakers)
			{
				AkSoundPosition l_SoundPosition = {};

				Vect3f lPos = it.first->GetPosition();
				AkVector AkPos;
				AkPos.X = lPos.x;
				AkPos.Y = lPos.y;
				AkPos.Z = lPos.z;

				Vect3f lOrientation = it.first->GetForward();
				AkVector AkOrFront;
				AkOrFront.X = lOrientation.x;
				AkOrFront.Y = lOrientation.y;
				AkOrFront.Z = lOrientation.z;
				AkVector AkOrTop;
				AkOrTop.X = it.first->GetUp().x;
				AkOrTop.Y = it.first->GetUp().y;
				AkOrTop.Z = it.first->GetUp().z;

				l_SoundPosition.Set(AkPos, AkOrFront, AkOrTop);
				AKRESULT r = AK::SoundEngine::SetPosition(it.second, l_SoundPosition);
				assert(r == AKRESULT::AK_Success);
			}
		}
		
		void CSoundManager::Update()
		{
			//update dynamic speaker positions
			UpdateSpeakersPositions();
			//update camera listener position
			SetListenerPosition(CEngine::GetInstance().GetCameraManager().GetMainCamera());
			//tell sound to make a tick update
			AK::SOUNDENGINE_DLL::Tick();
		}

		bool CSoundManager::Load()
		{
			bool lOk = true;

			lOk = LoadSoundBanksXML();
			lOk &= LoadSpeakersXML();

			return lOk;
		}

		bool CSoundManager::Reload()
		{
			Clean();
			return Load();
		}

		bool CSoundManager::LoadSoundBank(const std::string &bank)
		{
			AkBankID bankID;
			AKRESULT retVal = AK::SoundEngine::LoadBank(bank.c_str(), AK_DEFAULT_POOL_ID, bankID);
			if (retVal != AK_Success)
			{
				return false;
			}
			return true;
		}

		bool CSoundManager::LoadSoundBank(const unsigned long& AkBankId)
		{
			AKRESULT retVal = AK::SoundEngine::LoadBank(AkBankId, AK_DEFAULT_POOL_ID);
			if (retVal != AK_Success)
			{
				return false;
			}
			return true;
		}

		bool CSoundManager::UnloadSoundBank(const std::string &bank)
		{
			AKRESULT retValue = AK::SoundEngine::UnloadBank(bank.c_str(), nullptr);
			if (retValue != AK_Success)
			{
				return false;
			}
			return true;
		}

		bool CSoundManager::UnloadSoundBank(const unsigned long& AkBankId)
		{
			AKRESULT retValue = AK::SoundEngine::UnloadBank(AkBankId, nullptr);
			if (retValue != AK_Success)
			{
				return false;
			}
			return true;
		}

		void CSoundManager::RegisterSpeaker(scenes::CSceneNode* _speaker)
		{
			if (m_GameObjectSpeakers.find(_speaker) != m_GameObjectSpeakers.end())
				UnregisterSpeaker(_speaker);

			AkGameObjectID id = GenerateObjectId();
			m_GameObjectSpeakers[_speaker] = id;

			AkSoundPosition l_SoundPosition = {};

			Vect3f lPos = _speaker->GetPosition();
			AkVector AkPos;
			AkPos.X = lPos.x;
			AkPos.Y = lPos.y;
			AkPos.Z = lPos.z;

			Vect3f lOrientation = _speaker->GetForward();
			AkVector AkOrFront;
			AkOrFront.X = lOrientation.x;
			AkOrFront.Y = lOrientation.y;
			AkOrFront.Z = lOrientation.z;
			AkVector AkOrTop; 
			AkOrTop.X = _speaker->GetUp().x;
			AkOrTop.Y = _speaker->GetUp().y;
			AkOrTop.Z = _speaker->GetUp().z;

			l_SoundPosition.Set(AkPos, AkOrFront, AkOrTop);
			AK::SoundEngine::RegisterGameObj(id);
			AKRESULT r = AK::SoundEngine::SetPosition(id, l_SoundPosition);
			assert(r == AKRESULT::AK_Success);
		}

		void CSoundManager::UnregisterSpeaker(const scenes::CSceneNode* _speaker)
		{
			auto it = m_GameObjectSpeakers.find(_speaker);
			if (it != m_GameObjectSpeakers.end())
			{
				AK::SoundEngine::UnregisterGameObj(it->second);

				m_FreeObjectsIDs.push_back(it->second);

				m_GameObjectSpeakers.erase(it);
			}
			else
			{
				assert(false);
			}
		}

		void CSoundManager::PlayEvent(const AkUniqueID &_AkEventID) const
		{
			PlayEvent(_AkEventID, m_DefaultSpeakerId);
		}
		void CSoundManager::PlayEvent(const SoundEvent &_event)
		{
			PlayEvent(_event, m_DefaultSpeakerId);
		}
		void CSoundManager::PlayEvent(const unsigned long& _AkEventID)
		{
			PlayEvent(_AkEventID, m_DefaultSpeakerId);
		}
		void CSoundManager::PlayEvent(const unsigned long& _AkEventID, const std::string& _speaker)
		{
			auto it = m_NamedSpeakers.find(_speaker);
			if (it != m_NamedSpeakers.end())
			{
				PlayEvent(_AkEventID, it->second);
			}
			else
			{
				assert(false);
			}
		}
		void CSoundManager::PlayEvent(const SoundEvent &_event, const AkGameObjectID &id) const
		{
			AK::SoundEngine::PostEvent(_event.eventName.c_str(), id);
		}
		void CSoundManager::PlayEvent(const AkUniqueID &_AkEventID, const AkGameObjectID &id) const
		{
			AK::SoundEngine::PostEvent(_AkEventID, id);
		}
		void CSoundManager::PlayEvent(const SoundEvent &_event, const std::string &_speaker)
		{
			auto it = m_NamedSpeakers.find(_speaker);
			if (it != m_NamedSpeakers.end())
			{
				PlayEvent(_event, it->second);
			}
			else
			{
				assert(false);
			}
		}
		void CSoundManager::PlayEvent(const SoundEvent &_event, const scenes::CSceneNode* _speaker)
		{
			auto it = m_GameObjectSpeakers.find(_speaker);
			if (it != m_GameObjectSpeakers.end())
			{
				PlayEvent(_event, it->second);
			}
			else
			{
				assert(false);
			}
			
		}

		void CSoundManager::PlayEvent(const unsigned long& _AkEventID, const scenes::CSceneNode* _speaker)
		{
			if (m_GameObjectSpeakers.find(_speaker) != m_GameObjectSpeakers.end())
			{
				PlayEvent(_AkEventID, m_GameObjectSpeakers[_speaker]);
			}
			else
			{
				CEngine::GetInstance().GetImGUI().Log("[Warning] dynamic speaker not found: " + _speaker->GetName());
			}
		}

		void CSoundManager::SetSwitch(const SoundSwitchValue& switchValue, const AkGameObjectID& id) const
		{
			AKRESULT res = AK::SoundEngine::SetSwitch(switchValue.soundSwitch.switchName.c_str(), switchValue.valueName.c_str(), id);
			assert(res == AK_Success);
		}
		void CSoundManager::SetSwitch(const SoundSwitchValue &switchValue)
		{
			SetSwitch(switchValue, m_DefaultSpeakerId);
		}

		void CSoundManager::SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState)
		{
			AKRESULT res = AK::SoundEngine::SetSwitch(switchGroup, switchState, m_DefaultSpeakerId);
			
			assert(res == AK_Success);
		}

		void CSoundManager::SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState, const scenes::CSceneNode* _speaker)
		{
			AKRESULT res = AK_Fail;
			if (m_GameObjectSpeakers.find(_speaker) != m_GameObjectSpeakers.end())
			{
				res = AK::SoundEngine::SetSwitch(switchGroup, switchState, m_GameObjectSpeakers[_speaker]);
			}
			else
			{
				CEngine::GetInstance().GetImGUI().Log("[Warning] dynamic speaker not found: " + _speaker->GetName());
			}
			assert(res == AK_Success);
		}

		void CSoundManager::SetSwitch(const SoundSwitchValue &switchValue, const std::string &_speaker)
		{
			auto it = m_NamedSpeakers.find(_speaker);
			if (it != m_NamedSpeakers.end())
			{
				SetSwitch(switchValue, it->second);
			}
			else
			{
				assert(false);
			}
		}
		void CSoundManager::SetSwitch(const SoundSwitchValue &switchValue, const scenes::CSceneNode* _speaker)
		{
			auto it = m_GameObjectSpeakers.find(_speaker);
			if (it != m_GameObjectSpeakers.end())
			{
				SetSwitch(switchValue, it->second);
			}
			else
			{
				assert(false);
			}
		}

		void CSoundManager::SetRTPCValue(const SoundRTPC& _rtpc, float value, const AkGameObjectID& id)
		{
			AKRESULT res = AK::SoundEngine::SetRTPCValue(_rtpc.RTPCName.c_str(), static_cast<AkRtpcValue>(value), id);
			assert(res);
		}
		void CSoundManager::BroadcastRTPCValue(const SoundRTPC &_rtpc, float value)
		{
			AKRESULT res = AK::SoundEngine::SetRTPCValue(_rtpc.RTPCName.c_str(), static_cast<AkRtpcValue>(value));
			assert(res == AK_Success);
		}
		void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value)
		{
			SetRTPCValue(_rtpc, value, m_DefaultSpeakerId);
		}
		void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value, const std::string &_speaker)
		{
			auto it = m_NamedSpeakers.find(_speaker);
			if (it != m_NamedSpeakers.end())
			{
				SetRTPCValue(_rtpc, value, it->second);
			}
			else
			{
				assert(false);
			}
		}
		void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value, const scenes::CSceneNode* _speaker)
		{
			auto it = m_GameObjectSpeakers.find(_speaker);
			if (it != m_GameObjectSpeakers.end())
			{
				SetRTPCValue(_rtpc, value, it->second);
			}
			else
			{
				assert(false);
			}
		}

		void CSoundManager::BroadcastState(const SoundStateValue &_state)
		{
			AKRESULT res = AK::SoundEngine::SetState(_state.soundState.stateName.c_str(), _state.valueName.c_str());
			assert(res == AK_Success);
		}

		void CSoundManager::SetState(const unsigned long& switchGroup, const unsigned long& switchState)
		{
			AKRESULT res = AK::SoundEngine::SetState(switchGroup, switchState);
			assert(res == AK_Success);
		}

		AkGameObjectID CSoundManager::GenerateObjectId()
		{
			AkGameObjectID result;

			if (m_FreeObjectsIDs.size() > 0)
			{
				result = m_FreeObjectsIDs.back();
				m_FreeObjectsIDs.pop_back();
			}
			else
			{
				result = ++m_LastGameObjectID;
			}

			return result;
		}

		bool CSoundManager::LoadSoundBanksXML()
		{
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(std::string(m_Path + "../" +m_Filename).c_str()));
			assert(loaded);

			CXMLElement* sounds = xmldoc.FirstChildElement("sounds");
			assert(sounds != nullptr);

			CXMLElement* banks = sounds->FirstChildElement("banks");
			assert(banks != nullptr);

			CXMLElement* bank = banks->FirstChildElement("bank");

			while (bank != nullptr)
			{
				std::string name = bank->GetAttribute<std::string>("name", "");
				assert(name != "");

				assert(LoadSoundBank(name));

				LOG_INFO_APPLICATION(std::string("Loaded bank " + name).c_str(), "");

				bank = bank->NextSiblingElement("bank");
			}
			return true;
		}

		bool CSoundManager::LoadSpeakersXML()
		{
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(std::string(m_Path + "../" + m_Filename).c_str()));
			assert(loaded);

			CXMLElement* sounds = xmldoc.FirstChildElement("sounds");
			assert(sounds != nullptr);

			CXMLElement* speakers = sounds->FirstChildElement("NamedSpeakers");
			assert(speakers != nullptr);

			//speakers2D (attached to maincamera allways)
			CXMLElement* speakers2D = speakers->FirstChildElement("Speakers2D");
			assert(speakers2D != nullptr);

			CXMLElement* speaker2D = speakers2D->FirstChildElement("Speaker2D");

			while (speaker2D != nullptr)
			{
				std::string name = speaker2D->GetAttribute<std::string>("name", "");
				assert(name != "");

				AkSoundPosition l_SoundPosition = {};

				AkVector AkPos;
				AkPos.X = 0;
				AkPos.Y = 0;
				AkPos.Z = 0;

				AkVector AkOrFront;
				AkOrFront.X = 1;
				AkOrFront.Y = 0;
				AkOrFront.Z = 0;
				AkVector AkOrTop;
				AkOrTop.X = 0;
				AkOrTop.Y = 1;
				AkOrTop.Z = 0;

				l_SoundPosition.Set(AkPos, AkOrFront, AkOrTop);

				AkGameObjectID id = GenerateObjectId();

				if (speaker2D->GetAttribute<bool>("default", false))
				{
					m_DefaultSpeakerId = id;
				}

				m_NamedSpeakers[name] = id;
				AK::SoundEngine::RegisterGameObj(id);
				AK::SoundEngine::SetPosition(id, l_SoundPosition);

				LOG_INFO_APPLICATION(std::string("Registered speaker2D " + name).c_str(), "");

				speaker2D = speaker2D->NextSiblingElement("Speaker2D");
			}

			//speakers3D (attached to SceneNodes)
			CXMLElement* speakers3D = speakers->FirstChildElement("NamedSpeakers3D");
			assert(speakers3D != nullptr);

			CXMLElement* speaker3D = speakers3D->FirstChildElement("NamedSpeaker3D");

			while (speaker3D != nullptr)
			{
				std::string name = speaker3D->GetAttribute<std::string>("name", "");
				assert(name != "");

				Vect3f lPos = speaker3D->GetAttribute<Vect3f>("position", v3fZERO);
				Vect3f lOrientation = speaker3D->GetAttribute<Vect3f>("direction", v3fZERO);
				AkSoundPosition l_SoundPosition = {};

				AkVector AkPos;
				AkPos.X = lPos.x;
				AkPos.Y = lPos.y;
				AkPos.Z = lPos.z;

				AkVector AkOrFront;
				AkOrFront.X = lOrientation.x;
				AkOrFront.Y = lOrientation.y;
				AkOrFront.Z = lOrientation.z;
				AkVector AkOrTop;
				AkOrTop.X = 0;
				AkOrTop.Y = 1;
				AkOrTop.Z = 0;

				l_SoundPosition.SetPosition(AkPos);
				l_SoundPosition.SetOrientation(AkOrFront, AkOrTop);

				AkGameObjectID id = GenerateObjectId();

				if (speaker3D->GetAttribute<bool>("default", false))
				{
					m_DefaultSpeakerId = id;
				}

				m_NamedSpeakers[name] = id;
				AK::SoundEngine::RegisterGameObj(id);
				AK::SoundEngine::SetPosition(id, l_SoundPosition);

				LOG_INFO_APPLICATION(std::string("Registered speaker3D " + name).c_str(), "");

				speaker3D = speaker3D->NextSiblingElement("NamedSpeaker3D");
			}

			return true;
		}

		void CSoundManager::Terminate()
		{
			AK::SoundEngine::ClearBanks();
			AK::SoundEngine::UnregisterAllGameObj();
			AK::SOUNDENGINE_DLL::Term();
		}

		void CSoundManager::Clean()
		{
			AK::SoundEngine::ClearBanks();

			for (auto it : m_NamedSpeakers)
			{
				AK::SoundEngine::UnregisterGameObj(it.second);
				m_FreeObjectsIDs.push_back(it.second);
			}
			for (auto it : m_GameObjectSpeakers)
			{
				AK::SoundEngine::UnregisterGameObj(it.second);
				m_FreeObjectsIDs.push_back(it.second);
			}
			m_NamedSpeakers.clear();
			m_GameObjectSpeakers.clear();
		}
	}
}
