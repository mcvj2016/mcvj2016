#ifndef __H_ISOUNDMANAGER__20170516
#define __H_ISOUNDMANAGER__20170516

#include <string>

class CCameraController;

namespace engine {
	namespace scenes {
		class CSceneNode;
	}
}

struct SoundEvent
{
	std::string eventName;
};

struct SoundSwitch
{
	std::string switchName;
};

struct SoundSwitchValue
{
	SoundSwitch soundSwitch;
	std::string valueName;
};

struct SoundRTPC
{
	std::string RTPCName;
};

struct SoundState
{
	std::string stateName;
};

struct SoundStateValue
{
	SoundState soundState;
	std::string valueName;
};

namespace engine
{
	namespace sound
	{
		class ISoundManager
		{
		public:
			static ISoundManager* InstantiateSoundManager();
			virtual ~ISoundManager();

			virtual void SetPath(const std::string &path) = 0;

			virtual bool Init() = 0;
			virtual void Update() = 0;
			virtual bool Load() = 0;
			virtual bool Reload() = 0;

			virtual bool LoadSoundBank(const std::string &bank) = 0;
			virtual bool LoadSoundBank(const unsigned long &AkBankId) = 0;
			virtual bool UnloadSoundBank(const std::string &bank) = 0;
			virtual bool UnloadSoundBank(const unsigned long &AkBankId) = 0;

			virtual void RegisterSpeaker(scenes::CSceneNode* _speaker) = 0;
			virtual void UnregisterSpeaker(const scenes::CSceneNode* _speaker) = 0;

			virtual void PlayEvent(const SoundEvent &_event) = 0;
			virtual void PlayEvent(const unsigned long &_AkEventID) = 0;
			virtual void PlayEvent(const unsigned long &_AkEventID, const std::string &_speaker) = 0;
			virtual void PlayEvent(const SoundEvent &_event, const std::string &_speaker) = 0;
			virtual void PlayEvent(const SoundEvent &_event, const scenes::CSceneNode* _speaker) = 0;
			virtual void PlayEvent(const unsigned long &_AkEventID, const scenes::CSceneNode* _speaker) = 0;

			virtual void SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState) = 0;
			virtual void SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState, const scenes::CSceneNode* _speaker) = 0;
			virtual void SetSwitch(const SoundSwitchValue &switchValue) = 0;
			virtual void SetSwitch(const SoundSwitchValue &switchValue, const std::string &_speaker) = 0;
			virtual void SetSwitch(const SoundSwitchValue &switchValue, const scenes::CSceneNode* _speaker) = 0;

			virtual void BroadcastRTPCValue(const SoundRTPC &_rtpc, float value) = 0;
			virtual void SetRTPCValue(const SoundRTPC &_rtpc, float value) = 0;
			virtual void SetRTPCValue(const SoundRTPC &_rtpc, float value, const std::string &_speaker) = 0;
			virtual void SetRTPCValue(const SoundRTPC &_rtpc, float value, const scenes::CSceneNode* _speaker) = 0;

			virtual void BroadcastState(const SoundStateValue &_state) = 0;
			virtual void SetState(const unsigned long& switchGroup, const unsigned long& switchState) = 0;

			virtual void Terminate() = 0;
			virtual void Clean() = 0;
			virtual bool LoadSoundBanksXML() = 0;
			virtual bool LoadSpeakersXML() = 0;

		protected:
			ISoundManager() {}
			std::string m_Path;
		};
	}
}
#endif