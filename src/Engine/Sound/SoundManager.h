#ifndef __H_SOUNDMANAGER__20170516
#define __H_SOUNDMANAGER__20170516

#include "ISoundManager.h"
#include <vector>
#include <unordered_map>
#include <AK/SoundEngine/Common/AkMemoryMgr.h>                  // Memory Manager 
#include <AK/SoundEngine/Common/AkModule.h>                     // Default memory and stream managers 
#include <AK/SoundEngine/Common/IAkStreamMgr.h>                 // Streaming Manager 
#include <AK/Tools/Common/AkPlatformFuncs.h>                    // Thread defines 
#include <AkFilePackageLowLevelIOBlocking.h>                    // Sample low-level I/O implementation 
#include <AK/SoundEngine/Common/AkSoundEngine.h>                // Sound engine 
#include <AK/MusicEngine/Common/AkMusicEngine.h>                // Music Engine 
#include <AkSoundEngineDLL.h> 

#pragma comment(lib, "AkSoundEngineDLL.lib")

#ifdef UNICODE_WAS_UNDEFINED
#undef UNICODE
#endif

class CAkFilePackageLowLevelIOBlocking;
namespace engine
{
	namespace sound
	{
		class CSoundManager : public ISoundManager
		{
		public:

			CSoundManager();
			virtual ~CSoundManager();

			void SetPath(const std::string &path) override;

			bool Init() override;
			void SetListenerPosition(CCameraController* camera);
			void UpdateSpeakersPositions();
			void Update() override;
			bool Load() override;
			bool Reload() override;

			CAkFilePackageLowLevelIOBlocking* g_lowLevelIO;

			bool LoadSoundBank(const std::string &bank) override;
			bool LoadSoundBank(const unsigned long &AkBankId) override;
			bool UnloadSoundBank(const std::string &bank) override;
			bool UnloadSoundBank(const unsigned long &AkBankId) override;

			void RegisterSpeaker(scenes::CSceneNode* _speaker) override;
			void UnregisterSpeaker(const scenes::CSceneNode* _speaker) override;
			void PlayEvent(const AkUniqueID &_AkEventID) const;

			void PlayEvent(const SoundEvent &_event) override;
			void PlayEvent(const unsigned long &_AkEventID) override;
			void PlayEvent(const unsigned long &_AkEventID, const std::string &_speaker) override;
			void PlayEvent(const SoundEvent &_event, const std::string &_speaker) override;
			void PlayEvent(const SoundEvent &_event, const scenes::CSceneNode* _speaker) override;
			void PlayEvent(const unsigned long &_AkEventID, const scenes::CSceneNode* _speaker) override;

			void SetSwitch(const SoundSwitchValue &switchValue) override;
			void SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState) override;
			void SetSwitch(const unsigned long& switchGroup, const unsigned long& switchState, const scenes::CSceneNode* _speaker) override;
			void SetSwitch(const SoundSwitchValue &switchValue, const std::string &_speaker) override;
			void SetSwitch(const SoundSwitchValue &switchValue, const scenes::CSceneNode* _speaker) override;

			void BroadcastRTPCValue(const SoundRTPC &_rtpc, float value) override;
			void SetRTPCValue(const SoundRTPC &_rtpc, float value) override;
			void SetRTPCValue(const SoundRTPC &_rtpc, float value, const std::string &_speaker) override;
			void SetRTPCValue(const SoundRTPC &_rtpc, float value, const scenes::CSceneNode* _speaker) override;

			void BroadcastState(const SoundStateValue &_state) override;
			void SetState(const unsigned long& switchGroup, const unsigned long& switchState) override;

			virtual AkGameObjectID GenerateObjectId();

			bool LoadSoundBanksXML() override;
			bool LoadSpeakersXML() override;

			void Terminate() override;
			void Clean() override;

		private:
			AkGameObjectID m_LastGameObjectID;
			std::vector<AkGameObjectID> m_FreeObjectsIDs;
			AkGameObjectID m_DefaultSpeakerId;
			std::unordered_map<std::string, AkGameObjectID> m_NamedSpeakers;
			std::unordered_map<const scenes::CSceneNode*, AkGameObjectID> m_GameObjectSpeakers;
			std::string m_Filename;

			void PlayEvent(const SoundEvent &_event, const AkGameObjectID &id) const;
			void PlayEvent(const AkUniqueID& _AkEventID, const AkGameObjectID& id) const;
			void SetSwitch(const SoundSwitchValue &switchValue, const AkGameObjectID &id) const;
			void SetRTPCValue(const SoundRTPC &_rtpc, float value, const AkGameObjectID &id);
		};


	}
}

#endif // __H_SOUNDMANAGER__20170516