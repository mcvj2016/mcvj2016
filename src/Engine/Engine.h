#ifndef _ENGINE_ENGINE_DPVD1_27102016203657_H
#define _ENGINE_ENGINE_DPVD1_27102016203657_H
#include <chrono>
#include "Utils/Singleton.h"
#include <Windows.h>
#include "Utils/NoSillyWarningsPlease.h"

//forward declarations
class CPhysXManager;

namespace base
{
	namespace utils
	{
		class CCoroutine;
	}
}
namespace engine
{
	class CImguiHelper;
	namespace sound
	{
		class ISoundManager;
	}
	namespace buffers
	{
		class CConstantBufferManager;
	}
	namespace input
	{
		class CActionManager;
		class CInputManager;
	}
	namespace render
	{
		class CRenderManager;
		class CRenderPipelineManager;
	}
	namespace camera
	{
		class CCameraManager;
	}
	namespace cinematics
	{
		class CCinematicManager;
	}
	namespace cal3dimpl
	{
		class CAnimatedModelManager;
	}
	namespace materials
	{
		class CMaterialManager;
		class CTextureManager;
		class CMeshManager;
	}
	namespace scenes{
		class CSceneManager;
		class CPrefabManager;
	}
	namespace shaders
	{
		class CShaderManager;
	}
	namespace effects
	{
		class CEffectManager;
		class CTechniquePoolManager;
	}
	namespace lights
	{
		class CLightManager;
	}
	namespace particles
	{
		class CParticleManager;
	}
	namespace gui
	{
		class CGUIManager;
	}
}
namespace logic
{
	class CScriptManager;
}

namespace engine
{
	class CEngine : public base::utils::CSingleton<CEngine>
	{
	public:

		bool m_DebugMode = true;
		float m_totalTime;

		CEngine();
		virtual ~CEngine();

		void ProcessInputs();
		void Update();
		void Render();
		void Dispose();
		void Init(HWND& hWnd, int widht, int height);

		//CinematicManager
		void SetCinematicManager(engine::cinematics::CCinematicManager* aCinematicManager) { m_CinematicManager = aCinematicManager; }
		const engine::cinematics::CCinematicManager& GetCinematicManager() const { return *m_CinematicManager; }
		engine::cinematics::CCinematicManager& GetCinematicManager() { return *m_CinematicManager; }

		//lightmanager
		void SetLightManager(engine::lights::CLightManager* aLightManager) { m_LightManager = aLightManager; }
		const engine::lights::CLightManager& GetLightManager() const { return *m_LightManager; }
		engine::lights::CLightManager& GetLightManager() { return *m_LightManager; }

		//AnimatedModelManager
		void SetAnimatedModelManager(engine::cal3dimpl::CAnimatedModelManager* aAnimatedModelManager) { m_AnimatedModelManager = aAnimatedModelManager; }
		const engine::cal3dimpl::CAnimatedModelManager& GetAnimatedModelManager() const { return *m_AnimatedModelManager; }
		engine::cal3dimpl::CAnimatedModelManager& GetAnimatedModelManager() { return *m_AnimatedModelManager; }

		//materialmanager
		void SetMaterialManager(engine::materials::CMaterialManager* aMaterialManager) { m_MaterialManager = aMaterialManager; }
		const engine::materials::CMaterialManager& GetMaterialManager() const { return *m_MaterialManager; }
		engine::materials::CMaterialManager& GetMaterialManager() { return *m_MaterialManager; }

		//rendermanager
		void SetRenderManager(engine::render::CRenderManager* aRenderManager) { m_RenderManager = aRenderManager; }
		const engine::render::CRenderManager& GetRenderManager() const { return *m_RenderManager; }
		engine::render::CRenderManager& GetRenderManager() { return *m_RenderManager; }

		//action nmanager
		void SetActionManager(engine::input::CActionManager* aActionManager) { m_ActionManager = aActionManager; }
		const engine::input::CActionManager& GetActionManager() const { return *m_ActionManager; }
		engine::input::CActionManager& GetActionManager() { return *m_ActionManager; }

		//input nmanager
		void SetInputManager(engine::input::CInputManager* aInputManager) { m_InputManager = aInputManager; }
		const engine::input::CInputManager& GetInputManager() const { return *m_InputManager; }
		engine::input::CInputManager& GetInputManager() { return *m_InputManager; }

		//texture nmanager
		void SetTextureManager(engine::materials::CTextureManager* aTextureManager) { m_TextureManager = aTextureManager; }
		const engine::materials::CTextureManager& GetTextureManager() const { return *m_TextureManager; }
		engine::materials::CTextureManager& GetTextureManager() { return *m_TextureManager; }

		//ConstantBuffer Manager
		void SetConstantBufferManager(engine::buffers::CConstantBufferManager* aConstantBufferManager) { m_ConstantBufferManager = aConstantBufferManager; }
		const engine::buffers::CConstantBufferManager& GetConstantBufferManager() const { return *m_ConstantBufferManager; }
		engine::buffers::CConstantBufferManager& GetConstantBufferManager() { return *m_ConstantBufferManager; }

		//Scene Manager
		void SetSceneManager(engine::scenes::CSceneManager* aSceneManager) { m_SceneManager = aSceneManager; }
		const engine::scenes::CSceneManager& GetSceneManager() const { return *m_SceneManager; }
		engine::scenes::CSceneManager& GetSceneManager() { return *m_SceneManager; }

		//Shader Manager
		void SetShaderManager(engine::shaders::CShaderManager* aShaderManager) { m_ShaderManager = aShaderManager; }
		const engine::shaders::CShaderManager& GetShaderManager() const { return *m_ShaderManager; }
		engine::shaders::CShaderManager& GetShaderManager() { return *m_ShaderManager; }

		//Effect Manager
		void SetEffectManager(engine::effects::CEffectManager* aEffectManager) { m_EffectManager = aEffectManager; }
		const engine::effects::CEffectManager& GetEffectManager() const { return *m_EffectManager; }
		engine::effects::CEffectManager& GetEffectManager() { return *m_EffectManager; }

		//Mesh Manager
		void SetMeshManager(engine::materials::CMeshManager* aMeshManager) { m_MeshManager = aMeshManager; }
		const engine::materials::CMeshManager& GetMeshManager() const { return *m_MeshManager; }
		engine::materials::CMeshManager& GetMeshManager() { return *m_MeshManager; }

		//Technique Pool Manager
		void SetTechniquePoolManager(engine::effects::CTechniquePoolManager* aTechniquePoolManager) { m_TechniquePoolManager = aTechniquePoolManager; }
		const engine::effects::CTechniquePoolManager& GetTechniquePoolManager() const { return *m_TechniquePoolManager; }
		engine::effects::CTechniquePoolManager& GetTechniquePoolManager() { return *m_TechniquePoolManager; }

		//CameraManager
		void SetCameraManager(engine::camera::CCameraManager* aCameraManager) { m_CameraManager = aCameraManager; }
		const engine::camera::CCameraManager& GetCameraManager() const { return *m_CameraManager; }
		engine::camera::CCameraManager& GetCameraManager() { return *m_CameraManager; }

		//ImGUi
		void SetImGUI(CImguiHelper* aImGUI) { m_ImGui = aImGUI; }
		const CImguiHelper& GetImGUI() const { return *m_ImGui; }
		CImguiHelper& GetImGUI() { return *m_ImGui; }

		//PhysXManager
		void SetPhysXManager(CPhysXManager* aPhysXManager) { m_PhysXManager = aPhysXManager; }
		const CPhysXManager& GetPhysXManager() const { return *m_PhysXManager; }
		CPhysXManager& GetPhysXManager() { return *m_PhysXManager; }

		//ParticleManager
		void SetParticleManager(engine::particles::CParticleManager* aParticleManager) { m_ParticleManager = aParticleManager; }
		const engine::particles::CParticleManager& GetParticleManager() const { return *m_ParticleManager; }
		engine::particles::CParticleManager& GetParticleManager() { return *m_ParticleManager; }

		//RenderPipelineManager
		void SetRenderPipelineManager(engine::render::CRenderPipelineManager* aRenderPipelineManager) { m_RenderPipelineManager = aRenderPipelineManager; }
		const render::CRenderPipelineManager& GetRenderPipelineManager() const { return *m_RenderPipelineManager; }
		engine::render::CRenderPipelineManager& GetRenderPipelineManager() { return *m_RenderPipelineManager; }

		//GuiManager
		void SetGUIManager(engine::gui::CGUIManager* aGUIManager) { m_GUIManager = aGUIManager; }
		const engine::gui::CGUIManager& GetGUIManager() const { return *m_GUIManager; }
		engine::gui::CGUIManager& GetGUIManager() { return *m_GUIManager; }

		//SoundManager
		void SetSoundManager(sound::ISoundManager * aSoundManager) { m_SoundManager = aSoundManager; }
		const sound::ISoundManager & GetSoundManager() const { return *m_SoundManager; }
		sound::ISoundManager & GetSoundManager() { return *m_SoundManager; }

		//Effect Manager
		void SetPrefabManager(scenes::CPrefabManager* aPM) { m_PrefabsManager = aPM; }
		const scenes::CPrefabManager& GetPrefabManager() const { return *m_PrefabsManager; }
		scenes::CPrefabManager& GetPrefabManager() { return *m_PrefabsManager; }

	//BUILD_GET_SET_ENGINE_MANAGER(SceneManager);

		float GetDeltaTime() const;
		bool m_ExitGame;
		bool m_ShowDebug;

		GET_SET_BOOL(Paused)

	private:
		materials::CMaterialManager* m_MaterialManager;
		render::CRenderManager* m_RenderManager;
		input::CActionManager* m_ActionManager;
		input::CInputManager* m_InputManager;
		materials::CTextureManager* m_TextureManager;
		buffers::CConstantBufferManager* m_ConstantBufferManager;
		scenes::CSceneManager* m_SceneManager;
		shaders::CShaderManager* m_ShaderManager;
		effects::CEffectManager* m_EffectManager;
		materials::CMeshManager* m_MeshManager;
		effects::CTechniquePoolManager* m_TechniquePoolManager;
		cal3dimpl::CAnimatedModelManager* m_AnimatedModelManager;
		lights::CLightManager* m_LightManager;
		cinematics::CCinematicManager* m_CinematicManager;
		camera::CCameraManager* m_CameraManager;
		particles::CParticleManager* m_ParticleManager;
		render::CRenderPipelineManager* m_RenderPipelineManager;
		scenes::CPrefabManager* m_PrefabsManager;
		CPhysXManager* m_PhysXManager;
		gui::CGUIManager* m_GUIManager;
		base::utils::CCoroutine* m_Coroutine;

		sound::ISoundManager* m_SoundManager;
		std::chrono::monotonic_clock m_Clock;
		std::chrono::monotonic_clock::time_point m_PrevTime;
		float m_DeltaTime;
		bool m_Paused;
	
		CImguiHelper* m_ImGui;
		
		
	};
}
#undef BUILD_GET_SET_ENGINE_MANAGER
#endif