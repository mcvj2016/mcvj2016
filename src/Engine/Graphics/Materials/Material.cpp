#include "Material.h"
#include "Utils/StringUtils.h"
#include "Utils/CheckedDelete.h"
#include "MaterialParameter.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Textures/Texture.h"
#include "Render/RenderManager.h"
#include "Graphics/Effects/Effect.h"
#include <algorithm>

namespace engine
{
	namespace materials
	{
		CMaterial::CMaterial(): m_IsCommon(false), m_Technique(nullptr)
		{
		}

		CMaterial::CMaterial(const std::string& Name): m_IsCommon(false), m_Technique(nullptr)
		{
			this->m_Name = Name;
		}

		CMaterial::~CMaterial()
		{
			base::utils::CheckedDelete(m_Parameters);
		}

		void CMaterial::AddTexture(CTexture* t)
		{
			this->m_Textures.push_back(t);
		}

		void CMaterial::AddParameter(CMaterialParameter* p)
		{
			this->m_Parameters.push_back(p);
		}

		std::vector<CTexture*> CMaterial::GetTextures() const
		{
			return m_Textures;
		}

		CMaterial::CMaterial(const tinyxml2::XMLElement* TreeNode): m_IsCommon(false), m_Technique(nullptr)
		{
		}

		void CMaterial::Apply()
		{
			//assert(mTechnique);
			ID3D11DeviceContext * lContext = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
			// Bind the technique that will render this geometry
			m_Technique->Bind(lContext);

			buffers::CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();
			for (size_t i = 0, lCount = m_Parameters.size(); i < lCount; ++i)
			{
				if (m_Parameters[i]){
					l_CBM.mMaterialDesc.m_RawData[i] = *(Vect4f *)m_Parameters[i]->GetAddr();
				}
			}
			engine::render::CRenderManager &l_CRM = CEngine::GetInstance().GetRenderManager();
			l_CBM.BindPSBuffer(l_CRM.GetDeviceContext(), buffers::CConstantBufferManager::CB_Material);

			if (m_Technique->GetEffect()->GetShader<shaders::CShader>(shaders::CShader::EShaderStage::eGeometryShader) != nullptr)
			{
				l_CBM.BindGSBuffer(l_CRM.GetDeviceContext(), buffers::CConstantBufferManager::CB_MaterialGS);
			}

			for (size_t i = 0, lCount = m_Textures.size(); i < lCount; ++i)
			{
				if (m_Textures[i]){
					m_Textures[i]->Bind(m_Textures[i]->GetTextureIndex(), lContext);
				}
			}
		}

		void CMaterial::ApplyUnbind()
		{
			ID3D11DeviceContext * lContext = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
			m_Technique->Unbind(lContext);
		}

		void CMaterial::SetTechnique(effects::CTechnique* aTechnique)
		{
			m_Technique = aTechnique;
		}

		effects::CTechnique* CMaterial::GetTechnique()
		{
			return m_Technique;
		}

		void CMaterial::DeleteTextures()
		{
			base::utils::CheckedDelete(m_Textures);
		}

		std::vector<CMaterialParameter*> CMaterial::GetParameters()
		{
			return m_Parameters;
		}

		CMaterialParameter* CMaterial::GetParameter(const std::string& paramName)
		{
			for (CMaterialParameter* parameter: m_Parameters)
			{
				if (parameter->GetName()==paramName)
				{
					return parameter;
				}
			}
			return nullptr;
		}

		void CMaterial::RemoveTexture(CTexture* texture)
		{
			m_Textures.erase(std::remove(m_Textures.begin(), m_Textures.end(), texture), m_Textures.end());
		}
	}
}
