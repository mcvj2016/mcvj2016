#include <future>
#include "MaterialManager.h"
#include "Utils/TemplatedMap.h"
#include "Material.h"
#include "XML/XML.h"
#include "Graphics/Textures/TextureManager.h"
#include "Engine.h"
#include "TemplatedMaterialParameter.h"
#include "Graphics/Effects/TechniquePoolManager.h"
#include "Math/Color.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/FileUtils.h"
#include <iomanip>
#include <sstream>

namespace engine
{
	namespace materials
	{
		//Valor por defecto ok?
		CMaterialManager::CMaterialManager()
			: m_XmlTechniqueMaterialsName("data/common/technique_materials.xml")
		{
			loadCommonTechniqueMaterials();
		}

		CMaterialManager::~CMaterialManager()
		{
		}

		void CMaterialManager::loadCommonTechniqueMaterials()
		{
			load();
		}

		void CMaterialManager::load(std::string aName)
		{
			//doc object
			tinyxml2::XMLDocument xmldoc;

			//Load xml
			bool loaded = false;
			
			if (aName == "")
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_XmlTechniqueMaterialsName.c_str()));
				
			}
			else
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(aName.c_str()));
			}

			if (!loaded)
			{
				throw "Error Loading Materials XML";
			}

			tinyxml2::XMLElement* materials = xmldoc.FirstChildElement("materials");
			
			if (materials != nullptr)
			{
				tinyxml2::XMLNode* material = materials->FirstChild();

				while (material != nullptr)
				{
					tinyxml2::XMLElement* element = material->ToElement();
					//guardamos atributo nombre
					std::string materialName = element->GetAttribute<std::string>("name", "");
					assert(materialName != "");
					CMaterial* m = new CMaterial(materialName);
					m->m_IsCommon = element->GetAttribute<bool>("common", false);

					std::string vertexType = element->GetAttribute<std::string>("vertex_type", "");
					m->m_VertexType = vertexType;
					if (vertexType.empty()){
						m->m_TechniqueName = element->GetAttribute<std::string>("technique", "");
						m->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()(m->m_TechniqueName));
					}
					else{						
						m->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()(vertexType));
					}

					//cargamos texturas
					tinyxml2::XMLElement* textures = material->FirstChildElement("textures");
					if (textures != nullptr)
					{
						tinyxml2::XMLNode* texture = textures->FirstChildElement("texture");

						while (texture != nullptr)
						{
							tinyxml2::XMLElement* textureElement = texture->ToElement();
							//guardamos atributo nombre
							std::string textureName = textureElement->Attribute("name");
							std::string textureType = textureElement->Attribute("type");
							CTexture* tex = CEngine::GetInstance().GetTextureManager().GetTexture(textureName);//m_TextureManager->GetTexture(textureName);
							if (tex!=nullptr){
								CMaterial::ETextureIndex lType;
								bool typed = EnumString<CMaterial::ETextureIndex>::ToEnum(lType, textureType);
								assert(typed);
								tex->SetTextureIndex(lType);
								m->AddTexture(tex);
							}
							texture = texture->NextSiblingElement("texture");
						}
					}

					//cargamos parametros
					tinyxml2::XMLElement* parameters = material->FirstChildElement("parameters");
					if (parameters != nullptr)
					{
						tinyxml2::XMLNode* parameter = parameters->FirstChildElement("parameter");

						while (parameter != nullptr)
						{
							tinyxml2::XMLElement* parameterElement = parameter->ToElement();

							//guardamos atributo nombre
							std::string paramName = parameterElement->Attribute("name");
							std::string paramType = parameterElement->Attribute("type");

							CMaterial::TParameterType lType;
							if (EnumString<CMaterial::TParameterType>::ToEnum(lType, paramType))
							{
								CMaterialParameter* lParameter = nullptr;
								switch (lType)
								{
								case CMaterial::eFloat: lParameter = new CTemplatedMaterialParameter<float>(paramName, parameterElement->GetAttribute<float>("value", 0.0f), lType); break;
								case CMaterial::eFloat2: lParameter = new CTemplatedMaterialParameter<Vect2f>(paramName, parameterElement->GetAttribute<Vect2f>("value", v2fZERO), lType); break;
								case CMaterial::eFloat3: lParameter = new CTemplatedMaterialParameter<Vect3f>(paramName, parameterElement->GetAttribute<Vect3f>("value", v3fZERO), lType); break;
								case CMaterial::eFloat4: lParameter = new CTemplatedMaterialParameter<Vect4f>(paramName, parameterElement->GetAttribute<Vect4f>("value", v4fZERO), lType); break;
								case CMaterial::eColor: lParameter = new CTemplatedMaterialParameter<CColor>(paramName, parameterElement->GetAttribute<CColor>("value", CColor(0,0,0,1)), lType); break;
								}

								if (lParameter)
								{
									m->AddParameter(lParameter);
								}
								else
								{
									base::utils::CheckedDelete(lParameter);
								}
							}

							parameter = parameter->NextSiblingElement("parameter");
						}
					}

					if (!this->Add(materialName, m))
					{
						base::utils::CheckedDelete(m);
					}
					//siguiente elemento
					material = material->NextSiblingElement("material");
				}
			}
		}

		void CMaterialManager::Reload()
		{
			Destroy();
			loadCommonTechniqueMaterials();
			this->load("data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/materials.xml");
			
		}

		void CMaterialManager::Save()
		{
			tinyxml2::XMLDocument xmldoc;
			tinyxml2::XMLElement * materials_root = xmldoc.NewElement("materials");
			xmldoc.InsertFirstChild(materials_root);

			tinyxml2::XMLDocument xmldocTechniques;
			tinyxml2::XMLElement * materials_root_techniques = xmldocTechniques.NewElement("materials");
			xmldocTechniques.InsertFirstChild(materials_root_techniques);

			for (size_t i = 0; i < GetCount(); ++i)
			{
				CMaterial* material = GetByIndex(i);
				if (material == nullptr)
				{
					continue;
				}
				tinyxml2::XMLDocument* xmldoc_temp;
				tinyxml2::XMLElement* material_root_temp;
				if (material->m_IsCommon){
					xmldoc_temp = &xmldocTechniques;
					material_root_temp = materials_root_techniques;
				}
				else
				{
					xmldoc_temp = &xmldoc;
					material_root_temp = materials_root;
				}
				tinyxml2::XMLElement * material_element = xmldoc_temp->NewElement("material");
				material_root_temp->InsertEndChild(material_element);
				material_element->SetAttribute("name", material->GetName().c_str());
				material_element->SetAttribute("common", material->m_IsCommon);
				if (material->m_VertexType.empty()){
					material_element->SetAttribute("technique", material->m_TechniqueName.c_str());
				}
				else
				{
					material_element->SetAttribute("vertex_type", material->m_VertexType.c_str());
				}
				std::vector<CTexture*> matTextures = material->GetTextures();
				if (matTextures.size()>0)
				{
					tinyxml2::XMLElement * textures_element = xmldoc_temp->NewElement("textures");
					material_element->InsertEndChild(textures_element);
					for (int i_tex = 0, Cont = matTextures.size(); i_tex < Cont; ++i_tex)
					{
						CTexture* tex = matTextures[i_tex];
						tinyxml2::XMLElement * texture = xmldoc_temp->NewElement("texture");
						textures_element->InsertEndChild(texture);
						texture->SetAttribute("name", tex->m_DriveName.c_str());
						texture->SetAttribute("type", EnumString<CMaterial::ETextureIndex>::ToStr(tex->GetTextureIndex()).c_str());
					}
				}
				std::vector<CMaterialParameter*> matParams = material->GetParameters();
				if (matParams.size()>0)
				{
					tinyxml2::XMLElement * parameters_element = xmldoc_temp->NewElement("parameters");
					material_element->InsertEndChild(parameters_element);
					for (int i_mat = 0, Cont = matParams.size(); i_mat < Cont; ++i_mat)
					{
						CMaterialParameter* parameter = matParams[i_mat];
						tinyxml2::XMLElement * param = xmldoc_temp->NewElement("parameter");
						param->SetAttribute("name", parameter->GetName().c_str());
						param->SetAttribute("type", EnumString<CMaterial::TParameterType>::ToStr(parameter->GetType()).c_str());
						CMaterialParameter* lParameter = nullptr;
						Vect4f* floatsValues = (Vect4f*)parameter->GetAddr();
						std::string value;
						switch (parameter->GetType())
						{
						case CMaterial::eFloat:
							value = std::to_string(floatsValues->x);
							break;
						case CMaterial::eFloat2:
							value = std::to_string(floatsValues->x) + " " + std::to_string(floatsValues->y);
							break;
						case CMaterial::eFloat3:
							value = std::to_string(floatsValues->x) + " " + std::to_string(floatsValues->y) + " " + std::to_string(floatsValues->z);
							break;
						case CMaterial::eFloat4:
							value = std::to_string(floatsValues->x) + " " + std::to_string(floatsValues->y) + " " + std::to_string(floatsValues->z) + " " + std::to_string(floatsValues->w);
							break;
						case CMaterial::eColor:
							value = std::to_string(floatsValues->x) + " " + std::to_string(floatsValues->y) + " " + std::to_string(floatsValues->z) + " " + std::to_string(floatsValues->w);
							break;
						}
						param->SetAttribute("value", value.c_str());
						parameters_element->InsertEndChild(param);
					}
				}
			}
			const std::string& sceneFolder = "data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName();
			const std::string& commonFolder = "data/common/";
			const std::string& lSavePath = sceneFolder + "/saves/";
			const std::string& originalMatFile = sceneFolder + "/materials.xml";
			const std::string& commonMatFile = commonFolder + "/technique_materials.xml";
			if (base::utils::CreateFolder(lSavePath))
			{
				auto t = std::time(nullptr);
				auto tm = *std::localtime(&t);

				std::ostringstream oss;
				oss << std::put_time(&tm, "%d-%m-%Y_%H-%M-%S");
				auto str = oss.str();
				const std::string& matsBackFile = lSavePath + "/materials_" + str + ".xml";
				const std::string& matsCommonBackFile = lSavePath + "/technique_materials_" + str + ".xml";
				base::utils::copyFile(originalMatFile.c_str(), matsBackFile.c_str());
				base::utils::copyFile(commonMatFile.c_str(), matsCommonBackFile.c_str());

				xmldoc.SaveFile(originalMatFile.c_str());
				xmldocTechniques.SaveFile(commonMatFile.c_str());
			}
			else
			{
				// Failed to create directory.
				assert(!"Error creating saves folder");
			}
		}
	}
}
