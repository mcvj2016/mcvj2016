#ifndef _MATERIAL_UAB_
#define _MATERIAL_UAB_
#include "Utils/Name.h"
#include <vector>
#include "Utils/EnumToString.h"
#include "Graphics/Effects/Technique.h"

namespace engine 
{
	namespace materials 
	{
		class CMaterialParameter;
		class CTexture;
	}
}

namespace engine
{
	namespace materials
	{
		class CMaterial : public CName
		{
		public:

			enum ETextureIndex
			{
				eDiffuse = 0,
				eBump,
				eLightMap,
				eSpecular,
				eCube,
				eIndexCount,
				eSplatR = 10,
				eSplatRBump,
				eSplatG,
				eSplatGBump,
				eSplatB,
				eSplatBBump,
			};

			enum TParameterType
			{
				eFloat = 0,
				eFloat2,
				eFloat3,
				eFloat4,
				eColor,
				eParametersCount
			};

			CMaterial();
			CMaterial(const CXMLElement* TreeNode);
			CMaterial(const std::string& Name);
			virtual ~CMaterial();
			void Apply();
			void ApplyUnbind();
			void AddTexture(CTexture* t);
			void AddParameter(CMaterialParameter* p);
			std::vector<CTexture*> GetTextures() const;
			void SetTechnique(effects::CTechnique* aTechnique);
			effects::CTechnique* GetTechnique();
			void DeleteTextures();//use in calcoremodel as texturemnager is not having this textures
			std::vector<CMaterialParameter*> GetParameters();
			CMaterialParameter* GetParameter(const std::string& paramName);
			std::string m_VertexType;
			std::string m_TechniqueName;
			void RemoveTexture(CTexture* texture);
			bool m_IsCommon;
		private:
			DISALLOW_COPY_AND_ASSIGN(CMaterial);
			effects::CTechnique* m_Technique;//Que obtendremos del manager de techniques por el vertex type./*mTechnique = CEngine::GetInstance().GetTechniquePoolManager()(aElement->GetAttribute<std::string>("vertex_type", ""));*/
			std::vector<CTexture*> m_Textures;
			std::vector<CMaterialParameter*> m_Parameters;
			
		};
	}
}

//---------------------------------------------------------------------------------------------------------
Begin_Enum_String(engine::materials::CMaterial::ETextureIndex)
{
	Enum_String_Id(engine::materials::CMaterial::eDiffuse, "diffuse");
	Enum_String_Id(engine::materials::CMaterial::eBump, "bump");
	Enum_String_Id(engine::materials::CMaterial::eLightMap, "lightmap");
	Enum_String_Id(engine::materials::CMaterial::eSpecular, "specular");
	Enum_String_Id(engine::materials::CMaterial::eCube, "cube");
	Enum_String_Id(engine::materials::CMaterial::eSplatR, "splatR");
	Enum_String_Id(engine::materials::CMaterial::eSplatRBump, "splatRBump");
	Enum_String_Id(engine::materials::CMaterial::eSplatG, "splatG");
	Enum_String_Id(engine::materials::CMaterial::eSplatGBump, "splatGBump");
	Enum_String_Id(engine::materials::CMaterial::eSplatB, "splatB");
	Enum_String_Id(engine::materials::CMaterial::eSplatBBump, "splatBBump");
}
End_Enum_String;

//---------------------------------------------------------------------------------------------------------
Begin_Enum_String(engine::materials::CMaterial::TParameterType)
{
	Enum_String_Id(engine::materials::CMaterial::eFloat, "float");
	Enum_String_Id(engine::materials::CMaterial::eFloat2, "float2");
	Enum_String_Id(engine::materials::CMaterial::eFloat3, "float3");
	Enum_String_Id(engine::materials::CMaterial::eFloat4, "float4");
	Enum_String_Id(engine::materials::CMaterial::eColor, "color");
}
End_Enum_String;

#endif
