#ifndef _ENGINE_TEMPLATEDMATERIALPARAMETER_DPVD1_27102016203720_H
#define _ENGINE_TEMPLATEDMATERIALPARAMATER_DPVD1_27102016203720_H
#include "Utils/Defines.h"
#include "MaterialParameter.h"

template<typename T>
class CTemplatedMaterialParameter : public engine::materials::CMaterialParameter
{
public:
	CTemplatedMaterialParameter(const std::string& aName, const T &Value,
	                            engine::materials::CMaterial::TParameterType aType)
		: CMaterialParameter(aName, aType)
		, m_Value(Value)
	{
		m_Name = aName;
		m_Type = aType;
	}
	virtual ~CTemplatedMaterialParameter();
	virtual uint32 GetSize() const { return sizeof(T); };
	void * GetAddr(int index = 0) const;
	void SetValue(const T &Value)
	{
		m_Value=Value;
	} 
	const T & GetValue() const
	{
		return m_Value;
	};
private:
	T m_Value;
};

template <typename T>
CTemplatedMaterialParameter<T>::~CTemplatedMaterialParameter()
{
}

template<typename T>
void* CTemplatedMaterialParameter<T>::GetAddr(int index = 0) const
{
	return (void*)((float*)&m_Value + index);
}
#endif // _ENGINE_TEMPLATEDMATERIALPARAMETER_DPVD1_27102016203720_H