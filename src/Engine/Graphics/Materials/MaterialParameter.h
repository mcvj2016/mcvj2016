#ifndef _ENGINE_MATERIALPARAMETER_DPVD1_27102016203720_H
#define _ENGINE_MATERIALPARAMATER_DPVD1_27102016203720_H
#include "Utils/Name.h"
#include "Utils/Types.h"
#include "Material.h"

namespace engine{
	namespace materials
	{
		
		class CMaterialParameter : public CName
		{
		public:
			CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType);
			virtual ~CMaterialParameter();
			virtual uint32 GetSize() const = 0;
			virtual void * GetAddr(int index = 0) const = 0;
			GET_SET_REF(std::string, Description)
			GET_SET(CMaterial::TParameterType, Type)
				
		protected:
			std::string m_Description;
			CMaterial::TParameterType m_Type;
		private:
			DISALLOW_COPY_AND_ASSIGN(CMaterialParameter);
		};
	}
}
#endif	// _ENGINE_MATERIALPARAMETER_DPVD1_27102016203720_H