#ifndef _ENGINE_MATERIALMANAGER_DPVD1_27102016203657_H
#define _ENGINE_MATERIALMANAGER_DPVD1_27102016203657_H
#include "Utils/TemplatedMap.h"
#include "Material.h"

namespace engine{
	namespace materials
	{
		class CMaterialManager : public base::utils::CTemplatedMap<CMaterial>
		{
		public:

			CMaterialManager();

			virtual ~CMaterialManager();
			void loadCommonTechniqueMaterials();

			void load(std::string aName = "");

			void Reload();

			void Save();

		private:
			std::string m_XmlTechniqueMaterialsName;
		};
	}
}

#endif	// _ENGINE_MATERIALMANAGER_DPVD1_27102016203657_H
