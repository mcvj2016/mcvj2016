#include "TemplatedMaterialParameter.h"

namespace engine{
	namespace materials
	{
		CMaterialParameter::CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType)
			: m_Type(aType)
		{
		}

		CMaterialParameter::~CMaterialParameter()
		{
			
		}
	}
}
