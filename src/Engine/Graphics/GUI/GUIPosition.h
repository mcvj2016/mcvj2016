#ifndef _ENGINE_GUIPOSITION_DPVD1_01062017175800_H
#define _ENGINE_GUIPOSITION_DPVD1_01062017175800_H
#include "GUIManager.h"

namespace engine
{
	namespace gui
	{
		class CGUIPosition
		{
		public:
			//CGUIPosition(int x,int y,int width,int height);
			CGUIPosition(int x, int y, int width, int height,
				GUIAnchor _anchor = GUIAnchor::TOP_LEFT,
				GUICoordType _anchorCoordsType = GUICoordType::GUI_ABSOLUTE,
				GUICoordType _sizeCorrdsType = GUICoordType::GUI_ABSOLUTE);
			virtual ~CGUIPosition();
			int m_x;
			int m_y;
			int m_width;
			int m_height;
		};
	}
}

#endif