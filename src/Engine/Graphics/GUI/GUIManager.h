#ifndef _ENGINE_GUIMANAGER_DPVD1_29052017161500_H
#define _ENGINE_GUIMANAGER_DPVD1_29052017161500_H

#include "Utils/TemplatedMap.h"
#include "Graphics/Materials/Material.h"
#include "Graphics/Buffers/TemplatedGeometry.h"
#include "Graphics/Shaders/VertexType.h"
#include <unordered_map>

#define MAX_VERTEXES 200

namespace engine
{
	namespace gui{
		class CGUIPosition;

		struct SpriteMapInfo
		{
			std::string name;
			int MaterialIndex;
			int w, h;
		};

		struct SpriteInfo
		{
			SpriteMapInfo* SpriteMap;
			float u1, u2, v1, v2;
		};

		struct GUICommand
		{
			SpriteInfo *sprite;
			int x1, y1, x2, y2; //top left is 0,0
			float u1, v1, u2, v2;
			CColor color;
		};

		struct SliderResult
		{
			float real;
			float temp;
		};

		struct Button {
			SpriteInfo* m_Normal;
			SpriteInfo* m_Highlight;
			SpriteInfo* m_Pressed;
		};

		struct Slider
		{
			SpriteInfo* m_Base;
			SpriteInfo* m_Top;
			SpriteInfo* m_Handle;
			SpriteInfo* m_PressedHandle;
			float handleRelativeWidth;
			float handleRelativeHeight;
		};

		struct FontChar
		{
			uint16 x, y, width, height;
			int16 xoffset, yoffset, xadvance;
			uint8 page, chnl;
		};

		enum class GUICoordType
		{
			GUI_ABSOLUTE,
			GUI_RELATIVE,
			GUI_RELATIVE_WIDTH,
			GUI_RELATIVE_HEIGHT
		};

		enum class GUIAnchor
		{
			TOP = 0x1,
			MID = 0x2,
			BOTTOM = 0x4,

			LEFT = 0x10,
			CENTER = 0x20,
			RIGHT = 0x40,

			TOP_LEFT = TOP | LEFT,
			TOP_CENTER = TOP | CENTER,
			TOP_RIGHT = TOP | RIGHT,
			MID_LEFT = MID | LEFT,
			MID_CENTER = MID | CENTER,
			MID_RIGHT = MID | RIGHT,
			BOTTOM_LEFT = BOTTOM | LEFT,
			BOTTOM_CENTER = BOTTOM | CENTER,
			BOTTOM_RIGHT = BOTTOM | RIGHT,
		};

		class CGUIManager
		{
		protected:
			std::string m_ActiveItem;
			std::string m_HotItem;
			std::string m_SelectedItem;

			std::string m_TestText;

			int m_MouseX;
			int m_MouseY;
			int m_MousePrecisionX;
			int m_MousePrecisionY;
			bool m_MouseWentPressed;
			bool m_MouseWentReleased;

			bool IsMouseInside(int mouseX, int mouseY, int GUIElementX, int GUIElementY, int GUIElementW, int GUIElementH);

		public:
			CGUIManager();
			virtual ~CGUIManager();

			void Load(std::string aName);
			void Reload();

			void SetActive(const std::string& id);
			void SetNotActive();
			void SetHot(const std::string& id);
			void SetNotHot(const std::string& id);
			void SetSelected(const std::string& id);
			void SetNotSelected(const std::string& id);
			void Update(float dt);
			void Render(render::CRenderManager& aRendermanager);

			bool DoButton(const std::string& guiID, const std::string& buttonID, const CGUIPosition& position);			
			SliderResult DoSlider(const std::string& guiID, const std::string& sliderID, const CGUIPosition& position, float minValue, float maxValue, float currentValue);
			std::string DoTextBox(const std::string& guiID, const std::string& font, const std::string& currentText, CGUIPosition position);

			int FillCommandQueueWithText(const std::string& font, const std::string& text,
				const CColor& _color = CColor(1, 1, 1, 1), Vect4f* textBox = nullptr);
			void FillCommandQueueWithText(const std::string& font, const std::string& text,	Vect2f coord,
				const CColor& _color = CColor(1, 1, 1, 1), GUIAnchor anchor = GUIAnchor::BOTTOM_LEFT);

		private:
			std::unordered_map<std::string, int16> m_LineHeightPerFont;
			std::unordered_map<std::string, int16> m_BasePerFont;
			std::unordered_map<std::string, std::unordered_map<wchar_t, FontChar >> m_CharactersPerFont;
			std::unordered_map<std::string, std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>>> m_KerningsPerFont;
			std::unordered_map<std::string, std::vector<SpriteInfo*>> m_TexturePerFont;

			std::vector<buffers::CGeometryTriangleList<shaders::PositionColorUV>*> m_Geometry;
			std::vector<materials::CMaterial*> m_Materials;
			std::vector<GUICommand> m_Commands;
			base::utils::CTemplatedMap<SpriteInfo> m_sprites;
			base::utils::CTemplatedMap<Button> m_Buttons;
			base::utils::CTemplatedMap<Slider> m_Slider;
			std::vector<SpriteMapInfo*> m_SpriteMapInfos;
			
			std::string m_XmlName;
		};
	}
}

#endif