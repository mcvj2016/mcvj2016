#ifndef _ENGINE_SLIDER_DPVD1_31052017191000_H
#define _ENGINE_SLIDER_DPVD1_31052017191000_H

#include "GUIManager.h"

namespace engine
{
	namespace gui
	{
		class CSlider 
		{
		public:
			CSlider();
			virtual ~CSlider();
			SpriteInfo* m_Base;
			SpriteInfo* m_Top;
			SpriteInfo* m_Handle;
			SpriteInfo* m_PressedHandle;
			float handleRelativeWidth;
			float handleRelativeHeight;
		};
	}
}

#endif