#ifndef _ENGINE_BUTTON_DPVD1_31052017192300_H
#define _ENGINE_BUTTON_DPVD1_31052017192300_H

#include "GUIManager.h"

namespace engine
{
	namespace gui
	{
		class CButton
		{
		public:
			CButton();
			virtual ~CButton();
			SpriteInfo* m_Normal;
			SpriteInfo* m_Highlight;
			SpriteInfo* m_Pressed;
		};
	}
}

#endif