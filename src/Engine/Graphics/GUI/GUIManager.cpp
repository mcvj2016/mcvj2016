#include "GUIManager.h"
#include "Engine.h"
#include "Input/InputManager.h"
#include "Render/RenderManager.h"
#include "XML/XML.h"
#include "Graphics/Textures/TextureManager.h"
#include "Graphics/Effects/TechniquePoolManager.h"
#include "GUIPosition.h"

namespace engine
{
	namespace gui{

		CGUIManager::CGUIManager() :m_XmlName("data/common/gui.xml"), m_ActiveItem(""), m_HotItem(""), m_TestText("")
		{
			
		}

		CGUIManager::~CGUIManager()
		{
			m_sprites.Destroy();
			m_Buttons.Destroy();
			base::utils::CheckedDelete(m_Geometry);
			base::utils::CheckedDelete(m_Materials);
			base::utils::CheckedDelete(m_SpriteMapInfos);
		}

		void CGUIManager::Load(std::string aName)
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = false;
			if (aName == "")
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_XmlName.c_str()));
			}
			else
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(aName.c_str()));
			}
			if (!loaded)
			{
				throw "Error Loading GUI XML";
			}

			CXMLElement* guiElements = xmldoc.FirstChildElement("gui_elements");
			if (guiElements != nullptr)
			{
				CXMLElement* guiSpriteMaps = guiElements->FirstChildElement("gui_spritemap");

				while (guiSpriteMaps != nullptr)
				{
					SpriteMapInfo* spriteMap = new SpriteMapInfo();
					m_SpriteMapInfos.push_back(spriteMap);
					spriteMap->w = guiSpriteMaps->GetAttribute<int>("width", 0);
					spriteMap->h = guiSpriteMaps->GetAttribute<int>("height", 0);
					spriteMap->name = guiSpriteMaps->GetAttribute<std::string>("name", "");

					CXMLElement* material = guiSpriteMaps->FirstChildElement("material");
					CXMLElement* textures = material->FirstChildElement("textures");					

					std::string materialName = material->GetAttribute<std::string>("name", "");
					assert(materialName != "");
					materials::CMaterial* l_material = new materials::CMaterial(materialName);
					std::string vertexType = material->GetAttribute<std::string>("vertex_type", "");
					l_material->m_VertexType = vertexType;
					if (vertexType.empty()){
						l_material->m_TechniqueName = material->GetAttribute<std::string>("technique", "");
						l_material->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()(l_material->m_TechniqueName));
					}
					else{
						l_material->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()(vertexType));
					}
					shaders::PositionColorUV l_vertexes[MAX_VERTEXES];
					buffers::CVertexBuffer<shaders::PositionColorUV>* l_Vertexes = new buffers::CVertexBuffer<shaders::PositionColorUV>(CEngine::GetInstance().GetRenderManager(), l_vertexes, MAX_VERTEXES, true);
					buffers::CGeometryTriangleList<shaders::PositionColorUV>* l_geometry = new buffers::CGeometryTriangleList<shaders::PositionColorUV>(l_Vertexes);
					m_Geometry.push_back(l_geometry);

					if (textures != nullptr)
					{
						CXMLElement* texture = textures->FirstChildElement("texture");
						while (texture != nullptr)
						{
							tinyxml2::XMLElement* textureElement = texture->ToElement();
							//guardamos atributo nombre
							std::string textureName = textureElement->Attribute("name");
							std::string textureType = textureElement->Attribute("type");
							materials::CTexture* tex = CEngine::GetInstance().GetTextureManager().GetTexture(textureName);
							assert(tex != nullptr);
							tex->SetTextureIndex(materials::CMaterial::eDiffuse);
							l_material->AddTexture(tex);							
							texture = texture->NextSiblingElement("texture");
						}
					}
					
					m_Materials.push_back(l_material);
					spriteMap->MaterialIndex = m_Materials.size()-1;

					CXMLElement* sprites = guiSpriteMaps->FirstChildElement("sprites");
					CXMLElement* sprite = sprites->FirstChildElement("sprite");
					while (sprite != nullptr)
					{
						std::string spriteName = sprite->GetAttribute<std::string>("name", "");

						SpriteInfo* l_sprite = new SpriteInfo();
						l_sprite->SpriteMap = spriteMap;
						float width = sprite->GetAttribute<float>("w", 0.0f);
						float height = sprite->GetAttribute<float>("h", 0.0f);
						float u1 = sprite->GetAttribute<float>("x", 0.0f);
						float u2 = u1 + width;
						float v1 = sprite->GetAttribute<float>("y", 0.0f);
						float v2 = v1 + height;

						l_sprite->u1 = u1 / spriteMap->w;
						l_sprite->u2 = u2 / spriteMap->w;
						l_sprite->v1 = v1 / spriteMap->h;
						l_sprite->v2 = v2 / spriteMap->h;

						m_sprites.Add(spriteName, l_sprite);
						sprite = sprite->NextSiblingElement("sprite");
					}

					guiSpriteMaps = guiSpriteMaps->NextSiblingElement("gui_spritemap");
				}

				CXMLElement* button = guiElements->FirstChildElement("button");
				while (button != nullptr)
				{
					Button * l_Button = new Button();
					l_Button->m_Normal = m_sprites(button->GetAttribute<std::string>("normal", ""));
					l_Button->m_Highlight = m_sprites(button->GetAttribute<std::string>("highlight", ""));
					l_Button->m_Pressed = m_sprites(button->GetAttribute<std::string>("pressed", ""));

					m_Buttons.Add(button->GetAttribute<std::string>("name", ""), l_Button);					
					button = button->NextSiblingElement("button");
				}
				CXMLElement* slider = guiElements->FirstChildElement("slider");
				while (slider != nullptr)
				{
					Slider * l_Slider = new Slider();
					l_Slider->m_Base = m_sprites(slider->GetAttribute<std::string>("base", ""));
					l_Slider->m_Top = m_sprites(slider->GetAttribute<std::string>("top", ""));
					l_Slider->m_Handle = m_sprites(slider->GetAttribute<std::string>("handle", ""));
					l_Slider->m_PressedHandle = m_sprites(slider->GetAttribute<std::string>("pressed_handle", ""));

					l_Slider->handleRelativeWidth = (l_Slider->m_Handle->u2 - l_Slider->m_Handle->u1) / (l_Slider->m_Base->u2 - l_Slider->m_Base->u1);
					//l_Slider->handleRelativeHeight = (l_Slider->m_Handle->v2 - l_Slider->m_Handle->v1) / (l_Slider->m_Base->v2 - l_Slider->m_Base->v1);
					l_Slider->handleRelativeHeight = l_Slider->m_Handle->v2;

					m_Slider.Add(slider->GetAttribute<std::string>("name", ""), l_Slider);
					slider = slider->NextSiblingElement("slider");
				}

				CXMLElement* font = guiElements->FirstChildElement("font");
				while (font != nullptr)
				{
					std::string fontName = font->GetAttribute<std::string>("name", "");
					std::string fontPath = font->GetAttribute<std::string>("path", "");
					
					tinyxml2::XMLDocument xmldoc;
					loaded = false;
					loaded = base::xml::SucceedLoad(xmldoc.LoadFile(fontPath.c_str()));
					assert(loaded);
					CXMLElement* fontXML = xmldoc.FirstChildElement("font");
					assert(fontXML != nullptr);

					CXMLElement* common = fontXML->FirstChildElement("common");
					assert(common!=nullptr);
					m_LineHeightPerFont[fontName] = static_cast<int16>(common->GetAttribute<int>("lineHeight", 0));
					m_BasePerFont[fontName] = static_cast<int16>(common->GetAttribute<int>("base", 0));

					CXMLElement* pages = fontXML->FirstChildElement("pages");
					CXMLElement* page = pages->FirstChildElement("page");
					assert(page != nullptr);
					std::vector<SpriteInfo*> l_Textures;
					while (page != nullptr)
					{
						std::string textureName = page->GetAttribute<std::string>("file", "");
						l_Textures.push_back( m_sprites(textureName) );
						page = page->NextSiblingElement("page");
					}
					m_TexturePerFont[fontName] = l_Textures;

					CXMLElement* chars = fontXML->FirstChildElement("chars");
					CXMLElement* charElement = chars->FirstChildElement("char");
					assert(charElement != nullptr);
					std::unordered_map<wchar_t, FontChar > l_fontchars;
					while (charElement != nullptr)
					{
						FontChar l_FontChar = {};
						
						l_FontChar.x = static_cast<uint16>(charElement->GetAttribute<int>("x", 0));
						l_FontChar.y = static_cast<uint16>(charElement->GetAttribute<int>("y", 0));
						l_FontChar.width = static_cast<uint16>(charElement->GetAttribute<int>("width", 0));
						l_FontChar.height = static_cast<uint16>(charElement->GetAttribute<int>("height", 0));
								  
						l_FontChar.xoffset = static_cast<int16>(charElement->GetAttribute<int>("xoffset", 0));
						l_FontChar.yoffset = static_cast<int16>(charElement->GetAttribute<int>("yoffset", 0));
						l_FontChar.xadvance = static_cast<int16>(charElement->GetAttribute<int>("xadvance", 0));
								  
						l_FontChar.page = static_cast<uint8>(charElement->GetAttribute<int>("page", 0));
						l_FontChar.chnl = static_cast<uint8>(charElement->GetAttribute<int>("chnl", 0));
						
						l_fontchars[static_cast<wchar_t>(charElement->GetAttribute<int>("id", 0))] = l_FontChar;
						charElement = charElement->NextSiblingElement("char");
					}
					m_CharactersPerFont[fontName] = l_fontchars;

					CXMLElement* kernings = fontXML->FirstChildElement("kernings");
					CXMLElement* kerning = kernings->FirstChildElement("kerning");
					assert(kerning != nullptr);
					while (kerning != nullptr)
					{
						wchar_t first = static_cast<wchar_t>(kerning->GetAttribute<int>("first", 0));
						wchar_t	second = static_cast<wchar_t>(kerning->GetAttribute<int>("second",0));
						int amount = kerning->GetAttribute<int>("amount",0);
						m_KerningsPerFont[fontName][first][second] = amount;
						kerning = kerning->NextSiblingElement("kerning");
					}

					font = font->NextSiblingElement("font");
				}
			}
		}

		void CGUIManager::Reload()
		{

		}

		void CGUIManager::SetActive(const std::string& id)
		{
			m_ActiveItem = id;
			m_SelectedItem = "";
		}

		void CGUIManager::SetNotActive()
		{
			m_ActiveItem = "";
		}

		void CGUIManager::SetHot(const std::string& id)
		{
			if (m_ActiveItem == "" || m_ActiveItem == id)
			{
				m_HotItem = id;
			}
		}

		void CGUIManager::SetNotHot(const std::string& id)
		{
			if (m_HotItem == id){
				m_HotItem = "";
			}
		}

		void CGUIManager::SetSelected(const std::string& id)
		{
			m_SelectedItem = id;
		}

		void CGUIManager::SetNotSelected(const std::string& id)
		{
			if (m_SelectedItem == id)
			{
				m_SelectedItem = "";
			}
		}

		void CGUIManager::Update(float dt)
		{
			//if (!m_InputUpToDate)
			//{
				input::CInputManager& input = CEngine::GetInstance().GetInputManager();
				m_MouseX = input.GetMouseX();
				m_MouseY = input.GetMouseY();
				m_MouseWentPressed = input.LeftMouseButtonBecomesPressed();
				m_MouseWentReleased = input.LeftMouseButtonBecomesReleased();				
				///m_InputUpToDate = true;
			//}

			POINT Cursor;
			if (GetCursorPos(&Cursor))
			{
				render::CRenderManager& RM = CEngine::GetInstance().GetRenderManager();
				if (ScreenToClient(RM.m_Hwnd,&Cursor))
				{
					m_MousePrecisionX = Cursor.x;
					m_MousePrecisionY = Cursor.y;
				}
			}

			m_TestText = DoTextBox("gui1", "ArialFont", m_TestText, CGUIPosition(100, 100, 100, 50));
			DoButton("gui1", "teula_button", CGUIPosition(100, 100, 100, 50));						

			//DoSlider("gui1", "teula_slider", CGUIPosition(100, 200, 500, 80),0,100,50);

			//FillCommandQueueWithText("ArialFont","Esta ES UNA prueba",Vect2f(100,100), CColor(1,0,0,1));
			
		}

		bool CGUIManager::IsMouseInside(int mouseX, int mouseY, int GUIElementX, int GUIElementY, int GUIElementW, int GUIElementH)
		{
			bool isInside = false;
			if ((mouseX >= GUIElementX) && (mouseX <= (GUIElementX + GUIElementW)) &&
					(mouseY >= GUIElementY) && (mouseY <= (GUIElementY + GUIElementH)))
			{
				isInside = true;
			}
			return isInside;
		}

		bool CGUIManager::DoButton(const std::string& guiID, const std::string& buttonID, const CGUIPosition& position)
		{
			SpriteInfo* sprite;
			bool Result = false;
			if (m_ActiveItem == guiID)
			{
				if (m_MouseWentReleased)
				{
					if (m_HotItem == guiID)
					{
						Result = true;
					}						
					SetNotActive();
				}
			}
			else if (m_HotItem == guiID)
			{
				if (m_MouseWentPressed)
				{
					SetActive(guiID);
				}
			}
			if (IsMouseInside(m_MouseX, m_MouseY, position.m_x, position.m_y, position.m_width, position.m_height))
			{
				SetHot(guiID);
			}
			else
			{
				SetNotHot(guiID);
			}

			if (m_ActiveItem == guiID && m_HotItem == guiID){
				sprite = m_Buttons(buttonID)->m_Pressed;
			}
			else
			{
				if (m_HotItem == guiID)
				{
					sprite = m_Buttons(buttonID)->m_Highlight;
				}
				else
				{
					sprite = m_Buttons(buttonID)->m_Normal;
				}
			}

			GUICommand command = {
				sprite,
				position.m_x, position.m_y, position.m_x + position.m_width, position.m_y + position.m_height,
				0, 0, 1, 1,
				CColor(1, 1, 1, 1) };

			m_Commands.push_back(command);

			return Result;
		}

		SliderResult CGUIManager::DoSlider(const std::string& guiID, const std::string& sliderID, const CGUIPosition& position, float minValue, float maxValue, float currentValue)
		{			
			Slider* sliderInfo = m_Slider(sliderID);
			SliderResult Result = { 0.0f, 0.0f };
			bool RealResult = false;

			float factor = (float)(m_MouseX - position.m_x) / ((float)position.m_width);
			if (factor<0)
			{
				factor = 0;
			}
			else if (factor>1)
			{
				factor = 1;
			}
			Result.temp = minValue + (maxValue - minValue) * factor;

			if (m_ActiveItem == guiID)
			{
				if (m_MouseWentReleased)
				{
					if (m_HotItem == guiID)
					{
						RealResult = true;
					}
					SetNotActive();
				}
			}
			else if (m_HotItem == guiID)
			{
				if (m_MouseWentPressed)
				{
					SetActive(guiID);
				}
			}

			if (RealResult)
			{
				Result.real = Result.temp;
			}
			else if (m_ActiveItem == guiID)
			{
				Result.real = currentValue;
			}
			else
			{
				Result.temp = currentValue;
				Result.real = currentValue;
			}

			float HandlePosition = position.m_x + position.m_width * (Result.temp - minValue) / (maxValue - minValue);
			float RealHandleWidth = sliderInfo->handleRelativeWidth * position.m_width;
			float RealHandleHeight = sliderInfo->handleRelativeHeight * position.m_height;

			int RealHandleX = (int)(HandlePosition - RealHandleWidth * 0.5f);
			int RealHandleY = (int)(position.m_y + position.m_height * 0.5f - RealHandleHeight * 0.5);

			if (IsMouseInside(m_MouseX, m_MouseY, position.m_x, position.m_y, position.m_width, position.m_height))
			{
				SetHot(guiID);
			}
			else if (IsMouseInside(m_MouseX, m_MouseY, RealHandleX, RealHandleY, (int)RealHandleWidth, (int)RealHandleHeight))
			{
				SetHot(guiID);
			}
			else
			{
				SetNotHot(guiID);
			}

			{
				GUICommand command = {
					(m_ActiveItem == guiID && m_HotItem == guiID) ? sliderInfo->m_PressedHandle : sliderInfo->m_Handle,
					RealHandleX, RealHandleY, RealHandleX + (int)RealHandleWidth, RealHandleY + (int)RealHandleHeight,
					0, 0, 1, 1,
					CColor(1.0f, 1.0f, 1.0f, 1.0f)
				};
				m_Commands.push_back(command);
			}
			{
				GUICommand command = {
					sliderInfo->m_Top,
					position.m_x, position.m_y, (int)HandlePosition, position.m_y + position.m_height,
					0, 0, (Result.temp - minValue) / (maxValue - minValue), 1,
					CColor(1.0f, 1.0f, 1.0f, 1.0f)
				};
				m_Commands.push_back(command);
			}
			{
				GUICommand command = {
					sliderInfo->m_Base,
					position.m_x, position.m_y, position.m_x + position.m_width, position.m_y + position.m_height,
					0, 0, 1, 1,
					CColor(1.0f, 1.0f, 1.0f, 1.0f)
				};
				m_Commands.push_back(command);
			}			
			return Result;
		}

		std::string CGUIManager::DoTextBox(const std::string& guiID, const std::string& font, 
			const std::string& currentText, CGUIPosition position)
		{
			if (m_ActiveItem == guiID)
			{
				if (m_MouseWentReleased)
				{
					if (m_HotItem == guiID)
					{
						SetSelected(guiID);
					}
					SetNotActive();
				}
			}
			else if (m_HotItem == guiID)
			{
				if (m_MouseWentPressed)
				{
					SetActive(guiID);
				}
			}
			if (IsMouseInside(m_MouseX, m_MouseY, position.m_x, position.m_y, position.m_width, position.m_height))
			{
				SetHot(guiID);
			}
			else
			{
				SetNotHot(guiID);
			}

			std::string displayText;
			std::string activeText = currentText;

			if (guiID == m_SelectedItem)
			{				
				input::CInputManager& l_IM = CEngine::GetInstance().GetInputManager();
				wchar_t lastChar = l_IM.GetLastChar();
				if (lastChar >= 0x20 && lastChar < 255)
				{
					activeText += (char)lastChar;
				}
				else if (lastChar == '\r')
				{
					activeText += '\n';
				}
				else if (lastChar == '\b')
				{
					activeText = activeText.substr(0, activeText.length() - 1);
				}				
				l_IM.SetLastChar(0);
			}
			FillCommandQueueWithText(font, activeText, Vect2f(position.m_x + position.m_width * 0.05f,
				position.m_y + position.m_height * 0.75f),CColor(1.0,0.0,0.0,1.0));

			return activeText;
		}

		void CGUIManager::Render(render::CRenderManager& aRendermanager)
		{
			shaders::PositionColorUV currentBufferData[MAX_VERTEXES];
			int currentVertex = 0;
			SpriteMapInfo *currentSpriteMap = nullptr;
			for (size_t i = 0; i < m_Commands.size(); ++i)  //commandsExecutionOrder.size()
			{
				GUICommand &command = m_Commands[i];
				assert(command.x1 <= command.x2);
				assert(command.y2 <= command.y2);

				SpriteInfo *commandSprite = command.sprite;
				SpriteMapInfo *commandSpriteMap = commandSprite->SpriteMap;

				if (currentSpriteMap != commandSpriteMap || currentVertex == MAX_VERTEXES)
				{
					if (currentVertex > 0)
					{
						m_Materials[currentSpriteMap->MaterialIndex]->Apply();
						m_Geometry[currentSpriteMap->MaterialIndex]->UpdateVertexs(currentBufferData, currentVertex);
						m_Geometry[currentSpriteMap->MaterialIndex]->Render(aRendermanager.GetDeviceContext(), currentVertex);
					}
					currentVertex = 0;
					currentSpriteMap = commandSpriteMap;
				}

				int screenWidth = aRendermanager.GetWidth();
				int screeHeight = aRendermanager.GetHeight();
				float x1 = (command.x1 / (screenWidth * 0.5f)) - 1.0f;
				float x2 = (command.x2 / (screenWidth * 0.5f)) - 1.0f;
				float y1 = 1.0f - (command.y1 / (screeHeight * 0.5f));
				float y2 = 1.0f - (command.y2 / (screeHeight * 0.5f));

				float u1 = commandSprite->u1 * (1.0f - command.u1) + commandSprite->u2 * command.u1;
				float u2 = commandSprite->u1 * (1.0f - command.u2) + commandSprite->u2 * command.u2;
				float v1 = commandSprite->v1 * (1.0f - command.v1) + commandSprite->v2 * command.v1;
				float v2 = commandSprite->v1 * (1.0f - command.v2) + commandSprite->v2 * command.v2;

				currentBufferData[currentVertex++] = { Vect4f(x1, y2, 0.0f, 1.0f), command.color, Vect2f(u1, v2) };
				currentBufferData[currentVertex++] = { Vect4f(x2, y2, 0.0f, 1.0f), command.color, Vect2f(u2, v2) };
				currentBufferData[currentVertex++] = { Vect4f(x1, y1, 0.0f, 1.0f), command.color, Vect2f(u1, v1) };

				currentBufferData[currentVertex++] = { Vect4f(x1, y1, 0.0f, 1.0f), command.color, Vect2f(u1, v1) };
				currentBufferData[currentVertex++] = { Vect4f(x2, y2, 0.0f, 1.0f), command.color, Vect2f(u2, v2) };
				currentBufferData[currentVertex++] = { Vect4f(x2, y1, 0.0f, 1.0f), command.color, Vect2f(u2, v1) };
			}
			if (currentVertex > 0)
			{
				m_Materials[currentSpriteMap->MaterialIndex]->Apply();
				m_Geometry[currentSpriteMap->MaterialIndex]->UpdateVertexs(currentBufferData, currentVertex);
				m_Geometry[currentSpriteMap->MaterialIndex]->Render(aRendermanager.GetDeviceContext(), currentVertex);
			}
			m_Commands.clear();
		}

		int CGUIManager::FillCommandQueueWithText(const std::string& font, const std::string& text,const CColor& color,
			Vect4f* textBox)
		{
			Vect4f dummy;
			if (textBox == nullptr) textBox = &dummy;

			*textBox = Vect4f(0, 0, 0, 0);

			assert(m_LineHeightPerFont.find(font) != m_LineHeightPerFont.end());
			assert(m_BasePerFont.find(font) != m_BasePerFont.end());
			assert(m_CharactersPerFont.find(font) != m_CharactersPerFont.end());
			assert(m_KerningsPerFont.find(font) != m_KerningsPerFont.end());
			assert(m_TexturePerFont.find(font) != m_TexturePerFont.end());

			int lineHeight = m_LineHeightPerFont[font];
			int base = m_BasePerFont[font];
			const std::unordered_map< wchar_t, FontChar > &l_CharacterMap = m_CharactersPerFont[font];
			const std::unordered_map< wchar_t, std::unordered_map< wchar_t, int>> &l_Kernings = m_KerningsPerFont[font];
			const std::vector<SpriteInfo*> &l_TextureArray = m_TexturePerFont[font];

			wchar_t last = 0;

			int cursorX = 0, cursorY = 0;

			float spritewidth = static_cast<float>(l_TextureArray[0]->SpriteMap->w);
			float spriteHeight = static_cast<float>(l_TextureArray[0]->SpriteMap->h);

			int addedCommands = 0;

			for (char c : text)
			{
				if (c == '\n')
				{
					cursorY += lineHeight;
					cursorX = 0;
					last = 0;
				}
				else
				{
					auto it = l_CharacterMap.find((wchar_t)c);
					if (it != l_CharacterMap.end())
					{
						const FontChar &fontChar = it->second;
						auto it1 = l_Kernings.find(last);
						if (it1 != l_Kernings.end())
						{
							auto it2 = it1->second.find(c);
							if (it2 != it1->second.end())
							{
								int kerning = it2->second;
								cursorX += kerning;
							}
						}
						GUICommand command = {};
						command.sprite = l_TextureArray[fontChar.page];
						command.x1 = cursorX + fontChar.xoffset;
						command.x2 = command.x1 + fontChar.width;
						command.y1 = cursorY - base + fontChar.yoffset;
						command.y2 = command.y1 + fontChar.height;

						command.u1 = (float)fontChar.x / spritewidth;
						command.u2 = (float)(fontChar.x + fontChar.width) / spritewidth;
						command.v1 = (float)fontChar.y / spriteHeight;
						command.v2 = (float)(fontChar.y + fontChar.height) / spriteHeight;

						command.color = color;

						m_Commands.push_back(command);
						++addedCommands;

						last = c;
						cursorX += fontChar.xadvance;

						if (command.x1 < textBox->x) textBox->x = (float)command.x1;
						if (command.y1 < textBox->y) textBox->y = (float)command.y1;
						if (command.x2 > textBox->z) textBox->z = (float)command.x2;
						if (command.y2 > textBox->w) textBox->w = (float)command.y2;
					}
				}
			}
			return addedCommands;
		}

		void CGUIManager::FillCommandQueueWithText(const std::string& font, const std::string& text, Vect2f coord,
			const CColor& color, GUIAnchor anchor)
		{
			Vect4f textSizes;

			int numCommands = FillCommandQueueWithText(font, text, color, &textSizes);

			Vect2f adjustment = coord;

			if ((int)anchor & (int)GUIAnchor::TOP)
			{
				adjustment.y -= textSizes.y;
			}
			else if ((int)anchor & (int)GUIAnchor::MID)
			{ 
				adjustment.y -= (textSizes.y + textSizes.w) * 0.5f;
			}
			else if ((int)anchor & (int)GUIAnchor::BOTTOM)
			{ 
				adjustment.y -= textSizes.w;
			}
			else
			{ 
				assert(false);
			}

			if ((int)anchor & (int)GUIAnchor::LEFT)
			{
				adjustment.x -= textSizes.x;
			}
			else if ((int)anchor & (int)GUIAnchor::CENTER)
			{ 
				adjustment.x -= (textSizes.x + textSizes.z) * 0.5f;
			}
			else if ((int)anchor & (int)GUIAnchor::RIGHT)
			{ 
				adjustment.x -= textSizes.z;
			}
			else
			{ 
				assert(false);
			}

			for (size_t i = m_Commands.size() - numCommands; i < m_Commands.size(); ++i)
			{
				m_Commands[i].x1 += static_cast<int>(adjustment.x);
				m_Commands[i].x2 += static_cast<int>(adjustment.x);
				m_Commands[i].y1 += static_cast<int>(adjustment.y);
				m_Commands[i].y2 += static_cast<int>(adjustment.y);
			}
		}

	}//End Namespace
}//End Namespace
