#include "GUIPosition.h"
#include "Engine.h"

namespace engine
{
	namespace gui
	{
		/*CGUIPosition::CGUIPosition(int x, int y, int width, int height) 
			:x(x), y(y), width(width), height(height)
		{
				
		}*/

		CGUIPosition::CGUIPosition(int x, int y, int width, int height,
			GUIAnchor _anchor, GUICoordType _anchorCoordsType, GUICoordType _sizeCorrdsType)
		{
			int l_Width = CEngine::GetInstance().GetRenderManager().GetWidth();
			int l_Height = CEngine::GetInstance().GetRenderManager().GetHeight();

			switch (_sizeCorrdsType)
			{
			default:
				assert(false);
			case GUICoordType::GUI_ABSOLUTE:
				m_width = (int)width;
				m_height = (int)height;
				break;
			case GUICoordType::GUI_RELATIVE:
				m_width = (int)(width * l_Width);
				m_height = (int)(height * l_Height);
				break;
			case GUICoordType::GUI_RELATIVE_WIDTH:
				m_width = (int)(width * l_Width);
				m_height = (int)(height * l_Width);
				break;
			case GUICoordType::GUI_RELATIVE_HEIGHT:
				m_width = (int)(width * l_Height);
				m_height = (int)(height * l_Height);
				break;
			}

			float unitPixelSizeX, unitPixelSizeY;

			switch (_anchorCoordsType)
			{
			default:
				assert(false);
			case GUICoordType::GUI_ABSOLUTE:
				unitPixelSizeX = 1;
				unitPixelSizeY = 1;
				break;
			case GUICoordType::GUI_RELATIVE:
				unitPixelSizeX = (float)l_Width;
				unitPixelSizeY = (float)l_Height;
			case GUICoordType::GUI_RELATIVE_WIDTH:
				unitPixelSizeX = (float)l_Width;
				unitPixelSizeY = (float)l_Width;
				break;
			case GUICoordType::GUI_RELATIVE_HEIGHT:
				unitPixelSizeX = (float)l_Height;
				unitPixelSizeY = (float)l_Height;
				break;
			}

			if (x < 0)
			{
				x = 1 + x;
			}

			if (y < 0)
			{
				y = 1 + y;
			}

			if ((int)_anchor & (int)GUIAnchor::LEFT)
			{
				m_x = (int)(x * unitPixelSizeX);
			}
			else if ((int)_anchor & (int)GUIAnchor::CENTER)
			{
				m_x = (int)(x * unitPixelSizeX - m_width * 0.5f);
			}
			else if ((int)_anchor & (int)GUIAnchor::RIGHT)
			{
				m_x = (int)(x*unitPixelSizeX - m_width);
			}


			if ((int)_anchor & (int)GUIAnchor::TOP)
			{
				m_y = (int)(y * unitPixelSizeY);
			}
			else if ((int)_anchor & (int)GUIAnchor::CENTER)
			{
				m_y = (int)(y * unitPixelSizeY - m_height * 0.5f);
			}
			else if ((int)_anchor & (int)GUIAnchor::BOTTOM)
			{
				m_y = (int)(y*unitPixelSizeY - m_height);
			}
		}

		CGUIPosition::~CGUIPosition()
		{
				
		}
	}
}
