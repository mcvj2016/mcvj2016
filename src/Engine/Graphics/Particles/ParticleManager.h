#ifndef _ENGINE_PARTICLEMANAGER_23042017194800_H
#define _ENGINE_PARTICLEMANAGER_23042017194800_H

#include "Utils/TemplatedMap.h"
#include "ParticleSystemType.h"

namespace engine
{
	namespace particles
	{
		class CParticleManager : public base::utils::CTemplatedMap<CParticleSystemType>
		{
		public:
			CParticleManager();
			virtual ~CParticleManager();
			void NewParticle();
			void NewSizeControlPoint(CParticleSystemType& particle);
			void NewColorControlPoint(CParticleSystemType& particle);
			void Load(std::string aName = "");
			void Reload();
			void Save();
		private:
			std::string m_XmlName;
		};
	}
}

#endif