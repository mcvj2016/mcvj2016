#ifndef _ENGINE_PARTICLESYSTEMTYPE_23042017194600_H
#define _ENGINE_PARTICLESYSTEMTYPE_23042017194600_H

#include "Graphics/Materials/Material.h"
#include "Math/Color.h"

namespace engine
{
	namespace particles
	{
		class CParticleSystemType : public CName
		{

		public:
			struct ControlPointColor
			{
				Vect2f m_Time;
				CColor m_Color1, m_Color2;
			};

			struct ControlPointSize
			{
				Vect2f m_Time;
				Vect2f m_Size;
			};

			CParticleSystemType(const std::string& name);
			CParticleSystemType(const CXMLElement* aElement);
			virtual ~CParticleSystemType();
			void Destroy();

			materials::CMaterial* m_Material;
			int m_NumFrames;
			float m_TimePerFrame;
			bool m_LoopFrames;

			bool m_EmitAbsolute, m_InstantSpawn;
			float m_StartingDirectionAngle, m_StartingAccelerationAngle;
			Vect2f m_Size;
			Vect2f m_EmitRate, m_AwakeTime, m_SleepTime, m_Life;
			Vect2f m_StartingAngle, m_StartingAngularSpeed, m_AngularAcceleration;
			Vect3f m_StartingSpeed1, m_StartingSpeed2;
			Vect3f m_StartingAcceleration1, m_StartingAcceleration2;
			CColor m_Color1, m_Color2;

			int m_MaxParticles;

			std::vector<ControlPointColor> m_ControlPointColors;
			std::vector<ControlPointSize> m_ControlPointSizes;
		};
	}
}

#endif