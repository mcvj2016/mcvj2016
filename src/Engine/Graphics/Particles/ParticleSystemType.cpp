#include "ParticleSystemInstance.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Materials/MaterialManager.h"

namespace engine
{
	namespace particles
	{
		CParticleSystemType::CParticleSystemType(const std::string& name) :
			CName(name),
			m_MaxParticles(1),
			m_NumFrames(1),
			m_TimePerFrame(1.0f),
			m_LoopFrames(false),
			m_EmitAbsolute(false), 
			m_InstantSpawn(false),
			m_StartingDirectionAngle(1.0f),
			m_StartingAccelerationAngle(1.0f),
			m_Size(v2fZERO),
			m_EmitRate(v2fZERO),
			m_AwakeTime(v2fZERO),
			m_SleepTime(v2fZERO),
			m_Life(v2fZERO),
			m_StartingAngle(v2fZERO),
			m_StartingAngularSpeed(v2fZERO),
			m_AngularAcceleration(v2fZERO),
			m_StartingSpeed1(v3fZERO),
			m_StartingSpeed2(v3fZERO),
			m_StartingAcceleration1(v3fZERO),
			m_StartingAcceleration2(v3fZERO),
			m_Color1(v4fZERO),
			m_Color2(v4fONE)
		{
			m_Material = CEngine::GetInstance().GetMaterialManager()("default_particle_material");
		}

		CParticleSystemType::CParticleSystemType(const CXMLElement* aElement):CName(aElement)
		{
			m_Material = CEngine::GetInstance().GetMaterialManager()(aElement->GetAttribute<std::string>("material", ""));
			m_MaxParticles = aElement->GetAttribute<int>("max_num_particles", 1);
			m_NumFrames = aElement->GetAttribute<int>("num_frames", 1);
			m_TimePerFrame = aElement->GetAttribute<float>("time_per_frame", 1.0f);
			m_LoopFrames = aElement->GetAttribute<bool>("loop_frames", false);
			m_EmitAbsolute = aElement->GetAttribute<bool>("emit_absolute", false);
			m_InstantSpawn = aElement->GetAttribute<bool>("instant_spawn", false);
			m_StartingDirectionAngle = aElement->GetAttribute<float>("starting_dir_angle", 1.0f);
			m_StartingAccelerationAngle = aElement->GetAttribute<float>("starting_acc_angle", 1.0f);
			m_Size = aElement->GetAttribute<Vect2f>("size", Vect2f(0.0, 0.0));
			m_EmitRate = aElement->GetAttribute<Vect2f>("emit_rate", Vect2f(0.0, 0.0));
			m_AwakeTime = aElement->GetAttribute<Vect2f>("awake_time", Vect2f(0.0, 0.0));
			m_SleepTime = aElement->GetAttribute<Vect2f>("sleep_time", Vect2f(0.0, 0.0));
			m_Life = aElement->GetAttribute<Vect2f>("life", Vect2f(0.0, 0.0));
			m_StartingAngle = aElement->GetAttribute<Vect2f>("starting_angle", Vect2f(0.0, 0.0));
			m_StartingAngularSpeed = aElement->GetAttribute<Vect2f>("starting_angular_speed", Vect2f(0.0, 0.0));
			m_AngularAcceleration = aElement->GetAttribute<Vect2f>("angular_acceleration", Vect2f(0.0, 0.0));
			m_StartingSpeed1 = aElement->GetAttribute<Vect3f>("starting_speed_min", Vect3f(0.0, 0.0, 0.0));
			m_StartingSpeed2 = aElement->GetAttribute<Vect3f>("starting_speed_max", Vect3f(1.0, 1.0, 1.0));
			m_StartingAcceleration1 = aElement->GetAttribute<Vect3f>("starting_acceleration_min", Vect3f(0.0, 0.0, 0.0));
			m_StartingAcceleration2 = aElement->GetAttribute<Vect3f>("starting_acceleration_max", Vect3f(1.0, 1.0, 1.0));
			m_Color1 = aElement->GetAttribute<CColor>("color_min", CColor(0.0, 0.0, 0.0, 0.0));
			m_Color2 = aElement->GetAttribute<CColor>("color_max", CColor(1.0, 1.0, 1.0, 1.0));

 			const CXMLElement* sizeControlPoints = aElement->FirstChildElement("SizeControlPoints");
			const CXMLElement* sizePoint = sizeControlPoints->FirstChildElement("Point");
			while (sizePoint!=nullptr)
			{
				ControlPointSize point;
				point.m_Size = sizePoint->GetAttribute<Vect2f>("size", Vect2f(0.0, 0.0));
				point.m_Time = sizePoint->GetAttribute<Vect2f>("time", Vect2f(0.0, 0.0));
				m_ControlPointSizes.push_back(point);
				sizePoint = sizePoint->NextSiblingElement();
			}

			const CXMLElement* colorControlPoints = aElement->FirstChildElement("ColorControlPoints");
			const CXMLElement* colorPoint = colorControlPoints->FirstChildElement("Point");
			while (colorPoint!=nullptr)
			{
				ControlPointColor point;
				point.m_Color1 = colorPoint->GetAttribute<CColor>("start_color", CColor(1.0, 1.0, 1.0, 1.0));
				point.m_Color2 = colorPoint->GetAttribute<CColor>("end_color", CColor(1.0, 1.0, 1.0, 1.0));
				point.m_Time = colorPoint->GetAttribute<Vect2f>("time", Vect2f(0.0, 0.0));
				m_ControlPointColors.push_back(point);
				colorPoint = colorPoint->NextSiblingElement();
			}
		}

		CParticleSystemType::~CParticleSystemType()
		{
			m_ControlPointColors.clear();
			m_ControlPointSizes.clear();
		}
	}
}
