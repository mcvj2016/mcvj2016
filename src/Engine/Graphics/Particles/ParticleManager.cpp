#include "ParticleManager.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Logger/Logger.h"

namespace engine
{
	namespace particles
	{
		CParticleManager::CParticleManager()
			: m_XmlName("data/common/ParticlesSystem.xml")
		{
			
		}

		CParticleManager::~CParticleManager()
		{
			
		}

		void CParticleManager::NewParticle()
		{
			std::string name = "NewParticle (" + std::to_string(GetCount()) + ")";
			CParticleSystemType* particle = new CParticleSystemType(name);
			Add(name, particle);
		}

		void CParticleManager::NewSizeControlPoint(CParticleSystemType& particle)
		{
			CParticleSystemType::ControlPointSize point;
			point.m_Size = v2fZERO;
			point.m_Time = v2fZERO;
			particle.m_ControlPointSizes.push_back(point);
		}

		void CParticleManager::NewColorControlPoint(CParticleSystemType& particle)
		{
			CParticleSystemType::ControlPointColor point;
			point.m_Color1 = CColor(1.0, 1.0, 1.0, 1.0);
			point.m_Color2 = CColor(1.0, 1.0, 1.0, 1.0);
			point.m_Time = v2fZERO;
			particle.m_ControlPointColors.push_back(point);
		}

		void CParticleManager::Load(std::string aName)
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = false;
			if (aName == "")
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_XmlName.c_str()));
			}
			else
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(aName.c_str()));
			}
			if (loaded)
			{
				CXMLElement* particleSystems = xmldoc.FirstChildElement("particle_systems");
				if (particleSystems != nullptr)
				{
					//particle
					CXMLElement* particleElement = particleSystems->FirstChildElement("particle");
					while (particleElement != nullptr)
					{
						CParticleSystemType* l_ParticleSystemType = new CParticleSystemType(particleElement);
						this->Add(l_ParticleSystemType->GetName(), l_ParticleSystemType);
						particleElement = particleElement->NextSiblingElement("particle");
					}
				}
			}
			else
			{
				LOG_WARNING_APPLICATION("Particle Manager could not load the XML");
			}
			
		}

		void CParticleManager::Reload()
		{
			Destroy();
			Load();
		}

		void CParticleManager::Save()
		{
			tinyxml2::XMLDocument xmldoc;
			tinyxml2::XMLElement * particle_root = xmldoc.NewElement("particle_systems");
			xmldoc.InsertFirstChild(particle_root);

			for (size_t i = 0; i < GetCount(); ++i)
			{
				auto particle = GetByIndex(i);
				tinyxml2::XMLElement * particle_node = xmldoc.NewElement("particle");
				particle_root->InsertEndChild(particle_node);

				particle_node->SetAttribute("name", particle->GetName().c_str());
				particle_node->SetAttribute("material", particle->m_Material->GetName().c_str());
				particle_node->SetAttribute("max_num_particles", particle->m_MaxParticles);
				particle_node->SetAttribute("num_frames", particle->m_NumFrames);
				particle_node->SetAttribute("time_per_frame", particle->m_TimePerFrame);
				particle_node->SetAttribute("loop_frames", particle->m_LoopFrames);
				particle_node->SetAttribute("emit_absolute", particle->m_EmitAbsolute);
				particle_node->SetAttribute("starting_dir_angle", particle->m_StartingDirectionAngle);
				particle_node->SetAttribute("starting_acc_angle", particle->m_StartingAccelerationAngle);
				particle_node->SetAttribute("size", std::string(std::to_string(particle->m_Size.x) + " " + std::to_string(particle->m_Size.y)).c_str());
				particle_node->SetAttribute("emit_rate", std::string(std::to_string(particle->m_EmitRate.x) + " " + std::to_string(particle->m_EmitRate.y)).c_str());
				particle_node->SetAttribute("awake_time", std::string(std::to_string(particle->m_AwakeTime.x) + " " + std::to_string(particle->m_AwakeTime.y)).c_str());
				particle_node->SetAttribute("sleep_time", std::string(std::to_string(particle->m_SleepTime.x) + " " + std::to_string(particle->m_SleepTime.y)).c_str());
				particle_node->SetAttribute("life", std::string(std::to_string(particle->m_Life.x) + " " + std::to_string(particle->m_Life.y)).c_str());
				particle_node->SetAttribute("starting_angle", std::string(std::to_string(particle->m_StartingAngle.x) + " " + std::to_string(particle->m_StartingAngle.y)).c_str());
				particle_node->SetAttribute("starting_angular_speed", std::string(std::to_string(particle->m_StartingAngularSpeed.x) + " " + std::to_string(particle->m_StartingAngularSpeed.y)).c_str());
				particle_node->SetAttribute("angular_acceleration", std::string(std::to_string(particle->m_AngularAcceleration.x) + " " + std::to_string(particle->m_AngularAcceleration.y)).c_str());
				particle_node->SetAttribute("starting_speed_min", std::string(std::to_string(particle->m_StartingSpeed1.x) + " " + std::to_string(particle->m_StartingSpeed1.y) + " " + std::to_string(particle->m_StartingSpeed1.z)).c_str());
				particle_node->SetAttribute("starting_speed_max", std::string(std::to_string(particle->m_StartingSpeed2.x) + " " + std::to_string(particle->m_StartingSpeed2.y) + " " + std::to_string(particle->m_StartingSpeed2.z)).c_str());
				particle_node->SetAttribute("starting_acceleration_min", std::string(std::to_string(particle->m_StartingAcceleration1.x) + " " + std::to_string(particle->m_StartingAcceleration1.y) + " " + std::to_string(particle->m_StartingAcceleration1.z)).c_str());
				particle_node->SetAttribute("starting_acceleration_max", std::string(std::to_string(particle->m_StartingAcceleration2.x) + " " + std::to_string(particle->m_StartingAcceleration2.y) + " " + std::to_string(particle->m_StartingAcceleration2.z)).c_str());
				particle_node->SetAttribute("color_min", std::string(std::to_string(particle->m_Color1.x) + " " + std::to_string(particle->m_Color1.y) + " " + std::to_string(particle->m_Color1.z) + " " + std::to_string(particle->m_Color1.w)).c_str());
				particle_node->SetAttribute("color_max", std::string(std::to_string(particle->m_Color2.x) + " " + std::to_string(particle->m_Color2.y) + " " + std::to_string(particle->m_Color2.z) + " " + std::to_string(particle->m_Color2.w)).c_str());

				//sie points
				tinyxml2::XMLElement * sizeControlPoint = xmldoc.NewElement("SizeControlPoints");
				particle_node->InsertEndChild(sizeControlPoint);

				for (size_t j = 0; j < particle->m_ControlPointSizes.size(); ++j)
				{
					auto point = particle->m_ControlPointSizes[j];
					tinyxml2::XMLElement * sizeControlPointNode = xmldoc.NewElement("Point");
					sizeControlPoint->InsertEndChild(sizeControlPointNode);

					sizeControlPointNode->SetAttribute("size", std::string(std::to_string(point.m_Size.x) + " " + std::to_string(point.m_Size.y)).c_str());
					sizeControlPointNode->SetAttribute("time", std::string(std::to_string(point.m_Time.x) + " " + std::to_string(point.m_Time.y)).c_str());
				}

				//control point colors
				tinyxml2::XMLElement * controlColorPoint = xmldoc.NewElement("ColorControlPoints");
				particle_node->InsertEndChild(controlColorPoint);

				for (size_t k = 0; k < particle->m_ControlPointColors.size(); ++k)
				{
					auto colorPoint = particle->m_ControlPointColors[k];
					tinyxml2::XMLElement * colorControlPointNode = xmldoc.NewElement("Point");
					controlColorPoint->InsertEndChild(colorControlPointNode);

					colorControlPointNode->SetAttribute("start_color", std::string(std::to_string(colorPoint.m_Color1.x) + " " + std::to_string(colorPoint.m_Color1.y) + " " + std::to_string(colorPoint.m_Color1.z) + " " + std::to_string(colorPoint.m_Color1.w)).c_str());
					colorControlPointNode->SetAttribute("end_color", std::string(std::to_string(colorPoint.m_Color2.x) + " " + std::to_string(colorPoint.m_Color2.y) + " " + std::to_string(colorPoint.m_Color2.z) + " " + std::to_string(colorPoint.m_Color2.w)).c_str());
					colorControlPointNode->SetAttribute("time", std::string(std::to_string(colorPoint.m_Time.x) + " " + std::to_string(colorPoint.m_Time.y)).c_str());
				}
			}

			xmldoc.SaveFile(std::string("data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/particle_systems.xml").c_str());
		}
	}
}
