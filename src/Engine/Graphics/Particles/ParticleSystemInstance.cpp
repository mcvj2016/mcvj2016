#include "ParticleSystemInstance.h"
#include "XML/XML.h"
#include "Engine.h"
#include "ParticleManager.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Camera/CameraManager.h"

namespace engine
{
	namespace particles
	{
		CParticleSystemInstance::CParticleSystemInstance(const CXMLElement* aElement)
			: CSceneNode(aElement), m_ActiveParticles(0), m_RandomEngine(m_rnd()), m_UnitDistribution(0.0f, 1.0f)
		{
			CParticleManager& l_ParticleManager = CEngine::GetInstance().GetParticleManager();
			m_ParticleSystemType = l_ParticleManager(aElement->GetAttribute<std::string>("type",""));
			m_NextParticleEmission = aElement->GetAttribute("next_particle_emission", 1.0f);
			m_Awake = aElement->GetAttribute("awake", false);
			m_AwakeTimer = aElement->GetAttribute("awake_timer", 1.0f);
			m_EmissionBoxHalfSize = aElement->GetAttribute<Vect3f>("emission_box_size", Vect3f(1.0, 1.0, 1.0)) * 0.5f;
			m_EmissionVolume = m_EmissionBoxHalfSize.x * m_EmissionBoxHalfSize.y * m_EmissionBoxHalfSize.z * 8;
			m_EmissionScaler = m_ParticleSystemType->m_EmitAbsolute ? 1 : 1.0f / m_EmissionVolume;
			m_MaxParticlesPerInstance = m_ParticleSystemType->m_MaxParticles;
			m_ParticleData = new ParticleData[m_MaxParticlesPerInstance];
			m_ParticleRenderableData = new shaders::PositionColorUVUV2[m_MaxParticlesPerInstance];
			buffers::CVertexBuffer<shaders::PositionColorUVUV2>* l_ParticlesBuffer = new buffers::CVertexBuffer<shaders::PositionColorUVUV2>(CEngine::GetInstance().GetRenderManager(), m_ParticleRenderableData, m_MaxParticlesPerInstance, true);
			m_RenderableVertex = new buffers::CGeometryPointList<shaders::PositionColorUVUV2>(l_ParticlesBuffer);
		}

		CParticleSystemInstance::~CParticleSystemInstance()
		{
			base::utils::CheckedDelete(m_ParticleData);
			base::utils::CheckedDelete(m_ParticleRenderableData);
			base::utils::CheckedDelete(m_RenderableVertex);
		}
		void CParticleSystemInstance::ResetParticles()
		{
			for (int i = 0; i < m_ActiveParticles; ++i)
			{
				ParticleData *particle = &m_ParticleData[i];
				particle->Position = GetRandomValue(-m_EmissionBoxHalfSize, m_EmissionBoxHalfSize);
				particle->Velocity = GetRandomValue(m_ParticleSystemType->m_StartingSpeed1, m_ParticleSystemType->m_StartingSpeed2);
				particle->Acceleration = GetRandomValue(m_ParticleSystemType->m_StartingAcceleration1, m_ParticleSystemType->m_StartingAcceleration2);
				
				particle->SizeControlPoint = 0;
				particle->LastSizeControlTime = 0;
				if (m_ParticleSystemType->m_ControlPointColors.size() > 0)
				{
					particle->LastSize = GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[0].m_Size);
				}

				particle->NextSizeControlTime = m_ParticleSystemType->m_ControlPointSizes.size() < 2 ? particle->LifeDurationTime : GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[1].m_Time);
				particle->NextSize = m_ParticleSystemType->m_ControlPointSizes.size() < 2 ? particle->LastSize : GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[1].m_Size);

				particle->ColorControlPoint = 0;
				particle->LastColorControlTime = 0;
				if (m_ParticleSystemType->m_ControlPointColors.size() > 0)
				{
					particle->LastColor = GetRandomValue(m_ParticleSystemType->m_ControlPointColors[0].m_Color1, m_ParticleSystemType->m_ControlPointColors[0].m_Color2);
				}

				particle->NextColorControlTime = m_ParticleSystemType->m_ControlPointColors.size() < 2 ? particle->LifeDurationTime : GetRandomValue(m_ParticleSystemType->m_ControlPointColors[1].m_Time);
				particle->NextColor = m_ParticleSystemType->m_ControlPointColors.size() < 2 ? particle->LastColor : GetRandomValue(m_ParticleSystemType->m_ControlPointColors[1].m_Color1, m_ParticleSystemType->m_ControlPointColors[1].m_Color2);

				particle->Angle = GetRandomValue(m_ParticleSystemType->m_StartingAngle.x, m_ParticleSystemType->m_StartingAngle.y);

				particle->CurrentFrame = 0;
				particle->TimeToNextFrame = m_ParticleSystemType->m_TimePerFrame;

				particle->ElapsedTimeSinceCreated = 0.0f;
				particle->LifeDurationTime = GetRandomValue(m_ParticleSystemType->m_Life);
				m_NextParticleEmission = 1;
			}
		}

		void CParticleSystemInstance::Update(float ElapsedTime)
		{
			
			//added condition forinsta spawn
			if (!m_ParticleSystemType->m_InstantSpawn)
			{
				m_AwakeTimer -= ElapsedTime;
				while (m_AwakeTimer < 0)
				{
					m_Awake = !m_Awake;
					m_AwakeTimer += GetRandomValue(m_Awake ? m_ParticleSystemType->m_AwakeTime : m_ParticleSystemType->m_SleepTime);
				}
			}
			else
			{
				m_Awake = true;
			}

			if (m_Awake)
			{
				m_NextParticleEmission -= ElapsedTime;
				while (m_NextParticleEmission < 0)
				{
					if (m_ActiveParticles < m_MaxParticlesPerInstance)
					{
						ParticleData particle = {};

						particle.Position = GetRandomValue(-m_EmissionBoxHalfSize, m_EmissionBoxHalfSize);
						particle.Velocity = GetRandomValue(m_ParticleSystemType->m_StartingSpeed1, m_ParticleSystemType->m_StartingSpeed2);
						particle.Acceleration = GetRandomValue(m_ParticleSystemType->m_StartingAcceleration1, m_ParticleSystemType->m_StartingAcceleration2);

						particle.SizeControlPoint = 0;
						particle.LastSizeControlTime = 0;
						if (m_ParticleSystemType->m_ControlPointColors.size() > 0)
						{
							particle.LastSize = GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[0].m_Size);
						}
						
						particle.NextSizeControlTime = m_ParticleSystemType->m_ControlPointSizes.size() < 2 ? particle.LifeDurationTime : GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[1].m_Time);
						particle.NextSize = m_ParticleSystemType->m_ControlPointSizes.size() < 2 ? particle.LastSize : GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[1].m_Size);

						particle.ColorControlPoint = 0;
						particle.LastColorControlTime = 0;
						if (m_ParticleSystemType->m_ControlPointColors.size() > 0)
						{
							particle.LastColor = GetRandomValue(m_ParticleSystemType->m_ControlPointColors[0].m_Color1, m_ParticleSystemType->m_ControlPointColors[0].m_Color2);
						}

						particle.NextColorControlTime = m_ParticleSystemType->m_ControlPointColors.size() < 2 ? particle.LifeDurationTime : GetRandomValue(m_ParticleSystemType->m_ControlPointColors[1].m_Time);
						particle.NextColor = m_ParticleSystemType->m_ControlPointColors.size() < 2 ? particle.LastColor : GetRandomValue(m_ParticleSystemType->m_ControlPointColors[1].m_Color1, m_ParticleSystemType->m_ControlPointColors[1].m_Color2);

						particle.Angle = GetRandomValue(m_ParticleSystemType->m_StartingAngle.x, m_ParticleSystemType->m_StartingAngle.y);

						particle.CurrentFrame = 0;
						particle.TimeToNextFrame = m_ParticleSystemType->m_TimePerFrame;

						particle.ElapsedTimeSinceCreated = 0.0f;
						particle.LifeDurationTime = GetRandomValue(m_ParticleSystemType->m_Life);

						m_ParticleData[m_ActiveParticles] = particle;
						++m_ActiveParticles;
					}
					m_NextParticleEmission += ComputeTimeToNextParticle();
				}
			}

			for (int i = 0; i < m_ActiveParticles;++i)
			{
				ParticleData *particle = &m_ParticleData[i];
				particle->Position += particle->Velocity*ElapsedTime + 0.5f*ElapsedTime*ElapsedTime*particle->Acceleration;
				particle->Velocity += particle->Acceleration*ElapsedTime;

				particle->TimeToNextFrame -= ElapsedTime;
				particle->ElapsedTimeSinceCreated += ElapsedTime;

				particle->DistanceFromCamera = DistanceFromCamera(particle);

				while (particle->TimeToNextFrame < 0 && (m_ParticleSystemType->m_LoopFrames || particle->CurrentFrame < m_ParticleSystemType->m_NumFrames - 1))
				{
					particle->CurrentFrame = (particle->CurrentFrame + 1) % m_ParticleSystemType->m_NumFrames;
					particle->TimeToNextFrame = m_ParticleSystemType->m_TimePerFrame;
				}

				while (particle->ElapsedTimeSinceCreated > particle->NextSizeControlTime && particle->ElapsedTimeSinceCreated < particle->LifeDurationTime)
				{
					++particle->SizeControlPoint;

					particle->LastSize = particle->NextSize;
					particle->LastSizeControlTime = particle->NextSizeControlTime;

					if (static_cast<size_t>(particle->SizeControlPoint) + 1 < m_ParticleSystemType->m_ControlPointSizes.size())
					{
						particle->NextSize = GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[particle->SizeControlPoint + 1].m_Size);
						particle->NextSizeControlTime = GetRandomValue(m_ParticleSystemType->m_ControlPointSizes[particle->SizeControlPoint + 1].m_Time);
					}
					else
					{
						particle->NextSizeControlTime = particle->LifeDurationTime;
					}
				}

				while (particle->ElapsedTimeSinceCreated > particle->NextColorControlTime && particle->ElapsedTimeSinceCreated < particle->LifeDurationTime)
				{
					++particle->ColorControlPoint;

					particle->LastColor = particle->NextColor;
					particle->LastColorControlTime = particle->NextColorControlTime;

					if (static_cast<size_t>(particle->ColorControlPoint) + 1 < m_ParticleSystemType->m_ControlPointColors.size())
					{
						particle->NextColor = GetRandomValue(m_ParticleSystemType->m_ControlPointColors[particle->ColorControlPoint + 1].m_Color1,
							m_ParticleSystemType->m_ControlPointColors[particle->ColorControlPoint + 1].m_Color2);
						particle->NextColorControlTime = GetRandomValue(m_ParticleSystemType->m_ControlPointColors[particle->ColorControlPoint + 1].m_Time);
					}
					else
					{
						particle->NextColorControlTime = particle->LifeDurationTime;
					}
				}

				if (m_ParticleData[i].ElapsedTimeSinceCreated > m_ParticleData[i].LifeDurationTime)
				{
					--m_ActiveParticles;
					m_ParticleData[i] = m_ParticleData[m_ActiveParticles];
					--i;
				}
			}
			if (m_ActiveParticles>1)
			{
				InsertSort(m_ParticleData, m_ActiveParticles);
			}
		}

		bool CParticleSystemInstance::Render(render::CRenderManager& aRendermanager)
		{
			for (int i = 0; i < m_ActiveParticles; ++i)
			{
				ParticleData *particle = &m_ParticleData[i];

				m_ParticleRenderableData[i].position = particle->Position;

				float ColorControlAlpha = (particle->ElapsedTimeSinceCreated < particle->NextColorControlTime) ?
					(particle->ElapsedTimeSinceCreated - particle->LastColorControlTime) / (particle->NextColorControlTime - particle->LastColorControlTime) : 1.0f;
				m_ParticleRenderableData[i].color = particle->LastColor.Lerp(particle->NextColor, ColorControlAlpha);

				float SizeControlAlpha = (particle->ElapsedTimeSinceCreated < particle->NextSizeControlTime) ?
					(particle->ElapsedTimeSinceCreated - particle->LastSizeControlTime) / (particle->NextSizeControlTime - particle->LastSizeControlTime) :	
					1.0f;
				m_ParticleRenderableData[i].uv.x = mathUtils::Lerp<float>(particle->LastSize, particle->NextSize, SizeControlAlpha);
				m_ParticleRenderableData[i].uv.y = particle->Angle;
				m_ParticleRenderableData[i].uv2.x = static_cast<float>(particle->CurrentFrame);
				m_ParticleRenderableData[i].uv2.y = 0;
			}

			if (m_ActiveParticles > 0)
			{
				buffers::CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
				lCB.mObjDesc.m_World = GetMatrix();
				lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), buffers::CConstantBufferManager::CB_Object);
				lCB.BindGSBuffer(aRendermanager.GetDeviceContext(), buffers::CConstantBufferManager::CB_FrameGS);
				materials::CMaterial* l_Material = m_ParticleSystemType->m_Material;
				l_Material->Apply();
				m_RenderableVertex->UpdateVertexs(m_ParticleRenderableData, m_ActiveParticles);
				m_RenderableVertex->Render(aRendermanager.GetDeviceContext(), m_ActiveParticles);
				l_Material->ApplyUnbind();
			}			

			return true;
		}

		float CParticleSystemInstance::DistanceFromCamera(ParticleData *particle)
		{
			CCameraController* camera = CEngine::GetInstance().GetCameraManager().GetMainCamera();
			Vect3f Distance = particle->Position - camera->GetSceneCamera()->GetPosition();
			return Distance * camera->GetSceneCamera()->GetForward();
		}

		void CParticleSystemInstance::InsertSort(ParticleData particles[], int length) {
			int i, j;
			ParticleData tmp;
			for (i = 1; i < length; ++i) {
				j = i;
				while (j > 0 && particles[j - 1].DistanceFromCamera < particles[j].DistanceFromCamera) {
					tmp = particles[j];
					particles[j] = particles[j - 1];
					particles[j - 1] = tmp;
					--j;
				}
			}
		}

		float CParticleSystemInstance::GetRandomValue(float min, float max)
		{
			float a = m_UnitDistribution(m_RandomEngine);
			float value = mathUtils::Lerp(min, max, a);
			return value;
		}

		Vect3f CParticleSystemInstance::GetRandomValue(Vect3f min, Vect3f max)
		{
			float a1 = m_UnitDistribution(m_RandomEngine);
			float a2 = m_UnitDistribution(m_RandomEngine);
			float a3 = m_UnitDistribution(m_RandomEngine);
			Vect3f value;
			value.x = mathUtils::Lerp(min.x, max.x, a1);
			value.y = mathUtils::Lerp(min.y, max.y, a2);
			value.z = mathUtils::Lerp(min.z, max.z, a3);
			return value;
		}

		CColor CParticleSystemInstance::GetRandomValue(CColor min, CColor max)
		{
			float a = m_UnitDistribution(m_RandomEngine);
			CColor value = min.Lerp(max, a);
			return value;
		}

		float CParticleSystemInstance::GetRandomValue(Vect2f value)
		{
			return GetRandomValue(value.x, value.y);
		}

		float CParticleSystemInstance::ComputeTimeToNextParticle()
		{
			float particlePerSecPerM3 = GetRandomValue(m_ParticleSystemType->m_EmitRate);
			return m_EmissionScaler / particlePerSecPerM3;
		}
	}
}
