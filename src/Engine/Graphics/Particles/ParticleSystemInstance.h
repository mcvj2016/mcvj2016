#ifndef _ENGINE_PARTICLESYSTEMINSTANCE_23042017194800_H
#define _ENGINE_PARTICLESYSTEMINSTANCE_23042017194800_H

#include "ParticleSystemType.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Graphics/Shaders/VertexType.h"
#include "Graphics/Buffers/TemplatedGeometry.h"
#include <random>

//#define m_MaxParticlesPerInstance 10

namespace engine
{
	namespace particles
	{
		class CParticleSystemInstance : public scenes::CSceneNode
		{
		public:

			struct ParticleData
			{
				Vect3f Position, Velocity, Acceleration;
				int CurrentFrame;
				float TimeToNextFrame;
				float ElapsedTimeSinceCreated, LifeDurationTime;
				float Angle, AngularSpeed, AngularAcceleration;
				float DistanceFromCamera;

				int ColorControlPoint, SizeControlPoint;

				float LastColorControlTime, NextColorControlTime;
				float LastSizeControlTime, NextSizeControlTime;

				CColor LastColor, NextColor;
				float LastSize, NextSize;
			};

			CParticleSystemInstance(){};
			CParticleSystemInstance(const CXMLElement* aElement);
			virtual ~CParticleSystemInstance();

			int m_ActiveParticles;
			void ResetParticles();
			void Destroy();
			void Update(float ElapsedTime);
			bool Render(render::CRenderManager& aRendermanager) override;

			float m_NextParticleEmission;
			bool m_Awake;
			float m_AwakeTimer;
			CParticleSystemType *m_ParticleSystemType;
			Vect3f m_EmissionBoxHalfSize;
			float m_EmissionVolume, m_EmissionScaler;

			ParticleData* GetParticleData()
			{
				return m_ParticleData;
			}

			float DistanceFromCamera(ParticleData *particle);
			void InsertSort(ParticleData arr[], int length);
			float GetRandomValue(float min, float max);
			Vect3f GetRandomValue(Vect3f min, Vect3f max);
			CColor GetRandomValue(CColor min, CColor max);
			float GetRandomValue(Vect2f value);			
			float ComputeTimeToNextParticle();

		private:
			int m_MaxParticlesPerInstance;
			ParticleData* m_ParticleData;// [m_MaxParticlesPerInstance];

			shaders::PositionColorUVUV2* m_ParticleRenderableData;// [m_MaxParticlesPerInstance];
			buffers::CGeometryPointList<shaders::PositionColorUVUV2>* m_RenderableVertex;

			std::mt19937 m_RandomEngine;
			std::uniform_real_distribution<float> m_UnitDistribution;
			std::random_device m_rnd;
		};
	}
}

#endif