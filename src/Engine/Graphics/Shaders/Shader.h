#ifndef _ENGINE_SHADER_DPVD1_22122016103300_H
#define _ENGINE_SHADER_DPVD1_22122016103300_H

#include <d3d11.h>
#include "Utils/Name.h"
#include "Utils/EnumToString.h"
#include "XML/XML.h"
#include <vector>

namespace engine
{
	namespace shaders{
		class CShader : public CName
		{
		public:
			enum EShaderStage
			{
				eVertexShader = 0,
				eGeometryShader,
				ePixelShader,
				EStageCount
			};
		public:
			CShader(const std::string& aShaderCode, EShaderStage aType);
			CShader(const CXMLElement* aElement, EShaderStage aType);
			virtual ~CShader();
			//TODO IMPLEMENATAR EL DESTROY?
			//void Destroy();
			virtual bool Load();
			virtual void Bind(ID3D11DeviceContext*) = 0;
			virtual void Unbind(ID3D11DeviceContext*) = 0;
			GET_SET(EShaderStage, Type)
			GET_SET_REF(std::string, Preprocessor)
			GET_SET_REF(std::string, EntryPoint)
			GET_SET_REF(std::string, Filename)
			GET_SET_REF(std::string, ShaderCode)
			GET_SET_PTR(ID3DBlob, Blob);
			//TODO IMPLEMENATAR EL RELOAD?
			bool Reload();
		protected:
			DISALLOW_COPY_AND_ASSIGN(CShader);
			EShaderStage m_Type;
			std::string m_Preprocessor;
			std::string m_EntryPoint;
			std::string m_Filename;
			std::string m_ShaderCode;
			//TODO Implementar las shaderMacros
			D3D_SHADER_MACRO *m_ShaderMacros;
			std::vector<std::string> m_PreprocessorMacros;
			void CreateShaderMacro();
			std::string CreateMacroHash() const;

			ID3DBlob *m_Blob;
			virtual std::string GetShaderModel() = 0;
		};
	}
}

Begin_Enum_String(engine::shaders::CShader::EShaderStage)
{
	Enum_String_Id(engine::shaders::CShader::eVertexShader, "vertex");
	Enum_String_Id(engine::shaders::CShader::eGeometryShader, "geometry");
	Enum_String_Id(engine::shaders::CShader::ePixelShader, "pixel");
}
End_Enum_String;

#endif