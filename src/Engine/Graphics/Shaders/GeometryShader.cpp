#include "GeometryShader.h"
#include "Engine.h"
#include "Render/RenderManager.h"

namespace engine
{
	namespace shaders
	{
		CGeometryShader::CGeometryShader(const std::string& aShaderCode) :CShader(aShaderCode, eGeometryShader)
		{
		}

		CGeometryShader::CGeometryShader(const CXMLElement* aElement) : CShader(aElement, eGeometryShader)
		{
		}

		CGeometryShader::~CGeometryShader()
		{
			
		}

		bool CGeometryShader::Load()
		{
			bool lOk = CShader::Load();
			if (lOk)
			{
				render::CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
				ID3D11Device *l_Device = lRenderManager.GetDevice();
				HRESULT lHR = l_Device->CreateGeometryShader(m_Blob->GetBufferPointer(), m_Blob->GetBufferSize(), nullptr, &m_GeometryShader);
				lOk = SUCCEEDED(lHR);
			}
			return lOk;
		}

		void CGeometryShader::Bind(ID3D11DeviceContext* aContext)
		{
			aContext->GSSetShader(m_GeometryShader, NULL, 0);
		}

		void CGeometryShader::Unbind(ID3D11DeviceContext* aContext)
		{
			aContext->GSSetShader(nullptr, NULL, 0);
		}

		std::string CGeometryShader::GetShaderModel()
		{
			// Query the current feature level:
			D3D_FEATURE_LEVEL featureLevel = CEngine::GetInstance().GetRenderManager().GetDevice()->GetFeatureLevel();
			switch (featureLevel)
			{
			case D3D_FEATURE_LEVEL_11_1:
			case D3D_FEATURE_LEVEL_11_0:
			{
										   return "gs_5_0";
			}
				break;
			case D3D_FEATURE_LEVEL_10_1:
			{
										   return "gs_4_1";
			}
				break;
			case D3D_FEATURE_LEVEL_10_0:
			{
										   return "gs_4_0";
			}
				break;
			case D3D_FEATURE_LEVEL_9_3:
			{
										  return "gs_4_0_level_9_3";
			}
				break;
			case D3D_FEATURE_LEVEL_9_2:
			case D3D_FEATURE_LEVEL_9_1:
			{
										  return "gs_4_0_level_9_1";
			}
				break;
			}
			return "";
		}
	}
}
