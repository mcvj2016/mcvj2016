#include "VertexType.h"
#include "VertexShader.h"
#include "Engine.h"

namespace engine
{
	namespace shaders
	{
		CVertexShader::CVertexShader(const std::string& aShaderCode, uint32 aVertexFlags):CShader(aShaderCode,CShader::eVertexShader),m_VertexFlags(aVertexFlags)
		{
		}

		CVertexShader::CVertexShader(const CXMLElement* aElement) : CShader(aElement, CShader::eVertexShader)
		{
			m_VertexFlags = GetFlagsFromString(aElement->GetAttribute<std::string>("vertex_type", ""));
		}

		CVertexShader::~CVertexShader()
		{
			m_VertexLayout->Release();
			m_VertexShader->Release();
		}

		bool CVertexShader::Load()
		{
			bool lOk = CShader::Load();
			if (lOk)
			{
				render::CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
				HRESULT lHR = lRenderManager.GetDevice()->CreateVertexShader(m_Blob->GetBufferPointer(), m_Blob->GetBufferSize(), nullptr, &m_VertexShader);
				lOk = SUCCEEDED(lHR);
				if (lOk)
				{
					lOk &= m_VertexFlags != 0;
					lOk &= CreateInputLayout(lRenderManager, m_VertexFlags, m_Blob, &m_VertexLayout);
				}
			}
			return lOk;
		}

		void CVertexShader::Bind(ID3D11DeviceContext* aContext)
		{
			// Configure how the CPU will send the vertexes to the GPU( COORDS | NORMALS | COLOR | UV ... )
			aContext->IASetInputLayout(m_VertexLayout);
			// Bind the vertex shader and its uniform data across all the vertexes
			aContext->VSSetShader(m_VertexShader, NULL, 0);
		}

		void CVertexShader::Unbind(ID3D11DeviceContext* aContext)
		{
			// Configure how the CPU will send the vertexes to the GPU( COORDS | NORMALS | COLOR | UV ... )
			aContext->IASetInputLayout(nullptr);
			// Bind the vertex shader and its uniform data across all the vertexes
			aContext->VSSetShader(nullptr, NULL, 0);
		}

		std::string CVertexShader::GetShaderModel()
		{
			// Query the current feature level:
			D3D_FEATURE_LEVEL featureLevel = CEngine::GetInstance().GetRenderManager().GetDevice()->GetFeatureLevel();
			switch (featureLevel)
			{
			case D3D_FEATURE_LEVEL_11_1:
			case D3D_FEATURE_LEVEL_11_0:
			{
										   return "vs_5_0";
			}
				break;
			case D3D_FEATURE_LEVEL_10_1:
			{
										   return "vs_4_1";
			}
				break;
			case D3D_FEATURE_LEVEL_10_0:
			{
										   return "vs_4_0";
			}
				break;
			case D3D_FEATURE_LEVEL_9_3:
			{
										  return "vs_4_0_level_9_3";
			}
				break;
			case D3D_FEATURE_LEVEL_9_2:
			case D3D_FEATURE_LEVEL_9_1:
			{
										  return "vs_4_0_level_9_1";
			}
				break;
			} // switch( featureLevel )
			return "";
		}
	}
}
