#ifndef _ENGINE_SHADERUTILS_DPVD1_22122016155700_H
#define _ENGINE_SHADERUTILS_DPVD1_22122016155700_H

#include "Utils/CheckedDelete.h"
#include <d3d11.h>

static std::string sShadersDirectory = "data/common/shaders/";

namespace engine
{
	namespace shaders
	{
		class CShaderInclude : public ID3DInclude
		{
		public:
			virtual ~CShaderInclude();
			CShaderInclude();

			HRESULT __stdcall Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes);
			
			HRESULT __stdcall Close(LPCVOID pData);
			
		};
		ID3DBlob* CompileShader(const std::string& aShader, const std::string& aEntryPoint, const std::string& aShaderModel,
			const D3D_SHADER_MACRO* aDefines);
		
	}
}
#endif