#ifndef _ENGINE_GEOMETRYSHADER_DPVD1_28042017135000_H
#define _ENGINE_GEOMETRYSHADER_DPVD1_28042017135000_H

#include "Shader.h"

namespace engine
{
	namespace shaders{
		class CGeometryShader : public CShader
		{
		public:
			CGeometryShader(const std::string& aShaderCode);
			CGeometryShader(const CXMLElement* aElement);
			virtual ~CGeometryShader();
			virtual bool Load();
			virtual void Bind(ID3D11DeviceContext* aContext);
			virtual void Unbind(ID3D11DeviceContext* aContext);
			GET_SET_PTR(ID3D11GeometryShader, GeometryShader);
		private:
			DISALLOW_COPY_AND_ASSIGN(CGeometryShader);
			ID3D11GeometryShader *m_GeometryShader;
			virtual std::string GetShaderModel();
		};
	}
}
#endif