#include "ShaderManager.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "GeometryShader.h"
#include "Utils/CheckedDelete.h"

namespace engine
{
	namespace shaders
	{

		CShaderManager::CShaderManager():m_Filename("data/common/shaders.xml")
		{
			m_Library.resize(CShader::EStageCount);
		}
		CShaderManager::~CShaderManager()
		{
			for (size_t i = 0; i < m_Library.size(); i++)
			{
				m_Library[i].Destroy();
			}
			m_Library.clear();
		}

		bool CShaderManager::Load()
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Shaders";
			}

			tinyxml2::XMLElement* shaders = xmldoc.FirstChildElement("shaders");
			if (shaders != nullptr)
			{
				TShadersLibrary& lVertexLibrary = m_Library[CShader::eVertexShader];
				tinyxml2::XMLElement* vertexShaders = shaders->FirstChildElement("vertex_shaders");
				if (vertexShaders != nullptr)
				{
					tinyxml2::XMLElement* vShader = vertexShaders->FirstChildElement("shader");
					while (vShader != nullptr)
					{
						CVertexShader* VertexShader = new CVertexShader(vShader);
						VertexShader->Load();
						lVertexLibrary.Add(VertexShader->GetName(), VertexShader);
						vShader = vShader->NextSiblingElement("shader");
					}
				}

				TShadersLibrary& lGeometryLibrary = m_Library[CShader::eGeometryShader];
				tinyxml2::XMLElement* geometryShaders = shaders->FirstChildElement("geometry_shaders");
				if (geometryShaders != nullptr)
				{
					tinyxml2::XMLElement* gShader = geometryShaders->FirstChildElement("shader");
					while (gShader != nullptr)
					{
						CGeometryShader* GeometryShader = new CGeometryShader(gShader);
						GeometryShader->Load();
						lGeometryLibrary.Add(GeometryShader->GetName(), GeometryShader);
						gShader = gShader->NextSiblingElement("shader");
					}
				}

				TShadersLibrary& lPixelLibrary = m_Library[CShader::ePixelShader];
				tinyxml2::XMLElement* pixelShaders = shaders->FirstChildElement("pixel_shaders");
				if (pixelShaders != nullptr)
				{
					tinyxml2::XMLElement* pShader = pixelShaders->FirstChildElement("shader");
					while (pShader != nullptr)
					{
						CPixelShader* PixelShader = new CPixelShader(pShader);
						PixelShader->Load();
						if (!lPixelLibrary.Add(PixelShader->GetName(), PixelShader))
						{
							base::utils::CheckedDelete(PixelShader);
						}
							
						pShader = pShader->NextSiblingElement("shader");
					}
				}
			}
			return true;
		}

		CShader* CShaderManager::GetShader(CShader::EShaderStage aStage, const std::string& aShaderName)
		{
			return (m_Library[aStage])(aShaderName);
		}

		bool CShaderManager::Reload()
		{
			for (size_t i = 0; i < CShader::EStageCount; i++)
			{
				for (size_t j = 0; j < m_Library[i].GetCount(); j++)
				{
					m_Library[i].operator[](j)->Load();
				}
			}
			return true;
		}
	}
}
