#include "Shader.h"
#include "ShaderUtils.h"
#include "Utils/StringUtils.h"
#include <fstream>
#include <d3dcompiler.h>
#include "Utils/FileUtils.h"
#include <filesystem>

namespace engine
{
	namespace shaders
	{
		CShader::CShader(const std::string& aShaderCode, EShaderStage aType) : m_ShaderCode(aShaderCode), m_Type(aType)
		{
		}

		CShader::CShader(const CXMLElement* aElement, EShaderStage aType) : m_Type(aType)
		{
			m_Name = aElement->Attribute("name");
			m_Filename = aElement->Attribute("file");
			m_EntryPoint = aElement->Attribute("entry_point");
			m_Preprocessor = aElement->Attribute("preprocessor");
		}

		CShader::~CShader()
		{
			m_Blob->Release();
			base::utils::CheckedDelete(m_ShaderMacros);
		}

		bool CShader::Load()
		{
			if (!m_Filename.empty())
			{
				std::ifstream lStream("data/common/shaders/" + m_Filename);
				m_ShaderCode = std::string((std::istreambuf_iterator<char>(lStream)), (std::istreambuf_iterator<char>()));
			}
			if (!m_ShaderCode.empty())
			{
				/*
				CreateShaderMacro();
				m_Blob = CompileShader(m_ShaderCode, m_EntryPoint, GetShaderModel(), m_ShaderMacros);
				/*/
				const std::string& fileOrig = std::string("data/common/shaders/" + m_Filename + "." + CreateMacroHash() + ".blob");
				const std::string& fileNew = std::string("data/common/shaders/" + m_Filename);
				std::wstring stemp = base::utils::StringToWs(fileOrig);
				LPCWSTR result = stemp.c_str();


				if (base::utils::DoesFileExists(fileOrig))
				{
					CreateShaderMacro();
					
					if (base::utils::IsFileTimeLessThan(fileOrig, fileNew)){

						m_Blob = CompileShader(m_ShaderCode, m_EntryPoint, GetShaderModel(), m_ShaderMacros);
						HRESULT hr = D3DWriteBlobToFile(m_Blob, result, true);
						assert(SUCCEEDED(hr));
					}
					else
					{
						HRESULT hr = D3DReadFileToBlob(result, &m_Blob);
						assert(SUCCEEDED(hr));
					}
				}
				else
				{
					CreateShaderMacro();
					m_Blob = CompileShader(m_ShaderCode, m_EntryPoint, GetShaderModel(), m_ShaderMacros);
					HRESULT hr = D3DWriteBlobToFile(m_Blob, result, true);
					assert(SUCCEEDED(hr));
				}
				//*/
			}
			return m_Blob != nullptr;
		}

		void CShader::CreateShaderMacro()
		{
			m_PreprocessorMacros.clear();
			if (m_Preprocessor.empty())
			{
				m_ShaderMacros = NULL;
				return;
			}
			std::vector<std::string> l_PreprocessorItems = base::utils::Split(m_Preprocessor, ';');
			m_ShaderMacros = new D3D10_SHADER_MACRO[l_PreprocessorItems.size() + 1];
			for (size_t i = 0; i<l_PreprocessorItems.size(); ++i)
			{
				std::vector<std::string> l_PreprocessorItem = base::utils::Split(l_PreprocessorItems[i], '=');
				if (l_PreprocessorItem.size() == 1)
				{
					m_PreprocessorMacros.push_back(l_PreprocessorItems[i]);
					m_PreprocessorMacros.push_back("1");
				}
				else if (l_PreprocessorItem.size() == 2)
				{
					m_PreprocessorMacros.push_back(l_PreprocessorItem[0]);
					m_PreprocessorMacros.push_back(l_PreprocessorItem[1]);
				}
				else
				{
					base::utils::DeleteArray(m_ShaderMacros);
					return;
				}
			}
			for (size_t i = 0; i<l_PreprocessorItems.size(); ++i)
			{
				m_ShaderMacros[i].Name = m_PreprocessorMacros[i * 2].c_str();
				m_ShaderMacros[i].Definition = m_PreprocessorMacros[(i * 2) + 1].c_str();
			}
			m_ShaderMacros[l_PreprocessorItems.size()].Name = NULL;
			m_ShaderMacros[l_PreprocessorItems.size()].Definition = NULL;
		}

		std::string CShader::CreateMacroHash() const
		{
			std::hash<std::string> hash;
			return std::to_string(hash(m_Preprocessor));
		}
	}
}
