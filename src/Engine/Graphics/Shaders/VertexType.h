#ifndef _ENGINE_VERTEXTYPE_DPVD1_21122016174800_H
#define _ENGINE_VERTEXTYPE_DPVD1_21122016174800_H

#include "Render/RenderManager.h"
#include "Utils/VertexTypes_HelperMacros.h"
#include <string>
#include <cassert>

namespace engine
{
	namespace shaders
	{
		enum EFlags
		{
			ePosition = 0x0001,
			eNormal = 0x0002,
			eTangent = 0x0004,
			eBinormal = 0x0008,
			eUV = 0x0010,
			eUV2 = 0x0020,
			eColor = 0x0040,
			ePosition4 = 0x0080,
			eDummy = 0x0100,
			eBump = eNormal | eTangent | eBinormal,
			eWeight = 0x0200,
			eIndices = 0x0400
		};

		/*struct PositionColor
		{
		Vect3f position;
		Vect4f color;
		static bool CreateInputLayout(engine::render::CRenderManager &RenderManager, ID3DBlob *VSBlob, ID3D11InputLayout **VertexLayout)
		{
		D3D11_INPUT_ELEMENT_DESC l_Layout[] =
		{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		UINT l_NumElements = ARRAYSIZE(l_Layout);
		HRESULT l_HR = RenderManager.GetDevice()->CreateInputLayout(l_Layout, l_NumElements, VSBlob->GetBufferPointer(), VSBlob->GetBufferSize(), VertexLayout);
		return !FAILED(l_HR);
		}
		static unsigned int GetVertexFlags()
		{
		return ePosition | eColor;
		}
		};*/

		/*struct PositionUV
		{
		POSITION; UV;
		GET_VERTEX_FLAGS(ePosition | eUV);
		BEGIN_INPUT_LAYOUT
		{
		LAYOUT_POSITION(0),
		LAYOUT_UV(12),
		}
		END_INPUT_LAYOUT
		};*/

		struct Position
		{
			POSITION;
			GET_VERTEX_FLAGS(ePosition);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0)
			}
			END_INPUT_LAYOUT
		};

		struct PositionNormal
		{
			POSITION; NORMAL;
			GET_VERTEX_FLAGS(ePosition | eNormal);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_NORMAL(12),
			}
			END_INPUT_LAYOUT
		};

		struct PositionNormalUV
		{
			POSITION; NORMAL; UV;
			GET_VERTEX_FLAGS(ePosition | eNormal | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_NORMAL(12),
				LAYOUT_UV(24),
			}
			END_INPUT_LAYOUT
		};

		struct PositionUV
		{
			POSITION; UV;
			GET_VERTEX_FLAGS(ePosition | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_UV(12)
			}
			END_INPUT_LAYOUT
		};

		struct PositionNormalUVUV2
		{
			POSITION; NORMAL; UV; UV2;
			GET_VERTEX_FLAGS(ePosition | eNormal | eUV | eUV2);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_NORMAL(12),
				LAYOUT_UV(24),
				LAYOUT_UV2(32),
			}
			END_INPUT_LAYOUT
		};

		struct PositionBump
		{
			POSITION; BUMP;
			GET_VERTEX_FLAGS(ePosition | eBump);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_BUMP(12),
			}
			END_INPUT_LAYOUT
		};

		struct PositionBumpUV
		{
			POSITION; BUMP; UV;
			GET_VERTEX_FLAGS(ePosition | eBump | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_BUMP(12),
				LAYOUT_UV(56),
			}
			END_INPUT_LAYOUT
		};

		struct PositionBumpUVUV2
		{
			POSITION; BUMP; UV; UV2;
			GET_VERTEX_FLAGS(ePosition | eBump | eUV | eUV2);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_BUMP(12),
				LAYOUT_UV(52),
				LAYOUT_UV2(60)
			}
			END_INPUT_LAYOUT
		};

		struct PositionWeightIndicesNormalUV
		{
			POSITION; WEIGHT; INDICES; NORMAL; UV;
			GET_VERTEX_FLAGS(ePosition | eWeight | eIndices | eNormal | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_WEIGHT(12),
				LAYOUT_INDICES(28),
				LAYOUT_NORMAL(44),
				LAYOUT_UV(56),
			}
			END_INPUT_LAYOUT
		};

		struct PositionWeightIndicesBumpUV
		{
			POSITION; WEIGHT; INDICES; BUMP; UV;
			GET_VERTEX_FLAGS(ePosition | eWeight | eIndices | eBump | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_WEIGHT(12),
				LAYOUT_INDICES(28),
				LAYOUT_BUMP(44),
				LAYOUT_UV(88)
			}
			END_INPUT_LAYOUT
		};

		struct PositionColorUVUV2
		{
			POSITION; COLOR; UV; UV2;
			GET_VERTEX_FLAGS(ePosition | eColor | eUV | eUV2);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION(0),
				LAYOUT_COLOR(12),
				LAYOUT_UV(28),
				LAYOUT_UV2(36)
			}
			END_INPUT_LAYOUT
		};

		struct PositionColorUV
		{
			POSITION4; COLOR; UV;
			GET_VERTEX_FLAGS(ePosition4 | eColor | eUV);
			BEGIN_INPUT_LAYOUT
			{
				LAYOUT_POSITION4(0),
				LAYOUT_COLOR(16),
				LAYOUT_UV(32)
			}
			END_INPUT_LAYOUT
		};

		inline uint32 GetVertexSize(uint32 aVertexFlags)
		{
			if (aVertexFlags == PositionNormal::GetVertexFlags()){
				return sizeof(PositionNormal);
			}
			if (aVertexFlags == PositionUV::GetVertexFlags()){
				return sizeof(PositionUV);
			}
			else if (aVertexFlags == PositionNormalUV::GetVertexFlags()){
				return sizeof(PositionNormalUV);
			}
			else if (aVertexFlags == PositionNormalUVUV2::GetVertexFlags()){
				return sizeof(PositionNormalUVUV2);
			}
			else if (aVertexFlags == PositionBump::GetVertexFlags()){
				return sizeof(PositionBump);
			}
			else if (aVertexFlags == PositionBumpUV::GetVertexFlags()){
				return sizeof(PositionBumpUV);
			}
			else if (aVertexFlags == PositionBumpUVUV2::GetVertexFlags()){
				return sizeof(PositionBumpUVUV2);
			}
			else if (aVertexFlags == PositionWeightIndicesNormalUV::GetVertexFlags()){
				return sizeof(PositionWeightIndicesNormalUV);
			}
			else if (aVertexFlags == PositionColorUVUV2::GetVertexFlags()){
				return sizeof(PositionColorUVUV2);
			}
			else if (aVertexFlags == PositionWeightIndicesBumpUV::GetVertexFlags()){
				return sizeof(PositionWeightIndicesBumpUV);
			}
			else if (aVertexFlags == PositionColorUV::GetVertexFlags()){
				return sizeof(PositionColorUV);
			}
			return 0;
		}

		inline bool CreateInputLayout(engine::render::CRenderManager &aRenderManager, uint32 aVertexFlags, ID3DBlob* aBlob, ID3D11InputLayout** aVertexLayout)
		{
			bool result = false;
			if (aVertexFlags == PositionNormal::GetVertexFlags()){
				result = PositionNormal::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionUV::GetVertexFlags()){
				result = PositionUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionNormalUV::GetVertexFlags()){
				result = PositionNormalUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionNormalUVUV2::GetVertexFlags()){
				result = PositionNormalUVUV2::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionBump::GetVertexFlags()){
				result = PositionBump::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionBumpUV::GetVertexFlags()){
				result = PositionBumpUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionBumpUVUV2::GetVertexFlags()){
				result = PositionBumpUVUV2::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionWeightIndicesNormalUV::GetVertexFlags()){
				result = PositionWeightIndicesNormalUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionColorUVUV2::GetVertexFlags()){
				result = PositionColorUVUV2::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionWeightIndicesBumpUV::GetVertexFlags()){
				result = PositionWeightIndicesBumpUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			else if (aVertexFlags == PositionColorUV::GetVertexFlags()){
				result = PositionColorUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
			}
			return result;
		}

		inline uint32 GetFlagsFromString(const std::string& aString)
		{
			if (aString.compare("PositionNormal") == 0){
				return ePosition | eNormal;
			}
			if (aString.compare("PositionUV") == 0){
				return ePosition | eUV;
			}
			else if (aString.compare("PositionNormalUV") == 0){
				return ePosition | eNormal | eUV;
			}
			else if (aString.compare("PositionNormalUVUV2") == 0){
				return ePosition | eNormal | eUV | eUV2;
			}
			else if (aString.compare("PositionBump") == 0){
				return ePosition | eBump;
			}
			else if (aString.compare("PositionBumpUV") == 0){
				return ePosition | eBump | eUV;
			}
			else if (aString.compare("PositionBumpUVUV2") == 0){
				return ePosition | eBump | eUV | eUV2;
			}
			else if (aString.compare("PositionWeightIndicesNormalUV") == 0){
				return ePosition | eWeight | eIndices | eNormal | eUV;
			}
			else if (aString.compare("PositionColorUVUV2") == 0){
				return ePosition | eColor | eUV | eUV2;
			}
			else if (aString.compare("PositionWeightIndicesBumpUV") == 0){
				return ePosition | eWeight | eIndices | eBump | eUV;
			}
			else if (aString.compare("PositionColorUV") == 0){
				return ePosition4 | eColor | eUV;
			}
			return 0;
		}

		inline void CalcTangentsAndBinormals(void *VtxsData, unsigned short *IdxsData, size_t VtxCount,
			size_t IdxCount, size_t VertexStride, size_t GeometryStride, size_t NormalStride, size_t
			TangentStride, size_t BiNormalStride, size_t TextureCoordsStride)
		{
			Vect3f *tan1 = new Vect3f[VtxCount * 2];
			Vect3f *tan2 = tan1 + VtxCount;
			ZeroMemory(tan1, VtxCount * sizeof(Vect3f) * 2);
			unsigned char *l_VtxAddress = (unsigned char *)VtxsData;
			for (size_t b = 0; b<IdxCount; b += 3)
			{
				unsigned short i1 = IdxsData[b];
				unsigned short i2 = IdxsData[b + 1];
				unsigned short i3 = IdxsData[b + 2];
				Vect3f *v1 = (Vect3f *)&l_VtxAddress[i1*VertexStride + GeometryStride];
				Vect3f *v2 = (Vect3f *)&l_VtxAddress[i2*VertexStride + GeometryStride];
				Vect3f *v3 = (Vect3f *)&l_VtxAddress[i3*VertexStride + GeometryStride];
				Vect2f *w1 = (Vect2f *)&l_VtxAddress[i1*VertexStride + TextureCoordsStride];
				Vect2f *w2 = (Vect2f *)&l_VtxAddress[i2*VertexStride + TextureCoordsStride];
				Vect2f *w3 = (Vect2f *)&l_VtxAddress[i3*VertexStride + TextureCoordsStride];
				float x1 = v2->x - v1->x;
				float x2 = v3->x - v1->x;
				float y1 = v2->y - v1->y;
				float y2 = v3->y - v1->y;
				float z1 = v2->z - v1->z;
				float z2 = v3->z - v1->z;
				float s1 = w2->x - w1->x;
				float s2 = w3->x - w1->x;
				float t1 = w2->y - w1->y;
				float t2 = w3->y - w1->y;
				float r = 1.0F / (s1 * t2 - s2 * t1);
				Vect3f sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
				Vect3f tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
				assert(i1<VtxCount);
				assert(i2<VtxCount);
				assert(i3<VtxCount);
				tan1[i1] += sdir;
				tan1[i2] += sdir;
				tan1[i3] += sdir;
				tan2[i1] += tdir;
				tan2[i2] += tdir;
				tan2[i3] += tdir;
			}
			for (size_t b = 0; b<VtxCount; ++b)
			{
				Vect3f *l_NormalVtx = (Vect3f *)&l_VtxAddress[b*VertexStride + NormalStride];
				Vect3f *l_TangentVtx = (Vect3f *)&l_VtxAddress[b*VertexStride + TangentStride];
				Vect4f *l_TangentVtx4 = (Vect4f *)&l_VtxAddress[b*VertexStride + TangentStride];
				Vect4f *l_BiNormalVtx = (Vect4f *)&l_VtxAddress[b*VertexStride + BiNormalStride];
				const Vect3f& t = tan1[b];
				// Gram-Schmidt orthogonalize
				Vect3f l_VAl = t - (*l_NormalVtx)*((*l_NormalVtx)*t);
				l_VAl.Normalize();
				*l_TangentVtx = l_VAl;
				// Calculate handedness
				Vect3f l_Cross;
				l_Cross = (*l_NormalVtx) ^ (*l_TangentVtx);
				l_TangentVtx4->w = (l_Cross*(tan2[b]))< 0.0f ? -1.0f : 1.0f;
				*l_BiNormalVtx = (*l_NormalVtx) ^ (*l_TangentVtx);
			}
			delete[] tan1;
		}
	}
}
#endif