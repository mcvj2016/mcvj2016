#ifndef _ENGINE_SHADERMANAGER_DPVD1_22122016173700_H
#define _ENGINE_SHADERMANAGER_DPVD1_22122016173700_H

#include "Shader.h"
#include "Utils/TemplatedMapVector.h"

namespace engine
{
	namespace shaders
	{
		class CShaderManager
		{
		public:
			CShaderManager();
			virtual ~CShaderManager();
			//Comento este por que no se por que recibe parametro, solo utilizo el de abajo sin parametro y creo asigno m_Filename en el constructor. ->Unused bool Load(const std::string& aFilename);
			bool Load();
			CShader* GetShader(CShader::EShaderStage aStage, const std::string& aShaderName);
			bool Reload();
		private:
			DISALLOW_COPY_AND_ASSIGN(CShaderManager);
			typedef base::utils::CTemplatedMapVector<CShader> TShadersLibrary;
			std::vector< TShadersLibrary > m_Library;
			std::string m_Filename;
		};
	}
}

#endif
