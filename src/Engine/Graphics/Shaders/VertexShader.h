#ifndef _ENGINE_VERTEXSHADER_DPVD1_22122016155700_H
#define _ENGINE_VERTEXSHADER_DPVD1_22122016155700_H

#include "Shader.h"
#include "Utils/Types.h"

namespace engine
{
	namespace shaders
	{
		class CVertexShader : public CShader
		{
		public:
			CVertexShader(const std::string& aShaderCode, uint32 aVertexFlags);
			CVertexShader(const CXMLElement* aElement);
			virtual ~CVertexShader();
			virtual bool Load();
			virtual void Bind(ID3D11DeviceContext* aContext);
			virtual void Unbind(ID3D11DeviceContext* aContext);
			GET_SET_REF(uint32, VertexFlags)
			GET_SET_PTR(ID3D11VertexShader, VertexShader);
			GET_SET_PTR(ID3D11InputLayout, VertexLayout);
		private:
			DISALLOW_COPY_AND_ASSIGN(CVertexShader);
			uint32 m_VertexFlags;
			ID3D11VertexShader *m_VertexShader;
			ID3D11InputLayout *m_VertexLayout;
			virtual std::string GetShaderModel();
		};
	}
}
#endif