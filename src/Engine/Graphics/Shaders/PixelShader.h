#ifndef _ENGINE_PIXELSHADER_DPVD1_22122016171700_H
#define _ENGINE_PIXELSHADER_DPVD1_22122016171700_H

#include "Shader.h"

namespace engine
{
	namespace shaders{
		class CPixelShader : public CShader
		{
		public:
			CPixelShader(const std::string& aShaderCode);
			CPixelShader(const CXMLElement* aElement);
			virtual ~CPixelShader();
			virtual bool Load();
			virtual void Bind(ID3D11DeviceContext* aContext);
			virtual void Unbind(ID3D11DeviceContext* aContext);
			GET_SET_PTR(ID3D11PixelShader, PixelShader);
		private:
			DISALLOW_COPY_AND_ASSIGN(CPixelShader);
			ID3D11PixelShader *m_PixelShader;
			virtual std::string GetShaderModel();
		};
	}
}
#endif