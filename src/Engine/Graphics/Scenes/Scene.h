#ifndef _SCENE_DPVD1_31122016171200_H
#define _SCENE_DPVD1_31122016171200_H

#include "Utils/TemplatedMapVector.h"
#include "Utils/Name.h"
#include "Layer.h"

namespace engine
{
	namespace scenes
	{
		class CScene : public CName, public base::utils::CTemplatedMapVector< CLayer >
		{
		public: 
			CScene(const std::string& aName);
			virtual ~CScene();
			bool Load(const std::string& aFilename);
			void LoadLights();
			void LoadCameras();
			bool Reload();
			bool Update(float elapsedTime);
			void DestroyObject(CSceneNode* node);
			bool Render(); 
			bool Render(const std::string& aLayerName);
			GET_SET_BOOL(Active);
			void LoadCinematics();
			CSceneNode* GetSceneNode(const std::string& aName);
			CSceneNode* GetFirstSceneNodeByTag(CSceneNode::TAG tag);
			std::vector<CSceneNode*> GetSceneNodesByTag(CSceneNode::TAG tag);
			std::vector<CSceneNode*> CScene::GetSceneNodes();
			void Save();
			void LoadNodesComponents();
			bool m_Reloading;
		protected: 
			bool m_Active;
			std::string m_fileName;
			std::vector<CSceneNode*> m_DestroyList;
			void DestroySceneNodes();
			void DestroyPhysx(CSceneNode* node);
		private: 
			DISALLOW_COPY_AND_ASSIGN(CScene);
		};
	}
}

#endif
