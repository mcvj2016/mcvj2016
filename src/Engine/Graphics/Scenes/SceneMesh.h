#ifndef _ENGINE_SCENEMESH_DPVD1_08012017021600_H
#define _ENGINE_SCENEMESH_DPVD1_08012017021600_H

#include "SceneNode.h"
#include "PhysXImpl/PhysXManager.h"

namespace engine
{
	namespace materials{
		class CMesh;
	}
	namespace scenes
	{
		
		class CSceneMesh : public CSceneNode
		{
		public:
			CSceneMesh(const CXMLElement* aElement);
			CSceneMesh(const CXMLElement* aElement, materials::CMesh* aMesh);
			virtual ~CSceneMesh();
			bool Render(render::CRenderManager& aRendermanager) override;
			GET_SET_PTR(engine::materials::CMesh, Mesh);
			std::string m_PhysxFilename;
			
		protected:
			materials::CMesh* m_Mesh;
		};
	}
}
#endif
