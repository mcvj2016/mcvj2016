#ifndef _ENGINE_LAYER_DPVD1_08012017020300_H
#define _ENGINE_LAYER_DPVD1_08012017020300_H

#include "Utils/Name.h"
#include "Utils/TemplatedMapVector.h"
#include "SceneNode.h"

namespace engine
{
	namespace scenes
	{
		class CLayer : public CName, public base::utils::CTemplatedMapVector< CSceneNode > 
		{
		public: 
			CLayer(const std::string& aName);
			virtual ~CLayer();
			bool Load(const CXMLElement* aElement);
			void LoadBasicPrimitivesLayer(const tinyxml2::XMLElement* aElement);
			void LoadAnimatedModelsLayer(const tinyxml2::XMLElement* aElement);
			void LoadParticlesLayer(const tinyxml2::XMLElement* aElement);
			void LoadTriggersLayer(const CXMLElement* aElement);
			void LoadEmptyNodes(const CXMLElement* aElement);
			bool Update(float elapsedTime);
			bool Render();	
			void InsertSort();
			void DistanceFromCamera(CSceneNode* node);
			CSceneNode* GetSceneNode(const std::string& aName);
			CSceneNode* GetFirstSceneNodeByTag(CSceneNode::TAG tag);
			std::vector<CSceneNode*> GetSceneNodes() const;
			GET_SET_BOOL(OrderElements);
			GET_SET_BOOL(Active);
			GET_SET_BOOL(IsPauseLayer)
		protected:
			bool m_Active, m_IsPauseLayer;
			bool m_OrderElements;
		private: 
			DISALLOW_COPY_AND_ASSIGN(CLayer);
		};
	}
}
#endif
