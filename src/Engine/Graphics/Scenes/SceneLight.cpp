#include "SceneLight.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "XML/XML.h"
#include "Graphics/Lights/Light.h"
#include "Graphics/Lights/LightManager.h"

namespace engine
{
	namespace scenes
	{
		CSceneLight::CSceneLight(const CXMLElement* aElement) :CSceneNode(aElement)
		{
			lights::CLightManager& lightManager = CEngine::GetInstance().GetLightManager();
			if (aElement->GetAttribute<std::string>("light", "") != "")
			{
				m_Light = lightManager.GetLight(aElement->Attribute("light"));
				m_Visible = m_Light->IsEnabled();
				
				const CXMLElement* transform = aElement->FirstChildElement("transform");
				Vect3f forward = transform->GetAttribute<Vect3f>("forward", v3fZERO);
				SetForward(forward);
			}
		}

		CSceneLight::CSceneLight(const tinyxml2::XMLElement* aElement, lights::CLight* aLight)
		{
			m_Name = aElement->GetAttribute<std::string>("name", "");
			assert(m_Name != "");

			m_Light = aLight;
			m_Visible = m_Light->IsEnabled();

			Vect3f forward = aElement->GetAttribute<Vect3f>("forward", v3fZERO);
			Vect3f position = aElement->GetAttribute<Vect3f>("position", v3fZERO);
			SetForward(forward);
			SetPosition(position);
		}

		CSceneLight::~CSceneLight()
		{

		}

		lights::CLight* CSceneLight::GetLight() const
		{
			return this->m_Light;
		}

		bool CSceneLight::Render(render::CRenderManager& aRendermanager)
		{
			/*bool lOk = false;
			if (m_Light)
			{
				materials::CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
				lCB.mObjDesc.m_World = GetMatrix();
				lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), materials::CConstantBufferManager::CB_Object);
				lOk = m_Light->Render(aRendermanager);
			}
			return lOk;*/
			return true;
		}
	}
}
