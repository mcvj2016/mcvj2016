#ifndef _H_CPrefabManager
#define _H_CPrefabManager

#include "Utils/Name.h"
#include "Layer.h"

namespace engine
{
	namespace scenes
	{
		class CPrefabManager : public CName
		{
		public:
			CPrefabManager();
			virtual ~CPrefabManager();
			bool Load();
			CSceneNode* LoadAnimatedModel(const tinyxml2::XMLElement* aElement);
			std::string GetUniqueName(const std::string& baseName);
			CSceneNode* LoadPrefabFromLayer(const CXMLElement*  aElement);
			CSceneNode* Instantiate(const std::string& name, const Vect3f& pos = v3fZERO, const Quatf& rot = qfIDENTITY);

		private:
			DISALLOW_COPY_AND_ASSIGN(CPrefabManager);
			std::vector<CSceneNode*> m_PrefabElements;
		};
	}
}

#endif
