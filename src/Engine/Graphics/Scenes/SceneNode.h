#ifndef _SCENENODE_DPVD1_31122016164000_H
#define _SCENENODE_DPVD1_31122016164000_H

#include "Math/Transform.h"
#include "Utils/Name.h"
#include <vector>
#include <set>
#include <map>
#include "Utils/EnumToString.h"

namespace logic
{
	namespace components
	{
		class CScriptComponent;
	}
}

namespace engine
{
	namespace render{ class CRenderManager; }
	namespace scenes
	{
		class CSceneNode : public CTransform, public CName
		{
		public:

			enum TAG
			{
				undefined = 0,
				enemy,
				player,
				player2,
				playercinpos,
				weapon,
				maincamera,
				interactable,
				uimsgbox,
				cincallbacks,
				projectile,
				tagCount
			};

			enum TYPE
			{
				empty = 0,
				mesh,
				meshQuat,
				animated,
				light,
				particle
			};

			CSceneNode();
			CSceneNode(const CXMLElement* aElement);
			virtual ~CSceneNode();

			void PreLoadScriptsFromNode(const tinyxml2::XMLElement* aElement);
			void LoadScriptsFromNode();
			void StartComponents();
			virtual void Update(float aDeltaTime);
			void SetEnabled(bool enabled);
			bool GetEnabled() const;
			virtual void UpdateComponents(float aDeltaTime);
			virtual bool Render(render::CRenderManager& aRendermanager)=0;
			GET_SET_BOOL(Visible)
			GET_SET_BOOL(FrustumIgnored)
			GET_SET_REF(std::vector<logic::components::CScriptComponent*>, Components)
			bool m_hasPhysx;
			Vect3f m_Size;
			float m_Radius;
			bool m_OnlyDestroyPhysx;
			bool m_IsTrigger;
			bool m_IsStatic;
			bool m_IsKinematic;
			std::string m_ShapeType;
			float m_DistanceToCamera;
			CTransform* GetTransform();
			void AddComponent(logic::components::CScriptComponent* component);
			void AddComponentName(std::string componentName);
			void DisableComponents();
			void EnableComponents();

			void  OnTrigger(CSceneNode* other);
			void  OnTriggerEnter(CSceneNode* other);
			void  OnTriggerExit(CSceneNode* other);

			logic::components::CScriptComponent* GetComponent(std::string name);

			GET_SET(TAG, Tag)
			GET_SET(TYPE, Type)

			template<typename T> T* GetComponent(std::string name)
			{
				for (auto component : m_Components)
				{
					if (component->GetName() == name)
					{
						return static_cast<T*>(component);
					}
				}
				return nullptr;
			}

			template<typename T> T* GetComponent()
			{
				for (auto component : m_Components)
				{
					T* prinComponent = dynamic_cast<T*>(component);
					if (prinComponent)
					{
						return prinComponent;
					}
				}
				return nullptr;
			}

			bool m_TriggerActive;
			void SetTriggerActive(bool triggerActive);
			bool IsTriggerActive() const;

			bool WasTriggered(CSceneNode* other);
			std::string m_LayerName;
			std::vector<std::string> m_ComponentNames;
		protected:
			bool m_LoadedComponents;
			bool m_StartedComponents;
			std::set<std::string> m_MeshInTrigger;
			bool m_Visible;
			bool m_FrustumIgnored;
			std::vector<logic::components::CScriptComponent*> m_Components;			
			std::map<std::string, std::vector<std::string>> m_Params;
			TAG m_Tag;
			bool m_Enabled;
			TYPE m_Type;
		};
	}
}

Begin_Enum_String(engine::scenes::CSceneNode::TAG)
{
	Enum_String_Id(engine::scenes::CSceneNode::enemy, "enemy");
	Enum_String_Id(engine::scenes::CSceneNode::player, "player");
	Enum_String_Id(engine::scenes::CSceneNode::player2, "player2");
	Enum_String_Id(engine::scenes::CSceneNode::playercinpos, "playercinpos");
	Enum_String_Id(engine::scenes::CSceneNode::weapon, "weapon");
	Enum_String_Id(engine::scenes::CSceneNode::maincamera, "maincamera");
	Enum_String_Id(engine::scenes::CSceneNode::interactable, "interactable");
	Enum_String_Id(engine::scenes::CSceneNode::uimsgbox, "uimsgbox");
	Enum_String_Id(engine::scenes::CSceneNode::cincallbacks, "cincallbacks");
	Enum_String_Id(engine::scenes::CSceneNode::projectile, "projectile");
}
End_Enum_String;

#endif

