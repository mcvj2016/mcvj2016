#include "SceneCamera.h"
#include "Camera/CameraManager.h"

using namespace engine::camera;

namespace engine
{
	namespace scenes
	{
		CSCeneCamera::CSCeneCamera(const tinyxml2::XMLElement* aElement)
			: CSceneNode(aElement), m_Camera(nullptr)
		{
		}

		CSCeneCamera::CSCeneCamera(const tinyxml2::XMLElement* aElement, CCameraController* cam)
			: CSceneNode(aElement), m_Camera(cam)
		{
		}

		CSCeneCamera::~CSCeneCamera()
		{
		}

		bool CSCeneCamera::Render(render::CRenderManager& aRendermanager)
		{
			return true;
		}

		CCameraController* CSCeneCamera::GetCamera() const
		{
			return m_Camera;
		}
	}
}
