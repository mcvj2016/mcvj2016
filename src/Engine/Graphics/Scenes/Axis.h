#ifndef __CAxis_HH__
#define __CAxis_HH__

#pragma once

#include "Graphics/Meshes/Mesh.h"
#include "SceneBasicPrimitive.h"


namespace engine
{
	namespace materials
	{
		class CGeometry;
		class CMaterial;

		class CAxis : public CMesh
		{
		public:
			CAxis(bool cameraAxis);
			virtual ~CAxis();
			bool Render(render::CRenderManager& aRenderManager) override;
			bool m_cameraAxis;
			scenes::CSceneBasicPrimitive* primitive;
		};		
	}
}
#endif
