#ifndef _ENGINE_SCENEMESHQUATRENDER_DPVD1_04082017021600_H
#define _ENGINE_SCENEMESHQUATRENDER_DPVD1_04082017021600_H

#include "SceneNode.h"

namespace engine
{
	namespace materials{
		class CMesh;
	}
	namespace scenes
	{
		
		class CSceneMeshQuatRender : public CSceneNode
		{
		public:
			CSceneMeshQuatRender(const CXMLElement* aElement);
			CSceneMeshQuatRender(const CXMLElement* aElement, engine::materials::CMesh* aMesh);
			virtual ~CSceneMeshQuatRender();
			bool Render(render::CRenderManager& aRendermanager) override;
			GET_SET_PTR(engine::materials::CMesh, Mesh);
			std::string m_PhysxFilename;
			
		protected:
			materials::CMesh* m_Mesh;
			
		};
	}
}
#endif
