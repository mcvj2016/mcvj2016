#ifndef _H_SCENE_Primitive__
#define _H_SCENE_Primitive__
#pragma once

#include "SceneMesh.h"

XML_FORWARD_DECLARATION
namespace engine{
	namespace scenes{
		class CSceneBasicPrimitive : public CSceneMesh
		{
		public:
			CSceneBasicPrimitive(const CXMLElement* aElement);
			CSceneBasicPrimitive(const tinyxml2::XMLElement* aElement, engine::materials::CMesh aMesh);
			virtual ~CSceneBasicPrimitive();
		};
	}
}
#endif
