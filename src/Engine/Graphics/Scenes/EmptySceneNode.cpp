#include "EmptySceneNode.h"
//#include "lua_utils.h"
#include "Engine.h"
//#include "ScriptManager.h"

namespace engine
{
	namespace scenes
	{
		CEmptySceneNode::CEmptySceneNode(const tinyxml2::XMLElement* aElement)
			: CSceneNode(aElement)
		{
		}

		CEmptySceneNode::~CEmptySceneNode()
		{
		}

		bool CEmptySceneNode::Render(render::CRenderManager& aRendermanager)
		{
			//update its components
			return true;
		}
	}
}
