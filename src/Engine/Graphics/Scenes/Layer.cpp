#include "Layer.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include "SceneBasicPrimitive.h"
#include "Camera/ThirdPersonCameraController.h"
#include "Camera/FirstPersonCameraController.h"
#include "Camera/CameraManager.h"
#include "Graphics/Lights/Light.h"
#include "Graphics/Meshes/Mesh.h"
#include "Graphics/Shaders/VertexType.h"
#include "Utils/BinFileReader.h"
#include "Graphics/Particles/ParticleSystemInstance.h"
#include "EmptySceneNode.h"
#include "SceneManager.h"
#include "PhysXImpl/PhysXManager.h"
#include "SceneMeshQuatRender.h"
#include "Utils/Logger/Logger.h"

namespace engine
{
	namespace scenes
	{
		CLayer::CLayer(const std::string& aName) : CName(aName), m_Active(true), m_IsPauseLayer(false), m_OrderElements(false)
		{
		}

		CLayer::~CLayer()
		{
		}

		bool CLayer::Load(const CXMLElement* aElement)
		{
			/*Cargara todos los nodos de escena*/
			//scene meshes
			const CXMLElement* meshElement = aElement->FirstChildElement("scene_mesh");
			while (meshElement != nullptr)
			{
				CSceneMesh* sceneMesh = new CSceneMesh(meshElement);
				sceneMesh->m_LayerName = this->GetName();
				if (!Add(sceneMesh->GetName(), sceneMesh))
				{
					LOG_WARNING_APPLICATION("The scene node %s is duplicated and is not being added to layer %s", sceneMesh->GetName(), GetName());
					base::utils::CheckedDelete(sceneMesh);
				}

				const CXMLElement* physxElement = meshElement->FirstChildElement("physx");
				if (physxElement != nullptr)
				{
					CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
					lPhysxManager.CreatePhysxObject(sceneMesh, physxElement);
				}
				meshElement = meshElement->NextSiblingElement("scene_mesh");
			}

			/*Cargara todos los nodos de escena*/
			//scene meshes
			const CXMLElement* meshQuatElement = aElement->FirstChildElement("scene_mesh_quat");
			while (meshQuatElement != nullptr)
			{
				CSceneMeshQuatRender* sceneMeshQuat = new CSceneMeshQuatRender(meshQuatElement);
				sceneMeshQuat->m_LayerName = this->GetName();
				if (!Add(sceneMeshQuat->GetName(), sceneMeshQuat))
				{
					base::utils::CheckedDelete(sceneMeshQuat);
				}

				const CXMLElement* physxElement = meshQuatElement->FirstChildElement("physx");
				if (physxElement != nullptr)
				{
					CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
					lPhysxManager.CreatePhysxObject(sceneMeshQuat, physxElement);
				}
				meshQuatElement = meshQuatElement->NextSiblingElement("scene_mesh_quat");
			}
			
			LoadBasicPrimitivesLayer(aElement);
			LoadAnimatedModelsLayer(aElement);
			LoadParticlesLayer(aElement);
			LoadTriggersLayer(aElement);
			LoadEmptyNodes(aElement);

			return true;
		}

		void CLayer::LoadBasicPrimitivesLayer(const CXMLElement* aElement)
		{
			const CXMLElement* meshElement = aElement->FirstChildElement("scene_basic_primitive");
			while (meshElement != nullptr)
			{
				CSceneMesh* sceneMesh = new CSceneBasicPrimitive(meshElement);
				sceneMesh->m_LayerName = this->GetName();
				if (!this->Add(sceneMesh->GetName(), sceneMesh))
				{
					base::utils::CheckedDelete(sceneMesh);
				}
				else
				{
					//sceneMesh->PreLoadScriptsFromNode(meshElement);
				}
				
				meshElement = meshElement->NextSiblingElement("scene_basic_primitive");
			}
		}

		void CLayer::LoadAnimatedModelsLayer(const CXMLElement* aElement)
		{
			cal3dimpl::CAnimatedModelManager& animationsManager = CEngine::GetInstance().GetAnimatedModelManager();
			const CXMLElement* animatedModelElement = aElement->FirstChildElement("animated_models");
			while (animatedModelElement != nullptr)
			{
				cal3dimpl::CSceneAnimatedModel* sceneAnimated = new cal3dimpl::CSceneAnimatedModel(animatedModelElement);
				sceneAnimated->m_LayerName = this->GetName();
				std::string model = animatedModelElement->Attribute("model");

				//process physx
				const CXMLElement* physxElement = animatedModelElement->FirstChildElement("physx");
				if (physxElement != nullptr)
				{
					sceneAnimated->m_hasPhysx = true;
					sceneAnimated->AddPhysx(physxElement);
				}

				if (!this->Add(sceneAnimated->GetName(), sceneAnimated))
				{
					base::utils::CheckedDelete(sceneAnimated);
				}
				else
				{
					sceneAnimated->Initialize(animationsManager(model));
					if (!sceneAnimated->IsVisible())
					{
						sceneAnimated->HideAnimated();
					}
				}				
				animatedModelElement = animatedModelElement->NextSiblingElement("animated_models");
			}
		}

		void CLayer::LoadParticlesLayer(const CXMLElement* aElement)
		{
			const CXMLElement* particleElement = aElement->FirstChildElement("particle");
			while (particleElement != nullptr){
				particles::CParticleSystemInstance* lParticleSystemInstance = new particles::CParticleSystemInstance(particleElement);
				lParticleSystemInstance->m_LayerName = this->GetName();
				if (!this->Add(lParticleSystemInstance->GetName(), lParticleSystemInstance))
				{
					base::utils::CheckedDelete(lParticleSystemInstance);
				}
				else
				{
					//lParticleSystemInstance->PreLoadScriptsFromNode(particleElement);
				}
				
				particleElement = particleElement->NextSiblingElement("particle");
			}
		}

		void CLayer::LoadTriggersLayer(const CXMLElement* aElement)
		{
			const CXMLElement* triggerElement = aElement->FirstChildElement("scene_trigger");
			while (triggerElement != nullptr){
				CEmptySceneNode* trigger = new CEmptySceneNode(triggerElement);
				trigger->m_LayerName = this->GetName();
				if (!this->Add(trigger->GetName(), trigger))
				{
					base::utils::CheckedDelete(trigger);
				}

				const CXMLElement* physxElement = triggerElement->FirstChildElement("physx");
				if (physxElement != nullptr)
				{
					CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
					lPhysxManager.CreatePhysxObject(trigger, physxElement);
				}

				triggerElement = triggerElement->NextSiblingElement("scene_trigger");
			}
		}

		void CLayer::LoadEmptyNodes(const CXMLElement* aElement)
		{
			const CXMLElement* emptyNodeElement = aElement->FirstChildElement("empty_node");
			while (emptyNodeElement != nullptr){
				CEmptySceneNode* emptyNode = new CEmptySceneNode(emptyNodeElement);
				emptyNode->m_LayerName = this->GetName();
				if (!this->Add(emptyNode->GetName(), emptyNode))
				{
					base::utils::CheckedDelete(emptyNode);
				}

				const CXMLElement* physxElement = emptyNodeElement->FirstChildElement("physx");
				if (physxElement != nullptr)
				{
					CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
					lPhysxManager.CreatePhysxObject(emptyNode, physxElement);
				}

				emptyNodeElement = emptyNodeElement->NextSiblingElement("empty_node");
			}
		}

		bool CLayer::Update(float elapsedTime)
		{
			if (!CEngine::GetInstance().IsPaused()
				|| (CEngine::GetInstance().IsPaused() && m_IsPauseLayer))
			{
				/*Actualizar todos los nodos de escena*/
				for (CSceneNode* node : m_ResourcesVector)
				{
					if (node->GetEnabled())
					{
						node->Update(elapsedTime);
						node->UpdateComponents(elapsedTime);
						if (m_OrderElements){
							DistanceFromCamera(node);
						}
					}
				}

				if (m_OrderElements)
				{
					InsertSort();
				}
			}
			

			return true;
		}

		bool CLayer::Render()
		{
			render::CRenderManager& RenderManager = CEngine::GetInstance().GetRenderManager();

			if (m_Active)
			{
				/*Llamar render de cada nodo de escena*/
				for (CSceneNode* node : m_ResourcesVector)
				{
					if (node->IsVisible())
					{
						node->Render(RenderManager);
					}
				}
			}

			return true;
		}

		CSceneNode* CLayer::GetSceneNode(const std::string& aName)
		{
			return this->operator()(aName);
		}

		CSceneNode* CLayer::GetFirstSceneNodeByTag(CSceneNode::TAG tag)
		{
			for (CSceneNode* node : m_ResourcesVector)
			{
				if (node->GetTag() == tag)
				{
					return node;
				}
			}
			return nullptr;
		}

		std::vector<CSceneNode*> CLayer::GetSceneNodes() const
		{
			return m_ResourcesVector;
		}

		void CLayer::InsertSort()
		{
			size_t i, j;
			CSceneNode* tmp;
			for (i = 1; i < m_ResourcesVector.size(); ++i) {
				j = i;
				while (j > 0 && m_ResourcesVector[j - 1]->m_DistanceToCamera < m_ResourcesVector[j]->m_DistanceToCamera) {
					tmp = m_ResourcesVector[j];
					m_ResourcesVector[j] = m_ResourcesVector[j - 1];
					m_ResourcesVector[j - 1] = tmp;
					--j;
				}
			}
		}

		void CLayer::DistanceFromCamera(CSceneNode* node)
		{
			CCameraController* camera = CEngine::GetInstance().GetCameraManager().GetMainCamera();
			Vect3f Distance = node->m_Position - camera->GetSceneCamera()->GetPosition();
			node->m_DistanceToCamera = Distance * camera->GetSceneCamera()->GetForward();
		}
	}
}
