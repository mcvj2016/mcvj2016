#include "SceneManager.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Meshes/MeshManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Textures/TextureManager.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Graphics/Particles/ParticleManager.h"
#include "Camera/CameraManager.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include "Sound/ISoundManager.h"

namespace engine
{
	namespace scenes
	{
		CSceneManager::CSceneManager()
			:
			m_NextScene(std::string("")),
			m_ChangingScene(false),
			m_Filename("data/common/scenes.xml")
		{
		}

		CSceneManager::~CSceneManager()
		{
		}

		bool CSceneManager::Load(const std::string& activeScene)
		{
			//Preload dependant managers
			CEngine::GetInstance().GetAnimatedModelManager().Load();

			/*El manager de escena se encargar� de cargar un fichero con los directorios de cada escena e ir� cargando las escenas*/
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			assert(loaded);

			CXMLElement* scenes = xmldoc.FirstChildElement("scenes");
			if (scenes != nullptr)
			{
				CXMLElement* sceneElement = scenes->FirstChildElement("scene");
				while (sceneElement != nullptr)
				{
					std::string sceneName = sceneElement->Attribute("name");
					if (activeScene != "" )
					{
						if (activeScene == sceneName)
						{
							CScene* scene = new CScene(sceneName);
							scene->SetActive(true);
							this->Add(sceneName, scene);
							loaded = scene->Load(sceneElement->GetAttribute<std::string>("folder", ""));
							assert(loaded);
						}
					}
					else
					{
						if (sceneElement->BoolAttribute("active"))
						{
							CScene* scene = new CScene(sceneName);
							//we add the scene before loading it because other managers on the load step
							//they are asking for the current scene. So it must be added before asking for it

							scene->SetActive(true);
							this->Add(sceneName, scene);
							loaded = scene->Load(sceneElement->GetAttribute<std::string>("folder", ""));
							assert(loaded);
						}
					}
					sceneElement = sceneElement->NextSiblingElement("scene");
				}
				//after loading all layers, load the layers of each light with shadowmap
				CEngine::GetInstance().GetLightManager().SetLightsLayers();
				
			}
			return loaded;
		}

		bool CSceneManager::Update(float elapsedTime)
		{
			/*Actualizar� las escenas activas*/
			for (size_t i = 0, lCount = this->GetCount(); i < lCount; ++i)
			{
				if (this->operator[](i)->IsActive())
				{
					this->operator[](i)->Update(elapsedTime);
				}
			}
			return true;
		}

		bool CSceneManager::Render(const std::string& aLayer)
		{
			/*Llama al render de escena en la capa indicada, si la escena esta activa*/
			for (size_t i = 0, lCount = this->GetCount(); i < lCount; ++i)
			{
				if (this->operator[](i)->IsActive())
				{
					CScene* l_scene = this->operator[](i);
					l_scene->Render(aLayer);
				}
			}
			return true;
		}

		bool CSceneManager::Empty()
		{
			CEngine& engine = CEngine::GetInstance();
			engine.GetLightManager().Destroy();
			engine.GetCinematicManager().Destroy();
			engine.GetTextureManager().Destroy();
			engine.GetTextureManager().LoadCommonTextures();
			engine.GetMaterialManager().Destroy();
			engine.GetMaterialManager().loadCommonTechniqueMaterials();
			engine.GetMeshManager().Destroy();
			engine.GetMeshManager().Load();
			engine.GetParticleManager().Destroy();
			engine.GetCameraManager().Destroy();
			engine.GetAnimatedModelManager().Destroy();
			engine.GetSoundManager().Terminate();
			Destroy();

			return true;
		}

		void CSceneManager::ChangeScene(const std::string& name)
		{
			m_NextScene = name;
			m_ChangingScene = true;
		}


		bool CSceneManager::PerformChangeScene()
		{
			Empty();
			//reload sound manager
			CEngine::GetInstance().GetSoundManager().Init();
			CEngine::GetInstance().GetSoundManager().Load();
			bool loaded = Load(m_NextScene);
			assert(loaded);
			return loaded;
		}

		bool CSceneManager::Reload()
		{
			Empty();
			bool loaded = Load("");
			//start all script components
			GetCurrentScene()->LoadNodesComponents();
			return loaded;
		}

		void CSceneManager::SetActive(const std::string& aScene, bool aBool)
		{
			/*Activa o desactiva una escena dejando el resto desactivadas*/
			for (size_t i = 0, lCount = this->GetCount(); i < lCount; ++i)
			{
				if (this->operator[](i)->IsActive())
				{
					this->operator[](i)->SetActive(false);
				}
			}
			this->operator()(aScene)->SetActive(aBool);
		}

		CScene* CSceneManager::GetCurrentScene()
		{
			for (size_t i = 0, lCount = this->GetCount(); i < lCount; ++i)
			{
				if (this->operator[](i)->IsActive())
				{
					return this->operator[](i);
				}
			}
			return nullptr;
		}

		void CSceneManager::Save()
		{
			GetCurrentScene()->Save();
		}
	}
}
