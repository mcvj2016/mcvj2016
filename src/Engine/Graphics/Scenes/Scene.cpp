#include "Scene.h"
#include "XML/XML.h"
#include "Engine.h"
#include <filesystem>
#include "Graphics/Cinematics/CinematicManager.h"
#include "Camera/CameraManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Textures/TextureManager.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Meshes/MeshManager.h"
#include "Graphics/Particles/ParticleManager.h"
#include "SceneMesh.h"
#include "Math/Quaternion.h"
#include "Utils/FileUtils.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Camera/CameraController.h"
#include "Camera/AnimatedCamera.h"
#include "Core/ScriptComponent.h"
#include "Utils/Logger/Logger.h"

namespace engine
{
	namespace scenes
	{
		CScene::CScene(const std::string& aName): CName(aName), m_Reloading(false), m_Active(false)
		{
		}

		CScene::~CScene()
		{
		}
		bool CScene::Load(const std::string& aFilename)
		{
			m_fileName = aFilename;
			CXMLDocument xmldoc;

			//Load the textures of this scene
			std::string path = "data/" + m_fileName + "/textures/";
			CEngine::GetInstance().GetTextureManager().Load(path);

			//Load the materials of this scene
			path = "data/" + m_fileName + "/materials.xml";
			CEngine::GetInstance().GetMaterialManager().load(path);

			//Load the meshes of this scene
			path = "data/" + m_fileName + "/meshes/";
			CEngine::GetInstance().GetMeshManager().Load(path);

			//Load the particles system of the scene
			path = "data/" + m_fileName + "/particle_systems.xml";
			CEngine::GetInstance().GetParticleManager().Load(path);

			//layer
			path = "data/" + m_fileName + "/scene.xml";
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(path.c_str()));
			assert(loaded);

			CXMLElement* scene = xmldoc.FirstChildElement("scene");
			if (scene != nullptr)
			{
				//layer
				CXMLElement* layerElement = scene->FirstChildElement("layer");
				while (layerElement != nullptr)
				{
					CLayer* layer = new CLayer(layerElement->Attribute("name"));
					layer->SetOrderElements(layerElement->GetAttribute<bool>("must_order", false));
					layer->SetActive(layerElement->GetAttribute<bool>("active",true));
					layer->SetIsPauseLayer(layerElement->GetAttribute<bool>("pause", false));
					bool layerAdded = this->Add(layer->GetName(), layer);
					assert(layerAdded);
					layerAdded = layer->Load(layerElement);
					assert(layerAdded);
					layerElement = layerElement->NextSiblingElement("layer");
				}
				//Load the lights of this scene, adding a lihgts layer
				LoadLights();

				//Load the cameras of this scene, adding a camera layer
				LoadCameras();

				LoadCinematics();
			}			

#ifdef _DEBUG
			//sanity check for knowing duplicated nodes in different layers
			/*std::vector<CSceneNode*> duplicates;
			for (CSceneNode* node : GetSceneNodes())
			{
				duplicates = GetSceneNodes(node->GetName());

				if (duplicates.size() > 1)
				{
					LOG_WARNING_APPLICATION("The scene node %s is duplicated", duplicates[0]->GetName().c_str());
					duplicates.clear();
				}
			}*/
#endif

			return true;
		}
		void CScene::LoadCinematics()
		{
			//Load the cinematics of this scene, needs the layer loaded
			std::string path = "data/" + m_fileName + "/cinematics.xml";
			CEngine::GetInstance().GetCinematicManager().Load(path);
		}

		void CScene::LoadLights()
		{
			Add("lights", new CLayer("lights"));
			const std::string&path = "data/scenes/" + m_Name + "/lights.xml";
			bool loaded = CEngine::GetInstance().GetLightManager().Load(path);
			assert(loaded);
		}

		void CScene::LoadCameras()
		{
			CLayer* camLayer = new CLayer("cameras");
			if (!Add("cameras", camLayer))
			{
				CheckedDelete(camLayer);
			}
			const std::string& path = "data/scenes/" + m_Name + "/cameras.xml";
			camera::CCameraManager& camManager = CEngine::GetInstance().GetCameraManager();
			bool loaded = camManager.Load(path);

			for (size_t i = 0; i < camManager.GetCount(); ++i)
			{
				this->GetByName("cameras")->Add(camManager.GetByIndex(i)->GetSceneCamera()->GetName(), camManager.GetByIndex(i)->GetSceneCamera());
			}
			assert(loaded);
		}

		bool CScene::Update(float elapsedTime)
		{
			/*Actualizara aquellas layers activas*/
			for (CLayer* layer : m_ResourcesVector)
			{
				if (layer->IsActive())
				{
					layer->Update(elapsedTime);
				}
			}
			DestroySceneNodes();

			return true;
		}

		void CScene::DestroyObject(CSceneNode* node)
		{
			m_DestroyList.push_back(node);
			//call ondestroy of all of its components
			for (logic::components::CScriptComponent* const component : node->GetComponents())
			{
				component->OnDestroy();
			}
		}

		void CScene::DestroySceneNodes()
		{
			for (CSceneNode* node : m_DestroyList)
			{
				DestroyPhysx(node);
				if (node->m_OnlyDestroyPhysx)
				{
					node->m_OnlyDestroyPhysx = false;
				}
				else
				{
					CLayer* l_layer = this->operator()(node->m_LayerName);
					if (l_layer)
					{
						l_layer->Remove(node->GetName());
						node = nullptr;
					}
				}
			}
			if (m_DestroyList.size()>0)
			{
				m_DestroyList.clear();
			}
		}

		void CScene::DestroyPhysx(CSceneNode* node)
		{
			if (node->m_hasPhysx)
			{
				CPhysXManager* lPM = &CEngine::GetInstance().GetPhysXManager();
				node->m_hasPhysx = false;
				lPM->RemoveActor(node->GetName());
			}
		}

		bool CScene::Render()
		{
			/* Llamara al pintado de todas las capas.*/
			for (CLayer* layer : m_ResourcesVector)
			{
				if (layer->IsActive())
				{
					layer->Render();
				}
			}
			//lua_State* lLS = CEngine::GetInstance().GetScriptManager().GetState();
			//call_function<void>(lLS, "updateOnRender");
			return true;
		}

		bool CScene::Render(const std::string& aLayerName)
		{
			/*Llamar� al pintado de la layer seleccionada*/
			this->operator()(aLayerName)->Render();
			return true;
		}

		CSceneNode* CScene::GetSceneNode(const std::string& aName)
		{
			CSceneNode* node = nullptr;
			for (CLayer* layer : m_ResourcesVector)
			{
				node = layer->GetSceneNode(aName);
				if (node != nullptr)
				{
					return node;
				}
			}

			return nullptr;
		}

		CSceneNode* CScene::GetFirstSceneNodeByTag(CSceneNode::TAG tag)
		{
			CSceneNode* node = nullptr;
			for (CLayer* layer : m_ResourcesVector)
			{
				node = layer->GetFirstSceneNodeByTag(tag);
				if (node != nullptr)
				{
					return node;
				}
			}
			return nullptr;
		}

		std::vector<CSceneNode*> CScene::GetSceneNodesByTag(CSceneNode::TAG tag)
		{
			std::vector<CSceneNode*> nodes;
			for (CLayer* layer : m_ResourcesVector)
			{
				for (CSceneNode* node : layer->GetResourcesVector())
				{
					if (node->GetTag() == tag)
					{
						nodes.push_back(node);
					}
				}
			}
			return nodes;
		}

		std::vector<CSceneNode*> CScene::GetSceneNodes()
		{
			std::vector<CSceneNode*> nodes;
			
			for (CLayer* layer : m_ResourcesVector)
			{
				nodes.insert(
					nodes.end(),
					make_move_iterator(layer->GetResourcesVector().begin()),
					make_move_iterator(layer->GetResourcesVector().end())
					);
			}

			return nodes;
		}

		bool CScene::Reload()
		{
			CEngine& engine = CEngine::GetInstance();
			engine.GetLightManager().Destroy();
			engine.GetCinematicManager().Destroy();
			engine.GetTextureManager().Destroy();
			engine.GetTextureManager().LoadCommonTextures();
			engine.GetMaterialManager().Destroy();
			engine.GetMaterialManager().loadCommonTechniqueMaterials();
			engine.GetMeshManager().Destroy();
			engine.GetParticleManager().Destroy();
			engine.GetCameraManager().Destroy();
			Destroy();
			
			/*Recargar todas las layers*/
			return Load(m_fileName);
		}

		void CScene::Save()
		{
			tinyxml2::XMLDocument xmldoc;
			tinyxml2::XMLElement * scene_root = xmldoc.NewElement("scene");
			xmldoc.InsertFirstChild(scene_root);

			for (size_t i = 0; i < GetCount(); ++i)
			{
				tinyxml2::XMLElement * layer_element = xmldoc.NewElement("layer");
				scene_root->InsertEndChild(layer_element);
				CLayer * layer = GetByIndex(i);
				std::string layerName = layer->GetName();
				if (layerName == "opaque" || layerName == "transparent" || layerName == "skybox")
				{
					layer_element->SetAttribute("name", layer->GetName().c_str());
					for (size_t imesh = 0; imesh < layer->GetCount(); ++imesh){
						CSceneMesh* l_scene_mesh = (CSceneMesh*)layer->GetByIndex(imesh);
						tinyxml2::XMLElement * scene_mesh_element = xmldoc.NewElement("scene_mesh");
						layer_element->InsertEndChild(scene_mesh_element);
						scene_mesh_element->SetAttribute("name", l_scene_mesh->GetName().c_str());
						scene_mesh_element->SetAttribute("mesh", l_scene_mesh->GetMesh()->GetName().c_str());
						scene_mesh_element->SetAttribute("visible", l_scene_mesh->IsVisible());
						if (l_scene_mesh->IsFrustumIgnored()){
							scene_mesh_element->SetAttribute("frustum_ignored", l_scene_mesh->IsFrustumIgnored());
						}
						tinyxml2::XMLElement * transform_element = xmldoc.NewElement("transform");
						scene_mesh_element->InsertEndChild(transform_element);
						std::string posx = std::to_string(l_scene_mesh->GetPosition().x);
						std::string posy = std::to_string(l_scene_mesh->GetPosition().y);
						std::string posz = std::to_string(l_scene_mesh->GetPosition().z);
						transform_element->SetAttribute("position", (posx + " " + posy + " " + posz).c_str());

						Quatf quaternion(0.0, 0.0, 0.0, 0.0);
						quaternion.QuatFromYawPitchRoll(l_scene_mesh->GetYaw(), l_scene_mesh->GetPitch(), l_scene_mesh->GetRoll());
						std::string anglex = std::to_string(quaternion.x);
						std::string angley = std::to_string(quaternion.y);
						std::string anglez = std::to_string(quaternion.z);
						std::string anglew = std::to_string(quaternion.w);
						transform_element->SetAttribute("rotation", (anglex + " " + angley + " " + anglez + " " + anglew).c_str());

						std::string scalex = std::to_string(l_scene_mesh->GetScale().x);
						std::string scaley = std::to_string(l_scene_mesh->GetScale().y);
						std::string scalez = std::to_string(l_scene_mesh->GetScale().z);
						transform_element->SetAttribute("scale", (scalex + " " + scaley + " " + scalez).c_str());
					}
				}

				if (layerName == "animated_models")
				{
					layer_element->SetAttribute("name", "animated_models");
					for (size_t ianim = 0; ianim < layer->GetCount(); ++ianim){
						cal3dimpl::CSceneAnimatedModel* l_animated = static_cast<cal3dimpl::CSceneAnimatedModel*>(layer->GetByIndex(ianim));
						tinyxml2::XMLElement * animated_element = xmldoc.NewElement("animated_models");
						layer_element->InsertEndChild(animated_element);
						animated_element->SetAttribute("name", l_animated->GetName().c_str());
						animated_element->SetAttribute("model", l_animated->getModelName().c_str());

						tinyxml2::XMLElement * transform_element = xmldoc.NewElement("transform");
						animated_element->InsertEndChild(transform_element);
						std::string posx = std::to_string(l_animated->GetPosition().x);
						std::string posy = std::to_string(l_animated->GetPosition().y);
						std::string posz = std::to_string(l_animated->GetPosition().z);
						transform_element->SetAttribute("position", (posx + " " + posy + " " + posz).c_str());

						Quatf quaternion(0.0, 0.0, 0.0, 0.0);
						quaternion.QuatFromYawPitchRoll(l_animated->GetYaw(), l_animated->GetPitch(), l_animated->GetRoll());
						std::string anglex = std::to_string(quaternion.x);
						std::string angley = std::to_string(quaternion.y);
						std::string anglez = std::to_string(quaternion.z);
						std::string anglew = std::to_string(quaternion.w);
						transform_element->SetAttribute("rotation", (anglex + " " + angley + " " + anglez + " " + anglew).c_str());

						std::string scalex = std::to_string(l_animated->GetScale().x);
						std::string scaley = std::to_string(l_animated->GetScale().y);
						std::string scalez = std::to_string(l_animated->GetScale().z);
						transform_element->SetAttribute("scale", (scalex + " " + scaley + " " + scalez).c_str());

						for (size_t icom = 0; icom < l_animated->GetComponents().size(); ++icom){
							tinyxml2::XMLElement * components_element = xmldoc.NewElement("script_object");
							animated_element->InsertEndChild(components_element);
							const std::string& name = l_animated->GetComponents().at(icom)->GetName();
							components_element->SetAttribute("name", name.c_str());
						}
					}
				}

				if (layerName == "cameras")
				{
					engine::camera::CCameraManager& cameraMan = CEngine::GetInstance().GetCameraManager();
					layer_element->SetAttribute("name", "cameras");
					for (size_t icam = 0; icam < cameraMan.GetCount(); ++icam){
						CCameraController* l_camera = cameraMan.GetByIndex(icam);
						tinyxml2::XMLElement * camera_element = xmldoc.NewElement("scene_camera");
						layer_element->InsertEndChild(camera_element);
						camera_element->SetAttribute("name", l_camera->GetSceneCamera()->GetName().c_str());

						if (cameraMan.GetMainCamera()->GetSceneCamera()->GetName() == l_camera->GetSceneCamera()->GetName()){
							camera_element->SetAttribute("main", true);
						}
						if (l_camera->m_CameraType==CCameraController::THIRD_PERSON){
							camera_element->SetAttribute("type", "THIRD_PERSON");
						}

						tinyxml2::XMLElement * transform_element = xmldoc.NewElement("transform");
						camera_element->InsertEndChild(transform_element);
						std::string posx = std::to_string(l_camera->GetSceneCamera()->GetPosition().x);
						std::string posy = std::to_string(l_camera->GetSceneCamera()->GetPosition().y);
						std::string posz = std::to_string(l_camera->GetSceneCamera()->GetPosition().z);
						transform_element->SetAttribute("position", (posx + " " + posy + " " + posz).c_str());

						std::string upx;
						std::string upy;
						std::string upz;
						if (l_camera->m_CameraType == CCameraController::ANIMATED){
							CAnimatedCamera * aCamera = (CAnimatedCamera*)l_camera;
							upx = std::to_string(aCamera->GetUpAnim().x);
							upy = std::to_string(aCamera->GetUpAnim().y);
							upz = std::to_string(aCamera->GetUpAnim().z);
						} else 
						{
							upx = std::to_string(l_camera->GetSceneCamera()->GetUp().x);
							upy = std::to_string(l_camera->GetSceneCamera()->GetUp().y);
							upz = std::to_string(l_camera->GetSceneCamera()->GetUp().z);
						}
						transform_element->SetAttribute("up", (upx + " " + upy + " " + upz).c_str());

						Quatf quaternion(0.0, 0.0, 0.0, 0.0);
						quaternion.QuatFromYawPitchRoll(l_camera->GetSceneCamera()->GetYaw(), l_camera->GetSceneCamera()->GetPitch(), l_camera->GetSceneCamera()->GetRoll());
						std::string anglex = std::to_string(quaternion.x);
						std::string angley = std::to_string(quaternion.y);
						std::string anglez = std::to_string(quaternion.z);
						std::string anglew = std::to_string(quaternion.w);
						transform_element->SetAttribute("rotation", (anglex + " " + angley + " " + anglez + " " + anglew).c_str());

						std::string lookatx = std::to_string(l_camera->GetLookAt().x);
						std::string lookaty = std::to_string(l_camera->GetLookAt().y);
						std::string lookatz = std::to_string(l_camera->GetLookAt().z);
						transform_element->SetAttribute("look_at", (lookatx + " " + lookaty + " " + lookatz).c_str());

						std::string fov  = std::to_string(l_camera->GetFOV());
						transform_element->SetAttribute("fov", fov.c_str());

						std::string near_plane = std::to_string(l_camera->GetNear());
						transform_element->SetAttribute("near_plane", near_plane.c_str());
						std::string far_plane = std::to_string(l_camera->GetFar());
						transform_element->SetAttribute("far_plane", far_plane.c_str());
					}
				}
			}
			xmldoc.SaveFile("scene.saved.xml");
		}

		void CScene::LoadNodesComponents()
		{
			std::vector<CSceneNode*> l_nodesWithComponents;
			for (CLayer* layer : m_ResourcesVector)
			{
				for (CSceneNode* node : layer->GetResourcesVector() )
				{
					if (node->m_ComponentNames.size() > 0){
						node->LoadScriptsFromNode();
						l_nodesWithComponents.push_back(node);
					}
				}
			}
			for (CSceneNode* node : l_nodesWithComponents)
			{
				node->StartComponents();				
			}
			l_nodesWithComponents.clear();
		}
	}
}
