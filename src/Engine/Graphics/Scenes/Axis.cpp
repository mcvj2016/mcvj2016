#include "Axis.h"
#include "Graphics/Shaders/VertexType.h"
#include "Engine.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Buffers/TemplatedGeometry.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Camera/CameraManager.h"
#include "Graphics/Effects/TechniquePoolManager.h"

using namespace engine::materials;

namespace engine
{
	namespace materials
	{
		CAxis::CAxis(bool cameraAxis) : m_cameraAxis(cameraAxis), CMesh()
		{
			m_Geometries.resize(3);
			m_Materials.resize(3);
			m_Materials[0] = new CMaterial("CROJO");
			m_Materials[0]->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()("PositionNormal"));
			m_Materials[0]->AddParameter(new CTemplatedMaterialParameter<float>("diffuse", 1.0f, CMaterial::eFloat));
			m_Materials[0]->AddParameter(new CTemplatedMaterialParameter<CColor>("diffuse_color", CColor(1.0f, 0.0f, 0.0f, 1.0f), CMaterial::eColor));

			m_Materials[1] = new CMaterial("CVERDE");
			m_Materials[1]->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()("PositionNormal"));
			m_Materials[1]->AddParameter(new CTemplatedMaterialParameter<float>("diffuse", 1.0f, CMaterial::eFloat));
			m_Materials[1]->AddParameter(new CTemplatedMaterialParameter<CColor>("diffuse_color", CColor(0.0f, 1.0f, 0.0f, 1.0f), CMaterial::eColor));

			m_Materials[2] = new CMaterial("CAZUL");
			m_Materials[2]->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()("PositionNormal"));
			m_Materials[2]->AddParameter(new CTemplatedMaterialParameter<float>("diffuse", 1.0f, CMaterial::eFloat));
			m_Materials[2]->AddParameter(new CTemplatedMaterialParameter<CColor>("diffuse_color", CColor(0.0f, 0.0f, 1.0f, 1.0f), CMaterial::eColor));


			render::CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();

			shaders::PositionNormal lRedLineVertex[2] =
			{
				{ Vect3f(0.0f, 0.0f, 0.0f), Vect3f(1.0f, 0.0f, 0.0f) },
				{ Vect3f(1.0f, 0.0f, 0.0f), Vect3f(1.0f, 0.0f, 0.0f) },
			};

			shaders::PositionNormal lGreenLineVertex[2] =
			{
				{ Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 1.0f, 0.0f) },
				{ Vect3f(0.0f, 1.0f, 0.0f), Vect3f(0.0f, 1.0f, 0.0f) },
			};

			shaders::PositionNormal lBlueLineVertex[2] =
			{
				{ Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 0.0f, 1.0f) },
				{ Vect3f(0.0f, 0.0f, 1.0f), Vect3f(0.0f, 0.0f, 1.0f) },
			};

			//uint16 lIndices[6] = { 0, 3, 1, 3, 2, 1 };

			buffers::CVertexBuffer<shaders::PositionNormal> * lVBR = new buffers::CVertexBuffer<shaders::PositionNormal>(lRenderManager, lRedLineVertex, 2);
			//CIndexBuffer* lIB = new CIndexBuffer(lRenderManager, &lIndices, 6, 16);
			m_Geometries[0] = new buffers::CGeometryLinesList<shaders::PositionNormal >(lVBR);

			buffers::CVertexBuffer<shaders::PositionNormal> * lVBB = new buffers::CVertexBuffer<shaders::PositionNormal>(lRenderManager, lGreenLineVertex, 2);
			//CIndexBuffer* lIB = new CIndexBuffer(lRenderManager, &lIndices, 6, 16);
			m_Geometries[1] = new buffers::CGeometryLinesList<shaders::PositionNormal >(lVBB);

			buffers::CVertexBuffer<shaders::PositionNormal> * lVBG = new buffers::CVertexBuffer<shaders::PositionNormal>(lRenderManager, lBlueLineVertex, 2);
			//CIndexBuffer* lIB = new CIndexBuffer(lRenderManager, &lIndices, 6, 16);
			m_Geometries[2] = new buffers::CGeometryLinesList<shaders::PositionNormal >(lVBG);
		}

		CAxis::~CAxis()
		{
			base::utils::CheckedDelete(m_Materials);
			base::utils::CheckedDelete(m_Geometries);
		}

		bool CAxis::Render(render::CRenderManager& aRenderManager)
		{
			if (m_cameraAxis)
			{
				buffers::CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
				CCameraController* cameraCont = CEngine::GetInstance().GetCameraManager().GetMainCamera();
				this->primitive->SetPosition(cameraCont->GetSceneCamera()->GetPosition() + (cameraCont->GetSceneCamera()->GetForward()*0.7f));
				this->primitive->SetPosition(this->primitive->GetPosition() + (cameraCont->GetSceneCamera()->GetRight()*0.2f));
				lCB.mObjDesc.m_World = this->primitive->GetMatrix();
				lCB.BindVSBuffer(aRenderManager.GetDeviceContext(), engine::buffers::CConstantBufferManager::CB_Object);
			}

			bool lOk = true;
			ID3D11DeviceContext* lContext = aRenderManager.GetDeviceContext();
			for (size_t i = 0, lCount = m_Geometries.size(); i < lCount; ++i)
			{
				m_Materials[i]->Apply();
				m_Geometries[i]->Render(lContext);
			}
			return lOk;
		}
	}
}
