#ifndef _ENGINE_EMPTYSCENEMESH_DPVD1_08012017021600_H
#define _ENGINE_EMPTYSCENEMESH_DPVD1_08012017021600_H

#include "SceneNode.h"

namespace engine
{
	namespace materials{
		class CMesh;
	}
	namespace scenes
	{

		class CEmptySceneNode : public CSceneNode
		{
		public:
			CEmptySceneNode(const CXMLElement* aElement);
			virtual ~CEmptySceneNode();
			bool Render(render::CRenderManager& aRendermanager) override;
		};
	}
}
#endif
