#include "SceneNode.h"
#include "XML/XML.h"
#include "Math/Quaternion.h"
#include "Engine.h"
#include "Core/ScriptComponentFactory.h"
#include "Utils/CheckedDelete.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "PhysXImpl/PhysXManager.h"

namespace engine
{
	namespace scenes
	{
		CSceneNode::CSceneNode()
			:
			m_hasPhysx(false),
			m_Visible(true),
			m_FrustumIgnored(false),
			m_LoadedComponents(false),
			m_StartedComponents(false),
			m_Tag(TAG::undefined),
			m_Enabled(true),
			m_IsStatic(false),
			m_IsTrigger(false),
			m_IsKinematic(false),
			m_TriggerActive(false),
			m_OnlyDestroyPhysx(false),
			m_Size(Vect3f(0.0f, 0.0f, 0.0f)),
			m_Radius(0.1f),
			m_DistanceToCamera(0.0f),
			m_Type(TYPE::empty)
		{
		}

		void CSceneNode::PreLoadScriptsFromNode(const CXMLElement* aElement){
			for (const CXMLElement *scriptNode = aElement->FirstChildElement("script_object"); scriptNode != nullptr; scriptNode = scriptNode->NextSiblingElement("script_object")) {
				const std::string& name = scriptNode->GetAttribute<std::string>("name", "");
				assert(name != "");
				m_ComponentNames.push_back(name);
				const CXMLElement* paramElement = scriptNode->FirstChildElement("param");
				std::vector<std::string> l_paramVector;
				while (paramElement != nullptr){
					l_paramVector.push_back(paramElement->GetAttribute<std::string>("value", ""));
					paramElement = paramElement->NextSiblingElement("param");
				}
				if (l_paramVector.size()>0){
					m_Params.insert(std::make_pair(name, l_paramVector));
				}
			}
		}

		void CSceneNode::LoadScriptsFromNode()
		{
			if (!m_LoadedComponents){
				for (const std::string& componentName : m_ComponentNames)
				{
					m_Components.push_back(logic::components::CScriptComponentFactory::GetInstance().CreateInstance(componentName, GetName()));
				}
				m_LoadedComponents = true;
			}
		}

		void CSceneNode::StartComponents()
		{
			if (!m_StartedComponents){
				for (auto component : m_Components)
				{
					if (m_Params.find(component->GetName()) != m_Params.end())
					{
						
						component->InitMembers(m_Params[component->GetName()]);
					}
					component->Start();
				}
				m_StartedComponents = true;
			}
		}

		CSceneNode::CSceneNode(const CXMLElement* aElement) 
			: CName(aElement), m_Visible(aElement->GetAttribute<bool>("visible", true)),
			m_hasPhysx(false),
			m_IsStatic(false),
			m_IsTrigger(false),
			m_IsKinematic(false),
			m_FrustumIgnored(aElement->GetAttribute<bool>("frustum_ignored", false)),
			m_LoadedComponents(false),
			m_StartedComponents(false),
			m_Tag(TAG::undefined),
			m_Enabled(aElement->GetAttribute<bool>("enabled", true)),
			m_TriggerActive(false),
			m_OnlyDestroyPhysx(false),
			m_Size(Vect3f(0.0f, 0.0f, 0.0f)),
			m_Radius(0.1f),
			m_DistanceToCamera(0.0f),
			m_Type(TYPE::empty)
		{
			const CXMLElement* transformElement = aElement->FirstChildElement("transform");
			if (transformElement!=nullptr)
			{
				m_Position = transformElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f) );
				m_Scale = transformElement->GetAttribute<Vect3f>("scale", Vect3f(1.0f, 1.0f, 1.0f));
				m_Rotation = transformElement->GetAttribute<Quatf>("rotation", Quatf(0.0f, 0.0f, 0.0f, 0.0f));
				//Quatf quatToEuler = Quatf(m_Rotation.x, -m_Rotation.y, m_Rotation.z, m_Rotation.w);
				m_Yaw = m_Rotation.GetYaw();
				m_Pitch = m_Rotation.GetPitch();
				m_Roll = m_Rotation.GetRoll();
				m_YawOffSet = transformElement->GetAttribute<float>("yaw_offset", 0.0f);
				m_PitchOffSet = transformElement->GetAttribute<float>("pitch_offset", 0.0f );
				m_RollOffSet = transformElement->GetAttribute<float>("roll_offset", 0.0f);
				
			}

			const CXMLElement* tagElement = aElement->FirstChildElement("tag");
			if (tagElement != nullptr){
				std::string lTag = tagElement->GetAttribute<std::string>("value", "");
				bool typed = EnumString<TAG>::ToEnum(m_Tag, lTag);
				assert(typed);
			}
			PreLoadScriptsFromNode(aElement);
		}
		CSceneNode::~CSceneNode()
		{
			base::utils::CheckedDelete(m_Components);
		}
		
		void CSceneNode::Update(float aDeltaTime)
		{			
		}

		void CSceneNode::SetEnabled(bool enabled)
		{
			m_Enabled = enabled;
			if (m_Enabled)
			{
				for (logic::components::CScriptComponent* component : m_Components)
				{
					component->OnEnable();
				}
			}
			else
			{
				for (logic::components::CScriptComponent* component : m_Components)
				{
					component->OnDisable();
				}
			}
		}

		bool CSceneNode::GetEnabled() const
		{
			return m_Enabled;
		}

		void CSceneNode::UpdateComponents(float aDeltaTime)
		{			
			for (auto component : m_Components)
			{
				//only update if enabled
				if (component->GetEnabled())
				{
					component->Update(aDeltaTime);
					//on trigger stay
					for (std::string meshesInTrigger : m_MeshInTrigger)
					{
						CSceneNode* nodeOther = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(meshesInTrigger);
						component->OnTriggerStay(nodeOther);
					}
				}
			}
		}

		CTransform* CSceneNode::GetTransform()
		{
			return static_cast<CTransform*>(this);
		}

		void CSceneNode::AddComponent(logic::components::CScriptComponent* component)
		{
			m_Components.push_back(component);
		}

		void CSceneNode::AddComponentName(std::string componentName)
		{
			m_ComponentNames.push_back(componentName);
		}

		void CSceneNode::DisableComponents()
		{
			for (logic::components::CScriptComponent* component : m_Components)
			{
				component->SetEnabled(false);
			}
		}

		void CSceneNode::EnableComponents()
		{
			for (logic::components::CScriptComponent* component : m_Components)
			{
				component->SetEnabled(true);
			}
		}

		bool CSceneNode::WasTriggered(CSceneNode* other)
		{
			return std::find(m_MeshInTrigger.begin(), m_MeshInTrigger.end(), other->GetName()) != m_MeshInTrigger.end();
		}

		void CSceneNode::OnTrigger(CSceneNode* other)
		{
			if (std::find(m_MeshInTrigger.begin(), m_MeshInTrigger.end(), other->GetName()) == m_MeshInTrigger.end())
			{
				m_MeshInTrigger.insert(other->GetName());
				OnTriggerEnter(other);
			}
			else if (std::find(m_MeshInTrigger.begin(), m_MeshInTrigger.end(), other->GetName()) != m_MeshInTrigger.end())
			{
				OnTriggerExit(other);
				m_MeshInTrigger.erase(other->GetName());
			}
		}

		void CSceneNode::OnTriggerEnter(CSceneNode* other)
		{
			if (m_Enabled){
				for (auto component : m_Components)
				{
					if (component->GetEnabled())
					{
						component->OnTriggerEnter(other);
					}
				}		
			}
		}

		void CSceneNode::OnTriggerExit(CSceneNode* other)
		{
			if (m_Enabled){
				for (auto component : m_Components)
				{
					if (component->GetEnabled())
					{
						component->OnTriggerExit(other);
					}
				}
			}
		}

		logic::components::CScriptComponent* CSceneNode::GetComponent(std::string name)
		{
			for (auto component : m_Components)
			{
				if (component->GetName()==name)
				{
					return component;
				}
			}
			return nullptr;
		}

		void CSceneNode::SetTriggerActive(bool triggerActive)
		{
			m_TriggerActive = triggerActive;
			if (m_TriggerActive)
			{
				CEngine::GetInstance().GetPhysXManager().ActiveTrigger(m_Name);
			}
			else
			{
				CEngine::GetInstance().GetPhysXManager().DisableTrigger(m_Name);
			}
		}

		bool CSceneNode::IsTriggerActive() const
		{
			return m_TriggerActive;
		}
	}
}
