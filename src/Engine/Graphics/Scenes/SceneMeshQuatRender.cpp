#include "SceneMeshQuatRender.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "XML/XML.h"
#include "Camera/CameraManager.h"
#include "Graphics/Meshes/MeshManager.h"
#include "PhysXImpl/PhysXManager.h"

using namespace engine::materials;

namespace engine
{
	namespace scenes
	{
		CSceneMeshQuatRender::CSceneMeshQuatRender(const CXMLElement* aElement)
			: CSceneNode(aElement),
			m_PhysxFilename("")
		{
			CMeshManager& meshManager = CEngine::GetInstance().GetMeshManager();
			if (aElement->GetAttribute<std::string>("mesh", "") != "")
			{
				m_Mesh = meshManager.GetMesh(aElement->Attribute("mesh"));
				m_PhysxFilename = m_Mesh->GetLocalPath() + "PhysXMeshes/" + aElement->GetAttribute<std::string>("mesh", "");
			}
		}

		CSceneMeshQuatRender::CSceneMeshQuatRender(const CXMLElement* aElement, CMesh* aMesh)
		: CSceneNode(aElement), 
		m_Mesh(aMesh), 
		m_PhysxFilename("")
		{
			
		}

		CSceneMeshQuatRender::~CSceneMeshQuatRender()
		{
			
		}

		bool CSceneMeshQuatRender::Render(render::CRenderManager& aRendermanager)
		{
			bool lOk = false;
			if (m_Mesh)
			{
				//check dentro del frustrum
				CFrustum* lFrustum = CEngine::GetInstance().GetCameraManager().GetMainCamera()->GetFrustum();
				if (m_FrustumIgnored || lFrustum->IsVisible(m_Mesh->GetBoundingSphere(), m_Position, m_Scale))
				{
					buffers::CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
					lCB.mObjDesc.m_World = GetMatrixQuat();
					lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), buffers::CConstantBufferManager::CB_Object);
					lOk = m_Mesh->Render(aRendermanager);
				}				
			}
			return lOk;
		}
	}
}
