#ifndef _ENGINE_SCENELIGHT_DPVD1_08012017021600_H
#define _ENGINE_SCENELIGHT_DPVD1_08012017021600_H

#include "SceneNode.h"

namespace engine
{
	namespace lights
	{
		class CLight;
	}
}

namespace engine
{
	namespace scenes
	{
		class CSceneLight : public CSceneNode
		{
		public:
			CSceneLight(const CXMLElement* aElement);
			CSceneLight(const CXMLElement* aElement, lights::CLight* aLight);
			virtual ~CSceneLight();
			lights::CLight* GetLight() const;
			bool Render(render::CRenderManager& aRendermanager) override;
		protected:
			lights::CLight* m_Light;
			
		};
	}
}
#endif
