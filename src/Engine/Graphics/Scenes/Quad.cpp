#include "Quad.h"


#include "Utils/CheckedDelete.h"

#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Shaders/VertexShader.h"
#include "Render/RenderManager.h"
#include "Engine.h"
#include "Graphics/Buffers/VertexBuffer.h"
#include "Graphics/Buffers/TemplatedGeometry.h"
#include "Graphics/Shaders/VertexType.h"
#include "Graphics/Effects/Effect.h"
#include "Graphics/Effects/TechniquePoolManager.h"

using namespace engine::shaders;
using namespace engine::buffers;

CQuad::CQuad()
    : mGeometry( nullptr )
{
	Init();
}

CQuad::~CQuad()
{
	base::utils::CheckedDelete(mGeometry);
}

bool CQuad::Init()
{
    bool lOk = false;
	engine::render::CRenderManager& lRenderManager = engine::CEngine::GetInstance().GetRenderManager();
	PositionUV lScreenVertexsQuad[4] =
    {
        { Vect3f(-1.0f, 1.0f, 0.5f),  Vect2f(0.0f, 0.0f) },
        { Vect3f(-1.0f, -1.0f, 0.5f), Vect2f(0.0f, 1.0f) },
        { Vect3f(1.0f, 1.0f, 0.5f),   Vect2f(1.0f, 0.0f) },
        { Vect3f(1.0f, -1.0f, 0.5f),  Vect2f(1.0f, 1.0f) }
    };

	CVertexBuffer<PositionUV> * lVB = new CVertexBuffer<PositionUV>(lRenderManager, lScreenVertexsQuad, 4);
	mGeometry = new CGeometryTriangleStrip< PositionUV >(lVB);

    return lOk;
}

bool CQuad::Render()
{
	engine::render::CRenderManager& lRenderManager = engine::CEngine::GetInstance().GetRenderManager();
    ID3D11DeviceContext* lContext = lRenderManager.GetDeviceContext();
    return mGeometry->Render(lContext);
}

bool CQuad::Render(engine::effects::CEffect *Effect)
{
	engine::render::CRenderManager& lRenderManager = engine::CEngine::GetInstance().GetRenderManager();
	ID3D11DeviceContext* lContext = lRenderManager.GetDeviceContext();
	Effect->Bind(lContext);
	return Render();
}