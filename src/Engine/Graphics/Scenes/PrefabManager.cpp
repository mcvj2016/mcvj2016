#include "PrefabManager.h"
#include "XML/XML.h"
#include "Engine.h"
#include "SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include <algorithm>
#include "Utils/StringUtils.h"

#include <iomanip>
#include <numeric>

namespace engine
{
	namespace scenes
	{
		CPrefabManager::CPrefabManager()
		{
		}

		CPrefabManager::~CPrefabManager()
		{
		}

		bool CPrefabManager::Load()
		{
			CXMLDocument xmldoc;
			bool loaded = false;

			//Load the textures of this scene
			std::string path = "data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/prefabs.xml";
			loaded = base::xml::SucceedLoad(xmldoc.LoadFile(path.c_str()));

			assert(loaded);

			CXMLElement* prefabs = xmldoc.FirstChildElement("prefabs");
			if (prefabs != nullptr)
			{
				CXMLElement* prefab = prefabs->FirstChildElement("prefab");
				while (prefab != nullptr){
					m_PrefabElements.push_back(LoadPrefabFromLayer(prefab));

					prefab = prefab->NextSiblingElement("prefab");
				}
			}
			

			return loaded;
		}

		CSceneNode* CPrefabManager::LoadAnimatedModel(const CXMLElement* aElement)
		{
			cal3dimpl::CAnimatedModelManager& animationsManager = CEngine::GetInstance().GetAnimatedModelManager();

			cal3dimpl::CSceneAnimatedModel* sceneAnimated = new cal3dimpl::CSceneAnimatedModel(aElement);
			sceneAnimated->m_LayerName = aElement->GetAttribute<std::string>("layer", "");
			sceneAnimated->m_hasPhysx = aElement->GetAttribute<bool>("physx", true);
			std::string model = aElement->Attribute("model");

			//add it to the layer of the scene
			if (!CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetByName(sceneAnimated->m_LayerName)->Add(sceneAnimated->GetName(), sceneAnimated))
			{
				base::utils::CheckedDelete(sceneAnimated);
			}
			else
			{
				sceneAnimated->Initialize(animationsManager(model));
				sceneAnimated->HideAnimated();
				sceneAnimated->SetEnabled(false);
			}

			return sceneAnimated;
		}

		std::string CPrefabManager::GetUniqueName(const std::string& baseName)
		{
			const std::vector<CSceneNode*> sceneNodes = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNodes();
			std::string baseNameToLower = baseName;

			std::transform(baseNameToLower.begin(), baseNameToLower.end(), baseNameToLower.begin(),
				[](unsigned char c){ return std::toupper(c); }
			);

			for (size_t i = 1; i < sceneNodes.size(); --i) {
				//grab the node name
				std::string nodeName = sceneNodes.at(i)->GetName();
				//tolowercase and compare if has the basename prefab name
				std::transform(nodeName.begin(), nodeName.end(), nodeName.begin(),
					[](unsigned char c){ return std::toupper(c); }
				);
				
				//find first occurrence by the end of the array
				std::size_t found = baseNameToLower.find(nodeName);
				if (found != std::string::npos)
				{
					std::vector<std::string> splitted = base::utils::Split(nodeName, '_');
					const int prefabCount = std::stoi(splitted.back()) + 1;
					splitted.pop_back(); //delete las item, the last prefab count

					return base::utils::Join(splitted) + std::to_string(prefabCount);
				}
			}	
			
			return baseNameToLower + "_Clone_1";
		}

		CSceneNode* CPrefabManager::LoadPrefabFromLayer(const CXMLElement* aElement)
		{
			const std::string& layerName = aElement->GetAttribute<std::string>("layer", "");
			assert(layerName != "");

			CSceneNode* node = nullptr;

			//TODO: add more layers for creating the correct scenenode
			if (layerName == "animated_models")
			{
				node = LoadAnimatedModel(aElement);
			}

			return node;
		}

		CSceneNode* CPrefabManager::Instantiate(const std::string& name, const Vect3f& pos, const Quatf& rot)
		{
			for (CSceneNode* element : m_PrefabElements)
			{
				if (element->GetName() == name)
				{
					if (element->GetType() == CSceneNode::TYPE::animated)
					{
						cal3dimpl::CSceneAnimatedModel* node = static_cast<cal3dimpl::CSceneAnimatedModel*>(element);
						std::string name = node->GetName();
						base::utils::RemoveSubstring(name, "Prefab");
						node->SetName(GetUniqueName(name));
						node->SetPosition(pos);
						node->setRotationFromQuat(rot);
						node->SetEnabled(true);
						node->ShowAnimated();
					}
					
					return element;
				}
			}

			assert(!"We should not reach this point");

			return nullptr;
		}
	}
}
