#include "Plane.h"
#include "Graphics/Buffers/TemplatedIndexedGeometry.h"
#include "Graphics/Shaders/VertexType.h"
#include "Engine.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Effects/TechniquePoolManager.h"


namespace engine
{
	namespace materials
	{
		CPlane::CPlane(): CMesh()
		{
			//m_Materials.resize(1);
			m_Geometries.resize(1);

			//CMaterialManager& lMM = CEngine::GetInstance().GetMaterialManager();
			m_Materials.resize(1);
			m_Materials[0] = new CMaterial("PrimitivePlane");
			m_Materials[0]->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()("PositionNormal"));

			bool lOk = false;

			shaders::PositionNormal lPlaneVertex[4] =
			{
				{ Vect3f(-0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f) },
				{ Vect3f(0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f) },
				{ Vect3f(0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f) },
				{ Vect3f(-0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f) }
			};

			uint16 lIndices[6] = { 0, 3, 1, 3, 2, 1 };
			render::CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
			buffers::CVertexBuffer<shaders::PositionNormal> * lVB = new buffers::CVertexBuffer<shaders::PositionNormal>(lRenderManager, lPlaneVertex, 4);
			buffers::CIndexBuffer* lIB = new buffers::CIndexBuffer(lRenderManager, &lIndices, 6, 16);
			m_Geometries[0] = new buffers::CIndexedGeometryTriangleList<shaders::PositionNormal >(lVB, lIB);
		}

		CPlane::~CPlane()
		{
			base::utils::CheckedDelete(m_Materials);
			base::utils::CheckedDelete(m_Geometries);
		}
	}
}
