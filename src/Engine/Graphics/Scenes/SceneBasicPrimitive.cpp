#include "SceneBasicPrimitive.h"
#include "XML/XML.h"
#include "Engine.h"
#include "Render/RenderManager.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Plane.h"
#include "Axis.h"
#include "Utils/CheckedDelete.h"

using namespace engine::materials;

namespace engine
{
	namespace scenes
	{
		CSceneBasicPrimitive::CSceneBasicPrimitive(const CXMLElement* aElement)
			: CSceneMesh(aElement)
		{
			if (aElement->GetAttribute<std::string>("type", "") == "plane"){
				m_Mesh = new CPlane();
			}
			else if(aElement->GetAttribute<std::string>("type", "") == "axis"){
				CAxis* axis = new CAxis(aElement->GetAttribute<bool>("camera", false));
				if (axis->m_cameraAxis)
				{
					axis->primitive = this;
				}
				m_Mesh = axis;
			}
		}

		CSceneBasicPrimitive::~CSceneBasicPrimitive()
		{
			base::utils::CheckedDelete(m_Mesh);
		}
	}
}
