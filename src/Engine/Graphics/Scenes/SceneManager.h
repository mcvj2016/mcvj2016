#ifndef _SCENEMANAGER_DPVD1_31122016165200_H
#define _SCENEMANAGER_DPVD1_31122016165200_H

#include "Utils/TemplatedMapVector.h"
#include "Scene.h"

namespace engine
{
	namespace scenes
	{
		class CSceneManager : public base::utils::CTemplatedMapVector<CScene> 
		{
		public:
			CSceneManager();
			virtual ~CSceneManager();
			bool Load(const std::string& activeScene);
			bool Update(float elapsedTime);
			bool Render(const std::string& aLayer);
			bool Empty();
			void ChangeScene(const std::string& name);
			bool PerformChangeScene();
			bool Reload();
			void SetActive(const std::string& aScene, bool aBool);
			void Save();
			CScene* GetCurrentScene();
			std::string m_NextScene;
			bool m_ChangingScene;
		protected: 
			std::string m_Filename;
			
		};
	}
}
#endif
