#ifndef __CQUAD_HH__
#define __CQUAD_HH__

#pragma once
#include "Graphics/Shaders/Shader.h"

namespace engine
{
	namespace effects
	{
		class CEffect;
	}
	namespace materials
	{
		class CGeometry;
	}
	namespace shaders
	{
		class CPixelShader;
	}
}

class CQuad
{
public:
  CQuad();
  virtual ~CQuad();

  bool Init();
  bool Render();
  bool Render(engine::effects::CEffect *Effect);

private:
	engine::materials::CGeometry* mGeometry;
};

#endif
