#ifndef _ENGINE_SCENECAMERA_DPVD1_08012017021600_H
#define _ENGINE_SCENECAMERA_DPVD1_08012017021600_H

#include "SceneNode.h"

class CCameraController;

using namespace engine::scenes;

namespace engine
{
	namespace scenes
	{
		class CSCeneCamera : public CSceneNode
		{
		public:
			CSCeneCamera(const CXMLElement* aElement);
			CSCeneCamera(const CXMLElement* aElement, CCameraController* cam);
			virtual ~CSCeneCamera();

			bool Render(render::CRenderManager& aRendermanager) override;
			CCameraController* GetCamera() const;

		protected:
			CCameraController* m_Camera;
		};
	}
}

#endif _ENGINE_SCENECAMERA_DPVD1_08012017021600_H
