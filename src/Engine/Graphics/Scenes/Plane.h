#ifndef __CPlane_HH__
#define __CPlane_HH__

#pragma once

#include "Graphics/Meshes/Mesh.h"
#include "Graphics/Meshes/Geometry.h"


namespace engine
{
	namespace materials
	{
		//class CGeometry;
		class CMaterial;

		class CPlane : public engine::materials::CMesh
		{
		public:
			CPlane();
			virtual ~CPlane();
		};
	}
}
#endif
