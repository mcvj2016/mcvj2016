#ifndef _DIRECTIONALLIGHT_DPVD1_08012017101200_H
#define _DIRECTIONALLIGHT_DPVD1_08012017101200_H

#include "Light.h"

namespace engine
{
	namespace lights
	{
		class CDirectionalLight : public CLight
		{
		public:
			CDirectionalLight();
			CDirectionalLight(const CXMLElement* aElement);
			virtual ~CDirectionalLight();
			void SetShadowMap(render::CRenderManager& RM) override;
		protected:
			Vect2f m_OrthoShadowMapSize;
		};
	}
}

#endif
