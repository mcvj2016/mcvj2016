#include "Light.h"
#include "XML/XML.h"
#include "Graphics/Meshes/Mesh.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Scenes/SceneLight.h"
#include "Math/Mathconstants.cpp"
#include "Graphics/Scenes/SceneManager.h"

using namespace engine::materials;

namespace engine
{
	namespace lights
	{
		CLight::CLight(ELightType aLightType)
			: CName(""),
			  m_Type(aLightType),
			  m_Intensity(0.0f),
			  m_Angle(0),
			  m_FallOff(0),
			  m_Enabled(false),
			  m_GenerateShadowMap(false),
			  m_Color(CColor()), m_SceneLight(nullptr),
			  m_Layers(std::vector<scenes::CLayer*>()),
			  m_ShadowMap(nullptr),
			  m_ShadowMaskTexture(nullptr),
			  m_ViewShadowMap(m44fZERO),
			  m_ProjectionShadowMap(m44fZERO)
		{
		}

		CLight::CLight(const CXMLElement* aElement, ELightType aLightType)
			: CName(aElement),
			m_GenerateShadowMap(aElement->GetAttribute<bool>("generate_shadow_map", false)),
			m_Type(aLightType),// get atribute para leer string, aElement equivale a un <light>....</light> completo a saco
			m_Intensity(aElement->GetAttribute<float>("intensity", 0.0f)), // no definido, por definir aElement->GetAttribute<float>("specularIntensity", 0.0f)
			m_Color(aElement->GetAttribute<CColor>("color", CColor(1.0f, 1.0f, 1.0f, 1.0f))),
			m_Enabled(aElement->GetAttribute<bool>("enabled", true))
		{			
			if (m_GenerateShadowMap){
				const CXMLElement* layersElement = aElement->FirstChildElement("layers");
				if (layersElement != nullptr){
					const CXMLElement* layer = layersElement->FirstChildElement("layer");

					scenes::CSceneManager& lSM = CEngine::GetInstance().GetSceneManager();

					while (layer != nullptr){
						const CXMLElement* layerElement = layer->ToElement();
						std::string name = layerElement->GetAttribute<std::string>("name", "");
						assert(name != "");
						m_layerNames.push_back(name);
						layer = layer->NextSiblingElement();
					}
				}

				m_ShadowMap = new render::CDynamicTexture(
					GetName() + "_shadowmap",
					aElement->GetAttribute<float>("shadow_map_width", .0f),
					aElement->GetAttribute<float>("shadow_map_height", .0f),
					true,
					aElement->GetAttribute<std::string>("shadow_map_format", "")
					);
				m_ShadowMaskTexture = nullptr;
			}
			else
			{
				m_ShadowMap = nullptr;
				m_ShadowMaskTexture = nullptr;
			}
		}

		void CLight::SetLayers(){
			//load other values if generates shadowmap
			if (m_GenerateShadowMap){
				for (std::string layerName : m_layerNames){
					scenes::CLayer* layer = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetByName(layerName);
					m_Layers.push_back(layer);
				}	
			}
			
		}

		bool CLight::Render(render::CRenderManager& aRendermanager)
		{
			/*materials::CMesh * lMesh = CEngine::GetInstance().GetMeshManager().GetMesh("DummyLight.bin");
			bool lOk = false;
			if (lMesh)
			{
				lOk = lMesh->Render(aRendermanager);
			}
			return lOk;*/
			return true;
		}

		void CLight::SetSceneLight(scenes::CSceneLight* aSceneLight)
		{
			m_SceneLight = aSceneLight;
		}

		scenes::CSceneLight* CLight::GetSceneLight()
		{
			return this->m_SceneLight;
		}

		CLight::~CLight()
		{
		}
	}
}
