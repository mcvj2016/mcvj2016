#include "DirectionalLight.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Scenes/SceneLight.h"
#include "XML/XML.h"
#include "Graphics/Textures/TextureManager.h"
#include "Camera/CameraManager.h"

namespace engine
{
	namespace lights
	{//DONE Implementar los constructores
		CDirectionalLight::CDirectionalLight() 
			: CLight(eDirectional)
		{ 
		}

		CDirectionalLight::CDirectionalLight(const CXMLElement* aElement) 
			: CLight(aElement,eDirectional)
		{ 
			if (m_GenerateShadowMap)
			{
				m_OrthoShadowMapSize = aElement->GetAttribute<Vect2f>("ortho_shadow_map_size", v2fZERO);
				assert(m_OrthoShadowMapSize != v2fZERO);
				materials::CTextureManager& ltm = CEngine::GetInstance().GetTextureManager();
				ltm.AddTexture(m_ShadowMap);
			}
		}

		CDirectionalLight::~CDirectionalLight()
		{
		}
		
		void CDirectionalLight::SetShadowMap(render::CRenderManager &RM)
		{
			/*m_ViewShadowMap.SetIdentity();
			CCameraController* l_auxCameraController = CEngine::GetInstance().GetCameraManager().GetMainCamera();
			m_RangeAttenuation = Vect2f(0, 100);
			scenes::CSceneLight* sceneLight = GetSceneLight();
			m_ViewShadowMap.SetIdentity();
			float l_Half_Range = 100;
			Vect3f l_Position = l_auxCameraController->GetPosition() + l_auxCameraController->GetForward().GetNormalized() * l_Half_Range*0.5f - sceneLight->GetForward().GetNormalized() * l_Half_Range*0.5f;
			Vect3f up = Vect3f(sceneLight->GetForward().z, sceneLight->GetForward().y, sceneLight->GetForward().x);
			up = ((up) ^ (sceneLight->GetForward()));
			m_ViewShadowMap.SetFromLookAt(l_Position, l_Position + sceneLight->GetForward(), up.y < 0 ? (up * -1) : up);
			 */
			m_RangeAttenuation = Vect2f(0, 100);
			scenes::CSceneLight* sceneLight = GetSceneLight();
			m_ViewShadowMap.SetIdentity();
			m_ViewShadowMap.SetFromLookAt(sceneLight->GetPosition(), sceneLight->GetPosition() + sceneLight->GetForward(), v3fY);
			

			
			unsigned int l_ShadowMapWidth = m_ShadowMap->GetSize().x;
			unsigned int l_ShadowMapHeight = m_ShadowMap->GetSize().y;
			m_ProjectionShadowMap.SetFromOrtho(m_OrthoShadowMapSize.x, m_OrthoShadowMapSize.y,
				0.1f, m_RangeAttenuation.y);
			buffers::CConstantBufferManager& lCBM = CEngine::GetInstance().GetConstantBufferManager();
			RM.SetViewProjectionMatrix(m_ViewShadowMap, m_ProjectionShadowMap);
			lCBM.mFrameDesc.m_View = m_ViewShadowMap;
			lCBM.mFrameDesc.m_Projection = m_ProjectionShadowMap;
			lCBM.mFrameDesc.m_ViewProjection = RM.GetViewProjectionMatrix();
			lCBM.mFrameDesc.m_InverseView = lCBM.mFrameDesc.m_View.GetInverted();
			lCBM.mFrameDesc.m_InverseProjection = lCBM.mFrameDesc.m_Projection.GetInverted();
			lCBM.BindVSBuffer(RM.GetDeviceContext(), buffers::CConstantBufferManager::CB_Frame);
			lCBM.BindPSBuffer(RM.GetDeviceContext(), buffers::CConstantBufferManager::CB_FramePS);
			ID3D11RenderTargetView *l_RenderTargetViews[1];
			l_RenderTargetViews[0] = m_ShadowMap->GetRenderTargetView();
			D3D11_VIEWPORT m_viewport;
			m_viewport.Width = static_cast<float>(l_ShadowMapWidth);
			m_viewport.Height = static_cast<float>(l_ShadowMapHeight);
			m_viewport.MinDepth = 0.0f;
			m_viewport.MaxDepth = 1.0f;
			m_viewport.TopLeftX = 0.0f;
			m_viewport.TopLeftY = 0.0f;
			RM.GetDeviceContext()->RSSetViewports(1, &m_viewport);
			RM.SetRenderTargets(1, l_RenderTargetViews, m_ShadowMap->GetDepthStencilView());
		}
	}
}
