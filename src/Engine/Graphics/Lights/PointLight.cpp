#include "PointLight.h"
#include "XML/XML.h"

namespace engine
{
	namespace lights
	{
		CPointLight::CPointLight()
			: CLight(ePoint)
		{ }
		CPointLight::CPointLight(const CXMLElement* aElement)
			: CLight(aElement, ePoint)
		{
			m_RangeAttenuation = aElement->GetAttribute<Vect2f>("atenuation_range", Vect2f(0.0f, 0.0f));
		}
		CPointLight::~CPointLight()
		{ }

		void CPointLight::SetShadowMap(render::CRenderManager& RM)
		{
		}
	}
}
