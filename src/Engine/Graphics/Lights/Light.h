#ifndef _ENGINE_LIGHT_DPVD1_08012017092800_H
#define _ENGINE_LIGHT_DPVD1_08012017092800_H

#include "Math/Color.h"
#include "Utils/EnumToString.h"
#include "Utils/Defines.h"
#include <vector>
#include "Math/Matrix44.h"
#include "Render/DynamicTexture.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace render {
		class CRenderManager;
	}

	namespace scenes
	{
		class CSceneLight;
		class CLayer;
		class CSceneNode;
	}
	namespace materials
	{
		class CTexture;
	}
}

namespace engine
{
	namespace lights
	{
		class CLight : public CName
		{
		public:
			enum ELightType//tipo
			{
				ePoint = 0,
				eSpot,
				eDirectional
			};

			CLight(ELightType aLightType);

			CLight(const CXMLElement* aElement, ELightType aLightType);

			virtual ~CLight();

			bool Render(render::CRenderManager& aRendermanager);

			GET_SET(ELightType, Type);
			GET_SET(float, Intensity);
			GET_SET_BOOL(Enabled);
			GET_SET_REF(CColor, Color);
			GET_SET_REF(Vect2f, RangeAttenuation);
			GET_PTR(render::CDynamicTexture, ShadowMap);
			GET_PTR(materials::CTexture, ShadowMaskTexture);
			GET_REF(Mat44f, ViewShadowMap);
			GET_REF(Mat44f, ProjectionShadowMap);
			GET_REF(std::vector<scenes::CLayer *>, Layers);
			virtual void SetShadowMap(render::CRenderManager &RenderManager) = 0;

			void SetSceneLight(scenes::CSceneLight* aSceneLight);
			scenes::CSceneLight* GetSceneLight();

			GET_SET(float, Angle);
			GET_SET(float, FallOff);
			bool m_GenerateShadowMap;
			void SetLayers();

		protected:
			ELightType  m_Type;
			float		m_Intensity;
			CColor		m_Color;
			Vect2f		m_RangeAttenuation;
			scenes::CSceneLight * m_SceneLight;
			float m_Angle;
			float m_FallOff;
			bool m_Enabled;
			
			render::CDynamicTexture *m_ShadowMap;
			materials::CTexture *m_ShadowMaskTexture;
			std::vector<scenes::CLayer *> m_Layers;
			Mat44f m_ViewShadowMap, m_ProjectionShadowMap;

		private:
			std::vector<std::string> m_layerNames;
			
		};
	}
}

//---------------------------------------------------------------------------------------------------------
Begin_Enum_String(engine::lights::CLight::ELightType)
{
	Enum_String_Id(engine::lights::CLight::ePoint, "point");
	Enum_String_Id(engine::lights::CLight::eSpot, "spot");
	Enum_String_Id(engine::lights::CLight::eDirectional, "directional");
}
End_Enum_String;


#endif