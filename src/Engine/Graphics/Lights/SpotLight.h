#ifndef _SPOTLIGHT_DPVD1_08012017153400_H
#define _SPOTLIGHT_DPVD1_08012017153400_H

#include "Light.h"

namespace engine
{
	namespace lights
	{
		class CSpotLight : public CLight
		{
		public:
			CSpotLight();

			CSpotLight(const CXMLElement* aElement);

			virtual ~CSpotLight();
			void SetShadowMap(render::CRenderManager& RM) override;
		};
	}
}
#endif
