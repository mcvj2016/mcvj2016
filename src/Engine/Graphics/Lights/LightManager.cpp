#include "Light.h"
#include "LightManager.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "DirectionalLight.h" 
#include "XML/XML.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Scenes/SceneLight.h"
#include "Graphics/Scenes/SceneManager.h"

#define ID_SHADOWMAP	5

namespace engine
{
	namespace lights
	{
		//Valor por defecto ok?
		CLightManager::CLightManager()
			:m_LevelLightsFilename("lights.xml")
		{ }

		CLightManager::~CLightManager()
		{ 
		}

		void CLightManager::SetLightsLayers()
		{
			for (CLight* light : m_ResourcesVector){
				light->SetLayers();
			}
		}

		bool CLightManager::Load(std::string aName)
		{
			tinyxml2::XMLDocument xmldoc;

			bool loaded = false;

			if (aName == "")
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_LevelLightsFilename.c_str()));
			}
			else
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(aName.c_str()));
			}

			assert(loaded);

			tinyxml2::XMLElement* lights = xmldoc.FirstChildElement("lights");

			if (lights != nullptr)
			{

				tinyxml2::XMLNode* light = lights->FirstChild();

				while (light != nullptr)
				{
					tinyxml2::XMLElement* element = light->ToElement();

					std::string type = element->Attribute("type");
					std::string lightName = element->Attribute("name");

					//create the scenelight
					CLight::ELightType lType;
					if (EnumString<CLight::ELightType>::ToEnum(lType, type))
					{
						switch (lType)
						{
						case CLight::ePoint:
						{
							CPointLight* pLight = new CPointLight(element);
							Add(lightName, pLight);
							scenes::CSceneLight* lLight = new scenes::CSceneLight(element, pLight);
							lLight->GetLight()->SetSceneLight(lLight); //referencia cruzada de scenelight a light
							pLight->SetSceneLight(lLight);
							CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("lights")->Add(lightName, lLight);
						}
						break;

						case CLight::eSpot:
						{
							CSpotLight* sLight = new CSpotLight(element);
							Add(lightName, sLight);
							scenes::CSceneLight* lLight = new scenes::CSceneLight(element, sLight);
							lLight->GetLight()->SetSceneLight(lLight); //referencia cruzada de scenelight a light
							sLight->SetSceneLight(lLight);
							CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("lights")->Add(lightName, lLight);
						}
						break;

						case CLight::eDirectional:
						{
							CDirectionalLight* dLight = new CDirectionalLight(element);
							Add(lightName, dLight);
							scenes::CSceneLight* lLight = new scenes::CSceneLight(element, dLight);
							lLight->GetLight()->SetSceneLight(lLight); //referencia cruzada de scenelight a light
							dLight->SetSceneLight(lLight);
							CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("lights")->Add(lightName, lLight);
						}
						break;
						}
					}

					//pasar al siguiente elemento dentro de <Lights>
					light = light->NextSiblingElement("light");
				}
			}

			return true;
		}

		void CLightManager::Reload()
		{
			Destroy();
			this->Load("data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/" + m_LevelLightsFilename);
		}

		void CLightManager::SetLightConstants(int idLight, CLight * iLight){
			
			scenes::CSceneLight* sceneLight = iLight->GetSceneLight();
			buffers::CConstantBufferManager &l_CM = CEngine::GetInstance().GetConstantBufferManager();
			l_CM.mLightsDesc.m_LightAmbient = Vect4f(0.336f, 0.336f, 0.336f, 1.0f);
			l_CM.mLightsDesc.m_LightEnabled[idLight] = static_cast<float>(iLight->IsEnabled());
			l_CM.mLightsDesc.m_LightType[idLight] = static_cast<float>(iLight->GetType());
			l_CM.mLightsDesc.m_LightPosition[idLight] = Vect4f(iLight->GetSceneLight()->GetPosition(),1.0f);
			l_CM.mLightsDesc.m_LightDirection[idLight] = Vect4f(iLight->GetSceneLight()->GetForward(), 1.0f);
			l_CM.mLightsDesc.m_LightAngle[idLight] = iLight->GetAngle();
			l_CM.mLightsDesc.m_LightFallOffAngle[idLight] = iLight->GetFallOff();
			l_CM.mLightsDesc.m_LightAttenuationStartRange[idLight] = iLight->GetRangeAttenuation().x;
			l_CM.mLightsDesc.m_LightAttenuationEndRange[idLight] = iLight->GetRangeAttenuation().y;
			l_CM.mLightsDesc.m_LightIntensity[idLight] = iLight->GetIntensity();
			l_CM.mLightsDesc.m_LightColor[idLight] = iLight->GetColor();
			l_CM.mLightsDesc.m_UseShadowMap[idLight] = static_cast<float>(iLight->m_GenerateShadowMap);
			l_CM.mLightsDesc.m_UseShadowMask[idLight] = static_cast<float>(false);
			//TODO: que putos valores van aqui
			l_CM.mLightsDesc.m_ShadowMapBias[idLight] = .0f;
			l_CM.mLightsDesc.m_ShadowMapStrength[idLight] = .0f;
			/*
			float4 m_ShadowMapBias;
			float4 m_ShadowMapStrength;
			*/
			l_CM.mLightsDesc.m_LightView[idLight] = iLight->GetViewShadowMap();
			l_CM.mLightsDesc.m_LightProjection[idLight] = iLight->GetProjectionShadowMap();


			//shadow map bindings
			if (iLight->m_GenerateShadowMap){
				engine::render::CDynamicTexture *l_ShadowMap = iLight->GetShadowMap();

				engine::materials::CTexture *l_ShadowMask = iLight->GetShadowMaskTexture();

				l_CM.mLightsDesc.m_UseShadowMask[idLight] = l_ShadowMask != nullptr ? 1.0f : 0.0f;
				l_CM.mLightsDesc.m_LightView[idLight] = iLight->GetViewShadowMap();
				l_CM.mLightsDesc.m_LightProjection[idLight] = iLight->GetProjectionShadowMap();

				engine::render::CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();

				l_ShadowMap->Bind(ID_SHADOWMAP, lRM.GetDeviceContext());

				if (l_ShadowMask != nullptr){
					l_ShadowMask->Bind(ID_SHADOWMAP + 1, lRM.GetDeviceContext());
				}
			}
		}

		void CLightManager::SetLightsConstants(){
			//solo 4 luces para forward, i < 4
			for (size_t i = 0; i < this->GetCount() && i < 4; i++)
			{
				SetLightConstants(i, this->operator[](i));
			}
		}

		void CLightManager::UpdateConstants(){
			buffers::CConstantBufferManager &l_CM = CEngine::GetInstance().GetConstantBufferManager();
			render::CRenderManager &l_CRM = CEngine::GetInstance().GetRenderManager();
			l_CM.BindPSBuffer(l_CRM.GetDeviceContext(), buffers::CConstantBufferManager::CB_LightPS);
		}

		CLight* CLightManager::GetLight(const std::string& aName)
		{
			return this->operator()(aName);
		}
	}
}
