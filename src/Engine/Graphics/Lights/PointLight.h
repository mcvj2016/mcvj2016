
#ifndef _POINTLIGHT_DPVD1_08012017103400_H
#define _POINTLIGHT_DPVD1_08012017103400_H

#include "Light.h"

namespace engine
{
	namespace lights
	{
		class CPointLight : public CLight
		{
		public:
			CPointLight();

			CPointLight(const CXMLElement* aElement);

			virtual ~CPointLight();
			void SetShadowMap(render::CRenderManager& RM) override;
		};
	}
}

#endif
