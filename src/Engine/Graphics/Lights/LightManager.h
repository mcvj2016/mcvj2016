#ifndef _LIGHTMANAGER_DPVD1_08012017155600_H
#define _LIGHTMANAGER_DPVD1_08012017155600_H

#include "Utils/TemplatedMapVector.h"
#include "Light.h"

namespace engine
{
	namespace lights
	{
		class CLightManager : public base::utils::CTemplatedMapVector<CLight>
		{
		public:

			CLightManager();

			virtual ~CLightManager();
			bool Load(std::string aName = "");
			void Reload();
			void SetLightConstants(int idLight, CLight * iLight); //segun el idLight establecera las constantes de la luz en el CB
			void SetLightsConstants(); //recorre el vector de luces y establece las luces en el CB
			void UpdateConstants(); //pasa a VRAN el CB
			CLight* GetLight(const std::string& aName);
			void SetLightsLayers();

			GET_SET(Vect4f, AmbientLightColor);

		private:

			Vect4f m_AmbientLightColor;

			std::string m_LevelLightsFilename;

			std::string m_XmlName;
		};
	}
}

#endif
