#include "SpotLight.h"
#include "XML/XML.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Graphics/Scenes/SceneLight.h"

namespace engine
{
	namespace lights
	{
		CSpotLight::CSpotLight()
			: CLight(eSpot)
		{ }

		CSpotLight::CSpotLight(const CXMLElement* aElement)
			: CLight(aElement, eSpot)
		{
			m_FallOff = aElement->GetAttribute<float>("fall_off", 0);
			m_Angle = aElement->GetAttribute<float>("angle", 0);
			m_RangeAttenuation = aElement->GetAttribute<Vect2f>("atenuation_range", Vect2f(0.0f, 0.0f));
		}

		CSpotLight::~CSpotLight()
		{ }

		void CSpotLight::SetShadowMap(render::CRenderManager &RM)
		{
			if (m_ShadowMap == NULL)
				return;
			m_ViewShadowMap.SetIdentity();
			scenes::CSceneLight* sceneLight = GetSceneLight();
			m_ViewShadowMap.SetFromLookAt(sceneLight->m_Position, sceneLight->m_Position + sceneLight->GetForward(), v3fY);
			unsigned int l_ShadowMapWidth = m_ShadowMap->GetSize().x;
			unsigned int l_ShadowMapHeight = m_ShadowMap->GetSize().y;
			m_ProjectionShadowMap.SetIdentity();
			m_ProjectionShadowMap.SetFromPerspective(m_FallOff,
			l_ShadowMapWidth / static_cast<float>(l_ShadowMapHeight), 0.1f, m_RangeAttenuation.y);
			buffers::CConstantBufferManager& lCBM = CEngine::GetInstance().GetConstantBufferManager();
			RM.SetViewProjectionMatrix(m_ViewShadowMap, m_ProjectionShadowMap);
			lCBM.mFrameDesc.m_View = m_ViewShadowMap;
			lCBM.mFrameDesc.m_Projection = m_ProjectionShadowMap;
			lCBM.mFrameDesc.m_ViewProjection = RM.GetViewProjectionMatrix();
			lCBM.mFrameDesc.m_InverseView = lCBM.mFrameDesc.m_View.GetInverted();
			lCBM.mFrameDesc.m_InverseProjection = lCBM.mFrameDesc.m_Projection.GetInverted();
			lCBM.BindVSBuffer(RM.GetDeviceContext(), buffers::CConstantBufferManager::CB_Frame);
			lCBM.BindPSBuffer(RM.GetDeviceContext(), buffers::CConstantBufferManager::CB_FramePS);
			ID3D11RenderTargetView *l_RenderTargetViews[1];
			l_RenderTargetViews[0] = m_ShadowMap->GetRenderTargetView();
			D3D11_VIEWPORT m_viewport;
			m_viewport.Width = static_cast<float>(l_ShadowMapWidth);
			m_viewport.Height = static_cast<float>(l_ShadowMapHeight);
			m_viewport.MinDepth = 0.0f;
			m_viewport.MaxDepth = 1.0f;
			m_viewport.TopLeftX = 0.0f;
			m_viewport.TopLeftY = 0.0f;
			RM.GetDeviceContext()->RSSetViewports(1, &m_viewport);
			RM.SetRenderTargets(1, l_RenderTargetViews, m_ShadowMap->GetDepthStencilView());
		}
	}
}
