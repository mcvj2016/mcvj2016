#ifndef _ENGINE_TECHNIQUE_DPVD1_29122016055100_H
#define _ENGINE_TECHNIQUE_DPVD1_29122016055100_H

#include "Utils/Defines.h"
#include <d3d11.h>

namespace engine
{
	namespace effects
	{
		class CEffect;
		class CTechnique
		{
		public:
			CTechnique();
			virtual ~CTechnique();
			void SetEffect(CEffect* aEffect);
			CEffect* GetEffect() const;
			void Bind(ID3D11DeviceContext* aContext);
			void Unbind(ID3D11DeviceContext* aContext);
		private:
			DISALLOW_COPY_AND_ASSIGN(CTechnique);
			CEffect* mEffect;
		};
	}
}
#endif
