#ifndef _EFFECTMANAGER_DPVD1_28122016162600_H
#define _EFFECTMANAGER_DPVD1_28122016162600_H

#include "Effect.h"
#include "Utils/TemplatedMapVector.h"

namespace engine
{
	namespace effects
	{
		class CEffectManager : public base::utils::CTemplatedMapVector<CEffect>
		{
		public:
			CEffectManager();
			virtual ~CEffectManager();
			//bool Load(const std::string& aFilename);
			bool Load();
			bool Reload();
		private:
			DISALLOW_COPY_AND_ASSIGN(CEffectManager);
			std::string m_Filename;
		};
	}
}
#endif