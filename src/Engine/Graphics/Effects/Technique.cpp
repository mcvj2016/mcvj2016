#include "Technique.h"
#include "Effect.h"

namespace engine
{
	namespace effects
	{
		CTechnique::CTechnique()
		{

		}

		CTechnique::~CTechnique()
		{

		}

		void CTechnique::SetEffect(CEffect* aEffect)
		{
			mEffect = aEffect;
		}

		CEffect* CTechnique::GetEffect() const
		{
			return mEffect;
		}

		void CTechnique::Bind(ID3D11DeviceContext* aContext)
		{
			mEffect->Bind(aContext);
		}

		void CTechnique::Unbind(ID3D11DeviceContext* aContext)
		{
			mEffect->Unbind(aContext);
		}
	}
}