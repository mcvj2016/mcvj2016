#ifndef _TECHNIQUEPOOLMANAGER_DPVD1_31122016044100_H
#define _TECHNIQUEPOOLMANAGER_DPVD1_31122016044100_H

#include "TechniquePool.h"
#include "Technique.h"

namespace engine
{
	namespace effects
	{
		class CTechniquePoolManager : public base::utils::CTemplatedMapVector<CTechnique>
		{
		public:
			CTechniquePoolManager();
			virtual ~CTechniquePoolManager();
			//bool Load(const std::string& aFilename);
			bool Load();
			bool Reload();
			bool Apply(const std::string& aPoolName);
		private:
			DISALLOW_COPY_AND_ASSIGN(CTechniquePoolManager);
			std::string m_Filename;
			typedef base::utils::CTemplatedMapVector<CTechniquePool> TMapPools;
			TMapPools mPools;
		};
	}
}
#endif