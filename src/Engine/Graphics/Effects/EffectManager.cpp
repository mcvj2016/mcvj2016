#include "EffectManager.h"
#include "XML/XML.h"

namespace engine
{
	namespace effects
	{
		CEffectManager::CEffectManager() : m_Filename("data/common/effects.xml")
		{ }

		CEffectManager::~CEffectManager()
		{ }

		bool CEffectManager::Load()
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Shaders";
			}
			tinyxml2::XMLElement* effects = xmldoc.FirstChildElement("effects");
			if (effects != nullptr)
			{
				tinyxml2::XMLElement* effectElement = effects->FirstChildElement("effect");
				while (effectElement != nullptr)
				{
					CEffect* effect = new CEffect(effectElement);
					this->Add(effect->GetName(), effect);
					effectElement = effectElement->NextSiblingElement("effect");
				}
			}
			return true;
		}

		bool CEffectManager::Reload()
		{
			Destroy();
			return Load();
		}
	}
}
