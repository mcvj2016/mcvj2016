#include "TechniquePool.h"
#include "XML/XML.h"
#include "Engine.h"
#include "EffectManager.h"

namespace engine
{
	namespace effects
	{
		CTechniquePool::CTechniquePool()
		{
			
		}

		CTechniquePool::~CTechniquePool()
		{
			this->Clear();
		}

		void CTechniquePool::Load(const CXMLElement* aElement)
		{
			CEffectManager &effect_manager = CEngine::GetInstance().GetEffectManager();
			const CXMLElement* technique_element = aElement->FirstChildElement("technique");
			while (technique_element != nullptr)
			{	
				std::string vertexType = technique_element->GetAttribute<std::string>("vertex_type", "");
				std::string effectName = technique_element->GetAttribute<std::string>("effect", "");

				if (effectName == "")
				{
					if (vertexType != "")
					{
						Add(vertexType, nullptr);
					}
					else
					{
						Add(technique_element->GetAttribute<std::string>("name", ""), nullptr);
					}
				}
				else
				{
					if (vertexType != "")
					{
						Add(vertexType, effect_manager(effectName));
					}
					else
					{
						Add(technique_element->GetAttribute<std::string>("name", ""), effect_manager(effectName));
					}
				}
				
				technique_element = technique_element->NextSiblingElement("technique");
			}
		}
	}
}
