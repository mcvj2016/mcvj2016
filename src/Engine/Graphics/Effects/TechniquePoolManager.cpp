#include "TechniquePoolManager.h"
#include "XML/XML.h"

namespace engine
{
	namespace effects
	{
		CTechniquePoolManager::CTechniquePoolManager() : m_Filename("data/common/techniques_pool.xml")
		{

		}

		CTechniquePoolManager::~CTechniquePoolManager()
		{
			
		}

		bool CTechniquePoolManager::Load()
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Techniques Pool";
			}

			tinyxml2::XMLElement* technique_pools = xmldoc.FirstChildElement("technique_pools");
			if (technique_pools != nullptr)
			{
				tinyxml2::XMLElement* pool_element = technique_pools->FirstChildElement("pool");
				while (pool_element != nullptr)
				{
					CTechniquePool* technique_pool = new CTechniquePool();
					technique_pool->SetName(pool_element->Attribute("name"));
					technique_pool->Load(pool_element);
					mPools.Add(pool_element->Attribute("name"), technique_pool);
					pool_element = pool_element->NextSiblingElement("pool");
				}
			}

			Apply("default");

			return true;
		}

		bool CTechniquePoolManager::Reload()
		{
			Destroy();
			return Load();
		}

		bool CTechniquePoolManager::Apply(const std::string& aPoolName)
		{
			if (mPools.Exist(aPoolName)){
				CTechniquePool* pool = mPools(aPoolName);
				CTechnique* technique;
				for (const auto &myPair : pool->GetResourcesMap()) {
					if (this->Exist(myPair.first))
					{
						technique = this->operator()(myPair.first);
						technique->SetEffect(pool->operator()(myPair.first));
					}
					else{
						technique = new CTechnique();
						technique->SetEffect(pool->operator()(myPair.first));
						this->Add(myPair.first, technique);
					}
				}
			}
			return true;
		}
	}
}
