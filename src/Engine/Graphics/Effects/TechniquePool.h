#ifndef _ENGINE_TECHNIQUEPOOL_DPVD1_29122016055100_H
#define _ENGINE_TECHNIQUEPOOL_DPVD1_29122016055100_H

#include "Effect.h"
#include "Utils/TemplatedMapVector.h"
#include "Utils/Name.h"

namespace engine
{
	namespace effects
	{
		class CTechniquePool : public base::utils::CTemplatedMapVector<CEffect>, public CName
		{
		public:
			CTechniquePool();
			virtual ~CTechniquePool();
			void Load(const CXMLElement* aElement);
		private:
			DISALLOW_COPY_AND_ASSIGN(CTechniquePool);
		};
	}
}
#endif