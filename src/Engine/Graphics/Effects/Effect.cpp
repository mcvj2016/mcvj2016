#include "Effect.h"
#include "Engine.h"
#include "Graphics/Shaders/ShaderManager.h"


namespace engine
{
	namespace effects
	{
		CEffect::CEffect()
		{
		}

		CEffect::~CEffect()
		{
		}

		CEffect::CEffect(const CXMLElement* aElement)
		{
			//Resize del vector
			m_Shaders.resize(shaders::CShader::EStageCount);

			//Asigno nombre del effecto
			std::string aEffectName = aElement->Attribute("name");
			m_Name = aEffectName;

			//Asigna los shaders al el effecto
			shaders::CShaderManager& shaderManager = CEngine::GetInstance().GetShaderManager();
			const tinyxml2::XMLElement* shader = aElement->FirstChildElement("shader");
			while (shader != nullptr)
			{
				std::string aShaderType = shader->Attribute("stage");
				std::string aShaderName = shader->Attribute("name");
				shaders::CShader::EShaderStage lType;
				if (EnumString<shaders::CShader::EShaderStage>::ToEnum(lType, aShaderType))
				{
					if (aShaderName == "ParticlesEffect")
					{
						aShaderName = "ParticlesEffect";
					}
					m_Shaders[lType] = shaderManager.GetShader(lType, aShaderName);
				}
				shader = shader->NextSiblingElement("shader");
			}
		}

		void CEffect::Bind(ID3D11DeviceContext* aContext)
		{
			for (size_t i = 0, lCount = m_Shaders.size(); i < lCount; ++i)
			{
				if (m_Shaders[i] != nullptr){
					m_Shaders[i]->Bind(aContext);
				}
			}
		}

		void CEffect::Unbind(ID3D11DeviceContext* aContext)
		{
			for (size_t i = 0, lCount = m_Shaders.size(); i < lCount; ++i)
			{
				if (m_Shaders[i] != nullptr){
					m_Shaders[i]->Unbind(aContext);
				}
			}
		}

		void CEffect::Refresh()
		{
			//TODO IMPLEMENTAR EL TODO
		}
	}
}
