#ifndef _EFFECT_DPVD1_28122016160800_H
#define _EFFECT_DPVD1_28122016160800_H

#include "Utils/Name.h"
#include <vector>
#include "Graphics/Shaders/Shader.h"

namespace engine
{
	namespace effects
	{
		class CEffect : public CName
		{
		public:
			CEffect();
			CEffect(const CXMLElement* aElement);
			virtual ~CEffect();
			void SetShader(shaders::CShader::EShaderStage aType, shaders::CShader* aShader);
			template < typename TShaderType > TShaderType* GetShader(shaders::CShader::EShaderStage aType) const
			{
				return static_cast<TShaderType*>(m_Shaders[aType]);
			}
			void Bind(ID3D11DeviceContext* aContext);
			void Unbind(ID3D11DeviceContext* aContext);
			void Refresh();
		private:
			DISALLOW_COPY_AND_ASSIGN(CEffect);
			std::vector<shaders::CShader* > m_Shaders;
		};
	}
}
#endif