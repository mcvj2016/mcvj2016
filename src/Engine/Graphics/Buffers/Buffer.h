#ifndef _ENGINE_BUFFER_DPVD1_27102016203720_H
#define _ENGINE_BUFFER_DPVD1_27102016203720_H
#include <d3d11.h>
#include "Utils/Defines.h"

namespace engine
{
	namespace buffers
	{
		class CBuffer
		{
		public:
			enum BufferUsage
			{
				eDefault = 0,
				eImmutable,
				eDynamic,
				eStaging
			};
			CBuffer() : m_Buffer(nullptr){}
			virtual ~CBuffer() {}
			GET_SET_PTR(ID3D11Buffer, Buffer);
			virtual void Update(ID3D11DeviceContext*, void*) {}
			virtual void Bind(ID3D11DeviceContext*) = 0;
		protected:
			ID3D11Buffer* m_Buffer;
		};
	}
}
#endif	// _ENGINE_BUFFER_DPVD1_27102016203720_H