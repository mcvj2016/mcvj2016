#ifndef _ENGINE_TEMPLATEDGEOMETRY_DPVD1_27102016203720_H
#define _ENGINE_TEMPLATEDGEOMETRY_DPVD1_27102016203720_H

#include <d3d11.h>
#include "Graphics/Buffers/VertexBuffer.h"
#include "Utils/CheckedDelete.h"
#include "Utils/VertexTypes_HelperMacros.h"
#include "Graphics/Meshes/Geometry.h"

namespace engine
{
	namespace buffers
	{
		template< class TVertexType >
		class CTemplatedGeometry : public materials::CGeometry
		{
		public:
			CTemplatedGeometry(CVertexBuffer<TVertexType>* VertexBuffer, D3D11_PRIMITIVE_TOPOLOGY PrimitiveTopology)
				: CGeometry(PrimitiveTopology)
				, m_VertexBuffer(VertexBuffer)
			{
			}

			virtual ~CTemplatedGeometry()
			{
				base::utils::CheckedDelete(m_VertexBuffer);
			}

			virtual bool Render(ID3D11DeviceContext* aContext)
			{
				// Send the vertex buffer to the GPU
				m_VertexBuffer->Bind(aContext);
				// Configure the type of topology to be renderer ( p.e. Triangles, Quads, points,... )
				aContext->IASetPrimitiveTopology(m_PrimitiveTopology);
				// Finally draw the geometry
				aContext->Draw(m_VertexBuffer->GetNumVertexs(), 0);
				return true;
			}

			virtual bool Render(ID3D11DeviceContext* aContext, int num_vertices)
			{
				//m_VertexBuffer->SetNumVertexs(num_vertices);
				/*Render(aContext);
				return true;*/
				// Send the vertex buffer to the GPU
				m_VertexBuffer->Bind(aContext);
				// Configure the type of topology to be renderer ( p.e. Triangles, Quads, points,... )
				aContext->IASetPrimitiveTopology(m_PrimitiveTopology);
				// Finally draw the geometry
				aContext->Draw(num_vertices, 0);
				return true;
			}

			bool UpdateVertexs(void *Vertices, unsigned int VerticesCount)
			{
				D3D11_MAPPED_SUBRESOURCE mappedResource;
				ZeroMemory(&mappedResource, sizeof(mappedResource));

				ID3D11DeviceContext * l_DeviceContext = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
				HRESULT l_HR = l_DeviceContext->Map(m_VertexBuffer->GetBuffer(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
				if (FAILED(l_HR))
					return false;

				memcpy(mappedResource.pData, Vertices, sizeof(TVertexType) * VerticesCount);

				l_DeviceContext->Unmap(m_VertexBuffer->GetBuffer(), 0);

				return true;
			}
		protected:
			engine::buffers::CVertexBuffer<TVertexType> *m_VertexBuffer;
		};

		GEOMETRY_DEFINITION(CGeometryPointList, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		GEOMETRY_DEFINITION(CGeometryLinesList, D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
		GEOMETRY_DEFINITION(CGeometryTriangleList, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		GEOMETRY_DEFINITION(CGeometryTriangleStrip, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	}
}
#endif //_ENGINE_TEMPLATEDGEOMETRY_DPVD1_27102016203720_H