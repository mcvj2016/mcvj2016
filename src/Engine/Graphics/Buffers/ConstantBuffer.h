#ifndef _ENGINE_CONSTANTBUFFER_DPVD1_08012017032200_H
#define _ENGINE_CONSTANTBUFFER_DPVD1_08012017032200_H

#include "Buffer.h"
#include <cassert>
#include "Utils/Logger/Logger.h"
#include "Render/RenderManager.h"

namespace engine
{
	namespace buffers
	{
		class CConstantBuffer : public CBuffer
		{
		public:
			CConstantBuffer(render::CRenderManager& RenderManager, uint32 aByteWidth)
				: CBuffer()
			{
				assert(aByteWidth % 16 == 0);
				ID3D11Device * lDevice = RenderManager.GetDevice();
				// Create the constant buffers for the variables defined in the vertex shader.
				D3D11_BUFFER_DESC constantBufferDesc;
				ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));
				constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				constantBufferDesc.ByteWidth = aByteWidth;
				constantBufferDesc.CPUAccessFlags = 0;
				constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
				HRESULT lHR = lDevice->CreateBuffer(&constantBufferDesc, nullptr, &m_Buffer);
				//LOG_ERROR_APPLICATION_IF(FAILED(lHR), "Error creating constant buffer");
			}

			CConstantBuffer(render::CRenderManager& RenderManager, void* aRawData, uint32 aByteWidth)
				: CBuffer()
			{
				assert(aByteWidth % 16 == 0);
				// Fill in a buffer description.
				D3D11_BUFFER_DESC lConstantBuffer;
				ZeroMemory(&lConstantBuffer, sizeof(lConstantBuffer));
				lConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
				lConstantBuffer.CPUAccessFlags = 0;
				lConstantBuffer.ByteWidth = aByteWidth;
				lConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				lConstantBuffer.MiscFlags = 0;
				lConstantBuffer.StructureByteStride = 0;
				// Fill in the subresource data.
				D3D11_SUBRESOURCE_DATA InitData;
				InitData.pSysMem = aRawData;
				m_RawData = aRawData;
				InitData.SysMemPitch = 0;
				InitData.SysMemSlicePitch = 0;
				// Create the buffer.
				ID3D11Device *lDevice = RenderManager.GetDevice();
				HRESULT lHR = lDevice->CreateBuffer(&lConstantBuffer, &InitData, &m_Buffer);
				LOG_ERROR_APPLICATION_IF(FAILED(lHR), "Error creating immutable constant buffer");
			}
			virtual ~CConstantBuffer()
			{
			}
			virtual void Bind(ID3D11DeviceContext* aContext)
			{
				assert("This method must not be called!");
			}
			virtual void BindVS(ID3D11DeviceContext* aContext, uint32 aBufferID)
			{
				aContext->VSSetConstantBuffers(aBufferID, 1, &m_Buffer);
			}

			virtual void BindPS(ID3D11DeviceContext* aContext, uint32 aBufferID)
			{
				aContext->PSSetConstantBuffers(aBufferID, 1, &m_Buffer);
			}

			virtual void BindGS(ID3D11DeviceContext* aContext, uint32 aBufferID)
			{
				aContext->GSSetConstantBuffers(aBufferID, 1, &m_Buffer);
			}

			virtual void Update(ID3D11DeviceContext* aContext, void* aRawData)
			{
				aContext->UpdateSubresource(m_Buffer, 0, NULL, aRawData, 0, 0);
			}

			void * GetRawData() const { return m_RawData; }
		protected:
			void *m_RawData;
		};
	}
}

#endif
