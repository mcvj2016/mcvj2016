#include "AnimatedModelManager.h"
#include "XML/XML.h"

namespace engine
{
	namespace cal3dimpl
	{
		CAnimatedModelManager::CAnimatedModelManager() :m_Filename("data/common/animated_models.xml")
		{
			//CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS | LOADER_INVERT_V_COORD);
		}

		CAnimatedModelManager::~CAnimatedModelManager()
		{
			
		}

		//void Load(const std::string &Filename)
		void CAnimatedModelManager::Load()
		{
			tinyxml2::XMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));
			if (!loaded)
			{
				throw "Error Loading Animated Model Manager XML";
			}
			tinyxml2::XMLElement* animatedModels = xmldoc.FirstChildElement("animated_models");
			if (animatedModels != nullptr){
				tinyxml2::XMLElement* animatedModelElement = animatedModels->FirstChildElement("animated_model");
				while (animatedModelElement != nullptr)
				{
					std::string coreModelName = animatedModelElement->Attribute("name");
					CAnimatedCoreModel* coreModel = new CAnimatedCoreModel(animatedModelElement);
					coreModel->Load();
					if (!this->Add(coreModelName, coreModel))
					{
						base::utils::CheckedDelete(coreModel);
					}
					animatedModelElement = animatedModelElement->NextSiblingElement("animated_model");
				}
			}			
		}

		void CAnimatedModelManager::Reload()
		{
			Destroy();
			Load();
		}
	}
}
