#ifndef _ANIMATEDMODELMANAGER_DPVD1_1701207044200_H
#define _ANIMATEDMODELMANAGER_DPVD1_1701207044200_H

#include "Utils/TemplatedMap.h"
#include "AnimatedCoreModel.h"

namespace engine
{
	namespace cal3dimpl
	{
		class CAnimatedModelManager : public base::utils::CTemplatedMap<CAnimatedCoreModel>
		{
		public:
			CAnimatedModelManager();
			virtual ~CAnimatedModelManager();
			void Load();
			void Reload();
		private:
			std::string m_Filename;
		};
	}
}
#endif
