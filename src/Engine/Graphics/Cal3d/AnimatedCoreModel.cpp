#include "Graphics/Scenes/SceneNode.h"
#include "AnimatedCoreModel.h"
#include "Engine.h"
#include "XML/XML.h"
#include "Graphics/Effects/TechniquePoolManager.h"
#include "Graphics/Textures/Texture.h"
#include "Cal3D/src/cal3d/loader.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"
#include "Math/Color.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Textures/TextureManager.h"
#include "Utils/StringUtils.h"
#include "Graphics/Materials/Material.h"
#include "Graphics/Meshes/Mesh.h"
#include "Graphics/Meshes/MeshManager.h"
#include "Callbacks/FactoryCallbacks.h"

namespace engine
{
	namespace cal3dimpl
	{
		/*CAnimatedCoreModel::CAnimatedCoreModel()
		{
			m_CalCoreModel = new CalCoreModel(m_Name);
			m_Materials = std::vector<materials::CMaterial*>();
		}

		CAnimatedCoreModel::CAnimatedCoreModel(std::string& aName) : CName(aName),m_Path("data/animations/default")
		{
			CAnimatedCoreModel();
		}*/

		CAnimatedCoreModel::CAnimatedCoreModel()
			: CName(),
			m_Weapon(nullptr),
			m_PhysxMesh(nullptr),
			m_HasBump(false),
			m_Tag(CSceneNode::TAG::undefined),
			m_CalCoreModel(new CalCoreModel(m_Name)),
			m_Path(""),
			m_BSRadius(0),
			m_InvertUV(false)
		{
		}

		CAnimatedCoreModel::CAnimatedCoreModel(const std::string& aName)
			: CAnimatedCoreModel()
		{
			m_Name = aName;
		}

		CAnimatedCoreModel::CAnimatedCoreModel(const CXMLElement* aElement)
			: CAnimatedCoreModel(aElement->GetAttribute<std::string>("name", ""))
		{
			m_Path = aElement->Attribute("path");
			assert(m_Name != "");
			assert(m_Path != "");
		}

		CAnimatedCoreModel::~CAnimatedCoreModel()
		{
			base::utils::CheckedDelete(m_CalCoreModel);
			base::utils::CheckedDelete(m_Weapon);
		}

		bool CAnimatedCoreModel::LoadMesh(const std::string &Filename)
		{
			bool loaded = m_CalCoreModel->loadCoreMesh(Filename) != -1;
			assert(loaded);
			return loaded;
		}
		bool CAnimatedCoreModel::LoadSkeleton(const std::string &Filename)
		{
			bool loaded = m_CalCoreModel->loadCoreSkeleton(Filename) != -1;
			assert(loaded);
			return loaded;
		}
		bool CAnimatedCoreModel::LoadAnimation(const std::string &Name, const std::string &Filename)
		{
			int id = m_CalCoreModel->loadCoreAnimation(Filename, Name);
			assert(id >= 0);

			return true;
		}

		bool CAnimatedCoreModel::LoadCallback(const std::string &AnimationName, const std::string &CallbackName)
		{
			int id = m_CalCoreModel->getCoreAnimationId(AnimationName);
			m_CalCoreModel->getCoreAnimation(id)->registerCallback(
				CCallbacksFactory::GetInstance().GetCallback(CallbackName), 180.0f);
			assert(id >= 0);

			return true;
		}

		CalCoreModel* CAnimatedCoreModel::GetCoreModel()
		{
			return m_CalCoreModel;
		}

		const std::vector<materials::CMaterial *> & CAnimatedCoreModel::GetMaterials() const
		{
			return m_Materials;
		}

		//void CAnimatedCoreModel::Load(const std::string &Path)
		void CAnimatedCoreModel::Load()
		{
			tinyxml2::XMLDocument xmldoc;
			std::string filePath = m_Path + "actor.xml";
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(filePath.c_str()));
			if (!loaded)
			{
				throw "Error Loading Animated Core Model XML";
			}
			CXMLElement* actorElement = xmldoc.FirstChildElement("actor");
			if (actorElement != nullptr)
			{
				
				//guardamos atributo nombre
				std::string actorName = actorElement->Attribute("name");
				//guardamos atributo nombre
				m_BSRadius = actorElement->GetAttribute<float>("radius_bs", 1.0f);
				m_BSPosition = actorElement->GetAttribute<Vect3f>("pos_bs", Vect3f(0.0f, 0.0f, 0.0f));
				m_InvertUV = actorElement->GetAttribute<bool>("invert_uv", false);
				CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS);
				if (m_InvertUV){
					CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS | LOADER_INVERT_V_COORD);
				}

				//Cargamos el esqueleto
				CXMLElement* skeletonElement = actorElement->FirstChildElement("skeleton");
				LoadSkeleton(m_Path + skeletonElement->Attribute("filename"));

				//Cargamos las meshes;
				CXMLElement* meshElement = actorElement->FirstChildElement("mesh");
				while (meshElement != nullptr){
					LoadMesh(m_Path + meshElement->Attribute("filename"));
					meshElement = meshElement->NextSiblingElement("mesh");
				}

				//Cargamos los materiales
				CXMLElement* materialElement = actorElement->FirstChildElement("material");
				while (materialElement != nullptr){
					materials::CMaterial* animationMaterial = new materials::CMaterial();
					animationMaterial->SetName(materialElement->Attribute("name"));

					std::string vertexType = materialElement->Attribute("vertex_type");
					animationMaterial->SetTechnique(CEngine::GetInstance().GetTechniquePoolManager()(vertexType));

					tinyxml2::XMLElement* textureElement = materialElement->FirstChildElement("texture");

					while (textureElement != nullptr)
					{
						//guardamos atributo nombre
						std::string textureName = textureElement->Attribute("filename");
						std::string textureType = textureElement->Attribute("type");

						std::vector<std::string> splitted = base::utils::Split(textureName, '/');

						std::string path = textureName.substr(0, (textureName.size() - splitted.back().size()));
						textureName = splitted.back();

						materials::CTexture* texture = new materials::CTexture(textureName);
						texture->SetPath(path);
						texture->Load();
						if (texture != nullptr){
							materials::CMaterial::ETextureIndex lType;
							if (EnumString<materials::CMaterial::ETextureIndex>::ToEnum(lType, textureType))
							{
								switch (lType)
								{
								case materials::CMaterial::eDiffuse:
									texture->SetTextureIndex(materials::CMaterial::eDiffuse);
									break;
								case materials::CMaterial::eBump:
									m_HasBump = true;
									texture->SetTextureIndex(materials::CMaterial::eBump);
									break;
								case materials::CMaterial::eLightMap:
									texture->SetTextureIndex(materials::CMaterial::eLightMap);
									break;
								case materials::CMaterial::eSpecular:
									texture->SetTextureIndex(materials::CMaterial::eSpecular);
									break;
								}
							}
							if (!CEngine::GetInstance().GetTextureManager().Add(texture->GetName(), texture))
							{
								animationMaterial->AddTexture(CEngine::GetInstance().GetTextureManager()(texture->GetName()));
								base::utils::CheckedDelete(texture);								
							}
							else
							{
								animationMaterial->AddTexture(texture);
							}
						}
						textureElement = textureElement->NextSiblingElement("texture");
					}

					tinyxml2::XMLElement* paramElement = materialElement->FirstChildElement("parameter");

					while (paramElement != nullptr)
					{
						//guardamos atributo nombre
						std::string paramName = paramElement->Attribute("name");
						std::string paramType = paramElement->Attribute("type");

						materials::CMaterial::TParameterType lType;
						if (EnumString<materials::CMaterial::TParameterType>::ToEnum(lType, paramType))
						{
							materials::CMaterialParameter* lParameter = nullptr;
							switch (lType)
							{
							case materials::CMaterial::eFloat: lParameter = new CTemplatedMaterialParameter<float>(paramName, paramElement->GetAttribute<float>("value", 0.0f), materials::CMaterial::eFloat); break;
							case materials::CMaterial::eFloat2: lParameter = new CTemplatedMaterialParameter<Vect2f>(paramName, paramElement->GetAttribute<Vect2f>("value", Vect2f(0.0f, 0.0f)), materials::CMaterial::eFloat2); break;
							case materials::CMaterial::eFloat3: lParameter = new CTemplatedMaterialParameter<Vect3f>(paramName, paramElement->GetAttribute<Vect3f>("value", Vect3f(0.0f, 0.0f, 0.0f)), materials::CMaterial::eFloat3); break;
							case materials::CMaterial::eFloat4: lParameter = new CTemplatedMaterialParameter<Vect4f>(paramName, paramElement->GetAttribute<Vect4f>("value", Vect4f(0.0f, 0.0f, 0.0f, 0.0f)), materials::CMaterial::eFloat4); break;
							case materials::CMaterial::eColor: lParameter = new CTemplatedMaterialParameter<CColor>(paramName, paramElement->GetAttribute<CColor>("value", CColor(0.0f, 0.0f, 0.0f)), materials::CMaterial::eColor); break;
							}

							if (lParameter){
								animationMaterial->AddParameter(lParameter);
							}
						}
						paramElement = paramElement->NextSiblingElement("parameter");
					}
					m_Materials.push_back(animationMaterial);
					materials::CMaterialManager & l_MM = CEngine::GetInstance().GetMaterialManager();
					if (!l_MM.Add(animationMaterial->GetName(), animationMaterial))
					{
						base::utils::CheckedDelete(animationMaterial);
					}
					materialElement = materialElement->NextSiblingElement("material");
				}

				//Cargamos las animaciones;
				CXMLElement* animationElement = actorElement->FirstChildElement("animation");
				while (animationElement != nullptr){
					std::string animationName = animationElement->Attribute("name");
					LoadAnimation(animationName, m_Path + animationElement->Attribute("filename"));
					animationElement = animationElement->NextSiblingElement("animation");
				}

				//Cargamos los callback asociados;
				CXMLElement* callbackElement = actorElement->FirstChildElement("callback");
				while (callbackElement != nullptr){
					LoadCallback(callbackElement->Attribute("animation_name"), callbackElement->Attribute("callback_name"));
					callbackElement = callbackElement->NextSiblingElement("callback");
				}

				//Cargamos los script del personaje;
				CXMLElement* scriptElement = actorElement->FirstChildElement("script_object");
				while (scriptElement != nullptr){
					std::string scriptName = scriptElement->Attribute("name");
					m_ComponentNames.push_back(scriptName);
					CXMLElement* paramElement = scriptElement->FirstChildElement("param");
					std::vector<std::string> l_paramVector;
					while (paramElement != nullptr){
						l_paramVector.push_back(paramElement->GetAttribute<std::string>("value", ""));
						paramElement = paramElement->NextSiblingElement("param");
					}
					if (l_paramVector.size()>0){
						m_Params.insert(std::make_pair(scriptName, l_paramVector));
					}
					scriptElement = scriptElement->NextSiblingElement("script_object");
				}

				//Cargamos el arma
				CXMLElement* weaponElement = actorElement->FirstChildElement("weapon");
				while (weaponElement != nullptr){
					m_Weapon = new CWeapon(weaponElement);
					m_Weapon->m_hasPhysx = weaponElement->GetAttribute<bool>("physx", true);
					if (m_Weapon->m_hasPhysx){
						m_PhysxMesh = m_Weapon->GetMesh()->GetPhysxMesh(m_Weapon->m_PhysxFilename);

						//add the mesh to the manager so it will take care of destroying it
						if (!CEngine::GetInstance().GetMeshManager().Add(m_Weapon->GetName(), (materials::CMesh*)m_PhysxMesh))
						{
							base::utils::CheckedDelete(m_PhysxMesh);
						}
					}					

					weaponElement = weaponElement->NextSiblingElement("weapon");
				}

				//Cargamos el tag
				CXMLElement* tagElement = actorElement->FirstChildElement("tag");
				if (tagElement != nullptr){
					std::string lTag = tagElement->GetAttribute<std::string>("value", "");					
					bool typed = EnumString<scenes::CSceneNode::TAG>::ToEnum(m_Tag, lTag);
					assert(typed);					
				}
			}
		}		
	}
}
