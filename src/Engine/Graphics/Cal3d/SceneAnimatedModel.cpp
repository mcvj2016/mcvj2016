#include "SceneAnimatedModel.h"
#include "XML/XML.h"
#include "Graphics/Buffers/TemplatedIndexedGeometry.h"
#include "Graphics/Shaders/VertexType.h"
#include "Engine.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Cal3D/src/cal3d/mixer.h"
#include "Math/Quaternion.h"
#include "AnimatedCoreModel.h"
#include "Graphics/Materials/Material.h"
#include "Cal3D/src/cal3d/skeleton.h"
#include "PhysXImpl/PhysXManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Input/ActionManager.h"
#include "Helper/ImguiHelper.h"

namespace engine
{
	namespace cal3dimpl
	{
		CSceneAnimatedModel::CSceneAnimatedModel(const CXMLElement* aElement)
			: CSceneNode(aElement), m_InstantiateInmediate(true), m_CalModel(nullptr),
			  m_AnimatedCoreModel(nullptr),
			  m_CalHardwareModel(nullptr),
			  m_Geometry(nullptr),
			  m_NumVertices(0),
			  m_NumFaces(0),
			  m_ActionAnimationId(0),
			  m_CycleAnimationId(0),
			  m_Weapon(nullptr),
			  m_Height(1),
			  m_Radius(0.5f), m_PhysxGroup(CPhysXManager::PhysxGroups::Default),
			  m_PhysxMaterialName("default")
		{
			
		}

		void CSceneAnimatedModel::AddPhysx(const CXMLElement* aElement)
		{
			m_Height = aElement->GetAttribute<float>("height", 1);
			m_Radius = aElement->GetAttribute<float>("radius", 0.5f);
			m_InstantiateInmediate = aElement->GetAttribute<bool>("instantiate", true);
			bool grouped = EnumString<CPhysXManager::PhysxGroups>::ToEnum(m_PhysxGroup, aElement->GetAttribute<std::string>("group", "default"));
			assert(grouped);

			m_PhysxMaterialName = aElement->GetAttribute<std::string>("material", "default");
		}

		CSceneAnimatedModel::~CSceneAnimatedModel()
		{
			base::utils::CheckedDelete(m_Geometry);
			base::utils::CheckedDelete(m_CalHardwareModel);
			base::utils::CheckedDelete(m_CalModel);
		}

		void CSceneAnimatedModel::InstantiatePhysx() const
		{
			CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
			lPhysxManager.AddCharacterController(m_Name, m_Height, m_Radius, m_Position, Quatf(0, 0, 0, 1), "default", 1, m_PhysxGroup);
			
			if (m_Weapon != nullptr && m_Weapon->m_hasPhysx){
				lPhysxManager.CreateConvexMesh(
					m_Weapon->GetName(),
					"default",
					m_Weapon->GetPosition(),
					m_Weapon->GetRotation(),
					m_AnimatedCoreModel->m_PhysxMesh->numVertexes,
					m_AnimatedCoreModel->m_PhysxMesh->m_Vertexes,
					m_AnimatedCoreModel->m_PhysxMesh->numIndices,
					m_AnimatedCoreModel->m_PhysxMesh->m_Indices,
					false,
					true,
					true
				);
				m_Weapon->SetTriggerActive(false);
				m_Weapon->LoadScriptsFromNode();
				//m_Weapon->StartComponents();
			}
		}

		bool CSceneAnimatedModel::Initialize(CAnimatedCoreModel *animatedCoreModel)
		{
			m_AnimatedCoreModel = animatedCoreModel;
			m_CalModel = new CalModel(m_AnimatedCoreModel->GetCoreModel());
			m_Tag = m_AnimatedCoreModel->m_Tag;

			// attach all meshes to the model
			for (int meshId = 0; meshId < m_AnimatedCoreModel->GetCoreModel()->getCoreMeshCount(); meshId++)
			{
				m_CalModel->attachMesh(meshId);
			}
			m_ActionAnimationId = -1;
			m_CycleAnimationId = -1;
			LoadMaterials();
			LoadVertexBuffer();
			
			m_CalModel->update(0.0f);

			CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
			//load components only if has physx for avoiding bugs with cinematic animated models
			if (m_hasPhysx){
				m_CalModel->setUserData(this);
				//process components
				m_ComponentNames.insert(m_ComponentNames.end(), m_AnimatedCoreModel->m_ComponentNames.begin(), m_AnimatedCoreModel->m_ComponentNames.end());// = m_AnimatedCoreModel->m_ComponentNames;
				for (auto param : m_AnimatedCoreModel->m_Params)
				{
					m_Params.insert(std::make_pair(param.first, param.second));
				}
				if (m_InstantiateInmediate) //create the physx character controller now?
				{
					lPhysxManager.AddCharacterController(m_Name, m_Height, m_Radius, m_Position, Quatf(0, 0, 0, 1), "default", 1, m_PhysxGroup);
				}
			}
			if (m_AnimatedCoreModel->m_Weapon != nullptr)
			{
				m_Weapon = new CWeapon(*m_AnimatedCoreModel->m_Weapon);
				if (!m_hasPhysx){
					m_Weapon->m_hasPhysx = m_hasPhysx;
				}
				m_Weapon->SetName(m_Name + "_" + m_Weapon->GetName());
				m_Weapon->SetParent(this);
				m_Weapon->m_LayerName = "animated_models";
				if (!CEngine::GetInstance().GetSceneManager().GetCurrentScene()->operator()("animated_models")->Add(m_Weapon->GetName(), m_Weapon))
				{
					base::utils::CheckedDelete(m_Weapon);
				}
				else
				{
					if (m_InstantiateInmediate)  //create the physx weapon mesh now?
					{
						if (m_Weapon->m_hasPhysx){
							lPhysxManager.CreateConvexMesh(
								m_Weapon->GetName(),
								"default",
								m_Weapon->GetPosition(),
								m_Weapon->GetRotation(),
								m_AnimatedCoreModel->m_PhysxMesh->numVertexes,
								m_AnimatedCoreModel->m_PhysxMesh->m_Vertexes,
								m_AnimatedCoreModel->m_PhysxMesh->numIndices,
								m_AnimatedCoreModel->m_PhysxMesh->m_Indices,
								false,
								true,
								true
								);
							m_Weapon->SetTriggerActive(false);
						}
						m_Weapon->LoadScriptsFromNode();
						//m_Weapon->StartComponents();
					}
					
				}
			}
			return true;
		}

		bool CSceneAnimatedModel::Render(engine::render::CRenderManager &RenderManager)
		{
			if (m_CalHardwareModel == nullptr)
			{
				return false;
			}
			buffers::CConstantBufferManager &l_CB = CEngine::GetInstance().GetConstantBufferManager();
			l_CB.mObjDesc.m_World = GetMatrix();

			l_CB.BindVSBuffer(RenderManager.GetDeviceContext(), buffers::CConstantBufferManager::CB_Object);
			ID3D11DeviceContext* l_DeviceContext = RenderManager.GetDeviceContext();

			for (int l_HardwareMeshId = 0; l_HardwareMeshId < m_CalHardwareModel->getHardwareMeshCount(); ++l_HardwareMeshId)
			{
				materials::CMaterial *l_Material = m_Materials[m_id[l_HardwareMeshId]];
				if (l_Material != nullptr && l_Material->GetTechnique() != nullptr && l_Material->GetTechnique()->GetEffect() != nullptr)
				{
					l_Material->Apply();
					m_CalHardwareModel->selectHardwareMesh(l_HardwareMeshId);
					Mat44f l_Transformations[MAXBONES];
					for (int l_BoneId = 0; l_BoneId < m_CalHardwareModel->getBoneCount();
						++l_BoneId)
					{
						Quatf l_Quaternion = reinterpret_cast<const Quatf &>(m_CalHardwareModel->getRotationBoneSpace(l_BoneId, m_CalModel->getSkeleton()));
						
						l_Transformations[l_BoneId].SetIdentity();
						l_Transformations[l_BoneId].SetRotByQuat(l_Quaternion);

						CalVector translationBoneSpace = m_CalHardwareModel->getTranslationBoneSpace(l_BoneId, m_CalModel->getSkeleton());

						l_Transformations[l_BoneId].SetPos(Vect3f(translationBoneSpace.x,
							translationBoneSpace.y, translationBoneSpace.z));
					}
					memcpy(&l_CB.mAnimatedModelDesc, l_Transformations, MAXBONES*sizeof(float) * 4 * 4);
					l_CB.BindVSBuffer(RenderManager.GetDeviceContext(), buffers::CConstantBufferManager::CB_AnimatedModel);
					m_Geometry->RenderIndexed(l_DeviceContext, m_CalHardwareModel->getFaceCount() * 3,
						m_CalHardwareModel->getStartIndex(), m_CalHardwareModel->getBaseVertexIndex());
				}
			}

			return true;
		}

		void CSceneAnimatedModel::Update(float ElapsedTime)
		{
			m_CalModel->update(ElapsedTime);
		}

		void CSceneAnimatedModel::Destroy() //TODO implementacion provicional
		{ }

		void CSceneAnimatedModel::ExecuteAction(int Id, float DelayIn, float DelayOut, float WeightTarget, bool AutoLock)
		{
			m_ActionAnimationId = Id;
			m_CalModel->getMixer()->executeAction(Id, DelayIn, DelayOut, WeightTarget, AutoLock);
		}

		bool CSceneAnimatedModel::RemoveAction(int Id)
		{
			m_ActionAnimationId = -1;
			return m_CalModel->getMixer()->removeAction(Id);
		}

		void CSceneAnimatedModel::BlendCycle(int Id, float Weight, float DelayIn)
		{
			m_CycleAnimationId = Id;
			m_CalModel->getMixer()->blendCycle(Id, Weight, DelayIn);
		}

		bool CSceneAnimatedModel::ClearCycle(int Id, float DelayOut)
		{
			m_CycleAnimationId = -1;
			return m_CalModel->getMixer()->clearCycle(Id, DelayOut);
		}

		bool CSceneAnimatedModel::IsCycleAnimationActive(int Id) const
		{
			return m_CycleAnimationId == Id;
		}

		bool CSceneAnimatedModel::IsActionAnimationActive(int Id) const
		{
			return m_ActionAnimationId == Id;
		}

		CalCoreAnimation* CSceneAnimatedModel::GetCurretAnimation(int coreAnimationId)
		{
			return this->GetAnimatedCoreModel()->GetCoreModel()->getCoreAnimation(coreAnimationId);
		}

		bool CSceneAnimatedModel::LoadVertexBuffer()
		{
			m_NumVertices = 0;
			m_NumFaces = 0;

			CalCoreModel *l_CalCoreModel = m_AnimatedCoreModel->GetCoreModel();

			m_CalHardwareModel = new CalHardwareModel(l_CalCoreModel);
			for (int i = 0; i < l_CalCoreModel->getCoreMeshCount(); ++i)
			{
				CalCoreMesh *l_CalCoreMesh = l_CalCoreModel->getCoreMesh(i);
				for (int j = 0; j < l_CalCoreMesh->getCoreSubmeshCount(); ++j)
				{
					CalCoreSubmesh *l_CalCoreSubmesh = l_CalCoreMesh->getCoreSubmesh(j);
					m_NumVertices += l_CalCoreSubmesh->getVertexCount();
					m_NumFaces += l_CalCoreSubmesh->getFaceCount();
					if (l_CalCoreSubmesh->getCoreMaterialThreadId() == -1)
					{
						m_id.push_back(0);
					} else
					{
						m_id.push_back(l_CalCoreSubmesh->getCoreMaterialThreadId());
					}
				}
			}
			if (m_AnimatedCoreModel->m_HasBump){
				shaders::PositionWeightIndicesBumpUV *l_Vertexs = static_cast<shaders::PositionWeightIndicesBumpUV*>(malloc(m_NumFaces * 3 * sizeof(shaders::PositionWeightIndicesBumpUV)));
				CalIndex *l_Faces = static_cast<CalIndex *>(malloc(m_NumFaces * 3 * sizeof(CalIndex)));

				m_CalHardwareModel->setVertexBuffer(reinterpret_cast<char*>(l_Vertexs), sizeof(shaders::PositionWeightIndicesBumpUV));
				m_CalHardwareModel->setWeightBuffer(reinterpret_cast<char*>(l_Vertexs)+12, sizeof(shaders::PositionWeightIndicesBumpUV));
				m_CalHardwareModel->setMatrixIndexBuffer(reinterpret_cast<char*>(l_Vertexs)+28, sizeof(shaders::PositionWeightIndicesBumpUV));
				m_CalHardwareModel->setNormalBuffer(reinterpret_cast<char*>(l_Vertexs)+44, sizeof(shaders::PositionWeightIndicesBumpUV));
				//m_CalHardwareModel->setTangentSpaceBuffer(0, reinterpret_cast<char*>(l_Vertexs)+56, sizeof(shaders::PositionWeightIndicesBumpUV));
				m_CalHardwareModel->setTextureCoordNum(1);
				m_CalHardwareModel->setTextureCoordBuffer(0, reinterpret_cast<char*>(l_Vertexs)+88, sizeof(shaders::PositionWeightIndicesBumpUV));
				m_CalHardwareModel->setIndexBuffer(l_Faces);

				m_CalHardwareModel->load(0, 0, MAXBONES);
				m_NumFaces = m_CalHardwareModel->getTotalFaceCount();
				m_NumVertices = m_CalHardwareModel->getTotalVertexCount();

				engine::shaders::CalcTangentsAndBinormals(l_Vertexs, l_Faces, m_NumVertices, m_NumFaces*3, sizeof(shaders::PositionWeightIndicesBumpUV),
					0, 44, 56, 72, 88);

				engine::render::CRenderManager &l_RenderManager = CEngine::GetInstance().GetRenderManager();

				m_Geometry = new buffers::CTemplatedIndexedGeometry<shaders::PositionWeightIndicesBumpUV>(
					new buffers::CVertexBuffer<shaders::PositionWeightIndicesBumpUV>(l_RenderManager, l_Vertexs, m_NumVertices),
					new buffers::CIndexBuffer(l_RenderManager, l_Faces, m_NumFaces * 3, sizeof(CalIndex) * 8), D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				free(l_Vertexs);
				free(l_Faces);
			}
			else{
				shaders::PositionWeightIndicesNormalUV *l_Vertexs = static_cast<shaders::PositionWeightIndicesNormalUV*>(malloc(m_NumFaces * 3 * sizeof(shaders::PositionWeightIndicesNormalUV)));
				CalIndex *l_Faces = static_cast<CalIndex *>(malloc(m_NumFaces * 3 * sizeof(CalIndex)));

				m_CalHardwareModel->setVertexBuffer(reinterpret_cast<char*>(l_Vertexs), sizeof(shaders::PositionWeightIndicesNormalUV));
				m_CalHardwareModel->setWeightBuffer(reinterpret_cast<char*>(l_Vertexs)+12, sizeof(shaders::PositionWeightIndicesNormalUV));
				m_CalHardwareModel->setMatrixIndexBuffer(reinterpret_cast<char*>(l_Vertexs)+28, sizeof(shaders::PositionWeightIndicesNormalUV));
				m_CalHardwareModel->setNormalBuffer(reinterpret_cast<char*>(l_Vertexs)+44, sizeof(shaders::PositionWeightIndicesNormalUV));
				m_CalHardwareModel->setTextureCoordNum(1);
				m_CalHardwareModel->setTextureCoordBuffer(0, reinterpret_cast<char*>(l_Vertexs)+56, sizeof(shaders::PositionWeightIndicesNormalUV));
				m_CalHardwareModel->setIndexBuffer(l_Faces);

				m_CalHardwareModel->load(0, 0, MAXBONES);
				m_NumFaces = m_CalHardwareModel->getTotalFaceCount();
				m_NumVertices = m_CalHardwareModel->getTotalVertexCount();

				engine::render::CRenderManager &l_RenderManager = CEngine::GetInstance().GetRenderManager();

				m_Geometry = new buffers::CTemplatedIndexedGeometry<shaders::PositionWeightIndicesNormalUV>(
					new buffers::CVertexBuffer<shaders::PositionWeightIndicesNormalUV>(l_RenderManager, l_Vertexs, m_NumVertices),
					new buffers::CIndexBuffer(l_RenderManager, l_Faces, m_NumFaces * 3, sizeof(CalIndex) * 8), D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				free(l_Vertexs);
				free(l_Faces);
			}
			return true;
		}

		void CSceneAnimatedModel::LoadMaterials()
		{
			m_Materials = m_AnimatedCoreModel->GetMaterials();
		}

		std::string CSceneAnimatedModel::getModelName()
		{
			return m_AnimatedCoreModel->GetName();
		}

		int CSceneAnimatedModel::GetBoneId(std::string BoneName)
		{
			return m_CalModel->getCoreModel()->getBoneId(BoneName);
		}

		CalBone * CSceneAnimatedModel::getBone(std::string boneName)
		{
			return m_CalModel->getSkeleton()->getBone(GetBoneId(boneName));
		}

		const CalQuaternion &CSceneAnimatedModel::getRotationAbsolute(int boneId)
		{
			const std::vector<CalBone *>& vectorBone = m_CalModel->getSkeleton()->getVectorBone();
			return vectorBone[boneId]->getRotationAbsolute();
		}

		const CalVector &CSceneAnimatedModel::getTranslationAbsolute(int boneId)
		{
			const std::vector<CalBone *>& vectorBone = m_CalModel->getSkeleton()->getVectorBone();
			return vectorBone[boneId]->getTranslationAbsolute();
		}

		void CSceneAnimatedModel::HideAnimated()
		{
			m_Visible = false;
			if (m_Weapon != nullptr)
			{
				m_Weapon->SetVisible(false);
			}
		}

		void CSceneAnimatedModel::ShowAnimated()
		{
			m_Visible = true;
			if (m_Weapon != nullptr)
			{
				m_Weapon->SetVisible(true);
			}
		}
	}
}
