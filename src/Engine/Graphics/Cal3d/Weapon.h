#ifndef _WEAPON_DPVD1_18072017090300_H
#define _WEAPON_DPVD1_18072017090300_H

#include "Graphics/Scenes/SceneMesh.h"
#include "PhysXImpl/PhysXManager.h"

namespace engine
{
	namespace cal3dimpl
	{
		class CSceneAnimatedModel;
		class CWeapon : public engine::scenes::CSceneMesh
		{
		private: 
			CSceneAnimatedModel* m_Parent;
			std::string boneName = "";
		public:
			CWeapon(const CXMLElement* aElement);
			CWeapon(const CXMLElement* aElement, engine::materials::CMesh* aMesh);			
			void Update(float aDeltaTime) override;
			bool Render(render::CRenderManager& aRendermanager) override;
			virtual ~CWeapon();		
			void SetParent(CSceneAnimatedModel* parent);
			
		};
	}
}
#endif
