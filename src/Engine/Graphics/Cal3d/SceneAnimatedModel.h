#ifndef _SCENEANIMATEDMODEL_DPVD1_1701207044200_H
#define _SCENEANIMATEDMODEL_DPVD1_1701207044200_H

#include "Cal3D/src/cal3d/model.h"
#include "Cal3D/src/cal3d/hardwaremodel.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Cal3D/src/cal3d/bone.h"
#include "Weapon.h"
#include "Cal3D/src/cal3d/coreanimation.h"

namespace tinyxml2 {
	class XMLElement;
}

namespace engine
{
	namespace materials
	{
		class CMaterial;
		class CGeometry;
	}
	namespace scenes
	{
		class CSceneNode;
	}
	namespace cal3dimpl
	{
		class CAnimatedCoreModel;
		class CSceneAnimatedModel : public scenes::CSceneNode
		{
		
		public:
			CSceneAnimatedModel(const CXMLElement* aElement);
			void AddPhysx(const tinyxml2::XMLElement* aElement);
			virtual ~CSceneAnimatedModel();
			void InstantiatePhysx() const;
			bool Initialize(CAnimatedCoreModel *animatedCoreModel);
			bool Render(render::CRenderManager &RenderManager) override;
			void Update(float ElapsedTime) override;
			void Destroy();//TODO IMPLEMETAR EN ALGUNA FUCKIN' PARTE
			void ExecuteAction(int Id, float DelayIn, float DelayOut, float WeightTarget = 1.0f, bool AutoLock = false);
			bool RemoveAction(int Id);
			void BlendCycle(int Id, float Weight, float DelayIn);
			bool ClearCycle(int Id, float DelayOut);
			bool IsCycleAnimationActive(int Id) const;
			bool IsActionAnimationActive(int Id) const;
			CalCoreAnimation* GetCurretAnimation(int coreAnimationId);
			std::string getModelName();
			bool m_InstantiateInmediate;
			void HideAnimated();
			void ShowAnimated();

			int GetBoneId(std::string BoneName);
			const CalQuaternion &CSceneAnimatedModel::getRotationAbsolute(int boneId);
			const CalVector & CSceneAnimatedModel::getTranslationAbsolute(int boneId);
			CalBone * CSceneAnimatedModel::getBone(std::string boneName);
			GET_SET_PTR(CWeapon, Weapon);
			GET_SET(CAnimatedCoreModel *, AnimatedCoreModel)
			GET_SET_PTR(CalModel, CalModel);

		private:
			CalModel *m_CalModel;
			CAnimatedCoreModel *m_AnimatedCoreModel;
			CalHardwareModel *m_CalHardwareModel;
			std::vector<materials::CMaterial *> m_Materials;
			std::vector<int> m_id;
			materials::CGeometry *m_Geometry;
			CWeapon* m_Weapon;

			int m_NumVertices;
			int m_NumFaces;
			float m_Height, m_Radius;
			CPhysXManager::PhysxGroups m_PhysxGroup;
			bool LoadVertexBuffer();
			void LoadMaterials();
			int m_ActionAnimationId;
			int m_CycleAnimationId;
			std::string m_PhysxMaterialName;
		};
	}
}
#endif