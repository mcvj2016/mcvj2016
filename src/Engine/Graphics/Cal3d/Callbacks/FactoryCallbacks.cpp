#include "FactoryCallbacks.h"

namespace engine
{
	namespace cal3dimpl
	{	
		CCallbacksFactory::CCallbacksFactory()
		{
			
		}

		CCallbacksFactory::~CCallbacksFactory()
		{
			
		}

		CalAnimationCallback* CCallbacksFactory::GetCallback(const std::string& CallbackName)
		{
			if (CallbackName == "model_take_damage")
			{
				return &m_ModelTakeDamage;
			}
			if (CallbackName=="orkelf_attack1")
			{
				return &m_OrkelfAttack1;
			}
			if (CallbackName == "orkelf2_attack1")
			{
				return &m_Orkelf2Attack1;
			}
			if (CallbackName == "orkelf_attack2")
			{
				return &m_OrkelfAttack2;
			}
			if (CallbackName == "orkelf2_attack2")
			{
				return &m_Orkelf2Attack2;
			}
			if (CallbackName == "orkelf_attack3")
			{
				return &m_OrkelfAttack3;
			}
			if (CallbackName == "orkelf2_attack3")
			{
				return &m_Orkelf2Attack3;
			}
			if (CallbackName == "orkelf_magic1")
			{
				return &m_OrkelfMagic1;
			}
			if (CallbackName == "orkelf2_magic1")
			{
				return &m_Orkelf2Magic1;
			}
			if (CallbackName == "gulak_magic1")
			{
				return &m_GulakMagic1;
			}
			if (CallbackName == "orkelf_evade")
			{
				return &m_OrkelfEvade;
			}
			if (CallbackName == "orkelf2_evade")
			{
				return &m_Orkelf2Evade;
			}
			
			return nullptr;
		}		
	}
}
