#ifndef _CALLBACKS_DPVD1_18072017090300_H
#define _CALLBACKS_DPVD1_18072017090300_H

#include "Cal3D/src/cal3d/animcallback.h"
#include "Utils/Singleton.h"
#include "Graphics/Cal3d/Weapon.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Components/Player/PlayerMagicAttackController.h"
#include "Components/Weapon/PlayerMagicProjectile.h"
#include "Components/IA/Seeker.h"
#include "Components/Common/Life.h"
#include "Utils/Coroutine.h"
#include "Components/Enemies/Gulak/GulakAttackController.h"
#include "Components/Weapon/GulakMagicProjectile.h"

using namespace logic::components;

namespace engine
{
	namespace cal3dimpl
	{
		struct COrkelfAttack1 : CalAnimationCallback
		{
			virtual ~COrkelfAttack1(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}

				if (!m_Weapon->IsTriggerActive() && anim_time>=0.39784f)
				{
					m_Weapon->SetTriggerActive(true);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override
			{
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelf2Attack1 : CalAnimationCallback
		{
			virtual ~COrkelf2Attack1(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}

				if (!m_Weapon->IsTriggerActive() && anim_time >= 0.39784f)
				{
					m_Weapon->SetTriggerActive(true);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override
			{
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelfAttack2 : CalAnimationCallback
		{
			virtual ~COrkelfAttack2(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}
				if (!m_Weapon->IsTriggerActive() && anim_time >= 0.03)
				{
					m_Weapon->SetTriggerActive(true);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override
			{
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelf2Attack2 : CalAnimationCallback
		{
			virtual ~COrkelf2Attack2(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}
				if (!m_Weapon->IsTriggerActive() && anim_time >= 0.03)
				{
					m_Weapon->SetTriggerActive(true);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override
			{
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelfAttack3 : CalAnimationCallback
		{
			virtual ~COrkelfAttack3(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}
				//t1 = 0.5
				if (!m_Weapon->IsTriggerActive() && anim_time >= 0.5f && anim_time < 0.73f)
				{
					m_Weapon->SetTriggerActive(true);
				}
				//t2 = 0.73
				else if (m_Weapon->IsTriggerActive() && anim_time >= 0.9f)
				{
					m_Weapon->SetTriggerActive(false);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelf2Attack3 : CalAnimationCallback
		{
			virtual ~COrkelf2Attack3(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
				}
				//t1 = 0.5
				if (!m_Weapon->IsTriggerActive() && anim_time >= 0.5f && anim_time < 0.73f)
				{
					m_Weapon->SetTriggerActive(true);
				}
				//t2 = 0.73
				else if (m_Weapon->IsTriggerActive() && anim_time >= 0.9f)
				{
					m_Weapon->SetTriggerActive(false);
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				if (m_Weapon->IsTriggerActive())
				{
					m_Weapon->SetTriggerActive(false);
				}
			}
		};
		struct COrkelfMagic1 : CalAnimationCallback
		{
			COrkelfMagic1(): m_AnimatedModel(nullptr), m_Projectile(nullptr), m_PrComp(nullptr)
			{
			}

			bool canSpawn = true;
			virtual ~COrkelfMagic1(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CSceneNode* m_Projectile;
			CPlayerMagicProjectile* m_PrComp;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Projectile = m_AnimatedModel->GetComponent<CPlayerMagicAttackController>()->m_MagicProjectile;
					m_PrComp = m_Projectile->GetComponent<CPlayerMagicProjectile>();
				}
				//seg = 1.041f
				if (!m_Projectile->IsTriggerActive() && anim_time >= 1.041f && canSpawn)
				{
					m_Projectile->SetTriggerActive(true);
					m_Projectile->SetEnabled(true);
					m_PrComp->Spawn();
					canSpawn = false;
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				canSpawn = true;
			}
		};
		struct COrkelf2Magic1 : CalAnimationCallback
		{
			COrkelf2Magic1() : m_AnimatedModel(nullptr), m_Projectile(nullptr), m_PrComp(nullptr)
			{
			}

			bool canSpawn = true;
			virtual ~COrkelf2Magic1(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CSceneNode* m_Projectile;
			CPlayerMagicProjectile* m_PrComp;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Projectile = m_AnimatedModel->GetComponent<CPlayerMagicAttackController>()->m_MagicProjectile;
					m_PrComp = m_Projectile->GetComponent<CPlayerMagicProjectile>();
				}
				//seg = 1.041f
				if (!m_Projectile->IsTriggerActive() && anim_time >= 1.041f && canSpawn)
				{
					m_Projectile->SetTriggerActive(true);
					m_Projectile->SetEnabled(true);
					m_PrComp->Spawn();
					canSpawn = false;
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				canSpawn = true;
			}
		};
		struct COrkelfEvade : CalAnimationCallback
		{
			COrkelfEvade(): m_AnimatedModel(nullptr), m_Life(nullptr)
			{
			}

			virtual ~COrkelfEvade(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CLife* m_Life;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Life = m_AnimatedModel->GetComponent<logic::components::CLife>();
				}
				m_Life->SetInvencible(true);
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				m_Life->SetInvencible(false);
			}
		};
		struct COrkelf2Evade : CalAnimationCallback
		{
			COrkelf2Evade() : m_AnimatedModel(nullptr), m_Life(nullptr)
			{
			}

			virtual ~COrkelf2Evade(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CLife* m_Life;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Life = m_AnimatedModel->GetComponent<logic::components::CLife>();
				}
				m_Life->SetInvencible(true);
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				m_Life->SetInvencible(false);
			}
		};
		struct CModelTakeDamage : CalAnimationCallback
		{
			CModelTakeDamage() : m_AnimatedModel(nullptr), m_Weapon(nullptr), m_Life(nullptr)
			{
			}

			virtual ~CModelTakeDamage(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CWeapon* m_Weapon;
			CLife* m_Life;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Weapon = m_AnimatedModel->GetWeapon();
					m_Life = m_AnimatedModel->GetComponent<CLife>();
				}

				m_Weapon->SetTriggerActive(false);
				m_Life->SetInvencible(true);
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				m_Life->SetInvencible(false);
			}
		};
		struct CGulakfMagic1 : CalAnimationCallback
		{
			CGulakfMagic1() : m_AnimatedModel(nullptr), m_Projectile(nullptr), m_PrComp(nullptr)
			{
			}

			bool canSpawn = true;
			virtual ~CGulakfMagic1(){}
			CSceneAnimatedModel* m_AnimatedModel;
			CSceneNode* m_Projectile;
			CGulakMagicProjectile* m_PrComp;

			void AnimationUpdate(float anim_time, CalModel *model, void * userData) override
			{
				//used as initializer 
				if (m_AnimatedModel == nullptr)
				{
					m_AnimatedModel = reinterpret_cast<CSceneAnimatedModel*>(userData);
					m_Projectile = m_AnimatedModel->GetComponent<CGulakAttackController>()->m_MagicProjectile;
					m_PrComp = m_Projectile->GetComponent<CGulakMagicProjectile>();
				}
				//seg = 2.083
				if (!m_Projectile->IsTriggerActive() && anim_time >= 2.083f && canSpawn)
				{
					m_Projectile->SetTriggerActive(true);
					m_Projectile->SetEnabled(true);
					m_PrComp->Launch();
					canSpawn = false;
				}
			}

			void AnimationComplete(CalModel *model, void * userData) override {
				canSpawn = true;
			}
		};

		class CCallbacksFactory : public base::utils::CSingleton<CCallbacksFactory>
		{
		private:
			COrkelfAttack1 m_OrkelfAttack1;
			COrkelfAttack2 m_OrkelfAttack2;
			COrkelfAttack3 m_OrkelfAttack3;
			COrkelfMagic1 m_OrkelfMagic1;
			CGulakfMagic1 m_GulakMagic1;
			COrkelfEvade m_OrkelfEvade;
			COrkelf2Attack1 m_Orkelf2Attack1;
			COrkelf2Attack2 m_Orkelf2Attack2;
			COrkelf2Attack3 m_Orkelf2Attack3;
			COrkelf2Magic1 m_Orkelf2Magic1;
			COrkelf2Evade m_Orkelf2Evade;
			CModelTakeDamage m_ModelTakeDamage;
		public:
			CCallbacksFactory();
			virtual ~CCallbacksFactory();
			CalAnimationCallback * GetCallback(const std::string& CallbackName);
		};
	}
}
#endif
