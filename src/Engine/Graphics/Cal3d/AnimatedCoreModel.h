#ifndef _ANIMATEDCOREMODEL_DPVD1_1701207044200_H
#define _ANIMATEDCOREMODEL_DPVD1_1701207044200_H
#include "Math/Vector3.h"
#include "Cal3D/src/cal3d/coremodel.h"
#include "Utils/Name.h"
#include "Weapon.h"
#include "Graphics/Meshes/PhysxMesh.h"

namespace engine
{
	namespace scenes
	{
		class CSceneNode;
	}
	namespace materials
	{
		class CMaterial;
	}
	namespace cal3dimpl
	{
		class CAnimatedCoreModel : public CName
		{
		public:
			CAnimatedCoreModel();
			CAnimatedCoreModel(const std::string& aName);
			CAnimatedCoreModel(const CXMLElement* aElement);
			virtual ~CAnimatedCoreModel();
			CalCoreModel *GetCoreModel();
			const std::vector<materials::CMaterial *> & GetMaterials() const;
			void AddMaterial(materials::CMaterial* aMaterial){ m_Materials.push_back(aMaterial); }
			//void Load(const std::string &Path);
			void Load();
			CWeapon* m_Weapon;
			materials::CPhysxMesh * m_PhysxMesh;
			bool m_HasBump;
			scenes::CSceneNode::TAG m_Tag;
			
			std::vector<std::string> m_ComponentNames;
			std::map<std::string, std::vector<std::string>> m_Params;
		private:
			CalCoreModel *m_CalCoreModel;
			std::vector<materials::CMaterial *> m_Materials;
			std::string m_Path;
			Vect3f m_BSPosition;
			float m_BSRadius;
			bool m_InvertUV;
			
			bool LoadMesh(const std::string &Filename);
			bool LoadSkeleton(const std::string &Filename);
			bool LoadAnimation(const std::string &Name, const std::string &Filename);
			bool LoadCallback(const std::string &AnimationName, const std::string &CallbackName);
		};
	}
}
#endif
