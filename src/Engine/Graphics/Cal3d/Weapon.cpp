#include "Weapon.h"
#include "XML/XML.h"
#include "Cal3D/src/cal3d/bone.h"
#include "Math/Quaternion.h"
#include "SceneAnimatedModel.h"
#include "Camera/CameraManager.h"
#include "Graphics/Meshes/Mesh.h"
#include "Engine.h"
#include "PhysXImpl/PhysXManager.h"
#include "Render/RenderManager.h"
#include "Graphics/Buffers/ConstantBufferManager.h"

namespace engine
{
	namespace cal3dimpl
	{
		CWeapon::CWeapon(const CXMLElement* aElement)
			: engine::scenes::CSceneMesh(aElement), m_Parent(nullptr)
		{
			boneName = aElement->GetAttribute<std::string>("bone", "");
		}

		CWeapon::CWeapon(const CXMLElement* aElement, materials::CMesh* aMesh)
			: engine::scenes::CSceneMesh(aElement), m_Parent(nullptr)
		{
		}

		CWeapon::~CWeapon()
		{
		}

		void CWeapon::Update(float aDeltaTime)
		{
			CalBone * bone = m_Parent->getBone(boneName);

			Mat44f TranslationMatrix;
			TranslationMatrix.SetIdentity();
			CalVector translation = bone->getTranslationAbsolute();
			Vect4f fpos = Vect4f(translation.x, translation.y, translation.z, 1.0f);
			TranslationMatrix.SetPos(fpos.x, fpos.y, fpos.z);

			Mat44f RotationMatrix;
			CalQuaternion calquat = bone->getRotationAbsolute();
			Quatf l_Quaternion = Quatf(calquat.x, calquat.y, calquat.z, calquat.w);
			RotationMatrix.SetIdentity();
			RotationMatrix.SetRotByQuat(l_Quaternion);

			Mat44f ScaleMatrix;
			ScaleMatrix.SetIdentity();
			ScaleMatrix.Scale(m_Scale.x, m_Scale.y, m_Scale.z);

			Mat44f AbsoluteMatrix;
			AbsoluteMatrix.SetIdentity();
			AbsoluteMatrix = ScaleMatrix*RotationMatrix*TranslationMatrix;
			m_TransformMatrix = AbsoluteMatrix*m_Parent->GetMatrix();

			Mat44f physxMatrix = RotationMatrix*TranslationMatrix*m_Parent->GetMatrix();
			if (m_hasPhysx){
				CPhysXManager& lPhysxManager = CEngine::GetInstance().GetPhysXManager();
				lPhysxManager.setKinematicTarget(m_Name, physxMatrix);
			}
		}

		bool CWeapon::Render(render::CRenderManager& aRendermanager)
		{
			bool lOk = false;
			if (m_Mesh)
			{
				//check dentro del frustrum
				CFrustum* lFrustum = CEngine::GetInstance().GetCameraManager().GetMainCamera()->GetFrustum();
				if (m_FrustumIgnored || lFrustum->IsVisible(m_Mesh->GetBoundingSphere(), m_Position, m_Scale))
				{
					buffers::CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
					lCB.mObjDesc.m_World = m_TransformMatrix;
					lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), buffers::CConstantBufferManager::CB_Object);
					lOk = m_Mesh->Render(aRendermanager);
				}
			}
			return lOk;
		}


		void CWeapon::SetParent(CSceneAnimatedModel* parent)
		{
			m_Parent = parent;
		}
	}
}
