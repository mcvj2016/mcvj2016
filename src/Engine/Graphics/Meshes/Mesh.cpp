#include "Mesh.h"
#include "Engine.h"
#include "Graphics/Shaders/VertexType.h"
#include "Graphics/Buffers/TemplatedGeometry.h"
#include "Graphics/Buffers/TemplatedIndexedGeometry.h"
#include "Utils/BinFileReader.h"
#include "Graphics/Buffers/VertexBuffer.h"
#include "Graphics/Buffers/IndexBuffer.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Render/RenderManager.h"
#include "Graphics/Materials/Material.h"
#include "Geometry.h"
#include "XML/XML.h"

using namespace engine::shaders;
using namespace engine::buffers;

namespace engine
{
	namespace materials
	{
		CMesh::CMesh()
			: mCount(0),
			m_Materials(std::vector<CMaterial*>()),
			m_Geometries(std::vector<CGeometry*>()),
			m_AABB(CAxisAlignedBoundingBox()),
			m_BoundingSphere(CBoundingSphere())
		{
		}

		CMesh::CMesh(tinyxml2::XMLElement* aElement) : CMesh()
		{
			m_Name = aElement->GetAttribute<std::string>("mesh", "");
			assert(m_Name != "");
		}

		CMesh::~CMesh()
		{
			for (std::size_t i = 0; i < m_Geometries.size(); i++)
			{
				delete m_Geometries[i];
			}
			m_Geometries.clear();
		}

		bool CMesh::Load(const std::string& aFilename)
		{
			bool loaded = false;
			CMaterialManager& MaterialManager = CEngine::GetInstance().GetMaterialManager();
			render::CRenderManager& RenderManager = CEngine::GetInstance().GetRenderManager();
			base::utils::CBinFileReader MeshBin(aFilename);

			if (MeshBin.Open())
			{
				if (MeshBin.Read<unsigned short>() == HEADER){
					int numMateriales = MeshBin.Read<unsigned short>();

					if (numMateriales > 1)
					{
						for (int cont = 0; cont < numMateriales; cont++)
						{
							//Lee el nombre del material
							std::string materialName = MeshBin.Read<std::string>();
							//lo agrego al vector;
							m_Materials.push_back(MaterialManager.operator()(materialName));
						}
							
					}
					else
					{
						//Lee el nombre del material
						std::string materialName = MeshBin.Read<std::string>();
						//lo agrego al vector;
						m_Materials.push_back(MaterialManager.operator()(materialName));
					}

					for (int cont = 0; cont < numMateriales; cont++)
					{
						//Lee el numero de vertices para crear la geometria
						unsigned short lVertexFlags = MeshBin.Read<unsigned short>();
						unsigned short lNumVertices = MeshBin.Read<unsigned short>();
						//Lee los vertices segun el flag
						uint32 lVertexSize = GetVertexSize(lVertexFlags);
						size_t lNumBytesVertices = lNumVertices * lVertexSize;
						void* lVertexData = MeshBin.ReadRaw(lNumBytesVertices);

						//Lee el nimero de indices para crear la geometr�a
						unsigned short lNumIndexes = MeshBin.Read<unsigned short>();
						//Lee los vertices segun el tipo de dato. Coloco unsigned short de momento.
						size_t lNumBytesIndices = lNumIndexes * sizeof(unsigned short);
						void* lIndexData = MeshBin.ReadRaw(lNumBytesIndices);

						/*if (lNumIndexes == 0)
						{
							//Creamos el Geometry con un Vertex Buffer con tipo
							if (lVertexFlags == PositionNormal::GetVertexFlags()){
								CVertexBuffer<PositionNormal>* vertexBuffer = new CVertexBuffer<PositionNormal>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionNormal>* geoTriangleList = new CGeometryTriangleList<PositionNormal>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
							else if (lVertexFlags == PositionNormalUV::GetVertexFlags()){
								CVertexBuffer<PositionNormalUV>* vertexBuffer = new CVertexBuffer<PositionNormalUV>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionNormalUV>* geoTriangleList = new CGeometryTriangleList<PositionNormalUV>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
							else if (lVertexFlags == PositionBump::GetVertexFlags()){
								CVertexBuffer<PositionBump>* vertexBuffer = new CVertexBuffer<PositionBump>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionBump>* geoTriangleList = new CGeometryTriangleList<PositionBump>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
							else if (lVertexFlags == PositionBumpUV::GetVertexFlags()){
								CalcTangentsAndBinormals(lVertexData, nullptr, lNumVertices, 0, sizeof(PositionBumpUV),
									sizeof(float) * 3, sizeof(float) * 3, sizeof(float) * 4, sizeof(float) * 4, sizeof(float) * 2);
								CVertexBuffer<PositionBumpUV>* vertexBuffer = new CVertexBuffer<PositionBumpUV>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionBumpUV>* geoTriangleList = new CGeometryTriangleList<PositionBumpUV>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
							else if (lVertexFlags == PositionBumpUVUV2::GetVertexFlags()){
								CVertexBuffer<PositionBumpUVUV2>* vertexBuffer = new CVertexBuffer<PositionBumpUVUV2>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionBumpUVUV2>* geoTriangleList = new CGeometryTriangleList<PositionBumpUVUV2>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
							else if (lVertexFlags == PositionNormalUVUV2::GetVertexFlags()){
								CVertexBuffer<PositionNormalUVUV2>* vertexBuffer = new CVertexBuffer<PositionNormalUVUV2>(RenderManager, lVertexData, lNumVertices);
								CGeometryTriangleList<PositionNormalUVUV2>* geoTriangleList = new CGeometryTriangleList<PositionNormalUVUV2>(vertexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
						}
						else
						{*/
						if (lNumIndexes > 0){
							//Creamos el IndexedGeometry con un Vertex Buffer con tipo y sus �ndices
							if (lVertexFlags == PositionNormal::GetVertexFlags()){
								CVertexBuffer<PositionNormal>* vertexBuffer = new CVertexBuffer<PositionNormal>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionNormal>* geoIndexedTriangleList = new CIndexedGeometryTriangleList<PositionNormal>(vertexBuffer, indexBuffer);
								m_Geometries.push_back(geoIndexedTriangleList);
							}
							else if (lVertexFlags == PositionNormalUV::GetVertexFlags()){
								CVertexBuffer<PositionNormalUV>* vertexBuffer = new CVertexBuffer<PositionNormalUV>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionNormalUV>* geoIndexedTriangleList = new CIndexedGeometryTriangleList<PositionNormalUV>(vertexBuffer, indexBuffer);
								//std::unique_ptr<PositionNormalUV> geoIndexedTriangleList(new PositionNormalUV());
								m_Geometries.push_back(geoIndexedTriangleList);
							}
							else if (lVertexFlags == PositionBump::GetVertexFlags()){
								CVertexBuffer<PositionBump>* vertexBuffer = new CVertexBuffer<PositionBump>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionBump>* geoIndexedTriangleList = new CIndexedGeometryTriangleList<PositionBump>(vertexBuffer, indexBuffer);
								m_Geometries.push_back(geoIndexedTriangleList);
							}
							else if (lVertexFlags == PositionBumpUV::GetVertexFlags()){
								CalcTangentsAndBinormals(lVertexData,(unsigned short*)lIndexData, lNumVertices, lNumIndexes, sizeof(PositionBumpUV),
									0, 12, 24, 40, 56);								
								CVertexBuffer<PositionBumpUV>* vertexBuffer = new CVertexBuffer<PositionBumpUV>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionBumpUV>* geoIndexedTriangleList = new CIndexedGeometryTriangleList<PositionBumpUV>(vertexBuffer, indexBuffer);
								m_Geometries.push_back(geoIndexedTriangleList);

							}
							else if (lVertexFlags == PositionBumpUVUV2::GetVertexFlags()){
								CVertexBuffer<PositionBumpUVUV2>* vertexBuffer = new CVertexBuffer<PositionBumpUVUV2>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionBumpUVUV2>* geoIndexedTriangleList = new CIndexedGeometryTriangleList<PositionBumpUVUV2>(vertexBuffer, indexBuffer);
								m_Geometries.push_back(geoIndexedTriangleList);
							}
							else if (lVertexFlags == PositionNormalUVUV2::GetVertexFlags()){
								CVertexBuffer<PositionNormalUVUV2>* vertexBuffer = new CVertexBuffer<PositionNormalUVUV2>(RenderManager, lVertexData, lNumVertices);
								CIndexBuffer* indexBuffer = new CIndexBuffer(RenderManager, lIndexData, lNumIndexes, 16);
								CIndexedGeometryTriangleList<PositionNormalUVUV2>* geoTriangleList = new CIndexedGeometryTriangleList<PositionNormalUVUV2>(vertexBuffer, indexBuffer);
								m_Geometries.push_back(geoTriangleList);
							}
						}
						delete lIndexData;
						delete lVertexData;
					}
					float minX = MeshBin.Read<float>();
					float minY = MeshBin.Read<float>();
					float minZ = MeshBin.Read<float>();
					float maxX = MeshBin.Read<float>();
					float maxY = MeshBin.Read<float>();
					float maxZ = MeshBin.Read<float>();
					m_AABB.SetMin(Vect3f(minX, minY, minZ));
					m_AABB.SetMax(Vect3f(maxX, maxY, maxZ));
					float centerX = MeshBin.Read<float>();
					float centerY = MeshBin.Read<float>();
					float centerZ = MeshBin.Read<float>();
					float radius = MeshBin.Read<float>();
					m_BoundingSphere.SetRadius(radius);
					m_BoundingSphere.SetCenter(Vect3f(centerX, centerY, centerZ));

					int val = MeshBin.Read<unsigned short>();
					if (val == FOOTER)
					{
						loaded = true;
					}
					else
					{
						//TODO: ignorar error al leer malla de physhx
						//assert(!strcat("El footer del binario ha fallado: ", aFilename.c_str()));
					}
					MeshBin.Close();
				}
				else
				{
					assert(!std::string("Error de lectura de la malla: " + aFilename).c_str());
				}
			}
			return loaded;
		}

		bool CMesh::Render(render::CRenderManager& aRenderManager)
		{
			bool lOk = true;
			ID3D11DeviceContext* lContext = aRenderManager.GetDeviceContext();

			for (size_t i = 0, lCount = m_Geometries.size(); i < lCount; ++i)
			{
				m_Materials[i]->Apply();
				m_Geometries[i]->RenderIndexed(lContext);
			}
			return lOk;
		}

		CBoundingSphere CMesh::GetBoundingSphere() const
		{
			return m_BoundingSphere;
		}

		CAxisAlignedBoundingBox CMesh::GetAABB() const
		{
			return m_AABB;
		}

		materials::CPhysxMesh* CMesh::GetPhysxMesh(std::string file)
		{
			materials::CPhysxMesh* pmesh = new materials::CPhysxMesh;

			base::utils::CBinFileReader MeshBin(file);
			if (MeshBin.Open())
			{
				if (MeshBin.Read<unsigned short>() == HEADER)
				{
					//Lee el nmero de vertices para crear la geometra
					pmesh->numVertexes = MeshBin.Read<unsigned short>();
					//Lee los vertices segun el flag
					size_t lNumBytesVertices = pmesh->numVertexes * (sizeof(float) * 3);
					pmesh->m_Vertexes = MeshBin.ReadRaw(lNumBytesVertices);

					//Lee el nmero de indices para crear la geometra
					pmesh->numIndices = MeshBin.Read<unsigned short>();
					//Lee los vertices segun el tipo de dato. Coloco unsigned short de momento.
					size_t lNumBytesIndices = pmesh->numIndices * sizeof(unsigned short);
					pmesh->m_Indices = MeshBin.ReadRaw(lNumBytesIndices);

					int val = MeshBin.Read<unsigned short>();
					if (val != FOOTER)
					{
						//TODO ERROR DE LECTURA DE MALLA EN ARCHIVO AFileName. Detener el programa
					}
					MeshBin.Close();
				}
				else
				{
					base::utils::CheckedDelete(pmesh);
					//TODO ERROR DE LECTURA DE MALLA EN ARCHIVO AFileName. Detener el programa
				}
			}
			
			return pmesh;
		}
	}
}
