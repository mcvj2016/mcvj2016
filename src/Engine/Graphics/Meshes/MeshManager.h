#ifndef _ENGINE_MESHMANAGER_DPVD1_21122016202800_H
#define _ENGINE_MESHMANAGER_DPVD1_21122016202800_H

#include "Mesh.h"
#include "Utils/TemplatedMapVector.h"

namespace engine
{
	namespace materials
	{
		class CMeshManager : public base::utils::CTemplatedMapVector<CMesh>
		{
		public:
			CMeshManager();
			virtual ~CMeshManager();
			bool Load(std::string aName = "");//Lo he adicionado por que me parece comodo, sino no s� como co�os habia que hacerlo.
			CMesh* GetMesh(const std::string& aFilename);
			bool Reload();
		private:
			std::string m_path;
		};
	}
}
#endif
