#include "PhysxMesh.h"
#include "Utils/CheckedDelete.h"

namespace engine
{
	namespace materials
	{
		CPhysxMesh::CPhysxMesh(): numVertexes(0), numIndices(0), m_Vertexes(nullptr), m_Indices(nullptr)
		{
		}

		CPhysxMesh::~CPhysxMesh()
		{
			base::utils::CheckedDelete(m_Indices);
			base::utils::CheckedDelete(m_Vertexes);
		}
	}
}
