#include "MeshManager.h"
#include <d3d11.h>
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/StringUtils.h"

namespace engine
{
	namespace materials
	{

		CMeshManager::CMeshManager() :m_path("data/common/meshes/")
		{
		}

		CMeshManager::~CMeshManager()
		{
		}

		bool CMeshManager::Load(std::string aPath)
		{
			std::string realPath;
			WIN32_FIND_DATA FindFileData;
			HANDLE hFind;
			if (aPath == "")
			{
				hFind = FindFirstFile((m_path + "*").c_str(), &FindFileData);
				realPath = m_path;
			}
			else
			{
				hFind = FindFirstFile((aPath + "*").c_str(), &FindFileData);
				realPath = aPath;
			}
			
			

			if (hFind == INVALID_HANDLE_VALUE){
				return false; /* No files found */
			}

			do{
				std::string filename = FindFileData.cFileName;
				if (_strcmpi(base::utils::GetFilenameExtension(filename).c_str(),"bin") == 0 ){
					CMesh* mesh = new CMesh();
					mesh->Load(realPath + filename);
					mesh->SetName(filename);
					mesh->SetLocalPath(realPath);
					Add(filename, mesh);
				}
			} while (FindNextFile(hFind, &FindFileData));

			return true;
		}

		CMesh* CMeshManager::GetMesh(const std::string& aFilename)
		{
			return operator()(aFilename);
		}

		bool CMeshManager::Reload()
		{
			Destroy();
			Load("");
			return Load("data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/meshes");
		}
	}
}
