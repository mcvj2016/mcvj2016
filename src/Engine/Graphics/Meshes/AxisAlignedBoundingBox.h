#ifndef _ENGINE_AXISALIGNEDBOUNDINGBOX_DPVD1_21122016201200_H
#define _ENGINE_AXISALIGNEDBOUNDINGBOX_DPVD1_21122016201200_H

#include "Utils/Defines.h"
#include "Math/Color.h"

namespace engine
{
	namespace materials
	{
		class CAxisAlignedBoundingBox
		{
		public:
			CAxisAlignedBoundingBox()
				: m_Min(.0,.0,.0),
				m_Max(.0,.0,.0)
			{
			}
			virtual ~CAxisAlignedBoundingBox()
			{
			}
			GET_SET_REF(Vect3f, Min);
			GET_SET_REF(Vect3f, Max);
		protected:
			Vect3f m_Min;
			Vect3f m_Max;
		};
	}
}
#endif