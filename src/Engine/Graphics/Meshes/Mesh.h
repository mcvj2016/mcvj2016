#ifndef _ENGINE_MESH_DPVD1_21122016202800_H
#define _ENGINE_MESH_DPVD1_21122016202800_H

#include "Utils/Name.h"
//#include "Render/RenderManager.h"
//#include "Graphics/Materials/Material.h"
//#include "Geometry.h"
#include "AxisAlignedBoundingBox.h"
#include "BoundingSphere.h"
#include <vector>
#include "PhysxMesh.h"

namespace engine
{
	namespace render
	{
		class CRenderManager;
	}
	namespace materials
	{
		class CMaterial;
		class CGeometry;
		class CBoundingSphere;
		class CAxisAlignedBoundingBox;

#define HEADER 0xFE55
#define FOOTER 0x55FE

		class CMesh : public CName
		{
		public:
			CMesh();
			CMesh(CXMLElement* aElement);
			virtual ~CMesh();
			bool Load(const std::string& aFilename);
			virtual bool Render(render::CRenderManager& aRenderManager);
			CBoundingSphere GetBoundingSphere() const;
			CAxisAlignedBoundingBox GetAABB() const;
			GET_SET(std::string, LocalPath);
			materials::CPhysxMesh* CMesh::GetPhysxMesh(std::string file);
			GET_SET(std::vector< CMaterial* >, Materials);
		protected:
			uint32 mCount;
			std::vector< CMaterial* > m_Materials;
			std::vector< CGeometry* > m_Geometries;
			CAxisAlignedBoundingBox m_AABB;
			CBoundingSphere m_BoundingSphere;
			std::string m_LocalPath;
		private:
			void Destroy();
		};
	}
}
#endif
