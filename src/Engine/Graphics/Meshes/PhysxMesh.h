#ifndef _PHYSX_MESH_DPVD1_21122016202800_H
#define _PHYSX_MESH_DPVD1_21122016202800_H

#include "Utils/Name.h"
#include "Utils/Types.h"

namespace engine
{
	namespace materials
	{
		class CPhysxMesh : public CName
		{
		public:
			CPhysxMesh();
			virtual ~CPhysxMesh();
			uint32 numVertexes;
			uint32 numIndices;
			void* m_Vertexes;
			void* m_Indices;
		private:
			void Destroy();
		};
	}
}
#endif
