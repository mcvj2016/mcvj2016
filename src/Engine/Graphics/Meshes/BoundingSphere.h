#ifndef _ENGINE_BOUNDINGSPHERE_DPVD1_21122016201200_H
#define _ENGINE_BOUNDINGSPHERE_DPVD1_21122016201200_H

#include "Utils/Defines.h"
#include "Math/Color.h"

namespace engine
{
	namespace materials
	{
		class CBoundingSphere
		{
		public:
			CBoundingSphere()
				: m_Radius(0.0f),
				m_Center((.0f,.0f,.0f))
			{
			}
			virtual ~CBoundingSphere()
			{
			}
			GET_SET_REF(Vect3f, Center);
			GET_SET_REF(float, Radius);
		protected:
			float m_Radius;
			Vect3f m_Center;
		};
	}
}
#endif