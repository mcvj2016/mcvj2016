#ifndef _ENGINE_TEXTURE_DPVD1_12122016165100_H
#define _ENGINE_TEXTURE_DPVD1_12122016165100_H

#include "Utils/Name.h"
#include "Math/Vector2.h"
#include <d3d11.h>
#include "Utils/Types.h"
#include "Graphics/Materials/Material.h"

namespace engine
{
	namespace materials
	{
		class CTexture : public CName
		{
			enum EType
			{
				e2D_Texture = 0,
				e2D_TextureArray,
				e3D_Texture
			};
		public:
			CTexture(const std::string& aName);
			CTexture(const CXMLElement *TreeNode);
			virtual ~CTexture();
			bool Load();
			bool Reload();
			void Bind(uint32 aStageId, ID3D11DeviceContext* aContext);
			GET_SET_PTR(ID3D11ShaderResourceView, Texture);
			GET_SET(CMaterial::ETextureIndex, TextureIndex);
			GET_SET(Vect2u, Size);
			GET_SET(bool, Dynamic);
			GET_SET(std::string, Path)
			std::string m_DriveName;
		protected:
			ID3D11ShaderResourceView *m_Texture;
			ID3D11SamplerState *m_SamplerState;
			Vect2u m_Size;
			bool m_Dynamic;
			CMaterial::ETextureIndex m_TextureIndex;
		private:
			DISALLOW_COPY_AND_ASSIGN(CTexture);
			void Destroy();
			std::string m_Path;
		};
	}
}
#endif
