#include "Texture.h"
#include "TextureManager.h"
#include "Utils/Imgui/imgui.h"
#include "Utils/CheckedDelete.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"

namespace engine
{
	namespace materials
	{
		CTextureManager::CTextureManager(const std::string& l_path) 
		:m_path(l_path),
		m_commonTexturesPath("data/common/textures/")
		{
			LoadCommonTextures();
		}

		CTextureManager::~CTextureManager()
		{
		}

		CTexture* CTextureManager::GetTexture(const std::string& aFilename){
			return operator()(aFilename);
		}

		bool CTextureManager::AddTexture(CTexture* texture)
		{
			bool added = Add(texture->GetName(), texture);
			return added;
		}

		void CTextureManager::LoadCommonTextures()
		{
			Load(m_commonTexturesPath);
		}

		bool CTextureManager::Load(const std::string& aPath)
		{

			std::string realPath;

			WIN32_FIND_DATA FindFileData;
			HANDLE hFind = FindFirstFile((m_path + "*").c_str(), &FindFileData);

			if (aPath == "")
			{
				hFind = FindFirstFile((m_path + "*").c_str(), &FindFileData);
				realPath = m_path;
			}
			else
			{
				hFind = FindFirstFile((aPath + "*").c_str(), &FindFileData);
				realPath = aPath;
			}

			if (hFind == INVALID_HANDLE_VALUE){
				return false; /* No files found */
			}

			do{
				std::string filename = FindFileData.cFileName;
				if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (filename != "." && filename != "..")
					{
						Load(realPath + filename + "/");
					}
				}
				else if (filename != "." && filename != "..")
				{
					CTexture* texture = new CTexture(filename);
					texture->SetPath(realPath);
					texture->m_DriveName = filename;
					texture->Load();
					if (!Add(filename, texture))
					{
						base::utils::CheckedDelete(texture);
					}
				}
			} while (FindNextFile(hFind, &FindFileData));

			return true;
		}

		bool CTextureManager::Reload()
		{
			Destroy();
			LoadCommonTextures();
			const std::string& path = "data/scenes/" + CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName() + "/textures/";
			return Load(path);
		}
	}
}
