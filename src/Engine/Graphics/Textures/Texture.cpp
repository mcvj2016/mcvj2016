#include "Texture.h"
#include "Utils/StringUtils.h"
#include "Engine.h"
#include "Render/RenderManager.h"
#include "XML/XML.h"
#include "DirectXTex/DirectXTex.h"
#include "Utils/CheckedDelete.h"

namespace engine
{
	namespace materials
	{
		CTexture::CTexture(const std::string& aName)
			: CName(aName), 
			m_Texture(nullptr), 
			m_SamplerState(nullptr), 
			m_Dynamic(false),
			m_TextureIndex(CMaterial::eDiffuse),
			m_Path(""),
			m_DriveName(""),
			m_Size(Vect2u(0, 0))
		{
			assert(m_Name != "");
		}

		CTexture::CTexture(const tinyxml2::XMLElement* TreeNode)
			: CTexture(TreeNode->GetAttribute<std::string>("name", ""))
		{
			assert(m_Name != "");
		}

		CTexture::~CTexture()
		{
			//m_SamplerState->Release();
			//m_Texture->Release();
		}

		bool CTexture::Load()
		{
			render::CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();
			ID3D11Device *lDevice = lRM.GetDevice();
			ID3D11DeviceContext *lContext = lRM.GetDeviceContext();

			ID3D11Resource * lResource = nullptr;

			std::string fullPath = m_Path + m_Name;
			// Convert the string filename to wide string
			std::wstring lTextureFileNameWString = std::wstring(fullPath.begin(), fullPath.end());

			std::string lExtension = base::utils::GetFilenameExtension(m_Name);
			HRESULT lHR = E_FAIL;

			DirectX::TexMetadata info;
			DirectX::ScratchImage *image = new DirectX::ScratchImage();

			if (_strcmpi(lExtension.c_str(), "dds") == 0){//lExtension == "dds")
				lHR = DirectX::LoadFromDDSFile(lTextureFileNameWString.c_str(), DirectX::DDS_FLAGS_NONE, &info, *image);
			}
			else if (_strcmpi(lExtension.c_str(), "tga") == 0){
				lHR = DirectX::LoadFromTGAFile(lTextureFileNameWString.c_str(), &info, *image);
			}
			else{
				lHR = DirectX::LoadFromWICFile(lTextureFileNameWString.c_str(), DirectX::WIC_FLAGS_NONE, &info, *image);
			}

			if (SUCCEEDED(lHR))
			{
				lHR = CreateTexture(lDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &lResource);
				if (SUCCEEDED(lHR))
					lHR = CreateShaderResourceView(lDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &m_Texture);

				if (SUCCEEDED(lHR))
				{
					ID3D11Texture2D * lTex2D = static_cast<ID3D11Texture2D *>(lResource);
					D3D11_TEXTURE2D_DESC desc;
					lTex2D->GetDesc(&desc);

					m_Size.x = desc.Width;
					m_Size.y = desc.Height;

					D3D11_SAMPLER_DESC lSamplerDesc;
					ZeroMemory(&lSamplerDesc, sizeof(lSamplerDesc));
					lSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
					lSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
					lSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
					lSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
					lSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
					lSamplerDesc.MinLOD = 0;
					lSamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
					lHR = lDevice->CreateSamplerState(&lSamplerDesc, &m_SamplerState);
				}
			}

			base::utils::CheckedDelete(image);

			return SUCCEEDED(lHR);
		}

		void CTexture::Bind(uint32 aStageId, ID3D11DeviceContext* aContext)
		{
			aContext->PSSetSamplers(aStageId, 1, &m_SamplerState);
			aContext->PSSetShaderResources(aStageId, 1, &m_Texture);
		}

		bool CTexture::Reload()
		{
			return Load();
		}
	}
}
