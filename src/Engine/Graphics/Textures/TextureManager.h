#ifndef _ENGINE_TEXTUREMANAGER_DPVD1_12122016165300_H
#define _ENGINE_TEXTUREMANAGER_DPVD1_12122016165300_H

#include "Texture.h"
#include "Utils/TemplatedMapVector.h"

namespace engine
{
	namespace materials
	{
		class CTextureManager : public base::utils::CTemplatedMapVector<CTexture>
		{
		public:
			CTextureManager(const std::string& l_path = "data/textures/");
			virtual ~CTextureManager();
			CTexture* GetTexture(const std::string& aFilename);
			bool AddTexture(CTexture* texture);
			void LoadCommonTextures();
			bool Load(const std::string& aPath);
			bool Reload();
		private:
			std::string m_path;
			std::string m_commonTexturesPath;
		};
	}
}

#endif