Vector3.h
namespace mathUtils
{
    template  <> inline  Vect3f Lerp<Vect3f>(const Vect3f& _a, const Vect3f& _b, const float& _lambda) { return _a.GetLerp(_b, _lambda); }
}

Vector2.h
namespace mathUtils
{
    template  <>                    inline  Vect2f     Lerp<Vect2f>(const Vect2f& _a, const Vect2f& _b, const float& _lambda) { return _a.GetLerp(_b, _lambda); }
}

MathTypes.h
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TIPOS DE INTERPOLACIÍN
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum ETypeFunction { FUNC_LINEAR, FUNC_INCREMENT, FUNC_DECREMENT };