#ifndef CINEMATICMANAGER_H
#define CINEMATICMANAGER_H

#include "Utils/TemplatedMapVector.h"
#include "Cinematic.h"
#include "Camera/CameraController.h"
#include "Components/Objects/CinematicCallbacks.h"

namespace engine
{
	namespace cinematics
	{
		class CCinematicManager : public base::utils::CTemplatedMapVector<CCinematic>
		{
		public:
			CCinematicManager();
			virtual ~CCinematicManager();

			void Load(const std::string &Filename = "");
			void Reload();
			void Update(float elapsedTime);
			void Play(const std::string &aName);
			void Stop(const std::string &aName);

			/*bool Load(const std::string &Filename);
			bool Reload();
			bool Update(float elapsedTime);
			bool Play(const std::string &Filename);*/

			void SaveMainCamera(CCameraController* cam);
			CCameraController* GetSavedMainCamera();

			logic::components::CCinematicCallback* m_CinematicCallback;
		protected:
			std::string m_Filename;
			CCameraController * savedCamera = nullptr;
		};

	}
}
#endif
