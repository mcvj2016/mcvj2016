#include "CinematicKey.h"
#include "XML/XML.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicKey::CCinematicKey()
			: m_Time(0),
			m_Position(0)
		{ }

		CCinematicKey::~CCinematicKey()
		{ }

		bool CCinematicKey::Load(const tinyxml2::XMLElement* aElement)
		{
			m_Time = aElement->GetAttribute<float>("time", 0.0f);
			m_Position = aElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
			return false;
		}	
	}
}
