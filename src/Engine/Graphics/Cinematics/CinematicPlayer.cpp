
#include "CinematicPlayer.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicPlayer::CCinematicPlayer()
			: mKeys(0), 
			m_CurrentKey(0), 
			m_NextKey(0)
		{ }

		CCinematicPlayer::~CCinematicPlayer()
		{ }

		void CCinematicPlayer::PlayFoward(float currentTime)
		{
			if (m_NextKey <= mKeys.size() - 1)
			{
				//// Calculate the % of the animation, in order to interpolate key by key
				float l_CurrentP = (currentTime - mKeys[m_CurrentKey]->GetTime()) /
					(mKeys[m_NextKey]->GetTime() - mKeys[m_CurrentKey]->GetTime());
				Apply(l_CurrentP, mKeys[m_CurrentKey], mKeys[m_NextKey]);
				if (m_NextKey < mKeys.size() - 1)
				{
					m_NextKey = m_CurrentKey + 1;
					if (currentTime >= mKeys[m_NextKey]->GetTime()){
						m_CurrentKey = m_NextKey;
					}
				}
			}
		}

		void CCinematicPlayer::PlayBackward(float currentTime)
		{
			if (m_NextKey >= 0)
			{
				
				//// Calculate the % of the animation, in order to interpolate key by key
				float l_CurrentP = (currentTime - mKeys[m_CurrentKey]->GetTime()) /
					(mKeys[m_NextKey]->GetTime() - mKeys[m_CurrentKey]->GetTime());
				Apply(l_CurrentP, mKeys[m_CurrentKey], mKeys[m_NextKey]);
				if (m_NextKey > 0)
				{
					m_NextKey = m_CurrentKey - 1;
					if (currentTime <= mKeys[m_NextKey]->GetTime()){
						m_CurrentKey = m_NextKey;
					}
				}
			}
		}

		void CCinematicPlayer::GetCurrentKeyForward(float currentTime)
		{
		}

		void CCinematicPlayer::GetCurrentKeyBackward(float currentTime)
		{
		}
	}
}
