
#include "CinematicObjectPlayer.h"
#include "XML/XML.h"
#include "CinematicObjectKey.h"
#include "Utils/CheckedDelete.h"
#include "Engine.h"
#include "Animator.h"
#include "Helper/ImguiHelper.h"
#include "Graphics/Scenes/SceneManager.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicObjectPlayer::CCinematicObjectPlayer(const std::string& aName)
			: m_SceneNode(nullptr)
		{
			m_Name = aName;
		}

		CCinematicObjectPlayer::~CCinematicObjectPlayer()
		{
			base::utils::CheckedDelete(mKeys);
		}

		bool CCinematicObjectPlayer::Load(const tinyxml2::XMLElement* aElement)
		{
			const tinyxml2::XMLElement* cok_key = aElement->FirstChildElement("key");
			while (cok_key != nullptr)
			{
				CCinematicObjectKey *i_key = new CCinematicObjectKey();
				i_key->Load(cok_key);
				mKeys.push_back(i_key); 
				cok_key = cok_key->NextSiblingElement("key");
			}

			m_SceneNode = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(m_Name);

			return true;
		}

		void CCinematicObjectPlayer::Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B)
		{
			// position
			CAnimator<Vect3f> lPositionAnimator;
			lPositionAnimator.Init(A->GetPosition(), B->GetPosition(), 1.0f, mathUtils::FUNC_LINEAR);
			Vect3f lCurrentPosition;
			lCurrentPosition.SetZero();
			lPositionAnimator.Update(aPercentage, lCurrentPosition);
			m_SceneNode->SetPosition(lCurrentPosition);
				
			//rotation
		    //Vect3f lCurrentRotation;
			//lCurrentRotation.SetZero();
			CAnimator<Quatf> lRotationAnimator;
			Quatf lCurrentRotation = qfIDENTITY;
			CCinematicObjectKey* a = static_cast<CCinematicObjectKey*>(A);
			CCinematicObjectKey* b = static_cast<CCinematicObjectKey*>(B);
			lRotationAnimator.Init(a->GetQuaternion(), b->GetQuaternion(), 1.0f, mathUtils::FUNC_LINEAR);
			lRotationAnimator.UpdateQuat(aPercentage, lCurrentRotation);
			/*m_SceneNode->SetYaw(lCurrentRotation.x);
			m_SceneNode->SetPitch(lCurrentRotation.y);
			m_SceneNode->SetRoll(lCurrentRotation.z);*/
			m_SceneNode->SetRotation(lCurrentRotation);

			//scale
			Vect3f lCurrentScale;
			lCurrentScale.SetZero();
			lPositionAnimator.Init(a->GetScale(), b->GetScale(), 1.0f, mathUtils::FUNC_LINEAR);
			lPositionAnimator.Update(aPercentage, lCurrentScale);
			m_SceneNode->SetScale(lCurrentScale);
		}

		void CCinematicObjectPlayer::Reset()
		{
			m_CurrentKey = 0;
			m_NextKey = 0;
			m_SceneNode->SetPosition(mKeys[0]->GetPosition());
		}
	}
}
