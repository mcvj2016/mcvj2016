#include "Cinematic.h"
#include "XML/XML.h"
#include "CinematicObjectPlayer.h"
#include "CinematicCameraPlayer.h"
#include "Engine.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Input/ActionManager.h"
#include "Components/Objects/CinematicCallbacks.h"
#include "Graphics/Cinematics/CinematicManager.h"

using namespace logic::components;

namespace engine
{
	namespace cinematics
	{
		CCinematic::CCinematic()
			: m_Active(false),
			m_Finish(false),
			m_Loop(false),
			m_Reversed(false),
			m_TotalTime(0),
			m_PlayingForward(false),
			m_PlayingBackward(false),
			m_CurrentTime(0),
			LAYER_NAME("cinematic_animated")
		{

		}

		CCinematic::~CCinematic()
		{ }

		void CCinematic::Load(const tinyxml2::XMLElement* aElement)
		{// aElement trae todos lo que hay dentro de <cinematic>
			m_Loop = aElement->GetAttribute<bool>("loop", false);
			m_Reversed = aElement->GetAttribute<bool>("reverse", false);
			m_TotalTime = aElement->GetAttribute<float>("duration", 0.0f);
			m_Name = aElement->GetAttribute<std::string>("name", "");

			const tinyxml2::XMLElement* cinematic_camera_player = aElement->FirstChildElement("cinematic_camera_player");

			while (cinematic_camera_player != nullptr) 
			{
				std::string lPlayerName = cinematic_camera_player->GetAttribute<std::string>("camera", "");
				CCinematicCameraPlayer *CCP = new CCinematicCameraPlayer(lPlayerName); // CCP = cinematic camera player
				CCP->Load(cinematic_camera_player);
				if (!Add(lPlayerName, CCP))
				{
					base::utils::CheckedDelete(CCP);
				}
				cinematic_camera_player = cinematic_camera_player->NextSiblingElement("cinematic_camera_player");
			}

			const tinyxml2::XMLElement* cinematic_object_player = aElement->FirstChildElement("cinematic_object_player");

			while (cinematic_object_player != nullptr) 
			{
				std::string lPlayerName = cinematic_object_player->GetAttribute<std::string>("scene_node", "");
				CCinematicObjectPlayer *COP = new CCinematicObjectPlayer(lPlayerName); //COP = cinematic object player
				COP->Load(cinematic_object_player);
				Add(lPlayerName, COP);
				cinematic_object_player = cinematic_object_player->NextSiblingElement("cinematic_object_player");
			}

			const tinyxml2::XMLElement* cinematic_animated = aElement->FirstChildElement("animated_objects");

			if (cinematic_animated != nullptr)
			{
				const tinyxml2::XMLElement* animated_element = cinematic_animated->FirstChildElement("animated");

				while (animated_element != nullptr)
				{
					m_AnimatedModel.push_back(animated_element->GetAttribute<std::string>("name", ""));
					m_Animation.push_back(animated_element->GetAttribute<int>("animation", 0));
					m_Cycle.push_back(animated_element->GetAttribute<bool>("cycle", false));
					animated_element = animated_element->NextSiblingElement("animated");
				}
			}
		}


		void CCinematic::Update(float elapsedTime)
		{
			//ESTO PARA EJECUTAR FRAME A FRAME DESDE EL TECLADO
			/*bool doFrame = false;
			if (CEngine::GetInstance().GetActionManager().IsInputActionActive("PerFrame"))
			{
				doFrame = true;
			}*/
			if (m_Active /*&& doFrame*/)
			{
				bool reset = false;
					
				//calcular ejecucion de los player
				if (!m_PlayingBackward)
				{
					m_PlayingForward = true;
				}
				//forward type
				if (m_PlayingForward)
				{	//si ya se ha superado
					if (m_CurrentTime >= m_TotalTime)
					{
						//si es reverse empezamos al reves
						if (m_Reversed)
						{
							m_CurrentTime = m_TotalTime;
							m_PlayingBackward = true;
							m_PlayingForward = false;
						}
						//si es loop reiniciamos tiempo
						else if (!m_Reversed && m_Loop)
						{
							m_CurrentTime = 0.0f;
							reset = true;
						}
						else
						{
							m_Finish = true;
						}
					}
					else
					{
						m_CurrentTime += elapsedTime;
					}
				}
				else if (m_PlayingBackward)
				{
					if (m_CurrentTime <= 0.0f)
					{
						if (m_Loop)
						{
							m_CurrentTime = 0.0f;
							m_PlayingBackward = false;
							m_PlayingForward = true;
						}
						else
						{
							m_Finish = true;
						}	
					}
					else{
						m_CurrentTime -= elapsedTime;
					}
				}

				//ejecutar players
				for (CCinematicPlayer* player : m_ResourcesVector)
				{
					if (reset)
					{
						player->Reset();
					}
					if (m_PlayingBackward)
					{
						player->PlayBackward(m_CurrentTime);
					}
					else
					{
						player->PlayFoward(m_CurrentTime);
					}
				}
				//si esta activa y ha acabado paramos cinematica y restablecemos la camara y animados
				if (m_Finish && m_Active){
					m_Active = false;
					StopAnimated();
				}
			}
		}

		void CCinematic::Play()
		{
			m_Active = true;
			m_Finish = false;
			StartAnimated();
		}

		void CCinematic::StartAnimated()
		{
			CScene* lScene = CEngine::GetInstance().GetSceneManager().GetCurrentScene();
			lScene->GetByName(LAYER_NAME)->SetActive(true);

			int position = 0;
			for (std::string animated : m_AnimatedModel)
			{
				cal3dimpl::CSceneAnimatedModel* model = static_cast<cal3dimpl::CSceneAnimatedModel*>(lScene->GetSceneNode(animated));
				model->ShowAnimated();
				if (m_Cycle[position]){
					model->BlendCycle(m_Animation[position], 1.0f, 0.0f);
				} 
				else
				{
					model->ExecuteAction(m_Animation[position], 0.0f, 5.0f, 1.0f, true);
				}
				position++;
			}
			CCinematicCallback* callback = CEngine::GetInstance().GetCinematicManager().m_CinematicCallback;
			callback->m_CinematicName = this->GetName();
			callback->m_AcumTime = 0.0f;
			callback->SetEnabled(true);
			callback->CinematicInit();
		}

		void CCinematic::StopAnimated()
		{
			CCinematicCallback* callback = CEngine::GetInstance().GetCinematicManager().m_CinematicCallback;
			callback->CinematicEnd();
			callback->m_CinematicName = "";
			callback->SetEnabled(false);

			CScene* lScene = CEngine::GetInstance().GetSceneManager().GetCurrentScene();
			lScene->GetByName(LAYER_NAME)->SetActive(false);

			int position = 0;
			for (std::string animated : m_AnimatedModel)
			{
				cal3dimpl::CSceneAnimatedModel* model = static_cast<cal3dimpl::CSceneAnimatedModel*>(lScene->GetSceneNode(animated));
				model->HideAnimated();
				if (model->GetTag()==CSceneNode::TAG::playercinpos)
				{
					CSceneNode* lplayer = lScene->GetFirstSceneNodeByTag(CSceneNode::TAG::player);
					lplayer->SetPosition(model->GetPosition());
					lplayer->SetYaw(model->GetYaw());
					lplayer->SetPitch(model->GetPitch());
					lplayer->SetRoll(model->GetRoll());
					CEngine::GetInstance().GetPhysXManager().SetCharacterControllerPosition(lplayer->GetName(), lplayer->GetPosition());
				}
				if (m_Cycle[position]){
					model->ClearCycle(m_Animation[position], 0.0f);
				}
			}
		}

		void CCinematic::Stop()
		{
			m_CurrentTime = 0.0f;
			m_PlayingBackward = false;
			m_PlayingForward = false;
			m_Finish = true;
			m_Active = false;
			for (CCinematicPlayer* player : m_ResourcesVector)
			{
				player->Reset();
			}
		}
	}
}
