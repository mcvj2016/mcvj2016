
#include "CinematicObjectKey.h"
#include "XML/XML.h"
#include "Utils/CheckedDelete.h"
#include "Math/Transform.h"
#include "Math/Quaternion.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicObjectKey::CCinematicObjectKey()
			: m_Rotation(0)
			,m_Scale(0)
			,m_Quaternion(Quatf(0,0,0,1))
		{ }

		CCinematicObjectKey::~CCinematicObjectKey()
		{
		}

		bool CCinematicObjectKey::Load(const tinyxml2::XMLElement* aElement)
		{
			CCinematicKey::Load(aElement);
			//float lYaw = aElement->GetAttribute<float>("yaw", .0f);
			//float lPitch = aElement->GetAttribute<float>("pitch", .0f);
			//float lRoll= aElement->GetAttribute<float>("roll", .0f);
			m_Quaternion = Quatf(aElement->GetAttribute<Quatf>("rotation", Quatf(0,0,0,0)));			
			//m_Rotation = Vect3f(m_Quaternion.EulerFromQuat().z, m_Quaternion.EulerFromQuat().y, m_Quaternion.EulerFromQuat().x);
			m_Rotation = Vect3f(m_Quaternion.GetYaw(), m_Quaternion.GetPitch(), m_Quaternion.GetRoll());
			m_Scale = aElement->GetAttribute<Vect3f>("scale", Vect3f(0.0f, 0.0f, 0.0f));
			return false;
		}
	}
}
