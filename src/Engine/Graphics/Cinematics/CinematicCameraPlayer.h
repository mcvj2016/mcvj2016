#ifndef CINEMATICCAMERAPLAYER_H
#define	CINEMATICCAMERAPLAYER_H

#include "CinematicPlayer.h"
#include <string>
#include "Utils/Defines.h"
#include "Camera/CameraController.h"
#include "Camera/AnimatedCamera.h"

namespace tinyxml2 
{
	class XMLElement;
}

namespace engine
{
	namespace cinematics
	{
		class CCinematicCameraPlayer : public CCinematicPlayer
		{
		public:
			CCinematicCameraPlayer(const std::string& aName);
			virtual ~CCinematicCameraPlayer();
				
			bool Load(const CXMLElement* aElement) override;//virtual
			void Reset() override;
		private:
			DISALLOW_COPY_AND_ASSIGN(CCinematicCameraPlayer);
			CAnimatedCamera* m_Camera;
			CCameraController* m_PrvMainCamera;

		protected:
			//virtual void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B) = 0;
			void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B);
		};

	}
}
#endif
