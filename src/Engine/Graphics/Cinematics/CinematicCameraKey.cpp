#include "CinematicCameraKey.h"
#include "XML/XML.h"

namespace engine
{

	namespace cinematics
	{
		CCinematicCameraKey::CCinematicCameraKey()
			: CCinematicKey(), 
			m_NearPlane(0),
			m_FarPlane(0),
			m_FOV(0),
			m_Eye(0),
			m_LookAt(0),
			m_Up(0)
		{ }

		CCinematicCameraKey::~CCinematicCameraKey()
		{ }

		bool CCinematicCameraKey::Load(const tinyxml2::XMLElement* aElement)
		{
			CCinematicKey::Load(aElement);
			m_NearPlane = aElement->GetAttribute<float>("near_plane", 1.0f);
			m_FarPlane = aElement->GetAttribute<float>("far_plane", 1000.0f);
			m_FOV = aElement->GetAttribute<float>("fov", 45.0f);
			m_Eye = aElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
			m_LookAt = aElement->GetAttribute<Vect3f>("look_at", Vect3f(0.0f, 0.0f, 0.0f));
			m_Up = aElement->GetAttribute<Vect3f>("up", Vect3f(0.0f, 1.0f, 0.0f));
			
			return false;
		}
	}
}
