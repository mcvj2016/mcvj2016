#ifndef  CINEMATICKEY_H
#define	 CINEMATICKEY_H

#include "Math/Vector3.h"
#include "Utils/Defines.h"

namespace tinyxml2 {class XMLElement;}

namespace engine
{
	namespace cinematics
	{
		class CCinematicKey
		{
		public:
			CCinematicKey();
			virtual ~CCinematicKey();

			virtual bool Load(const CXMLElement* aElement);
			// virtual bool Load(const CXMLElement* aElement);
			GET_SET_REF(float, Time);
			GET_SET_REF(Vect3f, Position);
		private:
			DISALLOW_COPY_AND_ASSIGN(CCinematicKey);

		protected:
			float m_Time;
			Vect3f m_Position;
		};

	}
}
#endif
