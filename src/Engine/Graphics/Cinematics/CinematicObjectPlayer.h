#ifndef CINEMATICOBJECTPLAYER_H
#define CINEMATICOBJECTPLAYER_H

#include "Graphics/Scenes/SceneNode.h"
#include "CinematicPlayer.h"

namespace engine
{
	namespace cinematics
	{
		class CCinematicKey;

		class CCinematicObjectPlayer : public CCinematicPlayer
		{
		public:
			CCinematicObjectPlayer(const std::string& aName);
			virtual ~CCinematicObjectPlayer();
			bool Load(const CXMLElement* aElement) override;

		private:
			DISALLOW_COPY_AND_ASSIGN(CCinematicObjectPlayer);
			scenes::CSceneNode* m_SceneNode;

		protected:
			void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B) override;
			void Reset() override;
		};

	}
}
#endif
