#ifndef CINEMATICCAMERAKEY_H
#define CINEMATICCAMERAKEY_H 

#include "CinematicKey.h"
#include "Utils/Defines.h"

namespace engine
{
	namespace cinematics
	{
		class CCinematicCameraKey : public CCinematicKey
		{
		public:
			CCinematicCameraKey();
			virtual ~CCinematicCameraKey();
			bool Load(const CXMLElement* aElement) override;//bool Load(const CXMLElement* aElement)

			GET_SET_REF(float, NearPlane);
			GET_SET_REF(float, FarPlane);
			GET_SET_REF(float, FOV);
			GET_SET_REF(Vect3f, Eye);
			GET_SET_REF(Vect3f, LookAt);
			GET_SET_REF(Vect3f, Up);

		private:
			float m_NearPlane;
			float m_FarPlane;
			float m_FOV;;
			Vect3f m_Eye; //added as new
			Vect3f m_LookAt;
			Vect3f m_Up;

		//private:
			DISALLOW_COPY_AND_ASSIGN(CCinematicCameraKey);
		};
	}
}
#endif
