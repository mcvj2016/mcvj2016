#ifndef CINEMATICOBJECTKEY_H
#define CINEMATICOBJECTKEY_H

#include "Utils/Defines.h"
#include "CinematicKey.h"
#include "Math/Quaternion.h"

namespace tinyxml2 { class XMLElement; }

namespace engine
{
	namespace cinematics
	{

		class CCinematicObjectKey : public CCinematicKey
		{
		public:
			CCinematicObjectKey();
			virtual ~CCinematicObjectKey();
			bool Load(const CXMLElement* aElement) override;
			//virtual bool Load(const CXMLElement* aElement);

			GET_SET_REF(Vect3f, Rotation);
			GET_SET_REF(Vect3f, Scale);
			GET_SET_REF(Quatf, Quaternion);

		private:
			Vect3f m_Rotation;
			Vect3f m_Scale;
			Quatf m_Quaternion;

			DISALLOW_COPY_AND_ASSIGN(CCinematicObjectKey);
		};
	}
}
#endif
