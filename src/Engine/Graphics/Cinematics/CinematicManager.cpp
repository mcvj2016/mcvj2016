#include "XML/XML.h"
#include "Engine.h"
#include "CinematicManager.h"
#include "Cinematic.h"
#include "Utils/Logger/Logger.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicManager::CCinematicManager()
			:m_Filename("data/common/cinematics.xml"), m_CinematicCallback(nullptr)
		{  }
		CCinematicManager::~CCinematicManager()
		{
		}

		void CCinematicManager::Load(const std::string &Filename)
		{
			//DONE Load: Cargar� el XML de cinematics exportado desde max
			tinyxml2::XMLDocument xmldoc;

			bool loaded = false;

			if (Filename == "")
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(this->m_Filename.c_str()));

			}
			else
			{
				loaded = base::xml::SucceedLoad(xmldoc.LoadFile(Filename.c_str()));
			}

			if (loaded)
			{
				tinyxml2::XMLElement* cinematics = xmldoc.FirstChildElement("cinematics");

				if (cinematics != nullptr)
				{
					tinyxml2::XMLElement* cinematic = cinematics->FirstChildElement("cinematic");
					// coje lo uqe hay dentro de <cinematics>

					while (cinematic != nullptr)
					{
						std::string cinematicName = cinematic->GetAttribute<std::string>("name", "");
						// otros parametros como loop, reverse y total_time estan cogidos en Cinematic.cpp
						CCinematic* cinematic_something_player = new CCinematic();

						cinematic_something_player->Load(cinematic);

						this->Add(cinematicName, cinematic_something_player);

						cinematic = cinematic->NextSiblingElement("cinematic");

					}
				}
				else
				{
					LOG_ERROR_APPLICATION(" ERROR EN LA PARTE DE <CINEMATIC> EN EL XML", cinematics);
				}
			}
		}

		void CCinematicManager::Reload()
		{
			Destroy();
			this->Load();
		}

		void CCinematicManager::Update(float elapsedTime)
		{
			for (CCinematic* cinematic : m_ResourcesVector)
			{
				cinematic->Update(elapsedTime);
			}
		}

		void CCinematicManager::Play(const std::string &aName)
		{
			this->operator()(aName)->Play();
		}

		void CCinematicManager::Stop(const std::string& aName)
		{
			this->operator()(aName)->Stop();
		}

		void CCinematicManager::SaveMainCamera(CCameraController* cam)
		{
			savedCamera = cam;
		}

		CCameraController* CCinematicManager::GetSavedMainCamera()
		{
			return savedCamera;
		}
	}
}

