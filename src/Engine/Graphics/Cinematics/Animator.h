#ifndef ANIMATOR_HH_
#define ANIMATOR_HH_
#pragma once

// Author: Alejandro V�zquez

#include "Utils\Types.h"
#include "Math\MathTypes.h"

template < typename T >
class CAnimator
{
public:
    CAnimator();
    virtual ~CAnimator() {}

    void Init( const T& aInitValue, const T& aEndValue, float aTotalTime, mathUtils::ETypeFunction type );
    void SetDegree(uint32 aDegree);
    bool Update(float aElapsedTime, T& aValue);
	bool UpdateQuat(float aElapsedTime, T & aValue);
    bool IsFinish();
private:
	mathUtils::ETypeFunction   mFunction;
    T               mInitValue;
    T               mEndValue;
    float           mTotalTime;
    float           mElapsedTime;
    uint32          mDegree;
};

template<typename T>
CAnimator<T>::CAnimator()
{
}

template<typename T>
void CAnimator<T>::Init(const T& aInitValue, const T& aEndValue, float aTotalTime, mathUtils::ETypeFunction aType)
{
    mInitValue      = aInitValue;
    mEndValue       = aEndValue;
    mTotalTime      = aTotalTime;
    mElapsedTime    = 0.f;
    mFunction       = aType;
}

template<typename T>
void CAnimator<T>::SetDegree(uint32 aDegree)
{
    mDegree = aDegree;
}

template<typename T>
bool CAnimator<T>::Update(float aElapsedTime, T & aValue)
{
    mElapsedTime += aElapsedTime;
    bool finish = false;
    if (mElapsedTime >= mTotalTime)
    {
        finish = true;
        mElapsedTime = mTotalTime;
    }

    // The mu alwais must be between 0 and 1
    float mu = mElapsedTime / mTotalTime;
    switch (mFunction)
    {
        case mathUtils::FUNC_LINEAR: {} break;
        case mathUtils::FUNC_INCREMENT: { mu = mathUtils::PowN(mu, mDegree); } break;
        case mathUtils::FUNC_DECREMENT: { mu = sqrt(mu); } break;
    }
	
	//aValue = Vect3f().Lerp(mInitValue, mEndValue, mu);
	aValue = mathUtils::Lerp(mInitValue, mEndValue, mu);
    return finish;
}

template<typename T>
bool CAnimator<T>::UpdateQuat(float aElapsedTime, T & aValue)
{
	mElapsedTime += aElapsedTime;
	bool finish = false;
	if (mElapsedTime >= mTotalTime)
	{
		finish = true;
		mElapsedTime = mTotalTime;
	}

	// The mu alwais must be between 0 and 1
	float mu = mElapsedTime / mTotalTime;
	switch (mFunction)
	{
	case mathUtils::FUNC_LINEAR: {} break;
	case mathUtils::FUNC_INCREMENT: { mu = mathUtils::PowN(mu, mDegree); } break;
	case mathUtils::FUNC_DECREMENT: { mu = sqrt(mu); } break;
	}

	//aValue = Vect3f().Lerp(mInitValue, mEndValue, mu);
	aValue = Quatf::Slerp(mInitValue, mEndValue, mu);
	return finish;
}

template<typename T>
bool CAnimator<T>::IsFinish()
{
    return mElapsedTime >= mTotalTime;
}

#endif
