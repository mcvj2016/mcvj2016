#ifndef _CINEMATIC_H
#define _CINEMATIC_H

#include "Utils/TemplatedMapVector.h"
#include "Utils/Name.h"
#include "CinematicPlayer.h"

namespace engine
{
	namespace cinematics
	{
		class CCinematic : public CName, public base::utils::CTemplatedMapVector< CCinematicPlayer >
		{
		public:
			CCinematic();
			virtual ~CCinematic();
				
			void Load(const CXMLElement* aElement);//bool
			void Update(float elapsedTime);
			void Play();
			void Stop();
			GET_SET_BOOL(Active);
			GET_SET_BOOL(Loop);
			GET_SET_BOOL(Reversed);
			GET_SET_BOOL(Finish);
			GET_SET_REF(float, TotalTime);
			GET_SET_REF(float, CurrentTime);

		protected:
			void StartAnimated();
			void StopAnimated();

			bool m_Active;
			bool m_Finish;
			bool m_Loop;
			bool m_Reversed;
			float m_TotalTime;
			bool m_PlayingForward;
			bool m_PlayingBackward;
			float m_CurrentTime;
			std::vector<std::string> m_AnimatedModel;
			std::vector<int> m_Animation;
			std::vector<bool> m_Cycle;
			std::string LAYER_NAME;
		private:
			DISALLOW_COPY_AND_ASSIGN(CCinematic);
		};
	}
}
#endif
