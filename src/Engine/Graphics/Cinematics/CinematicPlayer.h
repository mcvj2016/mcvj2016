#ifndef CINEMATICPLAYER_H
#define CINEMATICPLAYER_H

#include "Utils/Defines.h"
#include <vector>
#include "CinematicKey.h"
#include "Utils/Name.h"
#include "CinematicObjectKey.h"

namespace engine
{
	namespace cinematics
	{
		class CCinematicPlayer : public CName
		{
		public:
			CCinematicPlayer();
			virtual ~CCinematicPlayer();
			virtual bool Load(const CXMLElement* aElement) = 0;

			void PlayFoward(float currentTime);
			void PlayBackward(float currentTime);
			virtual void Reset() = 0;

		protected:
			typedef std::vector< CCinematicKey *> TKeys;
			TKeys mKeys; 
			size_t m_CurrentKey, m_NextKey;

			void GetCurrentKeyForward(float currentTime);
			void GetCurrentKeyBackward(float currentTime);

			virtual void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B) = 0;

		private:
			DISALLOW_COPY_AND_ASSIGN(CCinematicPlayer);
		};

	}
}
#endif
