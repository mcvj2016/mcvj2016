#include "CinematicCameraPlayer.h"
#include "XML/XML.h"
#include "CinematicCameraKey.h"
#include "Utils/CheckedDelete.h"
#include "Engine.h"
#include "Animator.h"
#include "Camera/AnimatedCamera.h"
#include "Camera/CameraManager.h"

namespace engine
{
	namespace cinematics
	{
		CCinematicCameraPlayer::CCinematicCameraPlayer(const std::string& aName)
		{
			m_Name = aName;
		}

		CCinematicCameraPlayer::~CCinematicCameraPlayer()
		{
			base::utils::CheckedDelete(mKeys);
		}

		bool CCinematicCameraPlayer::Load(const tinyxml2::XMLElement* aElement)
		{
			const tinyxml2::XMLElement* cck_key = aElement->FirstChildElement("key");// un elemento del xml <key...... />
				
			while (cck_key != nullptr) // 
			{
				CCinematicCameraKey *i_key = new CCinematicCameraKey();//crear instancia
				i_key->Load(cck_key);
				mKeys.push_back(i_key); // etiqueta <key ... en xml, acumula los keys 
				cck_key = cck_key->NextSiblingElement("key");
			}

			m_Camera = static_cast<CAnimatedCamera*>(CEngine::GetInstance().GetCameraManager().operator()(m_Name));

			return false;
		}

		void CCinematicCameraPlayer::Reset()
		{
			m_CurrentKey = 0;
			m_NextKey = 0;
			m_Camera->GetSceneCamera()->SetPosition(mKeys[0]->GetPosition());
		}

		void CCinematicCameraPlayer::Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B)
		{
			//cambiamos la camara del game. Mediante logica se deber� restrablecer
			CEngine::GetInstance().GetCameraManager().SetMainCamera(m_Camera);
				
			CCinematicCameraKey* a = static_cast<CCinematicCameraKey*>(A);
			CCinematicCameraKey* b = static_cast<CCinematicCameraKey*>(B);

			// position
			CAnimator<Vect3f> lPositionAnimator;
			lPositionAnimator.Init(a->GetEye(), b->GetEye(), 1.0f, mathUtils::FUNC_LINEAR);
			Vect3f lCurrentPosition = v3fZERO;
			lPositionAnimator.Update(aPercentage, lCurrentPosition);
			m_Camera->GetSceneCamera()->SetPosition(lCurrentPosition);

			//lookat
			CAnimator<Vect3f> lLookAtAnimator;
			Vect3f lCurrentLookAt = v3fZERO;
			lLookAtAnimator.Init(a->GetLookAt(), b->GetLookAt(), 1.0f, mathUtils::FUNC_LINEAR);
			lLookAtAnimator.Update(aPercentage, lCurrentLookAt);
			m_Camera->SetLookAt(lCurrentLookAt);

			//up
			CAnimator<Vect3f> lUpAnimator;
			Vect3f lCurrentUp = v3fZERO;
			lUpAnimator.Init(a->GetUp(), b->GetUp(), 1.0f, mathUtils::FUNC_LINEAR);
			lUpAnimator.Update(aPercentage, lCurrentUp);
			m_Camera->SetUpAnim(lCurrentUp);
			m_Camera->SetMatrixs();

			//rotation
			/*Vect3f lCurrentRotationAt = v3fZERO;
			lPositionAnimator.Init(a->GetRotation(), b->GetRotation(), 1.0f, mathUtils::FUNC_LINEAR);
			lPositionAnimator.Update(aPercentage, lCurrentRotationAt);
			m_Camera->SetYaw(lCurrentRotationAt.x);
			m_Camera->SetPitch(lCurrentRotationAt.y);
			m_Camera->SetRoll(lCurrentRotationAt.z);

			m_Camera->SetLookAt(m_Camera->GetForward());
			m_Camera->SetUpAnim(m_Camera->GetUp());
			m_Camera->SetMatrixs();*/

			//fov
			/*CAnimator<float> lFloatAnimator;
			float lCurrentFloat = .0f;
			lFloatAnimator.Init(a->GetFOV(), b->GetFOV(), 1.0f, mathUtils::FUNC_LINEAR);
			lFloatAnimator.Update(aPercentage, lCurrentFloat);
			m_Camera->SetFOV(lCurrentFloat);

			//near
			lFloatAnimator.Init(a->GetNearPlane(), b->GetNearPlane(), 1.0f, mathUtils::FUNC_LINEAR);
			lFloatAnimator.Update(aPercentage, lCurrentFloat);
			m_Camera->SetNear(lCurrentFloat);

			//far
			lFloatAnimator.Init(a->GetFarPlane(), b->GetFarPlane(), 1.0f, mathUtils::FUNC_LINEAR);
			lFloatAnimator.Update(aPercentage, lCurrentFloat);
			m_Camera->SetFar(lCurrentFloat);*/
		}
	}
}
