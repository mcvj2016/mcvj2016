#include "Engine.h"
#include "Camera/CameraController.h"
#include "Utils/Imgui/imgui_impl_dx11.h"
#include "Helper/ImguiHelper.h"
#include "Graphics/Buffers/ConstantBufferManager.h"
#include "Render/RenderPipeline.h"
#include "Camera/CameraManager.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Textures/TextureManager.h"
#include "Graphics/Shaders/ShaderManager.h"
#include "Graphics/Effects/EffectManager.h"
#include "Graphics/Effects/TechniquePoolManager.h"
#include "Graphics/Lights/LightManager.h"
#include "Input/ActionManager.h"
#include "Render/RenderManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Meshes/MeshManager.h"
#include "Render/RenderPipelineManager.h"
#include "Graphics/Particles/ParticleManager.h"
#include "Sound/ISoundManager.h"
#include "Graphics/GUI/GUIManager.h"
#include "PhysXImpl/PhysXManager.h"
#include "Utils/Coroutine.h"
#include "Graphics/Scenes/PrefabManager.h"

namespace engine
{
	CEngine::CEngine()
		: m_DebugMode(true),
		  m_totalTime(0),
		  m_MaterialManager(nullptr),
		  m_RenderManager(nullptr),
		  m_ActionManager(nullptr),
		  m_InputManager(nullptr),
		  m_TextureManager(nullptr),
		  m_ConstantBufferManager(nullptr),
		  m_SceneManager(nullptr),
		  m_ShaderManager(nullptr),
		  m_EffectManager(nullptr),
		  m_MeshManager(nullptr),
		  m_TechniquePoolManager(nullptr),
		  m_AnimatedModelManager(nullptr),
		  m_LightManager(nullptr),
		  m_CinematicManager(nullptr),
		  m_CameraManager(nullptr),
		  m_ParticleManager(nullptr),
		  m_RenderPipelineManager(nullptr),
		  m_PhysXManager(nullptr),
		  m_Clock(),
		  m_PrevTime(m_Clock.now()),
		  m_ImGui(nullptr),
		  m_SoundManager(nullptr),
		  m_GUIManager(nullptr),
		  m_DeltaTime(0), m_Paused(false),
		  m_ExitGame(false),
		  m_ShowDebug(true),
		  m_Coroutine(nullptr),
		  m_PrefabsManager(nullptr)
	{
	}

	CEngine::~CEngine()
	{
		base::utils::CheckedDelete(m_MaterialManager);
		base::utils::CheckedDelete(m_CameraManager);
		base::utils::CheckedDelete(m_RenderManager);
		base::utils::CheckedDelete(m_ActionManager);
		base::utils::CheckedDelete(m_TextureManager);
		base::utils::CheckedDelete(m_ConstantBufferManager);
		base::utils::CheckedDelete(m_SceneManager);
		base::utils::CheckedDelete(m_ShaderManager);
		base::utils::CheckedDelete(m_EffectManager);
		base::utils::CheckedDelete(m_MeshManager);
		base::utils::CheckedDelete(m_TechniquePoolManager);
		base::utils::CheckedDelete(m_AnimatedModelManager);
		base::utils::CheckedDelete(m_CinematicManager);
		base::utils::CheckedDelete(m_LightManager);
		base::utils::CheckedDelete(m_InputManager);
		base::utils::CheckedDelete(m_ImGui);
		base::utils::CheckedDelete(m_PhysXManager);
		base::utils::CheckedDelete(m_ParticleManager);
		base::utils::CheckedDelete(m_RenderPipelineManager);
		base::utils::CheckedDelete(m_SoundManager);
		base::utils::CheckedDelete(m_GUIManager);
		//base::utils::CheckedDelete(m_PrefabsManager);
	}

	void CEngine::ProcessInputs()
	{
		m_ActionManager->Update();
	}

	void CEngine::Update()
	{
		
		if (!m_Paused)
		{
			auto currentTime = m_Clock.now();
			std::chrono::duration<float> chronoDeltaTime = currentTime - m_PrevTime;
			m_PrevTime = currentTime;

			m_DeltaTime = chronoDeltaTime.count() > 0.5f ? 0.5f : chronoDeltaTime.count();
			//add the time to the total time of the game
			m_totalTime += m_DeltaTime;

			//update the coroutines
			m_Coroutine->Update(m_DeltaTime);
		}
		
		// -----------------------------

		// game logic

		// -----------------------------
		m_CameraManager->GetMainCamera()->Update(m_DeltaTime);

		m_SceneManager->Update(m_DeltaTime);

		m_CinematicManager->Update(m_DeltaTime);

		m_PhysXManager->Update(m_DeltaTime);

		m_SoundManager->Update();

		
	}

	void CEngine::Render()
	{
		m_RenderPipelineManager->GetActivePipeline().Execute();
	}

	void CEngine::Dispose()
	{
		ImGui_ImplDX11_Shutdown();
	}

	void CEngine::Init(HWND& hWnd, int width, int height)
	{
		//OUR STUFF
		m_RenderManager = new render::CRenderManager();
		m_RenderManager->Init(hWnd, width, height);
		//CAMERAS
		m_CameraManager = new camera::CCameraManager();
		//INPUT/ACTION MANAGER
		m_InputManager = new input::CInputManager(hWnd);
		m_ActionManager = new input::CActionManager(*m_InputManager);
		m_ActionManager->LoadXml();
		//SHADERMANAGER;
		m_ShaderManager = new shaders::CShaderManager();
		m_ShaderManager->Load();
		//EFFECTMANAGER;
		m_EffectManager = new effects::CEffectManager();
		m_EffectManager->Load();
		//TECHNIQUEPOOLMANAGER;
		m_TechniquePoolManager = new effects::CTechniquePoolManager();
		m_TechniquePoolManager->Load();
		//TEXTUREMANAGER
		m_TextureManager = new materials::CTextureManager();
		//MATERIAL
		m_MaterialManager = new materials::CMaterialManager();
		//ANIMATOR MANAGER
		m_AnimatedModelManager = new cal3dimpl::CAnimatedModelManager();
		//PHYSX
		m_PhysXManager = CPhysXManager::CreatePhysXManager();
		//MESHMANAGER
		m_MeshManager = new materials::CMeshManager();
		m_MeshManager->Load();

		//PARTICLES MANAGER
		m_ParticleManager = new particles::CParticleManager();

		//LIGHTS
		m_LightManager = new lights::CLightManager();
		//CINEMATICS
		m_CinematicManager = new cinematics::CCinematicManager();
		//SCENEMANAGER
		m_SceneManager = new CSceneManager();
		m_SceneManager->Load("");
		//CONSTANT BUFFERMANAGER;
		m_ConstantBufferManager = new buffers::CConstantBufferManager();
		//GUI MANAGER;
		m_GUIManager = new gui::CGUIManager();
		m_GUIManager->Load("");
		//prefabs manager
		//m_PrefabsManager = new CPrefabManager();
		//m_PrefabsManager->Load();

		//RENDER
		m_RenderPipelineManager = new render::CRenderPipelineManager();
		m_RenderPipelineManager->Load();
		//Sound
		m_SoundManager = sound::ISoundManager::InstantiateSoundManager();
		m_SoundManager->Init();
		m_SoundManager->Load();
		//IMGUI
		m_ImGui = new CImguiHelper();
		ImGui_ImplDX11_Init(hWnd, GetRenderManager().GetDevice(), GetRenderManager().GetDeviceContext());
		m_ImGui->LoadFont();

		//call start function for all components of the nodes
		m_SceneManager->GetCurrentScene()->LoadNodesComponents();

		m_Coroutine = &base::utils::CCoroutine::GetInstance();

		//check de que haya maincamera
		assert(m_CameraManager->GetMainCamera() != nullptr);
	}

	float CEngine::GetDeltaTime() const
	{
		return m_DeltaTime;
	}
}
