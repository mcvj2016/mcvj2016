#ifndef __H_COMMANDMANAGER__
#define __H_COMMANDMANAGER__
#include "Utils/TemplatedMap.h"
#include "Command.h"
#include "Utils/Singleton.h"

using namespace base::utils;

namespace engine {
	class CCommandManager : public CTemplatedMap<CCommand>, public CSingleton<CCommandManager>
	{
	public:
		CCommandManager();
		~CCommandManager();
		bool ExecCommand(const std::string& commandName);
		void AddCommands();
	};
}

#endif