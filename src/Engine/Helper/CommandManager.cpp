#include "CommandManager.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Components/Enemies/Orc/OrcAnimator.h"
#include "Components/Common/Life.h"
#include "Components/Common/AmbientSoundManager.h"
#include "Graphics/Scenes/PrefabManager.h"
#include "Components/Enemies/EnemyTag.h"
#include "Components/Enemies/Gulak/GulakCombatEnemies.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>
#include "Input/InputManager.h"
#include "Components/Enemies/EnemiesManager.h"
#include "Components/Enemies/Gulak/GulakActivator.h"

namespace engine {

	CCommandManager::CCommandManager()
	{
		AddCommands();
	}

	CCommandManager::~CCommandManager()
	{
	}

	bool CCommandManager::ExecCommand(const std::string& commandName)
	{
		if (Exist(commandName))
		{
			GetByName(commandName)->Exec();
			return true;
		}
		return false;
	}

	void CCommandManager::AddCommands()
	{
		CCommand* cmd = nullptr;

		//play orcboss
		cmd = new CCommand("spawnGulak", []() {
			auto node = (CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("Gulak");
			node->SetEnabled(true);
			node->ShowAnimated();
			CSceneNode *node2 = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers");
			node2->GetComponent<CGulakActivator>()->SetEnabled(false);
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HIT_GULAK);
			CAmbientSoundManager* sm = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")->GetComponent<CAmbientSoundManager>();
			sm->ChangeZone(CAmbientSoundManager::FinalZone);
			CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("VidaGulak")->SetEnabled(true);
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("spawnEnemies", []() {
			auto group1 = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GulakGroup1")->GetComponent<CGulakCombatEnemies>();
			auto group2 = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GulakGroup2")->GetComponent<CGulakCombatEnemies>();
			for (CSceneNode* node : CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNodesByTag(CSceneNode::enemy))
			{
				if (node->GetComponent<CEnemyTag>() != nullptr //added check for ignore cinematic enemies
					&& node->GetComponent<CEnemyTag>()->GetTag() != CEnemyTag::Gulak
					&& node->GetComponent<CEnemyTag>()->GetTag() != CEnemyTag::OrcBoss)
				{
					bool inGulakGroup = false; //find in groups
					for (size_t i = 0; i < group1->GetEnemies().size() && !inGulakGroup; ++i)
					{
						if (group1->GetEnemies().at(i)->GetName() == node->GetName())
						{
							inGulakGroup = true;
						}
					}
					for (size_t i = 0; i < group2->GetEnemies().size() && !inGulakGroup; ++i)
					{
						if (group2->GetEnemies().at(i)->GetName() == node->GetName())
						{
							inGulakGroup = true;
						}
					}

					if (!inGulakGroup)
					{
						CSceneAnimatedModel * enemy = static_cast<CSceneAnimatedModel *>(node);
						enemy->ShowAnimated();
						enemy->SetEnabled(true);
					}
				}
			}
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("MoveGulak", []() {
			CSceneAnimatedModel* node = (CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("Gulak");
			node->SetEnabled(true);
			node->ShowAnimated();
			CEngine::GetInstance().GetPhysXManager().SetCharacterControllerPosition("Gulak", Vect3f(-70.0f, 1.0f, -133.0f));
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("SetCombat", []() {

			auto node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers");
			node->GetComponent<CEnemiesManager>()->AddEnemyActive();
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("deduceLife", []() {
			CSceneAnimatedModel* node = (CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player);
			node->GetComponent<CLife>()->TakeDamage(10);
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("gob9", []() {
			CSceneAnimatedModel* node = (CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("goblin9");
			node->InstantiatePhysx();
			node->SetVisible(true);
			node->SetEnabled(true);
		});
		Add(cmd->GetName(), cmd);
		cmd = new CCommand("gob10", []() {
			CSceneAnimatedModel* node = (CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("goblin10");
			node->InstantiatePhysx();
			node->SetVisible(true);
			node->SetEnabled(true);
		});
		Add(cmd->GetName(), cmd);
		
		

		//play cin 4
		cmd = new CCommand("Cin4", []() {
			CEngine::GetInstance().GetCinematicManager().Stop("cin4");
			CEngine::GetInstance().GetCinematicManager().Play("cin4");
		});
		Add(cmd->GetName(), cmd);

		//play cin 3
		cmd = new CCommand("Cin3", []() {
			CEngine::GetInstance().GetCinematicManager().Stop("cin3");
			CEngine::GetInstance().GetCinematicManager().Play("cin3");
		});
		Add(cmd->GetName(), cmd);

		//play cin 5
		cmd = new CCommand("Cin5", []() {
			CEngine::GetInstance().GetCinematicManager().Stop("cin5");
			CEngine::GetInstance().GetCinematicManager().Play("cin5");
		});
		Add(cmd->GetName(), cmd);

		//play cin 2
		cmd = new CCommand("Cin2", []() {
			CEngine::GetInstance().GetCinematicManager().Stop("cin2");
			CEngine::GetInstance().GetCinematicManager().Play("cin2");
		});
		Add(cmd->GetName(), cmd);

		//play cin 1
		cmd = new CCommand("Cin1", []() {
			CEngine::GetInstance().GetCinematicManager().Stop("cin1");
			CEngine::GetInstance().GetCinematicManager().Play("cin1");
		});
		Add(cmd->GetName(), cmd);

		//play cin 6
		cmd = new CCommand("Cin6", []() {
			CEngine::GetInstance().GetImGUI().Log("Entra cin 6");
			CEngine::GetInstance().GetCinematicManager().Stop("cin6");
			CEngine::GetInstance().GetCinematicManager().Play("cin6");
		});
		Add(cmd->GetName(), cmd);

		//play cin 1
		cmd = new CCommand("BlendP1", []() {
			CSceneNode* node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("orkelf1PosCin1");
			cal3dimpl::CSceneAnimatedModel* model = static_cast<cal3dimpl::CSceneAnimatedModel*>(node);
			model->BlendCycle(0, 0.5f, 0.0f);
		});
		Add(cmd->GetName(), cmd);

		//play cin 1
		cmd = new CCommand("RestLife", []() {
			CSceneNode* node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::player);
			node->GetComponent<CLife>()->TakeDamage(200.0f);
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("AddLife", []() {
			CSceneNode* node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::player);
			node->GetComponent<CLife>()->SetLifePoints(node->GetComponent<CLife>()->GetLifePoints()+30.0f);
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("SetCombatTrack", []() {
			CSceneNode* node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers");
			node->GetComponent<CAmbientSoundManager>()->SetInCombat(true);
		});
		Add(cmd->GetName(), cmd);

		cmd = new CCommand("DisableCombatTrack", []() {
			CSceneNode* node = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers");
			node->GetComponent<CAmbientSoundManager>()->SetInCombat(false);
		});
		Add(cmd->GetName(), cmd);
	}
}
