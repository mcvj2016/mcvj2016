#ifndef __H_COMMAND__
#define __H_COMMAND__
#include "Utils/Name.h"
#include <functional>

typedef std::function<void()> func;
namespace engine {
	class CCommand : public CName
	{
	public:
		CCommand();
		CCommand(const std::string& name, func cb);
		~CCommand();
		void Exec() const;

	private:
		func m_Callback;
	};
}

#endif