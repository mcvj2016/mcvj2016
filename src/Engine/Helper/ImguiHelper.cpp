#include "ImguiHelper.h"
#include "Utils/Imgui/imgui.h"
#include "Utils/CheckedDelete.h"
#include "Utils/Logger/Logger.h"
#include "Engine.h"
#include "Graphics/Textures/TextureManager.h"
#include "Render/RenderManager.h"
#include "Input/ActionManager.h"
#include "Camera/CameraManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Math/Quaternion.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Scenes/SceneLight.h"
#include "Graphics/Particles/ParticleManager.h"
#include "Graphics/Shaders/ShaderManager.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include <algorithm>
#include "Graphics/Cal3d/AnimatedCoreModel.h"
#include "PhysXImpl/PhysXManager.h"
#include "Render/RenderPipelineManager.h"
#include "Utils/Imgui/imgui_internal.h"
#include "CommandManager.h"
#include "Components/Common/CinematicStarter.h"
#include "Render/SetAlphaBlendState.h"

using namespace engine;

bool InfoWindow = false;
bool LogWindow = true;
bool CameraWindow = false;
bool MaterialsWindow = false;
bool SceneNodesWindow = false;
bool SceneLightsWindow = false;
bool RenderPipelineWindow = false;
bool TexturesWindow = false; 
bool ParticlesWindow = false;
bool CinematicsWindow = false;
std::map<int, materials::CTexture*> staticTextures;
std::map<int, render::CDynamicTexture*> dynamicTextures;
std::vector<materials::CMaterial*> techniqueMats;
std::vector<materials::CMaterial*> meshMats;
CCameraController* PrevAnimMainCamera = nullptr;
char InputBuf[256];
std::vector<const char*> Items;
// Portable helpers
static int   Stricmp(const char* str1, const char* str2)         { int d; while ((d = toupper(*str2) - toupper(*str1)) == 0 && *str1) { str1++; str2++; } return d; }
static int   Strnicmp(const char* str1, const char* str2, int n) { int d = 0; while (n > 0 && (d = toupper(*str2) - toupper(*str1)) == 0 && *str1) { str1++; str2++; n--; } return d; }
static char* Strdup(const char *str)                             { size_t len = strlen(str) + 1; void* buff = malloc(len); return (char*)memcpy(buff, (const void*)str, len); }
char clear[10] = "CLEAR";
char help[10] = "HELP";
char history[10] = "HISTORY";
char begin[10] = "- ";

CImguiHelper::CImguiHelper() : m_Log(new ImguiLog())
, m_SavedWindowPadding(ImVec2(0.0f, 0.0f)), m_SavedWindowMinSize(ImVec2(0.0f, 0.0f))
{
	Preload();
	ClearConsoleLog();
	memset(InputBuf, 0, sizeof(InputBuf));
}

void CImguiHelper::Preload()
{
	//load textures for avoiding loading them every frame
	int dynCount = 0;
	int staticCount = 0;
	materials::CTextureManager& texManager = CEngine::GetInstance().GetTextureManager();
	materials::CMaterialManager& matManager = CEngine::GetInstance().GetMaterialManager();

	for (size_t i = 0; i < texManager.GetCount(); ++i)
	{
		if (texManager.GetByIndex(i)->GetDynamic())
		{
			dynamicTextures[dynCount] = static_cast<render::CDynamicTexture*>(texManager.GetByIndex(i));
			dynCount++;
		}
		else
		{
			staticTextures[staticCount] = texManager.GetByIndex(i);
			staticCount++;
		}
	}
	for (size_t i = 0; i < matManager.GetCount(); ++i)
	{
		if (matManager.GetByIndex(i) != nullptr)
		{
			matManager.GetByIndex(i)->m_IsCommon ? techniqueMats.push_back(matManager.GetByIndex(i))
				: meshMats.push_back(matManager.GetByIndex(i));
		}
	}
}

void CImguiHelper::Clear()
{
	dynamicTextures.clear();
	staticTextures.clear();
}

CImguiHelper::~CImguiHelper()
{
	base::utils::CheckedDelete(m_Log);
}

void CImguiHelper::Log(const std::string& entry)
{
	m_Log->AddLog((entry + "\n").c_str());
	LOG_INFO_APPLICATION(" %s", entry.c_str());
}

void CImguiHelper::LogV3f(const Vect3f& entry) const
{
	const std::string line = "Vect3f: (" + std::to_string(entry.x) + ", " + std::to_string(entry.y) + ", " + std::to_string(entry.z) + "\n";
	m_Log->AddLog(line.c_str());
	LOG_INFO_APPLICATION(" %s", line.c_str());
}

bool CImguiHelper::AddButton(const std::string& name) const
{
	return ImGui::Button(name.c_str());
}

bool CImguiHelper::NewWindow(const char* title) const
{
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_AlwaysAutoResize;
	return ImGui::Begin(title, nullptr, window_flags);
}

bool CImguiHelper::NewWindowWithFlags(const char* title, int flag) const
{
	ImGuiWindowFlags window_flags = flag;

	return ImGui::Begin(title, nullptr, window_flags);
}

void CImguiHelper::SetNextWindowPos(float x, float y) const
{
	ImGui::SetNextWindowPos(ImVec2(x, y));
}

void CImguiHelper::SetNextWindowSize(float x, float y) const
{
	ImGui::SetNextWindowSize(ImVec2(x, y), NULL);
}

void CImguiHelper::SetNextWindowPadding(float x, float y)
{
	ImGuiStyle * style = &ImGui::GetStyle();
	m_SavedWindowPadding = style->WindowPadding;
	style->WindowPadding = ImVec2(x, y);
}

void CImguiHelper::SetSavedWindowPadding() const
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->WindowPadding = m_SavedWindowPadding;
}

void CImguiHelper::SetNextWindowMinSize(float x, float y)
{
	ImGuiStyle * style = &ImGui::GetStyle();
	m_SavedWindowMinSize = style->WindowMinSize;
	style->WindowMinSize = ImVec2(x, y);
}

void CImguiHelper::SetSavedWindowMinSze() const
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->WindowMinSize = m_SavedWindowMinSize;
}

void CImguiHelper::SetAlign(float x, float y, float textSize, float fontSize, float lines) const
{
	ImGui::SetCursorPos(ImVec2((x / 2) - (textSize/2), (y / 2) - ((fontSize / 2)*lines)));
}

float CImguiHelper::GetCursorPosX() const
{
	return ImGui::GetCursorPosX();
}

float CImguiHelper::GetCursorPosY() const
{
	return ImGui::GetCursorPosY();
}

int CImguiHelper::RadioButton(const std::string& name, int buttonVal, int button)
{
	ImGui::RadioButton(name.c_str(), &buttonVal, button);
	return buttonVal;
}

void CImguiHelper::DisableWindowBackGroundColor()
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->Colors[ImGuiCol_WindowBg] = ImVec4(.0f, .0f, .0f, .0f);
}

void CImguiHelper::SetWindowBackGroundColor(Vect4f color)
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->Colors[ImGuiCol_WindowBg] = ImVec4(color.x,color.y, color.z, color.w);
}

void CImguiHelper::DisableRoundedWindow()
{
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
}

void CImguiHelper::RestorePushedVar()
{
	ImGui::PopStyleVar(1);
}

bool CImguiHelper::FullScreenWindow(const std::string& name)
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->WindowRounding = 0.0f;
	style->WindowPadding = ImVec2(0.0f,0.0f);
	style->FrameRounding = 0.0f;
	
	// Demonstrate the various window flags. Typically you would just use the default.
	ImGuiWindowFlags window_flags = 0;
	window_flags |= ImGuiWindowFlags_NoTitleBar;
	window_flags |= !ImGuiWindowFlags_ShowBorders;
	window_flags |= ImGuiWindowFlags_NoResize;
	window_flags |= ImGuiWindowFlags_NoMove;
	window_flags |= ImGuiWindowFlags_NoScrollbar;
	window_flags |= ImGuiWindowFlags_NoCollapse;
	window_flags |= ImGuiWindowFlags_NoInputs;
	window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
	window_flags |= ImGuiWindowFlags_NoScrollWithMouse;
	window_flags |= !ImGuiWindowFlags_MenuBar;

	ImGui::SetNextWindowPos(ImVec2(.0f, .0f));
	ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize, ImGuiSetCond_Always);
	return ImGui::Begin(name.c_str(), nullptr, window_flags);
}

void CImguiHelper::Image(const std::string& textureName, float width, float height)
{
	ImGui::Image(CEngine::GetInstance().GetTextureManager().GetByName(textureName)->GetTexture(), ImVec2(width, height));
}

void CImguiHelper::ImageRGBA(const std::string& textureName, float width, float height, const CColor& rgba) const
{
	ImGui::Image(
		CEngine::GetInstance().GetTextureManager().GetByName(textureName)->GetTexture(), 
		ImVec2(width, height),
		ImVec2(0,0), //uv start
		ImVec2(1,1), //uv end
		ImVec4(rgba.x, rgba.y, rgba.z, rgba.w));
}

void CImguiHelper::FullScreenImage(const std::string& textureName)
{
	ImGui::Image(CEngine::GetInstance().GetTextureManager().GetByName(textureName)->GetTexture(), ImGui::GetIO().DisplaySize);
}

void CImguiHelper::FullScreenImageRGBA(const std::string& textureName, const CColor& rgba)
{
	ImGui::Image(
		CEngine::GetInstance().GetTextureManager().GetByName(textureName)->GetTexture(),
		ImGui::GetIO().DisplaySize,
		ImVec2(0, 0), //uv start
		ImVec2(1, 1), //uv end
		ImVec4(rgba.x, rgba.y, rgba.z, rgba.w));
}

bool CImguiHelper::IsItemHovered() const
{
	return ImGui::IsItemHovered();
}

bool CImguiHelper::ImageButton(const std::string& textureName, float width, float height) const
{
	ImGuiStyle * style = &ImGui::GetStyle();
	style->Colors[ImGuiCol_Button] = ImVec4(.0f, .0f, .0f, .0f);
	style->Colors[ImGuiCol_ButtonHovered] = ImVec4(.0f, .0f, .0f, .0f);
	style->Colors[ImGuiCol_ButtonActive] = ImVec4(.0f, .0f, .0f, .0f);

	return ImGui::ImageButton(CEngine::GetInstance().GetTextureManager().GetByName(textureName)->GetTexture(), ImVec2(width, height));
}

void CImguiHelper::EndWindow() const
{
	ImGui::End();
}

void CImguiHelper::AddTextLine(std::string line) const
{
	ImGui::Text(line.c_str());
}

void CImguiHelper::ShowFrameRate() const
{
	ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
}

void CImguiHelper::AddSeparator() const
{
	ImGui::Separator();
}

void CImguiHelper::SetInline(float offset) const
{
	ImGui::SameLine(offset);
}

void CImguiHelper::SetInlineRight(float offset) const
{
	ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - offset);
}

bool CImguiHelper::AddCheckbox(const char* name, bool prop)
{
	ImGui::Checkbox(name, &prop);
	return prop;
}

bool CImguiHelper::BeginMainMenuBar() const
{
	return ImGui::BeginMainMenuBar();
}

void CImguiHelper::EndMainMenuBar() const
{
	ImGui::EndMainMenuBar();
}

bool CImguiHelper::BeginMenu(const char* aName) const
{
	return ImGui::BeginMenu(aName);
}

void CImguiHelper::EndMenu() const
{
	ImGui::EndMenu();
}

bool CImguiHelper::MenuItem(const char* aName, const char* shortcut) const
{
	return ImGui::MenuItem(aName, shortcut);
}

bool CImguiHelper::TreeNode(const char* aName) const
{
	return ImGui::TreeNode(aName);
}

void CImguiHelper::EndTreeNode() const
{
	ImGui::TreePop();
}

void CImguiHelper::DrawLog()
{
	m_Log->Draw("Logs");
}

float CImguiHelper::DragFloat(const char* aName, float f, float speed) const
{
	ImGui::DragFloat(aName, &f, speed);
	return f;
}

Vect2f CImguiHelper::DragFloat2(const char* aName, Vect2f f, float speed) const
{
	float Vect2Temp[2] = { f.x, f.y };
	ImGui::DragFloat2(aName, Vect2Temp, speed);
	f.x = Vect2Temp[0];
	f.y = Vect2Temp[1];
	return f;
}

Vect3f CImguiHelper::DragFloat3(const char* aName, Vect3f f, float speed) const
{
	float Vect3Temp[3] = { f.x, f.y, f.z };
	ImGui::DragFloat3(aName, Vect3Temp, speed);
	f.x = Vect3Temp[0];
	f.y = Vect3Temp[1];
	f.z = Vect3Temp[2];
	return f;
}

int CImguiHelper::DragInt(const char* aName, int i, float speed) const
{
	ImGui::DragInt(aName, &i, speed);
	return i;
}

int CImguiHelper::DragIntMinMax(const char* aName, int i, float speed, int min, int max) const
{
	ImGui::DragInt(aName, &i, speed, min, max);
	return i;
}

float CImguiHelper::DragFloatMinMax(const char* aName, float f, float speed, float min, float max) const
{
	ImGui::DragFloat(aName, &f, speed, min, max);
	return f;
}

Vect4f CImguiHelper::ColorEdit4(const char* name, float r, float g, float b, float a)
{
	float lColor[4] = { r, g, b, a };
	ImGui::ColorEdit4(name, lColor);
	return Vect4f(lColor[0], lColor[1], lColor[2], lColor[3]);
}

void CImguiHelper::SetMainFontColor()
{
	ImGuiStyle& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Text] = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);
}

void CImguiHelper::SetDefaultFontColor()
{
	ImGuiStyle& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Text] = ImVec4(0.90f, 0.90f, 0.90f, 0.90f);
}

Vect3f CImguiHelper::InputFloat3(std::string name, Vect3f vec)
{
	float v[3] = { vec.x, vec.y, vec.z };
	ImGui::InputFloat3(name.c_str(), v);

	return Vect3f(v[0], v[1], v[2]);
}

void CImguiHelper::ImGuiRender()
{
	ImGui::Render();
}

bool CImguiHelper::IsLastItemActive()
{
	return ImGui::IsItemActive();
}

void CImguiHelper::ClearConsoleLog()
{
	for (size_t i = 0; i < Items.size(); i++)
	{
		Items.clear();
	}
}

void CImguiHelper::DrawConsole()
{
	if (NewWindow("Console"))
	{
		// Command-line
		if (ImGui::InputText("Command", InputBuf, IM_ARRAYSIZE(InputBuf), ImGuiInputTextFlags_EnterReturnsTrue))
		{
			char* input_end = InputBuf + strlen(InputBuf);
			while (input_end > InputBuf && input_end[-1] == ' ') input_end--; *input_end = 0;
			if (InputBuf[0])
			{
				ExecCommand(InputBuf);
			}

			strcpy(InputBuf, "");
		}
		EndWindow();
	}
}

void CImguiHelper::ExecCommand(const char* command_line)
{
	Log("# " + std::string(command_line));
	CCommandManager& lCmdManager = CCommandManager::GetInstance();

	// Process command
	if (Stricmp(command_line, clear) == 0)
	{
		ClearConsoleLog();
		this->m_Log->Clear();
	}
	else if (Stricmp(command_line, help) == 0)
	{
		Log("Commands:");
		Log(begin + std::string(clear));
		Log(begin + std::string(help));
		for (size_t i = 0; i < lCmdManager.GetCount(); ++i)
		{
			Log(begin + CCommandManager::GetInstance().GetByIndex(i)->GetName());
		}
	}
	else
	{
		if (!CCommandManager::GetInstance().ExecCommand(command_line))
		{
			Log("Unknown command: " + std::string(command_line));
		}
	}
}

void CImguiHelper::DrawDebug()
{
	DrawMainMenu();
	DrawWindows();
	ShowFrameRate();
}

void CImguiHelper::DrawWindows()
{
	DrawLogWindow();
	DrawActionsInfoWindow();
	DrawCameraInfoWindow();
	DrawMaterialsWindow();
	DrawSceneNodesWindow();
	DrawSceneLightsWindow();
	DrawRenderPipelineWindow();
	DrawTexturesWindow();
	DrawParticlesWindow();
	DrawCinematicsWindow();
	DrawConsole();
}

void CImguiHelper::DrawCinematicsWindow()
{
	if (CinematicsWindow)
	{
		cinematics::CCinematicManager& lCinManager = CEngine::GetInstance().GetCinematicManager();
		ImGui::Begin("Cinematics", &CinematicsWindow);
		AddSeparator();
		for (size_t i = 0; i < lCinManager.GetCount(); ++i)
		{
			const std::string lAnim = lCinManager.GetByIndex(i)->GetName();
			if (TreeNode(lAnim.c_str()))
			{
				if (AddButton("Play"))
				{
					PrevAnimMainCamera = CEngine::GetInstance().GetCameraManager().GetMainCamera();
					auto gob = (cal3dimpl::CSceneAnimatedModel*)CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("goblin");
					gob->BlendCycle(0, 1.0f, 0.0f);
					gob->SetVisible(true);
					lCinManager.Play(lAnim);
				}
				SetInline(0);
				if (AddButton("Stop"))
				{
					CEngine::GetInstance().GetCameraManager().SetMainCamera(PrevAnimMainCamera);
					lCinManager.Stop(lAnim);
				}
				EndTreeNode();
			}
		}

		EndWindow();
	}
}

void CImguiHelper::DrawCameraInfoWindow()
{
	if (CameraWindow){
		CCameraController& mainCam = *CEngine::GetInstance().GetCameraManager().GetMainCamera();
		ImGui::Begin("Camera", &CameraWindow);
		AddTextLine("Camera: " + mainCam.GetSceneCamera()->GetName());
		AddSeparator();
		bool lock = AddCheckbox("Lock", mainCam.GetLock());
		mainCam.SetLock(lock);
		AddSeparator();
		Vect3f camPos = mainCam.GetSceneCamera()->m_Position;
		AddTextLine("Position");
		AddSeparator();
		AddTextLine(std::string("X: " + std::to_string(camPos.x)));
		AddTextLine(std::string("Y: " + std::to_string(camPos.y)));
		AddTextLine(std::string("Z: " + std::to_string(camPos.z)));
		AddSeparator();
		AddTextLine("Rotation");
		AddSeparator();
		AddTextLine(std::string("X: " + std::to_string(mainCam.GetSceneCamera()->GetYaw())));
		AddTextLine(std::string("Y: " + std::to_string(mainCam.GetSceneCamera()->GetPitch())));
		AddTextLine(std::string("Z: " + std::to_string(mainCam.GetSceneCamera()->GetRoll())));
		AddTextLine("INVENTARIO");
		/*logic::components::CInventory* inventory = CEngine::GetInstance().GetSceneManager().GetCurrentScene()
			->GetSceneNode("Inventario")->GetComponent<logic::components::CInventory>();
		inventory->m_inventoryPosX = DragFloat("INVETORY_POS_X", inventory->m_inventoryPosX, 0.1f);
		inventory->m_inventoryPosY = DragFloat("INVETORY_POS_Y", inventory->m_inventoryPosY, 0.1f);
		inventory->m_inventorySizeX = DragFloat("INVENTORY_SIZE_X", inventory->m_inventorySizeX, 0.1f);
		inventory->m_inventorySizeY = DragFloat("INVENTORY_SIZE_Y", inventory->m_inventorySizeY, 0.1f);
		inventory->m_helmPosX = DragFloat("HELM_POS_X", inventory->m_helmPosX, 0.1f);
		inventory->m_helmPosY = DragFloat("HELM_POS_Y", inventory->m_helmPosY, 0.1f);
		inventory->m_helmSizeX = DragFloat("HELM_SIZE_X", inventory->m_helmSizeX, 0.1f);
		inventory->m_helmSizeY = DragFloat("HELM_SIZE_Y", inventory->m_helmSizeY, 0.1f);
		inventory->m_keyPosX = DragFloat("KEY_POS_X", inventory->m_keyPosX, 0.1f);
		inventory->m_keyPosY = DragFloat("KEY_POS_Y", inventory->m_keyPosY, 0.1f);
		inventory->m_keySizeX = DragFloat("KEY_SIZE_X", inventory->m_keySizeX, 0.1f);
		inventory->m_keySizeY = DragFloat("KEY_SIZE_Y", inventory->m_keySizeY, 0.1f);
		inventory->m_bucketPosX = DragFloat("BUCKET_POS_X", inventory->m_bucketPosX, 0.1f);
		inventory->m_bucketPosY = DragFloat("BUCKET_POS_Y", inventory->m_bucketPosY, 0.1f);
		inventory->m_bucketSizeX = DragFloat("BUCKET_SIZE_X", inventory->m_bucketSizeX, 0.1f);
		inventory->m_bucketSizeY = DragFloat("BUCKET_SIZE_Y", inventory->m_bucketSizeY, 0.1f);
		inventory->m_chestPosX = DragFloat("CHEST_POS_X", inventory->m_chestPosX, 0.1f);
		inventory->m_chestPosY = DragFloat("CHEST_POS_Y", inventory->m_chestPosY, 0.1f);
		inventory->m_chestSizeX = DragFloat("CHEST_SIZE_X", inventory->m_chestSizeX, 0.1f);
		inventory->m_chestSizeY = DragFloat("CHEST_SIZE_Y", inventory->m_chestSizeY, 0.1f);
		inventory->m_ropePosX = DragFloat("ROPE_POS_X", inventory->m_ropePosX, 0.1f);
		inventory->m_ropePosY = DragFloat("ROPE_POS_Y", inventory->m_ropePosY, 0.1f);
		inventory->m_ropeSizeX = DragFloat("ROPE_SIZE_X", inventory->m_ropeSizeX, 0.1f);
		inventory->m_ropeSizeY = DragFloat("ROPE_SIZE_Y", inventory->m_ropeSizeY, 0.1f);
		inventory->m_bootsPosX = DragFloat("BOOTS_POS_X", inventory->m_bootsPosX, 0.1f);
		inventory->m_bootsPosY = DragFloat("BOOTS_POS_Y", inventory->m_bootsPosY, 0.1f);
		inventory->m_bootsSizeX = DragFloat("BOOTS_SIZE_X", inventory->m_bootsSizeX, 0.1f);
		inventory->m_bootsSizeY = DragFloat("BOOTS_SIZE_Y", inventory->m_bootsSizeY, 0.1f);
		AddTextLine("LIFE BAR DATA");
		logic::components::CLifeBar* lifeBar = CEngine::GetInstance().GetSceneManager().GetCurrentScene()
			->GetSceneNode("Vida")->GetComponent<logic::components::CLifeBar>();
		lifeBar->m_windowPosX = DragFloat("WINDOW_POS_X", lifeBar->m_windowPosX, 1.0f);
		lifeBar->m_windowPosY = DragFloat("WINDOW_POS_Y", lifeBar->m_windowPosY, 1.0f);
		lifeBar->m_windowSizeX = DragFloat("WINDOW_SIZE_X", lifeBar->m_windowSizeX, 1.0f);
		lifeBar->m_windowSizeY = DragFloat("WINDOW_SIZE_Y", lifeBar->m_windowSizeY, 1.0f);
		lifeBar->m_barPosX = DragFloat("BAR_POS_X", lifeBar->m_barPosX, 1.0f);
		lifeBar->m_barPosY = DragFloat("BAR_POS_Y", lifeBar->m_barPosY, 1.0f);
		lifeBar->m_barSizeX = DragFloat("BAR_SIZE_X", lifeBar->m_barSizeX, 1.0f);
		if (lifeBar->m_barSizeX<0.1)
		{
			lifeBar->m_barSizeX = 0.1f;
		}
		lifeBar->m_barSizeY = DragFloat("BAR_SIZE_Y", lifeBar->m_barSizeY, 1.0f);
		lifeBar->m_paddingX = DragFloat("BAR_PADDING_X", lifeBar->m_paddingX, 1.0f);
		lifeBar->m_paddingY = DragFloat("BAR_PADDING_Y", lifeBar->m_paddingY, 1.0f);
		lifeBar->m_minSizeX = DragFloat("BAR_MIN_SIZE_X", lifeBar->m_minSizeX, 1.0f);
		lifeBar->m_minSizeY = DragFloat("BAR_MIN_SIZE_Y", lifeBar->m_minSizeY, 1.0f);
		lifeBar->m_barImageSizeX = DragFloat("BAR_IMAGE_SIZE_X", lifeBar->m_barImageSizeX, 1.0f);
		lifeBar->m_barImageSizeY = DragFloat("BAR_IMAGE_SIZE_Y", lifeBar->m_barImageSizeY, 1.0f);*/

		/*AddTextLine("Enemy LIFE BAR DATA");
		logic::components::CEnemyLifeBar* enemyLifeBar = CEngine::GetInstance().GetSceneManager().GetCurrentScene()
			->GetSceneNode("VidaEnemigo")->GetComponent<logic::components::CEnemyLifeBar>();
		enemyLifeBar->m_windowPosX = DragFloat("ENEMYBAR_POS_X", enemyLifeBar->m_windowPosX, 1.0f);
		enemyLifeBar->m_windowPosY = DragFloat("ENEMYBAR_POS_Y", enemyLifeBar->m_windowPosY, 1.0f);
		enemyLifeBar->m_windowSizeX = DragFloat("ENEMYBAR_SIZE_X", enemyLifeBar->m_windowSizeX, 1.0f);
		if (enemyLifeBar->m_windowSizeX<0.1)
		{
			enemyLifeBar->m_windowSizeX = 0.1f;
		}
		enemyLifeBar->m_windowSizeY = DragFloat("ENEMYBAR_SIZE_Y", enemyLifeBar->m_windowSizeY, 1.0f);
		enemyLifeBar->m_paddingX = DragFloat("ENEMYBAR_PADDING_X", enemyLifeBar->m_paddingX, 1.0f);
		enemyLifeBar->m_paddingY = DragFloat("ENEMYBAR_PADDING_Y", enemyLifeBar->m_paddingY, 1.0f);
		enemyLifeBar->m_minSizeX = DragFloat("ENEMYBAR_MIN_SIZE_X", enemyLifeBar->m_minSizeX, 1.0f);
		enemyLifeBar->m_minSizeY = DragFloat("ENEMYBAR_MIN_SIZE_Y", enemyLifeBar->m_minSizeY, 1.0f);*/

		AddSeparator();
		AddTextLine("ALPHA STATE");
		for (render::CRenderCmd* cmd : CEngine::GetInstance().GetRenderPipelineManager().GetActivePipeline().GetResourcesVector())
		{
			if (cmd->GetName() == "LightScattering_Alpha_Blend")
			{
		render::CSetAlphaBlendState* alpha_blend_state = static_cast<render::CSetAlphaBlendState*>(cmd);
		int operation = alpha_blend_state->m_OpBlend.GetBlendOp();
		int operation_alpha = alpha_blend_state->m_OpAlphaBlend.GetBlendOp();
		int src = alpha_blend_state->m_SrcBlend.GetBlend();
		int dest = alpha_blend_state->m_DestBlend.GetBlend();
		int src_alpha = alpha_blend_state->m_SrcAlphaBlend.GetBlend();
		int dest_alpha = alpha_blend_state->m_DestAlphaBlend.GetBlend();

		ImGui::InputInt("operation", &operation);
		ImGui::InputInt("operation_alpha", &operation_alpha);
		ImGui::InputInt("src", &src);
		ImGui::InputInt("dest", &dest);
		ImGui::InputInt("src_alpha", &src_alpha);
		ImGui::InputInt("dest_alpha", &dest_alpha);
		
		if (operation != alpha_blend_state->m_OpBlend.GetBlendOp() ||
			operation_alpha != alpha_blend_state->m_OpAlphaBlend.GetBlendOp() ||
			src != alpha_blend_state->m_SrcBlend.GetBlend() ||
			dest != alpha_blend_state->m_DestBlend.GetBlend() ||
			src_alpha != alpha_blend_state->m_SrcAlphaBlend.GetBlend() ||
			dest_alpha != alpha_blend_state->m_DestAlphaBlend.GetBlend())
		{
			alpha_blend_state->m_OpBlend.m_BlendOp = operation;
			alpha_blend_state->m_OpAlphaBlend.m_BlendOp = operation_alpha;
			alpha_blend_state->m_SrcBlend.m_Blend = src;
			alpha_blend_state->m_DestBlend.m_Blend = dest;
			alpha_blend_state->m_SrcAlphaBlend.m_Blend = src_alpha;
			alpha_blend_state->m_DestAlphaBlend.m_Blend = dest_alpha;
			D3D11_BLEND_DESC l_desc = {};
			l_desc.RenderTarget[0].BlendEnable = true;
			l_desc.RenderTarget[0].SrcBlend = D3D11_BLEND(src);
			l_desc.RenderTarget[0].DestBlend = D3D11_BLEND(dest);
			l_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP(operation);
			l_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND(src_alpha);
			l_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND(dest_alpha);
			l_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP(operation_alpha);
			l_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			ID3D11BlendState *l_AlphaBlendState;
			ID3D11Device* l_Device = CEngine::GetInstance().GetRenderManager().GetDevice();

			HRESULT l_HR = l_Device->CreateBlendState(&l_desc, &l_AlphaBlendState);
			assert(l_HR == S_OK);
			alpha_blend_state->m_AlphaBlendState = l_AlphaBlendState;
		}

			}

		}

		EndWindow();
	}
}

void CImguiHelper::DrawActionsInfoWindow()
{
	if (InfoWindow){
		/*
		Log("MoveRightLeft: " + std::to_string(CEngine::GetInstance().GetActionManager().GetInputActionValue("MoveRightLeft")));
		Log("MoveForward: " + std::to_string(CEngine::GetInstance().GetActionManager().GetInputActionValue("MoveForward")));
		Log("CamRotateX: " + std::to_string(CEngine::GetInstance().GetActionManager().GetInputActionValue("CamRotateX")));
		Log("CamRotateY: " + std::to_string(CEngine::GetInstance().GetActionManager().GetInputActionValue("CamRotateY")));
		*/
		
		ImGui::Begin("Actions", &InfoWindow);
		float mouseSenssibility = DragFloat("Mouse Sensibility", CEngine::GetInstance().GetActionManager().GetMouseAxisSensibility(), 0.05f);
		CEngine::GetInstance().GetActionManager().SetMouseAxisSensibility(mouseSenssibility);
		float gamepadSensibility = DragFloat("Gamepad Sensibility", CEngine::GetInstance().GetActionManager().GetGamepadAxisSensibility(), 0.05f);
		CEngine::GetInstance().GetActionManager().SetGamepadAxisSensibility(gamepadSensibility);
		AddCheckbox("Attack", CEngine::GetInstance().GetActionManager().GetInputActionValue("Attack") != 0);
		AddCheckbox("StrongAttack", CEngine::GetInstance().GetActionManager().GetInputActionValue("StrongAttack") != 0);
		AddCheckbox("Evade", CEngine::GetInstance().GetActionManager().GetInputActionValue("Evade") != 0);
		AddCheckbox("Action", CEngine::GetInstance().GetActionManager().GetInputActionValue("Action") != 0);
		AddCheckbox("Pause", CEngine::GetInstance().GetActionManager().GetInputActionValue("Pause") != 0);
		AddCheckbox("StartAiming", CEngine::GetInstance().GetActionManager().GetInputActionValue("StartAiming") != 0);
		AddCheckbox("Aiming", CEngine::GetInstance().GetActionManager().GetInputActionValue("Aiming") != 0);
		AddCheckbox("AimingEnd", CEngine::GetInstance().GetActionManager().GetInputActionValue("AimingEnd") != 0);
		AddCheckbox("Perception", CEngine::GetInstance().GetActionManager().GetInputActionValue("Perception") != 0);
		AddCheckbox("GodMode", CEngine::GetInstance().GetActionManager().GetInputActionValue("GodMode") != 0);
		EndWindow();
	}
}

void CImguiHelper::DrawMaterialsWindow()
{
	if (MaterialsWindow){
		ImGui::Begin("Materials", &MaterialsWindow);
		//save the xml
		if (AddButton("Save XML"))
		{
			Log("Saving Materials XML");
			CEngine::GetInstance().GetMaterialManager().Save();
		}
		AddSeparator();

		static char buf[32] = "";
		ImGui::InputText("Filter", buf, 32);
		AddSeparator();

		//separate by technique and mesh mats
		if (TreeNode("Mesh Materials"))
		{
			//mesh mats
			for (size_t i = 0; i < meshMats.size(); ++i)
			{
				if (strlen(buf) > 0)
				{
					std::string iName = meshMats[i]->GetName();
					transform(meshMats[i]->GetName().begin(), meshMats[i]->GetName().end(), iName.begin(), ::tolower);
					std::string iFilter = buf;
					transform(iFilter.begin(), iFilter.end(), iFilter.begin(), ::tolower);
					size_t found = iName.find(iFilter);
					if (found != std::string::npos)
					{
						DrawMat(meshMats.at(i));
					}
				}
				else
				{
					DrawMat(meshMats.at(i));
				}
			}
			EndTreeNode();
		}

		//technique mats mats
		if (TreeNode("Effects Materials"))
		{
			for (size_t i = 0; i < techniqueMats.size(); ++i)
			{
				if (strlen(buf) > 0)
				{
					std::string iName = techniqueMats[i]->GetName();
					transform(techniqueMats[i]->GetName().begin(), techniqueMats[i]->GetName().end(), iName.begin(), ::tolower);
					std::string iFilter = buf;
					transform(iFilter.begin(), iFilter.end(), iFilter.begin(), ::tolower);
					size_t found = iName.find(iFilter);
					if (found != std::string::npos)
					{
						DrawMat(techniqueMats.at(i));
					}
				}
				else
				{
					DrawMat(techniqueMats.at(i));
				}
			}
			EndTreeNode();
		}

		EndWindow();
	}
}

void CImguiHelper::DrawMat(materials::CMaterial* mat)
{
	//check de mats dinamicos que son nulos
	if (mat != nullptr)
	{
		if (TreeNode(mat->GetName().c_str()))
		{
			AddTextLine("Material Parameters");
			AddSeparator();
			auto params = mat->GetParameters();
			for (size_t j = 0; j < params.size(); ++j)
			{
				auto param = params.at(j);

				if (params.at(j)->GetName() == "enabled")
				{
					CTemplatedMaterialParameter<float>* p = reinterpret_cast<CTemplatedMaterialParameter<float>*>(param);
					bool enabled = true;
					if (p->GetValue() == 1)
					{
						enabled = AddCheckbox("Enabled", true);
					}
					else
					{
						enabled = AddCheckbox("Enabled", false);
					}
					if (enabled)
					{
						p->SetValue(1);
					}
					else
					{
						p->SetValue(0);
					}
				}
				else
				{
					if (param->GetType() == materials::CMaterial::eFloat)
					{
						CTemplatedMaterialParameter<float>* p = reinterpret_cast<CTemplatedMaterialParameter<float>*>(param);
						p->SetValue(DragFloat(p->GetName().c_str(), p->GetValue(), 0.05f));
					}
					if (param->GetType() == materials::CMaterial::eFloat2)
					{
						CTemplatedMaterialParameter<Vect2f>* p = reinterpret_cast<CTemplatedMaterialParameter<Vect2f>*>(param);
						p->SetValue(DragFloat2(p->GetName().c_str(), p->GetValue(), 0.05f));
					}
					if (param->GetType() == materials::CMaterial::eFloat3)
					{
						CTemplatedMaterialParameter<Vect3f>* p = reinterpret_cast<CTemplatedMaterialParameter<Vect3f>*>(param);
						p->SetValue(DragFloat3(p->GetName().c_str(), p->GetValue(), 0.05f));
					}
					if (param->GetType() == materials::CMaterial::eColor)
					{
						CTemplatedMaterialParameter<CColor>* p = reinterpret_cast<CTemplatedMaterialParameter<CColor>*>(param);
						Vect4f lColor = ColorEdit4("Color", p->GetValue().x, p->GetValue().y, p->GetValue().z, p->GetValue().w);
						p->SetValue(CColor(lColor));
					}
				}
			}
			EndTreeNode();
		}
	}
}

void CImguiHelper::DrawSceneNode(CSceneNode* node, const std::string& layerName)
{
	if (TreeNode(node->GetName().c_str()))
	{

		node->SetEnabled(AddCheckbox("Enabled", node->GetEnabled()));
		node->SetVisible(AddCheckbox("Visible", node->IsVisible()));
		Vect3f nodePos = node->m_Position;
		Vect3f nodeRot = Vect3f(node->GetYaw(), node->GetPitch(), node->GetRoll());

		//physx
		if (node->m_hasPhysx)
		{
			AddTextLine("PHYSX");
			AddSeparator();
			CPhysXManager& physxManager = CEngine::GetInstance().GetPhysXManager();
			Vect3f v = v3fZERO;
			Quatf q = qfIDENTITY;
			physxManager.GetActorTransform(node->GetName(), v, q);
			//CTransform* t = new CTransform(v, q.GetYaw(), q.GetPitch(), q.GetRoll());
			Vect3f positionPhysx = DragFloat3("Position Physx", v, 0.01f);

			if (positionPhysx.x != nodePos.x || positionPhysx.y != nodePos.y || positionPhysx.z != nodePos.z)
			{
			//physxManager.SetActorPosition(node->GetName(), positionPhysx);
			}
			AddSeparator();
			Vect3f rotation = Vect3f(q.GetYaw(), q.GetPitch(), q.GetRoll());
			rotation = DragFloat3("Rotation Physx", rotation, 0.01f);
			//if (rotation.x != nodeRot.x || rotation.y != nodeRot.y || rotation.z != nodeRot.z)
			//{
			physxManager.SetActorRotation(node->GetName(), rotation.x, rotation.y, rotation.z);
			//physxManager.SetActorRotation(node->GetName(), node->m_Yaw, node->m_Pitch, node->m_Roll);
			//}
			AddTextLine("Equivalencia Quat");
			float a = DragFloat("Quat X", q.x, 0.01f);
			float b = DragFloat("Quat Y", q.y, 0.01f);
			float c = DragFloat("Quat Z", q.z, 0.01f);
			float d = DragFloat("Quat W", q.w, 0.01f);
			AddSeparator();
			Vect3f size = DragFloat3("Size Physx", node->m_Size, 0.01f);
			node->m_Size = size;
			if (AddButton(std::string("Resize Box")))
			{
				physxManager.RemoveActor(node->GetName());
				physxManager.CreateBox(node->GetName(), "default", positionPhysx, q, size, node->m_IsStatic, node->m_IsTrigger, true);
			}
			AddSeparator();
			float radius = DragFloat("Raidus Physx", node->m_Radius, 0.01f);
			node->m_Radius = radius;
			if (AddButton(std::string("Resize Sphere")))
			{
				physxManager.RemoveActor(node->GetName());
				physxManager.CreateSphere(node->GetName(), "default", positionPhysx, q, radius, node->m_IsStatic, node->m_IsTrigger, true);
			}
			AddSeparator();
		}
		//engine
		AddTextLine("Engine");
		AddSeparator();
		Vect3f pos = DragFloat3("Position Engine", nodePos, 0.01f);

		if (pos.x != nodePos.x || pos.y != nodePos.y || pos.z != nodePos.z)
		{
			node->m_Position = pos;
		}

		AddSeparator();
		AddTextLine("Rotation Engine");
		float yawDeg = mathUtils::Rad2Deg(node->GetYaw());
		float yaw = DragFloat("Yaw", yawDeg, 0.05f);
		if (yaw != yawDeg)
		{
			node->SetYaw(mathUtils::Deg2Rad(yaw));
		}
		float pitchDeg = mathUtils::Rad2Deg(node->GetPitch());
		float pitch = DragFloat("Pitch", pitchDeg, 0.05f);
		if (pitch != pitchDeg)
		{
			node->SetPitch(mathUtils::Deg2Rad(pitch));
		}
		float rollDeg = mathUtils::Rad2Deg(node->GetRoll());
		float roll = DragFloat("Roll", rollDeg, 0.05f);
		if (roll != rollDeg)
		{
			node->SetRoll(mathUtils::Deg2Rad(roll));
		}

		Quatf rotationQuad;
		rotationQuad.QuatFromYawPitchRoll(node->GetYaw(), node->GetPitch(), node->GetRoll());
		AddTextLine("Equivalencia del YAW PITCH ROLL");
		float a = DragFloat("Quat From EULER X", rotationQuad.x, 0.01f);
		float b = DragFloat("Quat From EULER Y", rotationQuad.y, 0.01f);
		float c = DragFloat("Quat From EULER Z", rotationQuad.z, 0.01f);
		float d = DragFloat("Quat From EULER W", rotationQuad.w, 0.01f);

		AddTextLine("Quat Rotation from XML");
		float qx = DragFloat("Quat X", node->GetRotation().x, 0.01f);
		float qy = DragFloat("Quat Y", node->GetRotation().y, 0.01f);
		float qz = DragFloat("Quat Z", node->GetRotation().z, 0.01f);
		float qw = DragFloat("Quat W", node->GetRotation().w, 0.01f);

		node->SetRotation(rotationQuad);

		AddSeparator();
		Vect3f scale = Vect3f(node->m_Scale.x, node->m_Scale.y, node->m_Scale.z);
		scale = DragFloat3("Scale", scale, 0.01f);
		node->SetScale(scale);

		AddSeparator();


		//animated models
		if (layerName == "animated_models")
		{
			AddTextLine("Animations");
			AddSeparator();
			auto animatedNode = static_cast<cal3dimpl::CSceneAnimatedModel*>(node);
			auto lCoreModel = animatedNode->GetAnimatedCoreModel()->GetCoreModel();
			for (size_t k = 0; k < static_cast<size_t>(lCoreModel->getCoreAnimationCount()); ++k)
			{
				auto lAnimation = lCoreModel->getCoreAnimation(k);
				AddTextLine(lAnimation->getName());
				SetInline(0);
				if (AddButton(std::string("Play " + lAnimation->getName())))
				{
					int actualAnim = lCoreModel->getCoreAnimationId(lAnimation->getName());
					animatedNode->ClearCycle(actualAnim, 0);
					animatedNode->BlendCycle(k, 1, 0);
				}
				AddSeparator();
			}
		}
		EndTreeNode();
	}
}

void CImguiHelper::DrawSceneNodesWindow()
{
	if (SceneNodesWindow){

		ImGui::Begin("Scene", &SceneNodesWindow);

		static char buf[32] = "";
		ImGui::InputText("Filter", buf, 32);
		AddSeparator();

		scenes::CScene* scene = CEngine::GetInstance().GetSceneManager().GetCurrentScene();
		for (size_t i = 0; i < scene->GetCount(); ++i)
		{
			scenes::CLayer* layer = scene->GetByIndex(i);
			const std::string& layerName = layer->GetName();

			//show hide layer render
			if (TreeNode(layer->GetName().c_str()))
			{
				AddSeparator();
				bool layerActive = layer->IsActive();
				if (layerActive)
				{
					if (AddButton("Hide"))
					{
						Log(std::string("Setting layer " + layerName + " inactive"));
						layer->SetActive(false);
					}
				}
				else
				{
					if (AddButton("Show"))
					{
						Log(std::string("Setting layer " + layerName + " active"));
						layer->SetActive(true);
					}
				}
				AddSeparator();

				for (size_t j = 0; j < layer->GetCount(); ++j)
				{
					scenes::CSceneNode* node = layer->GetByIndex(j);
					if (strlen(buf) > 0)
					{
						std::string iName = node->GetName();
						transform(node->GetName().begin(), node->GetName().end(), iName.begin(), ::tolower);
						std::string iFilter = buf;
						transform(iFilter.begin(), iFilter.end(), iFilter.begin(), ::tolower);
						size_t found = iName.find(iFilter);
						if (found != std::string::npos)
						{
							
							DrawSceneNode(node, layerName);
						}
					}
					else
					{
						DrawSceneNode(node, layerName);
					}

				}
				EndTreeNode();
			}
		}

		EndWindow();
	}
}

void CImguiHelper::DrawSceneLightsWindow()
{
	if (SceneLightsWindow){
		ImGui::Begin("Lights", &SceneLightsWindow);
		lights::CLightManager& lightManager = CEngine::GetInstance().GetLightManager();


		for (size_t i = 0; i < lightManager.GetCount(); ++i)
		{
			lights::CLight* light = lightManager.GetByIndex(i);
			scenes::CSceneLight* sceneLight = light->GetSceneLight();
			if (TreeNode(light->GetName().c_str()))
			{
				//enabled
				light->SetEnabled(AddCheckbox("Enabled", light->IsEnabled()));
				//intensity
				light->SetIntensity(DragFloatMinMax("Intensity", light->GetIntensity(), 0.05f, .0f, 1.0f));
				//position
				AddSeparator();
				sceneLight->m_Position = InputFloat3("Position", sceneLight->m_Position);
				//color
				AddSeparator();
				Vect4f lColor = ColorEdit4("Color", light->GetColor().x, light->GetColor().y, light->GetColor().z, light->GetColor().w);
				light->SetColor(CColor(lColor));
				
				AddTextLine("Direction");
				Vect3f forward = sceneLight->GetForward();
				forward.x = DragFloat("X", forward.x, 0.05f);
				forward.y = DragFloat("Y", forward.y, 0.05f);
				forward.z = DragFloat("Z", forward.z, 0.05f);
				sceneLight->SetForward(forward);
				//types
				if (light->GetType() == lights::CLight::eSpot)
				{
					light->SetAngle(DragFloat("Angle", light->GetAngle(), 0.05f));
					light->SetFallOff(DragFloat("FallOff", light->GetFallOff(), 0.05f));
				}
				if (light->GetType() == lights::CLight::ePoint)
				{
					light->SetRangeAttenuation(DragFloat2("Att. Range", light->GetRangeAttenuation(), 0.05f));
				}
				
				EndTreeNode();
			}
		}

		EndWindow();
	}
}

void CImguiHelper::DrawRenderPipelineWindow()
{
	if (RenderPipelineWindow){
		ImGui::Begin("Render Pipelines", &RenderPipelineWindow);
		render::CRenderPipelineManager& pipManager = CEngine::GetInstance().GetRenderPipelineManager();
		render::CRenderPipeline& actualPipeline = pipManager.GetActivePipeline();
		
		if (TreeNode("Render Pipelines"))
		{
			for (size_t i = 0; i < pipManager.GetCount(); ++i)
			{
				render::CRenderPipeline* pipeline = pipManager.GetByIndex(i);
				if (AddButton(std::string("Render with pipeline " + pipeline->GetName())))
				{
					Log(std::string("Changing render pipeline to " + pipeline->GetName()));
					//reload actual scene
					CEngine::GetInstance().GetSceneManager().ChangeScene(CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetName());
					pipManager.SetActivePipeline(pipeline->GetName());
				}
				
			}
			EndTreeNode();
		}

		if (TreeNode(actualPipeline.GetName().c_str()))
		{
			AddSeparator();
			if (TreeNode("Commands"))
			{
				for (size_t i = 0; i < actualPipeline.GetCount(); ++i)
				{
					render::CRenderCmd* command = actualPipeline.GetByIndex(i);
					AddTextLine(command->GetName());
					if (command->IsActive())
					{
						if (AddButton(std::string("Disable " + command->GetName())))
						{
							command->SetActive(false);
						}
					}
					else
					{
						if (AddButton(std::string("Enable" + command->GetName())))
						{
							command->SetActive(true);
						}
					}
					AddSeparator();
				}

				EndTreeNode();
			}

			EndTreeNode();
		}

		EndWindow();
	}
}

void CImguiHelper::DrawTexturesWindow()
{
	if (TexturesWindow){
		ImGui::Begin("Textures", &TexturesWindow);
		static char buf[32] = "";
		ImGui::InputText("Filter", buf, 32);

		AddSeparator();
		if (TreeNode("Dynamic"))
		{
			for (size_t i = 0; i < dynamicTextures.size(); ++i)
			{
				if (strlen(buf) > 0)
				{
					std::string iName = dynamicTextures[i]->GetName();
					transform(dynamicTextures[i]->GetName().begin(), dynamicTextures[i]->GetName().end(), iName.begin(), ::tolower);
					std::string iFilter = buf;
					transform(iFilter.begin(), iFilter.end(), iFilter.begin(), ::tolower);
					size_t found = iName.find(iFilter);
					if (found != std::string::npos)
					{
						AddTextLine(dynamicTextures[i]->GetName());
						Image(dynamicTextures[i]->GetName(), 300.0f, 300.0f);
						AddSeparator();
					}
				}
				else
				{
					AddTextLine(dynamicTextures[i]->GetName());
					Image(dynamicTextures[i]->GetName(), 300.0f, 300.0f);
					AddSeparator();
				}
			}
			EndTreeNode();
		}

		AddSeparator();
		if (TreeNode("Static"))
		{
			for (size_t i = 0; i < staticTextures.size(); ++i)
			{
				if (strlen(buf) > 0)
				{
					std::string iName = staticTextures[i]->GetName();
					transform(staticTextures[i]->GetName().begin(), staticTextures[i]->GetName().end(), iName.begin(), ::tolower);
					std::string iFilter = buf;
					transform(iFilter.begin(), iFilter.end(), iFilter.begin(), ::tolower);
					std::size_t found = staticTextures[i]->GetName().find(buf);
					if (found != std::string::npos)
					{
						AddTextLine(staticTextures[i]->GetName());
						Image(staticTextures[i]->GetName(), 300.0f, 300.0f);
						AddSeparator();
					}
				} 
				else
				{
					AddTextLine(staticTextures[i]->GetName());
					Image(staticTextures[i]->GetName(), 300.0f, 300.0f);
					AddSeparator();
				}
			}
			EndTreeNode();
		}
		EndWindow();
	}
}

bool SelectingTexture = false;
void CImguiHelper::DrawParticlesWindow()
{
	if (ParticlesWindow){
		ImGui::Begin("Particles", &ParticlesWindow);
		

		particles::CParticleManager& partManager = CEngine::GetInstance().GetParticleManager();

		if (AddButton("Save Particles XML"))
		{
			partManager.Save();
		}

		//new particle
		if (AddButton("New Particle"))
		{
			partManager.NewParticle();
		}
		for (size_t i = 0; i < partManager.GetCount(); ++i)
		{
			particles::CParticleSystemType* particle = partManager.GetByIndex(i);
			const std::string& partName = particle->GetName();

			if (TreeNode(partName.c_str()))
			{
				AddTextLine("Particle Texture");
				materials::CTextureManager& texManager = CEngine::GetInstance().GetTextureManager();
				materials::CTexture* tex = texManager.GetByName(particle->m_Material->GetTextures().at(0)->GetName());
				Image(tex->GetName(), 300.0f, 300.0f);

				if (AddButton("Select Texture"))
				{
					SelectingTexture = true;
				}

				//select texture
				if (SelectingTexture)
				{
					if (AddButton("Ok"))
					{
						SelectingTexture = false;
					}

					if (NewWindow("Textures"))
					{
						int staticCount = 0;
						for (size_t i = 0; i < staticTextures.size(); ++i)
						{
							AddTextLine(staticTextures[i]->GetName());
							if (ImageButton(staticTextures[i]->GetName(), 300.0f, 300.0f))
							{
								materials::CTexture* tex = texManager.GetByName(staticTextures[i]->GetName());
								particle->m_Material->RemoveTexture(particle->m_Material->GetTextures().at(0));
								particle->m_Material->AddTexture(tex);
							}
							AddSeparator();
						}
						EndWindow();
					}
				}

				//particle parameters
				AddTextLine("Particle Parameters");
				particle->m_MaxParticles = DragIntMinMax("Max Num Particles", particle->m_MaxParticles, 1.0f, 0, 1000);
				particle->m_NumFrames = DragIntMinMax("Num frames", particle->m_NumFrames, 1.0f, 0, 100);
				particle->m_TimePerFrame = static_cast<float>(DragIntMinMax("Time per Frame", static_cast<int>(particle->m_TimePerFrame), 0.05f, 0, 100));
				particle->m_LoopFrames = AddCheckbox("Loop Frames?", particle->m_LoopFrames);
				particle->m_EmitAbsolute = AddCheckbox("Emit Absolute?", particle->m_EmitAbsolute);
				particle->m_StartingDirectionAngle = DragFloat("Starting Direction Angle", particle->m_StartingDirectionAngle, 0.05f);
				particle->m_StartingAccelerationAngle = DragFloat("Starting Acceleration Angle", particle->m_StartingAccelerationAngle, 0.05f);
				Vect2f lEmitRate = DragFloat2("Emit Rate Min - Max", particle->m_EmitRate, 0.05f);
				if (lEmitRate.x > lEmitRate.y)
				{
					lEmitRate.x = lEmitRate.y;
				}
				particle->m_EmitRate = lEmitRate;
				Vect2f l_AwakeTime = DragFloat2("Awake Time Min - Max", particle->m_AwakeTime, 0.05f);
				if (l_AwakeTime.x > l_AwakeTime.y)
				{
					l_AwakeTime.x = l_AwakeTime.y;
				}
				particle->m_AwakeTime = l_AwakeTime;
				Vect2f m_SleepTime = DragFloat2("Sleep Time Min - Max", particle->m_SleepTime, 0.05f);
				if (m_SleepTime.x > m_SleepTime.y)
				{
					m_SleepTime.x = m_SleepTime.y;
				}
				particle->m_SleepTime = m_SleepTime;
				Vect2f l_Life = DragFloat2("Life Time Min - Max", particle->m_Life, 0.05f);
				if (l_Life.x > l_Life.y)
				{
					l_Life.x = l_Life.y;
				}
				particle->m_Life = l_Life;
				Vect2f l_StartingAngle = DragFloat2("Starting Angle Min - Max", particle->m_StartingAngle, 0.05f);
				if (l_StartingAngle.x > l_StartingAngle.y)
				{
					l_StartingAngle.x = l_StartingAngle.y;
				}
				particle->m_StartingAngle = l_StartingAngle;
				Vect2f l_StartingAngularSpeed = DragFloat2("Starting Angular Speed Min - Max", particle->m_StartingAngularSpeed, 0.05f);
				if (l_StartingAngularSpeed.x > l_StartingAngularSpeed.y)
				{
					l_StartingAngularSpeed.x = l_StartingAngularSpeed.y;
				}
				particle->m_StartingAngularSpeed = l_StartingAngularSpeed;
				Vect2f l_AngularAcceleration = DragFloat2("Starting Angular Acceleration Min - Max", particle->m_AngularAcceleration, 0.05f);
				if (l_AngularAcceleration.x > l_AngularAcceleration.y)
				{
					l_AngularAcceleration.x = l_AngularAcceleration.y;
				}
				particle->m_AngularAcceleration = l_AngularAcceleration;
				particle->m_StartingSpeed1 = DragFloat3("Starting Speed Min", particle->m_StartingSpeed1, 0.05f);
				particle->m_StartingSpeed2 = DragFloat3("Starting Speed Max", particle->m_StartingSpeed2, 0.05f);
				particle->m_StartingAcceleration1 = DragFloat3("Starting Acceleration Min", particle->m_StartingAcceleration1, 0.05f);
				particle->m_StartingAcceleration2 = DragFloat3("Starting Acceleration Max", particle->m_StartingAcceleration2, 0.05f);
				
				AddSeparator();
				AddTextLine("Size Control Points");
				SetInline(0);
				//new point
				if (AddButton("New Size control Point"))
				{
					partManager.NewSizeControlPoint(*particle);
				}
				AddSeparator();
				for (size_t j = 0; j < particle->m_ControlPointSizes.size(); ++j)
				{
					if (TreeNode(std::string("Size Control Point " + std::to_string(j)).c_str()))
					{
						particles::CParticleSystemType::ControlPointSize& SizeControlPoint = particle->m_ControlPointSizes.at(j);
						SizeControlPoint.m_Time = DragFloat2("Time Control Min - Max", SizeControlPoint.m_Time, 0.05f);
						SizeControlPoint.m_Size = DragFloat2("Size Control Min - Max", SizeControlPoint.m_Size, 1);
						AddSeparator();
						EndTreeNode();
					}
				}

				AddSeparator();
				AddTextLine("Color Control Points");
				SetInline(0);
				//new color point
				if (AddButton("New Color Control Point"))
				{
					partManager.NewColorControlPoint(*particle);
				}
				AddSeparator();
				for (size_t j = 0; j < particle->m_ControlPointColors.size(); ++j)
				{
					if (TreeNode(std::string("Color Control Point " + std::to_string(j)).c_str()))
					{
						particles::CParticleSystemType::ControlPointColor& ColorControlPoint = particle->m_ControlPointColors.at(j);
						ColorControlPoint.m_Time = DragFloat2("Time Control Min - Max", ColorControlPoint.m_Time, 0.05f);
						ColorControlPoint.m_Color1 = CColor(ColorEdit4("Color Control Start", ColorControlPoint.m_Color1.x, ColorControlPoint.m_Color1.y, ColorControlPoint.m_Color1.z, ColorControlPoint.m_Color1.w));
						ColorControlPoint.m_Color2 = CColor(ColorEdit4("Color Control End", ColorControlPoint.m_Color2.x, ColorControlPoint.m_Color2.y, ColorControlPoint.m_Color2.z, ColorControlPoint.m_Color2.w));
						AddSeparator();
						EndTreeNode();
					}
				}

				EndTreeNode();
			}
		}

		EndWindow();
	}
}

void CImguiHelper::DrawLogWindow()
{
	if (LogWindow){
		DrawLog();
	}
}

void CImguiHelper::LoadFont()
{
	ImGuiIO& io = ImGui::GetIO();
	ImFont* font1 = io.Fonts->AddFontDefault();
	ImFont* font2 = io.Fonts->AddFontFromFileTTF("data/common/font/forgotten_uncial.ttf", 26.0f);
	font2->GetCharAdvance('\xE3');
	io.Fonts->Build();
}

float CImguiHelper::FirstLineSize(int font, std::string text)
{
	ImGuiIO& io = ImGui::GetIO();
	ImFont* font1 = io.Fonts->Fonts[font];
	const char *s = text.c_str();
	const char *end = s + strlen(s);
	float finalsize = 0.0f;
	while (s < end && *s != '\n')
	{
		unsigned int c = (unsigned int)*s;
		finalsize += font1->FindGlyph(c)->XAdvance;
		s++;
	}
	return finalsize;
}

void CImguiHelper::PushFont()
{
	ImGuiIO& io = ImGui::GetIO();
	ImFont* font1 = io.Fonts->Fonts[1];
	ImGui::PushFont(font1);
}

void CImguiHelper::PopFont()
{
	ImGui::PopFont();
}

void CImguiHelper::DrawMainMenu()
{
	if (BeginMainMenuBar())
	{
		if (BeginMenu("Info"))
		{
			if (MenuItem("Actions", ""))
			{
				InfoWindow = true;
			}
			if (MenuItem("Camera", ""))
			{
				CameraWindow = true;
			}
			if (MenuItem("Log", ""))
			{
				LogWindow = true;
			}
			
			EndMenu();
		}

		if (BeginMenu("Materials"))
		{
			if (MenuItem("Materials", ""))
			{
				MaterialsWindow = true;
			}

			EndMenu();
		}

		if (BeginMenu("Scene"))
		{
			if (MenuItem("Scene Nodes", ""))
			{
				SceneNodesWindow = true;
			}

			if (MenuItem("Scene Lights", ""))
			{
				SceneLightsWindow = true;
			}

			if (MenuItem("Render Pipeline", ""))
			{
				RenderPipelineWindow = true;
			}

			if (MenuItem("Cinematics", ""))
			{
				CinematicsWindow = true;
				Log("Cinematics");
			}

			EndMenu();
		}

		if (BeginMenu("Textures"))
		{
			if (MenuItem("Textures", ""))
			{
				TexturesWindow = true;
			}

			EndMenu();
		}
		if (BeginMenu("Particles"))
		{
			if (MenuItem("Particles", ""))
			{
				ParticlesWindow = true;
			}

			EndMenu();
		}
		if (BeginMenu("Reloads"))
		{
			/*if (MenuItem("LUA", ""))
			{
				CEngine::GetInstance().GetScriptManager().m_Reloading = true;
			}*/
			if (MenuItem("Scene", ""))
			{
				CEngine::GetInstance().GetSceneManager().GetCurrentScene()->m_Reloading = true;
			}
			if (MenuItem("Shaders", ""))
			{
				CEngine::GetInstance().GetShaderManager().Reload();
			}
			if (MenuItem("Pipeline", ""))
			{
				CEngine::GetInstance().GetRenderPipelineManager().GetActivePipeline().m_Reloading = true;
			}

			EndMenu();
		}
		
		EndMainMenuBar();
	}
}