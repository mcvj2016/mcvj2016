#ifndef _BASE_IMGUIHELPER_DPVD1_27102016203657_H_H
#define _BASE_IMGUIHELPER_DPVD1_27102016203657_H_H

#include <string>
#include "Utils/Imgui/imgui.h"
#include "Math/Vector4.h"


class CColor;
namespace engine
{
	namespace scenes
	{
		class CSceneNode;
	}
	namespace materials
	{
		class CMaterial;
	}
}
struct ImguiLog
{
	ImGuiTextBuffer     Buf;
	ImGuiTextFilter     Filter;
	ImVector<int>       LineOffsets;        // Index to lines offset
	bool                ScrollToBottom;

	void    Clear()     { Buf.clear(); LineOffsets.clear(); }

	void    AddLog(const char* fmt, ...) IM_PRINTFARGS(2)
	{
		int old_size = Buf.size();
		va_list args;
		va_start(args, fmt);
		Buf.appendv(fmt, args);
		va_end(args);
		for (int new_size = Buf.size(); old_size < new_size; old_size++)
			if (Buf[old_size] == '\n')
				LineOffsets.push_back(old_size);
		ScrollToBottom = true;
	}

	void    Draw(const char* title, bool* p_opened = NULL)
	{
		ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiSetCond_FirstUseEver);
		ImGui::Begin(title, p_opened);
		if (ImGui::Button("Clear")) Clear();
		ImGui::SameLine();
		bool copy = ImGui::Button("Copy");
		ImGui::SameLine();
		Filter.Draw("Filter", -100.0f);
		ImGui::Separator();
		ImGui::BeginChild("scrolling");
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 1));
		if (copy) ImGui::LogToClipboard();

		if (Filter.IsActive())
		{
			const char* buf_begin = Buf.begin();
			const char* line = buf_begin;
			for (int line_no = 0; line != NULL; line_no++)
			{
				const char* line_end = (line_no < LineOffsets.Size) ? buf_begin + LineOffsets[line_no] : NULL;
				if (Filter.PassFilter(line, line_end))
					ImGui::TextUnformatted(line, line_end);
				line = line_end && line_end[1] ? line_end + 1 : NULL;
			}
		}
		else
		{
			ImGui::TextUnformatted(Buf.begin());
		}

		if (ScrollToBottom)
			ImGui::SetScrollHere(1.0f);
		ScrollToBottom = false;
		ImGui::PopStyleVar();
		ImGui::EndChild();
		ImGui::End();
	}
};

#define IMGUI_DisableWindowInteractionFlag 559
#define IMGUI_DisableWindowModificationFlag 63

namespace engine {
	class CImguiHelper
	{

		

	public:
		CImguiHelper();
		virtual ~CImguiHelper();
		void Log(const std::string& entry);
		void LogV3f(const Vect3f& entry) const;
		bool AddButton(const std::string& name) const;
		bool NewWindow(const char* title) const;
		bool NewWindowWithFlags(const char* title, int flag) const;
		void SetNextWindowPos(float x, float y) const;
		void SetNextWindowSize(float x, float y) const;
		void SetNextWindowPadding(float x, float y);
		void SetNextWindowMinSize(float x, float y);
		void SetSavedWindowMinSze() const;
		void SetSavedWindowPadding() const;
		void SetAlign(float x, float y, float textSize, float fontSize, float lines) const;
		float GetCursorPosX() const;
		float GetCursorPosY() const;
		int RadioButton(const std::string& name, int buttonVal, int button);
		void DisableWindowBackGroundColor();
		void SetWindowBackGroundColor(Vect4f color);
		void DisableRoundedWindow();
		void RestorePushedVar();
		bool FullScreenWindow(const std::string& name);
		void Image(const std::string& textureName, float width, float height);
		void ImageRGBA(const std::string& textureName, float width, float height, const CColor& rgba) const;
		void FullScreenImage(const std::string& textureName);
		void FullScreenImageRGBA(const std::string& textureName, const CColor& rgba);
		bool IsItemHovered() const;
		bool ImageButton(const std::string& textureName, float width, float height) const;
		void EndWindow() const;
		void AddTextLine(std::string line) const;
		void ShowFrameRate() const;
		void AddSeparator() const;
		void SetInline(float offset) const;
		void SetInlineRight(float offset) const;
		bool AddCheckbox(const char* name, bool prop);
		bool BeginMainMenuBar() const;
		void EndMainMenuBar() const;
		bool BeginMenu(const char* aName) const;
		void EndMenu() const;
		bool MenuItem(const char* aName, const char* shortcut) const;
		bool TreeNode(const char* aName) const;
		void EndTreeNode() const;
		void DrawLog();
		Vect3f InputFloat3(std::string name, Vect3f vec);
		void ImGuiRender();
		bool IsLastItemActive();
		void ClearConsoleLog();
		void DrawConsole();
		void ExecCommand(const char* command_line);
		void DrawDebug();
		void DrawWindows();
		void DrawCinematicsWindow();
		void DrawCameraInfoWindow();
		void DrawActionsInfoWindow();
		void DrawMaterialsWindow();
		void DrawMat(materials::CMaterial* mat);
		void DrawSceneNode(scenes::CSceneNode* node, const std::string& layerName);
		void DrawSceneNodesWindow();
		void DrawSceneLightsWindow();
		void DrawRenderPipelineWindow();
		void Preload();
		void Clear();
		void DrawTexturesWindow();
		void DrawParticlesWindow();
		void DrawLogWindow();
		void DrawMainMenu();
		float DragFloat(const char* aName, float f, float speed) const;
		Vect2f DragFloat2(const char* aName, Vect2f f, float speed) const;
		Vect3f DragFloat3(const char* aName, Vect3f f, float speed) const;
		float DragFloatMinMax(const char* aName, float f, float speed, float min, float max) const;
		int DragInt(const char* aName, int i, float speed) const;
		int DragIntMinMax(const char* aName, int i, float speed, int min, int max) const;
		Vect4f ColorEdit4(const char* name, float r, float g, float b, float a);
		void SetMainFontColor();
		void SetDefaultFontColor();
		void LoadFont();
		float FirstLineSize(int font, std::string text);
		void PushFont();
		void PopFont();
	private:
		ImguiLog* m_Log;
		ImVec2 m_SavedWindowPadding;
		ImVec2 m_SavedWindowMinSize;
	};
}

#endif // _BASE_IMGUIHELPER_DPVD1_27102016203657_H_H
