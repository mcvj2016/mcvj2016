#include  "Command.h"

namespace engine {
	CCommand::CCommand() :CName()
	{
	}

	CCommand::CCommand(const std::string& name, func cb)
		: CName(name),
		m_Callback(cb)
	{

	}

	CCommand::~CCommand()
	{
	}

	void CCommand::Exec() const
	{
		m_Callback();
	}
}