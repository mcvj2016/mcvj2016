#ifndef _ENGINE_THIRDPERSONCAMERACONTROLLER_27112016203538_H
#define _ENGINE_THIRDPERSONCAMERACONTROLLER_27112016203538_H
#include "CameraController.h"

class CThirdPersonCameraController : public CCameraController
{
private:
	float						m_FastSpeed;

public:

	float						m_YawSpeed;
	float						m_PitchSpeed;
	float						m_Speed;
	float						m_RightSpeed;

	//Added Juan
	Vect3f m_Center;// posicion orkelf

	float m_ZoomSpeed;
	float m_Zoom;

	float maxPitch, minPitch;
	float maxZoom, minZoom;

	CThirdPersonCameraController();
	CThirdPersonCameraController(const CXMLElement* aElement);
	virtual ~CThirdPersonCameraController();
	void Update(float ElapsedTime) override;
};

#endif	// _ENGINE_THIRDPERSONCAMERACONTROLLER_27112016203538_H