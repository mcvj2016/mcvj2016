#include "CameraManager.h"
#include "XML/XML.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "CSphericalCameraController.h"
#include "AnimatedCamera.h"
#include "Engine.h"

namespace engine
{
	namespace camera
	{
		CCameraManager::CCameraManager()
			: m_MainCamera(nullptr)
		{
		}

		void CCameraManager::AddCamera(CCameraController* aCamera)
		{
			Add(aCamera->GetSceneCamera()->GetName(), aCamera);
		}
		void CCameraManager::AddCamera(CCameraController* aCamera, const std::string& name)
		{
			Add(name, aCamera);
		}

		CCameraManager::~CCameraManager()
		{
		}

		bool CCameraManager::Load(const std::string& path)
		{
			/*El manager de escena se encargar� de cargar un fichero con los directorios de cada escena e ir� cargando las escenas*/
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(path.c_str()));
			assert(loaded);

			CXMLElement* cameras = xmldoc.FirstChildElement("cameras");
			if (cameras != nullptr)
			{
				CXMLElement* cameraElement = cameras->FirstChildElement("camera");

				while (cameraElement != nullptr)
				{
					CCameraController* cam;
					std::string cameraType = cameraElement->GetAttribute<std::string>("type", "ANIMATED");
					std::string cameraName = cameraElement->GetAttribute<std::string>("name", "MainCamera");
					CCameraController::TCameraType lType;
					if (EnumString<CCameraController::TCameraType>::ToEnum(lType, cameraType))
					{
						Vect3f rotation;
						switch (lType)
						{
						case CCameraController::THIRD_PERSON:
							cam = new CThirdPersonCameraController(cameraElement);
							
							break;
						case CCameraController::FIRST_PERSON:
							cam = new CFirstPersonCameraController(cameraElement);
							break;
						case CCameraController::SPHERICAL:
							cam = new CSphericalCameraController(cameraElement);
							break;
						default:
							cam = new CAnimatedCamera(cameraElement);
							break;
						}
						CSCeneCamera* sceneCam = new CSCeneCamera(cameraElement, cam);
						cam->SetSceneCamera(sceneCam);
						this->Add(sceneCam->GetName(), cam);
					}
					cameraElement = cameraElement->NextSiblingElement("camera");
				}
			}

			return true;
		}
	}
}
