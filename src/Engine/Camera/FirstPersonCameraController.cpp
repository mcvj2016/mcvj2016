#include "FirstPersonCameraController.h"
#include "Engine.h"
#include "Render/RenderManager.h"
#include "CameraManager.h"

//CFirstPersonCameraController::CFirstPersonCameraController(float maxPitch, float minPitch, float maxZoom, float minZoom)
CFirstPersonCameraController::CFirstPersonCameraController()
	: 
	m_FastSpeed(10.0f),
	m_YawSpeed(2.5f),
	m_PitchSpeed(2.f),
	m_axisXSpeed(0.0f),
	m_axisYSpeed(0.0f),
	m_Speed(2.0f),
	m_RightSpeed(0.0f)
{
	m_CameraType = FIRST_PERSON;
}

CFirstPersonCameraController::CFirstPersonCameraController(const tinyxml2::XMLElement* aElement)
:
CCameraController(aElement),
m_FastSpeed(10.0f),
m_YawSpeed(2.5f),
m_PitchSpeed(2.f),
m_axisXSpeed(0.0f),
m_axisYSpeed(0.0f),
m_Speed(2.0f),
m_RightSpeed(0.0f)
{
	m_CameraType = FIRST_PERSON;
}

CFirstPersonCameraController::~CFirstPersonCameraController()
{ }

void CFirstPersonCameraController::Update(float ElapsedTime)
{
	if (engine::CEngine::GetInstance().GetCameraManager().GetMainCamera() == this)
	{
		//get the viewproj fron rendermanager
		m_ViewProj = engine::CEngine::GetInstance().GetRenderManager().GetViewProjectionMatrix();
		//update frustrum
		m_Frustrum->Update(m_ViewProj);
	}
}

