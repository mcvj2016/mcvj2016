#include "CameraController.h"
#include "Engine.h"
#include "CameraManager.h"
#include "XML/XML.h"

CCameraController::CCameraController()
	:
	m_CameraType(THIRD_PERSON),
	m_Lock(false),
	m_LookAt(v3fZERO),
	m_View(m44fZERO),
	m_ViewProj(m44fZERO),
	m_Front(v3fFRONT),
	m_FOV(.0f),
	m_Near(.0f),
	m_Far(.0f),
	m_Frustrum(new CFrustum())
{
}

CCameraController::CCameraController(std::string aName)
	: 
	CCameraController()
{
	m_SceneCamera->SetName(aName);
}

CCameraController::~CCameraController()
{
	delete m_Frustrum;
}

CCameraController::CCameraController(const CXMLElement* aElement)
	: 
	m_CameraType(THIRD_PERSON),
	m_Lock(false),
	m_LookAt(v3fZERO),
	m_View(m44fZERO),
	m_ViewProj(m44fZERO),
	m_Front(v3fFRONT),
	m_FOV(.0f),
	m_Near(.0f),
	m_Far(.0f),
	m_Frustrum(new CFrustum())
{
	const CXMLElement* transformElement = aElement->FirstChildElement("transform");
	this->SetFOV(transformElement->GetAttribute<float>("fov", .0f));
	this->SetNear(transformElement->GetAttribute<float>("near_plane", .0f));
	this->SetFar(transformElement->GetAttribute<float>("far_plane", .0f));
	engine::CEngine::GetInstance().GetCameraManager().AddCamera(this, aElement->GetAttribute<std::string>("name", ""));
	if (aElement->GetAttribute<bool>("main", false))
	{
		engine::CEngine::GetInstance().GetCameraManager().SetMainCamera(this);
	}
}

void CCameraController::SetLock(const bool& Lock)
{
	if (Lock == true)
	{
		m_Lock = Lock;
	}
	m_Lock = Lock;
}

const bool& CCameraController::GetLock() const
{
	return m_Lock;
}

CFrustum* CCameraController::GetFrustum()
{
	return m_Frustrum;
}

const Mat44f& CCameraController::GetView() const
{
	return m_View;
}

void CCameraController::SetMatrixs()
{
	m_View.SetIdentity();
	m_View.SetFromLookAt(m_SceneCamera->m_Position, m_LookAt, m_SceneCamera->GetUp());
}

CSCeneCamera* CCameraController::GetSceneCamera()
{
	return m_SceneCamera;
}

void CCameraController::SetSceneCamera(CSCeneCamera* sceneCam)
{
	m_SceneCamera = sceneCam;
}
