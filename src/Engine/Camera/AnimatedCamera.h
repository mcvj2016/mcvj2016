#ifndef _ENGINE_ANIMATEDCAMERACONTROLLER_12022017165100_H
#define _ENGINE_ANIMATEDCAMERACONTROLLER_12022017165100_H
#include "CameraController.h"

class CAnimatedCamera : public CCameraController
{

public:

	CAnimatedCamera();
	CAnimatedCamera(const CXMLElement* aElement);
	virtual ~CAnimatedCamera();
	void Update(float ElapsedTime) override;
	void SetMatrixs();

	GET_SET_REF(Vect3f, UpAnim);

private:
	Vect3f m_UpAnim;
};

#endif	// _ENGINE_THIRDPERSONCAMERACONTROLLER_27112016203538_H