#ifndef _ENGINE_FIRSTPERSONCAMERACONTROLLER_DPVD1_27102016203657_H
#define _ENGINE_FIRSTPERSONCAMERACONTROLLER_DPVD1_27102016203657_H
#include "CameraController.h"

class CFirstPersonCameraController : public CCameraController
{
private:
	float						m_FastSpeed;
	
public:

	float						m_YawSpeed;
	float						m_PitchSpeed;
	float						m_axisXSpeed;
	float						m_axisYSpeed;
	float						m_Speed;
	float						m_RightSpeed;

	CFirstPersonCameraController();
	CFirstPersonCameraController(const CXMLElement* aElement);
	virtual ~CFirstPersonCameraController();
	void Update(float ElapsedTime) override;
};

#endif
