#ifndef _ENGINE_SPHERICALCAMERACONTROLLER_DPVD1_27102016203657_H
#define _ENGINE_SPHERICALCAMERACONTROLLER_DPVD1_27102016203657_H
#include "CameraController.h"

class CSphericalCameraController : public CCameraController
{
public:

	float m_Speed;
	float m_RightSpeed;
	float m_ZoomSpeed;
	float m_Zoom;

	CSphericalCameraController();
	CSphericalCameraController(const CXMLElement* aElement);
	virtual ~CSphericalCameraController();

	void Update(float ElapsedTime) override;

private:


};

#endif
