#ifndef _ENGINE_CAMERAMANAGER_11022017203958_H
#define _ENGINE_CAMERAMANAGER_11022017203958_H
#include "CameraController.h"
#include "Utils/TemplatedMap.h"

namespace engine
{
	namespace camera
	{
		class CCameraManager : public base::utils::CTemplatedMap<CCameraController>
		{
		public:
			CCameraManager();
			void AddCamera(CCameraController* aCamera);
			void AddCamera(CCameraController* aCamera, const std::string& name);
			virtual ~CCameraManager();
			//void SetMainCameraByName(const std::string& aName);
			bool Load(const std::string& path);

			void SetMainCamera(CCameraController* MainCamera) { m_MainCamera=MainCamera; } 
			CCameraController* GetMainCamera() { return m_MainCamera; };
		private:
			CCameraController* m_MainCamera;
		};
	}
}


#endif	// _ENGINE_CAMERAMANAGER_11022017203958_H
