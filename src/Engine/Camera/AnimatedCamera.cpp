#include "AnimatedCamera.h"
#include "Engine.h"
#include "Render/RenderManager.h"
#include "CameraManager.h"
#include "XML/XML.h"

CAnimatedCamera::CAnimatedCamera()
{
	m_CameraType = ANIMATED;
}

CAnimatedCamera::CAnimatedCamera(const tinyxml2::XMLElement* aElement)
	: CCameraController(aElement)
{
	const CXMLElement* transformElement = aElement->FirstChildElement("transform");
	this->SetLookAt(transformElement->GetAttribute<Vect3f>("look_at", v3fZERO));
	this->SetUpAnim(transformElement->GetAttribute<Vect3f>("up", v3fZERO));
	m_CameraType = ANIMATED;
}

CAnimatedCamera::~CAnimatedCamera()
{
	
}

void CAnimatedCamera::Update(float ElapsedTime)
{
	if (engine::CEngine::GetInstance().GetCameraManager().GetMainCamera() == this)
	{
		//get the viewproj fron rendermanager
		m_ViewProj = engine::CEngine::GetInstance().GetRenderManager().GetViewProjectionMatrix();

		//update frustrum
		m_Frustrum->Update(m_ViewProj);
	}
}

void CAnimatedCamera::SetMatrixs()
{
	m_View.SetIdentity();
	m_View.SetFromLookAt(m_SceneCamera->m_Position, m_LookAt, GetUpAnim());
}

