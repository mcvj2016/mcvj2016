#ifndef _ENGINE_CAMERACONTROLLER_DPVD1_27102016203657_H
#define _ENGINE_CAMERACONTROLLER_DPVD1_27102016203657_H
#include "Math/Quaternion.h"
#include "Utils/EnumToString.h"
#include "Frustum.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Graphics/Scenes/SceneCamera.h"

class CCameraController
{

public:

	enum TCameraType
	{
		THIRD_PERSON,
		FIRST_PERSON,
		SPHERICAL,
		ANIMATED
	};

	CCameraController();
	
	CCameraController(std::string aName);

	virtual ~CCameraController();
	virtual void Update(float ElapsedTime) = 0;
	CCameraController(const CXMLElement* aElement);

	void SetFOV(const float &FOV) { m_FOV=FOV; } const float & GetFOV() const { return m_FOV; };
	void SetNear(const float &Near) { m_Near=Near; } const float & GetNear() const { return m_Near; };
	void SetFar(const float &Far) { m_Far=Far; } const float & GetFar() const { return m_Far; };
	void SetLookAt(const Vect3f &LookAt) { m_LookAt=LookAt; } const Vect3f & GetLookAt() const { return m_LookAt; };
	void SetLock(const bool &Lock); 
	const bool & GetLock() const;

	CFrustum* GetFrustum();
	const Mat44f & GetView() const;

	virtual void SetMatrixs();

	TCameraType m_CameraType;
	CSCeneCamera* GetSceneCamera();
	void SetSceneCamera(CSCeneCamera* sceneCam);

protected:
	bool m_Lock;
	Vect3f m_LookAt;
	Mat44f m_View;
	Mat44f m_ViewProj;
	Vect3f m_Front;
	float m_FOV;
	float m_Near;
	float m_Far;
	CFrustum* m_Frustrum;
	CSCeneCamera* m_SceneCamera;
};

Begin_Enum_String(CCameraController::TCameraType)
{
	Enum_String_Id(CCameraController::THIRD_PERSON, "THIRD_PERSON");
	Enum_String_Id(CCameraController::FIRST_PERSON, "FIRST_PERSON");
	Enum_String_Id(CCameraController::SPHERICAL, "SPHERICAL");
	Enum_String_Id(CCameraController::ANIMATED, "ANIMATED");
}
End_Enum_String;
#endif