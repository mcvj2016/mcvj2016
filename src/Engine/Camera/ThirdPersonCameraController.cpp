#include "ThirdPersonCameraController.h"
#include "Engine.h"
#include "Render/RenderManager.h"
#include "CameraManager.h"

CThirdPersonCameraController::CThirdPersonCameraController()
	:
	m_FastSpeed(10.0f),
	m_YawSpeed(0.0f),
	m_PitchSpeed(0.0f),
	m_Speed(0.0f),
	m_RightSpeed(0.0f),
	m_Center(0.0f, 0.0f, 0.0f),
	m_ZoomSpeed(4.0f),
	m_Zoom(5.0f),
	maxPitch(1.0f), minPitch(3.1416f),
	maxZoom(5.0f), minZoom(0.5f)
{
	m_CameraType = THIRD_PERSON;
}

CThirdPersonCameraController::CThirdPersonCameraController(const tinyxml2::XMLElement* aElement)
	:
	CCameraController(aElement),
	m_FastSpeed(10.0f),
	m_YawSpeed(0.0f),
	m_PitchSpeed(0.0f),
	m_Speed(0.0f),
	m_RightSpeed(0.0f),
	m_Center(0.0f, 0.0f, 0.0f),
	m_ZoomSpeed(4.0f),
	m_Zoom(5.0f),
	maxPitch(1.0f), minPitch(3.1416f),
	maxZoom(5.0f), minZoom(0.5f)
{
	m_CameraType = THIRD_PERSON;
}

CThirdPersonCameraController::~CThirdPersonCameraController()
{ }

void CThirdPersonCameraController::Update(float ElapsedTime)
{
	if (engine::CEngine::GetInstance().GetCameraManager().GetMainCamera() == this)
	{
		//engine::scenes::CSceneManager& l_SM = engine::CEngine::GetInstance().GetSceneManager();
		//engine::scenes::CSceneNode* player = l_SM.GetCurrentScene()->GetSceneNode("Orkelf");
		//this->m_Center = player->m_Position + Vect3f(0.0f, 2.0f, 0.0f);
		//get the viewproj fron rendermanager
		m_ViewProj = engine::CEngine::GetInstance().GetRenderManager().GetViewProjectionMatrix();

		//ROTATION
		//add rotations
		/*if (!m_Lock)
		{
		m_Yaw += (m_YawSpeed * ElapsedTime);
		if (m_Yaw >= (3.1416f*2.0f))
		{
		m_Yaw = m_Yaw - (3.1416f*2.0f);
		}
		else if (m_Yaw < (3.1416f*2.0f))
		{
		m_Yaw = m_Yaw + (3.1416f*2.0f);
		}

		m_Pitch += (m_PitchSpeed * ElapsedTime);
		if (m_Pitch >= ((3.1416f/2.0f) - 0.5f ))
		{
		m_Pitch = ((3.1416f / 2.0f) - 0.5f );
		}
		else if (m_Pitch < ((-3.1416f / 2.0f) + 0.5f))
		{
		m_Pitch = ((-3.1416f / 2.0f) + 0.5f);
		}

		//Se obtiene el valor del forward, se multiplica por la velocidad del movimiento y se le suma el resultado a la posicion
		m_Position = m_Position + (m_Speed * ElapsedTime * GetForward().GetNormalized());

		//Se obtiene el valor del right, se multiplica por la velocidad del movimiento y se le suma el resultado a la posicion
		m_Position = m_Position + (m_RightSpeed * ElapsedTime * GetRight().GetNormalized());
		}*/

		//El LookAT es el forward desde el 0.0 0.0 0.0 sumado a la posici�n actual
		//SetLookAt(GetForward() + m_Position);
		//Establece  la matrix vista
		//SetMatrixs();
		//update frustrum
		m_Frustrum->Update(m_ViewProj);
	}
	
}

