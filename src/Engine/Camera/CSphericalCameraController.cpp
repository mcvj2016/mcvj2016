#include "CSphericalCameraController.h"
#include "Engine.h"
#include "CameraManager.h"

CSphericalCameraController::CSphericalCameraController()
	: m_Speed(0),
	m_RightSpeed(0),
	m_ZoomSpeed(2.f),
	m_Zoom(0)
{
	m_CameraType = SPHERICAL;
}

CSphericalCameraController::CSphericalCameraController(const tinyxml2::XMLElement* aElement)
	:
	CCameraController(aElement),
	m_Speed(0),
	m_RightSpeed(0),
	m_ZoomSpeed(2.f),
	m_Zoom(0)
{
	m_CameraType = SPHERICAL;
}

CSphericalCameraController::~CSphericalCameraController()
{ }

void CSphericalCameraController::Update(float ElapsedTime)
{
	if (engine::CEngine::GetInstance().GetCameraManager().GetMainCamera() == this)
	{
		//set our view matrix
		SetMatrixs();

		//ROTATION
		Vect3f cameraMovement(0, 0, 0);
		cameraMovement.x = m_Speed * ElapsedTime;
		cameraMovement.y = m_RightSpeed * ElapsedTime;
		//AddPitch(cameraMovement.y);
		//AddYaw(cameraMovement.x);
		m_Zoom -= m_ZoomSpeed * 0.5f;

		//fix negative zoom and apply
		if (m_Zoom < 1)
		{
			m_Zoom = 1;
			SetLookAt(m_SceneCamera->m_Position - (m_SceneCamera->GetForward()*m_Zoom));
		}
	}
}