#ifndef _ENGINE_FRUSTRUM_08012017023300_H
#define _ENGINE_FRUSTRUM_08012017023300_H

#include "Graphics/Meshes/AxisAlignedBoundingBox.h"
#include "Graphics/Meshes/BoundingSphere.h"
#include "Math/Matrix44.h"

class CFrustum
{
public:
	CFrustum() {}
	virtual ~CFrustum() {}
	void Update(const Mat44f &ViewProj);
	bool IsVisible(const engine::materials::CBoundingSphere &aBoundingSphere, const Vect3f& position, const Vect3f& scale) const;
	bool IsVisible(const engine::materials::CAxisAlignedBoundingBox &aAABB, const Vect3f& position) const;
private:
	float m_Proj[16];
	float m_Modl[16];
	float m_Clip[16];
	float m_Frustum[6][4];
};
#endif
