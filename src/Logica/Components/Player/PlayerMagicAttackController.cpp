#include "PlayerMagicAttackController.h"
#include "Input/ActionManager.h"
#include "PlayerLocomotion.h"
#include "Camera/CameraManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Graphics/Scenes/SceneManager.h"
#include "PhysXImpl/PhysXManager.h"
#include "Components/Weapon/PlayerMagicProjectile.h"
#include "Utils/Coroutine.h"
#include "PlayerAnimator.h"

namespace logic
{
	namespace components
	{
		CPlayerMagicAttackController::CPlayerMagicAttackController()
			:
			CScriptComponent(),
			m_PlayerLocomotion(nullptr),
			m_PrevCamera(nullptr),
			m_AttackMagicName1("MagicSpellProjectile"),
			m_AttackMagicName2("MagicSpellArea"),
			m_Camera(nullptr), 
			m_Magic1Cooldown(4), 
			m_Magic2Cooldown(8),
			m_Magic1InCooldown(false),
			m_Magic2InCooldown(false),
			m_AimCamName("AimingCamera"),
			m_ImpactPreviewPoint(nullptr),
			m_MagicProjectile(nullptr),
			m_ImpactPreviewName("AimImpactPreview"),
			m_MagicProjectileName("MagicProjectile"),
			m_Animator(nullptr)
		{
		}

		CPlayerMagicAttackController::CPlayerMagicAttackController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			  m_ImpactPreviewName("AimImpactPreview"),
			  m_MagicProjectileName("MagicProjectileComposer"),
			  m_ImpactPreviewPoint(nullptr),
			  m_MagicProjectile(nullptr),
			  m_PlayerLocomotion(nullptr),
			  m_PrevCamera(nullptr),
			  m_Camera(nullptr), 
			  m_Magic1Cooldown(4), 
			  m_Magic2Cooldown(8),
			  m_Magic1InCooldown(false),
			  m_Magic2InCooldown(false),
			  m_AttackMagicName1("MagicSpellProjectile"),
			  m_AttackMagicName2("MagicSpellArea"),
			  m_AimCamName("AimingCamera"),
			  m_Animator(nullptr)
		{
		}

		
		CPlayerMagicAttackController::~CPlayerMagicAttackController()
		{
		}

		void CPlayerMagicAttackController::Start()
		{
			m_PlayerLocomotion = m_Parent->GetComponent<CPlayerLocomotion>();
			m_Animator = m_Parent->GetComponent<CPlayerAnimator>();
			m_PrevCamera = m_Engine.GetCameraManager().GetMainCamera();
			m_Camera = m_Engine.GetCameraManager().GetByName(m_AimCamName);
			m_ImpactPreviewPoint = static_cast<CSceneNode*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_ImpactPreviewName));
			m_MagicProjectile = static_cast<CSceneNode*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_MagicProjectileName));
		}

		void CPlayerMagicAttackController::ProjectileUpdate(float dt)
		{
			//aiming
			if (m_Engine.GetActionManager().IsInputActionActive("StartAiming"))
			{
				m_Camera->GetSceneCamera()->SetForward(m_Parent->GetForward());
				m_PrevCamera->GetSceneCamera()->SetEnabled(false);
				m_Camera->GetSceneCamera()->SetEnabled(true);
				m_Engine.GetCameraManager().SetMainCamera(m_Camera);
				m_PlayerLocomotion->SetLockPlayer(true);

				//performance check
				if (m_Animator->GetActualIndex() != sm::AimingIndex)
				{
					m_Animator->SendEvent<sm::Stop>(sm::Stop());
					m_Animator->SendEvent<sm::Aiming>(sm::Aiming());
				}
			}
			if (m_Engine.GetActionManager().IsInputActionActive("Aiming"))
			{
				RaycastData hit;
				unsigned int groupsToHit = m_Engine.GetPhysXManager().GetPhysicalGroups();
				//launch the raycast
				if (m_Engine.GetPhysXManager().Raycast(m_Camera->GetSceneCamera()->GetPosition(), m_Camera->GetSceneCamera()->GetForward() * 500, groupsToHit, &hit))
				{
					m_ImpactPreviewPoint->SetPosition(hit.position);
					m_ImpactPreviewPoint->SetVisible(true);

					//cast the magic if we have a valid hit point
					if (m_Engine.GetActionManager().IsInputActionActive(m_AttackMagicName1)
						&& !m_Magic1InCooldown)
					{
						//set cooldown
						m_Magic1InCooldown = true;
						//start coroutine for setting off cooldown
						base::utils::CCoroutine::GetInstance().StartCoroutine(
							m_Magic1Cooldown,
							[this]() {
								m_Magic1InCooldown = false;
							}
						);
						m_Animator->SendEvent<sm::Magic1>(sm::Magic1());

						CPlayerMagicProjectile* projectile = m_MagicProjectile->GetComponent<CPlayerMagicProjectile>();
						projectile->SetTargetPoint(m_ImpactPreviewPoint->GetPosition());
					}
				}
				else
				{
					m_ImpactPreviewPoint->SetVisible(false);
				}
			}
			if (m_Engine.GetActionManager().IsInputActionActive("AimingEnd"))
			{
				m_Camera->GetSceneCamera()->SetEnabled(false);
				m_PrevCamera->GetSceneCamera()->SetEnabled(true);
				m_Engine.GetCameraManager().SetMainCamera(m_PrevCamera);
				m_PlayerLocomotion->SetLockPlayer(false);
				m_Animator->SendEvent<sm::Stop>(sm::Stop());
				m_ImpactPreviewPoint->SetVisible(false);
			}
		}

		void CPlayerMagicAttackController::Update(float dt)
		{
			ProjectileUpdate(dt);
		}
	}
}
