#ifndef _LOGICA_PLAYERINVENTORY_H
#define _LOGICA_PLAYERINVENTORY_H
#include "Utils/Singleton.h"

namespace logic
{
	namespace components
	{
		class CPlayerInventory : public base::utils::CSingleton<CPlayerInventory>
		{
		public:
			CPlayerInventory();
			virtual ~CPlayerInventory();

			bool m_hasKey;
			bool m_hasChest;
			bool m_hasBoots;
			bool m_hasBucket;
			bool m_hasRope;
			bool m_hasHelm;
			
		private:
			
		};
	}
}

#endif