#include "PlayerAnimator.h"
#include "StateMachines/PlayerSM.h"


namespace logic
{
	namespace player
	{
		CPlayerAnimator::CPlayerAnimator() : CModelAnimator(), m_ComboAttack(false), m_GoingToRun(false)
		{
		}

		CPlayerAnimator::CPlayerAnimator(const std::string& name, const std::string& parent)
			:
			CModelAnimator(name, parent), m_ComboAttack(false), m_GoingToRun(false)
		{
		}

		CPlayerAnimator::~CPlayerAnimator()
		{
		}

		void CPlayerAnimator::Start()
		{
			m_SM = new CPlayerSM();
			m_SM->m_Animator = this;
			m_SM->SetName(m_Parent->GetName());
			m_Model = reinterpret_cast<CSceneAnimatedModel*>(m_Parent);

			m_SM->Add<Idle>("Idle");
			m_SM->Add<Move>("Move");
			m_SM->Add<Run>("Run");
			m_SM->Add<Attack1>("Attack1");
			m_SM->Add<Attack2>("Attack2");
			m_SM->Add<Attack3>("Attack3");
			m_SM->Add<Aiming>("Aiming");
			m_SM->Add<StrongAttack>("StrongAttack");
			m_SM->Add<MagicProjectile>("MagicProjectile");
			m_SM->Add<MagicArea>("MagicArea");
			m_SM->Add<TakeDamage>("TakeDamage");
			m_SM->Add<Evade>("Evade");
			m_SM->Add<Death>("Death");
			m_SM->init("Idle");
			m_SM->dispatch(sm::Init());
		}
	}
}
