#include "PlayerController.h"
#include "PlayerAnimator.h"
#include "PlayerLocomotion.h"
#include "Input/InputManager.h"
#include "Components/Camera/ThirdPersonCamera.h"
#include "Camera/ThirdPersonCameraController.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Common/Life.h"
#include "Cal3D/src/cal3d/bone.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "PlayerAttackController.h"
#include "Input/ActionManager.h"
#include "Graphics/Cal3d/AnimatedCoreModel.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include "Components/GUI/ImgFader.h"
#include "Components/GUI/InGameMenuGameOver.h"
#include "Utils/Coroutine.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>


namespace logic
{
	using namespace player;

	namespace components
	{
		CPlayerController::CPlayerController()
			: m_CanControl(true),
			  m_PlayerLocomotion(nullptr),
			  m_PlayerAttackController(nullptr), 
			m_Animator(nullptr),
			  m_Life(nullptr)
		{
		}

		CPlayerController::CPlayerController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			  m_CanControl(true),
			  m_PlayerLocomotion(nullptr),
			  m_PlayerAttackController(nullptr), 
			m_Animator(nullptr),
			  m_Life(nullptr)
		{
		}

		CPlayerController::~CPlayerController()
		{
			
		}

		Mat44f CPlayerController::GetBoneMatrix(const std::string& boneName) const
		{
			CalBone * bone = static_cast<cal3dimpl::CSceneAnimatedModel*>(m_Parent)->getBone(boneName);

			Mat44f TranslationMatrix;
			TranslationMatrix.SetIdentity();
			CalVector translation = bone->getTranslationAbsolute();
			Vect4f fpos = Vect4f(translation.x, translation.y, translation.z, 1.0f);
			TranslationMatrix.SetPos(fpos.x, fpos.y, fpos.z);

			Mat44f RotationMatrix;
			CalQuaternion calquat = bone->getRotationAbsolute();
			Quatf l_Quaternion = Quatf(calquat.x, calquat.y, calquat.z, calquat.w);
			RotationMatrix.SetIdentity();
			RotationMatrix.SetRotByQuat(l_Quaternion);

			Mat44f ScaleMatrix;
			ScaleMatrix.SetIdentity();
			ScaleMatrix.Scale(1, 1, 1);

			Mat44f AbsoluteMatrix, lTransform;
			AbsoluteMatrix.SetIdentity();
			lTransform.SetIdentity();
			AbsoluteMatrix = ScaleMatrix*RotationMatrix*TranslationMatrix;
			lTransform = AbsoluteMatrix * m_Parent->GetMatrix();

			return lTransform;
		}

		void CPlayerController::Start()
		{
			m_PlayerLocomotion = m_Parent->GetComponent<CPlayerLocomotion>();
			m_PlayerAttackController = m_Parent->GetComponent<CPlayerAttackController>();
			m_Life = m_Parent->GetComponent<CLife>();
			m_Animator = m_Parent->GetComponent<CPlayerAnimator>();
		}

		
		void CPlayerController::Update(float dt)
		{
			//lock camera for debug

			if (m_Engine.GetActionManager().IsInputActionActive("GodMode"))
			{
				m_Life->SetGodMode(!m_Life->GetGodMode());
			}

			if (m_Engine.GetInputManager().KeyBecomesPressed(VK_ESCAPE))
			{
				if (m_CanControl)
				{
					DisableAllControlls();
				}
				else
				{
					EnableAllControlls();
				}
				//disable camera interaction
				CThirdPersonCameraController* cam = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::maincamera)->GetComponent<CThirdPersonCameraComponent>()->GetCamera();
				cam->SetLock(!cam->GetLock());
			}
			//send death event if life <= 0
			if (m_Life->GetLifePoints() <= 0)
			{
				Dead();
			}
		}

		void CPlayerController::OnDestroy()
		{
		}

		void CPlayerController::DisableAllControlls()
		{
			m_CanControl = false;
			m_PlayerLocomotion->SetEnabled(false);
			m_PlayerAttackController->SetEnabled(false);
		}

		void CPlayerController::EnableAllControlls()
		{
			m_CanControl = true;
			m_PlayerLocomotion->SetEnabled(true);
			m_PlayerAttackController->SetEnabled(true);
		}

		bool CPlayerController::CanControl() const
		{
			return m_CanControl;
		}

		void CPlayerController::Dead() const
		{
			m_Animator->SendEvent<sm::Dead>(sm::Dead());
			m_Parent->DisableComponents();
			//disable all components in enemies
			for (CSceneNode* node : m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNodesByTag(CSceneNode::enemy))
			{
				node->DisableComponents();
			}
			//show gameover menu
			base::utils::CCoroutine::GetInstance().StartCoroutine(6.0f, []()
			{
				CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("InGameMenuGameOver")->SetEnabled(true);
			});
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_DIE_PLAYER, m_Parent);
		}
	}
}
