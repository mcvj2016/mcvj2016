#ifndef PLAYERMAGICATTACKCONTROLLER_H
#define PLAYERMAGICATTACKCONTROLLER_H
#include "Core/ScriptComponent.h"

class CCameraController;
namespace logic
{
	namespace player
	{
		class CPlayerAnimator;
	}
}
using namespace logic::player;

namespace logic
{
	namespace components
	{
		class CPlayerLocomotion;
		class CPlayerMagicAttackController : public CScriptComponent
		{
		public:
			CPlayerMagicAttackController();
			CPlayerMagicAttackController(const std::string& name, const std::string& parent);
			~CPlayerMagicAttackController();

			void Start() override;
			void ProjectileUpdate(float dt);
			void Update(float dt) override;
			const std::string m_ImpactPreviewName, m_MagicProjectileName;
			CSceneNode* m_ImpactPreviewPoint;
			CSceneNode* m_MagicProjectile;
		private:
			CPlayerLocomotion* m_PlayerLocomotion;
			CCameraController* m_PrevCamera;
			CCameraController* m_Camera;
			CPlayerAnimator* m_Animator;
			float m_Magic1Cooldown, m_Magic2Cooldown;
			bool m_Magic1InCooldown, m_Magic2InCooldown;
			const std::string m_AttackMagicName1, m_AttackMagicName2, m_AimCamName, m_LeftHandBoneName;
			
		};
	}
}


#endif