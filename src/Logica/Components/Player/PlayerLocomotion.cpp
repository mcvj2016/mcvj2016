#include "PlayerLocomotion.h"
#include "Engine.h"
#include "Input/ActionManager.h"
#include "Camera/CameraManager.h"
#include "PhysXImpl/PhysXManager.h"
#include "PlayerController.h"
#include "Utils/Coroutine.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "PlayerAnimator.h"


namespace logic
{
	namespace components
	{
		CPlayerLocomotion::CPlayerLocomotion()
			: CScriptComponent(),
			  m_Camera(nullptr),
			  m_Animator(nullptr),
			  m_playerController(nullptr),
			  m_WalkSpeed(0), m_RunSpeed(0)
		{
		}

		CPlayerLocomotion::CPlayerLocomotion(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			  m_Camera(nullptr),
			  m_Animator(nullptr),
			  m_playerController(nullptr),
			  m_WalkSpeed(0), m_RunSpeed(0)
		{
		}

		CPlayerLocomotion::~CPlayerLocomotion()
		{
		}

		void CPlayerLocomotion::Start()
		{
			m_Animator = m_Parent->GetComponent<CPlayerAnimator>();
		}

		void CPlayerLocomotion::Update(float dt)
		{
			
			m_Camera = CEngine::GetInstance().GetCameraManager().GetMainCamera();
			if (!m_LockPlayer){
				MovePlayer(dt);
			}
			
		}

		void CPlayerLocomotion::MovePlayer(float dt) const
		{
			if (m_Engine.GetActionManager().IsInputActionActive(m_MoveForwardName) ||
				m_Engine.GetActionManager().IsInputActionActive(m_MoveRightLeftName))
			{
				float valueForward = m_Engine.GetActionManager().GetInputActionValue(m_MoveForwardName);
				float valueRight = m_Engine.GetActionManager().GetInputActionValue(m_MoveRightLeftName);
				float speed = m_WalkSpeed;
				Vect3f dir = m_Camera->GetSceneCamera()->GetForward().GetNormalized() * valueForward + m_Camera->GetSceneCamera()->GetRight().GetNormalized() * valueRight;
				dir = dir.GetNormalized();
				//not move up like flying
				dir.y = 0.0;
				//tell animator to change states				
				//check for running
				if (m_Engine.GetActionManager().IsInputActionActive(m_RunName))
				{
					speed = m_RunSpeed;
					if (m_Animator->GetActualIndex() != sm::RunIndex)
					{
						m_Animator->SendEvent<sm::Run>(sm::Run());
					}
				}
				//else check for walk
				else if (m_Animator->GetActualIndex() != sm::WalkIndex)
				{
					if (m_Animator->GetActualIndex() == sm::RunIndex)
					{
						m_Animator->SetGoingToRun(true);
						m_Animator->SendEvent<sm::Stop>(sm::Stop());
					}
					m_Animator->SendEvent<sm::Move>(sm::Move());
				}

				//traslate the player with physx
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
				m_Engine.GetPhysXManager().MoveCharacter(m_Parent->GetName(), m_Parent->GetForward(), speed, dt);
				m_Parent->SetForward(dir);
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
			}
			else
			{
				//performance check
				if (m_Animator->GetActualIndex() != sm::IdleIndex)
				{
					m_Animator->SendEvent<sm::Stop>(sm::Stop());
				}
			}

		}

		void CPlayerLocomotion::OnDestroy()
		{
		}

		void CPlayerLocomotion::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_WalkSpeed = strtof(membersValues[0].c_str(), nullptr);
			m_RunSpeed = strtof(membersValues[1].c_str(), nullptr);
			m_MoveForwardName = membersValues[2];
			m_MoveRightLeftName = membersValues[3];
			m_RunName = membersValues[4];
		}

		void CPlayerLocomotion::SetLockPlayer(bool l_lockplayer)
		{
			m_LockPlayer = l_lockplayer;
		}
	}
}

