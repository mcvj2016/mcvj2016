#ifndef _LOGICA_PLAYERLOCOMOTION_H
#define _LOGICA_PLAYERLOCOMOTION_H
#include "Core/ScriptComponent.h"

class CCameraController;

namespace logic
{
	namespace player
	{
		class CPlayerAnimator;
	}
}
using namespace logic::player;

namespace logic
{
	namespace components
	{
		
		class CPlayerController;
		class CPlayerLocomotion : public CScriptComponent
		{
		public:
			CPlayerLocomotion();
			CPlayerLocomotion(const std::string& name, const std::string& parent);
			~CPlayerLocomotion();

			void Start() override;
			void Update(float dt) override;
			void MovePlayer(float dt) const;
			void OnDestroy() override;
			void InitMembersValues(std::vector<std::string> membersValues) override;

			void SetLockPlayer(bool l_lockplayer);

		private:
			CCameraController* m_Camera;
			CPlayerAnimator* m_Animator;
			CPlayerController* m_playerController;
			std::string m_MoveRightLeftName, m_MoveForwardName, m_RunName;
			float m_WalkSpeed, m_RunSpeed;
			bool m_LockPlayer = false; //locks movement and camera
		};
	}
}

#endif