#include "PlayerInventory.h"

namespace logic
{	namespace components
	{
		CPlayerInventory::CPlayerInventory():
			m_hasKey(false),
			m_hasChest(false),
			m_hasBoots(false),
			m_hasBucket(false),
			m_hasRope(false),
			m_hasHelm(false)
		{
		}

		CPlayerInventory::~CPlayerInventory()
		{
		}

	}
}
