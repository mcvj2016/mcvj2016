#ifndef PLAYERANIMATOR_H
#define PLAYERANIMATOR_H

#include "Components/Common/Animator.h"

using namespace logic::components;

namespace logic
{
	namespace player
	{
		class CPlayerAnimator : public CModelAnimator
		{
		public:
			CPlayerAnimator();

			CPlayerAnimator(const std::string& name, const std::string& parent);

			~CPlayerAnimator();

			void Start() override;
			GET_SET(bool, ComboAttack)
			GET_SET(bool, GoingToRun)
		private:
			bool m_ComboAttack, m_GoingToRun;
		};
	}
}
#endif
