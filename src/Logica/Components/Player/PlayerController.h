#ifndef _LOGICA_PLAYERCONTROLLER_H
#define _LOGICA_PLAYERCONTROLLER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace player
	{
		class CPlayerAnimator;
	}
}
using namespace logic::player;

namespace logic
{
	namespace components
	{
		class CPlayerLocomotion;
		class CPlayerAttackController;
		class CLife;
		class CPlayerController : public CScriptComponent
		{
		public:
			CPlayerController();
			CPlayerController(const std::string& name, const std::string& parent);
			virtual ~CPlayerController();

			Mat44f GetBoneMatrix(const std::string& boneName) const;

			void Start() override;
			void Update(float dt) override;
			void OnDestroy() override;
			void DisableAllControlls();
			void EnableAllControlls();
			bool CanControl() const;
			void Dead() const;
		private:
			CLife * m_Life;
			bool m_CanControl;
			CPlayerLocomotion* m_PlayerLocomotion;
			CPlayerAttackController* m_PlayerAttackController;
			CPlayerAnimator* m_Animator;
		};
	}
}

#endif