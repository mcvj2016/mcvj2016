#include "PlayerAttackController.h"
#include "Input/ActionManager.h"
#include "PlayerLocomotion.h"
#include "PlayerAnimator.h"
#include "PhysXImpl/PhysXManager.h"
#include "Utils/Coroutine.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"

namespace logic
{
	using namespace player;
	namespace components
	{
		CPlayerAttackController::CPlayerAttackController()
			: 
			CScriptComponent(), 
			m_PlayerLocomotion(nullptr), 
			m_AttackName("Attack"), 
			m_StrongAttackName("StrongAttack"), 
			m_EvadeName("Evade"),
			m_Animator(nullptr)
		{
		}

		CPlayerAttackController::CPlayerAttackController(const std::string& name, const std::string& parent)
			: 
			CScriptComponent(name, parent), 
			m_PlayerLocomotion(nullptr), 
			m_AttackName("Attack"), 
			m_StrongAttackName("StrongAttack"),
			m_EvadeName("Evade"),
			m_Animator(nullptr)
		{
		}

		CPlayerAttackController::~CPlayerAttackController()
		{
		}

		void CPlayerAttackController::Start()
		{
			m_PlayerLocomotion = m_Parent->GetComponent<CPlayerLocomotion>();
			m_Animator = m_Parent->GetComponent<CPlayerAnimator>();
		}

		void CPlayerAttackController::Update(float dt)
		{
			int actualIndex = m_Animator->GetActualIndex();
			if (actualIndex != sm::Damage1Index && actualIndex != sm::Damage2Index)
			{
				//normal attack
				if (m_Engine.GetActionManager().IsInputActionActive(m_AttackName))
				{
					//check if we are performing a combo or its a single attack
					if (actualIndex == sm::AnimIndex::Attack1Index || actualIndex == sm::AnimIndex::Attack2Index)
					{
						m_Animator->SetComboAttack(true);
					}
					else
					{
						m_Animator->SendEvent<sm::Attack>(sm::Attack());
					}
				}
				//Strong attack
				else if (m_Engine.GetActionManager().IsInputActionActive(m_StrongAttackName))
				{
					m_Animator->SendEvent<sm::StrongAtack>(sm::StrongAtack());
				}
				//Evade
				else if (m_Engine.GetActionManager().IsInputActionActive(m_EvadeName))
				{
					if (m_Animator->GetActualIndex() != sm::EvadeIndex)
					{
						float corutineLife = m_Animator->GetModel()->GetCurretAnimation(m_Animator->GetActualIndex())->getDuration();
						base::utils::CCoroutine::GetInstance().StartCoroutineParallel(
							corutineLife, //corutine life time, equal than the evade anim length
							[this, corutineLife](float dt, float totalTime)
							{
								if (totalTime >= 0.3f && totalTime < 0.93f) //start moving the model with physx
								{
									m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
									m_Engine.GetPhysXManager().MoveCharacter(m_Parent->GetName(), m_Parent->GetBackward().GetNormalized(), 0.1f, dt);
									m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
								}
							}
						);
					}

					m_Animator->SendEvent<sm::Evade>(sm::Evade());
				}
			}

			//check if performing a locomotion blocking action
			if (m_Animator->GetActualIndex() == sm::AnimIndex::Magic1Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Magic2Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::AimingIndex
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Attack1Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Attack2Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Attack3Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::StrongAttackIndex
				|| m_Animator->GetActualIndex() == sm::AnimIndex::EvadeIndex
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Damage1Index
				|| m_Animator->GetActualIndex() == sm::AnimIndex::Damage2Index)
			{
				m_PlayerLocomotion->SetLockPlayer(true);
			}
			else
			{
				m_PlayerLocomotion->SetLockPlayer(false);
			}

			
		}

		void CPlayerAttackController::OnDestroy()
		{
		}
	}
}
