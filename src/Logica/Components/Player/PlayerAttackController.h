#ifndef PLAYERATTACKCONTROLLER_H
#define PLAYERATTACKCONTROLLER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace player
	{
		class CPlayerAnimator;
	}
	using namespace player;
	namespace components
	{
		class CPlayerLocomotion;
		class CPlayerAttackController : public CScriptComponent
		{
		public:
			CPlayerAttackController();
			CPlayerAttackController(const std::string& name, const std::string& parent);
			~CPlayerAttackController();

			void Start() override;
			void Update(float dt) override;
			void OnDestroy() override;
		private:
			CPlayerLocomotion* m_PlayerLocomotion;
			CPlayerAnimator* m_Animator;
			const std::string m_AttackName;
			const std::string m_StrongAttackName;
			const std::string m_EvadeName;
		};
	}
}


#endif