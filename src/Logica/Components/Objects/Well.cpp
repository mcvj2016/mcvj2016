#include "Well.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CWell::CWell() : CScriptComponent(), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CWell::CWell(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CWell::~CWell()
		{
		}

		void CWell::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
		}

		void CWell::Update(float dt)
		{
			if (m_IsBoxTriggered && m_Engine.GetActionManager().IsInputActionActive("Action"))
			{
				if (CPlayerInventory::GetInstance().m_hasRope && CPlayerInventory::GetInstance().m_hasBucket)
				{
					CPlayerInventory::GetInstance().m_hasHelm = true;
					m_Engine.GetCinematicManager().Play(m_CinematicName);
					m_TextBox->HideControlledTextInformation();
					this->SetEnabled(false);
				}
				else
				{
					m_TextBox->ShowTimedTextInformation(10);
				}
			}
		}

		void CWell::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = true;
				m_TextBox->ShowControlledTextInformation(3);
			}
		}

		void CWell::OnTriggerExit(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = false;
				m_TextBox->HideControlledTextInformation();
			}
		}

		void CWell::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_CinematicName = membersValues[0];
		}
	}
}
