#include "Bucket.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CBucket::CBucket() : CScriptComponent(), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CBucket::CBucket(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CBucket::~CBucket()
		{
		}

		void CBucket::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
			m_Cubeta = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cubo");
		}

		void CBucket::Update(float dt)
		{
			if (m_IsBoxTriggered && m_Engine.GetActionManager().IsInputActionActive("Action"))
			{
				CPlayerInventory::GetInstance().m_hasBucket = true;
				m_Parent->SetVisible(false);
				m_TextBox->ShowTimedTextInformation(8);
				this->SetEnabled(false);
				Destroy(m_Parent);
				m_Cubeta->SetVisible(false);
				DestroyPhysxOnly(m_Cubeta);
				m_Cubeta = nullptr;
			}
		}

		void CBucket::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = true;
				m_TextBox->ShowControlledTextInformation(3);
			}
		}

		void CBucket::OnTriggerExit(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = false;
				m_TextBox->HideControlledTextInformation();
			}
		}
	}
}
