#include "Rock.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>

namespace logic
{
	namespace components
	{
		CRock::CRock() : CScriptComponent()
		{
			m_Enabled = false;
		}

		CRock::CRock(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
			m_Enabled = false;
		}

		CRock::~CRock()
		{
		}

		void CRock::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::projectile)
			{
				m_Engine.GetCinematicManager().Play(m_CinematicName);
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_TOTEMRUPTURE);
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_ROCKFALL);
				m_Enabled = false;
				Destroy(m_Parent);
			}
		}

		void CRock::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_CinematicName = membersValues[0];
		}
	}
}
