#ifndef _LOGICA_WILDVISION_H
#define _LOGICA_WILDVISION_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"
#include "Graphics/Materials/Material.h"

namespace logic
{
	namespace components
	{
		class CWildvision : public CScriptComponent
		{
		public:
			CWildvision();
			CWildvision(const std::string& name, const std::string& parent);
			virtual ~CWildvision();

			void Start() override;
			void Update(float dt) override;

			void WildVisionState(bool active, float VisionState);
		private:
			bool m_VisionActive;
		};
	}
}
#endif