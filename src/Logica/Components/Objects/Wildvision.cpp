#include "Wildvision.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneMeshQuatRender.h"
#include "Graphics/Meshes/Mesh.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"

namespace logic
{
	namespace components
	{
		CWildvision::CWildvision() : CScriptComponent(), m_VisionActive(false)
		{
		}

		CWildvision::CWildvision(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_VisionActive(false)
		{
		}

		CWildvision::~CWildvision()
		{
		}

		void CWildvision::Start()
		{
			if (m_Parent->GetName() == "POZO" || m_Parent->GetName() == "Cubo" ||
				m_Parent->GetName() == "Cuerda" || m_Parent->GetName() == "Rock_4_Cube_002_Rock002")
			{
				m_Enabled = false;
			}
		}

		void CWildvision::Update(float dt)
		{
			if (!m_VisionActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionstart"))
			{
				/*m_VisionActive = true;
				CSceneMeshQuatRender* mesh = static_cast<CSceneMeshQuatRender*>(m_Parent);
				static_cast<CTemplatedMaterialParameter<float>*>(
					mesh->GetMesh()->GetMaterials()[0]->GetParameter("wild_perception"))
					->SetValue(1.0f);*/
				WildVisionState(true,1.0f);
			}
			else if(m_VisionActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionend") )
			{
				/*m_VisionActive = false;
				CSceneMeshQuatRender* mesh = static_cast<CSceneMeshQuatRender*>(m_Parent);
				static_cast<CTemplatedMaterialParameter<float>*>(
					mesh->GetMesh()->GetMaterials()[0]->GetParameter("wild_perception"))
					->SetValue(0.0f);*/
				WildVisionState(false, 0.0f);
			}
		}

		void CWildvision::WildVisionState(bool active, float VisionState)
		{
			m_VisionActive = active;
			CSceneMeshQuatRender* mesh = static_cast<CSceneMeshQuatRender*>(m_Parent);
			static_cast<CTemplatedMaterialParameter<float>*>(
				mesh->GetMesh()->GetMaterials()[0]->GetParameter("wild_perception"))
				->SetValue(VisionState);
		}
	}
}
