#ifndef _LOGICA_WELL_H
#define _LOGICA_WELL_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"

namespace logic
{
	namespace components
	{
		class CWell : public CScriptComponent
		{
		public:
			CWell();
			CWell(const std::string& name, const std::string& parent);
			virtual ~CWell();

			void Start() override;
			void Update(float dt) override;
			void OnTriggerEnter(CSceneNode* other) override;
			void OnTriggerExit(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;
		private:
			CGameTextBox* m_TextBox;
			std::string m_CinematicName;
			bool m_IsBoxTriggered;
		};
	}
}
#endif