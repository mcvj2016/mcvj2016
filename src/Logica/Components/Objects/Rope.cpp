#include "Rope.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CRope::CRope() : CScriptComponent(), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CRope::CRope(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_IsBoxTriggered(false)
		{
			m_Enabled = false;
		}

		CRope::~CRope()
		{
		}

		void CRope::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
			m_Cuerda = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cuerda");
		}

		void CRope::Update(float dt)
		{
			if (m_IsBoxTriggered && m_Engine.GetActionManager().IsInputActionActive("Action"))
			{
				CPlayerInventory::GetInstance().m_hasRope = true;
				m_Parent->SetVisible(false);
				m_TextBox->ShowTimedTextInformation(9);
				this->SetEnabled(false);
				Destroy(m_Parent);
				Destroy(m_Cuerda);
				m_Cuerda = nullptr;
			}
		}

		void CRope::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = true;
				m_TextBox->ShowControlledTextInformation(3);
			}
		}

		void CRope::OnTriggerExit(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = false;
				m_TextBox->HideControlledTextInformation();
			}
		}
	}
}
