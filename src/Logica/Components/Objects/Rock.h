#ifndef _LOGICA_ROCK_H
#define _LOGICA_ROCK_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CRock : public CScriptComponent
		{
		public:
			CRock();
			CRock(const std::string& name, const std::string& parent);
			virtual ~CRock();

			void OnTriggerEnter(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			std::string m_CinematicName;
		};
	}
}
#endif