#ifndef _LOGICA_BOOTS_H
#define _LOGICA_BOOTS_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"

namespace logic
{
	namespace components
	{
		class CBoots : public CScriptComponent
		{
		public:
			CBoots();
			CBoots(const std::string& name, const std::string& parent);
			virtual ~CBoots();

			void Start() override;
			void OnTriggerEnter(engine::scenes::CSceneNode* other) override;

		private:
			CGameTextBox* m_TextBox;
		};
	}
}
#endif