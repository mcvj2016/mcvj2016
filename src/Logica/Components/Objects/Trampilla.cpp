#include "Trampilla.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CTrampilla::CTrampilla() : CScriptComponent(), m_IsBoxTriggered(false)
		{
		}

		CTrampilla::CTrampilla(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_IsBoxTriggered(false)
		{
		}

		CTrampilla::~CTrampilla()
		{
		}

		void CTrampilla::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
		}

		void CTrampilla::Update(float dt)
		{
			if (m_IsBoxTriggered && m_Engine.GetActionManager().IsInputActionActive("Action"))
			{
				if (CPlayerInventory::GetInstance().m_hasKey)
				{
					m_Engine.GetCinematicManager().Play(m_CinematicName);
					m_TextBox->HideControlledTextInformation();
					this->SetEnabled(false);
					DestroyPhysxOnly(m_Parent);
				}
				else
				{
					m_TextBox->ShowTimedTextInformation(5);
				}
			}
		}

		void CTrampilla::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = true;
				m_TextBox->ShowControlledTextInformation(3);
			}
		}

		void CTrampilla::OnTriggerExit(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				m_IsBoxTriggered = false;
				m_TextBox->HideControlledTextInformation();
			}
		}

		void CTrampilla::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_CinematicName = membersValues[0];
		}
	}
}
