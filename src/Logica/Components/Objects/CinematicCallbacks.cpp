#include "CinematicCallbacks.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"
#include "PhysXImpl/PhysXManager.h"
#include "Chest.h"
#include "Boots.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Components/Player/PlayerController.h"
#include "Components/Player/PlayerAnimator.h"
#include "Camera/CameraManager.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Components/GUI/Inventory.h"
#include "Components/Common/AmbientSoundManager.h"
#include "Components/Common/EnemiesManager.h"
#include "Components/Enemies/EnemyNotifier.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>
#include "Graphics/Scenes/PrefabManager.h"
#include "Components/Enemies/EnemyGroupManager.h"
#include "Components/Enemies/EnemyTag.h"
#include "Components/Enemies/Gulak/GulakActivator.h"
#include "Components/Enemies/Gulak/GulakCombatEnemies.h"
#include "Components/Camera/ThirdPersonCamera.h"
#include "Camera/ThirdPersonCameraController.h"
#include "Components/Camera/AimingCamera.h"
#include "Components/Weapon/PlayerMagicProjectile.h"

namespace logic
{
	namespace components
	{
		CCinematicCallback::CCinematicCallback()
			: CScriptComponent(),
			  m_CinematicName(""), m_AcumTime(0.0f), m_NoiseAndVignetting(nullptr), m_TextBox(nullptr), m_InventoryGUI(nullptr), m_LifeBarGUI(nullptr), m_SoundManager(nullptr), m_EnemiesManager(nullptr),
			  m_Cin2CallbackInvoked(false), m_Cin2ShowOrcInvoked(false),
			  m_Cin1CallbackInvoked(false),
			  m_Cin3EnemiesGroupManager(nullptr),
			  m_Cin1DisableWildVision(false)
		{
			m_Enabled = false;
		}

		CCinematicCallback::CCinematicCallback(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			  m_CinematicName(""), 
			m_AcumTime(0.0f), 
			m_NoiseAndVignetting(nullptr), 
			m_TextBox(nullptr), 
			m_InventoryGUI(nullptr), 
			m_LifeBarGUI(nullptr), 
			m_SoundManager(nullptr), 
			m_EnemiesManager(nullptr),
			m_Cin3EnemiesGroupManager(nullptr),
			m_Cin1CallbackInvoked(false),
			m_Cin2CallbackInvoked(false),
			m_Cin2ShowOrcInvoked(false),
			m_Cin1DisableWildVision(false)
		{
			m_Enabled = false;
		}

		CCinematicCallback::~CCinematicCallback()
		{
		}

		void CCinematicCallback::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
			m_LifeBarGUI = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Vida")
				->GetComponent<CLifeBar>();
			m_InventoryGUI = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Inventario")
				->GetComponent<CInventory>();
			m_NoiseAndVignetting = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("NoiseVignetting")
				->GetComponent<CNoiseVignetting>();
			m_Noise = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Noise")
				->GetComponent<CNoise>();
			m_WildVision = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Z1_roca_goblin")
				->GetComponent<CWildvision>();

			InitCin6Vector();
			m_Engine.GetCinematicManager().m_CinematicCallback = this;
			m_SoundManager = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")->GetComponent<CAmbientSoundManager>();
			m_EnemiesManager = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")->GetComponent<CEnemiesManager>();
			m_Cin3EnemiesGroupManager = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("EnemyGroupCin3Manager")->GetComponent<CEnemyGroupManager>();
		}

		void CCinematicCallback::CinematicInit()
		{
			if (m_CinematicName != "cin6")
			{
				m_LifeBarGUI->SetEnabled(false);
				m_InventoryGUI->SetEnabled(false);
				SaveCamera();
				DisablePlayer();
			}

			if (m_CinematicName == "cin2")
			{
				Cin2Init();
			} 
			else if (m_CinematicName == "cin3")
			{
				Cin3Init();
			}
			else if (m_CinematicName == "cin5")
			{
				Cin5Init();
			}
		}

		void CCinematicCallback::Cin2Init()
		{
			static_cast<CSceneAnimatedModel*>(m_Engine.GetSceneManager().GetCurrentScene()
				->GetSceneNode("orcJefe1Cin2"))->HideAnimated();
			//change track according to cinematic. the enemy notifier of the orc will change the track again
			base::utils::CCoroutine::GetInstance().StartCoroutine(6.0f, [this]()
			{
				//register on this point the orcboss, but set as registered on the notifier component for not registering twice
				m_EnemiesManager->AddEnemyActive();
				cal3dimpl::CSceneAnimatedModel * orcjefe1 = static_cast<cal3dimpl::CSceneAnimatedModel *>(
					m_Engine.GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("OrcoJefe1"));
				orcjefe1->GetComponent<CEnemyNotifier>()->SetEnemyActivated(true);
			});
		}

		void CCinematicCallback::Cin3Init()
		{
			//register on this point the orc1, but set as registered on the notifier component for not registering twice
			m_EnemiesManager->AddEnemyActive();
			cal3dimpl::CSceneAnimatedModel * orc = static_cast<cal3dimpl::CSceneAnimatedModel *>(
				m_Engine.GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("OrcWarrior1"));
			orc->GetComponent<CEnemyNotifier>()->SetEnemyActivated(true);
		}

		void CCinematicCallback::Cin5Init()
		{
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("cuerda_inclinada")->SetVisible(true);
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("cuerda_cubo")->SetVisible(true);
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("cuerda_vertical")->SetVisible(true);
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cubo")->SetVisible(true);
		}

		void CCinematicCallback::Update(float dt)
		{
			m_AcumTime += dt;
			CallbackUpdate(dt);
		}

		void CCinematicCallback::CallbackUpdate(float dt)
		{
			if (m_CinematicName == "cin1")
			{
				Cin1Update(dt);
			}
			else if (m_CinematicName == "cin2")
			{
				Cin2Update(dt);
			}
			else if (m_CinematicName == "cin3")
			{
				Cin3Update(dt);
			}
			else if (m_CinematicName == "cin6")
			{
				Cin6Update(dt);
			}
		}

		void CCinematicCallback::Cin1Update(float dt)
		{
			if (m_AcumTime > 0.0f && !m_Cin1CallbackInvoked)
			{
				m_NoiseAndVignetting->m_ForCinematic = true;
				m_Noise->NoiseState(true);
				m_WildVision->WildVisionState(true, 1.0f);
				m_Cin1CallbackInvoked = true;
			}
			else if (m_AcumTime > 4.041f && !m_Cin1DisableWildVision)
			{
				m_NoiseAndVignetting->m_ForCinematic = false;
				m_Noise->NoiseState(false);
				m_WildVision->WildVisionState(false, 0.0f);
				m_Cin1DisableWildVision = true;
			}
		}

		void CCinematicCallback::Cin2Update(float dt)
		{
			if (m_AcumTime > 2.0f && !m_Cin2ShowOrcInvoked)
			{
				static_cast<CSceneAnimatedModel*>(m_Engine.GetSceneManager().GetCurrentScene()
					->GetSceneNode("orcJefe1Cin2"))->ShowAnimated();
				m_Cin2ShowOrcInvoked = true;
			}
			if (m_AcumTime > 17.4f && !m_Cin2CallbackInvoked)
			{
				m_TextBox->ShowControlledTextInformation(1);
				m_Cin2CallbackInvoked = true;
			}
		}

		bool gobCin3SoundPlayer = false;
		bool orcCin3SoundPlayer = false;
		void CCinematicCallback::Cin3Update(float dt)
		{
			if (m_AcumTime > 8.3f && !gobCin3SoundPlayer)
			{
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HEAL_GOB, CEngine::GetInstance().GetCameraManager().GetMainCamera()->GetSceneCamera());
				gobCin3SoundPlayer = true;
			}
			if (m_AcumTime > 8.60f && !orcCin3SoundPlayer)
			{
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_ATTACK_ORC, CEngine::GetInstance().GetCameraManager().GetMainCamera()->GetSceneCamera());
				orcCin3SoundPlayer = true;
			}
		}

		void CCinematicCallback::Cin6Update(float dt)
		{
			CPhysXManager& lPhysxManager = m_Engine.GetInstance().GetPhysXManager();
			for (CSceneNode* node : m_Cin6Nodes)
			{
				Mat44f objectMatrix = node->GetMatrixQuat();
				lPhysxManager.setKinematicTarget(node->GetName(), objectMatrix);
			}
		}

		void CCinematicCallback::CinematicEnd()
		{
			if (m_CinematicName != "cin6")
			{
				m_LifeBarGUI->SetEnabled(true);
				m_InventoryGUI->SetEnabled(true);
				RestoreCamera();
				EnablePlayer();
			}

			if (m_CinematicName == "cin1") //roca goblin
			{
				Cin1End();
			}
			else if (m_CinematicName == "cin2") //boss 1
			{
				Cin2End();
			}
			else if (m_CinematicName == "cin3") //orcos/goblins muerte boss1
			{
				Cin3End();
			}
			else if (m_CinematicName == "cin4") //trampilla
			{
				Cin4End();
			}
			else if (m_CinematicName == "cin5") //pozo
			{
				Cin5End();
			}
			else if (m_CinematicName == "cin6") //totem
			{
				Cin6End();
			}
		}

		void CCinematicCallback::Cin1End()
		{
			m_NoiseAndVignetting->m_ForCinematic = false;
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				1.0f,
				[this]()
			{
				m_TextBox->ShowTimedTextInformation(0);
			}
			);
		}

		void CCinematicCallback::Cin2End()
		{
			m_TextBox->HideControlledTextInformation();
			cal3dimpl::CSceneAnimatedModel * orcjefe1 = static_cast<cal3dimpl::CSceneAnimatedModel *>(
				m_Engine.GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("OrcoJefe1") );
			CSceneNode* vidajefe1 = m_Engine.GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("VidaJefe1");
			orcjefe1->ShowAnimated();
			orcjefe1->SetEnabled(true);
			vidajefe1->SetEnabled(true);
		}

		void CCinematicCallback::Cin3End()
		{
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				1.0f,
				[this]()
				{
					m_TextBox->ShowTimedTextInformation(2);
				}
			);
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				5.5f,
				[this]()
				{
					m_TextBox->ShowTimedTextInformation(12);
				}
			);

			for (CSceneNode* node : m_Cin3EnemiesGroupManager->GetEnemiesGroup())
			{
				CSceneAnimatedModel * enemy = static_cast<CSceneAnimatedModel *>(node);
				enemy->ShowAnimated();
				enemy->SetEnabled(true);
			}
			//add a callback function for enabling all other enemies except gulak
			m_Cin3EnemiesGroupManager->SetCallbackOnEnemiesDead([]()
			{
				auto scene = CEngine::GetInstance().GetSceneManager().GetCurrentScene();
				auto group1 = scene->GetSceneNode("GulakGroup1")->GetComponent<CGulakCombatEnemies>();
				auto group2 = scene->GetSceneNode("GulakGroup2")->GetComponent<CGulakCombatEnemies>();
				for (CSceneNode* node : scene->GetSceneNodesByTag(CSceneNode::enemy))
				{
					if (node->GetComponent<CEnemyTag>() != nullptr //added check for ignore cinematic enemies
						&& node->GetComponent<CEnemyTag>()->GetTag() != CEnemyTag::Gulak
						&& node->GetComponent<CEnemyTag>()->GetTag() != CEnemyTag::OrcBoss)
					{
						bool inGulakGroup = false; //find in groups
						for (size_t i = 0; i < group1->GetEnemies().size() && !inGulakGroup; ++i)
						{
							if (group1->GetEnemies().at(i)->GetName() == node->GetName())
							{
								inGulakGroup = true;
							}
						}
						for (size_t i = 0; i < group2->GetEnemies().size() && !inGulakGroup; ++i)
						{
							if (group2->GetEnemies().at(i)->GetName() == node->GetName())
							{
								inGulakGroup = true;
							}
						}

						if (!inGulakGroup)
						{
							CSceneAnimatedModel * enemy = static_cast<CSceneAnimatedModel *>(node);
							enemy->ShowAnimated();
							enemy->SetEnabled(true);
						}
					}
				}
			});
		}

		void CCinematicCallback::Cin4End()
		{
			CSceneNode * node = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("TrampillaPhysx");
			node->m_Position = Vect3f(-98.5f, 1.0f, -20.1153f);
			CEngine::GetInstance().GetPhysXManager().setKinematicTarget(node->GetName(), node->GetMatrix());
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Chest")->GetComponent<CChest>()->SetEnabled(true);
		}

		void CCinematicCallback::Cin5End()
		{
			CScene* scene = m_Engine.GetSceneManager().GetCurrentScene();
			//switch orkelf models with armor
			CSceneAnimatedModel* orkelf = static_cast<CSceneAnimatedModel*>(scene->GetFirstSceneNodeByTag(CSceneNode::player));
			CSceneAnimatedModel* orkelfArmor = static_cast<CSceneAnimatedModel*>(scene->GetFirstSceneNodeByTag(CSceneNode::player2));
			orkelf->SetEnabled(false);
			orkelf->HideAnimated();
			orkelfArmor->SetEnabled(true);
			orkelfArmor->ShowAnimated();
			//set it in the original pos
			m_Engine.GetPhysXManager().SetCharacterControllerPosition(orkelfArmor->GetName(), orkelf->GetPosition());
			//traspase life to new model
			orkelfArmor->GetComponent<CLife>()->SetLifePoints(orkelf->GetComponent<CLife>()->GetLifePoints());
			scene->GetSceneNode("Vida")->GetComponent<CLifeBar>()->SetPlayerLife(orkelfArmor->GetComponent<CLife>());
			//set player to the magic components
			CSCeneCamera* cam = static_cast<CSCeneCamera*>(scene->GetSceneNode("AimingCamera"));
			cam->GetComponent<CAimingCamera>()->SetPlayer(orkelfArmor);
			scene->GetSceneNode("MagicProjectileComposer")->GetComponent<CPlayerMagicProjectile>()->SetPlayer(orkelfArmor);
			//move original player to a position that will never be reached again (initial)
			m_Engine.GetPhysXManager().SetCharacterControllerPosition(orkelf->GetName(), Vect3f(0.0f, 0.0f, 19.6621f));
			//set that model to the camera too and force enable
			cam = static_cast<CSCeneCamera*>(scene->GetSceneNode("MainCamera"));
			cam->GetCamera()->SetLock(false);
			cam->GetComponent<CThirdPersonCameraComponent>()->SetPlayer(orkelfArmor);
			//disable helm on scene
			scene->GetSceneNode("Helm")->SetVisible(false);

			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.2f,
				[this, orkelfArmor]()
				{
					m_TextBox->ShowTimedTextInformation(11);
					m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_ROCKFALL);
				}
			);
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.5f,
				[orkelfArmor]()
				{
					//spawn boss2
					CSceneAnimatedModel* boss = static_cast<CSceneAnimatedModel*>(CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("OrcoJefe2"));
					CEngine::GetInstance().GetPhysXManager().SetCharacterControllerPosition(boss->GetName(), Vect3f(-88.0f, 0.5f, -76.0f));
					boss->ShowAnimated();
					boss->SetEnabled(true);
					boss->GetComponent<CSeeker>()->m_ForceSeek = true;
					//set target to be the new orkelf with armor model
					boss->GetComponent<CSeeker>()->SetTarget(orkelfArmor);
					//VidaJefe2
					CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("VidaJefe2")->SetEnabled(true);
				}
			);
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("cuerda_inclinada")->SetVisible(false);
//			Destroy(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Helm"));
			CSceneNode* node = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Z5_roca_entrada_1");
			m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(node);
			node = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Z5_roca_entrada_2");
			m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(node);
			node = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers");
			node->GetComponent<CGulakActivator>()->SetEnabled(true);
		}

		void CCinematicCallback::Cin6End()
		{
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Boots")->GetComponent<CBoots>()->SetEnabled(true);
		}

		void CCinematicCallback::InitCin6Vector()
		{
			CScene* lScene = m_Engine.GetSceneManager().GetCurrentScene();
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_2_Cube_Rock"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_3_Cube_001_Rock"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock001"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock001"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock002"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock003"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock004"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock005"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock002"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock006"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock007"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock003"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock004"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock008"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock009"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane009"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock005"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock010"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane010"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane011"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("rock-1_"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("rock-1_2"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane012"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane014"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane015"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane016"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock007"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock008"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock011"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_4_Cube_002_Rock012"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock009"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_1_Cube_003_Rock011"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Plane017"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_2_Cube_Rock001"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("Rock_2_Cube_Rock002"));
			m_Cin6Nodes.push_back(lScene->GetSceneNode("rock-1_003"));
		}

		void CCinematicCallback::EnablePlayer()
		{
			cal3dimpl::CSceneAnimatedModel* lPlayer = static_cast<cal3dimpl::CSceneAnimatedModel*>(
				m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::player));
			lPlayer->SetEnabled(true);
			lPlayer->ShowAnimated();
			CPlayerController* m_PlayerController = lPlayer->GetComponent<CPlayerController>();
			m_PlayerController->EnableAllControlls();
			m_PlayerController->GetParent()->GetComponent<CPlayerAnimator>()->SendEvent<logic::sm::Stop>(logic::sm::Stop());
		}

		void CCinematicCallback::DisablePlayer()
		{
			cal3dimpl::CSceneAnimatedModel* lPlayer = static_cast<cal3dimpl::CSceneAnimatedModel*>(
				m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::player));
			lPlayer->SetEnabled(false);
			lPlayer->HideAnimated();
			CPlayerController* m_PlayerController = lPlayer->GetComponent<CPlayerController>();
			m_PlayerController->DisableAllControlls();
		}

		void CCinematicCallback::RestoreCamera()
		{
			if (m_Engine.GetCinematicManager().GetSavedMainCamera() != nullptr){
				m_Engine.GetCameraManager().SetMainCamera(
					m_Engine.GetCinematicManager().GetSavedMainCamera());
			}
			m_Engine.GetCinematicManager().SaveMainCamera(nullptr);
		}

		void CCinematicCallback::SaveCamera()
		{
			m_Engine.GetCinematicManager().SaveMainCamera(
				m_Engine.GetCameraManager().GetMainCamera());
		}
	}
}

