#ifndef _LOGICA_TOTEM_H
#define _LOGICA_TOTEM_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"
#include "Rock.h"
#include "Components/Common/AmbientSoundManager.h"

namespace logic
{
	namespace components
	{
		class CTotem : public CScriptComponent
		{
		public:
			CTotem();
			CTotem(const std::string& name, const std::string& parent);
			virtual ~CTotem();

			void Start() override;
			void Update(float dt) override;
			void OnTriggerEnter(CSceneNode* other) override;

		private:
			CGameTextBox* m_TextBox;
			CRock* m_Rock;
			bool m_IsBoxTriggered;
			CAmbientSoundManager* m_SoundManager;
		};
	}
}
#endif