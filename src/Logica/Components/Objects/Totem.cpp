#include "Totem.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"
#include "Wildvision.h"

namespace logic
{
	namespace components
	{
		CTotem::CTotem() : CScriptComponent(), m_TextBox(nullptr), m_Rock(nullptr), m_IsBoxTriggered(false), m_SoundManager(nullptr)
		{
		}

		CTotem::CTotem(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_TextBox(nullptr), m_Rock(nullptr), m_IsBoxTriggered(false), m_SoundManager(nullptr)
		{
		}

		CTotem::~CTotem()
		{
		}

		void CTotem::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
			m_Rock = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Triger_Rock_4_Cube_002_Rock002")
				->GetComponent<CRock>();
			m_SoundManager = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")->GetComponent<CAmbientSoundManager>();
		}

		void CTotem::Update(float dt)
		{
			
		}

		void CTotem::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{ 
				if (!m_IsBoxTriggered)
				{
					m_Rock->SetEnabled(true);
					m_IsBoxTriggered = true;
					base::utils::CCoroutine::GetInstance().StartCoroutine(
						0.2f,
						[this]()
						{
							m_TextBox->ShowTimedTextInformation(6);
						}
					);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Rock_4_Cube_002_Rock002")->GetComponent<CWildvision>()->SetEnabled(true);
				}
				if (m_SoundManager->GetActiveZone() == CAmbientSoundManager::Zone4)
				{
					m_SoundManager->ChangeZone(CAmbientSoundManager::MainZone);
				}
				else
				{
					m_SoundManager->ChangeZone(CAmbientSoundManager::Zone4);
				}
			}
		}
	}
}
