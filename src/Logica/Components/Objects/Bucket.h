#ifndef _LOGICA_BUCKET_H
#define _LOGICA_BUCKET_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"

namespace logic
{
	namespace components
	{
		class CBucket : public CScriptComponent
		{
		public:
			CBucket();
			CBucket(const std::string& name, const std::string& parent);
			virtual ~CBucket();

			void Start() override;
			void Update(float dt) override;
			void OnTriggerEnter(CSceneNode* other) override;
			void OnTriggerExit(CSceneNode* other) override;

		private:
			CGameTextBox* m_TextBox;
			CSceneNode* m_Cubeta;
			std::string m_CinematicName;
			bool m_IsBoxTriggered;
		};
	}
}
#endif