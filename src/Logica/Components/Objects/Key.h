#ifndef _LOGICA_KEY_H
#define _LOGICA_KEY_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CKey : public CScriptComponent
		{
		public:
			CKey();
			CKey(const std::string& name, const std::string& parent);
			virtual ~CKey();

			void OnTriggerEnter(engine::scenes::CSceneNode* other) override;
			

		private:
			
		};
	}
}
#endif