#include "Key.h"
#include "Components/Player/PlayerInventory.h"

namespace logic
{
	namespace components
	{
		CKey::CKey() : CScriptComponent()
		{
		}

		CKey::CKey(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
		}

		CKey::~CKey()
		{
		}

		void CKey::OnTriggerEnter(CSceneNode* other){
			if (other->GetTag() == CSceneNode::TAG::player){
				CPlayerInventory::GetInstance().m_hasKey = true;
				m_Parent->SetVisible(false);
			}
		}
	}
}
