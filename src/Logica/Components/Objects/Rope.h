#ifndef _LOGICA_ROPE_H
#define _LOGICA_ROPE_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"

namespace logic
{
	namespace components
	{
		class CRope : public CScriptComponent
		{
		public:
			CRope();
			CRope(const std::string& name, const std::string& parent);
			virtual ~CRope();

			void Start() override;
			void Update(float dt) override;
			void OnTriggerEnter(CSceneNode* other) override;
			void OnTriggerExit(CSceneNode* other) override;

		private:
			CGameTextBox* m_TextBox;
			CSceneNode* m_Cuerda;
			std::string m_CinematicName;
			bool m_IsBoxTriggered;
		};
	}
}
#endif