#include "Boots.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Well.h"
#include "Bucket.h"
#include "Rope.h"
#include "Wildvision.h"

namespace logic
{
	namespace components
	{
		CBoots::CBoots() : CScriptComponent()
		{
			m_Enabled = false;
		}

		CBoots::CBoots(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
			m_Enabled = false;
		}

		CBoots::~CBoots()
		{
		}

		void CBoots::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
		}

		void CBoots::OnTriggerEnter(CSceneNode* other){
			if (other->GetTag() == CSceneNode::TAG::player){
				CPlayerInventory::GetInstance().m_hasBoots = true;
				m_TextBox->ShowTimedTextInformation(7);
				m_Parent->SetVisible(false);
				if (CPlayerInventory::GetInstance().m_hasBoots && CPlayerInventory::GetInstance().m_hasChest){
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("PozoTrigger")->GetComponent<CWell>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("CuboTrigger")->GetComponent<CBucket>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("CuerdaTrigger")->GetComponent<CRope>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("POZO")->GetComponent<CWildvision>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cubo")->GetComponent<CWildvision>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cuerda")->GetComponent<CWildvision>()->SetEnabled(true);
				}
				Destroy(m_Parent);
			}
		}
	}
}
