#ifndef _LOGICA_CINEMATICCALLBACK_H
#define _LOGICA_CINEMATICCALLBACK_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"
#include "Components/GUI/Inventory.h"
#include "Components/GUI/LifeBar.h"
#include "Components/GUI/NoiseAndVignetting.h"
#include "Components/GUI/Noise.h"
#include "Wildvision.h"

namespace logic
{
	namespace components
	{
		class CEnemyGroupManager;
		class CEnemiesManager;
		class CAmbientSoundManager;
		class CCinematicCallback : public CScriptComponent
		{
		public:
			CCinematicCallback();
			CCinematicCallback(const std::string& name, const std::string& parent);
			virtual ~CCinematicCallback();			

			void Start() override;
			void Update(float dt) override;

			std::string m_CinematicName;
			float m_AcumTime;

			void CinematicInit();
			void CinematicEnd();

			void DisablePlayer();
			void EnablePlayer();
		private:
			CNoiseVignetting* m_NoiseAndVignetting;
			CNoise* m_Noise;
			CWildvision* m_WildVision;
			CGameTextBox* m_TextBox;
			CInventory* m_InventoryGUI;
			CLifeBar* m_LifeBarGUI;
			std::vector<CSceneNode*> m_Cin6Nodes;
			CAmbientSoundManager* m_SoundManager;
			CEnemiesManager* m_EnemiesManager;
			CEnemyGroupManager* m_Cin3EnemiesGroupManager;

			void InitCin6Vector();
			void RestoreCamera();
			void SaveCamera();

			bool m_Cin1CallbackInvoked;
			bool m_Cin1DisableWildVision;
			bool m_Cin2CallbackInvoked;
			bool m_Cin2ShowOrcInvoked;

			void Cin2Init();
			void Cin3Init();
			void Cin5Init();

			void CallbackUpdate(float dt);
			void Cin1Update(float dt);
			void Cin2Update(float dt);
			void Cin3Update(float dt);
			void Cin6Update(float dt);

			void Cin1End();
			void Cin2End();
			void Cin3End();
			void Cin4End();
			void Cin5End();
			void Cin6End();
		};
	}
}
#endif