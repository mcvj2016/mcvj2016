#ifndef _LOGICA_CHEST_H
#define _LOGICA_CHEST_H
#include "Core/ScriptComponent.h"
#include "Components/GUI/GameTextBox.h"

namespace logic
{
	namespace components
	{
		class CChest : public CScriptComponent
		{
		public:
			CChest();
			CChest(const std::string& name, const std::string& parent);
			virtual ~CChest();

			void Start() override;
			void OnTriggerEnter(engine::scenes::CSceneNode* other) override;

		private:
			CGameTextBox* m_TextBox;
		};
	}
}
#endif