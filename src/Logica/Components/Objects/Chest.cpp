#include "Chest.h"
#include "Components/Player/PlayerInventory.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Rock.h"
#include "Well.h"
#include "Bucket.h"
#include "Rope.h"
#include "Wildvision.h"

namespace logic
{
	namespace components
	{
		CChest::CChest() : CScriptComponent()
		{
			m_Enabled = false;
		}

		CChest::CChest(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
			m_Enabled = false;
		}

		CChest::~CChest()
		{
		}

		void CChest::Start()
		{
			m_TextBox = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::uimsgbox)
				->GetComponent<CGameTextBox>();
		}

		void CChest::OnTriggerEnter(CSceneNode* other){
			if (other->GetTag() == CSceneNode::TAG::player){
				CPlayerInventory::GetInstance().m_hasChest = true;
				m_TextBox->ShowTimedTextInformation(4);
				m_Parent->SetVisible(false);
				if (CPlayerInventory::GetInstance().m_hasBoots && CPlayerInventory::GetInstance().m_hasChest){
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("PozoTrigger")->GetComponent<CWell>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("CuboTrigger")->GetComponent<CBucket>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("CuerdaTrigger")->GetComponent<CRope>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("POZO")->GetComponent<CWildvision>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cubo")->GetComponent<CWildvision>()->SetEnabled(true);
					m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Cuerda")->GetComponent<CWildvision>()->SetEnabled(true);
				}
				Destroy(m_Parent);
			}
		}
	}
}
