#ifndef _SEEKER_H
#define _SEEKER_H
#include "Core/ScriptComponent.h"
#include "Components/Common/Animator.h"
#include "Utils/SubjectObserver/Observer.h"

using namespace base::utils::patterns;

namespace logic
{
	namespace components
	{

		enum CSeekerType
		{
			OrcSeeker = 0,
			OrcBossSeeker,
			GoblinSeeker,
			GulakSeeker
		};

		class CSeeker : public CScriptComponent
		{
		public:
			CSeeker();
			CSeeker(const std::string& name, const std::string& parent);
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void CalculateState();
			void Seek(float dt) const;
			~CSeeker();
			void Start() override;
			void Update(float dt) override;

			GET_SET(CSceneNode*, Target)
			GET_SET(bool, InvertDirection)
			float GetDistanceToTarget() const { return m_Distance; }

			bool m_TargetSeen, m_TargetInPursueRange, m_TargetInCombatRange, m_TargetInMagicRange, m_ForceSeek;
			bool m_CanSeek, m_UseMagic;
			float m_Distance;

		private:
			CSceneNode* m_Target;
			float m_SeeDistance, m_CombatDistance, m_SeekSpeed, m_MaxMagicDistance;
			CModelAnimator* m_Animator;
			CSeekerType m_SeekerType;
			bool m_InvertDirection;
		};
	}
}

Begin_Enum_String(logic::components::CSeekerType)
{
	Enum_String_Id(logic::components::CSeekerType::OrcSeeker, "orc_seeker");
	Enum_String_Id(logic::components::CSeekerType::OrcBossSeeker, "orcboss_seeker");
	Enum_String_Id(logic::components::CSeekerType::GoblinSeeker, "goblin_seeker");
	Enum_String_Id(logic::components::CSeekerType::GulakSeeker, "gulak_seeker");
}
End_Enum_String;


#endif
