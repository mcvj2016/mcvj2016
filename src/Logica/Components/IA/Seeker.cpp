#include "Seeker.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Enemies/Orc/OrcAnimator.h"
#include "PhysXImpl/PhysXManager.h"
#include "Components/Enemies/Goblin/GoblinAnimator.h"
#include <sstream>
#include "Utils/Coroutine.h"

namespace logic
{
	namespace components
	{
		CSeeker::CSeeker()
			:
			CScriptComponent(),
			m_Target(nullptr),
			m_TargetSeen(false),
			m_TargetInPursueRange(false),
			m_TargetInCombatRange(false),
			m_TargetInMagicRange(false),
			m_SeeDistance(.0f),
			m_CombatDistance(.0f),
			m_SeekSpeed(.0f),
			m_MaxMagicDistance(0),
			m_Animator(nullptr),
			m_SeekerType(OrcSeeker),
			m_InvertDirection(false),
			m_ForceSeek(false),
			m_CanSeek(true),
			m_UseMagic(false),
			m_Distance(0)
		{
		}

		CSeeker::CSeeker(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Target(nullptr),
			m_TargetSeen(false),
			m_TargetInPursueRange(false),
			m_TargetInCombatRange(false),
			m_TargetInMagicRange(false),
			m_ForceSeek(false),
			m_SeeDistance(.0f),
			m_CombatDistance(.0f),
			m_SeekSpeed(.0f),
			m_MaxMagicDistance(0),
			m_Animator(nullptr),
			m_SeekerType(OrcSeeker),
			m_InvertDirection(false),
			m_CanSeek(true),
			m_UseMagic(false),
			m_Distance(0)
		{
		}

		CSeeker::~CSeeker()
		{
		}

		void CSeeker::Start()
		{
			//TODO: instanciar correctamente los animatos seguns eeker
			switch (m_SeekerType)
			{
			case CSeekerType::GoblinSeeker : 
				m_Animator = m_Parent->GetComponent<goblin::CGoblinAnimator>();
				break;
			case CSeekerType::OrcBossSeeker :
				m_Animator = m_Parent->GetComponent<orc::COrcAnimator>();				
				break;	
			case CSeekerType::GulakSeeker:
				m_Animator = m_Parent->GetComponent<orc::COrcAnimator>();
				break;
			default:
				m_Animator = m_Parent->GetComponent<orc::COrcAnimator>();
				break;
			}
		}

		void CSeeker::Update(float dt)
		{
			if (m_CanSeek)
			{
				CalculateState();
				Seek(dt);
			}
		}

		void CSeeker::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Target = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(membersValues[0]);
			assert(m_Target);
			if (!EnumString<CSeekerType>::ToEnum(m_SeekerType, membersValues[1]))
			{
				assert(!"Cannot assign seekertype");
			}
			
			m_SeekSpeed = strtof(membersValues[2].c_str(), nullptr);
			m_CombatDistance = strtof(membersValues[3].c_str(), nullptr);
			m_MaxMagicDistance = strtof(membersValues[4].c_str(), nullptr);
			m_SeeDistance = strtof(membersValues[5].c_str(), nullptr);
			std::istringstream(membersValues[6]) >> std::boolalpha >> m_UseMagic;
		}

		void CSeeker::CalculateState()
		{
			m_Distance = m_Target->GetPosition().Distance(m_Parent->GetPosition());
			if (m_Distance < m_SeeDistance)
			{
				m_TargetSeen = true;
				m_TargetInPursueRange = true;
				if (m_Distance < m_MaxMagicDistance)
				{
					m_TargetInMagicRange = true;
				}
				else
				{
					m_TargetInMagicRange = false;
				}
				if (m_Distance < m_CombatDistance)
				{
					m_TargetInCombatRange = true;
				}
				else
				{
					m_TargetInCombatRange = false;
				}
			}
			else
			{
				m_TargetSeen = false;
				m_TargetInPursueRange = false;
			}
		}

		void CSeeker::Seek(float dt) const
		{
			if (m_TargetInPursueRange || m_ForceSeek)
			{
				//calculate facing dir and set to face the target
				Vect3f facingDir = m_Target->GetPosition() - m_Parent->GetPosition();
				facingDir.y = 0;
				if (m_InvertDirection)
				{
					facingDir *= -1;
				}
				
				m_Parent->SetForward(facingDir);
				
				//tell animator to cycle walk movement if not in cc range
				if (!m_TargetInCombatRange)
				{
					if (m_Animator->GetActualIndex() != sm::WalkIndex)
					{
						m_Animator->SendEvent<sm::Move>(sm::Move());
					}
					m_Engine.GetPhysXManager().MoveCharacter(m_Parent->GetName(), m_Parent->GetForward(), m_SeekSpeed, dt);
				}
				else
				{
					if (m_Animator->GetActualIndex() != sm::IdleIndex)
					{
						m_Animator->SendEvent<sm::Stop>(sm::Stop());
					}
				}
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
			}
			else if (m_Animator->GetActualIndex() != sm::IdleIndex)
			{
				m_Animator->SendEvent<sm::Stop>(sm::Stop());
			}
		}
	}
}
