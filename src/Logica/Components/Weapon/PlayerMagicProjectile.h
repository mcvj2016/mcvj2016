#ifndef _LOGICA_PLAYERMAGIC_H
#define _LOGICA_PLAYERMAGIC_H
#include "Core/ScriptComponent.h"

namespace engine
{
	namespace particles
	{
		class CParticleSystemInstance;
	}
	namespace lights
	{
		class CPointLight;
	}
}

namespace logic
{
	namespace components
	{
		class CPlayerMagicProjectile : public CScriptComponent
		{

		public:
			bool m_Spawned;
			

			CPlayerMagicProjectile();
			CPlayerMagicProjectile(const std::string& name, const std::string& parent);
			virtual ~CPlayerMagicProjectile();

			void Start() override;
			void Spawn();
			void Update(float dt) override;
			void SetTargetPoint(const Vect3f& posf);
			void DisableProjectile();

			void OnTriggerEnter(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;
			GET_SET_PTR(CSceneNode, Player)

		private:
			float m_Damage;
			float m_MaxLifeTime;
			float m_AcumTime; 
			float m_InitialLightIntensity;
			Vect3f m_FinalPos;
			float m_Velocity;
			particles::CParticleSystemInstance* m_ParticleImpact;
			particles::CParticleSystemInstance* m_ParticleProjectile;
			void OnEnable() override;
			const std::string m_LeftHandBoneName;
			lights::CPointLight* m_ProjectileLight;
			Vect2f m_InitialLightRangeAtten;
			CSceneNode* m_Player;
		};
	}
}
#endif