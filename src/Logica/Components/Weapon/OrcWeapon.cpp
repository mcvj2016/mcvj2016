#include "OrcWeapon.h"
#include "Components/Common/Life.h"
#include "Components/Player/PlayerAnimator.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"

namespace logic
{
	namespace components
	{
		COrcWeapon::COrcWeapon() : CScriptComponent(), m_Damage(0)
		{
		}

		COrcWeapon::COrcWeapon(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_Damage(0)
		{
		}

		COrcWeapon::~COrcWeapon()
		{
		}

		void COrcWeapon::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player || other->GetTag() == CSceneNode::TAG::player2)
			{
				CModelAnimator* l_OtherAnimator = other->GetComponent<CModelAnimator>();
				//make effective taking damage only if we are not taking a previous one
				if (l_OtherAnimator->GetActualIndex() != sm::Damage1Index
					&& l_OtherAnimator->GetActualIndex() != sm::Damage2Index)
				{
					other->GetComponent<CLife>()->TakeDamage(m_Damage);
					//send the take damage event
					l_OtherAnimator->SendEvent<sm::TakeDamage>(sm::TakeDamage());
				}
			}
		}

		void COrcWeapon::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Damage = strtof(membersValues[0].c_str(), nullptr);
		}
	}
}
