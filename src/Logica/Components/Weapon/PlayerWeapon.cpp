#include "PlayerWeapon.h"
#include "Components/Common/Life.h"
#include "Components/Common/Animator.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"
#include "Graphics/Particles/ParticleSystemInstance.h"
#include "Utils/StringUtils.h"

namespace logic
{
	namespace components
	{
		CPlayerWeapon::CPlayerWeapon() : CScriptComponent(), m_Damage(0)
		{
		}

		CPlayerWeapon::CPlayerWeapon(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_Damage(0)
		{
		}

		CPlayerWeapon::~CPlayerWeapon()
		{
		}

		void CPlayerWeapon::Start()
		{
			for (std::string particleName : m_ParticleNames1)
			{
				CSceneNode* node = m_Engine.GetSceneManager().GetCurrentScene()->operator()("particles")->GetSceneNode(particleName);
				m_HitParticles1.push_back(static_cast<particles::CParticleSystemInstance*>(node));
			}
			for (std::string particleName : m_ParticleNames2)
			{
				CSceneNode* node = m_Engine.GetSceneManager().GetCurrentScene()->operator()("particles")->GetSceneNode(particleName);
				m_HitParticles2.push_back(static_cast<particles::CParticleSystemInstance*>(node));
			}
			for (std::string particleName : m_ParticleNames3)
			{
				CSceneNode* node = m_Engine.GetSceneManager().GetCurrentScene()->operator()("particles")->GetSceneNode(particleName);
				m_HitParticles3.push_back(static_cast<particles::CParticleSystemInstance*>(node));
			}
		}

		bool playerWeaponStarted = false;
		void CPlayerWeapon::Update(float dt)
		{
			if (!playerWeaponStarted)
			{
				playerWeaponStarted = true;
				
			}
		}

		void CPlayerWeapon::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::enemy)
			{
				CModelAnimator* l_OtherAnimator = other->GetComponent<CModelAnimator>();
				//make effective taking damage only if we are not taking a previous one
				if (l_OtherAnimator->GetActualIndex() != sm::Damage1Index
					&& l_OtherAnimator->GetActualIndex() != sm::Damage2Index)
				{
					other->GetComponent<CLife>()->TakeDamage(m_Damage);
					//send the take damage event
					l_OtherAnimator->SendEvent<sm::TakeDamage>(sm::TakeDamage());
					particles::CParticleSystemInstance* hit = GetFirstRandomHitParticleAvailable();
					
					if (hit != nullptr)
					{
						hit->SetEnabled(true);
						hit->SetVisible(true);
						Vect3f hitPos = m_Parent->GetTransformMatrix().GetPos();
						hitPos.y += 0.2f;
						hit->SetPosition(hitPos);
						base::utils::CCoroutine::GetInstance().StartCoroutineParallel(2, [hit](float dt, float totalTime)
						{
							if (hit->GetParticleData()->CurrentFrame >= 15)
							{
								//disable the particle projectile
								hit->SetEnabled(false);
								hit->SetVisible(false);
								hit->ResetParticles();
							}
						});
					}
				}
			}
			if (other->GetTag() != CSceneNode::TAG::player 
				&& other->GetTag() != CSceneNode::TAG::player2
				&& other->GetTag() != CSceneNode::TAG::weapon)
			{
				//play always the impact sound
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDHIT_PLAYER, other);
			}
			
		}

		particles::CParticleSystemInstance* CPlayerWeapon::GetFirstRandomHitParticleAvailable()
		{
			int min = 0;
			int max = 100;
			int probability = mathUtils::RandomLToH(min, max);

			if (probability < 30)
			{
				for (particles::CParticleSystemInstance* particle : m_HitParticles1)
				{
					if (!particle->GetEnabled())
					{
						return particle;
					}
				}
			}
			else if (probability < 60)
			{
				for (particles::CParticleSystemInstance* particle : m_HitParticles2)
				{
					if (!particle->GetEnabled())
					{
						return particle;
					}
				}
			}
			else
			{
				for (particles::CParticleSystemInstance* particle : m_HitParticles3)
				{
					if (!particle->GetEnabled())
					{
						return particle;
					}
				}
			}

			return nullptr;
		}

		void CPlayerWeapon::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Damage = strtof(membersValues[0].c_str(), nullptr);
			m_ParticleNames1 = base::utils::Split(membersValues[1], ';');
			m_ParticleNames2 = base::utils::Split(membersValues[2], ';');
			m_ParticleNames3 = base::utils::Split(membersValues[3], ';');
		}
	}
}
