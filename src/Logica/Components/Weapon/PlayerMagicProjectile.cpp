#include "PlayerMagicProjectile.h"
#include "Components/Common/Life.h"
#include "PhysXImpl/PhysXManager.h"
#include "Graphics/Scenes/SceneMesh.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Player/PlayerController.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>
#include "Graphics/Particles/ParticleSystemInstance.h"
#include "Utils/Coroutine.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Lights/PointLight.h"
#include "Graphics/Scenes/SceneLight.h"
#include "Components/Common/Animator.h"

namespace logic
{
	namespace components
	{

		CPlayerMagicProjectile::CPlayerMagicProjectile()
			:
			CScriptComponent(),
			m_Damage(),
			m_MaxLifeTime(5.0f),
			m_AcumTime(0), 
			m_InitialLightIntensity(0), 
			m_InitialLightRangeAtten(v2fZERO),
			m_FinalPos(v3fZERO),
			m_Velocity(20.0f), m_ParticleImpact(nullptr), m_ParticleProjectile(nullptr),
			m_Spawned(false),
			m_LeftHandBoneName("CATRigLArmPalm"), m_ProjectileLight(nullptr)
		{
		}

		CPlayerMagicProjectile::CPlayerMagicProjectile(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Damage(0),
			m_MaxLifeTime(5.0f),
			m_AcumTime(0), 
			m_InitialLightIntensity(0),
			m_InitialLightRangeAtten(v2fZERO),
			m_FinalPos(v3fZERO),
			m_Velocity(20.0f), m_ParticleImpact(nullptr), m_ParticleProjectile(nullptr),
			m_Spawned(false),
			m_LeftHandBoneName("CATRigLArmPalm"), m_ProjectileLight(nullptr)
		{
		}

		CPlayerMagicProjectile::~CPlayerMagicProjectile()
		{
		}
		void CPlayerMagicProjectile::Start()
		{
			m_Parent->SetTriggerActive(false);
			m_InitialLightRangeAtten = m_ProjectileLight->GetRangeAttenuation();
			m_InitialLightIntensity = m_ProjectileLight->GetIntensity();
			m_Player = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player);
		}

		void CPlayerMagicProjectile::OnEnable()
		{
			//reset next particle time emission for emiting another one
			m_ParticleImpact->m_NextParticleEmission = 0;
			m_ParticleProjectile->m_NextParticleEmission = 0;
			m_ParticleProjectile->SetEnabled(true);
		}

		void CPlayerMagicProjectile::Spawn()
		{
			CPlayerController* playerCtrl = m_Player->GetComponent<CPlayerController>();
			Mat44f projectileTransform = playerCtrl->GetBoneMatrix(m_LeftHandBoneName);
			m_Parent->m_Position = projectileTransform.GetPos();
			m_Parent->SetForward(Vect3f(m_FinalPos - m_Parent->m_Position).GetNormalized());
			m_Spawned = true;
			m_AcumTime = .0f;
			//play magicprojectile spawn sound
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HAND_M1_PLAYER, m_Parent);
			//enable the light
			m_ProjectileLight->GetSceneLight()->SetPosition(m_Parent->m_Position);
			m_ProjectileLight->SetEnabled(true);
			//enable the projectile particle
			m_ParticleProjectile->SetEnabled(true);
			m_ParticleProjectile->SetVisible(true);
		}

		void CPlayerMagicProjectile::Update(float dt)
		{
			//move the node forward
			m_Parent->m_Position += dt * m_Parent->GetForward() * m_Velocity;
			//move the light and projectile particle with it
			m_ProjectileLight->GetSceneLight()->SetPosition(m_Parent->m_Position);
			m_ParticleProjectile->SetPosition(m_Parent->m_Position);

			//move it in physx
			m_Engine.GetPhysXManager().setKinematicTarget(m_Parent->GetName(), m_Parent->GetMatrix());

			//if the projectile is traveling so far, destroy it
			m_AcumTime += dt;
			if (m_AcumTime >= m_MaxLifeTime)
			{
				DisableProjectile();
			}
		}

		void CPlayerMagicProjectile::SetTargetPoint(const Vect3f& posf)
		{
			m_FinalPos = posf;
		}

		void CPlayerMagicProjectile::DisableProjectile()
		{
			//disable update and render
			m_Parent->SetEnabled(false);
			m_Parent->SetVisible(false);
			m_Parent->SetTriggerActive(false);
			m_Spawned = false;
			//disable the particle projectile
			m_ParticleProjectile->SetEnabled(false);
			m_ParticleProjectile->SetVisible(false);
			m_ParticleProjectile->ResetParticles();

			//parallel rutine for controlling light intensity and disabling particle impact
			base::utils::CCoroutine::GetInstance().StartCoroutineParallel(
				1.5f, //corutine life time
				[this](float dt, float totalTime)
				{
					//lerp light intensity and att range growing the first half of rutine, decreasing the last one
					float lightIntensity = m_ProjectileLight->GetIntensity() + dt * 5.0f;
					Vect2f lightAttRange = m_ProjectileLight->GetRangeAttenuation();
					lightAttRange.x += dt * 2.0f;
					lightAttRange.y += dt * 2.0f;
					
					if (totalTime >= 0.8f)
					{
						lightAttRange.x -= dt * 2.0f;
						lightAttRange.y -= dt * 2.0f;
						lightIntensity = m_ProjectileLight->GetIntensity() - dt * 10.0f;
					}
					
					m_ProjectileLight->SetIntensity(lightIntensity);
					m_ProjectileLight->SetRangeAttenuation(lightAttRange);
					

					//on rutine end set default values and disable light/particle
					if (totalTime >= 1.5f)
					{
						m_ProjectileLight->SetIntensity(m_InitialLightIntensity);
						m_ProjectileLight->SetRangeAttenuation(m_InitialLightRangeAtten);
						m_ProjectileLight->SetEnabled(false);
						m_ParticleImpact->SetEnabled(false);
						m_ParticleImpact->ResetParticles();
					}
				}
			);
		}

		void CPlayerMagicProjectile::OnTriggerEnter(engine::scenes::CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::enemy)
			{
				CModelAnimator* l_OtherAnimator = other->GetComponent<CModelAnimator>();
				//make effective taking damage only if we are not taking a previous one
				if (l_OtherAnimator->GetActualIndex() != sm::Damage1Index
					&& l_OtherAnimator->GetActualIndex() != sm::Damage2Index)
				{
					other->GetComponent<CLife>()->TakeDamage(m_Damage);
					//send the take damage event
					l_OtherAnimator->SendEvent<sm::TakeDamage>(sm::TakeDamage());
				}
			}

			if (other->GetTag() != CSceneNode::TAG::player 
				&& other->GetTag() != CSceneNode::TAG::interactable
				&& other->GetTag() != CSceneNode::TAG::weapon)
			{
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_MAGICPROJECTILE_PLAYER, m_Parent);
				m_ParticleImpact->SetEnabled(true);
				m_ParticleImpact->SetPosition(m_Parent->m_Position + m_Parent->GetBackward() * 2);
				m_ProjectileLight->GetSceneLight()->SetPosition(m_Parent->m_Position + m_Parent->GetBackward() * 2);
				DisableProjectile();
			}
		}

		void CPlayerMagicProjectile::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Damage = strtof(membersValues[0].c_str(), nullptr);
			m_ParticleImpact = static_cast<particles::CParticleSystemInstance*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(membersValues[1]));
			m_ParticleProjectile = static_cast<particles::CParticleSystemInstance*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(membersValues[2]));
			m_ProjectileLight = static_cast<lights::CPointLight*>(m_Engine.GetLightManager().GetLight(membersValues[3]));	
		}
	}
}
