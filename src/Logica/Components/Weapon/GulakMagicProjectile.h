#ifndef _LOGICA_GULAKMAGIC_H
#define _LOGICA_GULAKMAGIC_H
#include "Core/ScriptComponent.h"

namespace engine
{
	namespace particles
	{
		class CParticleSystemInstance;
	}
	namespace lights
	{
		class CPointLight;
	}
}

namespace logic
{
	namespace components
	{
		class CGulakMagicProjectile : public CScriptComponent
		{

		public:
			bool m_Spawned;
			bool m_Launched;
			


			CGulakMagicProjectile();
			CGulakMagicProjectile(const std::string& name, const std::string& parent);
			virtual ~CGulakMagicProjectile();

			void Start() override;
			void Spawn();
			void Launch();
			void Update(float dt) override;
			void DisableProjectile();

			void OnTriggerEnter(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			float m_Damage;
			float m_UpOffset;
			float m_MaxLifeTime;
			float m_AcumTime;
			Vect3f m_FinalPos;
			CSceneNode* m_Player;
			CSceneNode* m_Gulak;
			float m_Velocity;
			particles::CParticleSystemInstance* m_ParticleImpact;
			particles::CParticleSystemInstance* m_ParticleProjectile;
			void OnEnable() override;
			lights::CPointLight* m_ProjectileLight;
			Vect2f m_InitialLightRangeAtten;
			float m_InitialLightIntensity;
		};
	}
}
#endif