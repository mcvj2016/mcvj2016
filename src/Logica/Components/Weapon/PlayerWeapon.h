#ifndef _LOGICA_PLAYERWEAPON_H
#define _LOGICA_PLAYERWEAPON_H
#include "Core/ScriptComponent.h"
#include "Graphics/Particles/ParticleSystemInstance.h"

namespace logic
{
	namespace components
	{
		class CPlayerWeapon : public CScriptComponent
		{
		
		public:
			CPlayerWeapon();
			CPlayerWeapon(const std::string& name, const std::string& parent);
			virtual ~CPlayerWeapon();
			void Start() override;
			void Update(float dt) override;
			void OnTriggerEnter(CSceneNode* other) override;
			particles::CParticleSystemInstance* GetFirstRandomHitParticleAvailable();

			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			float m_Damage;
			std::vector<std::string> m_ParticleNames1;
			std::vector<std::string> m_ParticleNames2;
			std::vector<std::string> m_ParticleNames3;
			std::vector<particles::CParticleSystemInstance*> m_HitParticles1;
			std::vector<particles::CParticleSystemInstance*> m_HitParticles2;
			std::vector<particles::CParticleSystemInstance*> m_HitParticles3;
		};
	}
}
#endif