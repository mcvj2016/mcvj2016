#ifndef _LOGICA_ORKWEAPON_H
#define _LOGICA_ORKWEAPON_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class COrcWeapon : public CScriptComponent
		{
		private:
			float m_Damage;

		public:
			COrcWeapon();
			COrcWeapon(const std::string& name, const std::string& parent);
			virtual ~COrcWeapon();

			void OnTriggerEnter(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;
		};
	}
}
#endif