#ifndef _LOGICA_THIRDPERSONCAMERA_H
#define _LOGICA_THIRDPERSONCAMERA_H
#include "Core/ScriptComponent.h"

class CThirdPersonCameraController;

namespace logic
{
	namespace components
	{
		class CThirdPersonCameraComponent : public CScriptComponent
		{
	

		public:
			CThirdPersonCameraComponent();
			CThirdPersonCameraComponent(const std::string& name, const std::string& parent);
			virtual ~CThirdPersonCameraComponent();

			GET_SET_PTR(CThirdPersonCameraController, Camera)
			GET_SET_PTR(CSceneNode, Player)
			void Start() override;
			;

			void MoveCamera(float dt);
			void Update(float dt) override;
			void OnDestroy() override;

		private:
			bool CheckObstacle(float dt);
			bool CheckObstacleInFront();
			bool CheckObstacleInBack();
			void AvoidObstacle(float dt, bool avoid = true);
			CThirdPersonCameraController* m_Camera;
			CSceneNode* m_Player;
			const std::string m_AimCamBone;
		};
	}
}
#endif