#include "FirstPersonCamera.h"
#include "Engine.h"
#include "Input/ActionManager.h"
#include "Camera/FirstPersonCameraController.h"
#include "Utils/Coroutine.h"
#include "Helper/ImguiHelper.h"

using namespace engine;

logic::components::CFirstPersonCameraComponent::CFirstPersonCameraComponent() : m_Camera(nullptr), m_Player(nullptr)
{
}

logic::components::CFirstPersonCameraComponent::CFirstPersonCameraComponent(const std::string& name, const std::string& parent)
	: CScriptComponent(name, parent), m_Camera(nullptr), m_Player(nullptr)
{
}

logic::components::CFirstPersonCameraComponent::~CFirstPersonCameraComponent()
{
}

void logic::components::CFirstPersonCameraComponent::Start()
{
	CSCeneCamera* cam = static_cast<CSCeneCamera*>(m_Parent);
	m_Camera = static_cast<CFirstPersonCameraController*>(cam->GetCamera());
	m_Camera->SetSceneCamera(cam);
}

void logic::components::CFirstPersonCameraComponent::MoveCamera(float dt)
{
	CEngine& lEngine = CEngine::GetInstance();

	if (!m_Camera->GetLock())
	{
		m_Camera->m_Speed = lEngine.GetActionManager().GetInputActionValue("MoveForward");
		m_Camera->m_RightSpeed = lEngine.GetActionManager().GetInputActionValue("MoveRightLeft");

		if (lEngine.GetActionManager().IsInputActionActive("CamRotateX"))
		{
			m_Camera->m_YawSpeed = lEngine.GetActionManager().GetInputActionValue("CamRotateX");
		}
		else
		{
			m_Camera->m_YawSpeed = .0f;
		}

		if (lEngine.GetActionManager().IsInputActionActive("CamRotateY"))
		{
			m_Camera->m_PitchSpeed = -lEngine.GetActionManager().GetInputActionValue("CamRotateY");
		}
		else{
			m_Camera->m_PitchSpeed = 0;
		}
		m_Camera->GetSceneCamera()->m_Yaw += (m_Camera->m_YawSpeed * dt);
		if (m_Camera->GetSceneCamera()->m_Yaw >= (3.1416f*2.0f))
		{
			m_Camera->GetSceneCamera()->m_Yaw = m_Camera->GetSceneCamera()->m_Yaw - (3.1416f*2.0f);
		}
		else if (m_Camera->GetSceneCamera()->m_Yaw < (3.1416f*2.0f))
		{
			m_Camera->GetSceneCamera()->m_Yaw = m_Camera->GetSceneCamera()->m_Yaw + (3.1416f*2.0f);
		}

		m_Camera->GetSceneCamera()->m_Pitch += (m_Camera->m_PitchSpeed * dt);
		if (m_Camera->GetSceneCamera()->m_Pitch >= ((3.1416f / 2.0f) - 0.5f))
		{
			m_Camera->GetSceneCamera()->m_Pitch = ((3.1416f / 2.0f) - 0.5f);
		}
		else if (m_Camera->GetSceneCamera()->m_Pitch < ((-3.1416f / 2.0f) + 0.5f))
		{
			m_Camera->GetSceneCamera()->m_Pitch = ((-3.1416f / 2.0f) + 0.5f);
		}

		//Se obtiene el valor del forward, se multiplica por la velocidad del movimiento y se le suma el resultado a la posicion
		m_Camera->GetSceneCamera()->m_Position = m_Camera->GetSceneCamera()->m_Position + (m_Camera->m_Speed * dt * 2*m_Camera->GetSceneCamera()->GetForward().GetNormalized());

		//Se obtiene el valor del right, se multiplica por la velocidad del movimiento y se le suma el resultado a la posicion
		m_Camera->GetSceneCamera()->m_Position = m_Camera->GetSceneCamera()->m_Position + (m_Camera->m_RightSpeed * dt * 2* m_Camera->GetSceneCamera()->GetRight().GetNormalized());

	}

	m_Camera->SetLookAt(m_Camera->GetSceneCamera()->GetForward() + m_Camera->GetSceneCamera()->m_Position);
	//Establece  la matrix vista
	m_Camera->m_YawSpeed = 0.0f;
	m_Camera->m_PitchSpeed = 0.0f;
	//lock camera for debug
#if defined(DEBUG) || defined(RELEASE)
	if (lEngine.GetInputManager().KeyBecomesPressed(VK_ESCAPE))
	{
		m_Camera->SetLock(!m_Camera->GetLock());
	}
#endif
}

void logic::components::CFirstPersonCameraComponent::Update(float dt)
{
	MoveCamera(dt);
	//Establece  la matrix vista
	m_Camera->SetMatrixs();
}

void logic::components::CFirstPersonCameraComponent::OnDestroy()
{
}
