#ifndef _LOGICA_FIRSTPERSONCAMERA_H
#define _LOGICA_FIRSTPERSONCAMERA_H
#include "Core/ScriptComponent.h"

class CFirstPersonCameraController;

namespace logic
{
	namespace components
	{
		class CFirstPersonCameraComponent : public CScriptComponent
		{

		public:
			CFirstPersonCameraComponent();
			CFirstPersonCameraComponent(const std::string& name, const std::string& parent);
			virtual ~CFirstPersonCameraComponent();

			void Start() override;
			void MoveCamera(float dt);
			void Update(float dt) override;
			void OnDestroy() override;

		private:
			CFirstPersonCameraController* m_Camera;
			CSceneNode* m_Player;
		};
	}
}
#endif