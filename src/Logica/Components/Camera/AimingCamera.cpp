#include "AimingCamera.h"
#include "Engine.h"
#include "Input/ActionManager.h"
#include "Camera/FirstPersonCameraController.h"
#include "Utils/Coroutine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Cal3D/src/cal3d/bone.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Helper/ImguiHelper.h"

using namespace engine;

namespace logic
{
	namespace components
	{

		CAimingCamera::CAimingCamera() 
		: 
		m_Camera(nullptr), 
		m_Player(nullptr)
		{
		}

		CAimingCamera::CAimingCamera(const std::string& name, const std::string& parent)
			: 
		CScriptComponent(name, parent), 
		m_Camera(nullptr), 
		m_Player(nullptr)
		{
		}

		CAimingCamera::~CAimingCamera()
		{
		}

		void CAimingCamera::Start()
		{
			CSCeneCamera* cam = static_cast<CSCeneCamera*>(m_Parent);
			m_Camera = static_cast<CFirstPersonCameraController*>(cam->GetCamera());
			m_Camera->SetSceneCamera(cam);
			m_Player = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player);
			//m_Camera->GetSceneCamera()->SetForward(m_Player->GetForward());
		}

		void CAimingCamera::MoveCamera(float dt) const
		{
			
			//accumulate rotations
			if (m_Engine.GetActionManager().IsInputActionActive("CamRotateX"))
			{
				m_Camera->m_YawSpeed = m_Engine.GetActionManager().GetInputActionValue("CamRotateX");
			}
			else
			{
				m_Camera->m_YawSpeed = .0f;
			}

			if (m_Engine.GetActionManager().IsInputActionActive("CamRotateY"))
			{
				m_Camera->m_PitchSpeed = -m_Engine.GetActionManager().GetInputActionValue("CamRotateY");
			}
			else{
				m_Camera->m_PitchSpeed = 0;
			}

			m_Camera->GetSceneCamera()->m_Pitch += (m_Camera->m_PitchSpeed * dt);
			if (m_Camera->GetSceneCamera()->m_Pitch >= ((3.1416f / 2.0f) - 0.5f))
			{
				m_Camera->GetSceneCamera()->m_Pitch = ((3.1416f / 2.0f) - 0.5f);
			}
			else if (m_Camera->GetSceneCamera()->m_Pitch < ((-3.1416f / 2.0f) + 0.5f))
			{
				m_Camera->GetSceneCamera()->m_Pitch = ((-3.1416f / 2.0f) + 0.5f);
			}

			//update player rotation in yaw
			m_Player->m_Yaw -= (m_Camera->m_YawSpeed * dt);
			if (m_Player->m_Yaw >= (3.1416f*2.0f))
			{
				m_Player->m_Yaw = m_Player->m_Yaw - (3.1416f*2.0f);
			}
			else if (m_Player->m_Yaw < (3.1416f*2.0f))
			{
				m_Player->m_Yaw = m_Player->m_Yaw + (3.1416f*2.0f);
			}
			m_Camera->GetSceneCamera()->m_Yaw = m_Player->m_Yaw * -1;

			Vect3f v = m_Player->GetPosition();
			v.y = v.y + 2;
			m_Camera->GetSceneCamera()->SetPosition(v);
			Vect3f p = m_Camera->GetSceneCamera()->GetPosition();
			p += m_Camera->GetSceneCamera()->GetLeft() * 1.2f;
			p += m_Camera->GetSceneCamera()->GetBackward() * 1.7f;
			m_Camera->GetSceneCamera()->SetPosition(p);

			m_Camera->SetLookAt(m_Camera->GetSceneCamera()->GetForward() + m_Camera->GetSceneCamera()->m_Position);
		}

		void CAimingCamera::Update(float dt)
		{
			if (!m_Camera->GetLock())
			{
				MoveCamera(dt);
			}
			//Establece  la matrix vista
			m_Camera->SetMatrixs();
		}
	}
}
