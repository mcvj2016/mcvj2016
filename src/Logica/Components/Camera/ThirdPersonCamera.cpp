#include "ThirdPersonCamera.h"
#include "Camera/ThirdPersonCameraController.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Input/ActionManager.h"
#include "Components/Player/PlayerController.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		CThirdPersonCameraComponent::CThirdPersonCameraComponent() 
		: 
			m_Camera(nullptr), 
			m_Player(nullptr), 
			m_AimCamBone("CATRigcabeza")
		{
		}

		CThirdPersonCameraComponent::CThirdPersonCameraComponent(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), 
			m_Camera(nullptr), 
			m_Player(nullptr),
			m_AimCamBone("CATRigcabeza")
		{
		}

		CThirdPersonCameraComponent::~CThirdPersonCameraComponent()
		{

		}

		void CThirdPersonCameraComponent::Start()
		{
			m_Player = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("Orkelf");
			CSCeneCamera* cam = static_cast<CSCeneCamera*>(m_Parent);
			cam->SetYaw(3.1416f);
			cam->SetPitch(.0f);
			cam->SetRoll(.0f);
			m_Camera = static_cast<CThirdPersonCameraController*>(cam->GetCamera());
			m_Camera->SetSceneCamera(cam);
		}

		bool CThirdPersonCameraComponent::CheckObstacleInFront()
		{
			//launch the raycast
			RaycastData ray;
			//launch raycast with gfroups to filter
			unsigned int groupsToHit = CPhysXManager::PhysxGroups::Wall;
			Vect3f initialPos = m_Camera->GetSceneCamera()->GetPosition() + m_Camera->GetSceneCamera()->GetBackward().GetNormalized();

			bool obstacle = false;
			if (m_Engine.GetPhysXManager().Raycast(initialPos, m_Player->GetPosition(), groupsToHit, &ray))
			{
				obstacle = true;
			}

			return obstacle;
		}

		bool CThirdPersonCameraComponent::CheckObstacleInBack()
		{
			//launch the raycast
			RaycastData ray;
			//launch raycast with gfroups to filter
			unsigned int groupsToHit = CPhysXManager::PhysxGroups::Wall;

			Vect3f backward = m_Camera->GetSceneCamera()->GetBackward().GetNormalized();
			backward.y = 0;
			bool obstacle = false;
			if (m_Engine.GetPhysXManager().Raycast(m_Camera->GetSceneCamera()->GetPosition(), m_Camera->GetSceneCamera()->GetPosition() + backward * 3, groupsToHit, &ray))
			{
				obstacle = true;
			}

			return obstacle;
		}
		
		bool CThirdPersonCameraComponent::CheckObstacle(float dt)
		{
			bool obstacle = false;
			if (CheckObstacleInFront())
			{
				AvoidObstacle(dt);
				obstacle = true;
			}
			if (CheckObstacleInBack())
			{
				obstacle = true;
			}
			return obstacle;
		}

		void CThirdPersonCameraComponent::AvoidObstacle(float dt, bool avoid)
		{
			avoid ? m_Camera->m_Zoom -= dt * m_Camera->m_ZoomSpeed : m_Camera->m_Zoom += dt * m_Camera->m_ZoomSpeed;
			if (m_Camera->m_Zoom > m_Camera->maxZoom)
			{
				m_Camera->m_Zoom = m_Camera->maxZoom;
			}
			if (m_Camera->m_Zoom < m_Camera->minZoom)
			{
				m_Camera->m_Zoom = m_Camera->minZoom;
			}
		}

		void CThirdPersonCameraComponent::MoveCamera(float dt)
		{
			m_Camera->m_Center = m_Player->m_Position;
			m_Camera->m_Center.y += 2;
			if (m_Engine.GetActionManager().IsInputActionActive("CamRotateX"))
			{
				m_Camera->m_YawSpeed = m_Engine.GetActionManager().GetInputActionValue("CamRotateX");
			}
			else
			{
				m_Camera->m_YawSpeed = 0;
			}
			if (m_Engine.GetActionManager().IsInputActionActive("CamRotateY"))
			{
				m_Camera->m_PitchSpeed = m_Engine.GetActionManager().GetInputActionValue("CamRotateY");
			}
			else
			{
				m_Camera->m_PitchSpeed = 0;
			}

			
			m_Camera->GetSceneCamera()->m_Yaw += (m_Camera->m_YawSpeed * dt);
			if (m_Camera->GetSceneCamera()->m_Yaw >= (3.1416f*2.0f))
			{
				m_Camera->GetSceneCamera()->m_Yaw = m_Camera->GetSceneCamera()->m_Yaw - (3.1416f*2.0f);
			}
			else if (m_Camera->GetSceneCamera()->m_Yaw < (3.1416f*2.0f))
			{
				m_Camera->GetSceneCamera()->m_Yaw = m_Camera->GetSceneCamera()->m_Yaw + (3.1416f*2.0f);
			}

			//limit pitch rotation for not overrotating when +-90�
			m_Camera->GetSceneCamera()->m_Pitch += (m_Camera->m_PitchSpeed * dt);
			if (m_Camera->GetSceneCamera()->m_Pitch >= ((3.1416f / 2.0f) - 0.5f))
			{
				m_Camera->GetSceneCamera()->m_Pitch = ((3.1416f / 2.0f) - 0.5f);
			}
			else if (m_Camera->GetSceneCamera()->m_Pitch < ((-3.1416f / 2.0f) + 0.5f))
			{
				m_Camera->GetSceneCamera()->m_Pitch = ((-3.1416f / 2.0f) + 0.5f);
			}
			//another limitation for not trespassing the ground
			if (m_Camera->GetSceneCamera()->m_Pitch > 0)
			{
				m_Camera->GetSceneCamera()->m_Pitch = 0;
			}

			m_Camera->GetSceneCamera()->m_Position = m_Camera->m_Center - m_Camera->GetSceneCamera()->GetForward().GetNormalized() * m_Camera->m_Zoom;

			//Hasta aqu� la camara gira alrededor de un punto.Lo estoy seteadno en el update de c++ : (
			//Centro de la camara el personaje.Si muevo personaje, mover camara.

			m_Camera->SetLookAt(m_Camera->m_Center);

			m_Camera->m_YawSpeed = 0.0;
			m_Camera->m_PitchSpeed = 0.0;
		}

		void CThirdPersonCameraComponent::Update(float dt)
		{
			if (!m_Camera->GetLock())
			{
				//check obstacles between player and cam
				if (!CheckObstacle(dt))
				{
					AvoidObstacle(dt, false);
				}
				MoveCamera(dt);
				
			}
			//Establece  la matrix vista
			m_Camera->SetMatrixs();
		}

		void CThirdPersonCameraComponent::OnDestroy()
		{
		}
	}
}
