#ifndef _LOGICA_AIMINGCAMERA_H
#define _LOGICA_AIMINGCAMERA_H
#include "Core/ScriptComponent.h"

class CFirstPersonCameraController;

namespace logic
{
	namespace components
	{
		class CAimingCamera : public CScriptComponent
		{

		public:
			CAimingCamera();
			CAimingCamera(const std::string& name, const std::string& parent);
			virtual ~CAimingCamera();

			void Start() override;
			void MoveCamera(float dt) const;
			void Update(float dt) override;
			GET_SET_PTR(CSceneNode, Player)

		private:
			CFirstPersonCameraController* m_Camera;
			CSceneNode* m_Player;
		};
	}
}
#endif