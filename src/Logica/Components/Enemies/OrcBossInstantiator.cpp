#include "OrcBossInstantiator.h"

namespace logic
{
	namespace components
	{
		COrcBossInstantiator::COrcBossInstantiator()
			: CScriptComponent(), m_EnemyActivated(false)
		{
		}

		COrcBossInstantiator::COrcBossInstantiator(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_EnemyActivated(false)
		{
		}

		COrcBossInstantiator::~COrcBossInstantiator()
		{
		}

		void COrcBossInstantiator::Start()
		{
		}

		void COrcBossInstantiator::OnEnable()
		{
		}

		void COrcBossInstantiator::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Boss = membersValues[0];
		}
	}
}