#include "EnemyNotifier.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CEnemyNotifier::CEnemyNotifier()
			: CScriptComponent(), 
			m_EnemiesManager(nullptr), 
			m_Seeker(nullptr), 
			m_EnemyActivated(false),
			m_ManagersNode("")
		{
		}

		CEnemyNotifier::CEnemyNotifier(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), 
			m_EnemiesManager(nullptr), 
			m_Seeker(nullptr), 
			m_EnemyActivated(false),
			m_ManagersNode("")
		{
		}

		CEnemyNotifier::~CEnemyNotifier()
		{
		}

		void CEnemyNotifier::Start()
		{
			m_EnemiesManager = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_ManagersNode)->GetComponent<CEnemiesManager>();
			m_Seeker = m_Parent->GetComponent<CSeeker>();
		}

		void CEnemyNotifier::Update(float dt)
		{
			if (!m_EnemyActivated && (m_Seeker->m_TargetSeen || m_Seeker->m_ForceSeek))
			{
				m_EnemyActivated = true;
				AddEnemyActive();
			}
		}

		void CEnemyNotifier::OnDestroy()
		{
			RemoveActiveEnemy();
		}

		void CEnemyNotifier::AddEnemyActive() const
		{
			m_EnemiesManager->AddEnemyActive();
		}

		void CEnemyNotifier::RemoveActiveEnemy() const
		{
			m_EnemiesManager->RemoveEnemyActive();
		}

		void CEnemyNotifier::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_ManagersNode = membersValues[0];
		}
	}
}
