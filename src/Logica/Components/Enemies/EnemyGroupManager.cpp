#include "EnemyGroupManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Common/Life.h"
#include "Utils/StringUtils.h"

namespace logic
{
	namespace components
	{
		CEnemyGroupManager::CEnemyGroupManager()
			: CScriptComponent(), 
			m_EnemiesAlive(0)
		{
		}

		CEnemyGroupManager::CEnemyGroupManager(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), 
			m_EnemiesAlive(0)
		{
		}

		CEnemyGroupManager::~CEnemyGroupManager()
		{
		}

		void CEnemyGroupManager::Start()
		{

		}

		void CEnemyGroupManager::Update(float dt)
		{
			//check each enemy, and if dead remove from vector
			if (!m_EnemiesGroup.empty()) {
				for (int i = m_EnemiesGroup.size() - 1; i >= 0; i--) {
					if (m_EnemiesGroup.at(i)->GetComponent<CLife>()->GetLifePoints() <= 0)
					{
						m_EnemiesAlive--;
						m_EnemiesGroup.erase(m_EnemiesGroup.begin() + i);
					}
				}
			}
			else
			{
				if (m_CallbackOnNotEnemiesAlive != nullptr)
				{
					m_CallbackOnNotEnemiesAlive();
					this->Destroy(m_Parent);
				}
			}
		}

		void CEnemyGroupManager::InitMembersValues(std::vector<std::string> membersValues)
		{
			std::vector<std::string> lEnemies = base::utils::Split(membersValues[0], ';');
			for (std::string enemyName : lEnemies)
			{
				CSceneNode* enemy = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(enemyName);
				assert(enemy);
				m_EnemiesGroup.push_back(enemy);
				m_EnemiesAlive++;
			}
		}

		void CEnemyGroupManager::SetCallbackOnEnemiesDead(std::function<void()> cb)
		{
			m_CallbackOnNotEnemiesAlive = cb;
		}
	}
}
