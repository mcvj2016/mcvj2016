#ifndef _LOGICA_ENEMYNOTIFIER_H 
#define _LOGICA_ENEMYNOTIFIER_H 
#include "Core/ScriptComponent.h" 
#include "Components/Common/EnemiesManager.h"
#include "Components/IA/Seeker.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CEnemyNotifier : public CScriptComponent
		{
		public:
			CEnemyNotifier();
			CEnemyNotifier(const std::string& name, const std::string& parent);
			~CEnemyNotifier();

			void Start() override;
			void Update(float dt) override;
			void OnDestroy() override;
			void AddEnemyActive() const;
			void RemoveActiveEnemy() const;
			void InitMembersValues(std::vector<std::string> membersValues) override;
			GET_SET(bool, EnemyActivated)

		private:
			CEnemiesManager* m_EnemiesManager;
			CSeeker* m_Seeker;
			bool m_EnemyActivated;
			std::string m_ManagersNode;
		};
	}
}

#endif