#ifndef _LOGICA_ENEMYGROUPMANAGER_H 
#define _LOGICA_ENEMYGROUPNOTIFIER_H 
#include "Core/ScriptComponent.h" 
#include "Components/Common/EnemiesManager.h"
#include "Components/IA/Seeker.h"
#include <functional>

using namespace engine;

namespace logic
{
	namespace components
	{
		class CEnemyGroupManager : public CScriptComponent
		{
		public:
			CEnemyGroupManager();
			CEnemyGroupManager(const std::string& name, const std::string& parent);
			~CEnemyGroupManager();

			void Start() override;
			void Update(float dt) override;
			void InitMembersValues(std::vector<std::string> membersValues) override;
			GET_SET(int, EnemiesAlive);
			void SetCallbackOnEnemiesDead(std::function<void()> cb);
			std::vector<CSceneNode*> GetEnemiesGroup() const { return m_EnemiesGroup; }

		private:
			std::function<void()> m_CallbackOnNotEnemiesAlive;
			std::vector<CSceneNode*> m_EnemiesGroup;
			int m_EnemiesAlive;
		};
	}
}

#endif