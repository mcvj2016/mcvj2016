#include "EnemyTag.h"

namespace logic
{
	namespace components
	{
		void CEnemyTag::InitMembersValues(std::vector<std::string> membersValues)
		{
			bool tagged = EnumString<EnemyType>::ToEnum(m_Tag, membersValues[0]);
			assert(tagged);
		}
	}
}