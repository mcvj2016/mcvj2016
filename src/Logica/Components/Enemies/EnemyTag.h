#ifndef _LOGICA_ENEMYTAG_H 
#define _LOGICA_ENEMYTAG_H 
#include "Core/ScriptComponent.h" 

namespace logic
{
	namespace components
	{
		class CEnemyTag : public CScriptComponent
		{
		public:

			enum EnemyType
			{
				Orc = 0,
				OrcBoss,
				Goblin,
				Gulak
			};

			CEnemyTag() : CScriptComponent(), m_Tag(Orc){};
			CEnemyTag(const std::string& name, const std::string& parent) : CScriptComponent(name, parent), m_Tag(Orc){};
			~CEnemyTag(){};

			void InitMembersValues(std::vector<std::string> membersValues) override;

			EnemyType GetTag() const { return m_Tag; }

		private:
			EnemyType m_Tag;
		};	
	}
}
Begin_Enum_String(logic::components::CEnemyTag::EnemyType)
{
	Enum_String_Id(logic::components::CEnemyTag::EnemyType::Orc, "orc");
	Enum_String_Id(logic::components::CEnemyTag::EnemyType::OrcBoss, "orcboss");
	Enum_String_Id(logic::components::CEnemyTag::EnemyType::Goblin, "goblin");
	Enum_String_Id(logic::components::CEnemyTag::EnemyType::Gulak, "gulak");
}
End_Enum_String;


#endif