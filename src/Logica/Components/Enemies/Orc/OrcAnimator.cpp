#include "OrcAnimator.h"
#include "StateMachines/OrcStateMachine.h"

namespace logic
{
	namespace orc
	{
		COrcAnimator::COrcAnimator()
			: CModelAnimator()
		{
		}

		COrcAnimator::COrcAnimator(const std::string& name, const std::string& parent)
		:
		CModelAnimator(name, parent)
		{
		}

		COrcAnimator::~COrcAnimator()
		{
		}

		void COrcAnimator::Start()
		{
			m_SM = new COrcSM();
			m_SM->m_Animator = this;
			m_SM->SetName(m_Parent->GetName());
			m_Model = reinterpret_cast<CSceneAnimatedModel*>(m_Parent);
			
			m_SM->Add<Idle>("Idle");
			m_SM->Add<Move>("Move");
			m_SM->Add<Attack1>("Attack1");
			m_SM->Add<Attack2>("Attack2");
			m_SM->Add<StrongAttack>("StrongAttack");
			m_SM->Add<TakeDamage>("TakeDamage");
			m_SM->Add<Death>("Death");
			m_SM->init("Idle");
			m_SM->dispatch(sm::Init());
		}
	}
}
