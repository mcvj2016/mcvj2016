#ifndef ORCANIMATOR_H
#define ORCANIMATOR_H

#include "Core/ScriptComponent.h"
#include "Components/Common/Animator.h"

using namespace logic::components;

namespace logic
{
	namespace orc
	{
		class COrcAnimator : public CModelAnimator
		{
		public:
			COrcAnimator();

			COrcAnimator(const std::string& name, const std::string& parent);

			~COrcAnimator();

			void Start() override;
		};
	}
}
#endif
