#ifndef _ORCCONTROLLER_H
#define _ORCCONTROLLER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace orc
	{
		class COrcAnimator;
	}
	namespace components
	{
		using namespace orc;
		class CLife;

		class COrcController : public CScriptComponent
		{
		public:
			COrcController();
			COrcController(const std::string& name, const std::string& parent);
			virtual ~COrcController();

			void Start() override;
			void Update(float dt) override;
			void OnDestroy() override;

			bool IsDead() const { return m_Death; }

			void OnTriggerEnter(CSceneNode* other) override;

			void ThrowObject();
			void Dead();
		private:
			CLife * m_Life;
			CSceneNode* m_ObjectToThrow;
			bool m_Death = false;
			const std::string m_OrcBoss1;
			COrcAnimator* m_Animator;
		};
	}
}

#endif