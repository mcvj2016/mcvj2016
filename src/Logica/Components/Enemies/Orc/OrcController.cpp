#include "OrcController.h"
#include "Components/Common/Life.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "OrcAnimator.h"

namespace logic
{
	using namespace orc;
	namespace components
	{
		COrcController::COrcController()
			: CScriptComponent(),
			  m_Life(nullptr),
			  m_ObjectToThrow(nullptr),
			  m_OrcBoss1("OrcoJefe1"), 
			m_Animator(nullptr)
		{
		}

		COrcController::COrcController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), 
		m_Life(nullptr), 
		m_ObjectToThrow(nullptr),
		m_OrcBoss1("OrcoJefe1"), 
		m_Animator(nullptr)
		{
		}

		COrcController::~COrcController()
		{
		}

		void COrcController::Start()
		{
			m_Animator = m_Parent->GetComponent<COrcAnimator>();
			m_Life = m_Parent->GetComponent<CLife>();
			m_ObjectToThrow = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("LlaveReja");
		}

		void COrcController::Update(float dt)
		{
			if (m_Life->GetLifePoints() <= 0.0f && !m_Death)
			{
				Dead();
			}
		}

		void COrcController::OnDestroy()
		{
		}

		void COrcController::OnTriggerEnter(engine::scenes::CSceneNode* other)
		{
			
		}

		void COrcController::ThrowObject()
		{
			if ((m_Parent->GetName() == m_OrcBoss1) && (m_ObjectToThrow != nullptr))
			{
				m_Parent->m_Yaw = m_Parent->m_Yaw *-1;
				//m_ObjectToThrow->SetPosition(m_Parent->GetPosition() + Vect3f(-1.5f, 0.52f, 0.0f));
				m_ObjectToThrow->SetPosition(m_Parent->GetPosition() + m_Parent->GetBackward() * 2.3f + +Vect3f(0.0f, 0.52f, 0.0f));
				m_Parent->m_Yaw = m_Parent->m_Yaw *-1;
				Mat44f objectMatrix = m44fIDENTITY;
				objectMatrix.SetPos(m_ObjectToThrow->GetPosition());
				CEngine::GetInstance().GetPhysXManager().setKinematicTarget(m_ObjectToThrow->GetName(), objectMatrix);

				m_ObjectToThrow->SetVisible(true);
				m_ObjectToThrow = nullptr;
			}
		}

		void COrcController::Dead()
		{
			if (m_Animator->GetActualIndex() != sm::DeadIndex)
			{
				m_Animator->SendEvent<sm::Dead>(sm::Dead());
				m_Death = true;
				ThrowObject();
				//disable all components
				m_Parent->DisableComponents();
				DestroyPhysxOnly(m_Parent);
			}
		}
	}
}
