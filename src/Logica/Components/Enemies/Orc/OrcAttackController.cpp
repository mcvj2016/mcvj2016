#include "OrcAttackController.h"
#include "Utils/Coroutine.h"
#include "Components/IA/Seeker.h"
#include "OrcAnimator.h"
#include <sstream>

namespace logic
{
	namespace components
	{
		COrcAttackController::COrcAttackController():
			CScriptComponent(),
			m_Seeker(nullptr),
			m_MinAttackDelay(.0f),
			m_MaxAttackDelay(.0f),
			m_AttackInCooldown(false),
			m_Animator(nullptr), m_HasStrongAttack(false), m_StrongAttackProbability(0)
		{
		}

		COrcAttackController::COrcAttackController(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Seeker(nullptr),
			m_MinAttackDelay(.0f),
			m_MaxAttackDelay(.0f),
			m_AttackInCooldown(false),
			m_Animator(nullptr), m_HasStrongAttack(false), m_StrongAttackProbability(0)
		{
		}

		void COrcAttackController::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_MinAttackDelay = strtof(membersValues[0].c_str(), nullptr);
			m_MaxAttackDelay = strtof(membersValues[1].c_str(), nullptr);
			std::istringstream(membersValues[2]) >> std::boolalpha >> m_HasStrongAttack;
			if (m_HasStrongAttack)
			{
				m_StrongAttackProbability = strtof(membersValues[3].c_str(), nullptr);
			}
		}

		void COrcAttackController::Start()
		{
			m_Seeker = m_Parent->GetComponent<CSeeker>();
			m_Animator = m_Parent->GetComponent<orc::COrcAnimator>();
		}

		void COrcAttackController::Update(float dt)
		{
			Attack();
		}

		bool firstAttack = true;
		void COrcAttackController::Attack()
		{
			if (m_Seeker->m_TargetInCombatRange && !m_AttackInCooldown)
			{
				if (firstAttack)
				{
					firstAttack = false;
					m_Animator->SendEvent<sm::Attack>(sm::Attack());
				}
				else
				{
					m_AttackInCooldown = true;
					float waitTime = mathUtils::RandomLToH<float>(m_MinAttackDelay, m_MaxAttackDelay);
					base::utils::CCoroutine::GetInstance().StartCoroutine(
						waitTime,
						[this]()
						{
							//check if enabled for avoiding attacking on corutine while dead
							if (m_Enabled)
							{
								m_AttackInCooldown = false;
								if (m_HasStrongAttack)
								{
									int min = 0;
									int max = 100;
									int probability = mathUtils::RandomLToH(min, max);

									if (probability <= m_StrongAttackProbability) //strong attack probability
									{
										m_Animator->SendEvent<sm::StrongAtack>(sm::StrongAtack());
									}
									else
									{
										m_Animator->SendEvent<sm::Attack>(sm::Attack());
									}
								}
								else
								{
									m_Animator->SendEvent<sm::Attack>(sm::Attack());
								}
							}
						}
					);
				}
			}
		}
	}
}
