#ifndef _ORCATTACKCONTROLLER_H
#define _ORCATTACKCONTROLLER_H
#include "Core/ScriptComponent.h"


namespace logic
{
	namespace orc
	{
		class COrcAnimator;
	}
	using namespace orc;
	namespace components
	{
		class CSeeker;
		class COrcAttackController : public CScriptComponent
		{
		public:
			COrcAttackController();
			COrcAttackController(const std::string& name, const std::string& parent);
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void Start() override;
			void Update(float dt) override;
			void Attack();
		private:
			CSeeker* m_Seeker;
			float m_MinAttackDelay, m_MaxAttackDelay;
			bool m_AttackInCooldown;
			COrcAnimator* m_Animator;
			bool m_HasStrongAttack;
			float m_StrongAttackProbability;
		};
	}
}

#endif