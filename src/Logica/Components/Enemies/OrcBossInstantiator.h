#ifndef _LOGICA_ORCBOSSINSTANTIATOR_H 
#define _LOGICA_ORCBOSSINSTANTIATOR_H 
#include "Core/ScriptComponent.h" 

using namespace engine;

namespace logic
{
	namespace components
	{
		class COrcBossInstantiator : public CScriptComponent
		{
		public:
			COrcBossInstantiator();
			COrcBossInstantiator(const std::string& name, const std::string& parent);
			~COrcBossInstantiator();

			void Start() override;
			void OnEnable() override;
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			bool m_EnemyActivated;
			std::string m_Boss;
		};
	}
}

#endif