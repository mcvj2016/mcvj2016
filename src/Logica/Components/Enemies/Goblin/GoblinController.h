#ifndef _GOBLINCONTROLLER_H
#define _GOBLINCONTROLLER_H
#include "Core/ScriptComponent.h"
#include "GoblinMagic.h"

namespace logic
{
	namespace goblin
	{
		class CGoblinAnimator;
	}

	namespace components
	{
		using namespace goblin;
		class CLife;
		class CSeeker;
		
		class CGoblinController : public CScriptComponent
		{
		public:
			CGoblinController();
			CGoblinController(const std::string& name, const std::string& parent);
			virtual ~CGoblinController();

			void Start() override;
			void Update(float dt) override;
			bool IsEscaping(){ return m_Escaping; }
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void CheckEscapeFromPlayer();
			bool IsDead() const { return m_Death; }
			bool IsEscaping() const { return m_Escaping; }

			void Dead();

		private:
			CSeeker* m_Seeker;
			CSceneNode* m_Player;
			CLife * m_Life;
			bool m_Death, m_Escaping;
			CGoblinAnimator* m_Animator;
			float m_DistanceToEscape;
			CGoblinMagicController* m_MagicController;
		};
	}
}


#endif