#ifndef _GOBLINMAGICCONTROLLER_H
#define _GOBLINMAGICCONTROLLER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace goblin
	{
		class CGoblinAnimator;
	}
	using namespace goblin;
	namespace components
	{
		class CSeeker;
		class CGoblinController;
		class CLife;
		class CGoblinMagicController : public CScriptComponent
		{
		public:
			CGoblinMagicController();
			CGoblinMagicController(const std::string& name, const std::string& parent);
			void CheckTarget();
			CSceneNode* SearchWeakerOrc();
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void Start() override;
			void Update(float dt) override;
			void Heal();
			bool IsHealing() const { return m_Healing; }
			bool GetOrcsToHealDead() const { return m_OrcsToHealDead; }
		private:
			CSeeker* m_Seeker;
			CGoblinAnimator* m_Animator;
			CLife* m_TargetLife;
			bool m_Healing, m_OrcsToHealDead;
			float m_HealPower;
			float m_HealTicks;
			CGoblinController* m_GobController;
			std::vector<CSceneNode*> m_OrcsToHeal;
		};
	}
}

#endif