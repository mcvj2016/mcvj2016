#include "GoblinMagic.h"
#include "Components/IA/Seeker.h"
#include "GoblinAnimator.h"
#include "Components/Common/Life.h"
#include "GoblinController.h"
#include "Utils/Coroutine.h"
#include "Components/Enemies/Orc/OrcController.h"
#include "Utils/StringUtils.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Enemies/Gulak/GulakController.h"


namespace logic
{
	namespace components
	{
		CGoblinMagicController::CGoblinMagicController()
			:
			CScriptComponent(),
			m_Seeker(nullptr),
			m_Animator(nullptr),
			m_TargetLife(nullptr),
			m_Healing(false), m_OrcsToHealDead(false), m_HealPower(0), m_HealTicks(0), m_GobController(nullptr)
		{
		}

		CGoblinMagicController::CGoblinMagicController(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Seeker(nullptr),
			m_Animator(nullptr),
			m_TargetLife(nullptr),
			m_Healing(false), m_OrcsToHealDead(false), m_HealPower(0), m_HealTicks(0), m_GobController(nullptr)
		{
		}

		void CGoblinMagicController::InitMembersValues(std::vector<std::string> membersValues)
		{
			
			m_HealPower = strtof(membersValues[0].c_str(), nullptr);
			m_HealTicks = strtof(membersValues[1].c_str(), nullptr);
			std::vector<std::string> lTargetsToHeal = base::utils::Split(membersValues[2], ';');
			
			for (std::string name : lTargetsToHeal)
			{
				CSceneNode* target = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(name);
				assert(target);
				m_OrcsToHeal.push_back(target);
			}
		}

		CSceneNode* CGoblinMagicController::SearchWeakerOrc()
		{
			float temp = 0;
			float life = FLT_MAX;
			CSceneNode* newTarget = nullptr;
			CSceneNode* orc = nullptr;

			if (!m_OrcsToHeal.empty())
			{
				for (int i = m_OrcsToHeal.size() - 1; i >= 0; i--)
				{
					orc = m_OrcsToHeal[i];
					COrcController* comp = orc->GetComponent<COrcController>();
					CGulakController* comp2 = orc->GetComponent<CGulakController>();
					
					if ((comp!= nullptr && !comp->IsDead())
						|| (comp2 != nullptr && !comp2->IsDead()))
					{
						temp = orc->GetComponent<CLife>()->GetLifePoints();
						if (temp < life)
						{
							life = temp;
							newTarget = orc;
						}
					}
					else
					{
						m_OrcsToHeal.erase(m_OrcsToHeal.begin() + i);
					}
				}
			}

			return newTarget;
		}

		void CGoblinMagicController::CheckTarget()
		{
			//if not yet healing and not escaping
			if (!m_GobController->IsEscaping() && !m_Healing)
			{
				//if not check if need to heal orcs
				//search the orc with less life
				CSceneNode* target = SearchWeakerOrc();
				if (target == nullptr)
				{
					//if all target orcs are dead disable seeker
					m_OrcsToHealDead = true;
					m_Engine.GetImGUI().Log("All orcs are dead");
				}
				else
				{
					m_Seeker->SetTarget(target);
					m_Seeker->SetInvertDirection(false);
					m_TargetLife = m_Seeker->GetTarget()->GetComponent<CLife>();
				}
			}
		}

		void CGoblinMagicController::Heal()
		{
			m_Healing = false;
			if (!m_GobController->IsEscaping())
			{
				if (m_Seeker->m_TargetInMagicRange)
				{
					if (m_TargetLife->GetLifePoints() > 0 //dont heal if dead
						&& m_TargetLife->GetLifePoints() < m_TargetLife->GetMaxLifePoints())
					{
						//performance check
						if (m_Animator->GetActualIndex() != sm::Magic1Index)
						{
							m_Animator->SendEvent<sm::Magic1>(sm::Magic1());
							m_Seeker->m_CanSeek = false;
						}
						m_Healing = true;
						m_TargetLife->AddLifePoints(m_HealPower * m_Engine.GetDeltaTime());
					}
				}
			}
		}

		void CGoblinMagicController::Start()
		{
			m_Seeker = m_Parent->GetComponent<CSeeker>();
			m_Animator = m_Parent->GetComponent<CGoblinAnimator>();
			m_TargetLife = m_Seeker->GetTarget()->GetComponent<CLife>();
			m_GobController = m_Parent->GetComponent<CGoblinController>();
		}

		void CGoblinMagicController::Update(float dt)
		{
			CheckTarget();
			Heal();
		}
	}
}
