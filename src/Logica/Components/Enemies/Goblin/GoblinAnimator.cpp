#include "GoblinAnimator.h"
#include "StateMachines/GoblinStateMachine.h"

namespace logic
{
	namespace goblin
	{
		CGoblinAnimator::CGoblinAnimator()
			: CModelAnimator()
		{
		}

		CGoblinAnimator::CGoblinAnimator(const std::string& name, const std::string& parent)
			:
			CModelAnimator(name, parent)
		{
		}

		CGoblinAnimator::~CGoblinAnimator()
		{
		}

		void CGoblinAnimator::Start()
		{
			m_SM = new CGoblinSM();
			m_SM->m_Animator = this;
			m_SM->SetName(m_Parent->GetName());
			m_Model = reinterpret_cast<CSceneAnimatedModel*>(m_Parent);

			m_SM->Add<Idle>("Idle");
			m_SM->Add<Move>("Move");
			m_SM->Add<Heal>("Heal");
			m_SM->Add<Inmunice>("Inmunice");
			m_SM->Add<Death>("Death");
			m_SM->init("Idle");
			m_SM->dispatch(sm::Init());
		}
	}
}
