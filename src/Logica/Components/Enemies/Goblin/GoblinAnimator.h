#ifndef GOBLINANIMATOR_H
#define GOBLINANIMATOR_H

#include "Core/ScriptComponent.h"
#include "Components/Common/Animator.h"

using namespace logic::components;

namespace logic
{
	namespace goblin
	{
		class CGoblinAnimator : public CModelAnimator
		{
		public:
			CGoblinAnimator();

			CGoblinAnimator(const std::string& name, const std::string& parent);

			~CGoblinAnimator();

			void Start() override;
		};
	}
}
#endif
