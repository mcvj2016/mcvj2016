#include "GoblinController.h"
#include "Components/Common/Life.h"
#include "PhysXImpl/PhysXManager.h"
#include "GoblinAnimator.h"
#include "Components/IA/Seeker.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"

namespace logic
{
	namespace components
	{
		CGoblinController::CGoblinController()
			:
			CScriptComponent(),
			m_Seeker(nullptr),
			m_Player(nullptr),
			m_Life(nullptr),
			m_Death(false),
			m_Animator(nullptr),
			m_DistanceToEscape(0),
			m_MagicController(nullptr),
			m_Escaping(false)
		{
		}

		CGoblinController::CGoblinController(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Seeker(nullptr),
			m_Player(nullptr),
			m_Life(nullptr),
			m_Death(false),
			m_Animator(nullptr),
			m_DistanceToEscape(0),
			m_MagicController(nullptr),
			m_Escaping(false)
		{
		}

		CGoblinController::~CGoblinController()
		{
		}

		void CGoblinController::Start()
		{
			m_Life = m_Parent->GetComponent<CLife>();
			m_Animator = m_Parent->GetComponent<CGoblinAnimator>();
			m_Seeker = m_Parent->GetComponent<CSeeker>();
			m_Player = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player);
			m_MagicController = m_Parent->GetComponent<CGoblinMagicController>();
		}

		void CGoblinController::Update(float dt)
		{
			//check state for enablin/disabling seeker
			if (m_Escaping || !m_MagicController->IsHealing())
			{
				m_Seeker->m_CanSeek = true;
			}
			if (m_Life->GetLifePoints() <= 0.0f && !m_Death)
			{
				Dead();
			}
			CheckEscapeFromPlayer();			
		}

		void CGoblinController::InitMembersValues(std::vector<std::string> membersValues)
		{
			
			m_DistanceToEscape = strtof(membersValues[0].c_str(), nullptr);
		}

		bool inGraceTime = false;
		void CGoblinController::CheckEscapeFromPlayer()
		{
			if (!inGraceTime)
			{
				m_Escaping = false;
			}

			//if we have no more orcs to heal
			if (m_MagicController->GetOrcsToHealDead())
			{
				m_Seeker->SetTarget(m_Player);
				m_Escaping = true;
			}
			else
			{
				//if orkelf is close, escape from him
				float distance = m_Player->GetPosition().Distance(m_Parent->GetPosition());
				if (distance < m_DistanceToEscape)
				{
					
					m_Seeker->SetInvertDirection(true);
					m_Seeker->SetTarget(m_Player);
					m_Escaping = true;
					
					if (!inGraceTime)
					{
						base::utils::CCoroutine::GetInstance().StartCoroutine(
							0.5f,
							[]()
						{
							inGraceTime = false;
						}
						);
					}
					inGraceTime = true;
				}
				else
				{
					
				}
			}
		}

		void CGoblinController::Dead()
		{
			m_Animator->SendEvent<sm::Dead>(sm::Dead());
			m_Death = true;
			//disable all components
			m_Parent->DisableComponents();
			this->DestroyPhysxOnly(m_Parent);
		}
	}
}
