#ifndef _LOGICA_ENEMIESSOUNDMANAGER_H 
#define _LOGICA_ENEMIESSOUNDMANAGER_H 
#include "Core/ScriptComponent.h" 

using namespace engine;

namespace logic
{
	namespace components
	{

		class CAmbientSoundManager;
		class CEnemiesManager : public CScriptComponent
		{
		public:

			CEnemiesManager();
			CEnemiesManager(const std::string& name, const std::string& parent);
			~CEnemiesManager();

			void Start() override;
			void Update(float dt) override;
			void AddEnemyActive();
			void RemoveEnemyActive();

		private:
			uint8 m_EnemiesActives;
			uint8 m_TotalEnemies;
			CAmbientSoundManager* m_AmbientSound;
		};
	}
}

#endif