#ifndef _GULAKACTIVATOR_H
#define _GULAKACTIVATOR_H
#include "Core/ScriptComponent.h"
#include "Graphics/Scenes/Scene.h"

namespace engine {namespace cal3dimpl {
	class CSceneAnimatedModel;
}
}

namespace logic
{
	namespace components
	{
		class CGulakActivator : public CScriptComponent
		{
		public:
			CGulakActivator();
			CGulakActivator(const std::string& name, const std::string& parent);
			virtual ~CGulakActivator();

			void Start() override;
			void OnEnable() override;
			void Update(float dt) override;
			void InitMembersValues(std::vector<std::string> membersValues) override;
		private:
			cal3dimpl::CSceneAnimatedModel* m_Gulak;
			float m_Distance;
			CSceneNode* m_Player;
		};
	}
}
#endif