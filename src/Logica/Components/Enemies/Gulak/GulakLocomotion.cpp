#include "GulakLocomotion.h"
#include "Graphics/Scenes/SceneManager.h"
#include "GulakAnimator.h"
#include <valarray>

namespace logic
{
	namespace components
	{
		CGulakLocomotion::CGulakLocomotion() : CScriptComponent(), m_Animator(nullptr), m_Player(nullptr), m_LockMovement(false), m_MaxAngleWithPlayer(0)
		{
		}

		CGulakLocomotion::CGulakLocomotion(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_Animator(nullptr), m_Player(nullptr), m_LockMovement(false), m_MaxAngleWithPlayer(0)
		{
		}

		void CGulakLocomotion::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_MaxAngleWithPlayer = strtof(membersValues[0].c_str(), nullptr);
		}
		void CGulakLocomotion::Start()
		{
			m_Animator = m_Parent->GetComponent<CGulakAnimator>();
			m_Player = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player2);
		}

		
		void CGulakLocomotion::Update(float dt)
		{
			if (!m_LockMovement)
			{
				//FacePlayer(dt);
				Vect3f newDirection = m_Player->GetPosition() - m_Parent->GetPosition();
				newDirection.y = 0;
				m_Parent->SetForward(newDirection);
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
			}
		}

		void CGulakLocomotion::FacePlayer(float dt)
		{
			// the vector that we want to measure an angle from
			Vect3f referenceForward = m_Parent->GetBackward(); //use backward vector because forward vector sets 180 in front (0 degrees)
			// the vector perpendicular to referenceForward (90 degrees clockwise)
			// (used to determine if angle is positive or negative)
			Vect3f referenceRight = v3fFRONT ^ referenceForward;
			// the vector of interest
			Vect3f newDirection = m_Player->GetPosition() - m_Parent->GetPosition(); /* some vector that we're interested in */
			// Get the angle in degrees between 0 and 180
			float angle = newDirection.GetAngleWith(referenceForward);
			// Determine if the degree value should be negative.  Here, a positive value
			// from the dot product means that our vector is on the right of the reference vector   
			// whereas a negative value means we're on the left.
			float sign = 1;
			if ((newDirection * referenceRight) < 0) //dot product
			{
				sign = -1;
			}
			float finalAngle = sign * angle;

			//calculate angles before setting dir.y = 0
			//float angleWithPlayer = fwd.GetAngleWith(dir);
			//float angle = std::acos((fwd * dir) / (fwd.Length() * dir.Length()));
			finalAngle = mathUtils::Rad2Deg(finalAngle);
			//m_Engine.GetImGUI().Log(std::to_string(finalAngle));


			/*dir.y = .0f;
			m_Parent->SetForward(dir);
			m_Parent->m_Yaw = m_Parent->m_Yaw * -1;*/
			if (finalAngle > m_MaxAngleWithPlayer)
			{
				//newDirection.y = .0f;
				m_Parent->SetForward(newDirection);
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
				//m_Parent->m_Yaw -= dt * 0.5f;
			}
			
			else if (finalAngle < m_MaxAngleWithPlayer)
			{
				//newDirection.y = .0f;
				m_Parent->SetForward(newDirection);
				m_Parent->m_Yaw = m_Parent->m_Yaw * -1;
				//m_Parent->m_Yaw -= dt * 0.5f;
			}
		}
	}
}
