#ifndef _GULAKTTACKCONTROLLER_H
#define _GULAKTTACKCONTROLLER_H
#include "Core/ScriptComponent.h"
#include "Graphics/Scenes/Scene.h"


namespace logic
{
	namespace gulak
	{
		class CGulakAnimator;
	}
	using namespace gulak;
	namespace components
	{
		class CGulakAttackController : public CScriptComponent
		{
		public:
			CGulakAttackController();
			CGulakAttackController(const std::string& name, const std::string& parent);
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void Start() override;
			void Update(float dt) override;
			void OnDeath() const;
			void Attack();
			CSceneNode* m_MagicProjectile;
		private:
			float m_MinAttackDelay, m_MaxAttackDelay;
			bool m_AttackInCooldown;
			CGulakAnimator* m_Animator;
			
			std::string m_MagicProjectileName;
			CSceneNode* m_Player;
		};
	}
}

#endif