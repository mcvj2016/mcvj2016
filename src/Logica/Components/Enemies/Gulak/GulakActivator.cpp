#include "GulakActivator.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Components/GUI/EnemyLifeBar.h"
#include "Components/Common/AmbientSoundManager.h"

namespace logic
{
	namespace components
	{
		CGulakActivator::CGulakActivator(): CScriptComponent(), m_Gulak(nullptr), m_Distance(0), m_Player(nullptr)
		{
		}

		CGulakActivator::CGulakActivator(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), 
			m_Gulak(nullptr), m_Distance(0), m_Player(nullptr)
		{
		}

		CGulakActivator::~CGulakActivator()
		{
		}

		void CGulakActivator::Start()
		{
			m_Gulak = reinterpret_cast<cal3dimpl::CSceneAnimatedModel*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Gulak"));
			assert(m_Gulak);
			m_Player = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player2);
			m_Enabled = false;
		}

		void CGulakActivator::OnEnable()
		{
			m_Gulak->ShowAnimated();
		}

		void CGulakActivator::Update(float dt)
		{
			float distance = m_Player->GetPosition().Distance(m_Gulak->GetPosition());
			if (distance <= m_Distance)
			{
				m_Gulak->SetEnabled(true);
				m_Enabled = false;
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HIT_GULAK);
				CAmbientSoundManager* sm = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")->GetComponent<CAmbientSoundManager>();
				sm->ChangeZone(CAmbientSoundManager::FinalZone);
				m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("VidaGulak")->SetEnabled(true);
			}
		}

		void CGulakActivator::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_Distance = strtof(membersValues[0].c_str(), nullptr);
		}
	}
}
