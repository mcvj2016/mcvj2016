#include "GulakAttackController.h"
#include "GulakAnimator.h"
#include "Utils/Coroutine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Weapon/GulakMagicProjectile.h"

namespace logic
{
	namespace components
	{
		CGulakAttackController::CGulakAttackController()
			:
			CScriptComponent(),
			m_MinAttackDelay(0),
			m_MaxAttackDelay(0),
			m_AttackInCooldown(false),
			m_Animator(nullptr),
			m_MagicProjectile(nullptr), 
			m_Player(nullptr)
		{
		}

		CGulakAttackController::CGulakAttackController(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_MinAttackDelay(0),
			m_MaxAttackDelay(0),
			m_AttackInCooldown(false),
			m_Animator(nullptr), 
			m_MagicProjectile(nullptr), 
			m_Player(nullptr)
		{
		}

		void CGulakAttackController::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_MinAttackDelay = strtof(membersValues[0].c_str(), nullptr);
			m_MaxAttackDelay = strtof(membersValues[1].c_str(), nullptr);
			m_MagicProjectileName = membersValues[2];
		}

		void CGulakAttackController::Start()
		{
			m_Animator = m_Parent->GetComponent<CGulakAnimator>();
			m_MagicProjectile = static_cast<CSceneNode*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_MagicProjectileName));
			
		}

		void CGulakAttackController::Update(float dt)
		{
			if (!m_AttackInCooldown)
			{
				Attack();
			}
		}

		void CGulakAttackController::OnDeath() const
		{
			m_MagicProjectile->GetComponent<CGulakMagicProjectile>()->DisableProjectile();
		}

		void CGulakAttackController::Attack()
		{
			m_AttackInCooldown = true;
			float waitTime = mathUtils::RandomLToH<float>(m_MinAttackDelay, m_MaxAttackDelay);
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime,
				[this]()
				{
					//check if enabled for avoiding attacking on corutine while dead
					if (m_Enabled)
					{
						m_AttackInCooldown = false;
						if (m_Animator->GetActualIndex() != sm::Magic1Index
							&& m_Animator->GetActualIndex() != sm::DeadIndex)
						{
							m_Animator->SendEvent<sm::Magic1>(sm::Magic1());
							CGulakMagicProjectile* projectile = m_MagicProjectile->GetComponent<CGulakMagicProjectile>();
							projectile->Spawn();
						}
					}
				}
			);
		}
	}
}
