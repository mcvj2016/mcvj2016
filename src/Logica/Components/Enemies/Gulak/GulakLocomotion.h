#ifndef _GULAKLOCOMOTIONCONTROLLER_H
#define _GULAKLOCOMOTIONCONTROLLER_H
#include "Core/ScriptComponent.h"


namespace logic
{
	namespace gulak
	{
		class CGulakAnimator;
	}
	using namespace gulak;
	namespace components
	{
		class CGulakLocomotion : public CScriptComponent
		{
		public:
			CGulakLocomotion();
			CGulakLocomotion(const std::string& name, const std::string& parent);
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void Start() override;
			void Update(float dt) override;
			void FacePlayer(float dt);
			GET_SET(bool, LockMovement)
		private:
			CGulakAnimator* m_Animator;
			CSceneNode* m_Player;
			bool m_LockMovement;
			float m_MaxAngleWithPlayer;
		};
	}
}
#endif