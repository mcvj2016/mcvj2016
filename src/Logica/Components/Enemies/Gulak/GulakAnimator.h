#ifndef _H_GULAKANIMATOR_H
#define _H_GULAKANIMATOR_H

#include "Core/ScriptComponent.h"
#include "Components/Common/Animator.h"

using namespace logic::components;

namespace logic
{
	namespace gulak
	{
		class CGulakAnimator : public CModelAnimator
		{
		public:
			CGulakAnimator();

			CGulakAnimator(const std::string& name, const std::string& parent);

			~CGulakAnimator();

			void Start() override;
		};
	}
}
#endif
