#include "GulakController.h"
#include "Graphics/Scenes/SceneManager.h"
#include "GulakAnimator.h"
#include "Components/Common/Life.h"
#include "GulakAttackController.h"

namespace logic
{
	namespace components
	{
		CGulakController::CGulakController() :
			CScriptComponent(),
			m_Life(nullptr),
			m_Death(false),
			m_Animator(nullptr)
		{
		}

		CGulakController::CGulakController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			  m_Life(nullptr),
			  m_Death(false),
			  m_Animator(nullptr)
		{
		}

		CGulakController::~CGulakController()
		{
		}

		void CGulakController::Start()
		{
			m_Life = m_Parent->GetComponent<CLife>();
			m_Animator = m_Parent->GetComponent<CGulakAnimator>();
		}

		void CGulakController::Update(float dt)
		{
			if (m_Life->GetLifePoints() <= 0.0f && !m_Death)
			{
				Dead();
			}
		}

		void CGulakController::InitMembersValues(std::vector<std::string> membersValues)
		{
		}

		void CGulakController::Dead()
		{
			m_Animator->SendEvent<sm::Dead>(sm::Dead());
			m_Parent->GetComponent<CGulakAttackController>()->OnDeath();
			m_Death = true;
			//disable all components
			m_Parent->DisableComponents();
			this->DestroyPhysxOnly(m_Parent);
		}
	}
}
