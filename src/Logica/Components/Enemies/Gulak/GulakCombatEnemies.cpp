#include "GulakCombatEnemies.h"
#include "Utils/Coroutine.h"
#include "Utils/StringUtils.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Components/IA/Seeker.h"
#include "Components/Enemies/EnemyNotifier.h"
#include "Components/Common/Life.h"

namespace logic
{
	namespace components
	{
		CGulakCombatEnemies::CGulakCombatEnemies() : CScriptComponent(), m_GulakLifePercentajeToTrigger(0), m_GulakLife(nullptr)
		{
		}

		CGulakCombatEnemies::CGulakCombatEnemies(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_GulakLifePercentajeToTrigger(0), m_GulakLife(nullptr)
		{
		}

		CGulakCombatEnemies::~CGulakCombatEnemies()
		{
		}

		void CGulakCombatEnemies::Update(float dt)
		{
			float life = m_GulakLife->GetMaxLifePoints()*m_GulakLife->GetLifePoints() / 10000;
			if (life <= m_GulakLifePercentajeToTrigger)
			{
				InstantiateGroup();
				Destroy(m_Parent);
			}
		}

		void CGulakCombatEnemies::Start()
		{
			m_GulakLife = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Gulak")->GetComponent<CLife>();
			//disable enemies sound notifiers
			for (CSceneAnimatedModel* node : m_EnemiesGroup)
			{
				if (node->GetComponent<CEnemyNotifier>() != nullptr)
				{
					node->GetComponent<CEnemyNotifier>()->SetEnemyActivated(true);
				}
			}
		}

		void CGulakCombatEnemies::InitMembersValues(std::vector<std::string> membersValues)
		{
			std::vector<std::string> lTargetsToHeal = base::utils::Split(membersValues[0], ';');

			for (std::string name : lTargetsToHeal)
			{
				CSceneAnimatedModel* target = static_cast<CSceneAnimatedModel*>(m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(name));
				assert(target);
				m_EnemiesGroup.push_back(target);
			}
			m_GulakLifePercentajeToTrigger = strtof(membersValues[1].c_str(), nullptr);
		}

		void CGulakCombatEnemies::InstantiateGroup()
		{
			for (CSceneAnimatedModel* node : m_EnemiesGroup)
			{
				node->ShowAnimated();
				node->SetEnabled(true);
				node->GetComponent<CSeeker>()->m_ForceSeek = true;
				//set target to the new orkelf armor model
				node->GetComponent<CSeeker>()->SetTarget(m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::player2));
			}
		}
	}
}

