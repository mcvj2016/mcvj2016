#include "GulakAnimator.h"
#include "StateMachines/GulakStateMachine.h"

namespace logic
{
	namespace gulak
	{
		CGulakAnimator::CGulakAnimator() : CModelAnimator()
		{
		}

		CGulakAnimator::CGulakAnimator(const std::string& name, const std::string& parent) : CModelAnimator(name, parent)
		{
		}

		CGulakAnimator::~CGulakAnimator()
		{
		}

		void CGulakAnimator::Start()
		{
			m_SM = new CGulakStateMachine();
			m_SM->m_Animator = this;
			m_SM->SetName(m_Parent->GetName());
			m_Model = reinterpret_cast<CSceneAnimatedModel*>(m_Parent);

			m_SM->Add<Idle>("Idle");
			m_SM->Add<MagicAttack1>("MagicAttack1");
			m_SM->Add<TurnArround>("TurnArround");
			m_SM->Add<Death>("Death");
			m_SM->init("Idle");
			m_SM->dispatch(sm::Init());
		}
	}
}
