#ifndef _GULAKCONTROLLER_H
#define _GULAKCONTROLLER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace gulak
	{
		class CGulakAnimator;
	}

	namespace components
	{
		using namespace gulak;
		class CLife;

		class CGulakController : public CScriptComponent
		{
		public:
			CGulakController();
			CGulakController(const std::string& name, const std::string& parent);
			virtual ~CGulakController();

			void Start() override;
			void Update(float dt) override;
			void InitMembersValues(std::vector<std::string> membersValues) override;
			bool IsDead() const { return m_Death; }
			void Dead();

		private:
			CLife * m_Life;
			bool m_Death;
			CGulakAnimator* m_Animator;
		};
	}
}


#endif