#ifndef _LOGICA_GULAKCOMBATENEMIES_H
#define _LOGICA_GULAKCOMBATENEMIES_H
#include "Core/ScriptComponent.h"

namespace engine {namespace cal3dimpl {
	class CSceneAnimatedModel;
}
}

namespace logic
{
	namespace components
	{
		class CLife;

		class CGulakCombatEnemies : public CScriptComponent
		{
		public:
			CGulakCombatEnemies();
			CGulakCombatEnemies(const std::string& name, const std::string& parent);
			virtual ~CGulakCombatEnemies();
			void Update(float dt) override;
			void Start() override;
			void InitMembersValues(std::vector<std::string> membersValues) override;
			void InstantiateGroup();
			std::vector<cal3dimpl::CSceneAnimatedModel*> GetEnemies() const { return m_EnemiesGroup; };
		private:
			std::vector<cal3dimpl::CSceneAnimatedModel*> m_EnemiesGroup;
			float m_GulakLifePercentajeToTrigger;
			CLife* m_GulakLife;
		};
	}
}
#endif