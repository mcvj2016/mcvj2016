#include "CinematicStarter.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Player//PlayerController.h"
#include <sstream>
#include "Utils/Coroutine.h"

namespace logic
{
	namespace components
	{
		CCinematicStarter::CCinematicStarter() : CScriptComponent(), m_Cinematic(nullptr)
			, m_DelayedTime(0.0f), m_DelayedStart(false)
		{
		}

		CCinematicStarter::CCinematicStarter(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_Cinematic(nullptr)
			, m_DelayedTime(0.0f), m_DelayedStart(false)
		{
		}

		CCinematicStarter::~CCinematicStarter()
		{
		}

		void CCinematicStarter::Start()
		{
			m_Cinematic = m_Engine.GetCinematicManager().GetByName(m_CinematicName);
		}

		void CCinematicStarter::Update(float dt)
		{
			if (m_Cinematic->IsFinish())
			{
				this->SetEnabled(false);
				Destroy(m_Parent);
			}
		}

		void CCinematicStarter::OnTriggerEnter(CSceneNode* other)
		{
			if (other->GetTag() == CSceneNode::TAG::player)
			{
				if (m_DelayedStart)
				{
					base::utils::CCoroutine::GetInstance().StartCoroutine(m_DelayedTime, [this]()
					{
						m_Engine.GetCinematicManager().Play(m_CinematicName);
					});
					
				} else
				{
					m_Engine.GetCinematicManager().Play(m_CinematicName);
				}
			}
		}

		void CCinematicStarter::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_CinematicName = membersValues[0];
			m_DelayedTime = strtof(membersValues[1].c_str(), nullptr);
			std::istringstream(membersValues[2]) >> std::boolalpha >> m_DelayedStart;
		}
	}
}
