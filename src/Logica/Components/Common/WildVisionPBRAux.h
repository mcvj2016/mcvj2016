#ifndef _LOGICA_WILDVISIONPBRAUX_H
#define _LOGICA_WILDVISIONPBRAUX_H
#include "Core/ScriptComponent.h"
#include "Render/DrawQuad.h"
#include "Render/DeferredShading.h"

namespace logic
{
	namespace components
	{
		class CWildvisionPBRAux : public CScriptComponent
		{
		public:
			CWildvisionPBRAux();
			CWildvisionPBRAux(const std::string& name, const std::string& parent);
			virtual ~CWildvisionPBRAux();

			void Start() override;
			void Update(float dt) override;

			void WildVisionState(bool active, float VisionState);
		private:
			render::CDeferredShading* m_DeferredShadingCMD;
			render::CDrawQuad* m_DrawPBRCMD;
			bool m_VisionActive;
		};
	}
}
#endif