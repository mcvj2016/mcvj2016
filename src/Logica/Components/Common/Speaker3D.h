#ifndef _SPEAKER3D_H
#define _SPEAKER3D_H
#include "Core/ScriptComponent.h"
#include "Sound/ISoundManager.h"
#include <sstream>

namespace logic
{
	namespace components
	{
		class CSpeaker3D : public CScriptComponent
		{
		public:
			CSpeaker3D() : CScriptComponent(), m_UnregisterOnDestroy(true)
			{
			};
			CSpeaker3D(const std::string& name, const std::string& parent) : CScriptComponent(name, parent), m_UnregisterOnDestroy(true)
			{
			};
			virtual ~CSpeaker3D(){};

			void CSpeaker3D::Start() override
			{
				//register the node as speaker source
				m_Engine.GetSoundManager().RegisterSpeaker(m_Parent);
			}

			void OnDestroy() override
			{
				if (m_UnregisterOnDestroy)
				{
					UnregisterSpeaker();
				}
			}	

			void CSpeaker3D::InitMembersValues(std::vector<std::string> membersValues) override
			{
				if (membersValues.size() > 0)
				{
					std::istringstream(membersValues[0]) >> std::boolalpha >> m_UnregisterOnDestroy;
				}
			}

			void CSpeaker3D::UnregisterSpeaker() const
			{
				//register the node as speaker source
				m_Engine.GetSoundManager().UnregisterSpeaker(m_Parent);
			}

			private:
			bool m_UnregisterOnDestroy;
		};
	}
}
#endif