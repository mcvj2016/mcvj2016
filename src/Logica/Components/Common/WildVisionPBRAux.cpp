#include "WildVisionPBRAux.h"
#include "Input/ActionManager.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"
#include "Render/RenderPipelineManager.h"

namespace logic
{
	namespace components
	{
		CWildvisionPBRAux::CWildvisionPBRAux() : CScriptComponent(), m_VisionActive(false)
		{
		}

		CWildvisionPBRAux::CWildvisionPBRAux(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_VisionActive(false)
		{
		}

		CWildvisionPBRAux::~CWildvisionPBRAux()
		{
		}

		void CWildvisionPBRAux::Start()
		{
			for (render::CRenderCmd* cmd : m_Engine.GetRenderPipelineManager().GetActivePipeline().GetResourcesVector())
			{
				if (cmd->GetName() == "deffered_shading")
				{
					m_DeferredShadingCMD = static_cast<render::CDeferredShading*>(cmd);
				}
				if (cmd->GetName() == "draw_final_pbr")
				{
					m_DrawPBRCMD = static_cast<render::CDrawQuad*>(cmd);
				}
			}
		}

		void CWildvisionPBRAux::Update(float dt)
		{
			if (!m_VisionActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionstart"))
			{
				WildVisionState(true, 1.0f);
			}
			else if(m_VisionActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionend") )
			{
				WildVisionState(false, 0.0f);
			}
		}

		void CWildvisionPBRAux::WildVisionState(bool active, float VisionState)
		{
			m_VisionActive = active;
			/*if (m_DeferredShadingCMD){
				static_cast<CTemplatedMaterialParameter<float>*>(
					m_DeferredShadingCMD->mMaterial->GetParameter("wild_perception"))
					->SetValue(VisionState);
			}*/
			if (m_DrawPBRCMD){
				static_cast<CTemplatedMaterialParameter<float>*>(
					m_DrawPBRCMD->mMaterial->GetParameter("wild_perception"))
					->SetValue(VisionState);
			}
		}
	}
}
