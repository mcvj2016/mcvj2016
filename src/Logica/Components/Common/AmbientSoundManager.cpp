#include "AmbientSoundManager.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>

namespace logic
{
	namespace components
	{
		CAmbientSoundManager::CAmbientSoundManager() :
			CScriptComponent(),
			m_ActiveZone(None), m_InCombat(false)
		{
		}

		CAmbientSoundManager::CAmbientSoundManager(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_ActiveZone(None), m_InCombat(false)
		{
		}

		CAmbientSoundManager::~CAmbientSoundManager()
		{
		}

		void CAmbientSoundManager::Start()
		{
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::ZONES);
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::EFFECTS);
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::UI);
			//play the event
			m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::EXPLORING);
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_MAINTRACKS);
			
		}

		void CAmbientSoundManager::ChangeZone(ActiveZone zone)
		{
			m_ActiveZone = zone;
			switch (zone)
			{
			case Zone4:
				//set zone4 state
				m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::ZONE4);
				break;
			case FinalZone:
				//set final state
				m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::FINALBATTLE);
				break;
			default:
				//set exploring state (main zone)
				m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::EXPLORING);
				break;
			}
		}

		void CAmbientSoundManager::SetInCombat(bool InCombat)
		{
			m_InCombat = InCombat;
			if (InCombat)
			{
				switch (m_ActiveZone)
				{
				case FinalZone:
					//set final battle track
					m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::FINALBATTLE);
					break;
				default:
					//set main battle track
					m_Engine.GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::COMBAT);
					break;
				}
			}
			else 
			{
				ChangeZone(m_ActiveZone);
			}
		}
	}
}
