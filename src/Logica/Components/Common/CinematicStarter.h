#ifndef _LOGICA_CINEMATICSTARTER_H
#define _LOGICA_CINEMATICSTARTER_H
#include "Core/ScriptComponent.h"


namespace engine
{
	namespace cinematics
	{
		class CCinematic;
	}
}

namespace logic
{
	namespace components
	{
		class CPlayerController;

		class CCinematicStarter : public CScriptComponent
		{
		public:
			CCinematicStarter();
			CCinematicStarter(const std::string& name, const std::string& parent);
			~CCinematicStarter();

			void Start() override;
			void Update(float dt) override;

			void OnTriggerEnter(CSceneNode* other) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;

			GET_SET(std::string, CinematicName);

		private:
			std::string m_CinematicName;
			float m_DelayedTime;
			bool m_DelayedStart;
			cinematics::CCinematic* m_Cinematic;
		};
	}
}
#endif