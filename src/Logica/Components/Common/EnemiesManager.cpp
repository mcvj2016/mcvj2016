#include "EnemiesManager.h"
#include "AmbientSoundManager.h"

namespace logic
{
	namespace components
	{
		CEnemiesManager::CEnemiesManager():
			CScriptComponent(),
			m_EnemiesActives(0),
			m_TotalEnemies(0), m_AmbientSound(nullptr)
		{
		}

		CEnemiesManager::CEnemiesManager(const std::string& name, const std::string& parent):
			CScriptComponent(name, parent),
			m_EnemiesActives(0),
			m_TotalEnemies(0), m_AmbientSound(nullptr)
		{
		}

		CEnemiesManager::~CEnemiesManager()
		{
		}

		void CEnemiesManager::Start()
		{
			m_AmbientSound = m_Parent->GetComponent<CAmbientSoundManager>();
		}

		void CEnemiesManager::Update(float dt)
		{
			if (m_EnemiesActives == 0)
			{
				if (m_AmbientSound->GetInCombat())
				{
					m_AmbientSound->SetInCombat(false);
				}
			}
			else
			{
				if (!m_AmbientSound->GetInCombat())
				{
					m_AmbientSound->SetInCombat(true);
				}
			}
		}

		void CEnemiesManager::AddEnemyActive()
		{
			m_EnemiesActives++;
		}

		void CEnemiesManager::RemoveEnemyActive()
		{
			m_EnemiesActives--;
		}
	}
}
