#ifndef _MODELANIMATOR_H
#define _MODELANIMATOR_H
#include "Core/ScriptComponent.h"
#include "StateMachines/AnimatedSM.h"

namespace engine
{
	namespace cal3dimpl
	{
		class CSceneAnimatedModel;
	}
}

using namespace cal3dimpl;

namespace logic
{
	namespace components
	{
		class CModelAnimator : public CScriptComponent
		{
		public:
			CModelAnimator();
			CModelAnimator(const std::string& name, const std::string& parent);
			bool ClearCycle(const sm::AnimIndex& index, float delayOut = .0f) const;
			virtual ~CModelAnimator();

			GET_SET(short int, ActualWeight)
			GET_SET(sm::AnimIndex, ActualIndex)
			GET_SET(CSceneAnimatedModel*, Model)

			template<typename E>
			void SendEvent(E e) const
			{
				//if the event has a weight upper than the actual, dispatch the event
				//first dispatch the stop to enter always in idle
				if (e.Weight > m_ActualWeight)
				{
					m_SM->dispatch(e);
				}
			}

		protected:
			CSceneAnimatedModel* m_Model;
			sm::AnimIndex m_ActualIndex;
			short int m_ActualWeight;
			sm::CAnimatedSM* m_SM;
		};
	}
}

#endif
