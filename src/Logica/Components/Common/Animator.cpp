#include "Animator.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"

namespace logic
{
	namespace components
	{
		CModelAnimator::CModelAnimator()
			:
			CScriptComponent(),
			m_Model(nullptr),
			m_ActualIndex(sm::AnimIndex::NoneIndex),
			m_ActualWeight(-1), 
		m_SM(nullptr)
		{
		}

		CModelAnimator::CModelAnimator(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_Model(nullptr),
			m_ActualIndex(sm::AnimIndex::NoneIndex),
			m_ActualWeight(-1), 
		m_SM(nullptr)
		{
		}

		bool CModelAnimator::ClearCycle(const sm::AnimIndex& index, float delayOut) const
		{
			return m_Model->ClearCycle(m_ActualIndex, delayOut);
		}

		CModelAnimator::~CModelAnimator()
		{
			base::utils::CheckedDelete(m_SM);
		}
	}
}
