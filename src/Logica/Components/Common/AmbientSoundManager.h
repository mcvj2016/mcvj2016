#ifndef _LOGICA_AMBIENTSOUNDMANAGER_H 
#define _LOGICA_AMBIENTSOUNDMANAGER_H 
#include "Core/ScriptComponent.h" 

using namespace engine;
using namespace sound;

namespace logic
{
	namespace components
	{
		class CAmbientSoundManager : public CScriptComponent
		{
		public:

			enum ActiveZone
			{
				None = 0,
				MainZone,
				Zone2,
				Zone3,
				Zone4,
				FinalZone
			};

			CAmbientSoundManager();
			CAmbientSoundManager(const std::string& name, const std::string& parent);
			~CAmbientSoundManager();

			void Start() override;
			void ChangeZone(ActiveZone zone);
			void SetInCombat(bool InCombat);
			bool GetInCombat() const { return m_InCombat; }
			GET_SET(ActiveZone, ActiveZone)

		private:
			ActiveZone m_ActiveZone;
			bool m_InCombat;
		};
	}
}

#endif _LOGICA_AMBIENTSOUNDMANAGER_H