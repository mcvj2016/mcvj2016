#include "Life.h"
#include "Components/IA/Seeker.h"
#include "Utils/Coroutine.h"
#include <sstream>
#include "Components/Player/PlayerAnimator.h"

namespace logic
{
	namespace components
	{
		CLife::CLife() : CScriptComponent(), m_LifePoints(0), m_MaxLifePoints(0), m_InvulnerableHitPeriod(0), m_Invencible(false), m_GodMode(false)
		{
		}

		CLife::CLife(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_LifePoints(0), m_MaxLifePoints(0), m_InvulnerableHitPeriod(0), m_Invencible(false), m_GodMode(false)
		{
		}

		CLife::~CLife()
		{
		}

		void CLife::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_LifePoints = strtof(membersValues[0].c_str(), nullptr);
			m_InvulnerableHitPeriod = strtof(membersValues[1].c_str(), nullptr);
			std::istringstream(membersValues[2]) >> std::boolalpha >> m_GodMode;
			m_MaxLifePoints = strtof(membersValues[3].c_str(), nullptr);
		}

		void CLife::TakeDamage(float damage)
		{
			if (!m_Invencible)
			{
				m_Invencible = true;
				if (!m_GodMode)
				{
					m_LifePoints -= damage;
				}
				//start incencible period of time for avoiding hiperhit with same attack started with physx
				base::utils::CCoroutine::GetInstance().StartCoroutine(
					m_InvulnerableHitPeriod,
					[this]()
					{
						m_Invencible = false;
					}
				);
				CSeeker* seeker = m_Parent->GetComponent<CSeeker>();
				if (seeker != nullptr)
				{
					seeker->m_ForceSeek = true;
				}
			}
		}

		void CLife::AddLifePoints(float life)
		{
			m_LifePoints += life;
			if (m_LifePoints > m_MaxLifePoints)
			{
				m_LifePoints = m_MaxLifePoints;
			}
		}
	}
}

