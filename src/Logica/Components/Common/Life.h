#ifndef _LOGICA_LIFE_H
#define _LOGICA_LIFE_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CLife : public CScriptComponent
		{
		public:
			CLife();
			CLife(const std::string& name, const std::string& parent);
			virtual ~CLife();

			void InitMembersValues(std::vector<std::string> membersValues) override;

			void TakeDamage(float damage);
			void AddLifePoints(float life);

			GET_SET(bool, GodMode)
			GET_SET(bool, Invencible)
			GET_SET(float, LifePoints);
			GET_SET(float, MaxLifePoints);

		private:
			float m_LifePoints, m_MaxLifePoints, m_InvulnerableHitPeriod;
			bool m_Invencible, m_GodMode;
		};
	}
}
#endif