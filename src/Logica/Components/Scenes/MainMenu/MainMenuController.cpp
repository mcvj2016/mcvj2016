#include "MainMenuController.h" 
#include "Sound/ISoundManager.h" 
#include <Wwise_IDs.h>

namespace logic
{
	namespace components
	{
		CMainMenuController::CMainMenuController()
			:CScriptComponent()
		{
		}

		CMainMenuController::CMainMenuController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
		}

		CMainMenuController::~CMainMenuController()
		{
		}

		void CMainMenuController::Start()
		{
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::MAINMENU);
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::UI);
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_MAINMENU, "Default2DSpeaker");
		}
	}
}
