#ifndef _LOGICA_MAINMENUCONTROLLERSCRIPT_H 
#define _LOGICA_MAINMENUCONTROLLERSCRIPT_H 
#include "Core/ScriptComponent.h" 

using namespace engine;

namespace logic
{
	namespace components
	{
		class CMainMenuController : public CScriptComponent
		{
		public:
			CMainMenuController();
			CMainMenuController(const std::string& name, const std::string& parent);
			~CMainMenuController();

			void Start() override;
		};
	}
}

#endif _LOGICA_MAINMENUCONTROLLERSCRIPT_H