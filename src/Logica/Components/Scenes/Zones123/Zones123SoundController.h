#ifndef _LOGICA_ZONES123SOUNDCTRL_H 
#define _LOGICA_ZONES123SOUNDCTRL_H 
#include "Core/ScriptComponent.h" 

using namespace engine;

namespace logic
{
	namespace components
	{
		class CZones123SoundController : public CScriptComponent
		{
		public:
			CZones123SoundController();
			CZones123SoundController(const std::string& name, const std::string& parent);
			~CZones123SoundController();

			void Start() override;
		};
	}
}

#endif _LOGICA_ZONES123SOUNDCTRL_H