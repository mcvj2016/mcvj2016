#include "Zones123SoundController.h" 
#include "Sound/ISoundManager.h" 
#include <Wwise_IDs.h>

namespace logic
{
	namespace components
	{
		CZones123SoundController::CZones123SoundController()
			:CScriptComponent()
		{
		}

		CZones123SoundController::CZones123SoundController(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
		{
		}

		CZones123SoundController::~CZones123SoundController()
		{
		}

		void CZones123SoundController::Start()
		{
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::ZONES);
			m_Engine.GetSoundManager().LoadSoundBank(AK::BANKS::EFFECTS);
			//set exploring state
			m_Engine.GetSoundManager().SetSwitch(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::EXPLORING);
			//play the event
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_MAINTRACKS);
		}
	}
}
