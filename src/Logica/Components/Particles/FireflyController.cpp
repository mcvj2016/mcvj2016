#include "FireflyController.h"
#include "Graphics/Particles/ParticleSystemInstance.h"

namespace logic
{
	namespace components
	{
		CFireFly::CFireFly() : CScriptComponent(), m_AcumTime(0.0f)
		{
		}

		CFireFly::CFireFly(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_AcumTime(0.0f)
		{
		}

		CFireFly::~CFireFly()
		{
		}

		void CFireFly::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_ChangeTime = strtof(membersValues[0].c_str(), nullptr);
		}

		void CFireFly::Start()
		{
		}

		void CFireFly::Update(float dt)
		{
			m_AcumTime += dt;
			particles::CParticleSystemInstance* particleInstance = static_cast<particles::CParticleSystemInstance*>(m_Parent);
			particles::CParticleSystemInstance::ParticleData * particles = particleInstance->GetParticleData();
			if (m_AcumTime >= m_ChangeTime){
				for (int i = 0; i < particleInstance->m_ActiveParticles; ++i)
				{
					particles::CParticleSystemInstance::ParticleData *particle = &particles[i];
					particle->Velocity = particle->Velocity * -1;
				}
				m_AcumTime = 0.0f;
			}
		}

	}
}
