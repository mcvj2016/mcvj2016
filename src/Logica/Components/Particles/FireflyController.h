#ifndef _LOGICA_FIREFLY_H
#define _LOGICA_FIREFLY_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CFireFly : public CScriptComponent
		{
		public:
			CFireFly();
			CFireFly(const std::string& name, const std::string& parent);
			virtual ~CFireFly();

			void Start() override;
			void Update(float dt) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;
		private:
			float m_AcumTime;
			float m_ChangeTime;
		};
	}
}
#endif