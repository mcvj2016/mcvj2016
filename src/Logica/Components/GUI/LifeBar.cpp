#include "LifeBar.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CLifeBar::CLifeBar()
			: m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			  , m_windowPosX(0.0f), m_windowPosY(0.0f), m_windowSizeX(400.0f), m_windowSizeY(150.0f)
			  , m_barPosX(104.0f), m_barPosY(40.0f), m_barSizeX(250.0f), m_barSizeY(30.0f)
			  , m_barImageSizeX(250.0f), m_barImageSizeY(30.0f)
			  , m_paddingX(-4.0f), m_paddingY(0.0f)
			  , m_minSizeX(0.0f), m_minSizeY(0.0f)
			  , m_Add(false), m_Substraction(false)
			  , m_BarSpeed(0.0f), m_NewBarLength(0), m_MaxLifePoints(0), m_BarLength(0), m_PreviousPoints(0), m_PlayerLife(nullptr)
		{
		}

		CLifeBar::CLifeBar(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
			, m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			, m_windowPosX(0.0f), m_windowPosY(0.0f), m_windowSizeX(400.0f), m_windowSizeY(150.0f)
			, m_barPosX(104.0f), m_barPosY(40.0f), m_barSizeX(250.0f), m_barSizeY(30.0f)
			, m_barImageSizeX(250.0f), m_barImageSizeY(30.0f)
			, m_paddingX(-4.0f), m_paddingY(0.0f)
			, m_minSizeX(0.0f), m_minSizeY(0.0f)
			, m_Add(false), m_Substraction(false)
			, m_BarSpeed(0.0f), m_NewBarLength(0), m_MaxLifePoints(0), m_BarLength(0), m_PreviousPoints(0), m_PlayerLife(nullptr)
		{
			
		}

		CLifeBar::~CLifeBar()
		{
		}

		void CLifeBar::Start()
		{
			m_PlayerLife = m_Engine.GetSceneManager().GetCurrentScene()->GetFirstSceneNodeByTag(CSceneNode::TAG::player)
				->GetComponent<CLife>();
			m_MaxLifePoints = m_PlayerLife->GetLifePoints();
			m_BarLength = m_barSizeX;
			m_PreviousPoints = m_PlayerLife->GetLifePoints();
		}

		void CLifeBar::Update(float dt)
		{
			UpdateBarValues(dt);
			DrawLifeBar();
		}
		
		void CLifeBar::UpdateBarValues(float dt)
		{
			if (m_PreviousPoints != m_PlayerLife->GetLifePoints()){
				m_NewBarLength = m_BarLength * (m_PlayerLife->GetLifePoints() / m_MaxLifePoints);
				if (m_NewBarLength > m_BarLength)
				{
					m_NewBarLength = m_BarLength;
				}
				else if (m_NewBarLength > 0.1f && m_NewBarLength < 0.51f)
				{
					m_NewBarLength = 0.51f;
				}
				else if (m_NewBarLength <= 0.1f)
				{
					m_NewBarLength = 0.1f;
				}

				if (m_BarSpeed == 0.0f)
				{
					m_barSizeX = m_NewBarLength;
				}
				else
				{
					if (m_PlayerLife->GetLifePoints() > m_PreviousPoints)
					{
						m_Add = true;
					}
					else
					{
						m_Substraction = true;
					}
				}
				m_PreviousPoints = m_PlayerLife->GetLifePoints();
			}			
			if (m_Substraction){
				m_barSizeX -= dt * m_BarSpeed;
				if (m_NewBarLength >= m_barSizeX)
				{
					m_barSizeX = m_NewBarLength;
					m_Substraction = false;
				}
			}
			if (m_Add){
				m_barSizeX += dt * m_BarSpeed;
				if (m_NewBarLength <= m_barSizeX)
				{
					m_barSizeX = m_NewBarLength;
					m_Add = false;
				}
			}			
		}

		void CLifeBar::DrawLifeBar() const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();

			m_ImguiHelper.SetNextWindowPos(m_windowPosX, m_windowPosY);
			m_ImguiHelper.SetNextWindowSize(m_windowSizeX, m_windowSizeY);
			m_ImguiHelper.NewWindowWithFlags("LifeBarBackgound", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image("Orkelf_logo.dds", m_windowSizeX, m_windowSizeY);
			m_ImguiHelper.EndWindow();
			
			m_ImguiHelper.SetNextWindowPos(m_barPosX, m_barPosY);
			m_ImguiHelper.SetNextWindowSize(m_barSizeX, m_barSizeY);
			m_ImguiHelper.SetNextWindowPadding(m_paddingX, m_paddingY);
			m_ImguiHelper.SetNextWindowMinSize(m_minSizeX, m_minSizeY);
			m_ImguiHelper.NewWindowWithFlags("LifeBar", IMGUI_DisableWindowInteractionFlag);			
			m_ImguiHelper.Image("Orkelf_Barra_verde.dds", m_barImageSizeX, m_barImageSizeY);
			m_ImguiHelper.EndWindow();
			m_ImguiHelper.SetSavedWindowMinSze();
			m_ImguiHelper.SetSavedWindowPadding();
		}

		void CLifeBar::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_BarSpeed = strtof(membersValues[0].c_str(), nullptr);
		}
	}
}