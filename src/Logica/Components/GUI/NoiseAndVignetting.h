#ifndef _LOGICA_NOISEVIGNETTING_H
#define _LOGICA_NOISEVIGNETTING_H
#include "Core/ScriptComponent.h"
#include "Render/RenderCmd.h"

namespace logic
{
	namespace components
	{
		class CNoiseVignetting : public CScriptComponent
		{
		public:
			CNoiseVignetting();
			CNoiseVignetting(const std::string& name, const std::string& parent);
			~CNoiseVignetting();

			void Start() override;
			void SetVigneteImg(const std::string& img);
			void Update(float dt) override;
			void DrawVignetting(const std::string& img) const;

			bool m_ForCinematic;

			CImguiHelper& m_ImguiHelper;
		private:
			render::CRenderCmd* m_NoiseVignettingCMD;
			bool m_NoiseVignettingActive;
			std::string m_VigneteImg;
			void DrawNoiseAndVignetting();
			void DrawNoiseAndVignettingOffScreen();
		};
	}
}
#endif