#ifndef _LOGICA_IMGFADER_H
#define _LOGICA_IMGFADER_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "Math/Color.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CImgFader : public CScriptComponent
		{
			
		public:
			enum FadeStatus
			{
				none = 0,
				in,
				out
			};
			CImgFader();

			CImgFader(const std::string& name, const std::string& parent);

			virtual ~CImgFader(){};

			bool FadeIn(const std::string& imgName);
			bool FadeOut(const std::string& imgName);
			FadeStatus GetStatus() const { return m_FadeStatus; }
		private:
			void Draw(const std::string& imgName) const;
			CImguiHelper& m_ImguiHelper;
			FadeStatus m_FadeStatus;
			float m_Alpha;
		};
	}
}

#endif _LOGICA_MAINMENUGUI_H