#ifndef _LOGICA_ENEMYLIFEBAR_H
#define _LOGICA_ENEMYLIFEBAR_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "Components/Common/Life.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CEnemyLifeBar : public CScriptComponent
		{
		public:
			CEnemyLifeBar();
			CEnemyLifeBar(const std::string& name, const std::string& parent);
			virtual ~CEnemyLifeBar();

			void Start() override;
			void Update(float dt) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;

			CImguiHelper& m_ImguiHelper;

			float m_windowPosX, m_windowPosY, m_windowSizeX, m_windowSizeY;
			float m_paddingX, m_paddingY;
			float m_minSizeX, m_minSizeY;
		private:
			std::string m_EnemyName;
			bool m_Add;
			bool m_Substraction;
			float m_BarSpeed;
			float m_NewBarLength;
			float m_MaxLifePoints;
			float m_BarLength;
			float m_PreviousPoints;
			CLife* m_EnemyLife;			
			void UpdateBarValues(float dt);
			void DrawLifeBar() const;			
		};
	}
}

#endif