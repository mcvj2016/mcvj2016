#ifndef _LOGICA_INGAMEMENUGAMEOVER_H
#define _LOGICA_INGAMEMENUGAMEOVER_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CInGameMenuGameOver : public CScriptComponent
		{
		public:
			CInGameMenuGameOver();
			CInGameMenuGameOver(const std::string& name, const std::string& parent);
			~CInGameMenuGameOver();

			void Start() override;
			void DrawChangingScene() const;
			void Update(float dt) override;
			void OnEnable() override;
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			unsigned short int buttonsWidth = 375;
			unsigned short int buttonsHeight = 95;
			void Draw();
			void DrawTitle() const;
			void DrawButtonsHolder() const;
			void ResetHovered();
			void LastHovered();
			void DrawButtons();
			CSceneNode* m_PauseNode;
			CImguiHelper& m_ImguiHelper;
			std::string m_ContinueImgNameOrig, m_ContinueImgName, m_ContinueImgNameHover,
				m_ExitImgNameOrig, m_ExitImgName, m_ExitImgNameHover, m_BackgroundImgName,
				m_PauseMenuName, m_SceneToChange, m_SceneToExit, m_SceneToReload, m_TitleImgName;
			bool m_IsContinueButtonHovered, m_WasContinueButtonHovered,
				m_IsExitButtonHovered, m_WasExitButtonHovered, m_DrawChangingScene;
		};
	}
}
#endif