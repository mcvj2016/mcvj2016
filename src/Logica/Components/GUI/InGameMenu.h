#ifndef _LOGICA_INGAMEMENU_H
#define _LOGICA_INGAMEMENU_H
#include "Core/ScriptComponent.h"

namespace logic
{
	namespace components
	{
		class CInGameMenu : public CScriptComponent
		{
		public:
			CInGameMenu();
			CInGameMenu(const std::string& name, const std::string& parent);
			~CInGameMenu();

			void Update(float dt) override;
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			unsigned short int buttonsWidth = 375;
			unsigned short int buttonsHeight = 95;
			void Draw();
			void DrawButtonsHolder() const;
			void ResetHovered();
			void LastHovered();
			void DrawButtons();
			CImguiHelper& m_ImguiHelper;
			std::string m_ContinueImgNameOrig, m_ContinueImgName, m_ContinueImgNameHover,
				m_ExitImgNameOrig, m_ExitImgName, m_ExitImgNameHover, m_BackgroundImgName;
			bool m_Draw, m_IsContinueButtonHovered, m_WasContinueButtonHovered, 
				m_IsExitButtonHovered, m_WasExitButtonHovered;
		};
	}
}
#endif