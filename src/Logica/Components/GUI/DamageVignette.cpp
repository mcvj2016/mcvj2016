#include "DamageVignette.h"

namespace logic
{
	namespace components
	{
		CDamageVignetting::CDamageVignetting()
			:
			CScriptComponent(),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_VigneteName("damage.png"), m_Draw(false),
			m_TintColor(CColor(1, 1, 1, 1))
		{
		}

		CDamageVignetting::CDamageVignetting(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_VigneteName("damage.png"), m_Draw(false),
			m_TintColor(CColor(1,1,1,1))
		{
		}

		CDamageVignetting::~CDamageVignetting()
		{
		}

		void CDamageVignetting::SetColor(const CColor& color)
		{
			m_TintColor = color;
		}

		CColor CDamageVignetting::GetColor()
		{
			return m_TintColor;
		}

		void CDamageVignetting::Update(float dt)
		{
			if (m_Draw)
			{
				Draw();
			}
		}

		void CDamageVignetting::DrawVignetting()
		{
			m_Draw = true;
		}

		void CDamageVignetting::StopDrawVignetting()
		{
			m_Draw = false;
		}

		void CDamageVignetting::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_VigneteName = membersValues[0];
		}

		void CDamageVignetting::Draw() const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(-20.0f, -20.0f);
			m_ImguiHelper.SetNextWindowSize(ImGui::GetIO().DisplaySize.x + 25.0f, ImGui::GetIO().DisplaySize.y + 25);
			m_ImguiHelper.NewWindowWithFlags(std::string("Vignetting_" + m_VigneteName).c_str(), IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.ImageRGBA(m_VigneteName, ImGui::GetIO().DisplaySize.x + 25, ImGui::GetIO().DisplaySize.y + 25, m_TintColor);
			m_ImguiHelper.EndWindow();
		}
	}
}