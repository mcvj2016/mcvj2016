#include "EnemyLifeBar.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CEnemyLifeBar::CEnemyLifeBar()
			: m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			  , m_windowPosX(433.0f), m_windowPosY(665.0f), m_windowSizeX(414.0f), m_windowSizeY(20.0f)
			  , m_paddingX(-4.0f), m_paddingY(0.0f)
			  , m_minSizeX(0.0f), m_minSizeY(0.0f)
			  , m_Add(false), m_Substraction(false)
			  , m_BarSpeed(0.0f), m_NewBarLength(0), m_MaxLifePoints(0), m_BarLength(0), m_PreviousPoints(0), m_EnemyLife(nullptr)
		{
		}

		CEnemyLifeBar::CEnemyLifeBar(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent)
			, m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			, m_windowPosX(433.0f), m_windowPosY(665.0f), m_windowSizeX(414.0f), m_windowSizeY(20.0f)
			, m_paddingX(-4.0f), m_paddingY(0.0f)
			, m_minSizeX(0.0f), m_minSizeY(0.0f)
			, m_Add(false), m_Substraction(false), m_NewBarLength(0), m_MaxLifePoints(0), m_BarLength(0), m_PreviousPoints(0), m_EnemyLife(nullptr)
			, m_BarSpeed(0.0f)
		{
		}

		CEnemyLifeBar::~CEnemyLifeBar()
		{
		}

		void CEnemyLifeBar::Start()
		{
			m_EnemyLife = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_EnemyName)
				->GetComponent<CLife>();
			m_MaxLifePoints = m_EnemyLife->GetLifePoints();
			m_BarLength = m_windowPosX;
			m_PreviousPoints = m_EnemyLife->GetLifePoints();
		}

		void CEnemyLifeBar::Update(float dt)
		{
			if (m_EnemyLife->GetLifePoints() <= 0)
			{
				Destroy(m_Parent);
			}
			else
			{
				UpdateBarValues(dt);
				DrawLifeBar();
			}
		}
		
		void CEnemyLifeBar::UpdateBarValues(float dt)
		{
			if (m_PreviousPoints != m_EnemyLife->GetLifePoints()){
				m_NewBarLength = m_BarLength * (m_EnemyLife->GetLifePoints() / m_MaxLifePoints);
				if (m_NewBarLength > m_BarLength)
				{
					m_NewBarLength = m_BarLength;
				}
				else if (m_NewBarLength > 0.1f && m_NewBarLength < 0.51f)
				{
					m_NewBarLength = 0.51f;
				}
				else if (m_NewBarLength <= 0.1f)
				{
					m_NewBarLength = 0.1f;
				}

				if (m_BarSpeed == 0.0f)
				{
					m_windowSizeX = m_NewBarLength;
				}
				else
				{
					if (m_EnemyLife->GetLifePoints() > m_PreviousPoints)
					{
						m_Add = true;
					}
					else
					{
						m_Substraction = true;
					}
				}
				m_PreviousPoints = m_EnemyLife->GetLifePoints();
			}
			if (m_Substraction){
				m_windowSizeX -= dt * m_BarSpeed;
				if (m_NewBarLength >= m_windowSizeX)
				{
					m_windowSizeX = m_NewBarLength;
					m_Substraction = false;
				}
			}
			if (m_Add){
				m_windowSizeX += dt * m_BarSpeed;
				if (m_NewBarLength <= m_windowSizeX)
				{
					m_windowSizeX = m_NewBarLength;
					m_Add = false;
				}
			}
		}

		void CEnemyLifeBar::DrawLifeBar() const
		{
			m_ImguiHelper.SetWindowBackGroundColor(Vect4f(1.0f, 0.0f, 0.0f, 1.0f));

			m_ImguiHelper.SetNextWindowPos(m_windowPosX, m_windowPosY);
			m_ImguiHelper.SetNextWindowSize(m_windowSizeX, m_windowSizeY);
			m_ImguiHelper.SetNextWindowPadding(m_paddingX, m_paddingY);
			m_ImguiHelper.SetNextWindowMinSize(m_minSizeX, m_minSizeY);
			m_ImguiHelper.DisableRoundedWindow();
			m_ImguiHelper.NewWindowWithFlags("EnemyLifeBar", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.EndWindow();
			m_ImguiHelper.RestorePushedVar();
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetSavedWindowMinSze();
			m_ImguiHelper.SetSavedWindowPadding();
		}

		void CEnemyLifeBar::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_BarSpeed = strtof(membersValues[0].c_str(), nullptr);
			m_EnemyName = membersValues[1];
		}

	}
}