#ifndef _LOGICA_DAMAGEVIGNETTING_H
#define _LOGICA_DAMAGEVIGNETTING_H
#include "Core/ScriptComponent.h"
#include "Render/RenderCmd.h"
#include "Math/Color.h"

namespace logic
{
	namespace components
	{
		class CDamageVignetting : public CScriptComponent
		{
		public:
			CDamageVignetting();
			CDamageVignetting(const std::string& name, const std::string& parent);
			~CDamageVignetting();
			void SetColor(const CColor& color);
			CColor GetColor();

			void Update(float dt) override;
			void DrawVignetting();
			void StopDrawVignetting();
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			void Draw() const;
			CImguiHelper& m_ImguiHelper;
			std::string m_VigneteName;
			CColor m_TintColor;
			bool m_Draw;
		};
	}
}
#endif