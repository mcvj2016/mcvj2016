#include "Ending.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	namespace components
	{
		CEnding::CEnding() : CScriptComponent(), m_ImgName(""), m_FadedIn(false), m_FadedOut(false), m_ImgFader(nullptr), m_ImguiHelper(m_Engine.GetImGUI())
		{
		}

		CEnding::CEnding(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_ImgName(""), m_FadedIn(false), m_FadedOut(false), m_ImgFader(nullptr), m_ImguiHelper(m_Engine.GetImGUI())
		{
		}

		void CEnding::Start()
		{
			m_ImgFader = m_Parent->GetComponent<CImgFader>();
			m_Enabled = false;
		}

		void CEnding::Update(float dt)
		{
			if (!m_FadedIn)
			{
				m_FadedIn = m_ImgFader->FadeIn(m_FadeImgname);
			}
			else
			{
				DrawEnding();
				if (!m_FadedOut)
				{
					m_FadedOut = m_ImgFader->FadeOut(m_FadeImgname);
				}
			}
		}

		void CEnding::DrawEnding() const
		{
			m_ImguiHelper.FullScreenWindow("Ending_" + m_ImgName);
			m_ImguiHelper.FullScreenImage(m_ImgName);
			m_ImguiHelper.EndWindow();
		}

		void CEnding::OnEnable()
		{
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Inventario")->SetEnabled(false);
			m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Vida")->SetEnabled(false);
		}

		void CEnding::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_ImgName = membersValues[0];
			m_FadeImgname = membersValues[1];
		}
	}
}
