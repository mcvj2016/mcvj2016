#include "Inventory.h"
#include "Components/Player/PlayerInventory.h"


namespace logic
{
	namespace components
	{
		CInventory::CInventory()
			: m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			, m_inventoryPosX(922.0f), m_inventoryPosY(-8.0f), m_inventorySizeX(360.0f), m_inventorySizeY(200.0f)
			, m_helmPosX(990.0f), m_helmPosY(-2.0f), m_helmSizeX(75.0f), m_helmSizeY(75.0f)
			, m_keyPosX(1021.0f), m_keyPosY(27.0f), m_keySizeX(80.0f), m_keySizeY(80.0f)
			, m_bucketPosX(1060.0f), m_bucketPosY(51.0f), m_bucketSizeX(80.0f), m_bucketSizeY(80.0f)
			, m_chestPosX(1084.0f), m_chestPosY(-13.0f), m_chestSizeX(80.0f), m_chestSizeY(80.0f)
			, m_ropePosX(1113.0f), m_ropePosY(43.0f), m_ropeSizeX(85.0f), m_ropeSizeY(85.0f)
			, m_bootsPosX(1149.0f), m_bootsPosY(-19.000f), m_bootsSizeX(85.0f), m_bootsSizeY(85.0f)
		{
			
		}

		CInventory::CInventory(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI())
			, m_inventoryPosX(922.0f), m_inventoryPosY(-8.0f), m_inventorySizeX(360.0f), m_inventorySizeY(200.0f)
			, m_helmPosX(990.0f), m_helmPosY(-2.0f), m_helmSizeX(75.0f), m_helmSizeY(75.0f)
			, m_keyPosX(1021.0f), m_keyPosY(27.0f), m_keySizeX(80.0f), m_keySizeY(80.0f)
			, m_bucketPosX(1060.0f), m_bucketPosY(51.0f), m_bucketSizeX(80.0f), m_bucketSizeY(80.0f)
			, m_chestPosX(1084.0f), m_chestPosY(-13.0f), m_chestSizeX(80.0f), m_chestSizeY(80.0f)
			, m_ropePosX(1113.0f), m_ropePosY(43.0f), m_ropeSizeX(85.0f), m_ropeSizeY(85.0f)
			, m_bootsPosX(1149.0f), m_bootsPosY(-19.000f), m_bootsSizeX(85.0f), m_bootsSizeY(85.0f)
		{
			
		}

		CInventory::~CInventory()
		{
		}

		void CInventory::Start()
		{
			
		}

		void CInventory::Update(float dt)
		{
			DrawInventory();
		}
		
		void CInventory::DrawInventory()
		{
			m_ImguiHelper.DisableWindowBackGroundColor();

			m_ImguiHelper.SetNextWindowPos(m_inventoryPosX, m_inventoryPosY);
			m_ImguiHelper.SetNextWindowSize(m_inventorySizeX, m_inventorySizeY);
			m_ImguiHelper.NewWindowWithFlags("InventoryImage", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image("inventario.dds", m_inventorySizeX, m_inventorySizeY);
			m_ImguiHelper.EndWindow();

			if (CPlayerInventory::GetInstance().m_hasHelm){
				m_ImguiHelper.SetNextWindowPos(m_helmPosX, m_helmPosY);
				m_ImguiHelper.SetNextWindowSize(m_helmSizeX, m_helmSizeY);
				m_ImguiHelper.NewWindowWithFlags("HelmImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("yelmo_dibujo-verde.dds", m_helmSizeX, m_helmSizeY);
				m_ImguiHelper.EndWindow();
			}

			if (CPlayerInventory::GetInstance().m_hasKey){
				m_ImguiHelper.SetNextWindowPos(m_keyPosX, m_keyPosY);
				m_ImguiHelper.SetNextWindowSize(m_keySizeX, m_keySizeY);
				m_ImguiHelper.NewWindowWithFlags("KeyImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("llave_dibujo-verde.dds", m_keySizeX, m_keySizeY);
				m_ImguiHelper.EndWindow();
			}

			if (CPlayerInventory::GetInstance().m_hasBucket){
				m_ImguiHelper.SetNextWindowPos(m_bucketPosX, m_bucketPosY);
				m_ImguiHelper.SetNextWindowSize(m_bucketSizeX, m_bucketSizeY);
				m_ImguiHelper.NewWindowWithFlags("BucketImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("cubo-dibujo-verde.dds", m_bucketSizeX, m_bucketSizeY);
				m_ImguiHelper.EndWindow();
			}

			if (CPlayerInventory::GetInstance().m_hasChest){
				m_ImguiHelper.SetNextWindowPos(m_chestPosX, m_chestPosY);
				m_ImguiHelper.SetNextWindowSize(m_chestSizeX, m_chestSizeY);
				m_ImguiHelper.NewWindowWithFlags("ChestImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("pechera_dibujo-verde.dds", m_chestSizeX, m_chestSizeY);
				m_ImguiHelper.EndWindow();
			}

			if (CPlayerInventory::GetInstance().m_hasRope){
				m_ImguiHelper.SetNextWindowPos(m_ropePosX, m_ropePosY);
				m_ImguiHelper.SetNextWindowSize(m_ropeSizeX, m_ropeSizeY);
				m_ImguiHelper.NewWindowWithFlags("RopeImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("cuerda_dibujo-verde.dds", m_ropeSizeX, m_ropeSizeY);
				m_ImguiHelper.EndWindow();
			}

			if (CPlayerInventory::GetInstance().m_hasBoots){
				m_ImguiHelper.SetNextWindowPos(m_bootsPosX, m_bootsPosY);
				m_ImguiHelper.SetNextWindowSize(m_bootsSizeX, m_bootsSizeY);
				m_ImguiHelper.NewWindowWithFlags("BootsImage", IMGUI_DisableWindowInteractionFlag);
				m_ImguiHelper.Image("grebas_dibujo-verde.dds", m_bootsSizeX, m_bootsSizeY);
				m_ImguiHelper.EndWindow();
			}
		}

	}
}
