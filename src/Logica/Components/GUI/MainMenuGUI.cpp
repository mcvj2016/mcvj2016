#include "MainMenuGUI.h"
#include "Engine.h"
#include "Sound/ISoundManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Render/RenderPipelineManager.h"
#include "Wwise_IDs.h"
#include "Utils/Coroutine.h"

logic::components::CMainMenuGUI::CMainMenuGUI()
	: m_ImguiHelper(m_Engine.GetImGUI()),
	  m_IsPlayButtonHovered(false),
	  m_WasPlayButtonHovered(false),
	  m_IsCreditsButtonHovered(false),
	  m_WasCreditsButtonHovered(false),
	  m_IsOptionsButtonHovered(false),
	  m_WasOptionsButtonHovered(false),
	  m_IsExitButtonHovered(false),
	  m_WasExitButtonHovered(false),
	  m_IsBackButtonHovered(false),
	  m_WasBackButtonHovered(false),
	  m_IsGamepadButtonHovered(false),
	  m_WasGamepadButtonHovered(false),
	  m_IsKeyboardButtonHovered(false),
	  m_WasKeyBoardButtonHovered(false), 
	  m_Opening(nullptr)
{
}

logic::components::CMainMenuGUI::CMainMenuGUI(const std::string& name, const std::string& parent)
	: CScriptComponent(name, parent), m_ImguiHelper(m_Engine.GetImGUI()),
	m_IsPlayButtonHovered(false),
	m_WasPlayButtonHovered(false),
	m_IsCreditsButtonHovered(false),
	m_WasCreditsButtonHovered(false),
	m_IsOptionsButtonHovered(false),
	m_WasOptionsButtonHovered(false),
	m_IsExitButtonHovered(false),
	m_WasExitButtonHovered(false),
	m_IsBackButtonHovered(false),
	m_WasBackButtonHovered(false),
	m_IsGamepadButtonHovered(false),
	m_WasGamepadButtonHovered(false),
	m_IsKeyboardButtonHovered(false),
	m_WasKeyBoardButtonHovered(false),
	m_Opening(nullptr)
{
}

logic::components::CMainMenuGUI::~CMainMenuGUI()
{
}

void logic::components::CMainMenuGUI::Start()
{
	m_Opening = m_Parent->GetComponent<CMainMenuOpening>();
}

void logic::components::CMainMenuGUI::Update(float dt)
{
	Draw();
}

void logic::components::CMainMenuGUI::Draw()
{
	m_ImguiHelper.DisableWindowBackGroundColor();

	ResetHovered();
	DrawBackground();
	//DrawTitle();
	DrawButtonsHolder();
	DrawButtons();
	if (DrawOptions)
	{
		DrawOptionsWindow();
	}

	LastHovered();
}

void logic::components::CMainMenuGUI::ResetHovered()
{
	m_IsPlayButtonHovered = false;
	m_IsCreditsButtonHovered = false;
	m_IsBackButtonHovered = false;
	m_IsExitButtonHovered = false;
	m_IsGamepadButtonHovered = false;
	m_IsKeyboardButtonHovered = false;
	m_IsOptionsButtonHovered = false;
}

void logic::components::CMainMenuGUI::LastHovered()
{
	m_WasPlayButtonHovered = m_IsPlayButtonHovered;
	m_WasCreditsButtonHovered = m_IsCreditsButtonHovered;
	m_WasBackButtonHovered = m_IsBackButtonHovered;
	m_WasExitButtonHovered = m_IsExitButtonHovered;
	m_WasGamepadButtonHovered = m_IsGamepadButtonHovered;
	m_WasKeyBoardButtonHovered = m_IsKeyboardButtonHovered;
	m_WasOptionsButtonHovered = m_IsOptionsButtonHovered;
}

void logic::components::CMainMenuGUI::DrawBackground() const
{
	m_ImguiHelper.FullScreenWindow("FullScreenBackgroundMainMenu");
	m_ImguiHelper.FullScreenImage("inicio_NEW.jpg");
	m_ImguiHelper.EndWindow();
}

void logic::components::CMainMenuGUI::DrawTitle() const
{
	m_ImguiHelper.SetNextWindowPos(84, 43);
	m_ImguiHelper.NewWindowWithFlags("Title", IMGUI_DisableWindowInteractionFlag);
	m_ImguiHelper.Image("titulo.png", 504, 175);
	m_ImguiHelper.EndWindow();
}

void logic::components::CMainMenuGUI::DrawButtonsHolder() const
{
	m_ImguiHelper.SetNextWindowPos(689, -20);
	m_ImguiHelper.NewWindowWithFlags("ButtonsHolder", IMGUI_DisableWindowInteractionFlag);
	m_ImguiHelper.Image("INICIO_PERGAMINO.png", 607, 752);
	m_ImguiHelper.EndWindow();
}

void logic::components::CMainMenuGUI::DrawButtons()
{
	m_ImguiHelper.SetNextWindowPos(800, 155);
	//disable or enable button interactions
	if (m_MainButtonsActive)
	{
		m_ImguiHelper.NewWindowWithFlags("Buttons", IMGUI_DisableWindowModificationFlag);
	}	
	else
	{
		m_ImguiHelper.NewWindowWithFlags("Buttons", IMGUI_DisableWindowInteractionFlag);
	}
	//play button
	if (m_ImguiHelper.ImageButton(ButtonPlay, buttonsWidth, buttonsHeight))
	{
		if (m_MainButtonsActive)
		{
			m_Opening->SetEnabled(true);
			this->SetEnabled(false);
		}
	}
	if (m_ImguiHelper.IsItemHovered())
	{
		m_IsPlayButtonHovered = true;
		if (m_IsPlayButtonHovered && !m_WasPlayButtonHovered)
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
		}

		ButtonPlay = HoveredButtonPlay;
	}
	else
	{
		ButtonPlay = ButtonPlayOrig;
	}
	//credits button
	/*m_ImguiHelper.ImageButton(ButtonCredits, buttonsWidth, buttonsHeight);
	if (m_ImguiHelper.IsItemHovered())
	{
		m_IsCreditsButtonHovered = true;
		if (m_IsCreditsButtonHovered && !m_WasCreditsButtonHovered)
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
		}
		ButtonCredits = HoveredButtonCredits;
	}
	else{
		ButtonCredits = ButtonCreditsOrig;
	}*/
	//options button
	if (m_ImguiHelper.ImageButton(ButtonOptions, buttonsWidth, buttonsHeight))
	{
		DrawOptions = true;
	}
	if (m_ImguiHelper.IsItemHovered())
	{
		m_IsOptionsButtonHovered = true;
		if (m_IsOptionsButtonHovered && !m_WasOptionsButtonHovered)
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
		}
		ButtonOptions = HoveredButtonOptions;
	}
	else{
		ButtonOptions = ButtonOptionsOrig;
	}
	//exit button
	if (m_ImguiHelper.ImageButton(ButtonExit, buttonsWidth, buttonsHeight))
	{
		m_Engine.m_ExitGame = true;
	}
	if (m_ImguiHelper.IsItemHovered())
	{
		m_IsExitButtonHovered = true;
		if (m_IsExitButtonHovered && !m_WasExitButtonHovered)
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
		}
		ButtonExit = HoveredButtonExit;
	}
	else{
		ButtonExit = ButtonExitOrig;
	}

	m_ImguiHelper.EndWindow();
}

void logic::components::CMainMenuGUI::DrawOptionsWindow()
{
	m_ImguiHelper.SetNextWindowPos(186, 53);
	m_ImguiHelper.NewWindowWithFlags("Options", IMGUI_DisableWindowModificationFlag);
	m_MainButtonsActive = false;

	if (DrawKeyboadOptions)
	{
		m_ImguiHelper.Image("teclado.jpg", 900, 500);
		if (m_ImguiHelper.ImageButton(ButtonBack, buttonsWidth, buttonsHeight))
		{
			m_MainButtonsActive = true;
			DrawOptions = false;
		}
	}
	else
	{
		m_ImguiHelper.Image("gamepad.jpg", 900, 500);
		if (m_ImguiHelper.ImageButton(ButtonBack, buttonsWidth, buttonsHeight))
		{
			m_MainButtonsActive = true;
			DrawOptions = false;
		}
	}
	//back button
	if (m_ImguiHelper.IsItemHovered())
	{
		m_IsBackButtonHovered = true;
		if (m_IsBackButtonHovered && !m_WasBackButtonHovered)
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
		}
		ButtonBack = HoveredButtonBack;
	}
	else{
		ButtonBack = ButtonBackOrig;
	}
	m_ImguiHelper.SetInline(525);
	//draw button for changing keyborad/gamepad image
	//gamepad
	if (DrawKeyboadOptions)
	{
		if (m_ImguiHelper.ImageButton(ButtonGamepad, buttonsWidth, buttonsHeight))
		{
			DrawKeyboadOptions = false;
		}
		if (m_ImguiHelper.IsItemHovered())
		{
			m_IsGamepadButtonHovered = true;
			if (m_IsGamepadButtonHovered && !m_WasGamepadButtonHovered)
			{
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
			}
			ButtonGamepad = HoveredButtonGamepad;
		}
		else{
			ButtonGamepad = ButtonGamepadOrig;
		}
	}
	else
	{
		//keyboard
		if (m_ImguiHelper.ImageButton(ButtonKeyboard, buttonsWidth, buttonsHeight))
		{
			DrawKeyboadOptions = true;
		}
		if (m_ImguiHelper.IsItemHovered())
		{
			m_IsKeyboardButtonHovered = true;
			if (m_IsKeyboardButtonHovered && !m_WasKeyBoardButtonHovered)
			{
				m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON);
			}
			ButtonKeyboard = HoveredButtonKeyboard;
		}
		else{
			ButtonKeyboard = ButtonKeyboardOrig;
		}
	}

	m_ImguiHelper.EndWindow();
}