#ifndef _LOGICA_MAINMENUGUI_H
#define _LOGICA_MAINMENUGUI_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "MainMenuOpening.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CMainMenuGUI : public CScriptComponent
		{
		public:
			CMainMenuGUI();

			CMainMenuGUI(const std::string& name, const std::string& parent);

			virtual ~CMainMenuGUI();

			void Start() override;
			void Update(float dt) override;
			void Draw();
			void ResetHovered();
			void LastHovered();
			void DrawBackground() const;
			void DrawTitle() const;
			void DrawButtonsHolder() const;
			void DrawButtons();
			void DrawOptionsWindow();

		private:
			CMainMenuOpening* m_Opening;
			//BUTTONS
			unsigned short int buttonsWidth = 375;
			unsigned short int buttonsHeight = 95;
			//playbutton
			const std::string ButtonPlayOrig = "botonJugarCafe.png";
			std::string ButtonPlay = "botonJugarCafe.png";
			const std::string HoveredButtonPlay = "botonJugarRojo.png";

			//credits
			const std::string ButtonCreditsOrig = "botonCreditosCafe.png";
			std::string ButtonCredits = "botonCreditosCafe.png";
			const std::string HoveredButtonCredits = "botonCreditosRojo.png";

			//options
			const std::string ButtonOptionsOrig = "botonOpcionesCafe.png";
			std::string ButtonOptions = "botonOpcionesCafe.png";
			const std::string HoveredButtonOptions = "botonOpcionesRojo.png";

			//exit
			const std::string ButtonExitOrig = "botonSalirCafe.png";
			std::string ButtonExit = "botonSalirCafe.png";
			const std::string HoveredButtonExit = "botonSalirRojo.png";

			//back
			const std::string ButtonBackOrig = "regresar_marron.png";
			std::string ButtonBack = "regresar_marron.png";
			const std::string HoveredButtonBack = "regresar_rojo.png";

			//keyboard
			const std::string ButtonKeyboardOrig = "teclado_marron.png";
			std::string ButtonKeyboard = "teclado_marron.png";
			const std::string HoveredButtonKeyboard = "teclado_rojo.png";

			//gamepad
			const std::string ButtonGamepadOrig = "mando_marron.png";
			std::string ButtonGamepad = "mando_marron.png";
			const std::string HoveredButtonGamepad = "mando_rojo.png";

			bool m_MainButtonsActive = true;

			bool DrawOptions = false;
			bool DrawKeyboadOptions = false;

			CImguiHelper& m_ImguiHelper;
			bool m_IsPlayButtonHovered;
			bool m_WasPlayButtonHovered;
			bool m_IsCreditsButtonHovered;
			bool m_WasCreditsButtonHovered;
			bool m_IsOptionsButtonHovered;
			bool m_WasOptionsButtonHovered;
			bool m_IsExitButtonHovered;
			bool m_WasExitButtonHovered;
			bool m_IsBackButtonHovered;
			bool m_WasBackButtonHovered;
			bool m_IsGamepadButtonHovered;
			bool m_WasGamepadButtonHovered;
			bool m_IsKeyboardButtonHovered;
			bool m_WasKeyBoardButtonHovered;
		};
	}
}

#endif _LOGICA_MAINMENUGUI_H