#include "Noise.h"
#include "Render/RenderPipelineManager.h"
#include "Input/ActionManager.h"

namespace logic
{
	namespace components
	{
		CNoise::CNoise()
			: CScriptComponent(), m_NoiseActive(false),
			  m_NoiseCMD(nullptr),
			  m_NoiseEnabledCMD(nullptr),
			  m_NoiseDisableCMD(nullptr)
		{
		}

		CNoise::CNoise(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_NoiseActive(false),
			m_NoiseCMD(nullptr),
			m_NoiseEnabledCMD(nullptr),
			m_NoiseDisableCMD(nullptr)
		{
		}

		CNoise::~CNoise()
		{
		}

		void CNoise::Start()
		{
			for (render::CRenderCmd* cmd : m_Engine.GetRenderPipelineManager().GetActivePipeline().GetResourcesVector())
			{
				if (cmd->GetName() == "Noise Alpha Blend")
				{
					m_NoiseEnabledCMD = cmd;
				}
				if (cmd->GetName() == "Noise")
				{
					m_NoiseCMD = cmd;
				}
				if (cmd->GetName() == "Disable noise Alpha Blend")
				{
					m_NoiseDisableCMD = cmd;
				}
			}
		}

		void CNoise::Update(float dt)
		{
			if (!m_NoiseActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionstart"))
			{
				NoiseState(true);
			}
			else if (m_NoiseActive && m_Engine.GetActionManager().IsInputActionActive("Perceptionend"))
			{
				NoiseState(false);
			}
		}

		void CNoise::NoiseState(bool active)
		{
			m_NoiseActive = active;
			m_NoiseEnabledCMD->SetActive(active);
			m_NoiseCMD->SetActive(active);
			m_NoiseDisableCMD->SetActive(active);
		}
	}
}
