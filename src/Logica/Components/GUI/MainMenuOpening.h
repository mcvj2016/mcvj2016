#ifndef _LOGICA_MAINMENUOPENING_H
#define _LOGICA_MAINMENUOPENING_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "Math/Color.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CMainMenuOpening : public CScriptComponent
		{
		public:
			CMainMenuOpening();

			CMainMenuOpening(const std::string& name, const std::string& parent);

			virtual ~CMainMenuOpening();

			void Start() override;
			void Update(float dt) override;
			void OnDestroy() override;
			void OnEnable() override;
			void DrawBackgroundFull();
			void Draw();
			bool FadeInBlack();
			bool FadeOut();
			void DrawNextImg();
			void DrawBackground() const;
			void DrawChangingScene() const;
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			size_t m_CurrentImgIndex;
			CImguiHelper& m_ImguiHelper;
			bool m_DrawChangingScene, m_NextImg, m_CanFade, m_FirstDraw;
			std::string m_SceneToChange, m_ImgExtension;
			std::vector<std::string> m_Images;
			float m_TimeBetweenImg;
			CColor m_TintColor;
			std::string m_NextFade;
		};
	}
}

#endif _LOGICA_MAINMENUGUI_H