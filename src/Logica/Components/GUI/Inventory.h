#ifndef _LOGICA_INVENTORY_H
#define _LOGICA_INVENTORY_H
#include "Core/ScriptComponent.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CInventory : public CScriptComponent
		{
		public:
			CInventory();
			CInventory(const std::string& name, const std::string& parent);
			virtual ~CInventory();

			void Start() override;
			void Update(float dt) override;

			CImguiHelper& m_ImguiHelper;

			float m_inventoryPosX, m_inventoryPosY, m_inventorySizeX, m_inventorySizeY;
			float m_helmPosX, m_helmPosY, m_helmSizeX, m_helmSizeY;
			float m_keyPosX, m_keyPosY, m_keySizeX, m_keySizeY;
			float m_bucketPosX, m_bucketPosY, m_bucketSizeX, m_bucketSizeY;
			float m_chestPosX, m_chestPosY, m_chestSizeX, m_chestSizeY;
			float m_ropePosX, m_ropePosY, m_ropeSizeX, m_ropeSizeY;
			float m_bootsPosX, m_bootsPosY, m_bootsSizeX, m_bootsSizeY;
		private:
			void DrawInventory();			
		};
	}
}

#endif