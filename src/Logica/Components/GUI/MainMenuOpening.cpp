#include "MainMenuOpening.h"
#include "Engine.h"
#include "Sound/ISoundManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Render/RenderPipelineManager.h"
#include "Wwise_IDs.h"
#include "Utils/Coroutine.h"
#include "Utils/StringUtils.h"
#include "Input/ActionManager.h"

namespace logic
{
	namespace components
	{
		CMainMenuOpening::CMainMenuOpening()
			:
			CScriptComponent(), m_CurrentImgIndex(0),
			m_ImguiHelper(m_Engine.GetImGUI()),
			m_DrawChangingScene(false),
			m_NextImg(false),
			m_SceneToChange(""),
			m_ImgExtension(".jpg"), m_TimeBetweenImg(0),
			m_TintColor(CColor(1, 1, 1, 0)), m_CanFade(false), m_FirstDraw(true)
		{
		}

		CMainMenuOpening::CMainMenuOpening(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent), m_CurrentImgIndex(0),
			m_ImguiHelper(m_Engine.GetImGUI()),
			m_DrawChangingScene(false),
			m_NextImg(false),
			m_SceneToChange(""),
			m_ImgExtension(".jpg"), m_TimeBetweenImg(0), m_TintColor(CColor(1, 1, 1, 0)), m_CanFade(false), m_FirstDraw(true)
		{
		}

		CMainMenuOpening::~CMainMenuOpening()
		{
		}

		void CMainMenuOpening::Start()
		{
			m_Enabled = false;
			m_NextFade = "in";
		}

		void CMainMenuOpening::Update(float dt)
		{
			if (m_Engine.GetActionManager().IsInputActionActive("Attack"))
			{
				m_DrawChangingScene = true;
			}
			Draw();
		}

		void CMainMenuOpening::OnDestroy()
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::STOP_OPENING);
			m_Engine.GetSoundManager().Clean();
		}

		void CMainMenuOpening::OnEnable()
		{
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::STOP_MAINMENU);
			m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_OPENING);
		}


		void CMainMenuOpening::Draw()
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			if (m_DrawChangingScene)
			{
				DrawChangingScene();
			}
			else
			{
				if (m_FirstDraw)
				{
					DrawBackgroundFull();
				}
				else
				{
					DrawBackground();
					if (m_CurrentImgIndex < m_Images.size())
					{
						DrawNextImg();
					}
					else
					{
						m_DrawChangingScene = true;
					}
				}
			}
		}

		bool CMainMenuOpening::FadeInBlack()
		{	
			m_TintColor.w = m_TintColor.w + m_Engine.GetDeltaTime();
			if (m_TintColor.w >= 1)
			{
				m_NextFade = "out";
				m_TintColor.w = 1;
				return true;
			}
			return false;
		}

		bool CMainMenuOpening::FadeOut()
		{
			m_TintColor.w = m_TintColor.w - m_Engine.GetDeltaTime();
			if (m_TintColor.w <= 0)
			{
				m_NextFade = "in";
				m_TintColor.w = 0;
				return true;
			}
			return false;
		}

		void CMainMenuOpening::DrawNextImg()
		{
			m_ImguiHelper.FullScreenWindow("Ilustration");
			m_ImguiHelper.FullScreenImage(m_Images[m_CurrentImgIndex]);

			if (m_CanFade)
			{
				if (m_NextFade == "in")
				{
					if (FadeInBlack())
					{
						m_CurrentImgIndex++;
					}
				}
				else
				{
					if (FadeOut())
					{
						m_CanFade = false;
						base::utils::CCoroutine::GetInstance().StartCoroutine(m_TimeBetweenImg, [this]()
						{
							m_CanFade = true;
						});
					}
				}
			}

			m_ImguiHelper.EndWindow();
		}

		bool drawFirst = false;
		void CMainMenuOpening::DrawBackgroundFull()
		{
			m_ImguiHelper.FullScreenWindow("FullScreenBackgroundOpening");
			m_ImguiHelper.FullScreenImageRGBA("Black.dds", CColor(1,1,1,1));
			m_ImguiHelper.EndWindow();

			if (!drawFirst)
			{
				drawFirst = true;
				base::utils::CCoroutine::GetInstance().StartCoroutine(
					1,
					[this]()
					{
						m_FirstDraw = false;
					}
				);
				base::utils::CCoroutine::GetInstance().StartCoroutine(
					m_TimeBetweenImg,
					[this]()
					{
						m_CanFade = true;
					}
				);
			}
		}

		void CMainMenuOpening::DrawBackground() const
		{
			m_ImguiHelper.FullScreenWindow("FullScreenBackgroundOpening");
			m_ImguiHelper.FullScreenImageRGBA("Black.dds", m_TintColor);
			m_ImguiHelper.EndWindow();
		}

		bool changingDraw = false;
		void CMainMenuOpening::DrawChangingScene() const
		{
			m_ImguiHelper.FullScreenWindow("FullScreenLoading");
			m_ImguiHelper.FullScreenImage("cargando_img_txt.jpg");
			m_ImguiHelper.EndWindow();

			if (!changingDraw)
			{
				changingDraw = true;
				const std::string& sceneName = m_SceneToChange;
				base::utils::CCoroutine::GetInstance().StartCoroutine(0.5f, [sceneName]()
				{
					CEngine::GetInstance().GetSceneManager().ChangeScene(sceneName);
					CEngine::GetInstance().GetRenderPipelineManager().SetActivePipeline("pbr_deferred");
				});
			}
		}

		void CMainMenuOpening::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_SceneToChange = membersValues[0];
			m_Images = base::utils::Split(membersValues[1], ';');
			m_ImgExtension = membersValues[2];

			//add extension to images
			for (size_t i = 0; i < m_Images.size(); i++)
			{
				m_Images[i] += m_ImgExtension;
			}
			m_TimeBetweenImg = strtof(membersValues[3].c_str(), nullptr);
		}
	}
}
