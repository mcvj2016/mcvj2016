#ifndef _LOGICA_NOISE_H
#define _LOGICA_NOISE_H
#include "Core/ScriptComponent.h"
#include "Render/RenderCmd.h"

namespace logic
{
	namespace components
	{
		class CNoise : public CScriptComponent
		{
		public:
			CNoise();
			CNoise(const std::string& name, const std::string& parent);
			~CNoise();

			void Start() override;
			void Update(float dt) override;

			void NoiseState(bool active);
		private:
			render::CRenderCmd* m_NoiseCMD;
			render::CRenderCmd* m_NoiseEnabledCMD;
			render::CRenderCmd* m_NoiseDisableCMD;
			bool m_NoiseActive;
			
		};
	}
}
#endif