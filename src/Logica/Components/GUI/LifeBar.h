#ifndef _LOGICA_LIFEBAR_H
#define _LOGICA_LIFEBAR_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "Components/Common/Life.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CLifeBar : public CScriptComponent
		{
		public:
			CLifeBar();
			CLifeBar(const std::string& name, const std::string& parent);
			virtual ~CLifeBar();

			void Start() override;
			void Update(float dt) override;

			void InitMembersValues(std::vector<std::string> membersValues) override;
			GET_SET_PTR(CLife, PlayerLife)

			CImguiHelper& m_ImguiHelper;

			float m_windowPosX, m_windowPosY, m_windowSizeX, m_windowSizeY;
			float m_barPosX, m_barPosY, m_barSizeX, m_barSizeY;
			float m_barImageSizeX, m_barImageSizeY;
			float m_paddingX, m_paddingY;
			float m_minSizeX, m_minSizeY;
		private:
			bool m_Add;
			bool m_Substraction;
			float m_BarSpeed;
			float m_NewBarLength;
			float m_MaxLifePoints;
			float m_BarLength;
			float m_PreviousPoints;
			CLife* m_PlayerLife;			
			void UpdateBarValues(float dt);
			void DrawLifeBar() const;			
		};
	}
}

#endif