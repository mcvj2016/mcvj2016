#include "InGameMenuGameOver.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Input/ActionManager.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"
#include "Render/RenderPipelineManager.h"

namespace logic
{
	namespace components
	{
		CInGameMenuGameOver::CInGameMenuGameOver()
			:
			CScriptComponent(), m_PauseNode(nullptr),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_IsContinueButtonHovered(false), m_WasContinueButtonHovered(false), m_IsExitButtonHovered(false), m_WasExitButtonHovered(false), m_DrawChangingScene(false)
		{
		}

		CInGameMenuGameOver::CInGameMenuGameOver(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent), m_PauseNode(nullptr),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_IsContinueButtonHovered(false), m_WasContinueButtonHovered(false), m_IsExitButtonHovered(false), m_WasExitButtonHovered(false), m_DrawChangingScene(false)
		{
		}

		CInGameMenuGameOver::~CInGameMenuGameOver()
		{
		}

		void CInGameMenuGameOver::Start()
		{
			m_PauseNode = m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode(m_PauseMenuName);
			m_Parent->SetEnabled(false);
		}

		bool changingDrawGameOver = false;
		void CInGameMenuGameOver::DrawChangingScene() const
		{
			m_ImguiHelper.FullScreenWindow("FullScreenLoading");
			m_ImguiHelper.FullScreenImage("cargando_img_txt.jpg");
			m_ImguiHelper.EndWindow();

			if (!changingDrawGameOver)
			{
				changingDrawGameOver = true;
				const std::string& sceneName = m_SceneToChange;
				base::utils::CCoroutine::GetInstance().StartCoroutine(0.5f, [sceneName]()
				{
					CEngine::GetInstance().GetSceneManager().ChangeScene(sceneName);
					CEngine::GetInstance().GetRenderPipelineManager().SetActivePipeline("pbr_deferred");
				});
			}
		}

		void CInGameMenuGameOver::Update(float dt)
		{
			if (m_DrawChangingScene)
			{
				DrawChangingScene();
			}
			else
			{
				Draw();
			}
		}

		void CInGameMenuGameOver::OnEnable()
		{
			m_PauseNode->SetEnabled(false);
		}

		void CInGameMenuGameOver::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_ContinueImgName = membersValues[0];
			m_ContinueImgNameHover = membersValues[1];
			m_ExitImgName = membersValues[2];
			m_ExitImgNameHover = membersValues[3];
			m_BackgroundImgName = membersValues[4];
			m_PauseMenuName = membersValues[5];
			m_SceneToReload = membersValues[6];
			m_SceneToExit = membersValues[7];
			m_TitleImgName = membersValues[8];

			m_ContinueImgNameOrig = m_ContinueImgName;
			m_ExitImgNameOrig = m_ExitImgName;
		}

		void CInGameMenuGameOver::Draw()
		{
			ResetHovered();
			DrawTitle();
			DrawButtonsHolder();
			DrawButtons();
			LastHovered();
		}

		void CInGameMenuGameOver::DrawTitle() const
		{
			m_ImguiHelper.SetNextWindowPos(ImGui::GetIO().DisplaySize.x / 2 - 300, ImGui::GetIO().DisplaySize.y / 2 - 300);
			m_ImguiHelper.NewWindowWithFlags(std::string("InGameMenu_" + m_TitleImgName).c_str(), IMGUI_DisableWindowInteractionFlag);
			//m_ImguiHelper.NewWindow(std::string("InGameMenu_" + m_TitleImgName).c_str());
			m_ImguiHelper.Image(m_TitleImgName, ImGui::GetIO().DisplaySize.x / 2, 200);
			m_ImguiHelper.EndWindow();
		}

		void CInGameMenuGameOver::DrawButtonsHolder() const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(ImGui::GetIO().DisplaySize.x / 2 - 300, ImGui::GetIO().DisplaySize.y / 2 - 100);
			m_ImguiHelper.NewWindowWithFlags(std::string("InGameMenu_" + m_BackgroundImgName).c_str(), IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image(m_BackgroundImgName, ImGui::GetIO().DisplaySize.x / 2, ImGui::GetIO().DisplaySize.y / 2);
			m_ImguiHelper.EndWindow();
		}

		void CInGameMenuGameOver::ResetHovered()
		{
			m_IsContinueButtonHovered = false;
			m_IsExitButtonHovered = false;
		}

		void CInGameMenuGameOver::LastHovered()
		{
			m_WasContinueButtonHovered = m_IsContinueButtonHovered;
			m_WasExitButtonHovered = m_IsExitButtonHovered;
		}

		void CInGameMenuGameOver::DrawButtons()
		{
			m_ImguiHelper.SetNextWindowPos(ImGui::GetIO().DisplaySize.x / 2 - 162.5f, ImGui::GetIO().DisplaySize.y / 2 - 25);
			m_ImguiHelper.NewWindowWithFlags("ButtonsInGame", IMGUI_DisableWindowModificationFlag);

			//reload button
			if (m_ImguiHelper.ImageButton(m_ContinueImgName, buttonsWidth, buttonsHeight))
			{
				m_DrawChangingScene = true;
				m_SceneToChange = m_SceneToReload;
				//disable gui
				m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Vida")->SetEnabled(false);
				m_Engine.GetSceneManager().GetCurrentScene()->GetSceneNode("Inventario")->SetEnabled(false);
			}
			if (m_ImguiHelper.IsItemHovered())
			{
				m_IsContinueButtonHovered = true;
				if (!m_WasContinueButtonHovered)
				{
					m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON, "Default2DSpeaker");
				}

				m_ContinueImgName = m_ContinueImgNameHover;
			}
			else
			{
				m_ContinueImgName = m_ContinueImgNameOrig;
			}
			//exit button
			if (m_ImguiHelper.ImageButton(m_ExitImgName, buttonsWidth, buttonsHeight))
			{
				m_DrawChangingScene = true;
				m_SceneToChange = m_SceneToExit;
			}
			if (m_ImguiHelper.IsItemHovered())
			{
				m_IsExitButtonHovered = true;
				if (!m_WasExitButtonHovered)
				{
					m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON, "Default2DSpeaker");
				}

				m_ExitImgName = m_ExitImgNameHover;
			}
			else
			{
				m_ExitImgName = m_ExitImgNameOrig;
			}

			m_ImguiHelper.EndWindow();
		}
	}
}
