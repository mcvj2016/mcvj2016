#ifndef _LOGICA_GAMETEXTBOX_H
#define _LOGICA_GAMETEXTBOX_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		struct MsgInfo
		{
			std::string message = "";
			int lines = 1;
			float firstLineSize;

			MsgInfo(std::string l_message, int l_lines, float l_firstLineSize)
			{
				message = l_message;
				lines = l_lines;
				firstLineSize = l_firstLineSize;
			}	
		};

		class CGameTextBox : public CScriptComponent
		{
		public:
			CGameTextBox();
			CGameTextBox(const std::string& name, const std::string& parent);
			virtual ~CGameTextBox();

			void InitMembersValues(std::vector<std::string> membersValues) override;

			void Start() override;
			void Update(float dt) override;

			void ShowControlledTextInformation(int index);
			void ShowTimedTextInformation(int index, float totalTimeDisplay);
			void HideControlledTextInformation();

			void ShowTimedTextInformation(int index);

			CImguiHelper& m_ImguiHelper;
		private:
			bool m_TimedMessage;
			float m_DefaultBoxDisplayTime;
			float m_BoxDisplayTime;
			float m_AcumTime;
			void DrawTextBox() const;
			void InitVector();
			void HideTimedTextInformation();
			std::vector<MsgInfo> m_Messages;
			MsgInfo* m_ActualMessage;
		};
	}
}

#endif 