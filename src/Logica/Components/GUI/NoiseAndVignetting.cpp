#include "NoiseAndVignetting.h"
#include "Render/RenderPipelineManager.h"
#include "Input/ActionManager.h"

namespace logic
{
	namespace components
	{
		CNoiseVignetting::CNoiseVignetting()
			: CScriptComponent(),
			  m_ImguiHelper(CEngine::GetInstance().GetImGUI()), m_NoiseVignettingCMD(nullptr),
			  m_NoiseVignettingActive(false),
			  m_ForCinematic(false),
			  m_VigneteImg("vignette2.png")
		{
		}

		CNoiseVignetting::CNoiseVignetting(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()), m_NoiseVignettingCMD(nullptr),
			m_NoiseVignettingActive(false),
			m_ForCinematic(false),
			m_VigneteImg("vignette2.png")
		{
		}

		CNoiseVignetting::~CNoiseVignetting()
		{
		}

		void CNoiseVignetting::Start()
		{
			/*for (render::CRenderCmd* cmd : m_Engine.GetRenderPipelineManager().GetActivePipeline().GetResourcesVector())
			{
				if (cmd->GetName() == "noiseandvignetting")
				{
					m_NoiseVignettingCMD = cmd;
					break;
				}
			}*/
		}

		void CNoiseVignetting::SetVigneteImg(const std::string& img)
		{
			this->m_VigneteImg = img;
		}

		void CNoiseVignetting::Update(float dt)
		{
			if (m_ForCinematic)
			{
				DrawNoiseAndVignetting();
			}
			else
			{
				m_Engine.GetActionManager().IsInputActionActive("Perception") ?
					DrawNoiseAndVignetting() :
					DrawNoiseAndVignettingOffScreen();
				/*if (m_Engine.GetActionManager().IsInputActionActive("Perception"))
				{
					m_NoiseVignettingActive = true;
					//m_NoiseVignettingCMD->SetActive(true);
				}
				else if (m_NoiseVignettingActive && !m_Engine.GetActionManager().IsInputActionActive("Perception"))
				{
					//m_NoiseVignettingCMD->SetActive(false);
					m_NoiseVignettingActive = false;
				}
				if (m_NoiseVignettingActive)
				{
					DrawNoiseAndVignetting();
				} else
				{
					DrawNoiseAndVignettingOffScreen();
				}*/
			}
		}

		void CNoiseVignetting::DrawVignetting(const std::string& img) const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(-20.0f,-20.0f);
			m_ImguiHelper.SetNextWindowSize(ImGui::GetIO().DisplaySize.x + 25.0f, ImGui::GetIO().DisplaySize.y + 25);
			m_ImguiHelper.NewWindowWithFlags("Vignetting", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image(img, ImGui::GetIO().DisplaySize.x + 25, ImGui::GetIO().DisplaySize.y + 25);
			m_ImguiHelper.EndWindow();
		}

		void CNoiseVignetting::DrawNoiseAndVignetting()
		{
			/*m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(0.0f, 0.0f);
			m_ImguiHelper.NewWindowWithFlags("Noise", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image("NoiseAndGrain.png", ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y);
			m_ImguiHelper.EndWindow();*/
			
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(0.0f - 20.0f,0.0f - 20.0f );
			m_ImguiHelper.SetNextWindowSize(ImGui::GetIO().DisplaySize.x + 50.0f, ImGui::GetIO().DisplaySize.y + 50.0f);
			m_ImguiHelper.NewWindowWithFlags("Vignetting", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image(m_VigneteImg, ImGui::GetIO().DisplaySize.x + 50.0f, ImGui::GetIO().DisplaySize.y + 50.0f);
			m_ImguiHelper.EndWindow();
		}

		void CNoiseVignetting::DrawNoiseAndVignettingOffScreen()
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(0.0f - 20.0f, ImGui::GetIO().DisplaySize.y+100.0f);
			m_ImguiHelper.SetNextWindowSize(150.0f, 150.0f);
			m_ImguiHelper.NewWindowWithFlags("Vignetting", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image(m_VigneteImg, 100.0f, 100.0f);
			m_ImguiHelper.EndWindow();
		}
	}
}
