#ifndef _LOGICA_ENDING_H
#define _LOGICA_ENDING_H
#include "Core/ScriptComponent.h"
#include "Helper/ImguiHelper.h"
#include "Math/Color.h"
#include "ImgFader.h"

using namespace engine;

namespace logic
{
	namespace components
	{
		class CEnding : public CScriptComponent
		{
		public:
			CEnding();

			CEnding(const std::string& name, const std::string& parent);

			virtual ~CEnding(){};

			void Start() override;
			void Update(float dt) override;
			void DrawEnding() const;
			void OnEnable() override;
			void InitMembersValues(std::vector<std::string> membersValues) override;

		private:
			std::string m_ImgName, m_FadeImgname;
			bool m_FadedIn, m_FadedOut;
			CImgFader* m_ImgFader;
			CImguiHelper& m_ImguiHelper;
		};
	}
}

#endif _LOGICA_MAINMENUGUI_H