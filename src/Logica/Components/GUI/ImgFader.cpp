#include "ImgFader.h"
#include "Engine.h"


namespace logic
{
	namespace components
	{
		CImgFader::CImgFader()
			: CScriptComponent(), m_ImguiHelper(m_Engine.GetImGUI()), m_FadeStatus(none), m_Alpha(0)
		{
		}

		CImgFader::CImgFader(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent), m_ImguiHelper(m_Engine.GetImGUI()), m_FadeStatus(none), m_Alpha(0)
		{
		}


		bool CImgFader::FadeIn(const std::string& imgName)
		{
			if (m_Alpha >= 1)
			{
				m_FadeStatus = none;
				m_Alpha = 1;
				return true;
			}
	
			m_FadeStatus = in;
			Draw(imgName);
			m_Alpha += m_Engine.GetDeltaTime();
			return false;
		}

		bool CImgFader::FadeOut(const std::string& imgName)
		{
			if (m_Alpha <= 0)
			{
				m_Alpha = 0;
				m_FadeStatus = none;
				return true;
			}
			m_FadeStatus = out;
			Draw(imgName);
			m_Alpha -= m_Engine.GetDeltaTime();
			return false;
		}

		void CImgFader::Draw(const std::string& imgName) const
		{
			m_ImguiHelper.FullScreenWindow("ImgFadeIn_" + imgName);
			m_ImguiHelper.FullScreenImageRGBA(imgName, CColor(1, 1, 1, m_Alpha));
			m_ImguiHelper.EndWindow();
		}
	}
}
