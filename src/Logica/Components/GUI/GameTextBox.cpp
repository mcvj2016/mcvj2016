#include "GameTextBox.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Utils/Coroutine.h"

namespace logic
{
	namespace components
	{
		CGameTextBox::CGameTextBox()
			: CScriptComponent(),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_TimedMessage(false),
			m_BoxDisplayTime(0.0f),
			m_ActualMessage(nullptr)
		{
			InitVector();
			m_Enabled = false;
		}

		CGameTextBox::CGameTextBox(const std::string& name, const std::string& parent)
			: CScriptComponent(name, parent),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_TimedMessage(false),
			m_BoxDisplayTime(0.0f),
			m_ActualMessage(nullptr)
		{
			InitVector();
			m_Enabled = false;
		}

		CGameTextBox::~CGameTextBox()
		{
		}

		void CGameTextBox::Start()
		{
			//m_AcumTime = 0.0f;
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				2.0f,
				[this]()
				{
					ShowTimedTextInformation(13,5.0f);
				}
			);
		}

		void CGameTextBox::Update(float dt)
		{
			DrawTextBox();
			if (m_TimedMessage)
			{
				if (m_AcumTime > m_BoxDisplayTime)
				{
					HideTimedTextInformation();
				}
				m_AcumTime += dt;
			}
		}

		void CGameTextBox::ShowTimedTextInformation(int index)
		{
			m_BoxDisplayTime = m_DefaultBoxDisplayTime;
			m_ActualMessage = &m_Messages[index];
			m_AcumTime = 0.0f;
			m_TimedMessage = true;
			m_Enabled = true;
		}

		void CGameTextBox::ShowTimedTextInformation(int index, float totalTimeDisplay)
		{
			m_BoxDisplayTime = totalTimeDisplay;
			m_ActualMessage = &m_Messages[index];
			m_AcumTime = 0.0f;
			m_TimedMessage = true;
			m_Enabled = true;
		}

		void CGameTextBox::HideTimedTextInformation()
		{
			m_Enabled = false;
			m_TimedMessage = false;
			m_ActualMessage = nullptr;
		}

		void CGameTextBox::ShowControlledTextInformation(int index)
		{
			m_ActualMessage = &m_Messages[index];
			m_Enabled = true;
		}

		void CGameTextBox::HideControlledTextInformation()
		{
			m_Enabled = false;
			m_ActualMessage = nullptr;
		}

		void CGameTextBox::DrawTextBox() const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(140.5f, 520.5f);
			m_ImguiHelper.NewWindowWithFlags("TextBoxImage", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image("Pergamino-spotlights-base.dds", 999.0f, 169.0f);			
			m_ImguiHelper.EndWindow();

			m_ImguiHelper.SetNextWindowPos(200.5f, 555.5f);
			m_ImguiHelper.SetNextWindowSize(904.0f, 119.0f);
			m_ImguiHelper.NewWindowWithFlags("TextBoxMessage", IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.SetMainFontColor();
			m_ImguiHelper.PushFont();
			m_ImguiHelper.SetAlign(904.0f, 119.0f, m_ActualMessage->firstLineSize, 26.0f, static_cast<float>(m_ActualMessage->lines));
			m_ImguiHelper.AddTextLine(m_ActualMessage->message);
			m_ImguiHelper.PopFont();
			m_ImguiHelper.SetDefaultFontColor();
			m_ImguiHelper.EndWindow();
		}

		void CGameTextBox::InitVector()
		{
			std::string path = "data/scenes/" + m_Engine.GetSceneManager().GetCurrentScene()->GetName() +"/text_box_msgs.xml";
			CXMLDocument xmldoc;
			bool loaded = base::xml::SucceedLoad(xmldoc.LoadFile(path.c_str()));

			CXMLElement* messages = xmldoc.FirstChildElement("messages");
			if (messages != nullptr)
			{
				CXMLElement* messageElement = messages->FirstChildElement("message");
				while (messageElement != nullptr)
				{
					std::string message = messageElement->GetAttribute<std::string>("text", "");
					MsgInfo structmessage = MsgInfo{
						message,
						messageElement->GetAttribute<int>("lines", 1),
						m_ImguiHelper.FirstLineSize(1,message)
					};
					m_Messages.push_back(structmessage);
					messageElement = messageElement->NextSiblingElement("message");
				}
			}
		}

		void CGameTextBox::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_DefaultBoxDisplayTime = strtof(membersValues[0].c_str(), nullptr);
		}
	}
}