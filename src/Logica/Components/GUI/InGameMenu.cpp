#include "InGameMenu.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Input/ActionManager.h"

namespace logic
{
	namespace components
	{
		CInGameMenu::CInGameMenu()
			:
			CScriptComponent(),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_Draw(false), m_IsContinueButtonHovered(false), m_WasContinueButtonHovered(false), m_IsExitButtonHovered(false), m_WasExitButtonHovered(false)
		{
		}

		CInGameMenu::CInGameMenu(const std::string& name, const std::string& parent)
			:
			CScriptComponent(name, parent),
			m_ImguiHelper(CEngine::GetInstance().GetImGUI()),
			m_Draw(false), m_IsContinueButtonHovered(false), m_WasContinueButtonHovered(false), m_IsExitButtonHovered(false), m_WasExitButtonHovered(false)
		{
		}

		CInGameMenu::~CInGameMenu()
		{
		}

		void CInGameMenu::Update(float dt)
		{
			if (CEngine::GetInstance().GetActionManager().IsInputActionActive("Pause"))
			{
				m_Draw = !m_Draw;
				m_Engine.SetPaused(m_Draw);
			}
			if (m_Draw)
			{
				Draw();
			}
		}

		void CInGameMenu::InitMembersValues(std::vector<std::string> membersValues)
		{
			m_ContinueImgName = membersValues[0];
			m_ContinueImgNameHover = membersValues[1];
			m_ExitImgName = membersValues[2];
			m_ExitImgNameHover = membersValues[3];
			m_BackgroundImgName = membersValues[4];

			m_ContinueImgNameOrig = m_ContinueImgName;
			m_ExitImgNameOrig = m_ExitImgName;
		}

		void CInGameMenu::Draw()
		{
			ResetHovered();
			DrawButtonsHolder();
			DrawButtons();
			LastHovered();
		}

		void CInGameMenu::DrawButtonsHolder() const
		{
			m_ImguiHelper.DisableWindowBackGroundColor();
			m_ImguiHelper.SetNextWindowPos(ImGui::GetIO().DisplaySize.x / 2 - 300, ImGui::GetIO().DisplaySize.y / 2 - 200);
			m_ImguiHelper.NewWindowWithFlags(std::string("InGameMenu_" + m_BackgroundImgName).c_str(), IMGUI_DisableWindowInteractionFlag);
			m_ImguiHelper.Image(m_BackgroundImgName, ImGui::GetIO().DisplaySize.x / 2, ImGui::GetIO().DisplaySize.y / 2);
			m_ImguiHelper.EndWindow();
		}

		void CInGameMenu::ResetHovered()
		{
			m_IsContinueButtonHovered = false;
			m_IsExitButtonHovered = false;
		}

		void CInGameMenu::LastHovered()
		{
			m_WasContinueButtonHovered = m_IsContinueButtonHovered;
			m_WasExitButtonHovered = m_IsExitButtonHovered;
		}

		void CInGameMenu::DrawButtons()
		{
			m_ImguiHelper.SetNextWindowPos(ImGui::GetIO().DisplaySize.x / 2 - 162.5f, ImGui::GetIO().DisplaySize.y / 2 - 125);
			m_ImguiHelper.NewWindowWithFlags("ButtonsInGame", IMGUI_DisableWindowModificationFlag);

			//play button
			if (m_ImguiHelper.ImageButton(m_ContinueImgName, buttonsWidth, buttonsHeight))
			{
				m_Draw = false;
				m_Engine.SetPaused(false);
			}
			if (m_ImguiHelper.IsItemHovered())
			{
				m_IsContinueButtonHovered = true;
				if (!m_WasContinueButtonHovered)
				{
					m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON, "Default2DSpeaker");
				}

				m_ContinueImgName = m_ContinueImgNameHover;
			}
			else
			{
				m_ContinueImgName = m_ContinueImgNameOrig;
			}
			//exit button
			if (m_ImguiHelper.ImageButton(m_ExitImgName, buttonsWidth, buttonsHeight))
			{
				m_Engine.m_ExitGame = true;
			}
			if (m_ImguiHelper.IsItemHovered())
			{
				m_IsExitButtonHovered = true;
				if (!m_WasExitButtonHovered)
				{
					m_Engine.GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HOVERBUTTON, "Default2DSpeaker");
				}

				m_ExitImgName = m_ExitImgNameHover;
			}
			else
			{
				m_ExitImgName = m_ExitImgNameOrig;
			}

			m_ImguiHelper.EndWindow();
		}
	}
}
