#include "ScriptComponentFactory.h"
#include "Components/Player/PlayerController.h"
#include <cassert>
#include "Components/Camera/ThirdPersonCamera.h"
#include "XML/XML.h"
#include "Components/Camera/FirstPersonCamera.h"
#include "Components/GUI/MainMenuGUI.h"
#include "Components/Weapon/PlayerWeapon.h"
#include "Components/Weapon/OrcWeapon.h"
#include "Components/Common/Life.h"
#include "Components/Enemies/Orc/OrcController.h"
#include "Components/Common/CinematicStarter.h"
#include "Components/Player/PlayerLocomotion.h"
#include "Components/Scenes/MainMenu/MainMenuController.h"
#include "Components/Player/PlayerAttackController.h"
#include "Components/Player/PlayerMagicAttackController.h"
#include "Components/Camera/AimingCamera.h"
#include "Components/Objects/Key.h"
#include "Components/Weapon/PlayerMagicProjectile.h"
#include "Components/Objects/Trampilla.h"
#include "Components/GUI/GameTextBox.h"
#include "Components/Enemies/Orc/OrcAnimator.h"
#include "Components/IA/Seeker.h"
#include "Components/Enemies/Orc/OrcAttackController.h"
#include "Components/Player/PlayerAnimator.h"
#include "Components/Enemies/Goblin/GoblinController.h"
#include "Components/Enemies/Goblin/GoblinAnimator.h"
#include "Components/Objects/CinematicCallbacks.h"
#include "Components/Objects/Bucket.h"
#include "Components/Objects/Chest.h"
#include "Components/Objects/Well.h"
#include "Components/Objects/Totem.h"
#include "Components/Objects/Boots.h"
#include "Components/Objects/Rock.h"
#include "Components/GUI/Inventory.h"
#include "Components/GUI/LifeBar.h"
#include "Components/GUI/EnemyLifeBar.h"
#include "Components/Objects/Wildvision.h"
#include "Components/GUI/NoiseAndVignetting.h"
#include "Components/Scenes/Zones123/Zones123SoundController.h"
#include "Components/Common/Speaker3D.h"
#include "Components/Common/AmbientSoundManager.h"
#include "Components/Enemies/EnemyNotifier.h"
#include "Components/Enemies/Gulak/GulakController.h"
#include "Components/Enemies/Gulak/GulakAttackController.h"
#include "Components/Enemies/Gulak/GulakAnimator.h"
#include "Components/Enemies/Gulak/GulakLocomotion.h"
#include "Components/Weapon/GulakMagicProjectile.h"
#include "Components/Enemies/EnemyGroupManager.h"
#include "Components/Enemies/EnemyTag.h"
#include "Components/GUI/DamageVignette.h"
#include "Components/Objects/Rope.h"
#include "Components/Enemies/Gulak/GulakActivator.h"
#include "Components/Enemies/OrcBossInstantiator.h"
#include "Components/GUI/Noise.h"
#include "Components/Enemies/Gulak/GulakCombatEnemies.h"
#include "Components/Particles/FireflyController.h"
#include "Components/GUI/MainMenuOpening.h"
#include "Components/GUI/ImgFader.h"
#include "Components/GUI/Ending.h"
#include "Components/GUI/InGameMenu.h"
#include "Components/GUI/InGameMenuGameOver.h"

namespace logic
{
	namespace components
	{
		CScriptComponentFactory::CScriptComponentFactory()
		{
		}

		CScriptComponentFactory::~CScriptComponentFactory()
		{
		}

		CScriptComponent* CScriptComponentFactory::CreateInstance(const std::string& className, const std::string& parent) const
		{
			if (className == "PlayerController")
			{
				return new CPlayerController(className, parent);
			}
			if (className == "ImgFader")
			{
				return new CImgFader(className, parent);
			}
			if (className == "ThirdPersonCamera")
			{
				return new CThirdPersonCameraComponent(className, parent);
			}
			if (className == "Ending")
			{
				return new CEnding(className, parent);
			}
			if (className == "InGameMenu")
			{
				return new CInGameMenu(className, parent);
			}
			if (className == "InGameMenuGameOver")
			{
				return new CInGameMenuGameOver(className, parent);
			}
			if (className == "GulakActivator")
			{
				return new CGulakActivator(className, parent);
			}
			if (className == "MainMenuOpening")
			{
				return new CMainMenuOpening(className, parent);
			}
			if (className == "OrcBossInstantiator")
			{
				return new COrcBossInstantiator(className, parent);
			}
			if (className == "GulakCombatEnemies")
			{
				return new CGulakCombatEnemies(className, parent);
			}
			if (className == "EnemyTag")
			{
				return new CEnemyTag(className, parent);
			}
			if (className == "FirstPersonCamera")
			{
				return new CFirstPersonCameraComponent(className, parent);
			}
			if (className == "DamageVignette")
			{
				return new CDamageVignetting(className, parent);
			}
			if (className == "Speaker3D")
			{
				return new CSpeaker3D(className, parent);
			}
			if (className == "GulakMagicProjectile")
			{
				return new CGulakMagicProjectile(className, parent);
			}
			if (className == "EnemyGroupManager")
			{
				return new CEnemyGroupManager(className, parent);
			}
			if (className == "AimingCamera")
			{
				return new CAimingCamera(className, parent);
			}
			if (className == "GulakController")
			{
				return new CGulakController(className, parent);
			}
			if (className == "GulakAttackController")
			{
				return new CGulakAttackController(className, parent);
			}
			if (className == "GulakAnimator")
			{
				return new CGulakAnimator(className, parent);
			}
			if (className == "GulakLocomotion")
			{
				return new CGulakLocomotion(className, parent);
			}
			if (className == "AmbientSoundManager")
			{
				return new CAmbientSoundManager(className, parent);
			}
			if (className == "MainMenuController")
			{
				return new CMainMenuController(className, parent);
			}
			if (className == "EnemiesManager")
			{
				return new CEnemiesManager(className, parent);
			}
			if (className == "Zones123SoundController")
			{
				return new CZones123SoundController(className, parent);
			}
			if (className == "OrcAttackController")
			{
				return new COrcAttackController(className, parent);
			}
			if (className == "PlayerAnimator")
			{
				return new CPlayerAnimator(className, parent);
			}
			if (className == "EnemyNotifier")
			{
				return new CEnemyNotifier(className, parent);
			}
			if (className == "PlayerLocomotion")
			{
				return new CPlayerLocomotion(className, parent);
			}
			if (className == "MainMenuGUI")
			{
				return new CMainMenuGUI(className, parent);
			}
			if (className == "PlayerWeapon")
			{
				return new CPlayerWeapon(className, parent);
			}
			if (className == "OrcWeapon")
			{
				return new COrcWeapon(className, parent);
			}
			if (className == "Life")
			{
				return new CLife(className, parent);
			}
			if (className == "Seeker")
			{
				return new CSeeker(className, parent);
			}
			if (className == "OrcController")
			{
				return new COrcController(className, parent);
			}
			if (className == "OrcAnimator")
			{
				return new COrcAnimator(className, parent);
			}
			if (className == "CinematicStarter")
			{
				return new CCinematicStarter(className, parent);
			}
			if (className == "PlayerAttackController")
			{
				return new CPlayerAttackController(className, parent);
			}
			if (className == "PlayerMagicAttackController")
			{
				return new CPlayerMagicAttackController(className, parent);
			}
			if (className == "PlayerMagicProjectile")
			{
				return new CPlayerMagicProjectile(className, parent);
			}
			if (className == "Key")
			{
				return new CKey(className, parent);
			}
			if (className == "Trampilla")
			{
				return new CTrampilla(className, parent);
			}
			if (className == "GameTextBox")
			{
				return new CGameTextBox(className, parent);
			}
			if (className == "Inventory")
			{
				return new CInventory(className, parent);
			}
			if (className == "LifeBar")
			{
				return new CLifeBar(className, parent);
			}
			if (className == "EnemyLifeBar")
			{
				return new CEnemyLifeBar(className, parent);
			}
			if (className == "CinematicCallbacks")
			{
				return new CCinematicCallback(className, parent);
			}
			if (className == "Bucket_Trigger")
			{
				return new CBucket(className, parent);
			}
			if (className == "Chest")
			{
				return new CChest(className, parent);
			}
			if (className == "Well_Trigger")
			{
				return new CWell(className, parent);
			}			
			if (className == "Totem_Trigger")
			{
				return new CTotem(className, parent);
			}
			if (className == "Boots")
			{
				return new CBoots(className, parent);
			}
			if (className == "Rock")
			{
				return new CRock(className, parent);
			}
			if (className == "GoblinController")
			{
				return new CGoblinController(className, parent);
			}
			if (className == "GoblinAnimator")
			{
				return new CGoblinAnimator(className, parent);
			}
			if (className == "GoblinMagicController")
			{
				return new CGoblinMagicController(className, parent);
			}
			if (className == "WildPerception")
			{
				return new CWildvision(className, parent);
			}
			if (className == "NoiseVignetting")
			{
				return new CNoiseVignetting(className, parent);
			}
			if (className == "Noise")
			{
				return new CNoise(className, parent);
			}
			if (className == "Rope_Trigger")
			{
				return new CRope(className, parent);
			}
			if (className == "FireFlyController")
			{
				return new CFireFly(className, parent);
			}

			assert(!"We should not reach this point");
			return nullptr;
		}
	}
}
