#ifndef _LOGICA_SCRIPTCOMPONENT_H
#define _LOGICA_SCRIPTCOMPONENT_H
#include "Utils/Name.h"
#include <vector>
#include "XML/XML.h"//No borrar este include. Usado en clases derivadas.
#include "Graphics/Scenes/SceneNode.h"
#include "Engine.h"
#include "Helper/ImguiHelper.h" //dejarlo para hacer logs desde scripts siempre que sea necesario

using namespace engine;
using namespace scenes;

namespace logic
{
	namespace components
	{
		class CScriptComponent : public CName
		{
		public:
			CScriptComponent();
			CScriptComponent(const std::string& name, const std::string& parent);
			CScriptComponent(const CXMLElement* aElement, const std::string& name);
			virtual ~CScriptComponent();

			virtual void Start();
			virtual void Update(float dt);
			virtual void OnDestroy();
			virtual void OnEnable();
			virtual void OnDisable();

			CSceneNode* Parent();

			void SetEnabled(bool enabled);
			bool GetEnabled() const;

			CSceneNode* GetParent(){ return m_Parent; }

			virtual void OnTriggerEnter(CSceneNode* other);
			virtual void OnTriggerStay(CSceneNode* other);
			virtual void OnTriggerExit(CSceneNode* other);

			void InitMembers(std::vector<std::string> membersValues);

			CEngine& m_Engine;
			
		protected:
			CSceneNode* m_Parent;
			bool m_Enabled;
			virtual void InitMembersValues(std::vector<std::string> membersValues);
			void Destroy(CSceneNode* node);
			void DestroyPhysxOnly(CSceneNode* node);
		};
	}
}

#endif