#include "ScriptComponent.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"

namespace logic
{
	namespace components
	{

		CScriptComponent::CScriptComponent() 
		: 
		m_Engine(CEngine::GetInstance()), 
		m_Parent(nullptr),
		m_Enabled(true)
		{
		}

		CScriptComponent::CScriptComponent(const std::string& name, const std::string& parent)
			: CName(name),
			m_Engine(CEngine::GetInstance()), 
			m_Enabled(true)
		{
			m_Parent = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(parent);
		}

		CScriptComponent::CScriptComponent(const tinyxml2::XMLElement* aElement, const std::string& parent)
			: CName(aElement),
			m_Engine(CEngine::GetInstance()),
			m_Enabled(true)
		{
			m_Parent = engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(parent);
		}

		CScriptComponent::~CScriptComponent()
		{
		}

		void CScriptComponent::Start()
		{
		}

		void CScriptComponent::Update(float dt)
		{

		}

		void CScriptComponent::OnDestroy()
		{
		}

		void CScriptComponent::OnEnable()
		{
		}

		void CScriptComponent::OnDisable()
		{
		}

		CSceneNode* CScriptComponent::Parent()
		{
			return m_Parent;
		}

		void CScriptComponent::SetEnabled(bool enabled)
		{
			m_Enabled = enabled;
			m_Enabled ? OnEnable() : OnDisable();
		}

		bool CScriptComponent::GetEnabled() const
		{
			return m_Enabled;
		}

		void CScriptComponent::OnTriggerEnter(engine::scenes::CSceneNode* other)
		{
			
		}
		
		void CScriptComponent::OnTriggerStay(engine::scenes::CSceneNode* other)
		{
			
		}
		
		void CScriptComponent::OnTriggerExit(engine::scenes::CSceneNode* other)
		{
			
		}

		void CScriptComponent::InitMembers(std::vector<std::string> membersValues)
		{
			if (membersValues.size()>0)
			{
				InitMembersValues(membersValues);
			}
		}

		void CScriptComponent::InitMembersValues(std::vector<std::string> membersValues)
		{

		}

		void CScriptComponent::Destroy(CSceneNode* node)
		{
			cal3dimpl::CSceneAnimatedModel* model = dynamic_cast<cal3dimpl::CSceneAnimatedModel*>(node);
			if (model)
			{
				CSceneNode* weapon = model->GetWeapon();
				if (weapon)
				{
					m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(model->GetWeapon());
					model->SetWeapon(nullptr);
				}
			}
			m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(node);
		}

		void CScriptComponent::DestroyPhysxOnly(CSceneNode* node)
		{
			node->m_OnlyDestroyPhysx = true;
			cal3dimpl::CSceneAnimatedModel* model = dynamic_cast<cal3dimpl::CSceneAnimatedModel*>(node);
			if (model)
			{
				model->GetWeapon()->m_OnlyDestroyPhysx = true;
				m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(model->GetWeapon());
			}
			m_Engine.GetSceneManager().GetCurrentScene()->DestroyObject(node);
		}
	}
}
