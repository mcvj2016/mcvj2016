#ifndef _LOGICA_SCRIPTCOMPONENTFACTORY_H
#define _LOGICA_SCRIPTCOMPONENTFACTORY_H
#include "ScriptComponent.h"
#include "Utils/Singleton.h"

namespace logic
{
	namespace components
	{

		class CScriptComponentFactory : public base::utils::CSingleton<CScriptComponentFactory>
		{
		public:
			CScriptComponentFactory();
			virtual ~CScriptComponentFactory();
			CScriptComponent* CreateInstance(const std::string& className, const std::string& name) const;
		};
	}
}
#endif
