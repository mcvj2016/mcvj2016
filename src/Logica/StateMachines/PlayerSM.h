#ifndef PLAYERSTATEMACHINE_H
#define PLAYERSTATEMACHINE_H

#include "AnimatedSM.h"

using namespace logic;
using namespace sm;

namespace logic
{
	namespace player
	{
		class CPlayerSM : public CAnimatedSM
		{
		public:
			~CPlayerSM();
			CPlayerSM();

			void entry() override;
			void exit() override;

			components::CModelAnimator* GetAnimator() override;

			void react(sm::Init const& e) override;
			void react(sm::Stop const& e) override;
			void react(sm::ForceMove const& e) override;
			void react(sm::Move const& e) override;
			void react(sm::Run const& e) override;
			void react(sm::Attack const& e) override;
			void react(sm::StrongAtack const & e) override;
			void react(sm::TakeDamage const & e) override;
			void react(sm::Dead const& e) override;
			void react(sm::Aiming const &) override; //not being used
			void react(sm::Evade const &) override; //not being used
			void react(sm::Magic1 const &) override; //not being used
			void react(sm::Magic2 const &) override; //not being used
		};

		class Idle : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Move : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Run : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Death : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Evade : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class TakeDamage : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Aiming : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Attack1 : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Attack2 : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Attack3 : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class StrongAttack : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class MagicProjectile : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class MagicArea : public CPlayerSM
		{
		public:
			void entry() override;
			void exit() override;
		};
	}
}

#endif
