#ifndef SMEVENTSWRAPPER_H
#define SMEVENTSWRAPPER_H

#include "StateMachine.h"
#include "Utils/Types.h"

namespace logic
{
	namespace sm
	{
		struct Init : Event { uint8 Weight = 0; };
		struct Move : Event { uint8 Weight = 1; };
		struct Run : Event { uint8 Weight = 2; };
		struct ForceMove : Event { uint8 Weight = 5; };
		struct Stop : Event { uint8 Weight = 3; };
		struct Dead : Event { uint8 Weight = 10; };
		struct Evade : Event { uint8 Weight = 4; };
		struct TakeDamage : Event { uint8 Weight = 4; };
		struct StrongAtack : Event { uint8 Weight = 3; };
		struct Attack : Event { uint8 Weight = 3; }; //used for switching from attack states the same event
		struct Magic1 : Event { uint8 Weight = 3; };
		struct Magic2 : Event { uint8 Weight = 3; };
		struct Aiming : Event { uint8 Weight = 1; };

		enum AnimIndex
		{
			NoneIndex = -1,
			IdleIndex,
			WalkIndex,
			Attack1Index,
			Attack2Index,
			Attack3Index,
			StrongAttackIndex,
			Damage1Index,
			Damage2Index,
			DeadIndex,
			Magic1Index,
			Magic2Index,
			RunIndex,
			EvadeIndex,
			AimingIndex
		};
	}
}

#endif