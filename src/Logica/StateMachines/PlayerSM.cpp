#include "PlayerSM.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Utils/Coroutine.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Player/PlayerAnimator.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Components/GUI/DamageVignette.h"

namespace logic
{
	namespace player
	{
		//
		// Base class implementation
		//

		CPlayerSM::~CPlayerSM()
		{
		}

		CPlayerSM::CPlayerSM() : CAnimatedSM()
		{
		}

		CModelAnimator* CPlayerSM::GetAnimator()
		{
			if (m_Animator == nullptr)
			{
				m_Animator = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(GetName())
					->GetComponent<CPlayerAnimator>();
			}
			return m_Animator;
		}

		void CPlayerSM::react(sm::Move const&)
		{
			transit("Move");
		}

		void CPlayerSM::react(sm::Run const& e)
		{
			transit("Run");
		}

		void CPlayerSM::react(sm::Attack const&)
		{
			transit("Attack1");
		}

		void CPlayerSM::react(sm::StrongAtack const&)
		{
			transit("StrongAttack");
		}

		void CPlayerSM::react(sm::Magic1 const&)
		{
			transit("MagicProjectile");
		}

		void CPlayerSM::react(sm::Magic2 const& e)
		{
			transit("MagicArea");
		}

		void CPlayerSM::react(sm::TakeDamage const& e)
		{
			int min = 0;
			int max = 100;
			int probability = mathUtils::RandomLToH(min, max);

			//play sound
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HIT_PLAYER, GetAnimator()->GetModel());

			if (probability <= 30)
			{
				transit("TakeDamage");
			}

			//show damage vignetting
			CDamageVignetting* vignetting = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("DamageVignette")
				->GetComponent<CDamageVignetting>();
			vignetting->DrawVignetting();
			float waitTime = 1;
			base::utils::CCoroutine::GetInstance().StartCoroutineParallel(
				waitTime,
				[vignetting, waitTime](float dt, float totalTime)
				{
					//lerp alpha for fading out the vignette
					CColor imgColor = vignetting->GetColor();
					float alpha = imgColor.w - dt;
					vignetting->SetColor(CColor(1, 1, 1, alpha));
					if (totalTime >= waitTime || alpha <= 0)
					{
						vignetting->SetColor(CColor(1, 1, 1, 1));
						vignetting->StopDrawVignetting();
					}
				}
			);
		}

		void CPlayerSM::react(sm::Stop const& e)
		{
			//check if its an stop for performig the run from moving
			CPlayerAnimator* lAnimator = static_cast<CPlayerAnimator*>(GetAnimator());
			if (lAnimator->GetGoingToRun())
			{
				GetAnimator()->SetActualWeight(sm::Init().Weight);
			}
			else
			{
				transit("Idle");
			}
		}

		void CPlayerSM::react(sm::ForceMove const& e)
		{
			transit("Move");
		}

		void CPlayerSM::react(sm::Dead const& e)
		{
			transit("Death");
		}

		void CPlayerSM::react(sm::Aiming const& e)
		{
			transit("Aiming");
		}

		void CPlayerSM::react(sm::Evade const& e)
		{
			transit("Evade");
		}

		void CPlayerSM::react(sm::Init const& e)
		{
			transit("Idle");
		}

		void CPlayerSM::entry()
		{
		}

		void CPlayerSM::exit()
		{
			
		}

		//
		// StrongAttack class implementation
		//
		void StrongAttack::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::StrongAttackIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::StrongAttackIndex, 0.2f, 0.05f);
			GetAnimator()->SetActualWeight(sm::StrongAtack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::StrongAttackIndex)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime,
				[this]() {
					transit("Idle");
				}
			);
		}

		void StrongAttack::exit()
		{
		}

		//
		// MagicProjectilerea class implementation
		//
		void MagicProjectile::entry()
		{
			//play sound
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_FIREIN_M1_PLAYER, GetAnimator()->GetModel());

			GetAnimator()->SetActualIndex(sm::AnimIndex::Magic1Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Magic1Index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Magic1().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Magic1Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime,
				[this]() {
					transit("Idle");
				}
			);
		}

		void MagicProjectile::exit()
		{
		}

		//
		// MagicArea class implementation
		//
		void MagicArea::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::Magic2Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Magic2Index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Magic2().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Magic2Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime,
				[this]() {
					transit("Idle");
				}
			);
		}

		void MagicArea::exit()
		{
		}

		//
		// Idle class implementation
		//
		void Idle::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::IdleIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::IdleIndex, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Init().Weight);
		}
		void  Idle::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		//
		// Move class implementation
		//
		void Move::entry()
		{
			CPlayerAnimator* lAnimator = static_cast<CPlayerAnimator*>(GetAnimator()); 
			if (lAnimator->GetGoingToRun())
			{
				lAnimator->SetGoingToRun(false);
				GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::WalkIndex, static_cast<float>(sm::Move().Weight), 0.5f);
			}
			else
			{
				GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::WalkIndex, static_cast<float>(sm::Move().Weight), 0.2f);
			}
			GetAnimator()->SetActualIndex(sm::AnimIndex::WalkIndex);
			GetAnimator()->SetActualWeight(sm::Move().Weight);
		}

		void Move::exit()
		{			
			CPlayerAnimator* lAnimator = static_cast<CPlayerAnimator*>(GetAnimator());
			if (lAnimator->GetGoingToRun())
			{
				GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .5f);
			}
			else
			{
				GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
			}
			
		}

		/*
		* Run Class Implementation
		*/
		void Run::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::RunIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::RunIndex, static_cast<float>(sm::Run().Weight), .5f);
			GetAnimator()->SetActualWeight(sm::Run().Weight);
		}

		void Run::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .5f);
		}

		/*
		* Death Class Implementation
		*/
		void Death::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::DeadIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::DeadIndex, 0.2f, 0.2f, 1, true);
			GetAnimator()->SetActualWeight(sm::Dead().Weight);
		}

		void Death::exit()
		{
		}

		/*
		* Evade Class Implementation
		*/
		void Evade::entry()
		{
			//cancel actual cycle and action
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .1f);
			GetAnimator()->GetModel()->RemoveAction(GetAnimator()->GetActualIndex());
			GetAnimator()->SetActualIndex(sm::AnimIndex::EvadeIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::EvadeIndex, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Evade().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::EvadeIndex)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - 0.2f,
				[this]() {
					transit("Idle");
				}
			);
		}

		void Evade::exit()
		{
		}

		/*
		* TakeDamage Class Implementation
		*/
		void TakeDamage::entry()
		{
			//clear other cycle and action
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), 0.2f);
			GetAnimator()->GetModel()->RemoveAction(GetAnimator()->GetActualIndex());
			//generate random index fort parforming a random animation
			int min = sm::AnimIndex::Damage1Index;
			int max = sm::AnimIndex::Damage2Index;
			sm::AnimIndex random_index = static_cast<sm::AnimIndex>(mathUtils::RandomLToH<int>(min, max));

			GetAnimator()->SetActualIndex(random_index);
			GetAnimator()->GetModel()->ExecuteAction(random_index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::TakeDamage().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(random_index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - .2f,
				[this]()
				{
					transit("Idle");
				}
			);
		}

		void TakeDamage::exit()
		{
		}

		/*
		* Aiming Class Implementation
		*/
		void Aiming::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::AimingIndex);
			GetAnimator()->SetActualWeight(sm::Aiming().Weight);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::IdleIndex, 1.0f, 0.2f);
		}

		void Aiming::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		 * Attack1 Class Implementation
		 */
		void Attack1::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::Attack1Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Attack1Index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Attack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Attack1Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - 0.2f,
				[this]() {
					CPlayerAnimator* lAnimator = static_cast<CPlayerAnimator*>(GetAnimator());
					if (lAnimator->GetComboAttack() && lAnimator->GetActualIndex() == Attack1Index)
					{
						transit("Attack2");
					}
					else
					{
						transit("Idle");
					}
				}
			);

			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.39784f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack1Index)
					{
						CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_PLAYER);
					}
				}
			);
		}

		void Attack1::exit()
		{
			static_cast<CPlayerAnimator*>(GetAnimator())->SetComboAttack(false);
		}

		/*
		* Attack2 Class Implementation
		*/
		void Attack2::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::Attack2Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Attack2Index, 0.2f, .2f);
			GetAnimator()->SetActualWeight(sm::Attack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Attack2Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - .2f,
				[this]() {
					CPlayerAnimator* lAnimator = static_cast<CPlayerAnimator*>(GetAnimator());
					if (lAnimator->GetComboAttack() && lAnimator->GetActualIndex() == Attack2Index)
					{
						transit("Attack3");
					}
					else
					{
						transit("Idle");
					}
				}
			);
			//sword slash sound
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.03f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index)
					{
						CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_PLAYER);
					}
				}
			);
		}

		void Attack2::exit()
		{
			static_cast<CPlayerAnimator*>(GetAnimator())->SetComboAttack(false);
		}

		/*
		* Attack3 Class Implementation
		*/
		void Attack3::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::Attack3Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Attack3Index, 0.2f, .2f);
			GetAnimator()->SetActualWeight(sm::Attack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Attack3Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - .2f,
				[this]() {
				if (GetAnimator()->GetActualIndex() == Attack3Index)
					{
						transit("Idle");
					}
				}
			);
			//sword slash sound
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.5f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack3Index)
					{
						CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_PLAYER);
					}
				}
			);
		}

		void Attack3::exit()
		{
			static_cast<CPlayerAnimator*>(GetAnimator())->SetComboAttack(false);
		}
	}
}
