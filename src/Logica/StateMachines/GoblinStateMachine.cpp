#include "GoblinStateMachine.h"
#include "Components/Enemies/Goblin/GoblinAnimator.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/Enemies/Goblin/GoblinController.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>

namespace logic
{
	namespace goblin
	{
		CGoblinSM::~CGoblinSM()
		{
		}

		CGoblinSM::CGoblinSM()
		{
		}

		void CGoblinSM::entry()
		{
		}

		void CGoblinSM::exit()
		{
		}

		CModelAnimator* CGoblinSM::GetAnimator()
		{
			if (m_Animator == nullptr)
			{
				m_Animator = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(GetName())
					->GetComponent<CGoblinAnimator>();
			}
			return m_Animator;
		}

		void CGoblinSM::react(sm::Init const& e)
		{
			transit("Idle");
		}

		void CGoblinSM::react(sm::Stop const& e)
		{
			transit("Idle");
		}

		void CGoblinSM::react(ForceMove const& e)
		{
			if (GetAnimator()->GetActualIndex() != WalkIndex && GetAnimator()->GetActualIndex() != RunIndex)
			{
				transit("Move");
			}
		}

		void CGoblinSM::react(sm::Move const& e)
		{
			transit("Move");
		}

		void CGoblinSM::react(sm::Magic1 const& e)
		{
			transit("Heal");
		}

		void CGoblinSM::react(sm::Magic2 const& e)
		{
			transit("Inmunice");
		}

		void CGoblinSM::react(sm::TakeDamage const& e)
		{
			transit("Death");
		}

		void CGoblinSM::react(sm::Dead const& e)
		{
			transit("Death");
		}

		/*
		* STates Impl
		*/

		//
		// Idle class implementation
		//
		void Idle::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::IdleIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::IdleIndex, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Init().Weight);
		}

		void Idle::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* Move Class
		*/
		void Move::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::WalkIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::WalkIndex, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Move().Weight);
		}

		void Move::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* Heal Class
		*/

		void Heal::entry()
		{
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HEAL_GOB, GetAnimator()->GetModel());
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_CURSE_GOB, GetAnimator()->GetModel());
			GetAnimator()->SetActualIndex(sm::AnimIndex::Magic1Index);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::Magic1Index, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(0);
		}

		void Heal::exit()
		{
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::STOP_HEAL_GOB, GetAnimator()->GetModel());
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::STOP_CURSE_GOB, GetAnimator()->GetModel());
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* Inmunice Class
		*/

		void Inmunice::entry()
		{
		}

		void Inmunice::exit()
		{
		}

		/*
		* Death Class
		*/
		void Death::entry()
		{
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_DIE_GOB, GetAnimator()->GetModel());
			GetAnimator()->SetActualIndex(sm::AnimIndex::DeadIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::DeadIndex, 0.2f, 0.2f, 1, true);
			GetAnimator()->SetActualWeight(sm::Dead().Weight);
		}

		void Death::exit()
		{
		}
	}
}
