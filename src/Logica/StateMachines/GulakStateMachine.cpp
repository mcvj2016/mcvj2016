#include "GulakStateMachine.h"
#include "Engine.h"
#include "Components/Enemies/Gulak/GulakAnimator.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include <Wwise_IDs.h>
#include "Sound/ISoundManager.h"
#include "Utils/Coroutine.h"
#include "Components/GUI/Ending.h"

namespace logic
{
	namespace gulak
	{
		CGulakStateMachine::CGulakStateMachine()
		{
		}


		CGulakStateMachine::~CGulakStateMachine()
		{
		}

		void CGulakStateMachine::entry()
		{
		}

		void CGulakStateMachine::exit()
		{
		}

		CModelAnimator* CGulakStateMachine::GetAnimator()
		{
			if (m_Animator == nullptr)
			{
				m_Animator = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(GetName())
					->GetComponent<CGulakAnimator>();
			}
			return m_Animator;
		}

		void CGulakStateMachine::react(Init const& e)
		{
			transit("Idle");
		}

		void CGulakStateMachine::react(Stop const& e)
		{
			transit("Idle");
		}

		void CGulakStateMachine::react(sm::TakeDamage const& e)
		{
			//just play sounds
			int min = 0;
			int max = 100;
			int probability = mathUtils::RandomLToH(min, max);

			if (probability <= 30)
			{
				CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HIT_GULAK, m_Animator->GetModel());
			}
		}

		void CGulakStateMachine::react(Dead const& e)
		{
			transit("Death");
		}

		void CGulakStateMachine::react(Magic1 const&)
		{
			transit("MagicAttack1");
		}

		/*
		* STates Impl
		*/

		//
		// Idle class implementation
		//
		void Idle::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::IdleIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::IdleIndex, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Init().Weight);
		}

		void Idle::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* MAgic attack1 Class
		*/
		void MagicAttack1::entry()
		{
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_GULAKCHARGE, GetAnimator()->GetModel());
			GetAnimator()->SetActualIndex(sm::AnimIndex::Magic1Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Magic1Index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Magic1().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Magic1Index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - .2f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Magic1Index)
					{
						transit("Idle");
					}
				}
			);
		}

		void MagicAttack1::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* Giro Class
		*/
		void TurnArround::entry()
		{
		}

		void TurnArround::exit()
		{
		}

		bool fadedIn = false;
		bool fadedOut = false;
		/*
		* Death Class
		*/
		void Death::entry()
		{
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_GULAKDIE, GetAnimator()->GetModel());
			CEngine::GetInstance().GetSoundManager().SetState(AK::STATES::MAINTRACKSTATES::GROUP, AK::STATES::MAINTRACKSTATES::STATE::ENDING);
			GetAnimator()->SetActualIndex(sm::AnimIndex::DeadIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::DeadIndex, 0.2f, 0.2f, 1, true);
			GetAnimator()->SetActualWeight(sm::Dead().Weight);
			base::utils::CCoroutine::GetInstance().StartCoroutine(5,
				[]()
				{
					CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode("GlobalManagers")
						->GetComponent<CEnding>()->SetEnabled(true);
				});
		}

		void Death::exit()
		{
		}
	}
}
