#ifndef __H_ANIMATEDSTATEMACHINE__
#define __H_ANIMATEDSTATEMACHINE__

#include "SMrc.h"
#include "StateMachine.h"


namespace logic
{
	namespace components
	{
		class CModelAnimator;
	}
	namespace sm
	{
		class CAnimatedSM : public CStateMachine<CAnimatedSM>
		{
		public:
			~CAnimatedSM(){};
			CAnimatedSM(): m_Animator(nullptr)
			{
			};

			virtual void react(Event const & e) {};

			virtual components::CModelAnimator* GetAnimator() = 0;

			virtual void entry() = 0;
			virtual void exit() = 0;

			virtual void react(Init const& e) = 0;
			virtual void react(Stop const& e) = 0;
			virtual void react(ForceMove const& e) = 0;
			virtual void react(Move const& e) = 0;
			virtual void react(Run const& e) = 0;
			virtual void react(Attack const& e) = 0;
			virtual void react(StrongAtack const & e) = 0;
			virtual void react(Magic1 const &) = 0;
			virtual void react(Magic2 const &) = 0;
			virtual void react(TakeDamage const & e) = 0;
			virtual void react(Dead const& e) = 0;
			virtual void react(Aiming const &) = 0;
			virtual void react(Evade const &) = 0;
			components::CModelAnimator* m_Animator;			
		};
	}
}

#endif