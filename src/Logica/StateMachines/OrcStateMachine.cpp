#include "OrcStateMachine.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"
#include "Utils/Coroutine.h"
#include "Components/Enemies/Orc/OrcAnimator.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Components/IA/Seeker.h"
#include "Sound/ISoundManager.h"
#include <Wwise_IDs.h>
#include "Components/Common/Speaker3D.h"

namespace logic
{
	namespace orc
	{
		//
		// Base class implementation
		//

		COrcSM::~COrcSM()
		{
		}

		COrcSM::COrcSM() : CAnimatedSM()
		{
		}

		void COrcSM::entry()
		{
		}

		void COrcSM::exit()
		{
		}

		CModelAnimator* COrcSM::GetAnimator()
		{
			if (m_Animator == nullptr)
			{
				m_Animator = CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(GetName())
				                                   ->GetComponent<COrcAnimator>();
			}
			return m_Animator;
		}

		void COrcSM::react(sm::Init const& e)
		{
			transit("Idle");
		}

		void COrcSM::react(sm::Stop const& e)
		{
			m_Animator->SetActualWeight(0);
			transit("Idle");
		}

		void COrcSM::react(sm::Move const& e)
		{
			transit("Move");
		}

		void COrcSM::react(sm::Attack const& e)
		{
			m_Animator->SetActualWeight(e.Weight);

			//randomize attack, with probability in attackcombo
			//generate random index fort parforming a random animation
			int min = 0;
			int max = 100;
			int probability = mathUtils::RandomLToH(min, max);

			//play random attack sound
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_ATTACK_ORC, GetAnimator()->GetModel());

			//attack combo only has 30% of probability
			if (probability > 50)
			{
				transit("Attack1");
			}
			else
			{
				transit("Attack2");
			}
		}

		void COrcSM::react(sm::StrongAtack const& e)
		{
			transit("StrongAttack");
		}

		void COrcSM::react(sm::TakeDamage const& e)
		{
			int min = 0;
			int max = 100;
			int probability = mathUtils::RandomLToH(min, max);

			//play sound
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_HIT_ORC, GetAnimator()->GetModel());

			if (probability <= 30)
			{
				transit("TakeDamage");
			}
		}

		void COrcSM::react(sm::Dead const& e)
		{
			transit("Death");
		}

		/*
		* STates Impl
		*/

		//
		// Idle class implementation
		//

		void Idle::entry()
		{
			//sanity check for go to dead if some coroutine makes a transit and we should be dead
			if (GetAnimator()->GetActualIndex() != DeadIndex){
				GetAnimator()->SetActualIndex(sm::AnimIndex::IdleIndex);
				GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::IdleIndex, 1.0f, 0.2f);
				GetAnimator()->SetActualWeight(sm::Init().Weight);
			}
		}

		void Idle::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		 * Move Class
		 */
		void Move::entry()
		{
			GetAnimator()->SetActualIndex(sm::AnimIndex::WalkIndex);
			GetAnimator()->GetModel()->BlendCycle(sm::AnimIndex::WalkIndex, 1.0f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Move().Weight);
		}

		void Move::exit()
		{
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), .2f);
		}

		/*
		* Attack1 Class
		*/
		void Attack1::entry()
		{
			GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = false;
			GetAnimator()->SetActualIndex(sm::AnimIndex::Attack1Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Attack1Index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::Attack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Attack1Index)->getDuration();
			waitTime -= .2f;
			base::utils::CCoroutine::GetInstance().StartCoroutineParallel(
				waitTime,
				[this, waitTime](float dt, float totalTime)
				{
					if (!GetAnimator()->GetModel()->GetWeapon()->IsTriggerActive() 
						&& totalTime >= 0.416f && totalTime <= 0.833f 
						&& GetAnimator()->GetActualIndex() == Attack1Index)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(true);
					}
					else
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
					}
					
					//end of anim
					if (totalTime >= waitTime)
					{
						if (GetAnimator()->GetActualIndex() == Attack1Index
							&& GetAnimator()->GetActualIndex() != DeadIndex)
						{
							transit("Idle");
						}
					}
				}
			);

			//sword slash
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.416f,
				[this]()
				{
					CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_ORC, GetAnimator()->GetModel());
				}
			);
		}

		void Attack1::exit()
		{
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				.2f,
				[this]()
				{
					GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = true;
					GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
				}
			);
		}

		/*
		* Attack2 Class
		*/
		void Attack2::entry()
		{
			GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = false;
			GetAnimator()->SetActualIndex(sm::AnimIndex::Attack2Index);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::Attack2Index, 0.2f, .2f);
			GetAnimator()->SetActualWeight(sm::Attack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::Attack2Index)->getDuration();
			waitTime -= .2f;
			
			//splitted in basic corutines for the small diff between trigger activations
			//in a clip with attack1 and 2 concatenated. The corutineparallel sometimes was not
			//entering the second swing for this small temp diff 

			//main corutine for transit
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index
						&& GetAnimator()->GetActualIndex() != DeadIndex)
					{
						transit("Idle");
					}
				}
			);
			//first attack/sword slash
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.416f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(true);
						CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_ORC, GetAnimator()->GetModel());
					}
				}
			);
			//first attack end
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.833f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
					}
				}
			);

			//second attack/sword slash
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				1.667f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(true);
						CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_ORC, GetAnimator()->GetModel());
					}
				}
			);
			//second attack end
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				2.084f,
				[this]()
				{
					if (GetAnimator()->GetActualIndex() == Attack2Index)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
					}
				}
			);
		}

		void Attack2::exit()
		{
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				.2f,
				[this]()
				{
					GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = true;
					GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
				}
			);
		}

		/*
		* StrongAttack Class
		*/
		void StrongAttack::entry()
		{
			GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = false;
			GetAnimator()->SetActualIndex(sm::AnimIndex::StrongAttackIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::StrongAttackIndex, 0.2f, .2f);
			GetAnimator()->SetActualWeight(sm::StrongAtack().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(sm::AnimIndex::StrongAttackIndex)->getDuration();
			waitTime -= .2f;
			base::utils::CCoroutine::GetInstance().StartCoroutineParallel(
				waitTime,
				[this, waitTime](float dt, float totalTime)
				{
					if (!GetAnimator()->GetModel()->GetWeapon()->IsTriggerActive()
						&& totalTime >= 0.5416f && totalTime <= 0.82f)
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(true);
					}
					else
					{
						GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
					}
					//end of anim
					if (totalTime >= waitTime)
					{
						if (GetAnimator()->GetActualIndex() == StrongAttackIndex
							&& GetAnimator()->GetActualIndex() != DeadIndex)
						{
							transit("Idle");
						}
					}
				}
			);
			//sword slash
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.5416f,
				[this]()
				{
					CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_SWORDSLASH_ORC, GetAnimator()->GetModel());
				}
			);
		}

		void StrongAttack::exit()
		{
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				.2f,
				[this]()
				{
					GetAnimator()->GetModel()->GetComponent<CSeeker>()->m_CanSeek = true;
					GetAnimator()->GetModel()->GetWeapon()->SetTriggerActive(false);
				}
			);
		}

		/*
		* TakeDamage Class
		*/
		void TakeDamage::entry()
		{
			//clear other cycle and action
			GetAnimator()->ClearCycle(GetAnimator()->GetActualIndex(), 0.2f);
			GetAnimator()->GetModel()->RemoveAction(GetAnimator()->GetActualIndex());
			//generate random index fort parforming a random animation
			int min = sm::AnimIndex::Damage1Index;
			int max = sm::AnimIndex::Damage2Index;
			sm::AnimIndex random_index = static_cast<sm::AnimIndex>(mathUtils::RandomLToH<int>(min, max));

			GetAnimator()->SetActualIndex(random_index);
			GetAnimator()->GetModel()->ExecuteAction(random_index, 0.2f, 0.2f);
			GetAnimator()->SetActualWeight(sm::TakeDamage().Weight);

			float waitTime = GetAnimator()->GetModel()->GetCurretAnimation(random_index)->getDuration();
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				waitTime - .2f,
				[this]()
				{
					transit("Idle");
				}
			);
		}

		void TakeDamage::exit()
		{
		}

		/*
		* Death Class
		*/
		void Death::entry()
		{
			CEngine::GetInstance().GetImGUI().Log(GetAnimator()->GetModel()->GetName() + " muere");
			GetAnimator()->GetModel()->RemoveAction(GetAnimator()->GetActualIndex());
			GetAnimator()->SetActualIndex(sm::AnimIndex::DeadIndex);
			GetAnimator()->GetModel()->ExecuteAction(sm::AnimIndex::DeadIndex, 0.2f, 0.2f, 1, true);
			GetAnimator()->SetActualWeight(sm::Dead().Weight);
			//play orc dying
			CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_ORCDIE, GetAnimator()->GetModel());
			//play ground body slam sound
			base::utils::CCoroutine::GetInstance().StartCoroutine(
				0.9f,
				[this]()
				{
					CEngine::GetInstance().GetSoundManager().PlayEvent(AK::EVENTS::PLAY_FALL_ORC, GetAnimator()->GetModel());
				}
			);
		}

		void Death::exit()
		{
		}
	}
}
