#ifndef ORCSTATEMACHINE_H
#define ORCSTATEMACHINE_H

#include "AnimatedSM.h"

using namespace logic;
using namespace sm;

namespace logic
{
	namespace orc
	{
		class COrcSM : public CAnimatedSM
		{
		public:
			~COrcSM();
			COrcSM();

			void entry() override;
			void exit() override;

			components::CModelAnimator* GetAnimator() override;

			void react(Init const& e) override;
			void react(Stop const& e) override;
			void react(ForceMove const& e) override{};
			void react(sm::Move const& e) override;
			void react(sm::Run const& e) override{};
			void react(Attack const& e) override;
			void react(StrongAtack const & e) override;
			void react(sm::TakeDamage const & e) override;
			void react(Dead const& e) override;
			void react(Aiming const &) override{}; //not being used
			void react(Evade const &) override{}; //not being used
			void react(Magic1 const &) override{}; //not being used
			void react(Magic2 const &) override{}; //not being used
		};

		class Idle : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Move : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Attack1 : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Attack2 : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class StrongAttack : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class TakeDamage : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Death : public COrcSM
		{
		public:
			void entry() override;
			void exit() override;
		};
	}
}

#endif