#ifndef _H_GOBLINSTATEMACHINE_H
#define _H_GOBLINSTATEMACHINE_H

#include "AnimatedSM.h"

using namespace logic;
using namespace sm;

namespace logic
{
	namespace goblin
	{
		class CGoblinSM : public CAnimatedSM
		{
		public:
			~CGoblinSM();
			CGoblinSM();

			void entry() override;
			void exit() override;

			components::CModelAnimator* GetAnimator() override;

			void react(Init const& e) override;
			void react(Stop const& e) override;
			void react(ForceMove const& e) override;
			void react(sm::Move const& e) override;
			void react(sm::Run const& e) override{};
			void react(Attack const& e) override{}; //not being used
			void react(StrongAtack const & e) override{}; //not being used
			void react(sm::TakeDamage const & e) override; //not being used
			void react(Dead const& e) override;
			void react(Aiming const &) override{}; //not being used
			void react(Evade const &) override{}; //not being used
			void react(Magic1 const &) override; //healing
			void react(Magic2 const &) override; //invulnerability
		};

		class Idle : public CGoblinSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Move : public CGoblinSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Heal : public CGoblinSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Inmunice : public CGoblinSM
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Death : public CGoblinSM
		{
		public:
			void entry() override;
			void exit() override;
		};
	}
}

#endif
