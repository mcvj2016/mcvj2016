#ifndef __H_STATEMACHINE__
#define __H_STATEMACHINE__
#include <map>
#include "Utils/Name.h"
#include "Utils/CheckedDelete.h"

namespace logic
{
	namespace sm
	{

		struct Event { unsigned char Weight; };

		template<typename F>
		class CStateMachine : public CName
		{
		public:
			CStateMachine();
			~CStateMachine();

			void init(const std::string& name) const;
			void transit(const std::string& name) const;

			template<typename E>
			void Add(const std::string &name) const
			{
				m_States[GetName()][name] = new E();
			}

			template<typename E>
			void dispatch(E const & event) {
				current_state[GetName()]->react(event);
			}

		protected:
			static std::map<std::string, F*> current_state;

		private:
			void UpdateCurrentStates(const std::string& name) const;
			void UpdateCurrentStatesOnInit(const std::string& name) const;
			static std::map<std::string, std::map<std::string, F*>> m_States;
		};

		template <typename F>
		CStateMachine<F>::CStateMachine()
		{
		}

		template <typename F>
		CStateMachine<F>::~CStateMachine()
		{
			std::vector<F*> todelete;
			for (auto outer_iter = m_States.begin(); outer_iter != m_States.end(); ++outer_iter) {
				for (typename std::map<std::string, F*>::iterator inner_iter = outer_iter->second.begin(); inner_iter != outer_iter->second.end(); /*inner_iter++*/) {
					F* temp = inner_iter->second;
					inner_iter = outer_iter->second.erase(inner_iter);
					todelete.push_back(temp);
				}
			}
			base::utils::CheckedDelete(todelete);
		}

		template <typename F>
		void CStateMachine<F>::init(const std::string& name) const
		{
			UpdateCurrentStatesOnInit(name);
			current_state[GetName()]->entry();
		}

		template <typename F>
		void CStateMachine<F>::transit(const std::string& name) const
		{
			current_state[GetName()]->exit();
			UpdateCurrentStates(name);
			current_state[GetName()]->entry();
		}

		template <typename F>
		void CStateMachine<F>::UpdateCurrentStates(const std::string& name) const
		{
			typename std::map<std::string,  F*>::iterator it;
			for (it = m_States[GetName()].begin(); it != m_States[GetName()].end(); ++it)
			{
				it->second->current_state[GetName()] = m_States[GetName()][name];
			}
		}

		template <typename F>
		void CStateMachine<F>::UpdateCurrentStatesOnInit(const std::string& name) const
		{
			typename std::map<std::string, F*>::iterator it;
			for (it = m_States[GetName()].begin(); it != m_States[GetName()].end(); ++it)
			{
				it->second->SetName(GetName());
				it->second->current_state[GetName()] = m_States[GetName()][name];
			}
		}

		template <typename F>
		std::map<std::string, F*> CStateMachine<F>::current_state;

		template <typename F>
		std::map<std::string, std::map<std::string, F*>> CStateMachine<F>::m_States;
	}
	
	
}
#endif