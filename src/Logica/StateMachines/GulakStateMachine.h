#ifndef _H_GULAKSTATEMACHINE_H
#define _H_GULAKSTATEMACHINE_H

#include "AnimatedSM.h"

using namespace logic;
using namespace sm;

namespace logic
{
	namespace gulak
	{
		class CGulakStateMachine : public CAnimatedSM
		{
		public:
			CGulakStateMachine();
			virtual ~CGulakStateMachine();

			void entry() override;
			void exit() override;

			components::CModelAnimator* GetAnimator() override;

			void react(Init const& e) override;
			void react(Stop const& e) override;
			void react(ForceMove const& e) override{};
			void react(sm::Move const& e) override{};
			void react(sm::Run const& e) override{};
			void react(sm::Attack const& e) override{}; //not being used
			void react(StrongAtack const & e) override{}; //not being used
			void react(sm::TakeDamage const & e) override;
			void react(Dead const& e) override;
			void react(Aiming const &) override{}; //not being used
			void react(Evade const &) override{}; //not being used
			void react(Magic1 const &) override; //healing
			void react(Magic2 const &) override{}; //invulnerability
		};

		class Idle : public CGulakStateMachine
		{
		public:
			void entry() override;
			void exit() override;
		};

		class MagicAttack1 : public CGulakStateMachine
		{
		public:
			void entry() override;
			void exit() override;
		};

		class TurnArround : public CGulakStateMachine
		{
		public:
			void entry() override;
			void exit() override;
		};

		class Death : public CGulakStateMachine
		{
		public:
			void entry() override;
			void exit() override;
		};
	}
}

#endif