#include "BinFileReader.h"
#include "FileUtils.h"

namespace base {
  namespace utils {
	  
  	CBinFileReader::CBinFileReader(const std::string& aFileName)
        : mFilename(aFileName)
        , mStream(nullptr)
    {
    }

	CBinFileReader::~CBinFileReader()
    {
	}

	bool CBinFileReader::Open()
    {
		bool lOk = false;
		if (!mFilename.empty())
		{
			if (DoesFileExists(mFilename))
			{
				//m_Stream.open(mFilename, std::ios::out | std::ios::in | std::ios::binary);
				fopen_s(&mStream, mFilename.c_str(), "rb");
				lOk = mStream != nullptr;
			}
			else
			{
				CreateEmptyFile(mFilename);
				fopen_s(&mStream, mFilename.c_str(), "rb");
				lOk = mStream != nullptr;
			}
		}

		return lOk;
    }

	void CBinFileReader::Close()
	{
		// the fclose of std automatically deletes de memory of mStream
		//m_Stream.close();
		std::fclose(mStream);
	}

	  void CBinFileReader::Write(unsigned short in)
	  {
		  m_Stream.write((char *)&in, sizeof(unsigned short));
	  }

	  /*template < typename T > T CBinFileReader::Read()
    {
		T lBuffer;
		std::fread(&lBuffer, sizeof(T), 1, mStream);
		return lBuffer;
    }*/

	void* CBinFileReader::ReadRaw(size_t aNumBytes)
    {
		void *lAddr = (void *)malloc(aNumBytes);
		ZeroMemory(lAddr, aNumBytes);
		std::fread(lAddr, aNumBytes, 1, mStream);
		return lAddr;
    }

	  /*
    template<> std::string CBinFileReader::Read<std::string>()
    {
		unsigned short lCount = 0;
		std::fread(&lCount, sizeof(unsigned short), 1, mStream);
		++lCount; // Handle \0
		char* lString = (char *)malloc(sizeof(char) * (lCount));
		ZeroMemory(lString, sizeof(char) * lCount);
		std::fread(lString, sizeof(char) * lCount, 1, mStream);
		std::string lStandardString(lString);
		free(lString);
		return lStandardString;
    }*/
  }
}
