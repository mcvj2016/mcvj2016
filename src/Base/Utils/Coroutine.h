#ifndef __H_COROUTINE__
#define __H_COROUTINE__
#include <vector>
#include <functional>
#include "Singleton.h"

namespace base
{
	namespace utils
	{
		typedef std::function<void()> func;
		typedef std::function<void(float, float)> funcParallel; //params in order: deltaTime, totalTime starting in 0 when created

		class CCoroutine : public CSingleton<CCoroutine>
		{
			class CCoroutineExecutor
			{
			public:
				CCoroutineExecutor();
				CCoroutineExecutor(float timeToWait, func cb);
				CCoroutineExecutor(float corutineLive, funcParallel cb);
				void Update(float dt);
				void Execute() const;
				bool IsFinish() const { return m_Finish; }
				~CCoroutineExecutor();
			private:
				float m_Wait, m_TotalTime;
				func m_Callback;
				funcParallel m_CallbackParallel;
				bool m_Finish;
			};

		public:
			CCoroutine();
			~CCoroutine();
			void Update(float dt);
			void StartCoroutine(float timeToWait, func cb);
			void StartCoroutineParallel(float corutineLive, funcParallel cb);
		private:
			std::vector<CCoroutineExecutor*> m_Executors;
		};
	}
}

#endif
