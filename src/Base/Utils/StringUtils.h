#ifndef _H_VISIBLESU__
#define _H_VISIBLESU__

#include <string>
#include <vector>
#include <Windows.h>
#include <sstream>
#include <stdio.h>
#include <numeric>

namespace base {
    namespace utils
    {
	    inline void FormatString(std::string& output, const char* format, ...)
        {
            va_list args;
            char* buffer;
            va_start(args, format);
            int len = _vscprintf(format, args) + 1;
            buffer = (char*)malloc(len * sizeof(char));
            vsprintf_s(buffer, len, format, args);
            output = buffer;
            free(buffer);
            va_end(args);
        }

	    inline std::vector<std::string>& Split(const std::string& s, char delim,
            std::vector<std::string>& elems)
        {
            std::stringstream ss(s);
            std::string item;

            while (std::getline(ss, item, delim))
                elems.push_back(item);

            return elems;
        }

		inline void RemoveSubstring(std::string& sInput, const std::string& sub)
		{
			std::string::size_type foundpos = sInput.find(sub);
			if (foundpos != std::string::npos)
				sInput.erase(sInput.begin() + foundpos, sInput.begin() + foundpos + sub.length());
		}

	    inline std::vector<std::string> Split(const std::string& s, char delim)
        {
            std::vector<std::string> elems;
            Split(s, delim, elems);
            return elems;
        }

		inline std::string Join(std::vector<std::string>& s)
		{
			std::string prefabCountStr = "";
			prefabCountStr = std::accumulate(begin(s), end(s), prefabCountStr);
			return prefabCountStr;
		}

		inline std::string GetFilenameExtension(const std::string filename)
		{
			return filename.substr(filename.find_last_of(".") + 1);
		}
    }
}

#endif // _H_VISIBLESU__