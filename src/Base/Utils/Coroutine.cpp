#include "Coroutine.h"
#include "CheckedDelete.h"
#include "../../Engine/Engine.h"
#include "../../Engine/Helper/ImguiHelper.h"

namespace base
{
	namespace utils
	{
		CCoroutine::CCoroutineExecutor::CCoroutineExecutor() : m_Wait(.0f), m_TotalTime(.0f), m_Finish(false)
		{
			m_Callback = nullptr;
			m_CallbackParallel = nullptr;
		}

		CCoroutine::CCoroutineExecutor::CCoroutineExecutor(float timeToWait, func cb)
			: m_Wait(timeToWait), m_TotalTime(0), m_Finish(false)
		{
			m_Callback = cb;
			m_CallbackParallel = nullptr;
		}

		CCoroutine::CCoroutineExecutor::CCoroutineExecutor(float corutineLive, funcParallel cb)
			: m_Wait(corutineLive), m_TotalTime(0), m_Finish(false)
		{
			m_Callback = nullptr;
			m_CallbackParallel = cb;
		}

		void CCoroutine::CCoroutineExecutor::Update(float dt)
		{
			//for parallel update rutine
			if (m_CallbackParallel != nullptr)
			{
				m_CallbackParallel(dt, m_TotalTime);
			}
			if (m_TotalTime >= m_Wait)
			{
				//dispatch blocking rutines
				if (m_Callback != nullptr)
				{
					Execute();
				}
				
				m_Finish = true;
			}
			else
			{
				
				m_TotalTime += dt;
			}
		}

		void CCoroutine::CCoroutineExecutor::Execute() const
		{
			m_Callback();
		}

		CCoroutine::CCoroutineExecutor::~CCoroutineExecutor()
		{
		}

		CCoroutine::CCoroutine()
		{
		}

		CCoroutine::~CCoroutine()
		{
			//clear remaining running coroutines
			if (!m_Executors.empty()) {
				for (int i = m_Executors.size() - 1; i >= 0; i--) {
					CheckedDelete(m_Executors.at(i));
					m_Executors.erase(m_Executors.begin() + i);
				}
			}
		}

		void CCoroutine::Update(float dt)
		{
			//loop over executor in this way for updating them or deleting them if they have finish
			if (!m_Executors.empty()) {
				for (int i = m_Executors.size() - 1; i >= 0; i--) {
					if (m_Executors.at(i)->IsFinish()) {
						CheckedDelete(m_Executors.at(i));
						m_Executors.erase(m_Executors.begin() + i);
					}
					else
					{
						m_Executors.at(i)->Update(dt);
					}
				}
			}
		}

		void CCoroutine::StartCoroutine(float timeToWait, func cb)
		{
			m_Executors.push_back(new CCoroutineExecutor(timeToWait, cb));
		}

		void CCoroutine::StartCoroutineParallel(float corutineLive, funcParallel cb)
		{
			m_Executors.push_back(new CCoroutineExecutor(corutineLive, cb));
		}
	}
}
