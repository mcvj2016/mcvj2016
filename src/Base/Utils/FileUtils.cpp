#include "StringUtils.h"
#include <stdio.h>
#include <vector>
#include <Windows.h>
#include <string>
#include <sys\stat.h>
#include "FileUtils.h"
#include <cassert>
#include <fstream>

namespace base {
    namespace utils
    {
		long GetFileSize(std::string filename)
		{
			struct stat stat_buf;
			int rc = stat(filename.c_str(), &stat_buf);
			return rc == 0 ? stat_buf.st_size : -1;
		}

		bool DoesFileExists(const std::string& file)
		{
			struct stat buf;
			return (stat(file.c_str(), &buf) == 0);
		}

		void GetFilesFromPath( const std::string& Path, const std::string& Extension, std::vector<std::string>& _OutFiles)
		{
			std::string FilesToLookUp = Path + "*." + Extension;
			WIN32_FIND_DATA FindFileData;
			HANDLE hFind = FindFirstFile(FilesToLookUp.c_str(), &FindFileData);

			while (hFind != INVALID_HANDLE_VALUE)
			{
				_OutFiles.push_back(FindFileData.cFileName);

				if (!FindNextFile(hFind, &FindFileData))
				{
					FindClose(hFind);
					hFind = INVALID_HANDLE_VALUE;
				}
			}
		}

	    bool CreateFolder(const std::string& path)
		{
			if ((CreateDirectory(path.c_str(), NULL) ||
				ERROR_ALREADY_EXISTS == GetLastError()))
			{
				return true;
			}

			return false;
	    }

		void CreateEmptyFile(const std::string& file)
		{
			std::fstream fs;
			fs.open(file, std::ios::out);
			fs.close();
		}

		std::wstring StringToWs(const std::string& s)
	    {
			int len;
			int slength = (int)s.length() + 1;
			len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
			wchar_t* buf = new wchar_t[len];
			MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
			std::wstring r(buf);
			delete[] buf;
			return r;
	    }

		// copy in binary mode
		bool copyFile(const char *SRC, const char* DEST)
		{
			std::ifstream src(SRC, std::ios::binary);
			std::ofstream dest(DEST, std::ios::binary);
			dest << src.rdbuf();
			return src && dest;
		}

	    bool IsFileTimeLessThan(const std::string& file1, const std::string& file2)
	    {
			HANDLE hFileOrig = CreateFile(file1.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			HANDLE hFile = CreateFile(file2.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			FILETIME ftCreate, ftAccess, ftWrite, ftCreateOrig, ftAccessOrig, ftWriteOrig;
			auto res = GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite);
			auto res2 = GetFileTime(hFileOrig, &ftCreateOrig, &ftAccessOrig, &ftWriteOrig);

			assert(res && res2);

			CloseHandle(hFileOrig);
			CloseHandle(hFile);

			return (CompareFileTime(&ftWriteOrig, &ftWrite) == -1);
	    }
    }
}
