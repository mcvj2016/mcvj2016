#pragma once
#ifndef __H_File_UTILS__
#define __H_File_UTILS__

#include <vector>


namespace base {
    namespace utils
    {
		long GetFileSize(std::string filename);
		bool DoesFileExists(const std::string& file);
		void GetFilesFromPath(const std::string& Path, const std::string& Extension, std::vector<std::string>& _OutFiles);
		bool CreateFolder(const std::string& path);
		std::wstring StringToWs(const std::string& s);
		bool IsFileTimeLessThan(const std::string& file1, const std::string& file2);
		void CreateEmptyFile(const std::string& file);
		bool copyFile(const char *SRC, const char* DEST);
    }
}

#endif