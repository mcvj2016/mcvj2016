#pragma once
#ifndef __H_TRANSFORM__
#define __H_TRANSFORM__

#include "Utils/Defines.h"
#include "Math/Matrix44.h"
#include "Math/Quaternion.h"

XML_FORWARD_DECLARATION

class CTransform
{
public:
	Vect3f   m_Position, m_PrevPos, m_Scale;
	float    m_Yaw, m_Pitch, m_Roll;
	float    m_YawOffSet, m_PitchOffSet, m_RollOffSet;
	Quatf m_Rotation;
    CTransform();
    CTransform(const Vect3f &Position);
    CTransform(const Vect3f &Position, float Yaw, float Pitch, float Roll);
    CTransform(float Yaw, float Pitch, float Roll);
    CTransform(const CXMLElement* XMLTreeNode);
    virtual ~CTransform();
    virtual void SetPosition(const Vect3f &Position);
    const Vect3f & GetPosition() const;
    const Vect3f & GetPrevPosition() const;
    GET_SET_REF(Vect3f, Scale)
    const Mat44f & GetMatrix();
	const Mat44f & CTransform::GetMatrixQuat();
	virtual void setRotationFromQuat(Quatf quaternion);
	virtual Quatf getQuatFromRotation();

	void SetYaw(float Yaw);
	float GetYaw() { return m_Yaw; }
	float GetYaw() const { return m_Yaw; }
    GET_SET(float,Pitch)
    GET_SET(float,Roll)
	GET_SET(Quatf, Rotation)
	GET_SET(Mat44f, TransformMatrix)

    virtual Vect3f GetForward() const;
    Vect3f GetUp() const;
	Vect3f GetRight() const;
	Vect3f GetLeft() const;
	Vect3f GetBackward() const;
    
    void SetForward(const Vect3f& aForward );

protected:

    Mat44f   m_TransformMatrix, m_RotationMatrix, m_TranslationMatrix, m_ScaleMatrix;
};

#endif //__H_TRANSFORM__