#include <Windows.h>
#include "Utils/Imgui/imgui_impl_dx11.h"
#include "Engine.h"

//libreria de ayuda para memleaks
#if defined(_DEBUG)
#include <vld.h>
#endif
#include "Input/InputManager.h"
#include "Render/RenderManager.h"
#include "Utils/Logger/Logger.h"


#define APPLICATION_NAME	"Orkelf"

//captura el evento para pasar a pantalla completa
void ToggleFullscreen(HWND Window, WINDOWPLACEMENT& WindowPosition)
{
	// This follows Raymond Chen's prescription
	// for fullscreen toggling, see:
	// http://blogs.msdn.com/b/oldnewthing/archive/2010/04/12/9994016.aspx

	DWORD Style = GetWindowLongW(Window, GWL_STYLE);
	if (Style & WS_OVERLAPPEDWINDOW)
	{
		MONITORINFO MonitorInfo = { sizeof(MonitorInfo) };
		if (GetWindowPlacement(Window, &WindowPosition) &&
			GetMonitorInfoW(MonitorFromWindow(Window, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
		{
			SetWindowLongW(Window, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(Window, HWND_TOP,
				MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
				MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left,
				MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top,
				SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		}
	}
	else
	{
		SetWindowLongW(Window, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(Window, &WindowPosition);
		SetWindowPos(Window, 0, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
			SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	}
}

bool s_WindowActive = true;
bool appLoaded = false;

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	switch (msg)
	{
	
	case WM_SIZING:
	{
		if (wParam != SIZE_MINIMIZED)
		{
			auto& renderManager = engine::CEngine::GetInstance().GetRenderManager();
			renderManager.Resize(static_cast<UINT>(LOWORD(lParam)), static_cast<UINT>(HIWORD(lParam)), hWnd);
		}
	}
	case WM_DESTROY:
	{	
		PostQuitMessage(0);
		return 0;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
			//Cleanup();
			PostQuitMessage(0);
			return 0;
		}
	}
	case WM_ACTIVATE:
	{
		s_WindowActive = wParam != WA_INACTIVE;
	}
	case WM_SYSCOMMAND:
		if (wParam == SC_MAXIMIZE)
		{
			auto& renderManager = engine::CEngine::GetInstance().GetRenderManager();
			renderManager.Resize(static_cast<UINT>(LOWORD(lParam)), static_cast<UINT>(HIWORD(lParam)), hWnd);
		}
		break;
	default: break;
	}
	//end switch( msg )

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//-----------------------------------------------------------------------
// WinMain
//-----------------------------------------------------------------------
int main()
{
	// Register the window class
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(nullptr), nullptr, nullptr, nullptr, nullptr, APPLICATION_NAME, nullptr };

	RegisterClassEx(&wc);

	//ajustes de la ventana
	int width = 1280;
	int height = 720;
	RECT rc = {
		0, 0, width, height
	};
	auto dwStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
	AdjustWindowRect(&rc, dwStyle, false);


	// Create the application's window
	HWND hWnd = CreateWindow(APPLICATION_NAME, APPLICATION_NAME, dwStyle,
		100, 100, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, wc.hInstance, nullptr);

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);

	engine::CEngine& l_Engine = engine::CEngine::GetInstance();
	l_Engine.Init(hWnd, width, height);

	//MAIN MSG LOOP
	while (msg.message != WM_QUIT)
	{
		//preupdate del input manager
		l_Engine.GetInputManager().PreUpdate(s_WindowActive);
		while (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			ImGui_ImplDX11_WndProcHandler(hWnd, msg.message, msg.wParam, msg.lParam);
			bool fHandled = false;
			//bool WasDown = false, IsDown = false, Alt = false;

			if ((msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) || msg.message == WM_INPUT)
			{
				fHandled = l_Engine.GetInputManager().HandleMouse(msg);
			}
			if ((msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST) && msg.message != WM_CHAR)
			{
				fHandled = l_Engine.GetInputManager().HandleKeyboard(msg);
				TranslateMessage(&msg);
			}
			if (msg.message == WM_CHAR)
			{
				l_Engine.GetInputManager().SetLastChar(msg.wParam);
			}
			if (msg.message == WM_SYSKEYDOWN)
			{
				if (msg.wParam == VK_RETURN)
				{
					WINDOWPLACEMENT windowPosition = { sizeof(WINDOWPLACEMENT) };
					GetWindowPlacement(msg.hwnd, &windowPosition);

					ToggleFullscreen(msg.hwnd, windowPosition);
					fHandled = true;
				}
			}
			/*if (msg.message == WM_KEYUP)
			{
				WasDown = ((msg.lParam & (1 << 30)) != 0);
				IsDown = ((msg.lParam & (1 << 31)) == 0);
				Alt = ((msg.lParam & (1 << 29)) != 0);

				if (!WasDown && IsDown && msg.wParam == VK_RETURN && Alt)
				{
					LOG_INFO_APPLICATION("%s", "\nentra en toggle\n");

					WINDOWPLACEMENT windowPosition = { sizeof(WINDOWPLACEMENT) };
					GetWindowPlacement(msg.hwnd, &windowPosition);
					
					ToggleFullscreen(msg.hwnd, windowPosition);
					fHandled = true;
				}
			}*/
			//cualquier otro mensaje que no interesa o del propio windows
			if (!fHandled)
			{
				//funciones de windows...
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if (msg.message == WM_QUIT)
				break;
		}
		
		ImGui_ImplDX11_NewFrame();

		//reset de input tras recoger los mensages de este frame
		l_Engine.GetInputManager().PostUpdate();

		//procesado de nuevos inputs
		l_Engine.ProcessInputs();

		//update
		l_Engine.Update();

		//render
		l_Engine.Render();

		//END ENGINE INGAME
		if (l_Engine.m_ExitGame)
		{
			l_Engine.Dispose();
			UnregisterClass(APPLICATION_NAME, wc.hInstance);

			return 0;
		}
	}
	l_Engine.Dispose();
	UnregisterClass(APPLICATION_NAME, wc.hInstance);

	// Anyadir una llamada a la alicacion para finalizar/liberar memoria de todos sus datos
	return 0;
}