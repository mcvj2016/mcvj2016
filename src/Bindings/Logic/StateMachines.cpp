#include "lua_utils.h"
#include "StateMachines/PlayerSM.h"
#include "luabind_macros.h"
#include "StateMachines/SMEventsWrapper.h"

using namespace tinyfsm; 

//set initial playersm state
FSM_INITIAL_STATE(CPlayerSM, Idle)

namespace lua
{
	void StartSM(CAnimatedSM* sm)
	{
		sm->start();
	}

	template<typename E>
	void SendEvent(CAnimatedSM* sm, E const & event)
	{
		sm->dispatch(event);
	}

	template<> void BindClass<Event>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(Event)
			LUA_DECLARE_DEFAULT_CTOR
		LUA_END_DECLARATION
	}

	template<> void BindClass<Stop>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(Stop, Event)
			LUA_DECLARE_DEFAULT_CTOR
		LUA_END_DECLARATION
	}

	template<> void BindClass<Walk>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(Walk, Event)
			LUA_DECLARE_DEFAULT_CTOR
		LUA_END_DECLARATION
	}

	/*template<> void BindClass<Idle>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(Idle, CPlayerSM)
			LUA_DECLARE_METHOD(Idle, entry)
			LUA_DECLARE_METHOD(Idle, exit)
		LUA_END_DECLARATION
	}

	template<> void BindClass<Moving>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(Moving, CPlayerSM)
			LUA_DECLARE_METHOD(Moving, entry)
			LUA_DECLARE_METHOD(Moving, exit)
		LUA_END_DECLARATION
	}*/

	template<> void BindClass<Fsm<CAnimatedSM>>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(Fsm<CAnimatedSM>)
		LUA_END_DECLARATION
	}

	template<> void BindClass<CAnimatedSM>(lua_State *aLua)
	{
		//bind the class events
		BindClass<Event>(aLua);
		BindClass<Walk>(aLua);
		BindClass<Stop>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CAnimatedSM, Fsm<CAnimatedSM>)
		LUA_END_DECLARATION
	}

	template<> void BindClass<CPlayerSM>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CPlayerSM, CAnimatedSM)
			LUA_DECLARE_DEFAULT_CTOR
		LUA_END_DECLARATION

		//bind the dereived class states
		//BindClass<Idle>(aLua);
		//BindClass<Moving>(aLua);

		//resgister functions
		REGISTER_LUA_FUNCTION(aLua, "StartSM", &StartSM)
		REGISTER_LUA_FUNCTION(aLua, "SendSMEvent", &SendEvent<Walk>)
		REGISTER_LUA_FUNCTION(aLua, "SendSMEvent", &SendEvent<Stop>)
	}
}




