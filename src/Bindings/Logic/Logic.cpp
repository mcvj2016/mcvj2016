#include "lua_utils.h"
#include "StateMachines/AnimatedSM.h"
#include "StateMachines/PlayerSM.h"

using namespace tinyfsm;

namespace lua
{
	template<> void BindLibrary<Logic>(lua_State *aLua)
	{
		/*
		 * STATE MACHINES
		 */
		//bind parent classes
		BindClass<Fsm<CAnimatedSM>>(aLua);
		BindClass<CAnimatedSM>(aLua);

		//begin binding statemachines
		BindClass<CPlayerSM>(aLua);
	}
	
}
