#ifndef _LOGICA_SCRIPTMANAGER_15022017160752_H
#define _LOGICA_SCRIPTMANAGER_15022017160752_H
#include "lua/lua.hpp"
#include "ScriptManager.h"
#include "Script.h"
#include <Utils/FileUtils.h>
#include "Engine.h"
#include "Graphics/Scenes/SceneManager.h"

namespace logic
{
	CScriptManager::CScriptManager() :
		m_Path("data/lua"),
		m_LS(nullptr),
		m_CommonPath(std::string(m_Path + "/")),
		m_SpecificPath(std::string(m_Path + "/specific/")),
		m_LibPath(std::string(m_Path + "/lib/")),
		m_ComponentsPath(std::string(m_Path + "/components/")),
		m_CorePath(std::string(m_Path + "/core/")),
	m_Reloading(false)
	{
		Init();
	}

	CScriptManager::~CScriptManager()
	{
		Release();
		m_LS = nullptr;
	}

	bool CScriptManager::Load(bool aLoadSpecific)
	{
		//common anonym functions
		if (!aLoadSpecific)
		{
			LoadFiles(m_CommonPath, m_LS);
			LoadFiles(m_CorePath, m_LS);
			LoadFiles(m_ComponentsPath, m_LS);
		}
		else
		{
			std::vector<std::string> lOutFiles;
			//specific files
			base::utils::GetFilesFromPath(m_SpecificPath, "lua", lOutFiles);
			for (std::string l_out_file : lOutFiles)
			{
				//remove extension
				std::string lFileName = l_out_file;
				std::string s = ".lua";
				std::string::size_type i = l_out_file.find(s);

				if (i != std::string::npos)
					l_out_file.erase(i, s.length());

				CScript* lCs = new CScript(l_out_file);
				lCs->Load(std::string(m_SpecificPath + lFileName));
				Add(l_out_file, lCs);
			}
		}

		return true;
	}

	void CScriptManager::LoadFiles(std::string aPath, lua_State* aLs)
	{
		std::vector<std::string> lOutFiles;

		base::utils::GetFilesFromPath(aPath, "lua", lOutFiles);
		for (std::string l_out_file : lOutFiles)
		{
			// Loading and executing a file
			int status = luaL_loadfile(m_LS, std::string(aPath + l_out_file).c_str());
			auto const lua_ok = LUA_OK;
			if (status != lua_ok)
			{
				if (status == LUA_ERRSYNTAX)
					printf("[LUA]Sintax Error: %s", lua_tostring(aLs, -1));

				else if (status == LUA_ERRFILE)
					printf("[LUA]File Error: %s", lua_tostring(aLs, -1));

				else if (status == LUA_ERRMEM)
					printf("[LUA]Memory Error: %s", lua_tostring(aLs, -1));

			}
			//ejecucion del codigo
			status = lua_pcall(aLs, 0, LUA_MULTRET, 0);
			if (status != lua_ok)
			{
				std::string err = lua_tostring(aLs, -1);
				printf("[LUA]Error executing code %s", err.c_str());
			}
		}
	}

	void CScriptManager::Release() const
	{
		lua_close(m_LS);
	}

	lua_State* CScriptManager::GetState()
	{
		return m_LS;
	}

	bool CScriptManager::Reload()
	{
		Release(); 
		Init(); 
		Load();

		engine::CEngine& lEngine = engine::CEngine::GetInstance();

		//init lua
		lua::BindLibrary<lua::Loader>(lEngine.GetScriptManager().GetState());

		call_function<void>(lEngine.GetScriptManager().GetState(), "InitLua", std::ref(lEngine.m_DebugMode));

		//bind all scenenodes to lua and attach its components
		for (engine::scenes::CSceneNode* node : lEngine.GetSceneManager().GetCurrentScene()->GetSceneNodes())
		{
			node->BindToLua();
		}
		m_Reloading = false;
		return true;
	}

	void CScriptManager::Init()
	{
		m_LS = luaL_newstate();
		luaL_openlibs(m_LS);
		luabind::open(m_LS);
		lua_atpanic(m_LS, lua::atpanic);
		lua_register(m_LS, "_ALERT", lua::atpanic);
	}
}


#endif	// _LOGICA_SCRIPTMANAGER_15022017160752_H