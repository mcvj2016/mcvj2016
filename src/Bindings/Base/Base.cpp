#include "lua_utils.h"
#include "luabind_macros.h"
#include "Utils/Name.h"
#include "Math/Transform.h"
#include "Math/Color.h"
#include "Math/Quaternion.h"
#include "Utils/Singleton.h"

namespace lua
{
	class CSceneNode;

	template <> void BindClass<CName>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CName)
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_CTOR_1(std::string)
			LUA_DECLARE_METHOD(CName, SetName)
			LUA_DECLARE_METHOD(CName, GetName)
		LUA_END_DECLARATION
	}

	

	template <> void BindClass<CTransform>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<CTransform>("CTransform")
				.def(constructor<>())
				.def("GetYaw", &CTransform::GetYaw)
				.def("SetYaw", &CTransform::SetYaw)
				.def("GetPitch", &CTransform::GetPitch)
				.def("SetPitch", &CTransform::SetPitch)
				.def("GetRoll", &CTransform::GetRoll)
				.def("SetRoll", &CTransform::SetRoll)
				.def("SetScale", &CTransform::SetScale)
				.def("GetUp", &CTransform::GetUp)
				.def("GetForward", &CTransform::GetForward)
				.def("SetForward", &CTransform::SetForward)
				.def("GetRight", &CTransform::GetRight)
				.def("GetLeft", &CTransform::GetLeft)
				.def("GetBackward", &CTransform::GetBackward)
				.def("getQuatFromRotation", &CTransform::getQuatFromRotation)
				.def_readwrite("m_Position", &CTransform::m_Position)
				.def_readwrite("m_Yaw", &CTransform::m_Yaw)
				.def_readwrite("m_Pitch", &CTransform::m_Pitch)
				.def_readwrite("m_Roll", &CTransform::m_Roll)
			];
	}

	template <> void BindClass<Vect2f>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<Vect2f>("Vect2f")
				.def(constructor<>())
				.def(constructor<float, float>())
				.def_readwrite("x", &Vect2f::x)
				.def_readwrite("y", &Vect2f::y)
			];
	}

	template <> void BindClass<Vect3f>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<Vect3f>("Vect3f")
				.def(constructor<>())
				.def(constructor<float, float, float>())
				.def(const_self + const_self)
				.def(const_self - const_self)
				.def(const_self * const_self)
				.def(const_self ^ const_self)
				.def(const_self == const_self)
				.def(self * float())
				.def(const_self * float())
				.def(const_self / float())
				.def(float() * self)
				.def(float() * const_self)
				.def(float() / const_self)
				.def(const_self + float())
				.def(const_self - float())
				.def(float() + const_self)
				.def(float() - const_self)
				.def(-const_self)
				.def_readwrite("x", &Vect3f::x)
				.def_readwrite("y", &Vect3f::y)
				.def_readwrite("z", &Vect3f::z)
				.def( "GetNormalized", &Vect3f::GetNormalized )
			];
	}

	template <> void BindClass<Vect4f>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<Vect4f>("Vect4f")
				.def(constructor<>())
				.def(constructor<float, float, float, float>())
				.def(const_self + const_self)
				.def(const_self - const_self)
				.def(const_self * const_self)
				//.def(const_self ^ const_self)
				.def(const_self == const_self)
				.def(self * float())
				.def(const_self * float())
				.def(const_self / float())
				.def(float() * self)
				.def(float() * const_self)
				.def(float() / const_self)
				.def(const_self + float())
				.def(const_self - float())
				.def(float() + const_self)
				.def(float() - const_self)
				.def(-const_self)
				.def_readwrite("x", &Vect4f::x)
				.def_readwrite("y", &Vect4f::y)
				.def_readwrite("z", &Vect4f::z)
				.def_readwrite("w", &Vect4f::w)
			];
	}

	template <> void BindClass<CColor>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CColor, Vect4f)
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_CTOR_1(const Vect4f&)
			LUA_DECLARE_CTOR_4(float, float, float, float)
			LUA_DECLARE_METHOD(CColor, SetAlpha)
			LUA_DECLARE_METHOD(CColor, GetAlpha)
		LUA_END_DECLARATION
	}

	template <> void BindClass<Quatf>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< Quatf >( "Quatf" )
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_CTOR_2(Vect3f, float)
			LUA_DECLARE_CTOR_4(float, float, float, float)
			/*.def(const_self + const_self)
			.def(const_self - const_self)
			.def(const_self * const_self)
			.def(self * float())
			.def(float() * const_self)
			.def(float() + const_self)
			.def(float() - const_self)*/
			LUA_DECLARE_METHOD(Quatf, QuatFromYawPitchRoll)
			LUA_DECLARE_METHOD(Quatf, QuatFromEuler)
			LUA_DECLARE_METHOD(Quatf, EulerFromQuat)
			LUA_DECLARE_METHOD(Quatf, GetYaw)
			LUA_DECLARE_METHOD(Quatf, GetPitch)
			LUA_DECLARE_METHOD(Quatf, GetRoll)
			LUA_DECLARE_METHOD(Quatf, GetRightVector)
			LUA_DECLARE_METHOD(Quatf, GetForwardVector)
			LUA_DECLARE_METHOD(Quatf, GetUpVector)
			LUA_DECLARE_PROP_RW(x, Quatf)
			LUA_DECLARE_PROP_RW(y, Quatf)
			LUA_DECLARE_PROP_RW(z, Quatf)
			LUA_DECLARE_PROP_RW(w, Quatf)
		LUA_END_DECLARATION
	}

	template <> void BindLibrary<Base>(lua_State *aLua)
	{
		BindClass<Vect2f>(aLua);
		BindClass<Vect3f>(aLua);
		BindClass<Vect4f>(aLua);
		BindClass<CColor>(aLua);
		BindClass<Quatf>(aLua);
		BindClass<CTransform>(aLua);
		BindClass<CName>(aLua);
		
		//for std::ref things 
		module(aLua)
		[
			class_<int&>("int&"),
			class_<bool&>("bool&")
		];
	}
}
