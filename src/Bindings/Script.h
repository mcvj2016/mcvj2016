#ifndef _LOGICA_CSCRIPT_H
#define _LOGICA_CSCRIPT_H

#include "lua_utils.h"
#include "Utils/Name.h"

namespace logic
{
	class CScript : public CName
	{
	public:
		CScript();
		CScript(const std::string& aName);
		virtual ~CScript();
		void Release() const;
		bool Load(const std::string& aFilename) const;
		bool operator()(const std::string& aCode) const;
		bool Reload() const;
		lua_State* GetState();
		

	private:
		lua_State* m_LS;
		std::string m_Filename;
		void Init();
	};
}
#endif // _LOGICA_CSCRIPT_H