#include "Script.h"
#include "lua_utils.h"
#include <lua/lua.hpp>

namespace logic
{
	CScript::CScript() :
		CName(""),
		m_LS(nullptr),
		m_Filename("")
	{
		Init();
	}

	CScript::CScript(const std::string& aName) : CName(aName)
	{
		Init();
	}

	CScript::~CScript()
	{
		Release();
		m_LS = nullptr;
	}

	void CScript::Release() const
	{
		// Close the lua state
		lua_close(m_LS);
	}

	bool CScript::Load(const std::string& aFilename = "") const
	{
		// Loading and executing a file
		int status = luaL_loadfile(m_LS, aFilename.c_str());
		auto const lua_ok = LUA_OK;
		if (status != lua_ok)
		{
			if (status == LUA_ERRSYNTAX)
				printf("[LUA]Sintax Error: %s", lua_tostring(m_LS, -1));

			else if (status == LUA_ERRFILE)
				printf("[LUA]File Error: %s", lua_tostring(m_LS, -1));

			else if (status == LUA_ERRMEM)
				printf("[LUA]Memory Error: %s", lua_tostring(m_LS, -1));

			return false;
		}
		//ejecucion del codigo
		status = lua_pcall(m_LS, 0, LUA_MULTRET, 0);
		if (status != lua_ok)
		{
			std::string err = lua_tostring(m_LS, -1);
			printf("[LUA]Error executing code %s", err.c_str());
			return false;
		}

		return true;
	}

	bool CScript::operator()(const std::string& aCode) const
	{
		// Calling directly a piece of code
		return static_cast<bool>(luaL_dostring(m_LS, aCode.c_str()));
	}

	bool CScript::Reload() const
	{
		if (m_LS != nullptr)
		{
			Release();
		}
		
		return Load();
	}

	lua_State* CScript::GetState()
	{
		return m_LS;
	}

	void CScript::Init()
	{
		// Init of the lua state and the bindings
		m_LS = luaL_newstate();
		luaL_openlibs(m_LS);
		luabind::open(m_LS);
		lua_atpanic(m_LS, lua::atpanic);
		lua_register(m_LS, "_ALERT", lua::atpanic);
	}
}
