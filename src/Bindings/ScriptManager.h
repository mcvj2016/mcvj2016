#ifndef _LOGICA_SCRIPTMANAGER_15022017172018_H
#define _LOGICA_SCRIPTMANAGER_15022017172018_H
#include "Utils/TemplatedMap.h"
#include "lua_utils.h"
#include "Script.h"

namespace logic
{
	class CScriptManager : public base::utils::CTemplatedMap<CScript>
	{
	public:
		CScriptManager();
		~CScriptManager();
		bool Load(bool aLoadSpecific = false);
		void LoadFiles(std::string aPath, lua_State* aLs);
		void Release() const;
		lua_State* GetState();
		bool Reload();
		bool m_Reloading;
	private:
		std::string m_Path;
		std::string m_CommonPath;
		std::string m_SpecificPath;
		std::string m_LibPath;
		lua_State* m_LS;
		std::string m_CorePath;
		std::string m_ComponentsPath;
		void Init();
		
	};
}
#endif	// _LOGICA_SCRIPTMANAGER_15022017172018_H