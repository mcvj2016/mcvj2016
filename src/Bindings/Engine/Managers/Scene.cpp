#include "lua_utils.h"
#include "Engine.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Graphics/Scenes/SceneManager.h"
#include "luabind_macros.h"
#include "Graphics/Cinematics/Cinematic.h"
#include "Graphics/Scenes/SceneLight.h"
#include "Graphics/Shaders/ShaderManager.h"
#include "Graphics/Cal3d/SceneAnimatedModel.h"

using namespace engine::materials;
using namespace engine::scenes;
using namespace base::utils;
using namespace engine::cal3d;

namespace lua
{
	CSceneManager& GetSceneManager()
	{
		return engine::CEngine::GetInstance().GetSceneManager();
	}

	void ReloadShaders()
	{
		engine::CEngine::GetInstance().GetShaderManager().Reload();
	}

	void ReloadScene()
	{
		engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->m_Reloading = true;
	}

	CSceneNode* GetSceneNode(std::string name)
	{
		return engine::CEngine::GetInstance().GetSceneManager().GetCurrentScene()->GetSceneNode(name);
	}
	//CSceneNode
	template <> void BindClass<CSceneNode>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS2(CSceneNode, CTransform, CName)
			LUA_DECLARE_METHOD(CSceneNode, IsVisible)
			LUA_DECLARE_METHOD(CSceneNode, SetVisible)
			LUA_DECLARE_PROP_RW(m_hasPhysx, CSceneNode)
			LUA_DECLARE_PROP_RW(m_ShapeType, CSceneNode)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< std::vector<CSceneNode*> >( "CSceneNodeVector" )
			.def("size", &std::vector<CSceneNode*>::size)
			.def("at", (CSceneNode*&(std::vector<CSceneNode*>::*)(size_t))&std::vector<CSceneNode*>::at)
		LUA_END_DECLARATION
	}
	//SceneLight
	template <> void BindClass<CSceneLight>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CSceneLight, CSceneNode)
		LUA_END_DECLARATION
	}
	//SceneAnimatedModel
	template <> void BindClass<CSceneAnimatedModel>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CSceneAnimatedModel, CSceneNode)
			LUA_DECLARE_METHOD(CSceneAnimatedModel, BlendCycle)
			LUA_DECLARE_METHOD(CSceneAnimatedModel, ClearCycle)
			LUA_DECLARE_METHOD(CSceneAnimatedModel, GetAnimatedCoreModel)
			LUA_DECLARE_METHOD(CSceneAnimatedModel, getModelName)
		LUA_END_DECLARATION
	}
	//Layer : CTempaltedMapVector<CSceneNode>
	template <> void BindClass<CTemplatedMapVector< CSceneNode >>(lua_State *aLua)
	{
		BindClass<CSceneNode>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector< CSceneNode >)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CSceneNode >, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CSceneNode >, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CSceneNode >, GetByName)
		LUA_END_DECLARATION
	}
	//Layer
	template <> void BindClass<CLayer>(lua_State *aLua)
	{
		BindClass<CTemplatedMapVector<CSceneNode>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS2(CLayer, CName, CTemplatedMapVector< CSceneNode >)
			LUA_DECLARE_METHOD(CLayer, SetActive)
			LUA_DECLARE_METHOD(CLayer, IsActive)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< std::vector<CLayer*> >("CLayerVector")
			.def("size", &std::vector<CLayer*>::size)
			.def("at", (CLayer*&(std::vector<CLayer*>::*)(size_t))&std::vector<CLayer*>::at)
		LUA_END_DECLARATION
	}
	//CScene : CTempaltedMapVector<CLayer>
	template <> void BindClass<CTemplatedMapVector< CLayer >>(lua_State *aLua)
	{
		BindClass<CLayer>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector< CLayer >)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CLayer >, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CLayer >, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CLayer >, GetByName)
		LUA_END_DECLARATION
	}
	//Scene
	template <> void BindClass<CScene>(lua_State *aLua)
	{
		BindClass<CTemplatedMapVector<CLayer>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS2(CScene, CName, CTemplatedMapVector< CLayer >)
			LUA_DECLARE_METHOD(CScene, GetSceneNode)
			LUA_DECLARE_METHOD(CScene, GetSceneNodes)
			LUA_DECLARE_METHOD(CScene, IsActive)
			LUA_DECLARE_METHOD(CScene, SetActive)
			LUA_DECLARE_METHOD(CScene, Reload)
		LUA_END_DECLARATION

	}
	//SceneManager : CTemplatedMapVector<CScene>
	template <> void BindClass<CTemplatedMapVector<CScene>>(lua_State *aLua)
	{
		BindClass<CScene>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector< CScene >)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CScene >, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CScene >, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector< CScene >, GetByName)
		LUA_END_DECLARATION
	}
	//SceneManager : TODO
	template <> void BindClass<CSceneManager>(lua_State *aLua)
	{
		assert(aLua);

		BindClass<CTemplatedMapVector<CScene>>(aLua);
		//needs scenenode to be loaded by the scene binding
		BindClass<CSceneLight>(aLua);
		BindClass<CSceneAnimatedModel>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CSceneManager, CTemplatedMapVector<CScene>)
			LUA_DECLARE_METHOD(CSceneManager, GetCurrentScene)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "scene_ReloadShaders", &ReloadShaders);
		REGISTER_LUA_FUNCTION(aLua, "scene_ReloadScene", &ReloadScene);
		REGISTER_LUA_FUNCTION(aLua, "scene_GetSceneNode", &GetSceneNode);
		REGISTER_LUA_FUNCTION(aLua, "scene_GetSceneManager", &GetSceneManager);
	}
}
