#include "lua_utils.h"
#include "luabind_macros.h"
#include "Engine.h"
#include "Graphics/Particles/ParticleManager.h"

using namespace engine::particles;
using namespace base::utils;

namespace lua
{

	CParticleManager& GetParticleManager()
	{
		return engine::CEngine::GetInstance().GetParticleManager();
	}

	template <>
	void BindClass<CParticleSystemType>(lua_State *aLua)
	{
		//BindClass<CTemplatedMapVector<CLayer>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CParticleSystemType,CName)
			LUA_DECLARE_PROP_RW(m_Material, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_MaxParticles, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_NumFrames, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_TimePerFrame, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_LoopFrames, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_EmitAbsolute, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingDirectionAngle, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAccelerationAngle, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_EmitRate, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_AwakeTime, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_SleepTime, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_Life, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAngle, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAngularSpeed, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_AngularAcceleration, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingSpeed1, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingSpeed2, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAcceleration1, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAcceleration2, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_StartingAcceleration2, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_ControlPointColors, CParticleSystemType)
			LUA_DECLARE_PROP_RW(m_ControlPointSizes, CParticleSystemType)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
		class_<CParticleSystemType::ControlPointColor>("ControlPointColor")
		.def_readwrite("m_Time", &CParticleSystemType::ControlPointColor::m_Time)
		.def_readwrite("m_Color1", &CParticleSystemType::ControlPointColor::m_Color1)
		.def_readwrite("m_Color2", &CParticleSystemType::ControlPointColor::m_Color2)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
		class_<CParticleSystemType::ControlPointSize>("ControlPointSize")
		.def_readwrite("m_Time", &CParticleSystemType::ControlPointSize::m_Time)
		.def_readwrite("m_Size", &CParticleSystemType::ControlPointSize::m_Size)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
		class_< std::vector<CParticleSystemType::ControlPointColor> >("ControlPointColorVector")
		.def(constructor<>())
		.def("size", &std::vector<CParticleSystemType::ControlPointColor>::size)
		.def("at", (CParticleSystemType::ControlPointColor&(std::vector<CParticleSystemType::ControlPointColor>::*)(size_t))&std::vector<CParticleSystemType::ControlPointColor>::at)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
		class_< std::vector<CParticleSystemType::ControlPointSize> >("ControlPointSizeVector")
		.def("size", &std::vector<CParticleSystemType::ControlPointSize>::size)
		.def("at", (CParticleSystemType::ControlPointSize&(std::vector<CParticleSystemType::ControlPointSize>::*)(size_t))&std::vector<CParticleSystemType::ControlPointSize>::at)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CTemplatedMap<CParticleSystemType>>(lua_State *aLua)
	{
		BindClass<CParticleSystemType>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMap< CParticleSystemType >)
			LUA_DECLARE_METHOD(CTemplatedMap< CParticleSystemType >, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMap< CParticleSystemType >, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMap< CParticleSystemType >, GetByName)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CParticleManager>(lua_State *aLua)
	{
		BindClass<CTemplatedMap<CParticleSystemType>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CParticleManager, CTemplatedMap<CParticleSystemType>)
			LUA_DECLARE_METHOD(CParticleManager, Save)
			LUA_DECLARE_METHOD(CParticleManager, NewParticle)
			LUA_DECLARE_METHOD(CParticleManager, NewColorControlPoint)
			LUA_DECLARE_METHOD(CParticleManager, NewSizeControlPoint)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "part_GetParticleManager", &GetParticleManager);
	}
}
