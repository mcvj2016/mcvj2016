#include "lua_utils.h"
#include "luabind_macros.h"
#include "Engine.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"

using namespace base::utils;
using namespace engine::cal3d;

namespace lua
{
	CAnimatedModelManager& GetAnimatedModelManager()
	{
		return engine::CEngine::GetInstance().GetAnimatedModelManager();
	}

	template <>
	void BindClass<CalCoreAnimation>(lua_State* aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CalCoreAnimation)
			LUA_DECLARE_METHOD(CalCoreAnimation, getDuration)
			LUA_DECLARE_METHOD(CalCoreAnimation, getName)
			LUA_DECLARE_METHOD(CalCoreAnimation, getTotalNumberOfKeyframes)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CalCoreModel>(lua_State* aLua)
	{
		BindClass<CalCoreAnimation>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CalCoreModel)
			LUA_DECLARE_METHOD(CalCoreModel, getCoreAnimationCount)
			LUA_DECLARE_METHOD(CalCoreModel, getCoreAnimation)
			LUA_DECLARE_METHOD(CalCoreModel, getCoreAnimationId)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CAnimatedCoreModel>(lua_State* aLua)
	{
		BindClass<CalCoreModel>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CAnimatedCoreModel, CName)
			LUA_DECLARE_METHOD(CAnimatedCoreModel, GetCoreModel)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CTemplatedMap<CAnimatedCoreModel>>(lua_State* aLua)
	{
		BindClass<CAnimatedCoreModel>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMap<CAnimatedCoreModel>)
			LUA_DECLARE_METHOD(CTemplatedMap<CAnimatedCoreModel>, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMap<CAnimatedCoreModel>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMap<CAnimatedCoreModel>, GetByIndex)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CAnimatedModelManager>(lua_State* aLua)
	{
		BindClass<CTemplatedMap<CAnimatedCoreModel>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CAnimatedModelManager, CTemplatedMap<CAnimatedCoreModel>)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "cal3d_GetAnimatedModelManager", &GetAnimatedModelManager);
	}
}