#include "lua_utils.h"
#include "luabind_macros.h"
#include "Engine.h"
#include "Camera/ThirdPersonCameraController.h"
#include "Camera/CameraManager.h"
#include "Utils/TemplatedMap.h"
#include "Camera/FirstPersonCameraController.h"

using namespace base::utils;
using namespace engine::camera;
using namespace engine::scenes;

namespace lua
{
	CCameraManager& GetCameraManager()
	{
		return engine::CEngine::GetInstance().GetCameraManager();
	}

	template <>
	void BindClass<CCameraController>(lua_State* aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CCameraController, CSceneNode)
			LUA_DECLARE_METHOD(CCameraController, Update)
			LUA_DECLARE_METHOD(CCameraController, SetFOV)
			LUA_DECLARE_METHOD(CCameraController, GetFOV)
			LUA_DECLARE_METHOD(CCameraController, SetNear)
			LUA_DECLARE_METHOD(CCameraController, GetNear)
			LUA_DECLARE_METHOD(CCameraController, SetFar)
			LUA_DECLARE_METHOD(CCameraController, GetFar)
			LUA_DECLARE_METHOD(CCameraController, SetLookAt)
			LUA_DECLARE_METHOD(CCameraController, GetLookAt)
			LUA_DECLARE_METHOD(CCameraController, SetLock)
			LUA_DECLARE_METHOD(CCameraController, GetLock)
			LUA_DECLARE_METHOD(CCameraController, GetFrustum)
			LUA_DECLARE_METHOD(CCameraController, GetView)
			LUA_DECLARE_METHOD(CCameraController, SetMatrixs)
		LUA_END_DECLARATION
	}

	//Added Juan
	template <>
	void BindClass<CFirstPersonCameraController>(lua_State* aLua)
	{

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CFirstPersonCameraController, CCameraController)
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_METHOD(CFirstPersonCameraController, Update)
			LUA_DECLARE_PROP_RW(m_YawSpeed, CFirstPersonCameraController)
			LUA_DECLARE_PROP_RW(m_PitchSpeed, CFirstPersonCameraController)
			LUA_DECLARE_PROP_RW(m_Speed, CFirstPersonCameraController)
			LUA_DECLARE_PROP_RW(m_RightSpeed, CFirstPersonCameraController)
			LUA_DECLARE_PROP_RW(m_CameraType, CFirstPersonCameraController)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CThirdPersonCameraController>(lua_State* aLua)
	{

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CThirdPersonCameraController, CCameraController)
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_METHOD(CThirdPersonCameraController, Update)
			LUA_DECLARE_PROP_RW(m_YawSpeed, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_PitchSpeed, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_Speed, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_RightSpeed, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_CameraType, CThirdPersonCameraController)

		//Added Juan
			LUA_DECLARE_PROP_RW(m_Center, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_ZoomSpeed, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(m_Zoom, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(maxPitch, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(minPitch, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(maxZoom, CThirdPersonCameraController)
			LUA_DECLARE_PROP_RW(minZoom, CThirdPersonCameraController)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CTemplatedMap<CCameraController>>(lua_State* aLua)
	{
		BindClass<CCameraController>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMap<CCameraController>)
			LUA_DECLARE_METHOD(CTemplatedMap<CCameraController>, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMap<CCameraController>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMap<CCameraController>, GetByIndex)
		LUA_END_DECLARATION
	}

	template <>
	void BindClass<CCameraManager>(lua_State* aLua)
	{
		BindClass<CTemplatedMap<CCameraController>>(aLua);
		BindClass<CThirdPersonCameraController>(aLua);
		BindClass<CFirstPersonCameraController>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CCameraManager, CTemplatedMap<CCameraController>)
			LUA_DECLARE_METHOD(CCameraManager, GetMainCamera)
			LUA_DECLARE_METHOD(CCameraManager, SetMainCamera)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "camera_GetCameraManager", &GetCameraManager);
	}
}
