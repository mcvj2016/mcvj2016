#include "lua_utils.h"
#include "Engine.h"
#include "luabind_macros.h"
#include "Graphics/Textures/TextureManager.h"

using namespace engine::materials;
using namespace base::utils;

namespace lua
{

	CTextureManager& GetTextureManager()
	{
		return engine::CEngine::GetInstance().GetTextureManager();
	}

	//CTexture
	template <> void BindClass<CTexture>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTexture, CName)
			LUA_DECLARE_METHOD(CTexture, GetDynamic)
			LUA_DECLARE_METHOD(CTexture, GetTexture)
		LUA_END_DECLARATION
	}
	//CTemplatedMapVector<CTexture>
	template <> void BindClass<CTemplatedMapVector<CTexture>>(lua_State *aLua)
	{
		BindClass<CTexture>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector<CTexture>)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CTexture>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CTexture>, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CTexture>, GetByName)
		LUA_END_DECLARATION
	}

	template <> void BindClass<CTextureManager>(lua_State *aLua)
	{
		assert(aLua);

		BindClass<CTemplatedMapVector<CTexture>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTextureManager, CTemplatedMapVector<CTexture>)
			LUA_DECLARE_METHOD(CTextureManager, AddTexture)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "tex_GetTextureManager", &GetTextureManager);
	}
}
