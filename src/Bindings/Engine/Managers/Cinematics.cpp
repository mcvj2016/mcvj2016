#include "lua_utils.h"
#include "Engine.h"
#include "luabind_macros.h"
#include "Graphics/Cinematics/Cinematic.h"
#include "Graphics/Cinematics/CinematicManager.h"

using namespace engine::cinematics;
using namespace base::utils;

namespace lua
{

	CCinematicManager& GetCinematicManager()
	{
		return engine::CEngine::GetInstance().GetCinematicManager();
	}

	//CCinematinc
	template <> void BindClass<CCinematic>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CCinematic, CName)
		LUA_END_DECLARATION
	}
	//CTemplatedMapVector<CCinematic>
	template <> void BindClass<CTemplatedMapVector<CCinematic>>(lua_State *aLua)
	{
		BindClass<CCinematic>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector<CCinematic>)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CCinematic>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CCinematic>, GetCount)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CCinematic>, GetByName)
		LUA_END_DECLARATION
	}
	 
	template <> void BindClass<CCinematicManager>(lua_State *aLua)
	{
		assert(aLua);

		BindClass<CTemplatedMapVector<CCinematic>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CCinematicManager, CTemplatedMapVector<CCinematic>)
			LUA_DECLARE_METHOD(CCinematicManager, Play)
			LUA_DECLARE_METHOD(CCinematicManager, Stop)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "cin_GetCinematicManager", &GetCinematicManager);
	}
}
