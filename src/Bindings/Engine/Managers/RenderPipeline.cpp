#include "lua_utils.h"
#include "Engine.h"
#include "luabind_macros.h"
#include "Render/RenderPipelineManager.h"
#include "Render/RenderPipeline.h"

using namespace engine::render;
using namespace base::utils;

namespace lua
{
	void ReloadrenderPipeline()
	{
		engine::CEngine::GetInstance().GetRenderPipelineManager().GetActivePipeline().m_Reloading = true;
	}

	void ChangeRenderPipeline(const std::string& aName)
	{
		engine::CEngine::GetInstance().GetRenderPipelineManager().SetActivePipeline(aName);
	}

	CRenderPipelineManager& GetRenderPipelineManager()
	{
		return engine::CEngine::GetInstance().GetRenderPipelineManager();
	}

	//CRenderCmd
	template <> void BindClass<CRenderCmd>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CRenderCmd, CName)
			LUA_DECLARE_METHOD(CRenderCmd, SetActive)
			LUA_DECLARE_METHOD(CRenderCmd, IsActive)
		LUA_END_DECLARATION
	}
	//CTemplatedMapVector<CRenderCmd>
	template <> void BindClass<CTemplatedMapVector<CRenderCmd>>(lua_State *aLua)
	{
		BindClass<CRenderCmd>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMapVector<CRenderCmd>)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CRenderCmd>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CRenderCmd>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CRenderCmd>, GetCount)
		LUA_END_DECLARATION
	}
	//CRenderPipeline
	template <> void BindClass<CRenderPipeline>(lua_State *aLua)
	{
		BindClass<CTemplatedMapVector<CRenderCmd>>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS2(CRenderPipeline, CTemplatedMapVector<CRenderCmd>, CName)
			LUA_DECLARE_METHOD(CRenderPipeline, GetEnabled)
			LUA_DECLARE_METHOD(CRenderPipeline, SetEnabled)
			LUA_DECLARE_METHOD(CRenderPipeline, Load)
			LUA_DECLARE_METHOD(CRenderPipeline, Reload)
		LUA_END_DECLARATION
	}
	
	//CTemplatedMap<CRenderPipeline>
	template <> void BindClass<CTemplatedMap<CRenderPipeline>>(lua_State *aLua)
	{
		BindClass<CRenderPipeline>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CTemplatedMap<CRenderPipeline>)
			LUA_DECLARE_METHOD(CTemplatedMap<CRenderPipeline>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMap<CRenderPipeline>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMap<CRenderPipeline>, GetCount)
		LUA_END_DECLARATION
	}
	//CRenderPipelineManager
	template <> void BindClass<CRenderPipelineManager>(lua_State *aLua)
	{
		assert(aLua);

		BindClass<CTemplatedMap<CRenderPipeline>>(aLua);
		
		//renderpipelinemanager
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CRenderPipelineManager, CTemplatedMap<CRenderPipeline>)
			LUA_DECLARE_METHOD(CRenderPipelineManager, SetActivePipeline)
			LUA_DECLARE_METHOD(CRenderPipelineManager, GetActivePipeline)
			LUA_DECLARE_METHOD(CRenderPipelineManager, HotLoad)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "render_GetRenderPipelineManager", &GetRenderPipelineManager);
		REGISTER_LUA_FUNCTION(aLua, "render_ReloadRenderPipeline", &ReloadrenderPipeline);
	}
}
