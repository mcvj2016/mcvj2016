#include "lua_utils.h"
#include "Engine.h"
#include "luabind_macros.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Materials/TemplatedMaterialParameter.h"
#include "Math/Color.h"
#include "Graphics/Textures/Texture.h"

using namespace engine::materials;
using namespace base::utils;

namespace lua
{

	CMaterialManager& GetMaterialManager()
	{
		return engine::CEngine::GetInstance().GetMaterialManager();
	}

	void RegisterMaterialClasses(lua_State *aLua)
	{
		assert(aLua);

		//materialparameters
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CMaterialParameter, CName)
			LUA_DECLARE_METHOD(CMaterialParameter, GetType)
		LUA_END_DECLARATION
		//materialparameters vector
		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< std::vector<CMaterialParameter*> >("CMaterialParameterVector")
			.def("size", &std::vector<CMaterialParameter*>::size)
			.def("at", (CMaterialParameter*&(std::vector<CMaterialParameter*>::*)(size_t))&std::vector<CMaterialParameter*>::at)
		LUA_END_DECLARATION
		//templated material parameter
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTemplatedMaterialParameter<float>, CMaterialParameter)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<float>, SetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<float>, GetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<float>, GetType)
		LUA_END_DECLARATION
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTemplatedMaterialParameter<Vect2f>, CMaterialParameter)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect2f>, GetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect2f>, SetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect2f>, GetType)
		LUA_END_DECLARATION
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTemplatedMaterialParameter<Vect3f>, CMaterialParameter)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect3f>, GetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect3f>, SetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect3f>, GetType)
		LUA_END_DECLARATION
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTemplatedMaterialParameter<Vect4f>, CMaterialParameter)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect4f>, GetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect4f>, SetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<Vect4f>, GetType)
		LUA_END_DECLARATION
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CTemplatedMaterialParameter<CColor>, CMaterialParameter)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<CColor>, GetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<CColor>, SetValue)
			LUA_DECLARE_METHOD(CTemplatedMaterialParameter<CColor>, GetType)
		LUA_END_DECLARATION
		//textures vector
		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< std::vector<CTexture*> >("CTexturesVector")
			.def("size", &std::vector<CTexture*>::size)
			.def("at", (CTexture*&(std::vector<CTexture*>::*)(size_t))&std::vector<CTexture*>::at)
		LUA_END_DECLARATION
		//material
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CMaterial, CName)
			LUA_BEGIN_ENUM(TParameterType)
				LUA_ENUM_VALUE(eFloat, 0),
				LUA_ENUM_VALUE(eFloat2, 1),
				LUA_ENUM_VALUE(eFloat3, 2),
				LUA_ENUM_VALUE(eFloat4, 3),
				LUA_ENUM_VALUE(eColor, 4)
			LUA_END_ENUM()
			LUA_DECLARE_METHOD(CMaterial, GetParameters)
			LUA_DECLARE_METHOD(CMaterial, GetTextures)
			LUA_DECLARE_METHOD(CMaterial, AddTexture)
			LUA_DECLARE_METHOD(CMaterial, RemoveTexture)
		LUA_END_DECLARATION
		
		//templatedmapvector of materials
		LUA_BEGIN_DECLARATION(aLua)
		LUA_DECLARE_CLASS(CTemplatedMap<CMaterial>)
			LUA_DECLARE_METHOD(CTemplatedMap<CMaterial>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMap<CMaterial>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMap<CMaterial>, GetCount)
		LUA_END_DECLARATION
		//materialmanager
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CMaterialManager, CTemplatedMap<CMaterial>)
			LUA_DECLARE_METHOD(CMaterialManager, Save)
		LUA_END_DECLARATION
	}

	template <> void BindClass<CMaterialManager>(lua_State *aLua)
	{
		RegisterMaterialClasses(aLua);

		REGISTER_LUA_FUNCTION(aLua, "mat_GetMaterialManager", &GetMaterialManager);
	}
}
