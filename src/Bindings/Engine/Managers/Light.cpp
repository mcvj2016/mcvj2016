#include "lua_utils.h"
#include "Graphics/Lights/Light.h"
#include "luabind_macros.h"
#include "Engine.h"
#include "Graphics/Lights/LightManager.h"
#include "Graphics/Scenes/SceneLight.h" //no borrar al retornar el tipo uno de los metodos

using namespace engine::lights;
using namespace base::utils;

namespace lua
{
	CLightManager& GetLightManager(){
		return engine::CEngine::GetInstance().GetLightManager();
	}

	template <> void BindClass<CLight>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CLight)
			LUA_BEGIN_ENUM(ELightType)
				LUA_ENUM_VALUE(ePoint, 0),
				LUA_ENUM_VALUE(eSpot, 1),
				LUA_ENUM_VALUE(eDirectional, 2)
			LUA_END_ENUM()
			LUA_DECLARE_METHOD(CLight, GetAngle)
			LUA_DECLARE_METHOD(CLight, SetAngle)
			LUA_DECLARE_METHOD(CLight, GetColor)
			LUA_DECLARE_METHOD(CLight, SetColor)
			LUA_DECLARE_METHOD(CLight, GetFallOff)
			LUA_DECLARE_METHOD(CLight, SetFallOff)
			LUA_DECLARE_METHOD(CLight, GetIntensity)
			LUA_DECLARE_METHOD(CLight, SetIntensity)
			LUA_DECLARE_METHOD(CLight, GetSceneLight)
			LUA_DECLARE_METHOD(CLight, IsEnabled)
			LUA_DECLARE_METHOD(CLight, SetEnabled)
			LUA_DECLARE_METHOD(CLight, GetType)
			//LUA_DECLARE_METHOD(CLight, GetRangeAttenuation)
			//LUA_DECLARE_METHOD(CLight, SetRangeAttenuation)
		LUA_END_DECLARATION

		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< std::vector<CLight*> >("CLightVector")
			.def("size", &std::vector<CLight*>::size)
			.def("at", (CLight*&(std::vector<CLight*>::*)(size_t))&std::vector<CLight*>::at)
		LUA_END_DECLARATION
	}

	template <> void BindClass<CTemplatedMapVector<CLight>>(lua_State *aLua)
	{
		BindClass<CLight>(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			luabind::class_< CTemplatedMapVector<CLight> >("CTemplatedMapVector<CLight>")
			LUA_DECLARE_METHOD(CTemplatedMapVector<CLight>, GetByName)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CLight>, GetByIndex)
			LUA_DECLARE_METHOD(CTemplatedMapVector<CLight>, GetCount)
		LUA_END_DECLARATION
	}

	template <> void BindClass<CLightManager>(lua_State *aLua)
	{
		
		BindClass<CTemplatedMapVector<CLight>>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_DERIVED_CLASS(CLightManager, CTemplatedMapVector<CLight>)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "light_GetLightManager", &GetLightManager);
	}
}
