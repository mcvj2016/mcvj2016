#include "lua_utils.h"
#include "Engine.h"
#include "Input/ActionManager.h"
#include "luabind_macros.h"

using namespace engine::input;

namespace lua
{
	CActionManager& GetActionManager()
	{
		return engine::CEngine::GetInstance().GetActionManager();
	}

	CInputManager& GetInputManager()
	{
		return engine::CEngine::GetInstance().GetInputManager();
	}

	template <> void BindLibrary<Input>(lua_State *aLua)
	{
		assert(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CActionManager)
			LUA_DECLARE_METHOD(CActionManager, IsInputActionActive)
			LUA_DECLARE_METHOD(CActionManager, GetInputActionValue)
		LUA_END_DECLARATION

			LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CInputManager)
			LUA_DECLARE_METHOD(CInputManager, IsKeyPressed)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "input_GetInputManager", &GetInputManager);
		REGISTER_LUA_FUNCTION(aLua, "input_GetActionManager", &GetActionManager);
	}
}
