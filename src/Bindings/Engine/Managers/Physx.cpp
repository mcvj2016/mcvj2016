#include "lua_utils.h"
#include "Engine.h"
#include "PhysXImpl/PhysXManager.h"
#include "luabind_macros.h"
#include "Graphics/Scenes/SceneNode.h"
#include "Math/Quaternion.h"

namespace lua
{
	CTransform* GetPhysxTransform(std::string ActorName)
	{
		Vect3f v = v3fZERO;
		Quatf q = qfIDENTITY;
		engine::CEngine::GetInstance().GetPhysXManager().GetActorTransform(ActorName, v, q);

		CTransform* t = new CTransform(v, q.GetYaw(), q.GetPitch(), q.GetRoll());
		return t;
	}

	void SetPhysxPosition(std::string ActorName, Vect3f position)
	{
		engine::CEngine::GetInstance().GetPhysXManager().SetActorPosition(ActorName, position);

	}

	void SetPhysxRotation(std::string ActorName, float yaw, float pitch, float roll)
	{
		engine::CEngine::GetInstance().GetPhysXManager().SetActorRotation(ActorName, yaw, pitch, roll);
	}

	void RecalculatePhysx(std::string ActorName, std::string type)
	{
		engine::CEngine::GetInstance().GetPhysXManager().RecalculatePhysx(ActorName, type);
	}

	void WakeUpDynActor(std::string ActorName)
	{
		engine::CEngine::GetInstance().GetPhysXManager().WakeUpDynActor(ActorName);
	}

	RaycastData RayCast(Vect3f origin, Vect3f end)
	{
		RaycastData result = RaycastData{0};
		engine::CEngine::GetInstance().GetPhysXManager().Raycast(origin, end, &result);
		return result;
	}

	void MoveCharacterController(std::string name, Vect3f dir, float speed, float dt)
	{
		engine::CEngine::GetInstance().GetPhysXManager().MoveCharacter(name, dir, speed, dt);
	}

	engine::scenes::CSceneNode* GetPlayer(std::string name)
	{
		return engine::CEngine::GetInstance().GetPhysXManager().GetPlayer(name);
	}

	template <> void BindClass<CPhysXManager>(lua_State *aLua)
	{
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(base::utils::CTemplatedMapVector< CPhysXManager >)
			LUA_DECLARE_METHOD(CPhysXManager, GetActorTransform)
		LUA_END_DECLARATION
		/*
		 * struct RaycastData
			{
				Vect3f position;
				Vect3f normal;
				float distance;
				std::string actor;
			};
		 */
		LUA_BEGIN_DECLARATION(aLua)
		class_<RaycastData>("RaycastData")
		.def(constructor<>())
		.def_readonly("position", &RaycastData::position)
		.def_readonly("normal", &RaycastData::normal)
		.def_readonly("distance", &RaycastData::distance)
		.def_readonly("actor", &RaycastData::actor)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "physx_GetPhysxTransform", &GetPhysxTransform);
		REGISTER_LUA_FUNCTION(aLua, "physx_SetPhysxPosition", &SetPhysxPosition);
		REGISTER_LUA_FUNCTION(aLua, "physx_SetPhysxRotation", &SetPhysxRotation);
		REGISTER_LUA_FUNCTION(aLua, "physx_RecalculatePhysx", &RecalculatePhysx);
		REGISTER_LUA_FUNCTION(aLua, "physx_WakeUpDynActor", &WakeUpDynActor);
		REGISTER_LUA_FUNCTION(aLua, "physx_RayCast", &RayCast);
		REGISTER_LUA_FUNCTION(aLua, "physx_MoveCharacterController", &MoveCharacterController);
		REGISTER_LUA_FUNCTION(aLua, "physx_GetPlayer", &GetPlayer);
	}
}
