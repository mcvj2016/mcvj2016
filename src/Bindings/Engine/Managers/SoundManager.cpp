#include "lua_utils.h"
#include "luabind_macros.h"
#include "Engine.h"
#include "Sound/ISoundManager.h"

using namespace base::utils;
using namespace engine::sound;

namespace lua
{
	ISoundManager& GetSoundManager()
	{
		return engine::CEngine::GetInstance().GetSoundManager();
	}

	template <>
	void BindClass<ISoundManager>(lua_State* aLua)
	{
		module(aLua)[
			class_<SoundEvent>("SoundEvent")
				.def(constructor<>())
				.def_readwrite("eventName", &SoundEvent::eventName)
		];

		module(aLua)[
			class_<SoundSwitch>("SoundSwitch")
				.def(constructor<>())
				.def_readwrite("switchName", &SoundSwitch::switchName)
		];

		module(aLua)[
			class_<SoundSwitchValue>("SoundSwitchValue")
				.def(constructor<>())
				.def_readwrite("soundSwitch", &SoundSwitchValue::soundSwitch)
				.def_readwrite("valueName", &SoundSwitchValue::valueName)
		];

		module(aLua)[
			class_<SoundRTPC>("SoundRTPC")
				.def(constructor<>())
				.def_readwrite("RTPCName", &SoundRTPC::RTPCName)
		];

		module(aLua)[
			class_<SoundState>("SoundState")
				.def(constructor<>())
				.def_readwrite("stateName", &SoundState::stateName)
		];

		module(aLua)[
			class_<SoundStateValue>("SoundStateValue")
				.def(constructor<>())
				.def_readwrite("soundState", &SoundStateValue::soundState)
				.def_readwrite("valueName", &SoundStateValue::valueName)
		];

		module(aLua)[
			class_<ISoundManager>("ISoundManager")
				.def("SetPath", &ISoundManager::SetPath)
				.def("Update", &ISoundManager::Update)
				.def("Load", &ISoundManager::Load)
				.def("Reload", &ISoundManager::Reload)
				.def("LoadSoundBank", &ISoundManager::LoadSoundBank)
				.def("UnloadSoundBank", &ISoundManager::UnloadSoundBank)
				.def("RegisterSpeaker", &ISoundManager::RegisterSpeaker)
				.def("UnregisterSpeaker", &ISoundManager::UnregisterSpeaker)
				.def("PlayEvent", (void(ISoundManager::*)(const SoundEvent&)) &ISoundManager::PlayEvent)
				.def("PlayEvent", (void(ISoundManager::*)(const SoundEvent&, const std::string&)) &ISoundManager::PlayEvent)
				.def("PlayEvent", (void(ISoundManager::*)(const SoundEvent&, const engine::scenes::CSceneNode*)) &ISoundManager::PlayEvent)
				.def("SetSwitch", (void(ISoundManager::*)(const SoundSwitchValue&)) &ISoundManager::SetSwitch)
				.def("SetSwitch", (void(ISoundManager::*)(const SoundSwitchValue&, const std::string&)) &ISoundManager::SetSwitch)
				.def("SetSwitch", (void(ISoundManager::*)(const SoundSwitchValue&, const engine::scenes::CSceneNode*)) &ISoundManager::SetSwitch)
				.def("BroadcastRTPCValue", (void(ISoundManager::*)(const SoundRTPC&, float)) &ISoundManager::BroadcastRTPCValue)
				.def("SetRTPCValue", (void(ISoundManager::*)(const SoundRTPC&, float)) &ISoundManager::SetRTPCValue)
				.def("SetRTPCValue", (void(ISoundManager::*)(const SoundRTPC&, float, const std::string&)) &ISoundManager::SetRTPCValue)
				.def("SetRTPCValue", (void(ISoundManager::*)(const SoundRTPC&, float, const engine::scenes::CSceneNode*)) &ISoundManager::SetRTPCValue)
				.def("BroadcastState", &ISoundManager::BroadcastState)
		];

		REGISTER_LUA_FUNCTION(aLua, "sound_GetSoundManager", &GetSoundManager);
	}

}
