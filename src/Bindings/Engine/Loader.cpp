#include "lua_utils.h"
#include "Engine.h"
#include "ScriptManager.h"
#include "Graphics/Scenes/SceneManager.h"


namespace lua
{
	void BindLuaLibrary(std::string name)
	{
		lua_State* lLS = engine::CEngine::GetInstance().GetScriptManager().GetState();
		if (name == "Base")
		{
			lua::BindLibrary<lua::Base>(lLS);
		}
		else if (name == "Engine")
		{
			lua::BindLibrary<lua::Engine>(lLS);
		}
		else if (name == "Input")
		{
			lua::BindLibrary<lua::Input>(lLS);
		}
	}

	void ReloadLua()
	{
		engine::CEngine& lEngine = engine::CEngine::GetInstance();

		lEngine.GetScriptManager().m_Reloading = true;
	}

	template <>
	void BindLibrary<Loader>(lua_State* aLua)
	{
		//load all the libraries
		lua::BindLibrary<lua::Base>(aLua);
		lua::BindLibrary<lua::Input>(aLua);
		lua::BindLibrary<lua::Logic>(aLua);
		lua::BindLibrary<lua::Engine>(aLua);

		//register functions
		REGISTER_LUA_FUNCTION(aLua, "loader_ReloadLua", &ReloadLua);
		REGISTER_LUA_FUNCTION(aLua, "loader_BindLuaLibrary", &BindLuaLibrary);
	}
}
