#include "lua_utils.h"
#include "Engine.h"
#include "luabind_macros.h"
#include "Camera/CameraManager.h"
#include "Graphics/Cinematics/CinematicManager.h"
#include "Graphics/Textures/TextureManager.h"
#include "Graphics/Materials/MaterialManager.h"
#include "Graphics/Lights/LightManager.h"
#include "PhysXImpl/PhysXManager.h"
#include "Render/RenderPipelineManager.h"
#include "Helper/ImguiHelper.h"
#include "Graphics/Scenes/SceneManager.h"
#include "Graphics/Cal3d/AnimatedModelManager.h"
#include "Graphics/Particles/ParticleManager.h"
#include "Sound/ISoundManager.h"
#include "Input/ActionManager.h"

using namespace engine;
using namespace base::utils;

namespace lua
{
	template <> void BindLibrary<Engine>(lua_State *aLua)
	{
		BindClass<scenes::CSceneManager>(aLua);
		BindClass<camera::CCameraManager>(aLua);
		BindClass<cinematics::CCinematicManager>(aLua);
		BindClass<lights::CLightManager>(aLua);
		BindClass<materials::CTextureManager>(aLua);
		BindClass<materials::CMaterialManager>(aLua); //loads materialmanager, material and materialparameter
		BindClass<CPhysXManager>(aLua);
		BindClass<render::CRenderPipelineManager>(aLua);
		BindClass<CImguiHelper>(aLua);
		BindClass<engine::cal3d::CAnimatedModelManager>(aLua);
		BindClass<particles::CParticleManager>(aLua);
		BindClass<sound::ISoundManager>(aLua);

		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CEngine)
			.scope[
				def("GetInstance", &CEngine::GetInstance)
			]
			LUA_DECLARE_METHOD(CEngine, GetDeltaTime)
			.def("GetCameraManager", (camera::CCameraManager&(CEngine::*)()) &CEngine::GetCameraManager)
			.def("GetCinematicManager", (cinematics::CCinematicManager&(CEngine::*)()) &CEngine::GetCinematicManager)
			.def("GetLightManager", (lights::CLightManager&(CEngine::*)()) &CEngine::GetLightManager)
			.def("GetTextureManager", (materials::CTextureManager&(CEngine::*)()) &CEngine::GetTextureManager)
			.def("GetMaterialManager", (materials::CMaterialManager&(CEngine::*)()) &CEngine::GetMaterialManager)
			.def("GetPhysXManager", (CPhysXManager&(CEngine::*)()) &CEngine::GetPhysXManager)
			.def("GetRenderPipelineManager", (render::CRenderPipelineManager&(CEngine::*)()) &CEngine::GetRenderPipelineManager)
			.def("GetImGUI", (CImguiHelper&(CEngine::*)()) &CEngine::GetImGUI)
			.def("GetSceneManager", (scenes::CSceneManager&(CEngine::*)()) &CEngine::GetSceneManager)
			.def("GetAnimatedModelManager", (engine::cal3d::CAnimatedModelManager&(CEngine::*)()) &CEngine::GetAnimatedModelManager)
			.def("GetParticleManager", (particles::CParticleManager&(CEngine::*)()) &CEngine::GetParticleManager)
			.def("GetSoundManager", (sound::ISoundManager&(CEngine::*)()) &CEngine::GetSoundManager)
			.def("GetInputManager", (input::CInputManager&(CEngine::*)()) &CEngine::GetInputManager)
			.def("GetActionManager", (input::CActionManager&(CEngine::*)()) &CEngine::GetActionManager)

		LUA_DECLARE_PROP_RW(m_ExitGame, CEngine)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "engine_GetEngine", &CEngine::GetInstance);
		
	}
}
