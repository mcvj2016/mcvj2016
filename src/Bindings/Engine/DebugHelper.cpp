#include "lua_utils.h"
#include "Utils/Imgui/imgui.h"
#include "Engine.h"
#include "luabind_macros.h"

using namespace engine;

namespace lua
{
	CImguiHelper& GetImguiHelper()
	{
		return CEngine::GetInstance().GetImGUI();
	}

	void Log(const std::string& str)
	{
		CEngine::GetInstance().GetImGUI().Log(str);
	}

	template <> void BindClass<CImguiHelper>(lua_State *aLua)
	{
		assert(aLua);
		LUA_BEGIN_DECLARATION(aLua)
			LUA_DECLARE_CLASS(CImguiHelper)
			LUA_DECLARE_DEFAULT_CTOR
			LUA_DECLARE_METHOD(CImguiHelper, AddButton)
			LUA_DECLARE_METHOD(CImguiHelper, AddCheckbox)
			LUA_DECLARE_METHOD(CImguiHelper, AddSeparator)
			LUA_DECLARE_METHOD(CImguiHelper, AddTextLine)
			LUA_DECLARE_METHOD(CImguiHelper, Log)
			LUA_DECLARE_METHOD(CImguiHelper, DrawLog)
			LUA_DECLARE_METHOD(CImguiHelper, RadioButton)
			LUA_DECLARE_METHOD(CImguiHelper, NewWindow)
			LUA_DECLARE_METHOD(CImguiHelper, FullScreenWindow)
			LUA_DECLARE_METHOD(CImguiHelper, EndWindow)
			LUA_DECLARE_METHOD(CImguiHelper, SetInline)
			LUA_DECLARE_METHOD(CImguiHelper, ShowFrameRate)
			LUA_DECLARE_METHOD(CImguiHelper, BeginMainMenuBar)
			LUA_DECLARE_METHOD(CImguiHelper, EndMainMenuBar)
			LUA_DECLARE_METHOD(CImguiHelper, BeginMenu)
			LUA_DECLARE_METHOD(CImguiHelper, EndMenu)
			LUA_DECLARE_METHOD(CImguiHelper, MenuItem)
			LUA_DECLARE_METHOD(CImguiHelper, TreeNode)
			LUA_DECLARE_METHOD(CImguiHelper, EndTreeNode)
			LUA_DECLARE_METHOD(CImguiHelper, DragFloat)
			LUA_DECLARE_METHOD(CImguiHelper, DragFloatMinMax)
			LUA_DECLARE_METHOD(CImguiHelper, InputFloat3)
			LUA_DECLARE_METHOD(CImguiHelper, ColorEdit4)
			LUA_DECLARE_METHOD(CImguiHelper, IsLastItemActive)
			LUA_DECLARE_METHOD(CImguiHelper, ImGuiRender)
			LUA_DECLARE_METHOD(CImguiHelper, DragInt)
			LUA_DECLARE_METHOD(CImguiHelper, DragFloat2)
			LUA_DECLARE_METHOD(CImguiHelper, DragFloat3)
			LUA_DECLARE_METHOD(CImguiHelper, Image)
			LUA_DECLARE_METHOD(CImguiHelper, FullScreenImage)
			LUA_DECLARE_METHOD(CImguiHelper, ImageButton)
			LUA_DECLARE_METHOD(CImguiHelper, DisableWindowBackGroundColor)
			LUA_DECLARE_METHOD(CImguiHelper, GetCursorPosX)
			LUA_DECLARE_METHOD(CImguiHelper, SetNextWindowPos)
			LUA_DECLARE_METHOD(CImguiHelper, NewWindowWithFlags)
			LUA_DECLARE_METHOD(CImguiHelper, IsItemHovered)
			LUA_DECLARE_METHOD(CImguiHelper, SetInlineRight)
		LUA_END_DECLARATION

		REGISTER_LUA_FUNCTION(aLua, "imgui_GetDebugHelper", &GetImguiHelper);
		REGISTER_LUA_FUNCTION(aLua, "imgui_Log", &Log);
	}
}
