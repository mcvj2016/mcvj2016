/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_ATTACK_ORC = 2183147485U;
        static const AkUniqueID PLAY_CURSE_GOB = 1937424673U;
        static const AkUniqueID PLAY_DIE_GOB = 544260585U;
        static const AkUniqueID PLAY_DIE_PLAYER = 1602130624U;
        static const AkUniqueID PLAY_FALL_ORC = 2977262090U;
        static const AkUniqueID PLAY_FIREIDLELOOP = 4107595094U;
        static const AkUniqueID PLAY_FIREIN_M1_PLAYER = 3037539280U;
        static const AkUniqueID PLAY_FOOTSPETS_PLAYER = 508827085U;
        static const AkUniqueID PLAY_GULAKCHARGE = 3238090874U;
        static const AkUniqueID PLAY_GULAKDIE = 354734320U;
        static const AkUniqueID PLAY_HAND_M1_PLAYER = 668486350U;
        static const AkUniqueID PLAY_HEAL_GOB = 2591670473U;
        static const AkUniqueID PLAY_HIT_GULAK = 1607581900U;
        static const AkUniqueID PLAY_HIT_ORC = 1026703606U;
        static const AkUniqueID PLAY_HIT_PLAYER = 2257378511U;
        static const AkUniqueID PLAY_HOVERBUTTON = 3130647524U;
        static const AkUniqueID PLAY_MAGICPROJECTILE_PLAYER = 199961546U;
        static const AkUniqueID PLAY_MAINMENU = 3738780720U;
        static const AkUniqueID PLAY_MAINTRACKS = 4261127901U;
        static const AkUniqueID PLAY_OPENING = 1808131806U;
        static const AkUniqueID PLAY_ORCDIE = 3957710818U;
        static const AkUniqueID PLAY_ROCKFALL = 1120692842U;
        static const AkUniqueID PLAY_SWORDHIT_PLAYER = 2608427972U;
        static const AkUniqueID PLAY_SWORDSLASH_ORC = 1552855435U;
        static const AkUniqueID PLAY_SWORDSLASH_PLAYER = 3298055556U;
        static const AkUniqueID PLAY_TOTEMRUPTURE = 2136190704U;
        static const AkUniqueID STOP_CURSE_GOB = 1354679255U;
        static const AkUniqueID STOP_HEAL_GOB = 3460061695U;
        static const AkUniqueID STOP_MAINMENU = 890527358U;
        static const AkUniqueID STOP_OPENING = 760928136U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MAINTRACKSTATES
        {
            static const AkUniqueID GROUP = 2022393197U;

            namespace STATE
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID ENDING = 3966194980U;
                static const AkUniqueID EXPLORING = 1823678183U;
                static const AkUniqueID FINALBATTLE = 3203772045U;
                static const AkUniqueID ZONE4 = 831766777U;
            } // namespace STATE
        } // namespace MAINTRACKSTATES

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID EFFECTS = 1942696649U;
        static const AkUniqueID MAINMENU = 3604647259U;
        static const AkUniqueID UI = 1551306167U;
        static const AkUniqueID ZONES = 831766718U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
