fn existFile fname = (getfiles fname).count != 0

fn printLn str = (
	format "%\n" str
)

fn ParseColorValue p = (
	lValue = p as string
	lAlpha = p.a as string
	if (matchPattern lValue pattern:"(color*") == true then
	(
		-- eliminamos principio de parentesis
		lValue = substituteString lValue "(" ""
		-- eliminamos "color "
		lValue = substring lValue 7 -1;
		-- eliminamos final de parentesis
		lValue = substituteString lValue ")" ""
		-- anyadimos ultimo parametro al color
		
		lValue = lValue + " " + lAlpha
		
		--split por espacios para dividir por 255 cada componente y de paso pasarla a float
		local splited = filterString lValue " "
		lValue = ""
		for v in splited do 
		(
			lValue = lValue + (((v as float) / 255) as string) + " "
		)
		lValue = trimright lValue
	)
	return lValue
)

fn ParseFloat4Value iVal = (
	lValue = (iVal.x as string + " ") + (iVal.y as string + " ") + (iVal.z as string + " ") + (iVal.w as string)

	return (lValue as string)
)

fn ParseFloat3Value iVal = (
	lValue = (iVal.x as string + " ") + (iVal.y as string + " ") + (iVal.z as string)

	return (lValue as string)
)

fn ParseFloat2Value iVal = (
	lValue = (iVal.x as string + " ") + (iVal.y as string)

	return (lValue as string)
)

fn getRelativePath &pathString = 
(
	println pathString
	local startFrom = (findString pathString "Data")
	pathString = substring pathString startFrom (pathString.count-startFrom+1)
)

fn checkUserProp usrProp &usrDefined =
(
	for Obj in $* do
	(
		if (getUserProp Obj usrProp) == true then append usrDefined Obj
	)
)

fn checkUserPropValue Objs usrProp usrPropValue &usrDefined =
(
	for Obj in Objs do
	(
		if (getUserProp Obj usrProp) == usrPropValue then append usrDefined Obj
	)
)

fn dirExist dir =
(
	if (doesFileExist dir) == false then
		makeDir dir all:true
)

fn getCores Objs &cores=
(
	local areInstances
	append cores Objs[1]
	
	for l_Obj in Objs do
	(
		areInstances = false
		for l_Core in cores do
		(
			if ((AreNodesInstances l_Obj l_Core) == true) then
			(
				areInstances = true
				setUserProp l_Obj "core_name" l_Core.name
			)
		)
		if areInstances == false then
		(
			append cores l_Obj
			setUserProp l_Obj "core_name" l_Obj.name
		)
	)
)

struct PerformanceTester
(
    iStartMem = 0,
    iStartTime = 0,
    iStopMem = 0,
    iStopTime = 0,
    
    iCycle = 0,
    
    iResTime = 0,
    iResMem = 0,
    
    function start =
    (
        gc()
        iCycle += 1
        iStartMem = (heapSize - heapFree)
        iStartTime = timeStamp()    
    ),
    
    function stop =
    (
        iStopTime = timeStamp()
        iStopMem = (heapSize - heapFree)

        iResTime += ((iStopTime - iStartTime) / 1000.0)
        iResMem += ((iStopMem - iStartMem) / 1024.0)
    ),
    
    function report =
    (
		
		format "\n------------CLOCK----------------\n"
        format "Total: %sec & %Kb\n" iResTime iResMem
		if iCycle > 1 then
		(
			format "Avg: %sec & %Kb (% its)\n" (iResTime / iCycle) (iResMem / iCycle) iCycle
		)
        format "---------------------------------\n\n"
		
        iCycle = 0
    
        iResTime = 0
        iResMem = 0
    )
)

function SwapPoint3YZ iPoint3 = (
	
	local lPoint3 = point3 iPoint3.x iPoint3.z iPoint3.y
	
	return lPoint3
)
