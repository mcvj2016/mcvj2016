fileIn "math.ms"
fileIn "utils.ms"

struct CLight (
	m_Name,
	m_Type,
	m_Intensity,
	m_Enabled = true,
	m_Color,
	m_AttenuationRange,
	m_FallOff,
	m_Angle,
	m_GenerateShadowMap = false,
	m_ShadowMapHeight,
	m_ShadowMapWidth,
	m_ShadowMapFormat = "R32_FLOAT"
)

fn createLights dir filename Objs =
(

	local xmlDoc = XmlDocument()
	local rootNode = XmlNode tag:"lights"
	xmlDoc.rootNode = rootNode

	local lightPos
	local lightDir
	--file=createfile (dir+filename)
	xmlDoc.filename = (dir+filename)

	lLights = #()
	
	for l in Objs do
	(
		local lightNode = XmlNode tag:"light"

		local lightPos = l.pos
		TranslationToRH &lightPos
		local lightDir = -l.dir
		TranslationToRH &lightDir	
		pos = ParseFloat3Value(lightPos)
		dir = ParseFloat3Value(lightDir)

		--common properties
		local lLight = CLight()

		lLight.m_Name = l.name
		lLight.m_Enabled = getuserprop l "light_enabled"
		lLight.m_Color = ParseColorValue(l.rgb)
		
		lLight.m_Intensity = l.multiplier

		--export light data
		lightNode.AddAttribute "name" lLight.m_Name
		lightNode.AddAttribute "enabled" lLight.m_Enabled
		lightNode.AddAttribute "intensity" lLight.m_Intensity
		lightNode.AddAttribute "color" lLight.m_Color
		
		lightNode.AddAttribute "position" pos
		lightNode.AddAttribute "forward" dir
		
		if (ClassOf l == freeSpot or ClassOf l == targetSpot) then
		(
			l_angle = l.hotspot * (PI/180)
			l_falloff = l.falloff * (PI/180)
			lLight.m_Type = "spot"
			lLight.m_FallOff = l_falloff
			lLight.m_Angle = l_angle
			lLight.m_AttenuationRange = point2 l.farAttenStart l.farAttenEnd

			lightNode.AddAttribute "type" lLight.m_Type
			lightNode.AddAttribute "angle" lLight.m_Angle
			lightNode.AddAttribute "fall_off" lLight.m_FallOff
			attr = ParseFloat2Value(lLight.m_AttenuationRange)
			lightNode.AddAttribute "atenuation_range" attr

			if (getUserProp l "shadowmap_export" == true) then
			(
				lLight.m_GenerateShadowMap = true
				lLight.m_ShadowMapWidth = (getUserProp l "shadowmap_w")
				lLight.m_ShadowMapHeight = (getUserProp l "shadowmap_h")
				
				lightNode.AddAttribute "shadow_map_width" lLight.m_ShadowMapWidth
				lightNode.AddAttribute "shadow_map_height" lLight.m_ShadowMapHeight
				lightNode.AddAttribute "shadow_map_format" lLight.m_ShadowMapFormat

				local layersNode = XmlNode tag:"layers"
				local layerArr = execute (getUserProp l "shadowmap_layers")

				for layer in layerArr do
				(
					local layerNode = XmlNode tag:"layer"

					layerNode.AddAttribute "name" layer
					layersNode.AddChild layerNode
				)

				lightNode.AddChild layersNode
				
			)
			lightNode.AddAttribute "generate_shadow_map" lLight.m_GenerateShadowMap
		)
		else if (ClassOf l == Directionallight or ClassOf l == TargetDirectionallight) then
		(
			lLight.m_Type = "directional"
			

			lightNode.AddAttribute "type" lLight.m_Type
			

			if (getUserProp l "shadowmap_export" == true) then
			(
				lLight.m_GenerateShadowMap = true
				lLight.m_ShadowMapWidth = (getUserProp l "shadowmap_w")
				lLight.m_ShadowMapHeight = (getUserProp l "shadowmap_h")

				lightNode.AddAttribute "shadow_map_width" lLight.m_ShadowMapWidth
				lightNode.AddAttribute "shadow_map_height" lLight.m_ShadowMapHeight
				lightNode.AddAttribute "shadow_map_format" lLight.m_ShadowMapFormat

				local layersNode = XmlNode tag:"layers"
				local layerArr = execute (getUserProp l "shadowmap_layers")

				for layer in layerArr do
				(
					local layerNode = XmlNode tag:"layer"

					layerNode.AddAttribute "name" layer
					layersNode.AddChild layerNode
				)

				lightNode.AddChild layersNode
			)
			lightNode.AddAttribute "generate_shadow_map" lLight.m_GenerateShadowMap
			orthosize = point2 30 80
			attr = ParseFloat2Value(orthosize)
			lightNode.AddAttribute "ortho_shadow_map_size" attr --hardcoded
		)
		else if (ClassOf l == Omnilight) then
		(
			attrange = point2 l.farAttenStart l.farAttenEnd

			attr = ParseFloat2Value(attrange)
			lightNode.AddAttribute "atenuation_range" attr
			lightNode.AddAttribute "type" "point"
		) 

		rootNode.AddChild lightNode
		append lLights lLight
	)

	xmlDoc.SaveXmlDocument()

	return lLights
)
