fileIn "math.ms"
fileIn "utils.ms"

fn createCameras dir Objs =
(
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"cameras"
	xmlDoc.rootNode = rootNode
	xmlDoc.filename = (dir+"cameras.xml")

	local camNode = XmlNode tag:"camera"
	rootNode.AddChild camNode
	camNode.AddAttribute "name" "MainCamera"
	camNode.AddAttribute "type" "THIRD_PERSON"
	camNode.AddAttribute "main" "false"

	local transformNode = XmlNode tag:"transform"
	camNode.AddChild transformNode
	transformNode.AddAttribute "fov" "45.0"
	transformNode.AddAttribute "near_plane" "1.0"
	transformNode.AddAttribute "far_plane" "2000.0"

	camNode = XmlNode tag:"camera"
	rootNode.AddChild camNode
	camNode.AddAttribute "name" "DebugCamera"
	camNode.AddAttribute "type" "FIRST_PERSON"
	camNode.AddAttribute "main" "true"

	transformNode = XmlNode tag:"transform"
	camNode.AddChild transformNode
	transformNode.AddAttribute "fov" "45.0"
	transformNode.AddAttribute "near_plane" "1.0"
	transformNode.AddAttribute "far_plane" "2000.0"

	local scriptNode = XmlNode tag:"script_object"
	camNode.AddChild scriptNode
	scriptNode.AddAttribute "name" "FirstPersonCamera"	

	if Objs != undefined then
	(
		local cameraFileDir
		local cameraTransform
		local cameraTranslation
		local cameraRotation
		local cameraPos
		local cameraUp
		local cameraLookAt
		local cameraFieldOfView
		local key
		local cameraType
		
		for i=1 to Objs.count do
		(
			select Objs[i]
			cameraTransform = Objs[i].transform
			cameraTranslation = cameraTransform.translation
			cameraRotation = cameraTransform as eulerangles
			cameraUp = Objs[i].transform[2]
			--mat3xvect3 cameraTransform z_axis &cameraUp
			cameraType = getUserProp Objs[i] "camera_export_type"

			if(Objs[i].target != undefined) then
			(
				cameraLookAt = Objs[i].target.pos
			) else (
				mat3xvect3 cameraTransform (-y_axis) &cameraLookAt
				checkZeroes &cameraLookAt
				swapYZ &cameraLookAt
				cameraLookAt += cameraTranslation
			)
			cameraUp = SwapPoint3YZ cameraUp
			--TranslationToRH &cameraUp
			TranslationToRH &cameraLookAt
			TranslationToRH &cameraTranslation
			cameraFieldOfView = Objs[i].fov

			local firstframe = at time 999999999999 trackbar.getNextKeyTime()

			camNode = XmlNode tag:"camera"
			rootNode.AddChild camNode
			camNode.AddAttribute "name" Objs[i].name
			camNode.AddAttribute "type" cameraType
			camNode.AddAttribute "main" false
			local nearPlane = Objs[i].nearrange
			if (nearPlane == 0.0) then (
				nearPlane = 1.0
			)

			transformNode = XmlNode tag:"transform"
			camNode.AddChild transformNode
			local camPos = ParseFloat3Value(cameraTranslation)
			local camLookAt = ParseFloat3Value(cameraLookAt)
			local camUp = ParseFloat3Value(cameraUp)
			transformNode.AddAttribute "position" camPos
			transformNode.AddAttribute "look_at" camLookAt
			transformNode.AddAttribute "up" camUp
			transformNode.AddAttribute "fov" cameraFieldOfView
			transformNode.AddAttribute "near_plane" nearPlane
			transformNode.AddAttribute "far_plane" Objs[i].farrange

			if(cameraType == "FIRST_PERSON") then
			(
				scriptNode = XmlNode tag:"script_object"
				camNode.AddChild scriptNode
				scriptNode.AddAttribute "name" "FirstPersonCamera"
			)


			if (firstframe != undefined) and ((getUserProp Objs[i] "camera_export_type") != "static") then (
				key = (at time 999999999999 trackbar.getPreviousKeyTime()) as string
				key = replace key key.count 1 ""
				key = (key as float)/30		
				dirExist (dir+"cameras\\")
				cameraFileDir = ("cameras\\"+Objs[i].name+".xml")

				local j = firstframe
				cameraFile=createfile (dir+cameraFileDir)
				format "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" to:cameraFile
				format "<cinematic_camera_player camera=\"%\">\n" Objs[i].name to:cameraFile
				do (
					key = j as string	
					key = replace key key.count 1 ""
					key = (key as float)/30
					cameraTransform = at time j Objs[i].transform
					cameraTranslation = cameraTransform.translation
					cameraRotation = cameraTransform as eulerangles
					--mat3xvect3 cameraTransform z_axis &cameraUp
					cameraUp = at time j Objs[i].transform[2]
					--checkZeroes &CameraUp
					if(Objs[i].target != undefined) then
					(
						cameraLookAt = at time j Objs[i].target.pos
					) else (
						mat3xvect3 cameraTransform (-y_axis) &cameraLookAt
						checkZeroes &cameraLookAt
						swapYZ &cameraLookAt
						cameraLookAt += cameraTranslation
					)
					--cameraUp = SwapPoint3YZ cameraUp
					TranslationToRH &cameraUp
					TranslationToRH &cameraLookAt
					TranslationToRH &cameraTranslation
					cameraFieldOfView = at time j Objs[i].fov
					nearPlane = at time j Objs[i].nearrange
					if (nearPlane == 0.0) then (
						nearPlane = 1.0
					)

					format "\t<key time=\"%\" position=\"% % %\" up=\"% % %\" look_at=\"% % %\" fov=\"%\" near_plane=\"%\" far_plane=\"%\"/>\n" key cameraTranslation.x cameraTranslation.y cameraTranslation.z cameraUp.x cameraUp.y cameraUp.z cameraLookAt.x cameraLookAt.y cameraLookAt.z cameraFieldOfView nearPlane (at time j Objs[i].farrange) to:cameraFile
					j = at time j trackbar.getNextKeyTime()
				) while j != firstframe
				format "</cinematic_camera_player>\n" to:cameraFile		
				close cameraFile
			)
			max select none
		)		
	)
	xmlDoc.SaveXmlDocument()
)
