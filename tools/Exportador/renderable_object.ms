fileIn "math.ms"
fileIn "utils.ms"

fn createInstanceMesh dir filename Objs=
(	
	local pos
	local rot
	local core

	local lOpaque = #()
	local lAnimated_models = #()
	local lCinematics = #()
	local lCut_out = #()
	local lTransparent = #()
	local lParticles = #()
	local lTriggers = #()
	local lLights = #()
	
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"scene"
	xmlDoc.rootNode = rootNode
	xmlDoc.filename = (dir+filename)

	--file=createfile (dir+filename)
	--format "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" to:file
	--format "<scene>\n" to:file
	--format "\t<layer name=\"solid\" default=\"true\"/>\n" to:file
	--format "\t<layer name=\"alpha_objects\"/>\n" to:file
	--format "\t<layer name=\"alpha_blend_objects\"/>\n" to:file
	--format "\t<layer name=\"particles\"/>\n" to:file
	--format "\t<layer name=\"triggers\"/>\n" to:file
	--format "\t<layer name=\"manchas\"/>\n" to:file
	--format "\t<layer name=\"skybox\"/>\n" to:file

	--creamos nodos de layers
	local layerOpaque = XmlNode tag:"layer"
	layerOpaque.AddAttribute "name" "opaque"
	rootNode.AddChild layerOpaque
	
	local layerAnimated = XmlNode tag:"layer"
	layerAnimated.AddAttribute "name" "animated_models"
	rootNode.AddChild layerAnimated
	
	local layerSkyBox = XmlNode tag:"layer"
	layerSkyBox.AddAttribute "name" "skybox"
	rootNode.AddChild layerSkyBox
	
	local layerTransparent = XmlNode tag:"layer"
	layerTransparent.AddAttribute "name" "transparent"
	rootNode.AddChild layerTransparent
	
	local layerParticle = XmlNode tag:"layer"
	layerParticle.AddAttribute "name" "particles"
	rootNode.AddChild layerParticle
	
	local layerTrigger = XmlNode tag:"layer"
	layerTrigger.AddAttribute "name" "triggers"
	rootNode.AddChild layerTrigger

	if Objs != undefined then
	(
		for o in Objs do
		(
			--build layer arrays
			if (getUserProp o "render_layer") == "opaque" then 
				append lOpaque o
			if (getUserProp o "render_layer") == "triggers" then 
				append lTriggers o
		)

		for o in lOpaque do
		(
			local pos = o.transform.translation
			TranslationToRH &pos
			pos = ParseFloat3Value(pos)
			local rot = o.rotation
			QuatToRH &rot 
      		rot = point4 rot.x rot.y rot.z rot.w
			rot = ParseFloat4Value(rot)
			local lScale = ParseFloat3Value(o.scale)
			local core = (getUserProp o "core_name") + ".bin"

			local scneNode = XmlNode tag:"scene_mesh_quat"
			layerOpaque.AddChild scneNode

			local transformNode = XmlNode tag:"transform"
			scneNode.AddChild transformNode

			scneNode.AddAttribute "name" o.name
			scneNode.AddAttribute "mesh" core
			scneNode.AddAttribute "visible" (getUserProp o "render_visible")
			--debug
			--scneNode.AddAttribute "frustum_ignored" true

			transformNode.AddAttribute "position" pos
			transformNode.AddAttribute "rotation" rot
			transformNode.AddAttribute "scale" lScale

			if (getUserProp o "physics_generate") then
			(
				local scenePhysx = XmlNode tag:"physx"
				scneNode.AddChild scenePhysx

				if ((getUserProp o "physics_generate_type") == "plane_shape") then
				(
					local lplaneNormal = point3 (getUserProp o "plane_normal_x") (getUserProp o "plane_normal_y") (getUserProp o "plane_normal_z")
					
					if(lplaneNormal.y == 0) then(
						lplaneNormal.y = 1
					)

					lplaneNormal = ParseFloat3Value(lplaneNormal)

					scenePhysx.AddAttribute "mesh_type" "plane_shape"
					scenePhysx.AddAttribute "static" (getUserProp o "physics_static")
					scenePhysx.AddAttribute "physics_offset" (getUserProp o "plane_offset")
					scenePhysx.AddAttribute "physics_normal" lplaneNormal
					scenePhysx.AddAttribute "physics_material" (getUserProp o "physics_material")
					scenePhysx.AddAttribute "physics_group" (getUserProp o "physics_layer")
					scenePhysx.AddAttribute "visible" true
					scenePhysx.AddAttribute "density" 1
				) else (
					scenePhysx.AddAttribute "mesh_type" (getUserProp o "physics_generate_type") 
					scenePhysx.AddAttribute "static" (getUserProp o "physics_static")
					scenePhysx.AddAttribute "physics_material" (getUserProp o "physics_material")
					scenePhysx.AddAttribute "physics_group" (getUserProp o "physics_layer")
					scenePhysx.AddAttribute "visible" true
					scenePhysx.AddAttribute "density" 1
				)
			)
		)
		
		for o in lTriggers do
		(
			local pos = o.transform.translation
			TranslationToRH &pos
			pos = ParseFloat3Value(pos)
			local rot = o.rotation
			QuatToRH &rot 
      		rot = point4 rot.x rot.y rot.z rot.w
			rot = ParseFloat4Value(rot)
			local lScale = ParseFloat3Value(o.scale)
			local core = (getUserProp o "core_name") + ".bin"

			local scneNode = XmlNode tag:"scene_mesh"
			layerTrigger.AddChild scneNode

			local transformNode = XmlNode tag:"transform"
			scneNode.AddChild transformNode

			local l_TriggerEnterString
			local l_TriggerExitString
			local l_TriggerStayString
			if (getUserProp o "on_trigger_enter") == "" then
			(
				l_TriggerEnterString = ""
			) else (
				l_TriggerEnterString = (getUserProp o "on_trigger_enter")
			)
			if (getUserProp o "on_trigger_exit") == ""  then
			(
				l_TriggerExitString = ""
			) else (
				l_TriggerExitString = (getUserProp o "on_trigger_exit")
			)
			if (getUserProp o "on_trigger_stay") == "" then
			(
				l_TriggerStayString = ""
			) else (
				l_TriggerStayString = (getUserProp o "on_trigger_stay")
			)

			scneNode.AddAttribute "name" o.name
			scneNode.AddAttribute "mesh" core
			transformNode.AddAttribute "position" pos
			transformNode.AddAttribute "rotation" rot
			transformNode.AddAttribute "scale" lScale
			scneNode.AddAttribute "on_trigger_enter" l_TriggerEnterString
			scneNode.AddAttribute "on_trigger_exit" l_TriggerExitString
			scneNode.AddAttribute "on_trigger_stay" l_TriggerStayString
			scneNode.AddAttribute "material" (getUserProp o "physics_material")
			scneNode.AddAttribute "type" (getUserProp o "physics_generate_type")
			scneNode.AddAttribute "group" (getUserProp o "physics_layer")
			scneNode.AddAttribute "visible" (getUserProp o "render_visible")
		)

		--create the skybox
		local skyboxNode = XmlNode tag:"scene_mesh"
		layerSkyBox.AddChild skyboxNode

		skyboxNode.AddAttribute "mesh" "SkyBox.bin"
		skyboxNode.AddAttribute "frustum_ignored" "true"
		
		local skyBoxTransformNode = XmlNode tag:"transform"
		skyBoxTransformNode.AddAttribute "position" "0.0 0.0 0.0"
		skyBoxTransformNode.AddAttribute "rotation" "0.0 0.0 0.0 0.0"
		skyBoxTransformNode.AddAttribute "scale" "1.0 1.0 1.0"

		skyboxNode.AddChild skyBoxTransformNode

		/*pos = Objs[i].transform.translation
		TranslationToRH &pos
		rot = Objs[i].rotation
		QuatToRH &rot
		if (getUserProp Objs[i] "render_layer") == "triggers" then
		(
			core = (getUserProp Objs[i] "core_name")
			local l_TriggerEnterString
			local l_TriggerExitString
			local l_TriggerStayString
			if (getUserProp Objs[i] "on_trigger_enter") == "" then
			(
				l_TriggerEnterString = ""
			) else (
				l_TriggerEnterString = "on_trigger_enter=\""+(getUserProp Objs[i] "on_trigger_enter")+"\" "
			)
			if (getUserProp Objs[i] "on_trigger_exit") == ""  then
			(
				l_TriggerExitString = ""
			) else (
				l_TriggerExitString = "on_trigger_exit=\""+(getUserProp Objs[i] "on_trigger_exit")+"\" "
			)
			if (getUserProp Objs[i] "on_trigger_stay") == "" then
			(
				l_TriggerStayString = ""
			) else (
				l_TriggerStayString = "on_trigger_stay=\""+(getUserProp Objs[i] "on_trigger_stay")+"\" "
			)
			format "\t<instance_mesh name=\"%\" layer=\"triggers\" %%%is_active=\"true\" core_name=\"%\" position=\"% % %\" rotation=\"% % % %\" create_physics=\"true\" physics_type=\"%\" physics_material=\"%\" physics_group=\"%\" visible=\"%\">\n" Objs[i].name l_TriggerEnterString l_TriggerExitString l_TriggerStayString core pos[1] pos[2] pos[3] rot.x rot.y rot.z rot.w (getUserProp Objs[i] "physics_generate_type") (getUserProp Objs[i] "physics_material") (getUserProp Objs[i] "physics_layer") true to:file
			format "\t\t<active_actor actor_name=\"player\"/>\n" to:file
			format "\t</instance_mesh>\n" to:file
		) else (
			if((getUserProp Objs[i] "render_export_type") == "animated_instance_mesh") then
			(
				core = (getUserProp Objs[i] "anim_model_core")
			) else (
				core = (getUserProp Objs[i] "core_name")
			)
			
			if (getUserProp Objs[i] "physics_generate") then
			(
				if ((getUserProp Objs[i] "physics_generate_type") == "plane_shape") then
				(
					format "\t<% name=\"%\" layer =\"%\" core_name=\"%\" position=\"% % %\" rotation=\"% % % %\" create_physics=\"true\" physics_type=\"plane_shape\" physics_offset=\"%\" physics_normal=\"% % %\" physics_material=\"%\" physics_group=\"%\" visible=\"%\"/>\n" (getUserProp Objs[i] "render_export_type") Objs[i].name (getUserProp Objs[i] "render_layer") core pos[1] pos[2] pos[3] rot.x rot.y rot.z rot.w (getUserProp Objs[i] "plane_offset") (getUserProp Objs[i] "plane_normal_x") (getUserProp Objs[i] "plane_normal_y") (getUserProp Objs[i] "plane_normal_z") (getUserProp Objs[i] "physics_material") (getUserProp Objs[i] "physics_layer") true to:file
				) else (
					format "\t<% name=\"%\" layer =\"%\" core_name=\"%\" position=\"% % %\" rotation=\"% % % %\" create_physics=\"true\" physics_type=\"%\" physics_material=\"%\" physics_group=\"%\" visible=\"%\"/>\n" (getUserProp Objs[i] "render_export_type") Objs[i].name (getUserProp Objs[i] "render_layer") core pos[1] pos[2] pos[3] rot.x rot.y rot.z rot.w (getUserProp Objs[i] "physics_generate_type") (getUserProp Objs[i] "physics_material") (getUserProp Objs[i] "physics_layer") true to:file
				)
			) else (

				local scneNode = XmlNode tag:"scene_mesh"
				rootNode.AddChild scneNode

				scneNode.AddAttribute "name" Objs[i].name
				scneNode.AddAttribute "layer" Objs[i].name
				scneNode.AddAttribute "name" Objs[i].name

				format "\t<% name=\"%\" layer =\"%\" core_name=\"%\" position=\"% % %\" rotation=\"% % % %\" 
				create_physics=\"false\" visible=\"%\"/>\n" (getUserProp Objs[i] "render_export_type") 
				Objs[i].name (getUserProp Objs[i] "render_layer") core pos[1] pos[2] pos[3] rot.x rot.y rot.z rot.w true to:file
			)
		)*/
	)
	xmlDoc.SaveXmlDocument()
)
