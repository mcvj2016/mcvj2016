fileIn "math.ms"
fileIn "utils.ms"

fn getMaterials arx Obj =
(
	local n_materials = 1
	if (Obj.material != undefined)then
	(
		if(classOf(Obj.material)==Multimaterial)then
		(
			n_materials=Obj.material.numsubs
			WriteShort arx n_materials
			for i=1 to Obj.material.numsubs do
			(
				WriteShort arx Obj.material[i].name.count #unsigned
				writeString arx Obj.material[i].name
			)
		)
		else
		(
			WriteShort arx n_materials
			WriteShort arx Obj.material.name.count #unsigned
			writeString arx Obj.material.name
		)
	)
	else
	(
		WriteShort arx n_materials
		WriteShort arx 11 #unsigned
		writeString arx "no_material"
	)
)

fn createMaterials dir filename Objs copyTexture =
(	
	local pathfile
	local relativePathfile

	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"materials"
	xmlDoc.rootNode = rootNode
	xmlDoc.filename = (dir+filename)

	local matDummyNode = XmlNode tag:"material"
	matDummyNode.AddAttribute "name" "dummy"
	matDummyNode.AddAttribute "vertex_type" "PositionNormal"
	rootNode.AddChild matDummyNode

	if Objs != undefined then
	(
		materialsDictionary = dotNetObject "System.Collections.Hashtable"
		for i=1 to Objs.count do
		(
			mat = #()
			n_materials = 1
			
			if (Objs[i].material!=undefined) then
			(
				if(classof(Objs[i].material)==Multimaterial)then
				(
					mat = Objs[i].material
					n_materials = Objs[i].material.numsubs
				)
				else
				(
					append mat Objs[i].material
				)
				
				for j=1 to n_materials do
				(
					if (materialsDictionary.Item[mat[j].name]==undefined) then
					(
						local hasDiffuse = (classof mat[j].diffuseMap == Bitmaptexture) as booleanClass
						local hasReflection = (classof mat[j].reflectionMap == Bitmaptexture) as booleanClass
						local hasBump = (classof mat[j].bumpMap == Bitmaptexture or classof mat[j].bumpMap ==  Normal_Bump) as booleanClass
						local hasLightmap = (classof mat[j].selfIllumMap == Bitmaptexture) as booleanClass
						local hasRNM = (classof mat[j].selfIllumMap == Multi_Sub_Map) as booleanClass
						local hasGlossiness = (classof mat[j].glossinessMap == Bitmaptexture) as booleanClass
						local hasSpecular = (classof mat[j].specularMap == Bitmaptexture) as booleanClass

						local matNode = XmlNode tag:"material"
						rootNode.AddChild matNode

						matNode.AddAttribute "name" mat[j].name

						local lVertexType = "PositionNormal"

						-- DIFFUSE
						if mat[j].diffuseMapEnable == true then
						(			
							lVertexType = "PositionNormalUV"
						)
						-- BUMP
						if mat[j].bumpMapEnable == true then
						(
							lVertexType = "PositionBumpUV"
						)
						-- GLOSSINESS
						if mat[j].glossinessMapEnable == true then
						(
							lVertexType = "PositionNormalUV"
						)
						-- SELFILLUM
						if mat[j].SelfIllumMapEnable == true then
						(
							lVertexType = "PositionNormalUVUV2"
						)
						-- LIGHTMAP (RNM)
						if mat[j].bumpMapEnable == true and mat[j].SelfIllumMapEnable == true then
						(
							lVertexType = "PositionNormalBumpUVUV2"
						)
						-- SPECULLAR
						if mat[j].specularMapEnable == true then
						(
							lVertexType = "PositionNormalUV"
						)
						matNode.AddAttribute "vertex_type" lVertexType

						local technique = ""
						if (hasDiffuse and not hasReflection and not hasBump and not hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "lights"
						)
						else if (not hasDiffuse and hasReflection and not hasBump and hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "skybox"
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "reflection"
						)
						else if (hasDiffuse and not hasReflection and hasBump and not hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "bump"
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and not hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "glossiness"
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and not hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "specularmap"
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "bump_env"
						)
						else if (hasDiffuse and not hasReflection and hasBump and hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "lightmap_bump"
						)						
						else if (hasDiffuse and hasReflection  and hasBump and hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "lightmap_bump_env"
						)
						else if (hasDiffuse and hasReflection  and not hasBump and hasLightmap and not hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "lightmap_env"
						)						
						else if (hasDiffuse and hasReflection and hasBump and hasLightmap and not hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "lightmap_bump_specularmap"
						)
						else if (hasDiffuse and hasReflection and not hasBump and hasLightmap and not hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "lightmap_glossiness"
						)
						else if (hasDiffuse and hasReflection and not hasBump and hasLightmap and not hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "lightmap_specularmap"
						)
						else if (hasDiffuse and hasReflection and hasBump and hasLightmap and not hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "lightmap_bump_glossiness"
						)
						else if (hasDiffuse and not hasReflection and not hasBump and not hasLightmap and hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity"
						)
						else if (hasDiffuse and not hasReflection and hasBump and not hasLightmap and hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity_bump"
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity_bump_env"
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and hasRNM and not hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity_env"
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity_bump_glossiness"							
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "radiosity_bump_specularmap"
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "radiosity_glossiness"							
						)
						else if (hasDiffuse and hasReflection and not hasBump and not hasLightmap and hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "radiosity_specularmap"
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and not hasRNM and hasGlossiness and hasSpecular) then
						(
							technique = "bump_specularmap"
						)
						else if (hasDiffuse and hasReflection and hasBump and not hasLightmap and not hasRNM and hasGlossiness and not hasSpecular) then
						(
							technique = "bump_glossiness"
						)
						else
						(
							technique = "lights"
						)

						local texturesNode = XmlNode tag:"textures"
						matNode.AddChild texturesNode
						
						
						materialsDictionary.Add mat[j].name "defined"
						if (hasDiffuse == true) then
						(
							if (mat[j].diffuseMap.bitmap != undefined) then
							(
								
								if copyTexture == true then
								(
									nomTextura = filenameFromPath (mat[j].diffuseMap.bitmap.filename)					
									pathfile = (dir + "Textures\\" + nomTextura)
									relativePathfile = pathfile
									getRelativePath &relativePathFile
									local texNode = XmlNode tag:"texture"
									texNode.AddAttribute "name" nomTextura
									texNode.AddAttribute "type" "diffuse"
									texturesNode.AddChild texNode
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].diffuseMap.bitmap.filename).fileName pathfile
								)
								else
								(
									nomTextura = "default-texture.DDS"				
									local texNode = XmlNode tag:"texture"
									texNode.AddAttribute "name" nomTextura
									texNode.AddAttribute "type" "diffuse"
									texturesNode.AddChild texNode
								)
							)
						)
						
						if (hasRNM) then
						(
							nomTextura1 = filenameFromPath (mat[j].selfIllumMap.id_0_color_shader.filename)
							nomTextura2 = filenameFromPath (mat[j].selfIllumMap.id_1_color_shader.filename)
							nomTextura3 = filenameFromPath (mat[j].selfIllumMap.id_2_color_shader.filename)
							pathfile1 = (dir + "Textures\\" + nomTextura1)
							pathfile2 = (dir + "Textures\\" + nomTextura2)
							pathfile3 = (dir + "Textures\\" + nomTextura3)
							relativePathfile1 = pathfile1
							relativePathfile2 = pathfile2
							relativePathfile3 = pathfile3
							getRelativePath &relativePathFile1
							getRelativePath &relativePathFile2
							getRelativePath &relativePathFile3
							local texNode1 = XmlNode tag:"texture"
							texNode1.AddAttribute "type" "lightmap"
							texNode1.AddAttribute "name" nomTextura1
							texturesNode.AddChild texNode1
							local texNode2 = XmlNode tag:"texture"
							texNode2.AddAttribute "name" nomTextura2
							texNode2.AddAttribute "type" "lightmap2"
							texturesNode.AddChild texNode2
							local texNode3 = XmlNode tag:"texture"
							texNode3.AddAttribute "name" nomTextura3
							texNode3.AddAttribute "type" "lightma3"
							texturesNode.AddChild texNode3
							if copyTexture == true then
							(
								dirExist (dir+"textures")
								copyFile (openBitMap mat[j].selfIllumMap.id_0_color_shader.bitmap.filename).fileName pathfile1
								copyFile (openBitMap mat[j].selfIllumMap.id_1_color_shader.bitmap.filename).fileName pathfile2
								copyFile (openBitMap mat[j].selfIllumMap.id_2_color_shader.bitmap.filename).fileName pathfile3
							)
						)
						else if (hasLightmap) then
						(
							if (mat[j].selfIllumMap.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].selfIllumMap.bitmap.filename)					
								pathfile = (dir + "Textures\\" + nomTextura)
								relativePathfile = pathfile
								getRelativePath &relativePathFile
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "lightmap"
								texturesNode.AddChild texNode
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].selfIllumMap.bitmap.filename).fileName pathfile
								)
							)
						)
						
						if (hasReflection) then
						(
							if (mat[j].reflectionMap.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].reflectionMap.bitmap.filename)					
								pathfile = (dir + "Textures\\" + nomTextura)
								relativePathfile = pathfile
								getRelativePath &relativePathFile
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "reflection"
								texturesNode.AddChild texNode
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].reflectionMap.bitmap.filename).fileName pathfile
								)
							)
						)
						
						if (classof mat[j].bumpMap == Bitmaptexture) then
						(
							if (mat[j].bumpMap.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].bumpMap.bitmap.filename)					
								pathfile = (dir + "Textures\\" + nomTextura)
								relativePathfile = pathfile
								getRelativePath &relativePathFile
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "bump"
								texturesNode.AddChild texNode
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].bumpMap.bitmap.filename).fileName pathfile
								)
							)
						) 
						else if (classof mat[j].bumpMap == Normal_Bump) then
						(
							if (mat[j].bumpMap.normal_map.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].bumpMap.normal_map.bitmap.filename)					
								pathfile = (dir + "Textures\\" + nomTextura)
								relativePathfile = pathfile
								getRelativePath &relativePathFile
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "bump"
								texturesNode.AddChild texNode
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].bumpMap.normal_map.bitmap.filename).fileName pathfile
								)
							)
						)
						
						if (hasGlossiness and not hasSpecular) then
						(
							if (mat[j].glossinessMap.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].glossinessMap.bitmap.filename)	
								pathfile = (dir + "Textures\\" + nomTextura)
								relativePathfile = pathfile
								getRelativePath &relativePathFile
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "glossiness"
								texturesNode.AddChild texNode
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].glossinessMap.bitmap.filename).fileName pathfile
								)
							)
						)
						else if (hasGlossiness and hasSpecular) then
						(
							if (mat[j].specularMap.bitmap != undefined and mat[j].glossinessMap.bitmap != undefined) then
							(
								nomTextura = filenameFromPath (mat[j].glossinessMap.bitmap.filename)	
								nomTextura2 = filenameFromPath (mat[j].specularMap.bitmap.filename)	
								pathfile = (dir + "Textures\\" + nomTextura)
								pathfile2 = (dir + "Textures\\" + nomTextura2)
								relativePathfile = pathfile
								relativePathfile2 = pathfile2
								getRelativePath &relativePathFile
								getRelativePath &relativePathFile2
								local texNode = XmlNode tag:"texture"
								texNode.AddAttribute "name" nomTextura
								texNode.AddAttribute "type" "glossiness"
								texturesNode.AddChild texNode
								local texNode2 = XmlNode tag:"texture"
								texNode2.AddAttribute "name" nomTextura2
								texNode2.AddAttribute "type" "specular"
								texturesNode.AddChild texNode2
								if copyTexture == true then
								(
									dirExist (dir+"textures")
									copyFile (openBitMap mat[j].glossinessMap.bitmap.filename).fileName pathfile
									copyFile (openBitMap mat[j].specularMap.bitmap.filename).fileName pathfile2
								)
							)
						)
						
						if (hasDiffuse) then
						(
							local specularAmount = mat[j].specularLevelMapAmount/100
							local glossinessAmount = mat[j].glossiness

							local paramsNode = XmlNode tag:"parameters"
							matNode.AddChild paramsNode

							local paramEnabledNode = XmlNode tag:"parameter"
							--enabled
							paramsNode.AddChild paramEnabledNode
							paramEnabledNode.AddAttribute "type" "float"
							paramEnabledNode.AddAttribute "name" "enabled"
							paramEnabledNode.AddAttribute "value" 1.0

							--exposure
							local paramExposureNode = XmlNode tag:"parameter"
							paramsNode.AddChild paramExposureNode
							paramExposureNode.AddAttribute "type" "float"
							paramExposureNode.AddAttribute "name" "occlusion"
							paramExposureNode.AddAttribute "value" 0.8
						
							if (not hasSpecular) then
							(
								local paramMetalNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramMetalNode
								paramMetalNode.AddAttribute "type" "float"
								paramMetalNode.AddAttribute "name" "metallic"
								paramMetalNode.AddAttribute "value" 0.0	
								local paramMetalColorNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramMetalColorNode
								paramMetalColorNode.AddAttribute "type" "color"
								paramMetalColorNode.AddAttribute "name" "metal_color"
								paramMetalColorNode.AddAttribute "value" "1.0 1.0 1.0 0.0"
							)	
							--if (hasGlossiness and not hasSpecular) then
						--	(
								local paramGlossinessNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramGlossinessNode
								paramGlossinessNode.AddAttribute "type" "float"
								paramGlossinessNode.AddAttribute "name" "roughness"
								paramGlossinessNode.AddAttribute "value" glossinessAmount
								local paramReflectivityNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramReflectivityNode
								paramReflectivityNode.AddAttribute "type" "float"
								paramReflectivityNode.AddAttribute "name" "reflectivity"
								paramReflectivityNode.AddAttribute "value" specularAmount
						--	)			

							--else
							--(
								local paramSpecPowerNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramSpecPowerNode
								paramSpecPowerNode.AddAttribute "type" "float"
								paramSpecPowerNode.AddAttribute "name" "specular_power"
								paramSpecPowerNode.AddAttribute "value" glossinessAmount
								local paramSpecFactorNode = XmlNode tag:"parameter"
								paramsNode.AddChild paramSpecFactorNode
								paramSpecFactorNode.AddAttribute "type" "float"
								paramSpecFactorNode.AddAttribute "name" "specular_factor"
								paramSpecFactorNode.AddAttribute "value" specularAmount
							--)		
							local paramReflectionNode = XmlNode tag:"parameter"
							paramsNode.AddChild paramReflectionNode
							paramReflectionNode.AddAttribute "type" "float"
							paramReflectionNode.AddAttribute "name" "reflection"
							paramReflectionNode.AddAttribute "value" 1.0
							local paramSsrReflectionNode = XmlNode tag:"parameter"
							paramsNode.AddChild paramSsrReflectionNode
							paramSsrReflectionNode.AddAttribute "type" "float"
							paramSsrReflectionNode.AddAttribute "name" "ssr_reflection"
							paramSsrReflectionNode.AddAttribute "value" 0.0		
							local paramBumpFactorNode = XmlNode tag:"parameter"
							paramsNode.AddChild paramBumpFactorNode
							paramBumpFactorNode.AddAttribute "type" "float"
							paramBumpFactorNode.AddAttribute "name" "bump_factor"
							paramBumpFactorNode.AddAttribute "value" 1.0		
							local paramMipLevelsNode = XmlNode tag:"parameter"
							paramsNode.AddChild paramMipLevelsNode
							paramMipLevelsNode.AddAttribute "type" "float"
							paramMipLevelsNode.AddAttribute "name" "mip_levels"
							paramMipLevelsNode.AddAttribute "value" 20.0	
							
						)						
					)
				)
			)
		)
	)
	xmlDoc.SaveXmlDocument()
)