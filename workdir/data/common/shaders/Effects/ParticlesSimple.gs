#include "ConstantsGS.fxh"

static float sprite_sheet_width = m_RawDataValues[0];
static float sprite_sheet_height = m_RawDataValues[1];
static float lerp_sprite = m_RawDataValues[4];
static float ratio_y = m_RawDataValues[8];
static float du = 1.0 / sprite_sheet_width;
static float dv = 1.0 / sprite_sheet_height;

struct GS_INPUT
{
	float4 Pos   : SV_POSITION;
	float4 Color : COLOR0;
	float Size   : TEXCOORD0;
	float Angle  : TEXCOORD1;
	float SpriteIndex : TEXCOORD2;
};

struct PS_INPUT
{
	float4 Pos   : SV_POSITION;
	float4 Color : COLOR0;
	float2 UV    : TEXCOORD0;
	float2 UV2   : TEXCOORD1;
	float2 TextureBlendFactor : TEXCOORD2;
};

//Simple Geometry Shader

[maxvertexcount(4)]
void GS( point GS_INPUT input[1], inout TriangleStream<PS_INPUT> OutputStream )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	float halfSize = input[0].Size * 0.5;
	float4 pos = input[0].Pos;	
	l_Output.Color = input[0].Color;

	l_Output.Pos = mul( pos + halfSize * float4(1, 1, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(1, 0);
	l_Output.UV2 = float2(1, 0);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(-1, 1, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(0, 0);
	l_Output.UV2 = float2(0, 0);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(1, -1, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(1, 1);
	l_Output.UV2 = float2(1, 1);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(-1, -1, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(0, 1);
	l_Output.UV2 = float2(0, 1);
	OutputStream.Append( l_Output );

	OutputStream.RestartStrip();
}