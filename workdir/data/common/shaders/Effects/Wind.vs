#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float4 Pos : POSITION;
	float3 Normal : NORMAL;
	#ifdef HAS_COLOR
		float4 Color: COLOR;
	#endif
	float2 UV : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	#ifdef HAS_COLOR
		float4 Color: COLOR;
	#endif
	float2 UV : TEXCOORD0;
};

PS_INPUT VS(VS_INPUT IN)
{
	PS_INPUT l_Output = (PS_INPUT)0;
	float3 l_Position=IN.Pos.xyz*0.1;
	l_Position.z=l_Position.z*0.2;
	float3 l_WindDirection=float3(1,0,0);
	float l_WindSpeed=1.0;
	float l_WindAperture=3.0;
	l_Position=l_Position+l_WindDirection*l_WindAperture*sin(l_WindSpeed*m_totalTime)*IN.UV.y;
	l_Output.Pos=mul(float4(l_Position, 1.0), m_World);
	l_Output.Pos=mul(l_Output.Pos, m_ViewProjection);
	#ifdef HAS_COLOR
		l_Output.Color = IN.Color;
	#endif
	l_Output.UV=IN.UV;
	return l_Output;
}
