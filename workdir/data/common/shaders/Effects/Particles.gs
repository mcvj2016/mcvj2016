#include "ConstantsGS.fxh"

static float sprite_sheet_width = m_RawDataValues[0];
static float sprite_sheet_height = m_RawDataValues[1];
static float lerp_sprite = m_RawDataValues[4];
static float ratio_y = m_RawDataValues[8];
static float du = 1.0 / sprite_sheet_width;
static float dv = 1.0 / sprite_sheet_height;

struct GS_INPUT
{
	float4 Pos   : SV_POSITION;
	float4 Color : COLOR0;
	float Size   : TEXCOORD0;
	float Angle  : TEXCOORD1;
	float SpriteIndex : TEXCOORD2;
};

struct PS_INPUT
{
	float4 Pos   : SV_POSITION;
	float4 Color : COLOR0;
	float2 UV    : TEXCOORD0;
	float2 UV2   : TEXCOORD1;
	float2 TextureBlendFactor : TEXCOORD2;
};

//Geometry Shader

[maxvertexcount(4)]
void GS( point GS_INPUT input[1], inout TriangleStream<PS_INPUT> OutputStream )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	float halfSize = input[0].Size * 0.5;
	float4 pos = input[0].Pos;
	float spriteIndex1 = floor(input[0].SpriteIndex);
	float spriteIndex1X = fmod(spriteIndex1, sprite_sheet_width);
	// "spriteIndex / sprite_sheet_width"
	float spriteIndex1Y = floor(spriteIndex1 * du);
	float spriteIndex2 = fmod(spriteIndex1 + 1.0, sprite_sheet_width * sprite_sheet_height);
	float spriteIndex2X = fmod(spriteIndex2, sprite_sheet_width);
	// "spriteIndex / sprite_sheet_width"
	float spriteIndex2Y = floor(spriteIndex2 * du);
	l_Output.TextureBlendFactor = input[0].SpriteIndex - spriteIndex1;
	float x = 1.41421356237 * cos(input[0].Angle + 3.14159265359 * 0.25);
	float y = 1.41421356237 * sin(input[0].Angle + 3.14159265359 * 0.25) * ratio_y;

	l_Output.Color = input[0].Color;

	l_Output.Pos = mul( pos + halfSize * float4(x,	y,	0.0, 0.0), m_Projection );
	l_Output.UV = float2(spriteIndex1X * du, spriteIndex1Y * dv);
	l_Output.UV2 = float2(spriteIndex2X * du, spriteIndex2Y * dv);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(y, -x, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(spriteIndex1X * du, spriteIndex1Y * dv + dv);
	l_Output.UV2 = float2(spriteIndex2X * du, spriteIndex2Y * dv + dv);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(-y, x,	0.0, 0.0), m_Projection );
	l_Output.UV = float2(spriteIndex1X * du + du, spriteIndex1Y * dv);
	l_Output.UV2 = float2(spriteIndex2X * du + du, spriteIndex2Y * dv);
	OutputStream.Append( l_Output );

	l_Output.Pos = mul( pos + halfSize * float4(-x, -y, 0.0, 0.0), m_Projection );
	l_Output.UV = float2(spriteIndex1X * du + du, spriteIndex1Y * dv + dv);
	l_Output.UV2 = float2(spriteIndex2X * du + du, spriteIndex2Y * dv + dv);
	OutputStream.Append( l_Output );

	OutputStream.RestartStrip();
}