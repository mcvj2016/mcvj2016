struct VS_INPUT
{
	float4 Pos : POSITION;
	#ifdef HAS_COLOR
		float4 Color: COLOR;
	#endif
	float2 UV : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	#ifdef HAS_COLOR
		float4 Color: COLOR;
	#endif
	float2 UV : TEXCOORD0;
};

PS_INPUT VS(VS_INPUT IN)
{
	PS_INPUT l_Output = (PS_INPUT)0;
	l_Output.Pos=IN.Pos;
	#ifdef HAS_COLOR
		l_Output.Color = IN.Color;
	#endif
	l_Output.UV=IN.UV;
	return l_Output;
}
