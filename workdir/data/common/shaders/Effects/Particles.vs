#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 Pos	: POSITION;
	float4 Color: COLOR0;
	float2 UV   : TEXCOORD0;
	float2 UV2  : TEXCOORD1;
};

struct GS_INPUT
{
	float4 Pos  : SV_POSITION;
	float4 Color: COLOR0;
	float Size  : TEXCOORD0;
	float Angle : TEXCOORD1;
	float SpriteIndex : TEXCOORD2;
};

GS_INPUT VS( VS_INPUT IN )
{
	GS_INPUT l_Output = (GS_INPUT)0;
	l_Output.Pos = mul( float4(IN.Pos, 1.0), m_World );
	l_Output.Pos = mul( l_Output.Pos, m_View );
	l_Output.Color = IN.Color;
	l_Output.Size = IN.UV.x;
	l_Output.Angle = IN.UV.y;
	l_Output.SpriteIndex = IN.UV2.x;
	return l_Output;
}
