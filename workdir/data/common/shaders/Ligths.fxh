void CalculateSingleLight(uint id, float3 NormalPix, float3 WorldPos, float3 PixColor, out float3 DiffuseColor, out float3 SpecularColor)
{
	// Initialize outputs.
	//DiffuseColor = float3(0.0, 0.0, 0.0);
	//SpecularColor = float3(0.0, 0.0, 0.0);
	
	float3 l_Eye = m_CameraPosition.xyz;
	float3 l_ViewDir = normalize(l_Eye - WorldPos);
	float3 l_Normal = normalize(NormalPix);
	
	if(m_LightTypeArray[id] == 2.0) //SPOT
	{
		float3 Hn = normalize(l_ViewDir - m_LightDirection[id].xyz);
		
		float l_DiffuseContrib = saturate(dot(-m_LightDirection[id].xyz,l_Normal));
	
		float l_SpecularContrib = pow(saturate(dot(Hn,l_Normal)), m_SpecularPower ) ; 
		
		DiffuseColor = l_DiffuseContrib * m_LightIntensityArray[id] * m_LightColor[id] * PixColor.xyz;
		
		SpecularColor = l_SpecularContrib * m_LightIntensityArray[id] * m_LightColor[id] * m_SpecularFactor;
	}
	else if(m_LightTypeArray[id] == 0.0) //OMNI
	{
		float3 l_LightDirection = WorldPos - m_LightPosition[id];
		
		float l_DistanceToPixel = length(l_LightDirection);
		l_LightDirection = l_LightDirection/l_DistanceToPixel;
		
		float l_DiffuseContrib = saturate(dot(-l_LightDirection,l_Normal));
		
		float3 HnPoint = normalize(l_ViewDir-l_LightDirection);
		float l_SpecularContrib = pow(saturate(dot(HnPoint,l_Normal)), m_SpecularPower); 
		
		float l_DistanceAtten = 1.0 - saturate((l_DistanceToPixel - m_LightAttenuationStartRangeArray[id])/(m_LightAttenuationEndRangeArray[id]-m_LightAttenuationStartRangeArray[id]));
		
		DiffuseColor = l_DiffuseContrib * m_LightIntensityArray[id] * m_LightColor[id] * PixColor.xyz * l_DistanceAtten;
		SpecularColor = l_SpecularContrib* m_LightIntensityArray[id] *	m_LightColor[id] * m_SpecularFactor * l_DistanceAtten;
	}
	else if(m_LightTypeArray[id] == 1.0) //DIRECTIONAL
	{		
		float3 l_LightDirection = WorldPos - m_LightPosition[id];
	
		float l_DistanceToPixel = length(l_LightDirection);
		l_LightDirection = l_LightDirection/l_DistanceToPixel;
		
		float l_DiffuseContrib = saturate(dot(-l_LightDirection,l_Normal));
		
		float3 Hn = normalize(l_ViewDir-l_LightDirection);
		float l_SpecularContrib = pow(saturate(dot(Hn,l_Normal)), m_SpecularPower); 
		
		float l_DistanceAtten = 1.0 - saturate( ( l_DistanceToPixel - m_LightAttenuationStartRangeArray[id] )/( m_LightAttenuationEndRangeArray[id] - m_LightAttenuationStartRangeArray[id] ));
		float l_SpotAngle = cos(m_LightAngleArray[id]*0.5); //Formula con valor en grados cos(g_SpotAngle*0.5*(3.1416/180.0));
		float l_SpotFallOff = cos(m_LightFallOffAngleArray[id]*0.5); //Formula con valor en grados cos(g_SpotFallOff*0.5*(3.1416/180.0));
		float l_DotAngle = dot(l_LightDirection, m_LightDirection[id]);
		
		float l_AngleAtenuation =  saturate((l_DotAngle-l_SpotFallOff)/(l_SpotAngle-l_SpotFallOff));
		
		DiffuseColor = l_DiffuseContrib * m_LightIntensityArray[id] * m_LightColor[id] * PixColor.xyz * l_DistanceAtten * l_AngleAtenuation;
		SpecularColor = l_SpecularContrib* m_LightIntensityArray[id] *	m_LightColor[id] * m_SpecularFactor * l_DistanceAtten * l_AngleAtenuation;
	}
}