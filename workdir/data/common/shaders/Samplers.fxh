#ifndef SAMPLERS_FXH
#define SAMPLERS_FXH

Texture2D T0Texture: register( t0 );	// Diffuse
Texture2D T1Texture: register( t1 );	// Bump
Texture2D T2Texture: register( t2 );	// LightMap
Texture2D T3Texture: register( t3 );	// Specular
TextureCube T4TextureCube: register( t4 );	// CubeTexture
Texture2D T5Texture: register( t5 );	// ShadowMap
Texture2D T6Texture: register( t6 );	// SSR Mask
Texture2D T7Texture: register( t7 );	// SpecularMapTexture PBR
Texture2D T8Texture: register( t8 );	// DiffuseLight PBR
Texture2D T9Texture: register( t9 );	// SpecularLight PBR

//SPLAT TEXTURES
Texture2D TSplatRTexture: register( t10 );	// SPLAT R
Texture2D TSplatRBumpTexture: register( t11 );	// SPLAT R Bump
Texture2D TSplatGTexture: register( t12 );	// SPLAT G
Texture2D TSplatGBumpTexture: register( t13 );	// SPLAT G Bump
Texture2D TSplatBTexture: register( t14 );	// SPLAT B
Texture2D TSplatBBumpTexture: register( t15 );	// SPLAT B Bump


SamplerState S0Sampler: register( s0 );
SamplerState S1Sampler: register( s1 );
SamplerState S2Sampler: register( s2 );
SamplerState S3Sampler: register( s3 );
SamplerState S4Sampler: register( s4 );
SamplerState S5Sampler: register( s5 );
SamplerState S6Sampler: register( s6 );
SamplerState S7Sampler: register( s7 );
SamplerState S8Sampler: register( s8 );
SamplerState S9Sampler: register( s9 );

//SPLAT SAMPLERS
SamplerState SSplatRSampler: register( s10 );
SamplerState SSplatRBumpSampler: register( s11 );
SamplerState SSplatGSampler: register( s12 );
SamplerState SSplatGBumpSampler: register( s13 );
SamplerState SSplatBSampler: register( s14 );
SamplerState SSplatBBumpSampler: register( s15 );

#endif //SAMPLERS_FXH