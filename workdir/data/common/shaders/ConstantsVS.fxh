#include "Globals.fxh"

#define MAX_BONES 40

cbuffer PerFrame : register( b0 )
{
    float4x4 m_View;
	float4x4 m_Projection;
	float4x4 m_ViewProjection;
	float4 m_CameraPosition;
	float4 m_CameraForwardVector;
	float4 m_CameraRightVector;
	float4 m_CameraUpVector;
	float4 m_totalTime;
	float4x4 m_InverseView;
	float4x4 m_InverseProjection;
}
 
cbuffer PerObject : register( b1 )
{
    float4x4 m_World;
}

cbuffer PerAnimatedInstance : register( b2 )
{
	float4x4 m_Bones[MAX_BONES];
}

cbuffer PerLights : register( b3 )
{
	float4 m_LightAmbient;
	float4 m_LightEnabled;
	float4 m_LightType;
	float4 m_LightPosition[MAX_LIGHTS_BY_SHADER];
	float4 m_LightDirection[MAX_LIGHTS_BY_SHADER];
	float4 m_LightAngle;
	float4 m_LightFallOffAngle;
	float4 m_LightAttenuationStartRange;
	float4 m_LightAttenuationEndRange;
	float4 m_LightIntensity;
	float4 m_LightColor[MAX_LIGHTS_BY_SHADER];
	float4 m_UseShadowMap;
	float4 m_UseShadowMask;
	float4 m_ShadowMapBias;
	float4 m_ShadowMapStrength;
	float4x4 m_LightView[MAX_LIGHTS_BY_SHADER];
	float4x4 m_LightProjection[MAX_LIGHTS_BY_SHADER];
}