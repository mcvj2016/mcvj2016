#include "Globals.fxh"
#include "Samplers.fxh"

#define MAX_OBJECT_RAW_PARAMETER 16

cbuffer PerFrame : register( b0 )
{
    float4x4 m_View;
    
	float4x4 m_Projection;
	
	float4x4 m_ViewProjection;
	float4 m_CameraPosition;
	float4 m_CameraForwardVector;
	float4 m_CameraRightVector;
	float4 m_CameraUpVector;
	float4 m_totalTime;
	float4x4 m_InverseView;
	float4x4 m_InverseProjection;
}

cbuffer PerMaterial : register( b1 )
{
	float4 m_RawData[MAX_OBJECT_RAW_PARAMETER];
}

cbuffer PerLight : register( b2 )
{
	float4 m_LightAmbient;
	float4 m_LightEnabled;
	float4 m_LightType;
	float4 m_LightPosition[MAX_LIGHTS_BY_SHADER];
	float4 m_LightDirection[MAX_LIGHTS_BY_SHADER];
	float4 m_LightAngle;
	float4 m_LightFallOffAngle;
	float4 m_LightAttenuationStartRange;
	float4 m_LightAttenuationEndRange;
	float4 m_LightIntensity;
	float4 m_LightColor[MAX_LIGHTS_BY_SHADER];
	float4 m_UseShadowMap;
	float4 m_UseShadowMask;
	float4 m_ShadowMapBias;
	float4 m_ShadowMapStrength;
	float4x4 m_LightView[MAX_LIGHTS_BY_SHADER];
	float4x4 m_LightProjection[MAX_LIGHTS_BY_SHADER];
}

static float m_RawDataValues[64]=((float[64])m_RawData);

static float m_LightEnabledArray[4]=(float[4])m_LightEnabled;
static float m_LightTypeArray[4]=(float[4])m_LightType;
static float m_LightAngleArray[4]=(float[4])m_LightAngle;
static float m_LightFallOffAngleArray[4]=(float[4])m_LightFallOffAngle;
static float m_LightAttenuationStartRangeArray[4]=(float[4])m_LightAttenuationStartRange;
static float m_LightAttenuationEndRangeArray[4]=(float[4])m_LightAttenuationEndRange;
static float m_LightIntensityArray[4]=(float[4])m_LightIntensity;
static float m_UseShadowMapArray[4]=(float[4])m_UseShadowMap;

static float4 red = float4(1,0,0,1);
static float4 blue = float4(0,0,1,1);
static float m_DiffuseColorDefault = float4(0,0.5,0,1);
static float m_SpecularColorDefault = float4(0.9, 0.9, 0.9, 1.0);

static float m_Enabled = m_RawDataValues[0];
static float m_Occlusion = m_RawDataValues[4];
static float m_Metallic = m_RawDataValues[8];
static float4 m_MetalColor = m_RawData[3];
static float m_Roughness = m_RawDataValues[16];
static float m_Reflectivity = m_RawDataValues[20];
static float m_SpecularPower = m_RawDataValues[24];
static float m_SpecularFactor = m_RawDataValues[28];
static float m_Reflection = m_RawDataValues[32];
static float m_SsrReflection = m_RawDataValues[36];
static float m_BumpFactor = m_RawDataValues[40];
static float m_MipLevels = m_RawDataValues[44];
static float r_offset = m_RawDataValues[48];
static float g_offset = m_RawDataValues[49];
static float b_offset = m_RawDataValues[50];
static float m_Wildvision = m_RawDataValues[48];

#include "Ligths.fxh"