#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 Pos : POSITION;

	#ifdef HAS_WEIGHT_INDICES
		float4 Weight : BLENDWEIGHT;
		float4 Indices : BLENDINDICES;
	#endif
	#ifdef HAS_NORMAL
		float3 Normal : NORMAL;
	#endif
	#ifdef HAS_TANGENT
		float4 Tangent : TANGENT;//24
		float4 Binormal : BINORMAL;
	#endif
	#ifdef HAS_UV
		float2 UV : TEXCOORD0;
	#endif
	#ifdef HAS_UV2
		float2 UV2 : TEXCOORD1;
	#endif
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	#ifdef HAS_NORMAL
		float3 Normal : NORMAL;
	#endif
	#ifdef HAS_TANGENT
		float3 WorldBinormal: TEXCOORD2;
		float3 WorldTangent: TEXCOORD3;
		float3 WorldNormal: TEXCOORD4;
	#endif
	#ifdef HAS_UV
		float2 UV : TEXCOORD0;
	#endif
	#ifdef HAS_UV2
		float2 UV2 : TEXCOORD1;
	#endif
	float4 WorldPos : TEXCOORD5;
	float2 Depth : TEXCOORD6;
};

PS_INPUT VS(VS_INPUT IN)
{
	PS_INPUT l_Output = (PS_INPUT)0;

	float3 l_Normal= 0;
	#ifdef HAS_NORMAL
		l_Normal = IN.Normal;
	#endif
	#ifdef HAS_WEIGHT_INDICES
		float4 l_TempPos=float4(IN.Pos.xyz, 1.0);
		float3 l_Position= 0;
		float4 l_Indices=IN.Indices;
		l_Position=mul(l_TempPos, m_Bones[l_Indices.x]).xyz * IN.Weight.x;
		l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]).xyz * IN.Weight.y;
		l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]).xyz * IN.Weight.z;
		l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]).xyz * IN.Weight.w;
		#ifdef HAS_NORMAL
			float3x3 m;
			m[0].xyz = m_Bones[l_Indices.x][0].xyz;
			m[1].xyz = m_Bones[l_Indices.x][1].xyz;
			m[2].xyz = m_Bones[l_Indices.x][2].xyz;
			l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.x;
			m[0].xyz = m_Bones[l_Indices.y][0].xyz;
			m[1].xyz = m_Bones[l_Indices.y][1].xyz;
			m[2].xyz = m_Bones[l_Indices.y][2].xyz;
			l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.y;
		#endif
		l_Output.Pos = mul(float4(l_Position, 1.0), m_World);
	#else
		l_Output.Pos = mul( float4(IN.Pos, 1.0), m_World );
	#endif

	l_Output.WorldPos = l_Output.Pos;
	l_Output.Pos = mul( l_Output.Pos, m_ViewProjection );
	l_Output.Depth=l_Output.Pos.zw;
	
	#ifdef HAS_NORMAL
		l_Output.Normal = normalize(mul(l_Normal, (float3x3)m_World));	
	#endif
	
	#ifdef HAS_UV
		l_Output.UV = IN.UV;
	#endif

	#ifdef HAS_TANGENT
		l_Output.WorldTangent = mul(IN.Tangent.xyz,(float3x3)m_World);
		l_Output.WorldBinormal = mul(cross(IN.Tangent.xyz,l_Normal),(float3x3)m_World);
		l_Output.WorldNormal = mul(l_Normal.xyz,(float3x3)m_World);
	#endif

	#ifdef HAS_UV2
		l_Output.UV2 = IN.UV2;
	#endif

	return l_Output;
}