#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 pos : POSITION;
	float4 Weight :BLENDWEIGHT;
	float4 Indices :BLENDINDICES;
	float3 Normal : NORMAL;
	float4 Tangencial : TANGENT;//24
	float4 Binormal : BINORMAL;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT
{
	float4 pos : SV_POSITION;
	float3 Normal : NORMAL;
	float3 WorldTangencial: TANGENT;
	float3 WorldBinormal: BINORMAL;
	float2 UV : TEXCOORD0;
	float3 WorldPosition : TEXCOORD1;
};

PS_INPUT mainVertexShader( VS_INPUT vtx )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	float4 l_TempPos=float4(vtx.pos.xyz, 1.0);
	float4 l_Indices=vtx.Indices;
	float4 l_Position;
	float3 l_Normal=float3(0,0,0);
	l_Position=mul(l_TempPos, m_Bones[l_Indices.x]) * vtx.Weight.x;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]) * vtx.Weight.y;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]) * vtx.Weight.z;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]) * vtx.Weight.w;
	float3x3 m;
	m[0].xyz = m_Bones[l_Indices.x][0].xyz;
	m[1].xyz = m_Bones[l_Indices.x][1].xyz;
	m[2].xyz = m_Bones[l_Indices.x][2].xyz;
	l_Normal+=mul(vtx.Normal.xyz, m)* vtx.Weight.x;
	m[0].xyz = m_Bones[l_Indices.y][0].xyz;
	m[1].xyz = m_Bones[l_Indices.y][1].xyz;
	m[2].xyz = m_Bones[l_Indices.y][2].xyz;
	l_Normal+=mul(vtx.Normal.xyz, m)* vtx.Weight.y;
	l_Normal=normalize(l_Normal);
	l_Output.pos = mul( l_Position, m_World );
	l_Output.pos = mul( l_Output.pos, m_ViewProjection );
	l_Output.Normal = normalize(mul(l_Normal, (float3x3)m_World));
	//l_Output.WorldPosition = normalize(mul(l_Normal, (float3x3)m_World));
	l_Output.WorldPosition =  mul(float4(vtx.pos.xyz, 1.0), m_World).xyz;
	l_Output.WorldTangencial = normalize(mul(vtx.Tangencial.xyz, (float3x3)m_World));
	l_Output.WorldBinormal = normalize(mul(cross(vtx.Tangencial.xyz, vtx.Normal.xyz), (float3x3)m_World));
	l_Output.UV=vtx.UV;
	return l_Output;
}