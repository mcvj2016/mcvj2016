#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 Pos : POSITION;//0
	float3 Normal : NORMAL;//12
	float4 Tangencial : TANGENT;//24
	float4 Binormal : BINORMAL;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Normal : NORMAL;
	float3 WorldTangencial: TANGENT;
	float3 WorldBinormal: BINORMAL;
	float3 WorldPosition: TEXCOORD1;
	float2 UV : TEXCOORD0;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT mainVertexShader( VS_INPUT vtx )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	l_Output.Pos = mul( float4(vtx.Pos, 1.0), m_World );
	l_Output.Pos = mul( l_Output.Pos, m_View );
	l_Output.Pos = mul( l_Output.Pos, m_Projection );
	l_Output.Normal = normalize(mul(vtx.Normal, (float3x3)m_World));
	//l_Out.Tangent=normalize(mul(vtx.Tangent.xyz, (float3x3)m_World));
	//l_Out.Binormal=normalize(mul(cross(vtx.Tangent.xyz, vtx.Normal.xyz), (float3x3)m_World));
	l_Output.WorldTangencial = normalize(mul(vtx.Tangencial.xyz, (float3x3)m_World));
	l_Output.WorldBinormal = normalize(mul(cross(vtx.Tangencial.xyz, vtx.Normal.xyz), (float3x3)m_World));
	l_Output.WorldPosition = mul(float4(vtx.Pos.xyz, 1.0), m_World).xyz;
	l_Output.UV = vtx.UV;
	return l_Output;
}