#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;
	float3 WorldPosition : TEXCOORD2;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT mainVertexShader( VS_INPUT vtx )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	l_Output.Pos = mul( float4(vtx.Pos, 1.0), m_World );
	l_Output.Pos = mul( l_Output.Pos, m_View );
	l_Output.Pos = mul( l_Output.Pos, m_Projection );
	l_Output.Normal = normalize(mul(vtx.Normal, (float3x3)m_World));
	l_Output.UV = vtx.UV;
	l_Output.UV2 = vtx.UV2;
	//l_Output.WorldPosition = normalize(mul(normalize(vtx.Normal).xyz, (float3x3)m_World));	
	l_Output.WorldPosition = mul(float4(vtx.Pos.xyz, 1.0), m_World).xyz;
	return l_Output;
}