#include "ConstantsVS.fxh"

struct VS_INPUT
{
	float3 Pos    : POSITION;
	float3 Normal : NORMAL;
};

struct PS_INPUT
{
	float4 Pos    : SV_POSITION;
	float3 Normal : NORMAL;
	float3 WorldPosition : TEXCOORD0;
};

PS_INPUT mainVertexShader( VS_INPUT vtx )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	l_Output.Pos = mul( float4(vtx.Pos, 1.0), m_World );
	l_Output.Normal = normalize(mul(vtx.Normal, (float3x3)m_World));
	l_Output.Pos = mul( l_Output.Pos, m_ViewProjection );
	l_Output.WorldPosition = normalize(mul(normalize(vtx.Normal).xyz, (float3x3)m_World));
	return l_Output;
}