workspace "UAB"
	configurations { "Debug", "Release", "Final" }
	platforms { "x32", "x64" }
	location "../workdir/solution"
	language "C++"
	debugdir "../workdir"
	nativewchar "On"
	characterset ("MBCS")

	filter "configurations:Debug"
		defines { "DEBUG"  }
		flags { "Symbols" }
		libdirs { "$(DXSDK_DIR)lib/x86/", "../lib/debug/" }
		targetsuffix "_debug"		

	filter "configurations:Release"
		defines { "NO_DEBUG" , "NDEBUG", "RELEASE" }
		optimize "On"
		libdirs { "../lib/release/" }
		targetsuffix "_rel"
	  
	filter "configurations:Final"
		defines { "NO_DEBUG", "NO_LOG", "NDEBUG", "FINAL" }
		optimize "On"
		targetsuffix "_final"
		libdirs { "../lib/release/" }
	  
project "Orkelf"
	kind "WindowedApp"
	flags { "ExtraWarnings", "NoRTTI" }
	files { "../src/Videogame/**.h", "../src/Videogame/**.cpp" }
	links {"Engine", "Base", "d3d11", "d3dcompiler","Cal3D", "Logica", "DirectXTex"}
	includedirs { 
		"../src/Engine", 
		"../src/Base"
	}
	
group "Engine"

project "Base"
	kind "StaticLib"
	files { "../src/Base/**.h", "../src/Base/**.cpp", "../src/Base/**.inl"}
	includedirs { 
		"../src/Base", 
		"../include/"
	}
	
project "Engine"
	kind "StaticLib"
	files { "../src/Engine/**.h", "../src/Engine/**.cpp", "../src/Engine/**.inl" }
	includedirs { 
		"../src/Engine", 
		"../src/Base", 
		"../src/Logica",
		"../src/3rdParty/", 
		"../include/" , 
		"../src/3rdParty/DirectXTex",
		"../src/3rdParty/Wwise/SoundEngine/Win32",
		"../src/3rdParty/Wwise/DynamicLibraries/AkSoundEngineDLL"
	}
	configuration "Debug"
		postbuildcommands {
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\physx\\debug\\*.* \$(SolutionDir)\\bin\\x32\\Debug",
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\sound\\debug\\*.* \$(SolutionDir)\\bin\\x32\\Debug"
		}
	configuration "Release"
		postbuildcommands {
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\physx\\release\\*.* \$(SolutionDir)\\bin\\x32\\Release",
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\sound\\release\\*.* \$(SolutionDir)\\bin\\x32\\Release",
			"XCOPY /Y /E /I /H \$(SolutionDir)..\\data \$(SolutionDir)\\bin\\x32\\Release\\data",
			"XCOPY /Y /E /I /H \$(SolutionDir)..\\..\\tools\\Exportador \$(SolutionDir)\\bin\\x32\\Release\\Exportador"
		}
	configuration "Final"
		postbuildcommands {
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\physx\\release\\*.* \$(SolutionDir)\\bin\\x32\\Final",
			"XCOPY /Y \$(SolutionDir)..\\..\\dlls\\sound\\release\\*.* \$(SolutionDir)\\bin\\x32\\Final",
			"XCOPY /Y /E /I /H \$(SolutionDir)..\\data \$(SolutionDir)\\bin\\x32\\Final\\data",
			"XCOPY /Y /E /I /H \$(SolutionDir)..\\..\\tools\\Exportador \$(SolutionDir)\\bin\\x32\\Final\\Exportador"
		}

project "Logica"
	kind "StaticLib"
	files { "../src/Logica/**.cpp", "../src/Logica/**.h" }
	includedirs { 
		"../src/3rdParty", 
		"../src/Engine/", 
		"../src/Base/", 
		"../src/Logica/", 
		"../include/"
	}

group "3rdParty"

project "Cal3D"
	kind "SharedLib"
	files { "../src/3rdParty/Cal3D/src/**.h", "../src/3rdParty/Cal3D/src/**.cpp", "../src/3rdParty/Cal3D/src/**.inl" }
	defines { "WIN32", "_WINDOWS", "_USRDLL", "CAL3D_EXPORTS"}
	configuration "Debug"
		targetname "Cal3D_d"
	configuration "Release"
		targetname "Cal3D"

project "DirectXTex"
    kind "StaticLib"
    files { "../src/3rdParty/DirectXTex/**.inl",  "../src/3rdParty/DirectXTex/**.cpp",  "../src/3rdParty/DirectXTex/**.h" }
	includedirs { "../src/3rdParty/DirectXTex" }
	defines {"_WIN32_WINNT=0x0600","WIN32", "_WINDOWS"}

--project "tinyfsm"
--    kind "StaticLib"
--    files {"../src/3rdParty/tinyfsm/**.hpp"}
--    includedirs {"../src/3rdParty/tinyfsm/"}